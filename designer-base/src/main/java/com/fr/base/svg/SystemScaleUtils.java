package com.fr.base.svg;

import com.bulenkov.iconloader.util.UIUtil;
import com.fr.log.FineLoggerFactory;
import com.fr.stable.StableUtils;
import com.fr.stable.os.OperatingSystem;
import org.jetbrains.annotations.NotNull;

import java.awt.GraphicsConfiguration;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.lang.reflect.Method;
import java.util.concurrent.atomic.AtomicReference;

/**
 * 获取系统Scale相关的工具类
 * @author Yvan
 * @version 10.0
 * Created by Yvan on 2020/12/17
 */
public class SystemScaleUtils {

    private static final AtomicReference<Boolean> JRE_HIDPI = new AtomicReference<>();

    private static final String HI_DPI = "hidpi";

    /**
     * 判断是否支持高清
     * @return
     */
    public static boolean isJreHiDPIEnabled() {
        if (JRE_HIDPI.get() != null) {
            return JRE_HIDPI.get();
        }
        if (OperatingSystem.isMacos()) {
            // 如果是mac os系统，直接返回true
            return true;
        }
        if (OperatingSystem.isWindows() && StableUtils.getMajorJavaVersion() <= 8) {
            // 如果是jdk8 + Windows系统，直接返回false
            return false;
        }
        synchronized (JRE_HIDPI) {
            if (JRE_HIDPI.get() != null) {
                return JRE_HIDPI.get();
            }
            boolean result = false;
            if (getBooleanProperty(HI_DPI, true)) {
                try {
                    GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
                    Class<?> sunGraphicsEnvironmentClass = Class.forName("sun.java2d.SunGraphicsEnvironment");
                    if (sunGraphicsEnvironmentClass.isInstance(ge)) {
                        try {
                            Method method = sunGraphicsEnvironmentClass.getDeclaredMethod("isUIScaleEnabled");
                            method.setAccessible(true);
                            result = (Boolean)method.invoke(ge);
                        }
                        catch (NoSuchMethodException e) {
                            FineLoggerFactory.getLogger().error(e.getMessage());
                        }
                    }
                }
                catch (Throwable ignore) {
                }
            }
            JRE_HIDPI.set(result);
            return result;
        }
    }

    public static boolean getBooleanProperty(@NotNull final String key, final boolean defaultValue) {
        final String value = System.getProperty(key);
        return value == null ? defaultValue : Boolean.parseBoolean(value);
    }

    /**
     * 获取系统Scale
     * @return
     */
    public static float sysScale() {
        // 如果检测到是retina，直接返回2
        if (UIUtil.isRetina()) {
            return 2.0f;
        }
        float scale = 1.0f;
        // 先判断是否支持高清，不支持代表此时是Windows + jdk8 的设计器，返回的scale值为1.0
        if (isJreHiDPIEnabled()) {
            // 获取屏幕图形设备对象
            GraphicsDevice graphicsDevice = GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice();
            if (graphicsDevice != null) {
                // 获取图形配置对象
                GraphicsConfiguration configuration = graphicsDevice.getDefaultConfiguration();
                if (configuration != null && configuration.getDevice().getType() != GraphicsDevice.TYPE_PRINTER) {
                    // 获取屏幕缩放率，Windows+jdk11环境下会得到用户设置的dpi值
                    scale = (float) configuration.getDefaultTransform().getScaleX();
                }
            }
        }
        return scale;
    }
}
