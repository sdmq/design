package com.fr.common.exception;

/**
 * @author hades
 * @version 10.0
 * Created by hades on 2021/12/27
 */
public interface ThrowableHandler {

    boolean process(Throwable e);

}
