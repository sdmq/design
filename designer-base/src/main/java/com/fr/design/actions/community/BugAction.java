package com.fr.design.actions.community;

import com.fr.design.login.AbstractDesignerSSO;
import com.fr.design.menu.MenuKeySet;
import com.fr.general.CloudCenter;

import javax.swing.*;

public class BugAction extends AbstractDesignerSSO {

    public BugAction() {
        this.setMenuKeySet(BUG);
        this.setName(getMenuKeySet().getMenuName());
        this.setMnemonic(getMenuKeySet().getMnemonic());
        this.setSmallIcon("/com/fr/design/images/bbs/bug");
    }

    @Override
    public String getJumpUrl() {
        return CloudCenter.getInstance().acquireUrlByKind("bbs.bugs", "https://service.fanruan.com/PF/FR/feedback?type=2");
    }

    public static final MenuKeySet BUG = new MenuKeySet() {

        @Override
        public String getMenuName() {
            return com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Basic_Community_Bug");
        }

        @Override
        public KeyStroke getKeyStroke() {
            return null;
        }

        @Override
        public char getMnemonic() {

            return 'U';
        }
    };

}
