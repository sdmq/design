package com.fr.design.actions.community;

import com.fr.base.svg.IconUtils;
import com.fr.design.actions.UpdateAction;
import com.fr.design.locale.impl.BugNeedMark;
import com.fr.design.menu.MenuKeySet;
import com.fr.design.utils.BrowseUtils;
import com.fr.general.locale.LocaleCenter;
import com.fr.general.locale.LocaleMark;

import javax.swing.*;
import java.awt.event.ActionEvent;

/**
 * @Author: Yuan.Wang
 * @Date: 2020/7/28
 */
public class BugNeedAction extends UpdateAction {
    public BugNeedAction() {
        this.setMenuKeySet(BugAndNeed);
        this.setName(getMenuKeySet().getMenuName());
        this.setMnemonic(getMenuKeySet().getMnemonic());
        this.setSmallIcon("/com/fr/design/images/bbs/need");
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        LocaleMark<String> localeMark = LocaleCenter.getMark(BugNeedMark.class);
        BrowseUtils.browser(localeMark.getValue());
    }

    public static final MenuKeySet BugAndNeed = new MenuKeySet() {
        @Override
        public char getMnemonic() {
            return 0;
        }

        @Override
        public String getMenuName() {
            return com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Basic_Community_BugAndNeed");
        }

        @Override
        public KeyStroke getKeyStroke() {
            return null;
        }
    };
}
