package com.fr.design.actions.community;

import com.fr.design.i18n.Toolkit;
import com.fr.design.login.AbstractDesignerSSO;
import com.fr.general.CloudCenter;

public class StudyPlanAction extends AbstractDesignerSSO {
    public StudyPlanAction() {
        this.setName(Toolkit.i18nText("Fine-Design_Study_Plan"));
        this.setSmallIcon("/com/fr/design/images/bbs/studyPlan");
    }

    @Override
    public String getJumpUrl() {
        return CloudCenter.getInstance().acquireUrlByKind("bbs.studyPlan", "https://edu.fanruan.com/studypath/finereport");
    }
}
