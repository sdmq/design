package com.fr.design.actions.community;

import com.fr.design.login.AbstractDesignerSSO;
import com.fr.design.menu.MenuKeySet;
import com.fr.general.CloudCenter;

import javax.swing.KeyStroke;

/**
 * Created by XINZAI on 2018/8/23.
 */
public class TechSolutionAction extends AbstractDesignerSSO {
    public TechSolutionAction() {
        this.setMenuKeySet(TSO);
        this.setName(getMenuKeySet().getMenuName());
        this.setMnemonic(getMenuKeySet().getMnemonic());
        this.setSmallIcon("/com/fr/design/images/bbs/solution");

    }

    @Override
    public String getJumpUrl() {
        return CloudCenter.getInstance().acquireUrlByKind("bbs.solution", "http://bbs.fanruan.com/forum-113-1.html");
    }

    public static final MenuKeySet TSO = new MenuKeySet() {
        @Override
        public char getMnemonic() {
            return 'T';
        }

        @Override
        public String getMenuName() {
            return com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Basic_Commuinity_Solution");
        }

        @Override
        public KeyStroke getKeyStroke() {
            return null;
        }
    };

}
