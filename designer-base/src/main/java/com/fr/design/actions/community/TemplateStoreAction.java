package com.fr.design.actions.community;

import com.fr.base.BaseUtils;
import com.fr.design.actions.UpdateAction;
import com.fr.design.login.AbstractDesignerSSO;
import com.fr.design.mainframe.share.collect.ComponentCollector;
import com.fr.design.menu.MenuKeySet;
import com.fr.design.utils.BrowseUtils;
import com.fr.general.CloudCenter;

import javax.swing.KeyStroke;
import java.awt.event.ActionEvent;

/**
 * created by Harrison on 2020/03/24
 **/
public class TemplateStoreAction extends AbstractDesignerSSO {
    
    public TemplateStoreAction() {

        this.setMenuKeySet(TEMPLATE);
        this.setName(getMenuKeySet().getMenuName());
        this.setMnemonic(getMenuKeySet().getMnemonic());
        this.setSmallIcon(BaseUtils.readIcon("/com/fr/base/images/share/template_store.png"));
    }

    public static final MenuKeySet TEMPLATE = new MenuKeySet() {
        
        @Override
        public String getMenuName() {
            
            return com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Share_Template_Store");
        }
        
        @Override
        public KeyStroke getKeyStroke() {
            return null;
        }
        
        @Override
        public char getMnemonic() {
            
            return 'T';
        }
    };

    @Override
    public String getJumpUrl() {
        ComponentCollector.getInstance().collectTepMenuEnterClick();
        return CloudCenter.getInstance().acquireUrlByKind("design.market.template", "https://market.fanruan.com/template");
    }
}
