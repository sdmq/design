package com.fr.design.actions.community;

import com.fr.base.BaseUtils;
import com.fr.design.login.AbstractDesignerSSO;
import com.fr.design.menu.MenuKeySet;
import com.fr.general.CloudCenter;

import javax.swing.KeyStroke;

public class UpAction extends AbstractDesignerSSO {

    public UpAction() {
        this.setMenuKeySet(UPDATE);
        this.setName(getMenuKeySet().getMenuName());
        this.setMnemonic(getMenuKeySet().getMnemonic());
        this.setSmallIcon(BaseUtils.readIcon("/com/fr/design/images/update.png"));
    }

	@Override
	public String getJumpUrl() {
		return CloudCenter.getInstance().acquireUrlByKind("bbs.update", "http://bbs.fanruan.com/forum.php?mod=collection&action=view&ctid=10");
	}

    public static final MenuKeySet UPDATE = new MenuKeySet() {
        @Override
        public char getMnemonic() {
            return 'U';
        }

        @Override
        public String getMenuName() {
            return com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Basic_Community_Update");
        }

        @Override
        public KeyStroke getKeyStroke() {
            return null;
        }
    };

}
