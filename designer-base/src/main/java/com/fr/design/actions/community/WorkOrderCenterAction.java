package com.fr.design.actions.community;

import com.fr.design.i18n.Toolkit;
import com.fr.general.CloudCenter;

/**
 * @Description 工单中心
 * @Author Henry.Wang
 * @Date 2021/3/8 14:02
 **/
public class WorkOrderCenterAction extends UpAction {
    public WorkOrderCenterAction() {
        this.setSmallIcon("/com/fr/design/images/bbs/workOrderCenter");
        this.setName(Toolkit.i18nText("Fine-Design_Basic_Commuinity_Work_Order_Center"));
    }

    @Override
    public String getJumpUrl() {
        return CloudCenter.getInstance().acquireUrlByKind("bbs.work.order.center", "https://service.fanruan.com/ticket");
    }
}