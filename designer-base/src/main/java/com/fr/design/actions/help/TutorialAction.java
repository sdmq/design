package com.fr.design.actions.help;

import com.fr.design.i18n.Toolkit;
import com.fr.design.login.AbstractDesignerSSO;
import com.fr.design.menu.MenuKeySet;
import com.fr.general.GeneralContext;
import com.fr.general.http.HttpToolbox;
import com.fr.i18n.UrlI18nManager;
import com.fr.stable.CommonUtils;
import com.fr.stable.ProductConstants;
import com.fr.stable.StringUtils;
import com.fr.third.org.apache.http.HttpStatus;
import com.fr.third.org.apache.http.StatusLine;
import com.fr.third.org.apache.http.client.methods.HttpGet;

import javax.swing.KeyStroke;
import java.awt.event.KeyEvent;

public class TutorialAction extends AbstractDesignerSSO {

    public TutorialAction() {
        this.setMenuKeySet(HELP_TUTORIAL);
        this.setName(getMenuKeySet().getMenuName());
        this.setMnemonic(getMenuKeySet().getMnemonic());
        this.setSmallIcon("/com/fr/design/images/bbs/help");
        this.setAccelerator(getMenuKeySet().getKeyStroke());
    }

    @Override
    public String getJumpUrl() {
        return UrlI18nManager.getInstance().getI18nUrl("help");
    }

    public String getOffLineWarnMessage() {
        return Toolkit.i18nText("Fine-Design_Offline_Helptutorial_Msg");
    }

    // 生成帮助文档 sitecenter key, help.zh_CN.10
    protected String createDocKey() {
        String locale = GeneralContext.getLocale().toString();
        return CommonUtils.join(new String[]{"help", locale, ProductConstants.MAIN_VERSION}, ".");
    }

    // 判断是否可以访问在线文档
    protected boolean isServerOnline(String url) {
        if (StringUtils.isEmpty(url)) {
            return false;
        }

        HttpGet getHelp = new HttpGet(url);
        try {
            StatusLine statusLine = HttpToolbox.getHttpClient(url).execute(getHelp).getStatusLine();
            return statusLine.getStatusCode() == HttpStatus.SC_OK;
        } catch (Exception ignore) {
            // 网络异常
            return false;
        }
    }

    public static final MenuKeySet HELP_TUTORIAL = new MenuKeySet() {
        @Override
        public char getMnemonic() {
            return 'T';
        }

        @Override
        public String getMenuName() {
            return com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Report_Community_Help");
        }

        @Override
        public KeyStroke getKeyStroke() {
            return KeyStroke.getKeyStroke(KeyEvent.VK_F1, 0);
        }
    };

}
