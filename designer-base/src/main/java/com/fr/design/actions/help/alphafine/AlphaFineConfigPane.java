package com.fr.design.actions.help.alphafine;

import com.fr.base.FRContext;
import com.fr.design.DesignerEnvManager;
import com.fr.design.dialog.BasicPane;
import com.fr.design.gui.icheckbox.UICheckBox;
import com.fr.design.gui.ilable.UILabel;
import com.fr.design.gui.itextfield.UITextField;
import com.fr.design.layout.FRGUIPaneFactory;
import com.fr.design.layout.TableLayoutHelper;
import com.fr.log.FineLoggerFactory;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

/**
 * Created by XiaXiang on 2017/4/6.
 */
public class AlphaFineConfigPane extends BasicPane {
    private static final String TYPE = "pressed";
    private static final String DISPLAY_TYPE = "+";


    private static final double COLUMN_GAP = 180;
    private static final double ROW_GAP = 25;
    private KeyStroke shortCutKeyStore = null;
    private UICheckBox enabledCheckbox, searchOnlineCheckbox, needSegmentationCheckbox, needIntelligentCustomerService, productDynamicsCheckbox, containActionCheckbox, containDocumentCheckbox, containTemplateCheckbox, containPluginCheckbox, containFileContentCheckbox;
    private UITextField shortcutsField;

    public AlphaFineConfigPane() {
        this.initComponents();
    }

    private void initComponents() {
        JPanel contentPane = FRGUIPaneFactory.createY_AXISBoxInnerContainer_L_Pane();
        createOpenPane(contentPane);
        createOnlinePane(contentPane);
        createShortcutsPane(contentPane);
        createSearchConfigPane(contentPane);
        this.setLayout(FRGUIPaneFactory.createBorderLayout());
        this.add(contentPane, BorderLayout.NORTH);

    }

    private Component[][] initSearchRangeComponents() {
        Component[][] components = new Component[][]{
                new Component[]{productDynamicsCheckbox, containActionCheckbox, containDocumentCheckbox},
                new Component[]{containTemplateCheckbox, containPluginCheckbox, containFileContentCheckbox},
        };
        return components;
    }

    private Component[][] initOnlineComponents() {
        Component[][] components = new Component[][]{
                new Component[]{searchOnlineCheckbox, needSegmentationCheckbox, null}
        };
        return components;
    }

    private void createSearchConfigPane(JPanel contentPane) {
        double[] rowSize = {ROW_GAP, ROW_GAP, ROW_GAP};

        double[] columnSize = {COLUMN_GAP, COLUMN_GAP, COLUMN_GAP};

        JPanel northPane = FRGUIPaneFactory.createTitledBorderPane(com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Basic_AlphaFine_Search_Range"));
        productDynamicsCheckbox = new UICheckBox(com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Report_AlphaFine_Product_Dynamics"));
        containActionCheckbox = new UICheckBox(com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Report_Set"));
        containPluginCheckbox = new UICheckBox(com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Report_Plugin_Addon"));
        containDocumentCheckbox = new UICheckBox(com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Report_Community_Help"));
        containTemplateCheckbox = new UICheckBox(com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Report_Templates"));
        containFileContentCheckbox = new UICheckBox(com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Basic_Templates_Content"));
        needIntelligentCustomerService = new UICheckBox(com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Basic_AlphaFine_Intelligent_Customer_Service"));
        JPanel searchConfigPane = TableLayoutHelper.createTableLayoutPane(initSearchRangeComponents(), rowSize, columnSize);
        northPane.add(searchConfigPane);
        contentPane.add(northPane);
    }

    private void createShortcutsPane(JPanel contentPane) {
        JPanel northPane = FRGUIPaneFactory.createTitledBorderPane(com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Basic_AlphaFine_Shortcut_Config"));
        shortcutsField = new UITextField();
        shortcutsField.setEditable(false);
        shortcutsField.selectAll();
        shortcutsField.setPreferredSize(new Dimension(100, 20));
        initFieldListener();
        northPane.add(new UILabel(com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Basic_Open") + ":"));
        northPane.add(shortcutsField);
        UILabel label = new UILabel(com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Basic_AlphaFine_SetShortcuts"));
        label.setForeground(Color.RED);
        northPane.add(label);
        contentPane.add(northPane);
    }

    private void initFieldListener() {
        shortcutsField.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                shortcutsField.selectAll();
            }
        });
        shortcutsField.addKeyListener(new KeyAdapter() {
            @Override
            public void keyReleased(KeyEvent e) {
                int modifier = e.getModifiers();
                if (modifier == 0) {
                    return;
                }
                int keyCode = e.getKeyCode();
                shortCutKeyStore = KeyStroke.getKeyStroke(keyCode, modifier);
                String str = shortCutKeyStore.toString();
                shortcutsField.setText(AlphaFineShortCutUtil.getDisplayShortCut(str));
                shortcutsField.selectAll();
            }
        });
    }

    private void createOnlinePane(JPanel contentPane) {
        JPanel northPane = FRGUIPaneFactory.createTitledBorderPane(com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Basic_AlphaFine_Search_Type"));
        searchOnlineCheckbox = new UICheckBox(com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Basic_AlphaFine_Enable_Internet_Search"));
        needSegmentationCheckbox = new UICheckBox(com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Basic_AlphaFine_Enable_Segmentation"));
        searchOnlineCheckbox.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (!searchOnlineCheckbox.isSelected()) {
                    productDynamicsCheckbox.setEnabled(false);
                    containPluginCheckbox.setEnabled(false);
                    containDocumentCheckbox.setEnabled(false);
                    needIntelligentCustomerService.setEnabled(false);
                    productDynamicsCheckbox.setSelected(false);
                    containPluginCheckbox.setSelected(false);
                    containDocumentCheckbox.setSelected(false);
                    needIntelligentCustomerService.setSelected(false);
                } else {
                    productDynamicsCheckbox.setEnabled(true);
                    containPluginCheckbox.setEnabled(true);
                    containDocumentCheckbox.setEnabled(true);
                    needIntelligentCustomerService.setEnabled(true);
                }
            }
        });
        double[] rowSize = {ROW_GAP};
        double[] columnSize = {COLUMN_GAP, COLUMN_GAP, COLUMN_GAP};
        JPanel onlinePane = TableLayoutHelper.createTableLayoutPane(initOnlineComponents(), rowSize, columnSize);
        northPane.add(onlinePane);
        contentPane.add(northPane);
    }

    private void createOpenPane(JPanel contentPane) {
        JPanel northPane = FRGUIPaneFactory.createTitledBorderPane(com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Basic_AlphaFine_Enable"));
        enabledCheckbox = new UICheckBox(com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Basic_AlphaFine_Enable_AlphaFine"));
        northPane.add(enabledCheckbox);
        contentPane.add(northPane);
    }

    @Override
    protected String title4PopupWindow() {
        return "AlphaFine";
    }

    public void populate(AlphaFineConfigManager alphaFineConfigManager) {

        this.enabledCheckbox.setSelected(alphaFineConfigManager.isEnabled());

        boolean enabled4Locale = FRContext.isChineseEnv() && alphaFineConfigManager.isSearchOnLine();

        this.searchOnlineCheckbox.setEnabled(FRContext.isChineseEnv());
        this.searchOnlineCheckbox.setSelected(enabled4Locale);

        this.containActionCheckbox.setSelected(alphaFineConfigManager.isContainAction());
        this.containTemplateCheckbox.setSelected(alphaFineConfigManager.isContainTemplate());
        this.containFileContentCheckbox.setSelected(alphaFineConfigManager.isContainFileContent());

        this.containDocumentCheckbox.setSelected(alphaFineConfigManager.isContainDocument() && enabled4Locale);
        this.containDocumentCheckbox.setEnabled(enabled4Locale);

        this.containPluginCheckbox.setSelected(alphaFineConfigManager.isContainPlugin() && enabled4Locale);
        this.containPluginCheckbox.setEnabled(enabled4Locale);

        this.productDynamicsCheckbox.setSelected(alphaFineConfigManager.isProductDynamics() && enabled4Locale);
        this.productDynamicsCheckbox.setEnabled(enabled4Locale);

        this.shortcutsField.setText(AlphaFineShortCutUtil.getDisplayShortCut(alphaFineConfigManager.getShortcuts()));

        this.needSegmentationCheckbox.setSelected(alphaFineConfigManager.isNeedSegmentationCheckbox());

        this.needIntelligentCustomerService.setSelected(alphaFineConfigManager.isNeedIntelligentCustomerService() && enabled4Locale);
        this.needIntelligentCustomerService.setEnabled(enabled4Locale);

        shortCutKeyStore = convert2KeyStroke(alphaFineConfigManager.getShortcuts());
    }

    public void update() {
        DesignerEnvManager designerEnvManager = DesignerEnvManager.getEnvManager();
        AlphaFineConfigManager alphaFineConfigManager = designerEnvManager.getAlphaFineConfigManager();
        alphaFineConfigManager.setContainPlugin(this.containPluginCheckbox.isSelected());
        alphaFineConfigManager.setContainAction(this.containActionCheckbox.isSelected());
        alphaFineConfigManager.setContainDocument(this.containDocumentCheckbox.isSelected());
        alphaFineConfigManager.setProductDynamics(this.productDynamicsCheckbox.isSelected());
        alphaFineConfigManager.setEnabled(this.enabledCheckbox.isSelected());
        alphaFineConfigManager.setSearchOnLine(this.searchOnlineCheckbox.isSelected());
        alphaFineConfigManager.setContainTemplate(this.containTemplateCheckbox.isSelected());
        alphaFineConfigManager.setContainFileContent(this.containFileContentCheckbox.isSelected());
        alphaFineConfigManager.setNeedSegmentationCheckbox(this.needSegmentationCheckbox.isSelected());
        alphaFineConfigManager.setNeedIntelligentCustomerService(this.needIntelligentCustomerService.isSelected());
        alphaFineConfigManager.setShortcuts(shortCutKeyStore != null ? shortCutKeyStore.toString().replace(TYPE, DISPLAY_TYPE) : this.shortcutsField.getText());
        designerEnvManager.setAlphaFineConfigManager(alphaFineConfigManager);
        try {
            DesignerEnvManager.loadLogSetting();
            DesignerEnvManager.getEnvManager().saveXMLFile();
        } catch (Exception e) {
            FineLoggerFactory.getLogger().error(e.getMessage(), e);
        }


    }


    private KeyStroke convert2KeyStroke(String ks) {
        return KeyStroke.getKeyStroke(ks.replace(DISPLAY_TYPE, TYPE));
    }

    public KeyStroke getShortCutKeyStore() {
        return shortCutKeyStore;
    }

    public void setShortCutKeyStore(KeyStroke shortCutKeyStore) {
        this.shortCutKeyStore = shortCutKeyStore;
    }

    public UICheckBox getIsContainFileContentCheckbox() {
        return containFileContentCheckbox;
    }

    public void setIsContainFileContentCheckbox(UICheckBox isContainFileContentCheckbox) {
        this.containFileContentCheckbox = isContainFileContentCheckbox;
    }
}
