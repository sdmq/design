package com.fr.design.base.clipboard;

public interface ClipboardHandler<T> {
    /**
     * 剪切
     *
     * @param selection 选中
     * @return 处理后的内容
     */
    T cut(T selection);

    /**
     * 复制
     *
     * @param selection 选中
     * @return 处理后的内容
     */
    T copy(T selection);

    /**
     * 粘贴
     *
     * @param selection 选中
     * @return 处理后的内容
     */
    T paste(T selection);

    /**
     * 支持的类型
     *
     * @param selection 内容
     * @return 是否
     */
    boolean support(Object selection);
}
