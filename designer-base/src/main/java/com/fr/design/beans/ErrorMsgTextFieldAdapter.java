package com.fr.design.beans;

import javax.swing.JComponent;

public interface ErrorMsgTextFieldAdapter {
    void setText(String str);

    String getText();

    JComponent getErrorMsgTextField();
}
