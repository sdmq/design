package com.fr.design.cell;

import com.fr.design.designer.TargetComponent;
import com.fr.design.dialog.BasicPane;

import javax.swing.JPanel;

/**
 * @author zack
 * @version 10.0
 * Created by zack on 2020/7/14
 */
public abstract class AbstractCellElementPropertyPane extends BasicPane implements CellElementPropertyComponent {
    @Override
    public JPanel toPanel() {
        return this;
    }

    @Override
    public boolean accept(TargetComponent tc) {
        return true;
    }
}