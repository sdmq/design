package com.fr.design.data;

import org.jetbrains.annotations.NotNull;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * @author rinoux
 * @version 10.0
 * Created by rinoux on 2022/3/28
 */
public final class MapCompareUtils {


    /**
     * 对比两个map 查找出相比orig，other中有哪些是新增的、删除的或者被修改的，并分别进行处理
     *
     * 对比时默认用equals方法来判断是否为 EntryEventKind#UPDATED
     *
     * @param orig         原始map
     * @param other        参考的新map
     * @param eventHandler 有区别时的事件处理器
     * @param <K>          K
     * @param <V>          V
     */
    public static <K, V> void contrastMapEntries(@NotNull Map<K, V> orig, @NotNull Map<K, V> other, @NotNull EventHandler<K, V> eventHandler) {

        contrastMapEntries(orig, other, eventHandler, UpdateRule.DEFAULT);
    }

    /**
     * 对比两个map 查找出相比orig，other中有哪些是新增的、删除的或者被修改的，并分别进行处理
     *
     * 对比时用自定义的规则来判断是否为 EntryEventKind#UPDATED
     *
     * @param orig          原始map
     * @param other         参考的新map
     * @param eventHandler  有区别时的事件处理器
     * @param updateRule    自定义的Update事件判定规则
     * @param <K>
     * @param <V>
     */
    public static <K, V> void contrastMapEntries(@NotNull Map<K, V> orig, @NotNull Map<K, V> other, @NotNull EventHandler<K, V> eventHandler, @NotNull UpdateRule<K, V> updateRule) {

        Map<K, V> copiedOrig = new LinkedHashMap<>(orig);

        other.forEach((k, v) -> {
            V existedV = copiedOrig.remove(k);
            if (existedV != null) {
                if (updateRule.needUpdate(existedV, v)) {
                    eventHandler.on(EntryEventKind.UPDATED, k, v);
                }
            } else {
                eventHandler.on(EntryEventKind.ADDED, k, v);
            }
        });

        copiedOrig.forEach((k, v) -> eventHandler.on(EntryEventKind.REMOVED, k, v));
    }


    /**
     * 事件处理器，对应比较后的三种结果的事件处理
     * @param <K>
     * @param <V>
     */
    public interface EventHandler<K, V> {
        void on(EntryEventKind entryEventKind, K k, V v);
    }

    /**
     * 判定 数据被修改 的判定规则
     * @param <K>
     * @param <V>
     */
    public interface UpdateRule<K, V> {

        EntryEventKind eventKind = EntryEventKind.UPDATED;

        UpdateRule DEFAULT = new UpdateRule() {};

        default boolean needUpdate(V origin, V v) {
            return !v.equals(origin);
        }
    }

    public enum EntryEventKind {
        ADDED,
        REMOVED,
        UPDATED;
    }
}
