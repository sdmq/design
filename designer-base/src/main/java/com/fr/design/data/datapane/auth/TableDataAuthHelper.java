package com.fr.design.data.datapane.auth;

import com.fr.base.TableData;
import com.fr.data.impl.Connection;
import com.fr.data.impl.DBTableData;
import com.fr.data.impl.NameDatabaseConnection;
import com.fr.stable.StringUtils;
import com.fr.workspace.WorkContext;
import com.fr.workspace.server.connection.DBConnectAuth;

import java.util.Collection;
import java.util.Collections;

/**
 * 数据连接权限相关的工具类
 * @author Yvan
 */
public class TableDataAuthHelper {

    /**
     * 编辑数据集时是否需要检查权限
     * @param tableData
     * @return
     */
    public static boolean needCheckAuthWhenEdit(TableData tableData) {
        // 远程设计下，编辑DBTableData时需要判断权限
        return !WorkContext.getCurrent().isLocal() && tableData instanceof DBTableData;
    }

    /**
     * 获取无权限数据连接集合
     * 远程下需要调用RPC，为耗时操作，谨慎使用
     * @return
     */
    public static Collection<String> getNoAuthConnections() {
        // 获取无权限连接集合
        Collection<String> noAuthConnections = WorkContext.getCurrent().get(DBConnectAuth.class).getNoAuthConnections();
        return noAuthConnections == null ? Collections.emptyList() : noAuthConnections;
    }

    /**
     * 通过数据集获取其数据连接的名称
     *
     * 注意：
     * 1. Connection接口本身是不提供名称的，只有我们内部为了使用方便，将其包装成了NameDataBaseConnection
     *      如果不是NameDataBaseConnection类型，则无名称，因此这里只能用判断类型的方式获取名称
     * 2. 仅支持DBTableData获取连接名
     * @return
     */
    public static String getConnectionNameByDBTableData(DBTableData tableData) {
        Connection database = tableData.getDatabase();
        if (database instanceof NameDatabaseConnection) {
            return ((NameDatabaseConnection) database).getName();
        }
        return StringUtils.EMPTY;
    }
}
