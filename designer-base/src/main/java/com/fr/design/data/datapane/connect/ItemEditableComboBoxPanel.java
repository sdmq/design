package com.fr.design.data.datapane.connect;

import com.fr.base.BaseUtils;
import com.fr.design.gui.ibutton.UIButton;
import com.fr.design.gui.icombobox.UIComboBox;
import com.fr.design.i18n.Toolkit;
import com.fr.design.layout.FRGUIPaneFactory;

import com.fr.log.FineLoggerFactory;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.util.Iterator;
import java.util.concurrent.CancellationException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JPanel;
import javax.swing.SwingWorker;

public abstract class ItemEditableComboBoxPanel extends JPanel {
	/**
	 *
	 */
	private static final long serialVersionUID = 1L;

	private static final String PENDING = Toolkit.i18nText("Fine-Design_Basic_Loading") + "...";

	protected static final Object EMPTY = new Object() {
		public String toString() {
			return "";
		}
	};

	protected UIComboBox itemComboBox;
	protected UIButton editButton;
    protected UIButton refreshButton;

    private SwingWorker<Iterator<String>, Void> refreshWorker;

	public ItemEditableComboBoxPanel() {
		super();

		initComponents();
	}

	protected void initComponents() {
		this.setLayout(FRGUIPaneFactory.createM_BorderLayout());
		Dimension buttonSize = new Dimension(26, 20);
		itemComboBox = new UIComboBox();
		itemComboBox.setEnabled(true);
		this.add(itemComboBox, BorderLayout.CENTER);
        refreshButton = new UIButton(BaseUtils.readIcon("/com/fr/design/images/control/refresh.png"));
        JPanel jPanel = FRGUIPaneFactory.createNColumnGridInnerContainer_Pane(2, 4 ,4);
        editButton = initEditButton(editButton, buttonSize);
        jPanel.add(editButton);
        jPanel.add(refreshButton);
		this.add(jPanel, BorderLayout.EAST);
        refreshButton.setPreferredSize(buttonSize);
        refreshButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                refreshItems();
            }
        });
	}

	protected UIButton initEditButton(UIButton editButton, Dimension buttonSize) {
		editButton = new UIButton(BaseUtils.readIcon("/com/fr/design/images/control/control-center2.png"));
		editButton.setPreferredSize(buttonSize);
		editButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				editItems();
			}
		});
		return editButton;
	}


	/**
	 * 给itemComboBox添加ActionListener
	 */
	public void addComboBoxActionListener(ActionListener l) {
		itemComboBox.addActionListener(l);
	}

	/*
	 * 刷新itemComboBox的内容
	 */
	protected void refreshItems() {

		if (refreshWorker != null && !refreshWorker.isDone()) {
			refreshWorker.cancel(true);
		}

		// 记录原来选中的Item,重新加载后需要再次选中
		Object lastSelectedItem = itemComboBox.getSelectedItem();

		DefaultComboBoxModel model = ((DefaultComboBoxModel) itemComboBox.getModel());
		model.removeAllElements();

		// 先加EMPTY,再加items
		model.addElement(EMPTY);
		model.addElement(PENDING);

		// 存在两种场景之前只考虑了填充场景 有populate会填充下 把这边的填充逻辑删了 所以没有问题
		// 如果是纯通过刷新按钮 没有populate 需要手动设置下上次选中的内容
		if (lastSelectedItem != null) {
			model.setSelectedItem(lastSelectedItem);
		}

		refreshWorker = new SwingWorker<Iterator<String>, Void>() {
			@Override
			protected Iterator<String> doInBackground() throws Exception {
				return items();
			}

			@Override
			protected void done() {
				try {
					Iterator<String> itemIt = get();
					model.removeElement(PENDING);
					while(itemIt.hasNext()) {
						model.addElement(itemIt.next());
					}
					// 如果加载成功 但是下拉框是可见的 下拉框高度是会固定为原始高度 不会因为填充了更多下拉项而变化
					// 需要重新设置下拉框高度 但值一样时相关事件不会生效 所以先加再减下
					if (itemComboBox.isPopupVisible()) {
						itemComboBox.setMaximumRowCount(itemComboBox.getMaximumRowCount() + 1);
						itemComboBox.setMaximumRowCount(itemComboBox.getMaximumRowCount() - 1);
					}
					afterRefreshItems();
				} catch (Exception e) {
					if (!(e instanceof CancellationException)) {
						FineLoggerFactory.getLogger().error(e.getMessage(), e);
					}
					refreshItemsError();
				}

			}
		};
		refreshWorker.execute();
	}

	/*
	 * 得到其中的itemComboBox所选中的Item
	 */
	public String getSelectedItem() {
		Object selected = itemComboBox.getSelectedItem();

		return selected instanceof String ? (String)selected : null;
	}

	/*
	 * 选中name项
	 */
	public void setSelectedItem(String name) {
		DefaultComboBoxModel model = ((DefaultComboBoxModel) itemComboBox.getModel());
		model.setSelectedItem(name);
	}

	/*
	 * 刷新ComboBox.items
	 */
	protected abstract java.util.Iterator<String> items();

	/**
	 * 刷新ComboBox.items之后
	 */
	protected void afterRefreshItems() {
		// 空实现，供子类重写
	}

	/**
	 * 刷新ComboBox.items时出现异常
	 */
	protected void refreshItemsError() {
		// 空实现，供子类重写
	}

	/*
	 * 弹出对话框编辑Items
	 */
	protected abstract void editItems();
}
