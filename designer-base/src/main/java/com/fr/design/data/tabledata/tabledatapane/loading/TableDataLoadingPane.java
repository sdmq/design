package com.fr.design.data.tabledata.tabledatapane.loading;

import com.fr.design.dialog.BasicPane;
import com.fr.design.i18n.Toolkit;

import javax.swing.JPanel;
import java.awt.CardLayout;

/**
 * @author Yvan
 */
public class TableDataLoadingPane extends BasicPane {

    /** Loading面板 */
    public static final String LOADING_PANE_NAME = "Loading";
    /** 无权限提示面板 */
    public static final String NO_AUTH_PANE_NAME = "NoAuthority";
    /** 错误提示面板 */
    public static final String ERROR_NAME = "Error";

    private CardLayout card;

    /** 加载中面板 */
    private JPanel loadingPane;
    /** 错误提示面板 */
    private JPanel errorPane;
    /** 数据连接无权限面板 */
    private JPanel noAuthorityPane;

    public TableDataLoadingPane() {
        initPanes();
    }

    private void initPanes() {
        card = new CardLayout();
        this.setLayout(card);
        loadingPane = new TipsPane(true);
        errorPane = new TipsPane(Toolkit.i18nText("Fine-Design_Basic_Database_Connection_Error"));
        noAuthorityPane = new TipsPane(Toolkit.i18nText("Fine-Design_Basic_Database_Connection_No_Auth"));
        add(LOADING_PANE_NAME, loadingPane);
        add(NO_AUTH_PANE_NAME, noAuthorityPane);
        add(ERROR_NAME, errorPane);
        switchTo(LOADING_PANE_NAME);
    }

    public void switchTo(String panelName) {
        card.show(this, panelName);
    }

    @Override
    protected String title4PopupWindow() {
        return Toolkit.i18nText("Fine-Design_Basic_DS-Database_Query");
    }
}
