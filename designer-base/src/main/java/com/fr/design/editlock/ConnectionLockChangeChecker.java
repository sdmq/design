package com.fr.design.editlock;


import com.fr.report.LockItem;

/**
 * @author Yvan
 * @version 10.0
 * Created by Yvan on 2021/1/20
 * 定义数据连接的checker
 */
public class ConnectionLockChangeChecker extends EditLockChangeChecker{

    private static class Holder {
        private static final ConnectionLockChangeChecker INSTANCE = new ConnectionLockChangeChecker();
    }

    public static ConnectionLockChangeChecker getInstance() {
        return ConnectionLockChangeChecker.Holder.INSTANCE;
    }

    public ConnectionLockChangeChecker() {
        this.lockItem = LockItem.CONNECTION;
    }
}
