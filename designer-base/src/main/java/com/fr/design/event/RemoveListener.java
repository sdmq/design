package com.fr.design.event;

/**
 * @author hades
 * @version 10.0
 * Created by hades on 2020/3/19
 */
public interface RemoveListener {

    void doRemoveAction();
}
