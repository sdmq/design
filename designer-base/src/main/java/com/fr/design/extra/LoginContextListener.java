package com.fr.design.extra;

import com.fr.design.login.DesignerLoginSource;

/**
 * Created by lp on 2016/8/16.
 */
public interface LoginContextListener {
    void showLoginContext(DesignerLoginSource source);
}
