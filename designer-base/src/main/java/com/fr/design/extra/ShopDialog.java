package com.fr.design.extra;

import com.fr.design.dialog.BasicPane;
import com.fr.design.dialog.UIDialog;
import com.fr.design.jdk.JdkVersion;
import com.fr.design.plugin.DesignerPluginContext;
import com.fr.design.utils.gui.GUICoreUtils;
import com.fr.stable.StableUtils;

import javax.swing.*;
import java.awt.*;

/**
 * Created by vito on 16/4/18.
 */
public class ShopDialog extends UIDialog {

    public ShopDialog(Frame frame, BasicPane pane) {
        super(frame);
        if (StableUtils.getMajorJavaVersion() >= 8) {
            setUndecorated(true);
        }
        JPanel panel = (JPanel) getContentPane();
        panel.setLayout(new BorderLayout());
        add(pane, BorderLayout.CENTER);
        setSize(createDefaultDimension());
        GUICoreUtils.centerWindow(this);
        setResizable(false);
        DesignerPluginContext.setPluginDialog(this);
    }

    private Dimension createDefaultDimension() {
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        // jdk11 分辨率较低 缩放较大时 屏幕高度小于或接近设定的高度 需要调整下
        if  (JdkVersion.GE_9.support() && screenSize.height - 700 < 50) {
            return new Dimension(900, screenSize.height - 100);
        } else {
            return new Dimension(900, 700);
        }
    }

    @Override
    public void checkValid() throws Exception {
        // do nothing
    }
}
