package com.fr.design.extra.exe.callback.handle;

import com.fr.design.dialog.FineJOptionPane;
import com.fr.design.dialog.link.MessageWithLink;
import com.fr.design.i18n.Toolkit;
import com.fr.plugin.error.PluginErrorCode;
import com.fr.plugin.manage.control.PluginTaskResult;

/**
 * 帮助处理插件操作（安装、更新等）的弹窗
 * @author Yvan
 */
public class PluginCallBackHelper {

    private static final String NEW_LINE_TAG = "<br/>";
    private static final String REFERENCE = Toolkit.i18nText("Fine-Design_Basic_Plugin_File_Validate_Reference");
    private static final String HELP_DOCUMENT_NAME = Toolkit.i18nText("Fine-Design_Basic_Plugin_File_Validate_HELP_DOCUMENT_NAME");
    private static final String CONNECTOR = "-";
    private static final String HELP_DOCUMENT_LINK = Toolkit.i18nText("Fine-Design_Basic_Plugin_File_Validate_HELP_DOCUMENT_LINK");


    /**
     * 展示插件操作失败后的提示弹窗
     * @param result
     * @param pluginInfo
     */
    public static void showErrorMessage(PluginTaskResult result, String pluginInfo) {
        // 单独处理下插件完整性校验失败的提示
        if (PluginCallBackHelper.needHandleInvalidatePackage(result)) {
            MessageWithLink messageWithLink = PluginCallBackHelper.generate4InvalidatePackage(pluginInfo);
            PluginTaskResultErrorDialog resultDialog = new PluginTaskResultErrorDialog(null, messageWithLink);
            resultDialog.showResult();
        } else {
            FineJOptionPane.showMessageDialog(null, pluginInfo, Toolkit.i18nText("Fine-Design_Basic_Plugin_Warning"), FineJOptionPane.ERROR_MESSAGE);
        }
    }

    /**
     * 判断是否需要处理 插件安装包校验失败 导致的安装失败任务
     * @param result
     * @return
     */
    private static boolean needHandleInvalidatePackage(PluginTaskResult result) {
        return !result.isSuccess() && result.getCode() == PluginErrorCode.InstallPackageValidateFailed;
    }

    /**
     * 根据插件原始报错信息，构建MessageWithLink
     * @param originInfo
     * @return
     */
    private static MessageWithLink generate4InvalidatePackage(String originInfo) {

        return new MessageWithLink(getSupplementaryMessage(originInfo), HELP_DOCUMENT_NAME, HELP_DOCUMENT_LINK);
    }

    /**
     * 根据插件原始报错信息，获取增加了补充说明后的信息
     * @param originInfo
     * @return
     */
    private static String getSupplementaryMessage(String originInfo) {
        return new StringBuilder()
                .append(originInfo)
                .append(NEW_LINE_TAG)
                .append(REFERENCE)
                .append(CONNECTOR)
                .toString();
    }
}
