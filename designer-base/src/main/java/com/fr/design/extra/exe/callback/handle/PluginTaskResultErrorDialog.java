package com.fr.design.extra.exe.callback.handle;

import com.fr.base.svg.IconUtils;
import com.fr.design.dialog.link.MessageWithLink;
import com.fr.design.gui.ibutton.UIButton;
import com.fr.design.gui.ilable.UILabel;
import com.fr.design.i18n.Toolkit;
import com.fr.design.layout.FRGUIPaneFactory;
import com.fr.design.layout.VerticalFlowLayout;
import com.fr.design.utils.gui.GUICoreUtils;

import javax.swing.BorderFactory;
import javax.swing.JDialog;
import javax.swing.JPanel;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * 当前仅处理Error提示，之后有需要再扩展
 * @author Yvan
 */
public class PluginTaskResultErrorDialog extends JDialog {

    private static final Dimension LABEL = new Dimension(60, 90);

    private JPanel contentPane;

    private UILabel errorLabel;

    private UIButton confirmButton;

    private MessageWithLink messageWithLink;

    public PluginTaskResultErrorDialog(Frame parent, MessageWithLink messageWithLink) {
        super(parent, true);
        this.setTitle(Toolkit.i18nText("Fine-Design_Basic_Plugin_Warning"));
        this.messageWithLink = messageWithLink;

        initContentPane();
        this.setLayout(FRGUIPaneFactory.createBorderLayout());
        this.setResizable(false);
        this.add(contentPane, BorderLayout.CENTER);
        this.setSize(new Dimension( 380, 160));
        GUICoreUtils.centerWindow(this);
    }

    /**
     * 初始化内容面板
     */
    private void initContentPane() {
        this.contentPane = FRGUIPaneFactory.createBorderLayout_S_Pane();
        // error图标
        errorLabel = new UILabel(IconUtils.readIcon("/com/fr/design/standard/system/error_tips.svg"));
        errorLabel.setPreferredSize(LABEL);
        errorLabel.setBorder(BorderFactory.createEmptyBorder(10, 20, 40, 20));
        // 提示内容
        JPanel messagePane = FRGUIPaneFactory.createBorderLayout_S_Pane();
        messagePane.add(errorLabel, BorderLayout.WEST);
        messagePane.add(messageWithLink, BorderLayout.CENTER);
        messagePane.setBorder(BorderFactory.createEmptyBorder(20, 10, 0, 10));
        this.contentPane.add(messagePane, BorderLayout.CENTER);
        // 确定按钮
        confirmButton = new UIButton(Toolkit.i18nText("Fine-Design_Basic_Button_OK"));
        confirmButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                hideResult();
            }
        });
        JPanel confirmPane = new JPanel(new VerticalFlowLayout());
        confirmPane.add(confirmButton);
        confirmPane.setBorder(BorderFactory.createEmptyBorder(0, 160, 10, 0));
        this.contentPane.add(confirmPane, BorderLayout.SOUTH);
    }

    public void showResult() {
        this.setVisible(true);
    }

    public void hideResult() {
        this.setVisible(false);
    }
}
