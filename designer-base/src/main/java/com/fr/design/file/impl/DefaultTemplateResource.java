package com.fr.design.file.impl;

import com.fr.file.FILE;
import com.fr.workspace.WorkContext;
import com.fr.workspace.server.lock.TplOperator;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * @author hades
 * @version 10.0
 * Created by hades on 2020/12/23
 */
public class DefaultTemplateResource extends AbstractTemplateResource {

    @Override
    public InputStream readTemplate(String path) throws Exception {
        return new ByteArrayInputStream(WorkContext.getCurrent().get(TplOperator.class).readAndLockFile(path));
    }

    @Override
    public OutputStream saveTemplate(FILE file) throws Exception {
        return file.asOutputStream();
    }

    @Override
    public boolean closeTemplate(String path) {
        return WorkContext.getCurrent().get(TplOperator.class).closeAndFreeFile(path);
    }

    @Override
    public boolean delete(FILE file) {
        return WorkContext.getCurrent().get(TplOperator.class).delete(file.getPath());
    }

    @Override
    public boolean rename(String from, String to) {
        return WorkContext.getCurrent().get(TplOperator.class).rename(from, to);
    }

    @Override
    public boolean exist(String path) {
        return WorkContext.getWorkResource().exist(path);
    }

    @Override
    public boolean mkdir(String path) {
        return WorkContext.getWorkResource().createDirectory(path);
    }

}
