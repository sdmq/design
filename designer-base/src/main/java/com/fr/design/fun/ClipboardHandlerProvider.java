package com.fr.design.fun;

import com.fr.stable.fun.mark.Mutable;

/**
 * created by Harrison on 2020/05/14
 **/
public interface ClipboardHandlerProvider<T> extends Mutable {
    
    String XML_TAG = "ClipboardHandlerProvider";
    
    int CURRENT_LEVEL = 1;
    
    /**
     * 剪切
     *
     * @param selection 选中
     * @return 处理后的内容
     */
    T cut(T selection);
    
    /**
     * 复制
     *
     * @param selection 选中
     * @return 处理后的内容
     */
    T copy(T selection);
    
    /**
     * 粘贴
     *
     * @param selection 选中
     * @return 处理后的内容
     */
    T paste(T selection);
    
    /**
     * 支持的类型
     *
     * @param selection 内容
     * @return 是否
     */
    boolean support(Object selection);
}
