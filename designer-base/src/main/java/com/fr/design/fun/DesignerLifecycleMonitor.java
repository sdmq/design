package com.fr.design.fun;

/**
 * 设计器生命周期接口
 *
 * @author hades
 * @version 10.0
 * Created by hades on 2021/1/26
 */
public interface DesignerLifecycleMonitor {

    String MARK_STRING = "DesignerLifecycleMonitor";

    int CURRENT_LEVEL = 1;

    /**
     * 设计器启动之前
     */
    void beforeStart();

    /**
     * 设计器启动完成 界面出现之后
     */
    void afterStart();

    /**
     * 设计器关闭退出之前
     */
    void beforeStop();

}
