package com.fr.design.fun;

import com.fr.design.actions.UpdateAction;
import com.fr.stable.fun.mark.Selectable;

/**
 * 替换插件管理入口
 * @author Lucian.Chen
 * @version 10.0
 * Created by Lucian.Chen on 2021/2/20
 */
public interface PluginManagerProvider extends Selectable {

    String MARK_STRING = "PluginManagerProvider";

    int CURRENT_LEVEL = 1;

    // 插件管理
    UpdateAction pluginManagerAction();
}
