package com.fr.design.fun;

import com.fr.design.gui.frpane.RegFieldPane;
import com.fr.stable.fun.mark.Immutable;

public interface RegPaneProvider extends Immutable {
    int CURRENT_LEVEL = 1;
    String XML_TAG = "RegPaneProvider";

    RegFieldPane createRegPane();
}
