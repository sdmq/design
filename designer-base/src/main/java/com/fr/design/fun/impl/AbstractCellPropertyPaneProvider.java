package com.fr.design.fun.impl;

import com.fr.design.fun.CellPropertyPaneProvider;
import com.fr.stable.fun.mark.API;

/**
 * Created by zhouping on 2015/11/11.
 */
@API(level = CellPropertyPaneProvider.CURRENT_LEVEL)
public abstract class AbstractCellPropertyPaneProvider extends AbstractPropertyItemPaneProvider implements CellPropertyPaneProvider {

    public int currentAPILevel() {
        return CellPropertyPaneProvider.CURRENT_LEVEL;
    }
}