package com.fr.design.fun.impl;

import com.fr.design.fun.ClipboardHandlerProvider;
import com.fr.stable.fun.impl.AbstractProvider;
import com.fr.stable.fun.mark.API;

/**
 * created by Harrison on 2020/05/14
 **/
@API(level = ClipboardHandlerProvider.CURRENT_LEVEL)
public abstract class AbstractClipboardHandlerProvider<T> extends AbstractProvider implements ClipboardHandlerProvider<T> {
    
    @Override
    public int currentAPILevel() {
        return ClipboardHandlerProvider.CURRENT_LEVEL;
    }
}
