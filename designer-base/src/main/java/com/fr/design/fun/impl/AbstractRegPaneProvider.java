package com.fr.design.fun.impl;

import com.fr.design.fun.RegPaneProvider;
import com.fr.stable.fun.mark.API;

/**
 * @author Joe
 * 2021/10/8 15:19
 */
@API(level = RegPaneProvider.CURRENT_LEVEL)
public abstract class AbstractRegPaneProvider implements RegPaneProvider {

    @Override
    public int currentAPILevel() {
        return CURRENT_LEVEL;
    }

    @Override
    public int layerIndex() {
        return DEFAULT_LAYER_INDEX;
    }
}
