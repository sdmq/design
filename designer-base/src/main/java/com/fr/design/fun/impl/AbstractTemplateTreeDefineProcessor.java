package com.fr.design.fun.impl;

import com.fr.design.fun.TemplateTreeDefineProcessor;
import com.fr.stable.fun.mark.API;

/**
 * @author hades
 * @version 10.0
 * Created by hades on 2020/12/21
 */
@API(level = TemplateTreeDefineProcessor.CURRENT_LEVEL)
public abstract class AbstractTemplateTreeDefineProcessor implements TemplateTreeDefineProcessor {

    @Override
    public int currentAPILevel() {
        return CURRENT_LEVEL;
    }

    @Override
    public int layerIndex() {
        return DEFAULT_LAYER_INDEX;
    }

}
