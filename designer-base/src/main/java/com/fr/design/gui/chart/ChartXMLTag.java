package com.fr.design.gui.chart;

/**
 * @author Bjorn
 * @version 10.0
 * Created by Bjorn on 2021-08-02
 */
public class ChartXMLTag {

    public static final String CHART_TYPE_UI_PROVIDER = "ChartTypeUIProvider";
}
