package com.fr.design.gui.frpane;

import com.fr.design.ExtraDesignClassManager;
import com.fr.design.beans.ErrorMsgTextFieldAdapter;
import com.fr.design.beans.UITextFieldAdapter;
import com.fr.design.constants.LayoutConstants;
import com.fr.design.designer.IntervalConstants;
import com.fr.design.dialog.BasicPane;
import com.fr.design.fun.TextFieldAdapterProvider;
import com.fr.design.gui.ilable.UILabel;
import com.fr.design.layout.FRGUIPaneFactory;
import com.fr.design.layout.TableLayoutHelper;
import com.fr.form.ui.TextEditor;
import com.fr.form.ui.reg.NoneReg;
import com.fr.form.ui.reg.RegExp;
import com.fr.log.FineLoggerFactory;

import javax.swing.BorderFactory;
import javax.swing.JPanel;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;

/**
 * Created by kerry on 2017/9/4.
 */
public class RegFieldPane extends RegPane {
    protected RegErrorMsgPane regErrorMsgPane;

    public RegFieldPane() {
        this(ALL_REG_TYPE);
    }

    public RegFieldPane(RegExp[] types) {
        super(types);
        initComponents();
    }

    public void initComponents() {
        this.setBorder(BorderFactory.createEmptyBorder(0, 0, IntervalConstants.INTERVAL_L1, 0));
        regErrorMsgPane = new RegErrorMsgPane();
        final RegChangeListener regChangeListener = new RegChangeListener() {

            @Override
            public void regChangeAction() {
                RegExp regExp = (RegExp) getRegComboBox().getSelectedItem();
                if (regExp instanceof NoneReg) {
                    regErrorMsgPane.setVisible(false);
                    return;
                }
                regErrorMsgPane.setVisible(true);
            }
        };
        this.addRegChangeListener(regChangeListener);
        this.add(regErrorMsgPane, BorderLayout.CENTER);
    }

    @Override
    protected String title4PopupWindow() {
        return "RegFieldPane";
    }

    public void populate(TextEditor textEditor) {
        populate(textEditor.getRegex());
        regErrorMsgPane.populate(textEditor);
    }

    public void update(TextEditor textEditor) {
        textEditor.setRegex(update());
        regErrorMsgPane.update(textEditor);
    }

    private static class RegErrorMsgPane extends BasicPane {
        private ErrorMsgTextFieldAdapter errorMsgTextFieldAdapter;

        public RegErrorMsgPane() {
            initRegErrorMsgField();
            setStyle();
        }

        private void setStyle() {
            this.setLayout(FRGUIPaneFactory.createBorderLayout());
            this.setBorder(BorderFactory.createEmptyBorder(IntervalConstants.INTERVAL_L6, IntervalConstants.INTERVAL_L5, 0, 0));
            UILabel tipLabel = new UILabel(com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Basic_Widget_Error_Tip"));
            tipLabel.setPreferredSize(new Dimension(60, 20));
            JPanel panel = TableLayoutHelper.createGapTableLayoutPane(new Component[][]{new Component[]{tipLabel, errorMsgTextFieldAdapter.getErrorMsgTextField()}}, TableLayoutHelper.FILL_LASTCOLUMN, 10, LayoutConstants.VGAP_MEDIUM);
            this.add(panel);
        }

        private void initRegErrorMsgField() {
            TextFieldAdapterProvider provider = ExtraDesignClassManager.getInstance().getSingle(TextFieldAdapterProvider.XML_TAG);
            if (provider == null) {
                errorMsgTextFieldAdapter = new UITextFieldAdapter();
                return;
            }
            try {
                errorMsgTextFieldAdapter = provider.createTextFieldAdapter();
            } catch (Exception e) {
                FineLoggerFactory.getLogger().error(e.getMessage(), e);
                errorMsgTextFieldAdapter = new UITextFieldAdapter();

            }
        }

        @Override
        protected String title4PopupWindow() {
            return "RegErrorMsg";
        }

        public void populate(TextEditor textEditor) {
            errorMsgTextFieldAdapter.setText(textEditor.getRegErrorMessage());
        }

        public void update(TextEditor textEditor) {
            textEditor.setRegErrorMessage(errorMsgTextFieldAdapter.getText());
        }
    }

}
