package com.fr.design.gui.frpane;

import com.fr.design.gui.ilable.UILabel;

import javax.swing.*;
import java.awt.*;

/**
 * @author Starryi
 * @version 10.0.18
 * Created by Starryi on 2021/7/3
 */
public class UIPercentDragPane extends JPanel {

    private final UINumberDragPane dragPane = new UINumberDragPane(0, 100, 1);

    public UIPercentDragPane() {
        setLayout(new BorderLayout());
        add(dragPane, BorderLayout.CENTER);
        add(new UILabel(" %"), BorderLayout.EAST);
    }

    public void populateBean(double value) {
        dragPane.populateBean(value * 100);
    }

    public double updateBean() {
        return dragPane.updateBean() / 100.0;
    }
}
