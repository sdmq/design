package com.fr.design.gui.ibutton;

import com.fr.stable.StringUtils;

import javax.swing.Icon;

public class UIHead {
    private String text = StringUtils.EMPTY;
    private Icon icon = null;
    private boolean enable = true;
    private int index = 0;

    public UIHead(String text, int index) {
        this.text = text;
        this.index = index;
    }

    public UIHead(String text, int index, boolean enable) {
        this(text, index);
        this.enable = enable;
    }

    public UIHead(Icon icon, int index) {
        this.icon = icon;
        this.index = index;
    }

    public UIHead(Icon icon, int index, boolean enable) {
        this(icon, index);
        this.enable = enable;
    }

    public UIHead(String text, Icon icon, int index) {
        this.text = text;
        this.icon = icon;
        this.index = index;
    }

    public UIHead(String text, Icon icon, int index, boolean enable) {
        this(text, icon, index);
        this.enable = enable;
    }

    public boolean isOnlyText() {
        return StringUtils.isNotEmpty(text) && icon == null;
    }

    public String getText() {
        return text;
    }

    public Icon getIcon() {
        return icon;
    }

    public boolean isEnable() {
        return enable;
    }

    public int getIndex() {
        return index;
    }
}
