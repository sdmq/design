package com.fr.design.gui.ifilechooser;

/**
 * @author hades
 * @version 10.0
 * Created by hades on 2021/12/29
 */
public class ExtensionFilter {

    private final String des;
    private final String[] extensions;

    public ExtensionFilter(String des, String... extensions) {
        this.des = des;
        this.extensions = extensions;
    }

    public String getDes() {
        return des;
    }

    public String[] getExtensions() {
        return extensions;
    }
}
