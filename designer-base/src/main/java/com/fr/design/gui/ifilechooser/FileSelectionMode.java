package com.fr.design.gui.ifilechooser;

public enum FileSelectionMode {
    FILE, MULTIPLE_FILE, DIR
}
