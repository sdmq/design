package com.fr.design.gui.ifilechooser;

import com.fr.common.annotations.Negative;
import com.fr.design.upm.UpmUtils;
import com.fr.stable.ArrayUtils;
import com.fr.stable.StringUtils;
import java.awt.Component;
import java.io.File;
import javax.swing.JFileChooser;
import javax.swing.filechooser.FileFilter;
import javax.swing.filechooser.FileNameExtensionFilter;

/**
 * @author hades
 * @version 10.0
 * Created by hades on 2021/12/21
 */
@Negative(until = "2022-6-1")
class SwingFileChooser implements FileChooserProvider {

    private final JFileChooser fileChooser;
    private final Builder builder;

    private SwingFileChooser(Builder builder) {
        fileChooser = new JFileChooser();
        fileChooser.setFileSelectionMode(builder.fileSelectionMode);
        fileChooser.setFileFilter(builder.fileFilter);
        fileChooser.setSelectedFile(builder.selectedFile);
        fileChooser.setMultiSelectionEnabled(builder.multiSelectionEnabled);
        this.builder = builder;
    }

    @Override
    public File[] getSelectedFiles() {
        if (ArrayUtils.isNotEmpty(fileChooser.getSelectedFiles())) {
            return fileChooser.getSelectedFiles();
        } else {
            return new File[]{fileChooser.getSelectedFile()};
        }
    }

    @Override
    public File getSelectedFile() {
        return fileChooser.getSelectedFile();
    }

    @Override
    public int showDialog(Component parent) {
        if (StringUtils.isEmpty(builder.tipText)) {
            return fileChooser.showOpenDialog(parent);
        } else {
            return fileChooser.showDialog(parent, builder.tipText);
        }
    }

    @Override
    public int showOpenDialog(Component parent, String approveButtonText) {
        return fileChooser.showDialog(parent, approveButtonText);
    }

    @Override
    public void setCurrentDirectory(File file) {
        fileChooser.setCurrentDirectory(file);
    }

    @Override
    public void setMultiSelectionEnabled(boolean multiple) {
        fileChooser.setMultiSelectionEnabled(multiple);
    }

    /**
     * 和一般builder不同的是 setXXX还做参数转换逻辑
     */
    public static class Builder {

        private int fileSelectionMode = JFileChooser.FILES_ONLY;
        private FileFilter fileFilter;
        private File selectedFile;
        private boolean multiSelectionEnabled;
        private String tipText;

        public Builder setFileSelectionMode(FileSelectionMode fileSelectionMode) {
            if (FileSelectionMode.DIR.equals(fileSelectionMode)) {
                this.fileSelectionMode = JFileChooser.DIRECTORIES_ONLY;
            } else if (FileSelectionMode.FILE.equals(fileSelectionMode)) {
                this.fileSelectionMode = JFileChooser.FILES_ONLY;
            } else if (FileSelectionMode.MULTIPLE_FILE.equals(fileSelectionMode)) {
                this.fileSelectionMode = JFileChooser.FILES_AND_DIRECTORIES;
            }
            return this;
        }

        public Builder setFileFilter(String des, String... extension) {
            if (StringUtils.isNotEmpty(des) && ArrayUtils.isNotEmpty(extension)) {
                this.fileFilter = new FileNameExtensionFilter(des, UpmUtils.findMatchedExtension(extension));
            }
            return this;
        }

        public Builder setFileFilter(ExtensionFilter[] extensionFilters) {
            StringBuilder desBuilder = new StringBuilder();
            String[] extensions = new String[0];
            for (ExtensionFilter extensionFilter : extensionFilters) {
                desBuilder.append(extensionFilter.getDes()).append(" ");
                extensions = ArrayUtils.addAll(extensions, UpmUtils.findMatchedExtension(extensionFilter.getExtensions()));
            }
            if (ArrayUtils.isNotEmpty(extensionFilters)) {
                this.fileFilter = new FileNameExtensionFilter(desBuilder.toString().trim(), extensions);
            }
            return this;
        }

        public Builder setSelectedFile(String selectedPath) {
            if (StringUtils.isNotEmpty(selectedPath)) {
                this.selectedFile = new File(selectedPath);
            }
            return this;
        }

        public Builder setMultiSelectionEnabled(boolean multiSelectionEnabled) {
            this.multiSelectionEnabled = multiSelectionEnabled;
            return this;
        }

        public Builder setTipText(String tipText) {
            this.tipText = tipText;
            return this;
        }

        public SwingFileChooser build() {
            return new SwingFileChooser(this);
        }
    }
}
