package com.fr.design.gui.imenu;

import com.fr.design.editlock.EditLockChangeEvent;
import com.fr.design.editlock.EditLockChangeListener;
import com.fr.report.LockItem;

import javax.swing.Action;

/**
 * @author Yvan
 * @version 10.0
 * Created by Yvan on 2021/1/20
 */
public class UILockMenuItem extends UIMenuItem implements EditLockChangeListener {

    /**
     * 锁定状态的提示信息
     */
    private String lockedTooltips;
    /**
     * 正常状态的提示信息
     */
    private String normalTooltips;

    /**
     * 当前锁定项
     */
    private LockItem lockItem;

    public UILockMenuItem(Action action, String lockedTooltips, String normalTooltips, LockItem lockItem) {
        super(action);
        this.lockedTooltips = lockedTooltips;
        this.normalTooltips = normalTooltips;
        this.lockItem = lockItem;
        setUI(new UILockMenuItemUI());
    }

    public LockItem getLockItem() {
        return lockItem;
    }

    @Override
    public void updateLockedState(EditLockChangeEvent event) {
        this.setToolTipText(event.isLocked() ? lockedTooltips : normalTooltips);
        this.repaint();
    }
}
