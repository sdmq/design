package com.fr.design.gui.imenu;

import com.fr.design.editlock.EditLockUtils;

import javax.swing.JMenuItem;
import java.awt.Graphics;
import java.awt.Rectangle;

/**
 * @author Yvan
 * @version 10.0
 * Created by Yvan on 2021/1/20
 */
public class UILockMenuItemUI extends UIMenuItemUI{

    @Override
    protected void paintText(Graphics g, JMenuItem menuItem, Rectangle textRect, String text) {
        super.paintText(g, menuItem, textRect, text);
        // 除了绘制text之外，还需要画一下锁定图标
        UILockMenuItem lockMenuItem = (UILockMenuItem) menuItem;
        if (EditLockUtils.isLocked(lockMenuItem.getLockItem())) {
            int width = menuItem.getWidth();
            int height = menuItem.getHeight();
            g.drawImage(EditLockUtils.LOCKED_IMAGE, (int) Math.round(width * 0.9), (int) Math.round(height * 0.1), 16, 16, null);
        }

    }
}
