package com.fr.design.gui.ispinner.chart;

/**
 * @author Bjorn
 * @version 10.0
 * Created by Bjorn on 2021-01-21
 */
public class UISpinnerWithPx extends UISpinnerWithUnit {

    private static final String UNIT = "px";

    public UISpinnerWithPx(double defaultValue) {
        this(0, Double.MAX_VALUE, 1, defaultValue);
    }

    public UISpinnerWithPx(double minValue, double maxValue, double dierta, double defaultValue) {
        super(minValue, maxValue, dierta, defaultValue, UNIT);
        this.getTextField().canFillNegativeNumber(false);
    }
}
