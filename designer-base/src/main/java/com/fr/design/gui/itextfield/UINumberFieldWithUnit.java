package com.fr.design.gui.itextfield;

import com.fr.base.Utils;
import com.fr.general.ComparatorUtils;
import com.fr.stable.StringUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.awt.Toolkit;

/**
 * @author Bjorn
 * @version 10.0
 * Created by Bjorn on 2021-01-20
 * 这个控件不会限制输入的数字大小，但是同样不允许输入数字之外的字符
 */
public class UINumberFieldWithUnit extends UINumberField {

    private String unit;

    private List<String> unitList = new ArrayList<>();

    public UINumberFieldWithUnit(int columns, String unit) {
        super(columns);
        this.unit = unit;
        initUnitList();
    }

    //对单位的字符进行组合
    private void initUnitList() {
        char[] chars = unit.toCharArray();
        Set<String> set = new LinkedHashSet<>();
        initUnitList(chars, 0, StringUtils.EMPTY, set);
        unitList.addAll(set);
        Collections.sort(unitList, Comparator.comparing(String::length));
    }

    private void initUnitList(char[] chars, int index, String str, Set<String> set) {
        if (index == chars.length) {
            return;
        }
        for (int i = index; i < chars.length; i++) {
            String newStr = str + chars[i];
            set.add(newStr);
            initUnitList(chars, i + 1, newStr, set);
        }
    }

    public void setFieldDocument() {
        setDocument(new NumberDocumentNoLimit());
    }

    public void setValue(double value) {
        this.setText(Utils.doubleToString(value) + unit);
    }

    public double getValue() throws NumberFormatException {
        try {
            String text = this.getText();
            if (StringUtils.isEmpty(text)) {
                return 0;
            }

            return Double.parseDouble(text.replace(getEndString(text), StringUtils.EMPTY));
        } catch (NumberFormatException numberFormatException) {
            return UINumberField.ERROR_VALUE;
        }
    }

    private String getEndString(String text) {
        int size = unitList.size();
        for (int i = size - 1; i >= 0; i--) {
            String unit = unitList.get(i);
            if (text.endsWith(unit)) {
                return unit;

            }
        }
        return StringUtils.EMPTY;
    }

    class NumberDocumentNoLimit extends NumberDocument {

        public boolean checkString(int offset, String s, String str) {
            return (ComparatorUtils.equals(s, "F")
                    || ComparatorUtils.equals(s, "f")
                    || ComparatorUtils.equals(s, "D")
                    || ComparatorUtils.equals(s, "d")
                    || (ComparatorUtils.equals(s, ".") && getMaxDecimalLength() == 0));

        }

        protected boolean isOverMaxOrMinValue(String strIntPart, String strDecPart, String strNew) {
            return super.isOverMaxOrMinValue(strIntPart, strDecPart, strNew.replaceFirst(getEndString(strNew), StringUtils.EMPTY));
        }

        protected boolean checkNumber(String strValue, boolean isMinus) {
            try {
                if (ComparatorUtils.equals(strValue, StringUtils.EMPTY) || ComparatorUtils.equals(strValue, "-")) {
                    return false;
                }
                Double.parseDouble(strValue.replaceFirst(getEndString(strValue), StringUtils.EMPTY));
            } catch (Exception e) {
                Toolkit.getDefaultToolkit().beep();
                return true;
            }
            return false;
        }
    }
}
