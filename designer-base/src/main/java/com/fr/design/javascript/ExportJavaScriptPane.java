package com.fr.design.javascript;

import com.fr.base.BaseFormula;
import com.fr.base.Parameter;
import com.fr.base.extension.FileExtension;
import com.fr.design.dialog.BasicDialog;
import com.fr.design.dialog.BasicPane;
import com.fr.design.dialog.DialogActionAdapter;
import com.fr.design.dialog.FineJOptionPane;
import com.fr.design.editor.editor.FormulaEditor;
import com.fr.design.gui.frpane.ReportletParameterViewPane;
import com.fr.design.gui.ibutton.UIButton;
import com.fr.design.gui.ibutton.UIRadioButton;
import com.fr.design.gui.icheckbox.UICheckBox;
import com.fr.design.gui.icombobox.UIComboBox;
import com.fr.design.gui.icombobox.UIComboBoxRenderer;
import com.fr.design.gui.ilable.UILabel;
import com.fr.design.gui.itextfield.UITextField;
import com.fr.design.gui.itree.filetree.TemplateFileTree;
import com.fr.design.hyperlink.AbstractHyperLinkPane;
import com.fr.design.i18n.Toolkit;
import com.fr.design.layout.FRGUIPaneFactory;
import com.fr.design.layout.TableLayout;
import com.fr.design.layout.TableLayoutHelper;
import com.fr.design.mainframe.DesignerContext;
import com.fr.design.scrollruler.ModLineBorder;
import com.fr.file.filetree.IOFileNodeFilter;
import com.fr.general.GeneralUtils;
import com.fr.js.ExportJavaScript;
import com.fr.stable.ParameterProvider;
import com.fr.stable.StringUtils;

import javax.swing.AbstractButton;
import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.SwingUtilities;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

public class ExportJavaScriptPane extends AbstractHyperLinkPane<ExportJavaScript> {

    private ExportRadioGroup templateRadioGroup;
    private UIRadioButton currentTemplateRadio;
    private UIRadioButton otherTemplateRadio;
    private UITextField reportPathTextField;
    private UIButton browserButton;
    private UIComboBox exportTypeComboBox;
    private ExportRadioGroup fileNameRadioGroup;
    private UIRadioButton defaultNameRadio;
    private UIRadioButton customNameRadio;
    private FormulaEditor fileNameFormulaEditor;
    private UICheckBox extendParametersCheckBox;
    private ReportletParameterViewPane parameterViewPane;

    private static final double p = TableLayout.PREFERRED;
    private static final Map<String, String> EXPORT_TYPES_MAP = new HashMap<>();
    private static final String CURRENT_TEMPLATE = "current";
    private static final String DEFAULT_FILENAME = "default";


    static {
        EXPORT_TYPES_MAP.put(ExportJavaScript.EXPORT_PDF, Toolkit.i18nText("Fine-Design_Basic_Export_JS_PDF"));
        EXPORT_TYPES_MAP.put(ExportJavaScript.EXPORT_EXCEL_PAGE, Toolkit.i18nText("Fine-Design_Basic_Export_JS_Excel_Page"));
        EXPORT_TYPES_MAP.put(ExportJavaScript.EXPORT_EXCEL_SIMPLE, Toolkit.i18nText("Fine-Design_Basic_Export_JS_Excel_Simple"));
        EXPORT_TYPES_MAP.put(ExportJavaScript.EXPORT_EXCEL_SHEET, Toolkit.i18nText("Fine-Design_Basic_Export_JS_Excel_Sheet"));
        EXPORT_TYPES_MAP.put(ExportJavaScript.EXPORT_WORD, Toolkit.i18nText("Fine-Design_Basic_Export_JS_Word"));
        EXPORT_TYPES_MAP.put(ExportJavaScript.EXPORT_IMAGE, Toolkit.i18nText("Fine-Design_Basic_Export_JS_Image"));
    }

    public ExportJavaScriptPane() {
        initComponents();
    }

    private void initComponents() {
        this.setLayout(FRGUIPaneFactory.createBorderLayout());
        this.setBorder(BorderFactory.createTitledBorder(new ModLineBorder(ModLineBorder.TOP), Toolkit.i18nText("Fine-Design_Basic_Export_JS_Setting")));

        //导出模板+导出方式+导出文件名
        JPanel northPane = FRGUIPaneFactory.createBorderLayout_S_Pane();

        //导出模板
        JPanel chooseTemplatePane = initChooseTemplatePane();
        northPane.add(chooseTemplatePane, BorderLayout.NORTH);

        //导出方式
        JPanel exportTypePane = initExportTypePane();
        northPane.add(exportTypePane, BorderLayout.CENTER);

        //导出文件名
        JPanel fileNamePane = initFileNamePane();
        northPane.add(fileNamePane, BorderLayout.SOUTH);

        //参数
        JPanel centerPane = FRGUIPaneFactory.createBorderLayout_S_Pane();
        JPanel paramsPane = initParamsPane();
        centerPane.add(paramsPane);

        this.add(northPane, BorderLayout.NORTH);
        this.add(centerPane, BorderLayout.CENTER);
    }

    private JPanel initParamsPane() {
        extendParametersCheckBox = new UICheckBox(Toolkit.i18nText("Fine-Design_Basic_Hyperlink_Extends_Report_Parameters"));
        extendParametersCheckBox.setSelected(true);
        parameterViewPane = new ReportletParameterViewPane(getChartParaType(), getValueEditorPane(), getValueEditorPane());
        parameterViewPane.setVisible(false);
        parameterViewPane.addTableEditorListener(new TableModelListener() {
            public void tableChanged(TableModelEvent e) {
                List<ParameterProvider> list = parameterViewPane.update();
                HashSet<String> tempSet = new HashSet<>();
                for (int i = 0; i < list.size(); i++) {
                    if (StringUtils.isEmpty(list.get(i).getName())) {
                        continue;
                    }
                    if (tempSet.contains(list.get(i).toString())) {
                        list.remove(i);
                        FineJOptionPane.showMessageDialog(DesignerContext.getDesignerFrame(), com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Basic_Parameter_Duplicate_Name") + "!");
                        return;
                    }
                    tempSet.add(list.get(i).toString());
                }
            }
        });
        extendParametersCheckBox.addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent e) {
                parameterViewPane.setVisible(e.getStateChange() == ItemEvent.DESELECTED);
            }
        });
        JPanel paramsPane = FRGUIPaneFactory.createBorderLayout_S_Pane();
        paramsPane.setBorder(BorderFactory.createTitledBorder(new ModLineBorder(ModLineBorder.TOP), Toolkit.i18nText("Fine-Design_Basic_Parameters")));
        paramsPane.add(extendParametersCheckBox, BorderLayout.NORTH);
        JPanel dynamicPaneWrapper = FRGUIPaneFactory.createBorderLayout_S_Pane();
        dynamicPaneWrapper.add(parameterViewPane);
        paramsPane.add(dynamicPaneWrapper, BorderLayout.CENTER);
        return paramsPane;
    }

    private JPanel initFileNamePane() {
        UILabel nameLabel = new UILabel(Toolkit.i18nText("Fine-Design_Basic_Export_JS_Filename") + ":");
        fileNameRadioGroup = new ExportRadioGroup();
        defaultNameRadio = new UIRadioButton(Toolkit.i18nText("Fine-Design_Basic_Export_JS_Filename_Default"));
        defaultNameRadio.setSelected(true);
        customNameRadio = new UIRadioButton(Toolkit.i18nText("Fine-Design_Basic_Export_JS_Filename_Custom"));
        addRadioToGroup(fileNameRadioGroup, defaultNameRadio, customNameRadio);
        fileNameFormulaEditor = new FormulaEditor(Toolkit.i18nText("Fine-Design_Report_Parameter_Formula"));
        fileNameFormulaEditor.setEnabled(false);
        fileNameRadioGroup.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (defaultNameRadio.isSelected()) {
                    fileNameFormulaEditor.setEnabled(false);
                } else {
                    fileNameFormulaEditor.setEnabled(true);
                }
            }
        });
        Component[][] components = new Component[][]{{nameLabel, defaultNameRadio, customNameRadio, fileNameFormulaEditor}};
        JPanel fileNameRadioPane = TableLayoutHelper.createTableLayoutPane(components, new double[]{p}, new double[]{p, p, p, p});

        JPanel fileNameTipPane = FRGUIPaneFactory.createBorderLayout_S_Pane();
        UILabel fileNameTipLabel = new UILabel("<html><body style=\"color:red\">" + Toolkit.i18nText("Fine-Design_Basic_Export_JS_Title_Tip_Front") + "\\/:*?\"<>|" + Toolkit.i18nText("Fine-Design_Basic_Export_JS_Title_Tip_Back") + "</html>");
        fileNameTipPane.add(fileNameTipLabel);

        JPanel fileNamePane = FRGUIPaneFactory.createBorderLayout_S_Pane();
        fileNamePane.add(fileNameRadioPane, BorderLayout.NORTH);
        fileNamePane.add(fileNameTipPane, BorderLayout.CENTER);
        fileNameTipPane.setBorder(BorderFactory.createEmptyBorder(5,2,5,2));
        fileNamePane.setBorder(BorderFactory.createEmptyBorder(5,2,5,2));
        return fileNamePane;
    }

    private JPanel initExportTypePane() {
        UILabel typeLabel = new UILabel(Toolkit.i18nText("Fine-Design_Basic_Export_JS_Type") + ":");
        exportTypeComboBox = new UIComboBox(new DefaultComboBoxModel<String>());
        DefaultComboBoxModel<String> comboBoxModel = (DefaultComboBoxModel<String>) exportTypeComboBox.getModel();
        String[] allExportTypes = new String[]{ExportJavaScript.EXPORT_PDF, ExportJavaScript.EXPORT_EXCEL_PAGE, ExportJavaScript.EXPORT_EXCEL_SIMPLE, ExportJavaScript.EXPORT_EXCEL_SHEET, ExportJavaScript.EXPORT_WORD, ExportJavaScript.EXPORT_IMAGE};
        for (int i = 0; i < allExportTypes.length; i++) {
            comboBoxModel.addElement(allExportTypes[i]);
        }
        this.exportTypeComboBox.setRenderer(new UIComboBoxRenderer() {

            @Override
            public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected, boolean cellHasFocus) {
                super.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);
                if (value instanceof String) {
                    this.setText(EXPORT_TYPES_MAP.get(value));
                }
                return this;
            }
        });
        Component[][] components = new Component[][]{{typeLabel, exportTypeComboBox}};

        JPanel exportTypePane = TableLayoutHelper.createTableLayoutPane(components, new double[]{p}, new double[]{p, p});
        exportTypePane.setBorder(BorderFactory.createEmptyBorder(5,2,5,2));
        return exportTypePane;
    }

    private JPanel initChooseTemplatePane() {
        UILabel templateLabel = new UILabel(Toolkit.i18nText("Fine-Design_Basic_Export_JS_Template") + ":");
        templateRadioGroup = new ExportRadioGroup();
        currentTemplateRadio = new UIRadioButton(Toolkit.i18nText("Fine-Design_Basic_Export_JS_Template_Current"));
        currentTemplateRadio.setSelected(true);
        otherTemplateRadio = new UIRadioButton(Toolkit.i18nText("Fine-Design_Basic_Export_JS_Template_Other"));
        addRadioToGroup(templateRadioGroup, currentTemplateRadio, otherTemplateRadio);
        templateRadioGroup.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (currentTemplateRadio.isSelected()) {
                    browserButton.setEnabled(false);
                } else {
                    browserButton.setEnabled(true);
                }
            }
        });
        Component[][] components = new Component[][]{{templateLabel, currentTemplateRadio, otherTemplateRadio}};
        JPanel reportletRadioPane = TableLayoutHelper.createTableLayoutPane(components, new double[]{p}, new double[]{p, p, p});

        JPanel reportletNamePane = FRGUIPaneFactory.createBorderLayout_S_Pane();
        // 路径输入框
        reportPathTextField = new UITextField(20);
        reportPathTextField.setEnabled(false);
        reportletNamePane.add(reportPathTextField, BorderLayout.CENTER);

        // 选择路径按钮
        browserButton = new UIButton(Toolkit.i18nText("Fine-Design_Basic_Select"));
        browserButton.setPreferredSize(new Dimension(browserButton.getPreferredSize().width, 20));
        browserButton.setEnabled(false);
        reportletNamePane.add(browserButton, BorderLayout.EAST);
        browserButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent evt) {
                final ReportletPane reportletPane = new ReportletPane();
                reportletPane.setSelectedReportletPath(reportPathTextField.getText());
                BasicDialog reportletDialog = reportletPane.showWindow(SwingUtilities.getWindowAncestor(ExportJavaScriptPane.this));

                reportletDialog.addDialogActionListener(new DialogActionAdapter() {
                    @Override
                    public void doOk() {
                        reportPathTextField.setText(reportletPane.getSelectedReportletPath());
                    }
                });
                reportletDialog.setVisible(true);
            }
        });

        JPanel chooseTemplatePane = FRGUIPaneFactory.createBorderLayout_S_Pane();
        chooseTemplatePane.add(reportletRadioPane, BorderLayout.NORTH);
        chooseTemplatePane.add(reportletNamePane, BorderLayout.CENTER);
        chooseTemplatePane.setBorder(BorderFactory.createEmptyBorder(0,2,5,2));

        return chooseTemplatePane;
    }

    @Override
    public void populateBean(ExportJavaScript ob) {
        if (ob == null) {
            ob = new ExportJavaScript();
        }
        this.templateRadioGroup.selectIndexButton(ob.isCurrentTemplate() ? 0 : 1);
        if (ob.isCurrentTemplate()) {
            this.browserButton.setEnabled(false);
        } else {
            this.browserButton.setEnabled(true);
        }
        this.reportPathTextField.setText(ob.getTemplatePath());
        this.exportTypeComboBox.setSelectedItem(ob.getExportType());
        this.fileNameRadioGroup.selectIndexButton(ob.isDefaultFileName() ? 0 : 1);
        if (ob.isDefaultFileName()) {
            this.fileNameFormulaEditor.setEnabled(false);
        } else {
            this.fileNameFormulaEditor.setEnabled(true);
            this.fileNameFormulaEditor.setValue(BaseFormula.createFormulaBuilder().build(ob.getFileName()));
        }
        if (ob.isExtendParameters()) {
            this.extendParametersCheckBox.setSelected(true);
        } else {
            this.extendParametersCheckBox.setSelected(false);
            List<ParameterProvider> parameterList = this.parameterViewPane.update();
            parameterList.clear();
            ParameterProvider[] parameters = ob.getParameters();
            this.parameterViewPane.populate(parameters);
        }
    }

    @Override
    public ExportJavaScript updateBean() {
        ExportJavaScript exportJavaScript = new ExportJavaScript();
        updateBean(exportJavaScript);
        return exportJavaScript;
    }

    @Override
    public void updateBean(ExportJavaScript exportJavaScript) {
        exportJavaScript.setCurrentTemplate(this.currentTemplateRadio.isSelected());
        exportJavaScript.setTemplatePath(getTemplatePath());
        exportJavaScript.setExportType(GeneralUtils.objectToString(this.exportTypeComboBox.getSelectedItem()));
        exportJavaScript.setDefaultFileName(this.defaultNameRadio.isSelected());
        exportJavaScript.setFileName(getFileName());
        exportJavaScript.setExtendParameters(this.extendParametersCheckBox.isSelected());
        if (extendParametersCheckBox.isSelected()) {
            exportJavaScript.setParameters(null);
        } else {
            List<ParameterProvider> parameterList = this.parameterViewPane.update();
            if (!parameterList.isEmpty()) {
                Parameter[] parameters = new Parameter[parameterList.size()];
                parameterList.toArray(parameters);
                exportJavaScript.setParameters(parameters);
            }
        }
    }

    private String getTemplatePath() {
        return currentTemplateRadio.isSelected() ? CURRENT_TEMPLATE : reportPathTextField.getText();
    }

    private String getFileName() {
        return defaultNameRadio.isSelected() ? DEFAULT_FILENAME : fileNameFormulaEditor.getUITextField().getText();
    }


    @Override
    public boolean accept(Object ob) {
        return ob instanceof ExportJavaScript;
    }

    @Override
    public void reset() {
        populateBean(null);
    }

    @Override
    public java.lang.String title4PopupWindow() {
        return Toolkit.i18nText("Fine-Design_Basic_Export_JS_Event");
    }

    private void addRadioToGroup(ButtonGroup buttonGroup, UIRadioButton... radios) {
        for (UIRadioButton radio : radios) {
            buttonGroup.add(radio);
        }
    }

    class ExportRadioGroup extends ButtonGroup {
        private List<UIRadioButton> radioButtons = new ArrayList<>();

        @Override
        public void add(AbstractButton button) {
            super.add(button);

            UIRadioButton radioButton = (UIRadioButton) button;
            radioButtons.add(radioButton);
        }

        public void selectIndexButton(int index) {
            if (index < 0 || index > radioButtons.size() - 1) {
                return;
            }

            UIRadioButton button = radioButtons.get(index);
            button.setSelected(true);
        }

        public void addActionListener(ActionListener actionListener) {
            for (UIRadioButton radioButton : radioButtons) {
                radioButton.addActionListener(actionListener);
            }
        }
    }

    class ReportletPane extends BasicPane {
        private TemplateFileTree templateReportletTree;
        private JScrollPane t_panel;

        private JPanel cardPane;

        public ReportletPane() {
            this.setLayout(FRGUIPaneFactory.createM_BorderLayout());

            JPanel centerPane = FRGUIPaneFactory.createBorderLayout_S_Pane();
            this.add(centerPane, BorderLayout.CENTER);

            cardPane = FRGUIPaneFactory.createCardLayout_S_Pane();
            centerPane.add(cardPane, BorderLayout.CENTER);
            cardPane.setLayout(new CardLayout());
            templateReportletTree = new TemplateFileTree();
            IOFileNodeFilter filter = new IOFileNodeFilter(new String[]{".cpt"});
            templateReportletTree.setFileNodeFilter(filter);
            cardPane.add(t_panel = new JScrollPane(templateReportletTree), "TEMPLATE");

            this.refreshEnv();
        }

        /**
         * 检查是否符合规范
         *
         * @throws Exception 抛错
         */
        @Override
        public void checkValid() throws Exception {
            String path = this.getSelectedReportletPath();
            if (path == null) {
                throw new Exception(com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Basic_Function_The_Selected_File_Cannot_Be_Null"));
            }
            if (path.endsWith(FileExtension.CPTX.getExtension())) {
                throw new Exception(Toolkit.i18nText("Fine-Design_Basic_Function_Cptx_Unsupported"));
            }
        }

        /**
         * 刷新Env
         */
        public void refreshEnv() {
            this.templateReportletTree.refreshEnv();
        }

        @Override
        protected String title4PopupWindow() {
            return Toolkit.i18nText("Fine-Design_Basic_Export_JS_Event");
        }

        /*
         * 返回选中的Reportlet的路径
         */
        public String getSelectedReportletPath() {
            if (t_panel.isVisible()) {
                return templateReportletTree.getSelectedTemplatePath();
            }
            return null;
        }

        /*
         * 选中某Reportlet
         */
        public void setSelectedReportletPath(String reportletPath) {
            if (reportletPath == null) {
                return;
            }
            templateReportletTree.setSelectedTemplatePath(reportletPath);
        }
    }
}
