package com.fr.design.jdk;

import com.fr.stable.StableUtils;

/**
 * 设计器运行jdk版本
 *
 * @author hades
 * @version 10.0
 * Created by hades on 2020/9/27
 */
public enum JdkVersion {

    /**
     * 小于或等于jdk 8
     */
    LE_8 {

        @Override
        public boolean support() {
            return StableUtils.getMajorJavaVersion() <= 8;
        }
    },

    /**
     * 大于或等于jdk 9
     */
    GE_9 {

        @Override
        public boolean support() {
            return  StableUtils.getMajorJavaVersion() >= 9;
        }
    };


    abstract public boolean support();
}
