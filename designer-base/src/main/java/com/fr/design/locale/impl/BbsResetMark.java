package com.fr.design.locale.impl;

import com.fr.general.CloudCenter;
import com.fr.general.GeneralContext;
import com.fr.general.locale.LocaleMark;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

/**
 * @author hades
 * @version 10.0
 * Created by hades on 2021/1/29
 */
public class BbsResetMark implements LocaleMark<String> {

    private final Map<Locale, String> map = new HashMap<>();
    private static final String BBS_RESET_CN = CloudCenter.getInstance().acquireUrlByKind("bbs.reset", "https://id.fanruan.com/forget/forget.php?clue=activityfr");
    private static final String BBS_RESET_TW = CloudCenter.getInstance().acquireUrlByKind("bbs.reset", "https://id.fanruan.com/forget/forget.php?clue=activityfr");
    private static final String BBS_RESET_EN = CloudCenter.getInstance().acquireUrlByKind("bbs.reset.en_US", "https://id.fanruan.com/en/forget/forget.php");
    private static final String BBS_RESET_KR = CloudCenter.getInstance().acquireUrlByKind("bbs.reset.en_US", "https://id.fanruan.com/en/forget/forget.php");
    private static final String BBS_RESET_JP = CloudCenter.getInstance().acquireUrlByKind("bbs.reset.en_US", "https://id.fanruan.com/en/forget/forget.php");

    public BbsResetMark() {
        map.put(Locale.CHINA, BBS_RESET_CN);
        map.put(Locale.KOREA, BBS_RESET_KR);
        map.put(Locale.JAPAN, BBS_RESET_JP);
        map.put(Locale.US, BBS_RESET_EN);
        map.put(Locale.TAIWAN, BBS_RESET_TW);
    }

    @Override
    public String getValue() {
        String result = map.get(GeneralContext.getLocale());
        return result == null ? BBS_RESET_EN : result;
    }
}
