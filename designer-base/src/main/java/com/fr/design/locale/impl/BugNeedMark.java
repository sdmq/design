package com.fr.design.locale.impl;

import com.fr.general.CloudCenter;
import com.fr.general.GeneralContext;
import com.fr.general.locale.LocaleMark;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

/**
 * @Author: Yuan.Wang
 * @Date: 2020/7/29
 */
public class BugNeedMark implements LocaleMark<String> {
    private Map<Locale, String> map = new HashMap<>();
    private static final String TW_BUG_AND_NEEDS = CloudCenter.getInstance().acquireUrlByKind("bbs.bug.needs.zh_TW", "https://fanruanhelp.zendesk.com/hc/zh-tw/requests/new");

    public BugNeedMark() {
        map.put(Locale.TAIWAN, TW_BUG_AND_NEEDS);
    }

    @Override
    public String getValue() {
        String result = map.get(GeneralContext.getLocale());
        return result == null ? TW_BUG_AND_NEEDS : result;
    }

}
