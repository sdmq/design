package com.fr.design.login;

import com.fr.base.FRContext;
import com.fr.design.DesignerEnvManager;
import com.fr.design.actions.UpdateAction;
import com.fr.design.login.utils.DesignerLoginUtils;
import com.fr.design.os.impl.SupportOSImpl;
import com.fr.design.utils.BrowseUtils;
import com.fr.log.FineLoggerFactory;
import com.fr.stable.StringUtils;

import java.awt.event.ActionEvent;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Lanlan
 * @version 10.0
 * Created by Lanlan on 2021/6/7
 */
public abstract class AbstractDesignerSSO extends UpdateAction {

    @Override
    public void actionPerformed(ActionEvent event) {
        String url = getJumpUrl();
        if (!DesignerLoginUtils.isOnline()) {
            String message = getOffLineWarnMessage();
            if (StringUtils.isNotEmpty(message)) {
                FineLoggerFactory.getLogger().warn(message);
            }
            BrowseUtils.browser(url);
            return;
        }
        DesignerEnvManager manager = DesignerEnvManager.getEnvManager();
        int uid = manager.getDesignerLoginUid();
        if (uid > 0) {
            String ssoUrl = DesignerLoginUtils.generateDesignerSSOUrl(url);
            BrowseUtils.browser(ssoUrl);
        } else {
            if (!SupportOSImpl.DESIGNER_LOGIN.support() || !FRContext.isChineseEnv()) {
                BrowseUtils.browser(url);
                return;
            }
            boolean loginRemindBeforeJumpBBS = manager.isLoginRemindBeforeJumpBBS();
            if (loginRemindBeforeJumpBBS) {
                Map<String, String> params = new HashMap<>();
                params.put("bbsJumpUrl", url);
                DesignerLoginHelper.showLoginDialog(DesignerLoginSource.BBS_JUMP, params);
                manager.setLoginRemindBeforeJumpBBS(false);
            } else {
                BrowseUtils.browser(url);
            }
        }
    }

    public abstract String getJumpUrl();

    public String getOffLineWarnMessage() {
        return StringUtils.EMPTY;
    }
}
