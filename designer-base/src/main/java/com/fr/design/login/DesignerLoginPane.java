package com.fr.design.login;

import com.fr.design.DesignerEnvManager;
import com.fr.design.dialog.BasicPane;
import com.fr.design.login.utils.DesignerLoginUtils;
import com.fr.design.ui.ModernUIPane;
import com.teamdev.jxbrowser.chromium.JSValue;
import com.teamdev.jxbrowser.chromium.events.ScriptContextAdapter;
import com.teamdev.jxbrowser.chromium.events.ScriptContextEvent;
import java.awt.BorderLayout;
import java.util.Map;

/**
 * @author Lanlan
 * @version 10.0
 * Created by Lanlan on 2021/5/21
 */
public class DesignerLoginPane extends BasicPane {

    @Override
    protected String title4PopupWindow() {
        return "DESIGNER_LOGIN";
    }

    public DesignerLoginPane(DesignerLoginSource source, Map<String, String> params) {
        params.put("designerLoginSource", String.valueOf(source.getSource()));
        params.put("lastLoginType", String.valueOf(DesignerEnvManager.getEnvManager().getLastLoginType().getType()));
        params.put("lastLoginAccount", DesignerEnvManager.getEnvManager().getLastLoginAccount());
        setLayout(new BorderLayout());
        ModernUIPane<Object> modernUIPane = new ModernUIPane.Builder<>()
                .prepare(new ScriptContextAdapter() {
                    @Override
                    public void onScriptContextCreated(ScriptContextEvent event) {
                        JSValue window = event.getBrowser().executeJavaScriptAndReturnValue("window");
                        window.asObject().setProperty("DesignerLoginHelper", DesignerLoginBridge.getBridge(event.getBrowser(), params));
                    }
                })
                .withEMB(DesignerLoginHelper.getMainResourcePath(), DesignerLoginUtils.renderMap())
                .build();
        add(modernUIPane, BorderLayout.CENTER);
    }
}
