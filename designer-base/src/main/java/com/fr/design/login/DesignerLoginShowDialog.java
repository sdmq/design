package com.fr.design.login;

import com.fr.design.dialog.UIDialog;
import com.fr.design.plugin.DesignerPluginContext;
import com.fr.design.utils.gui.GUICoreUtils;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dialog;
import java.awt.Dimension;
import java.awt.Frame;
import javax.swing.JPanel;

/**
 * @author Lanlan
 * @version 10.0
 * Created by Lanlan on 2021/5/21
 */
public class DesignerLoginShowDialog extends UIDialog {

    private static final Dimension DEFAULT = new Dimension(422, 478);

    public DesignerLoginShowDialog(Frame frame, Component pane) {
        super(frame);
        init(pane);
    }

    public DesignerLoginShowDialog(Dialog dialog, Component pane) {
        super(dialog);
        init(pane);
    }

    private void init(Component pane) {
        setUndecorated(true);
        JPanel panel = (JPanel) getContentPane();
        panel.setLayout(new BorderLayout());
        add(pane, BorderLayout.CENTER);
        setSize(DEFAULT);
        GUICoreUtils.centerWindow(this);
        setResizable(false);
        DesignerPluginContext.setPluginDialog(this);
    }

    @Override
    public void checkValid() throws Exception {

    }
}
