package com.fr.design.login;

/**
 * @author Lanlan
 * @version 10.0
 * Created by Lanlan on 2021/6/7
 */
public enum DesignerLoginSource {
    NORMAL(0), GUIDE(1), SWITCH_ACCOUNT(2), BBS_JUMP(3), UNKNOWN(-1);

    private int source;

    DesignerLoginSource(int source) {
        this.source = source;
    }

    public int getSource() {
        return source;
    }

    public static DesignerLoginSource valueOf(int source) {
        for(DesignerLoginSource value : DesignerLoginSource.values()) {
            if(value.getSource() == source) {
                return value;
            }
        }
        return UNKNOWN;
    }
}
