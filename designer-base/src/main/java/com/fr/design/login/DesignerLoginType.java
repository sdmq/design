package com.fr.design.login;

/**
 * @author Lanlan
 * @version 10.0
 * Created by Lanlan on 2021/6/4
 */
public enum DesignerLoginType {
    NORMAL_LOGIN(0), SMS_LOGIN(1), UNKNOWN(-1);

    private int type;

    DesignerLoginType(int type) {
        this.type = type;
    }

    public int getType() {
        return type;
    }

    public static DesignerLoginType valueOf(int type) {
        for(DesignerLoginType value : DesignerLoginType.values()) {
            if(value.getType() == type) {
                return value;
            }
        }
        return UNKNOWN;
    }
}
