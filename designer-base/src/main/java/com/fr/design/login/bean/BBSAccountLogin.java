package com.fr.design.login.bean;

/**
 * @author Lanlan
 * @version 10.0
 * Created by Lanlan on 2021/6/3
 */
public class BBSAccountLogin {

    private int uid;

    private String username;

    private String appId;

    private String refreshToken;

    public int getUid() {
        return uid;
    }

    public void setUid(int uid) {
        this.uid = uid;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getAppId() {
        return appId;
    }

    public void setAppId(String appId) {
        this.appId = appId;
    }

    public String getRefreshToken() {
        return refreshToken;
    }

    public void setRefreshToken(String refreshToken) {
        this.refreshToken = refreshToken;
    }
}
