package com.fr.design.login.config;

import com.fr.design.login.DesignerLoginType;
import com.fr.stable.StringUtils;
import com.fr.stable.xml.XMLPrintWriter;
import com.fr.stable.xml.XMLReadable;
import com.fr.stable.xml.XMLWriter;
import com.fr.stable.xml.XMLableReader;

/**
 * @author Lanlan
 * @version 10.0
 * Created by Lanlan on 2021/6/2
 */
public class DesignerLoginConfigManager implements XMLReadable, XMLWriter {

    public static final String XML_TAG = "DesignerLoginConfigManager";

    private static DesignerLoginConfigManager singleton;

    /**
     * bbs uid
     */
    private int uid = -1;
    /**
     * bbs 用户名
     */
    private String username = StringUtils.EMPTY;
    /**
     * bbs 应用Id
     */
    private String appId = StringUtils.EMPTY;
    /**
     * bbs refreshToken
     */
    private String refreshToken = StringUtils.EMPTY;
    /**
     * 登录引导页一个月内不再提醒
     */
    private boolean doNotRemind = false;
    /**
     * 登录引导页一个月内不再提醒
     */
    private long doNotRemindSelectedTime = -1L;
    /**
     * 设计器激活时间
     */
    private long designerActivatedTime = -1L;
    /**
     * bbs 上次登录时间
     */
    private long lastLoginTime = -1L;
    /**
     * bbs 上次登录方式
     */
    private DesignerLoginType lastLoginType = DesignerLoginType.UNKNOWN;
    /**
     * bbs 上次登录账号
     */
    private String lastLoginAccount = StringUtils.EMPTY;
    /**
     * 当前版本第一次启动
     */
    private boolean currentVersionFirstLaunch = true;
    /**
     * bbs跳转前的登录提醒
     */
    private boolean loginRemindBeforeJumpBBS = true;
    /**
     * 插件管理第一次启动时的提醒
     */
    private boolean pluginRemindOnFirstLaunch = true;
    /**
     * 使用旧版登录
     */
    private boolean useOldVersionLogin = false;

    private DesignerLoginConfigManager() {

    }

    public static DesignerLoginConfigManager getInstance() {
        if (singleton == null) {
            singleton = new DesignerLoginConfigManager();
        }
        return singleton;
    }

    @Override
    public void readXML(XMLableReader reader) {
        if (reader.isAttr()) {
            this.setUid(reader.getAttrAsInt("uid", -1));
            this.setUsername(reader.getAttrAsString("username", StringUtils.EMPTY));
            this.setAppId(reader.getAttrAsString("appId", StringUtils.EMPTY));
            this.setRefreshToken(reader.getAttrAsString("refreshToken", StringUtils.EMPTY));
            this.setDoNotRemind(reader.getAttrAsBoolean("doNotRemind", false));
            this.setDoNotRemindSelectedTime(reader.getAttrAsLong("doNotRemindSelectedTime", -1L));
            this.setDesignerActivatedTime(reader.getAttrAsLong("designerActivatedTime", -1L));
            this.setLastLoginTime(reader.getAttrAsLong("lastLoginTime", -1L));
            this.setCurrentVersionFirstLaunch(reader.getAttrAsBoolean("currentVersionFirstLaunch", true));
            this.setLastLoginType(DesignerLoginType.valueOf(reader.getAttrAsInt("lastLoginType", -1)));
            this.setLastLoginAccount(reader.getAttrAsString("lastLoginAccount", StringUtils.EMPTY));
            this.setLoginRemindBeforeJumpBBS(reader.getAttrAsBoolean("loginRemindBeforeJumpBBS", true));
            this.setPluginRemindOnFirstLaunch(reader.getAttrAsBoolean("pluginRemindOnFirstLaunch", true));
            this.setUseOldVersionLogin(reader.getAttrAsBoolean("useOldVersionLogin", false));
        }
    }

    @Override
    public void writeXML(XMLPrintWriter writer) {
        writer.startTAG(XML_TAG);
        writer.attr("uid", uid);
        writer.attr("username", username);
        writer.attr("appId", appId);
        writer.attr("refreshToken", refreshToken);
        writer.attr("doNotRemind", doNotRemind);
        writer.attr("doNotRemindSelectedTime", doNotRemindSelectedTime);
        writer.attr("designerActivatedTime", designerActivatedTime);
        writer.attr("lastLoginTime", lastLoginTime);
        writer.attr("currentVersionFirstLaunch", currentVersionFirstLaunch);
        writer.attr("lastLoginType", lastLoginType.getType());
        writer.attr("lastLoginAccount", lastLoginAccount);
        writer.attr("loginRemindBeforeJumpBBS", loginRemindBeforeJumpBBS);
        writer.attr("pluginRemindOnFirstLaunch", pluginRemindOnFirstLaunch);
        writer.attr("useOldVersionLogin", useOldVersionLogin);
        writer.end();
    }

    public int getUid() {
        return uid;
    }

    public void setUid(int uid) {
        this.uid = uid;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getAppId() {
        return appId;
    }

    public void setAppId(String appId) {
        this.appId = appId;
    }

    public String getRefreshToken() {
        return refreshToken;
    }

    public void setRefreshToken(String refreshToken) {
        this.refreshToken = refreshToken;
    }

    public long getDoNotRemindSelectedTime() {
        return doNotRemindSelectedTime;
    }

    public void setDoNotRemindSelectedTime(long doNotRemindSelectedTime) {
        this.doNotRemindSelectedTime = doNotRemindSelectedTime;
    }

    public boolean isDoNotRemind() {
        return doNotRemind;
    }

    public void setDoNotRemind(boolean doNotRemind) {
        this.doNotRemind = doNotRemind;
    }

    public long getDesignerActivatedTime() {
        return designerActivatedTime;
    }

    public void setDesignerActivatedTime(long designerActivatedTime) {
        this.designerActivatedTime = designerActivatedTime;
    }

    public long getLastLoginTime() {
        return lastLoginTime;
    }

    public void setLastLoginTime(long lastLoginTime) {
        this.lastLoginTime = lastLoginTime;
    }

    public boolean isCurrentVersionFirstLaunch() {
        return currentVersionFirstLaunch;
    }

    public void setCurrentVersionFirstLaunch(boolean currentVersionFirstLaunch) {
        this.currentVersionFirstLaunch = currentVersionFirstLaunch;
    }

    public DesignerLoginType getLastLoginType() {
        return lastLoginType;
    }

    public void setLastLoginType(DesignerLoginType lastLoginType) {
        this.lastLoginType = lastLoginType;
    }

    public String getLastLoginAccount() {
        return lastLoginAccount;
    }

    public void setLastLoginAccount(String lastLoginAccount) {
        this.lastLoginAccount = lastLoginAccount;
    }

    public boolean isLoginRemindBeforeJumpBBS() {
        return loginRemindBeforeJumpBBS;
    }

    public void setLoginRemindBeforeJumpBBS(boolean loginRemindBeforeJumpBBS) {
        this.loginRemindBeforeJumpBBS = loginRemindBeforeJumpBBS;
    }

    public boolean isPluginRemindOnFirstLaunch() {
        return pluginRemindOnFirstLaunch;
    }

    public void setPluginRemindOnFirstLaunch(boolean pluginRemindOnFirstLaunch) {
        this.pluginRemindOnFirstLaunch = pluginRemindOnFirstLaunch;
    }

    public boolean isUseOldVersionLogin() {
        return useOldVersionLogin;
    }

    public void setUseOldVersionLogin(boolean useOldVersionLogin) {
        this.useOldVersionLogin = useOldVersionLogin;
    }
}
