package com.fr.design.login.executor;

import com.fr.design.bridge.exec.JSExecutor;
import com.teamdev.jxbrowser.chromium.JSFunction;
import com.teamdev.jxbrowser.chromium.JSObject;

/**
 * @author richie
 * @version 10.0
 * Created by richie on 2019-04-18
 */
public class DesignerLoginBrowserExecutor implements JSExecutor {

    public static DesignerLoginBrowserExecutor create(JSObject window, JSFunction callback) {
        return new DesignerLoginBrowserExecutor(window, callback);
    }

    private JSObject window;
    private JSFunction callback;

    private DesignerLoginBrowserExecutor(JSObject window, JSFunction callback) {
        this.window = window;
        this.callback = callback;
    }

    @Override
    public void executor(String newValue) {
        callback.invoke(window, newValue);
    }
}
