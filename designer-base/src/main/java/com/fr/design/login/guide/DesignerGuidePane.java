package com.fr.design.login.guide;

import com.fr.design.dialog.BasicPane;
import com.fr.design.login.guide.utils.DesignerGuideUtils;
import com.fr.design.ui.ModernUIPane;
import com.teamdev.jxbrowser.chromium.JSValue;
import com.teamdev.jxbrowser.chromium.events.ScriptContextAdapter;
import com.teamdev.jxbrowser.chromium.events.ScriptContextEvent;
import java.awt.BorderLayout;

/**
 * @author Lanlan
 * @version 10.0
 * Created by Lanlan on 2021/5/21
 */
public class DesignerGuidePane extends BasicPane {

    @Override
    protected String title4PopupWindow() {
        return "DESIGNER_GUIDE";
    }

    public DesignerGuidePane() {
        setLayout(new BorderLayout());
        ModernUIPane<Object> modernUIPane = new ModernUIPane.Builder<>()
                .prepare(new ScriptContextAdapter() {
                    @Override
                    public void onScriptContextCreated(ScriptContextEvent event) {
                        JSValue window = event.getBrowser().executeJavaScriptAndReturnValue("window");
                        window.asObject().setProperty("DesignerGuideHelper", DesignerGuideBridge.getBridge(event.getBrowser()));
                    }
                })
                .withEMB(DesignerGuideHelper.getMainResourcePath(), DesignerGuideUtils.renderMap())
                .build();
        add(modernUIPane, BorderLayout.CENTER);
    }
}
