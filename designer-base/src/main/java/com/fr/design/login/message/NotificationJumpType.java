package com.fr.design.login.message;

/**
 * @author Lanlan
 * @version 10.0
 * Created by Lanlan on 2021/6/11
 */
public enum NotificationJumpType {
    WEB_URL(1),
    DESIGNER_MODULE(2),
    UNKNOWN(-1);

    private int jumpType;

    NotificationJumpType(int jumpType) {
        this.jumpType = jumpType;
    }

    public int getJumpType() {
        return jumpType;
    }

    public static NotificationJumpType valueOf(int jumpType) {
        for(NotificationJumpType value : NotificationJumpType.values()) {
            if(value.getJumpType() == jumpType) {
                return value;
            }
        }
        return UNKNOWN;
    }
}
