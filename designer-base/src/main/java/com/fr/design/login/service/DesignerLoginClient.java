package com.fr.design.login.service;

import com.fr.general.CloudCenter;
import com.fr.general.http.HttpToolbox;
import com.fr.general.log.MessageFormatter;
import com.fr.json.JSON;
import com.fr.json.JSONFactory;
import com.fr.json.JSONObject;
import com.fr.log.FineLoggerFactory;
import com.fr.stable.StringUtils;
import com.fr.third.org.apache.commons.lang3.RandomStringUtils;
import java.util.HashMap;
import java.util.UUID;

/**
 * @author Lanlan
 * @version 10.0
 * Created by Lanlan on 2021/5/28
 */
public class DesignerLoginClient {

    private static final String LOGIN_API = CloudCenter.getInstance().acquireUrlByKind("designer.login.api", "http://api.shequ.fanruan.com/v1/user/login/");
    private static final String SEND_CAPTCHA_API = CloudCenter.getInstance().acquireUrlByKind("designer.send.captcha.api", "http://api.shequ.fanruan.com/v1/code/getsmscaptcha/?location={}&phone={}&smstype={}");
    private static final String SMS_LOGIN_API = CloudCenter.getInstance().acquireUrlByKind("designer.sms.login.api", "http://api.shequ.fanruan.com/v1/user/smslogin/");
    private static final String SMS_REGISTER_API = CloudCenter.getInstance().acquireUrlByKind("designer.sms.register.api", "http://api.shequ.fanruan.com/v1/user/register/");
    private static final String USERNAME = "username";
    private static final String PASSWORD = "password";
    private static final String SMS_TYPE_LOGIN_AND_REGISTER = "1";
    private static final String LOCATION = "location";
    private static final String PHONE = "phone";
    private static final String CODE = "code";
    private static final String STATUS = "status";
    private static final String DATA = "data";
    private static final String CLIENT = "client";
    private static final String UID = "uid";
    private static final String APP_ID = "appid";
    private static final String REFRESH_TOKEN = "refresh_token";
    private static final String REGISTER = "register";
    private static final String REG_TOKEN = "regtoken";
    private static final String REG_PHONE = "regphone";
    private static final String DEVICE = "device";
    private static final String REG_FROM = "reg_from";
    private static final String PRODUCT_FINEREPORT = "product-finereport";

    /**
     * 服务器内部错误
     */
    private static final int INTERNAL_ERROR = 0;

    /**
     * 未知错误
     */
    private static final int UNKNOWN_ERROR = -3;

    /**
     * 网络连接失败
     */
    private static final int NETWORK_CONNECTED_FAILED = -4;

    public DesignerLoginResult login(String username, String password) {
        try {
            HashMap<String, Object> params = new HashMap<>();
            params.put(USERNAME, username);
            params.put(PASSWORD, password);
            String result = HttpToolbox.post(LOGIN_API, params);
            JSONObject response = JSONFactory.createJSON(JSON.OBJECT, result);
            int status = response.optInt(STATUS);
            if (status < 0) {
                return DesignerLoginResult.create(status, StringUtils.EMPTY, StringUtils.EMPTY, StringUtils.EMPTY, false, StringUtils.EMPTY);
            }
            JSONObject data = response.optJSONObject(DATA);
            if (data != null) {
                JSONObject client = data.optJSONObject(CLIENT);
                if (client != null) {
                    int uid = client.optInt(UID);
                    if (uid > 0) {
                        return DesignerLoginResult.create(uid, client.optString(USERNAME), client.optString(APP_ID), data.optString(REFRESH_TOKEN), false, StringUtils.EMPTY);
                    }
                }
            }
        } catch (Exception e) {
            FineLoggerFactory.getLogger().error(e.getMessage(), e);
            return DesignerLoginResult.create(NETWORK_CONNECTED_FAILED, StringUtils.EMPTY, StringUtils.EMPTY, StringUtils.EMPTY, false, StringUtils.EMPTY);
        }
        return DesignerLoginResult.create(UNKNOWN_ERROR, StringUtils.EMPTY, StringUtils.EMPTY, StringUtils.EMPTY, false, StringUtils.EMPTY);
    }

    public int sendCaptcha(String regionCode, String phone) {
        try {
            String url = MessageFormatter.arrayFormat(SEND_CAPTCHA_API, new String[]{regionCode, phone, SMS_TYPE_LOGIN_AND_REGISTER}).getMessage();
            String result = HttpToolbox.get(url);
            JSONObject response = JSONFactory.createJSON(JSON.OBJECT, result);
            return response.optInt(STATUS);
        } catch (Exception e) {
            FineLoggerFactory.getLogger().error(e.getMessage(), e);
        }
        return INTERNAL_ERROR;
    }

    public DesignerLoginResult smsLogin(String regionCode, String phone, String code) {
        try {
            HashMap<String, Object> params = new HashMap<>();
            params.put(LOCATION, regionCode);
            params.put(PHONE, phone);
            params.put(CODE, code);
            String result = HttpToolbox.post(SMS_LOGIN_API, params);
            JSONObject response = JSONFactory.createJSON(JSON.OBJECT, result);
            int status = response.optInt(STATUS);
            if (status < 0) {
                return DesignerLoginResult.create(status, StringUtils.EMPTY, StringUtils.EMPTY, StringUtils.EMPTY, false, StringUtils.EMPTY);
            }
            JSONObject data = response.optJSONObject(DATA);
            if (data != null) {
                boolean register = data.optBoolean(REGISTER);
                if (register) {
                    String regToken = data.optString(REG_TOKEN);
                    if (regToken != null) {
                        return DesignerLoginResult.create(status, StringUtils.EMPTY, StringUtils.EMPTY, StringUtils.EMPTY, true, regToken);
                    }
                } else {
                    JSONObject client = data.optJSONObject(CLIENT);
                    if (client != null) {
                        int uid = client.optInt(UID);
                        if (uid > 0) {
                            return DesignerLoginResult.create(uid, client.optString(USERNAME), client.optString(APP_ID), data.optString(REFRESH_TOKEN), false, StringUtils.EMPTY);
                        }
                    }
                }
            }
        } catch (Exception e) {
            FineLoggerFactory.getLogger().error(e.getMessage(), e);
        }
        return DesignerLoginResult.create(INTERNAL_ERROR, StringUtils.EMPTY, StringUtils.EMPTY, StringUtils.EMPTY, false, StringUtils.EMPTY);
    }

    public DesignerLoginResult smsRegister(String regionCode, String phone, String password, String regToken) {
        try {
            HashMap<String, Object> params = new HashMap<>();
            params.put(USERNAME, RandomStringUtils.randomAlphabetic(8));
            params.put(PASSWORD, password);
            params.put(REG_TOKEN, regToken);
            params.put(LOCATION, regionCode);
            params.put(REG_PHONE, phone);
            params.put(DEVICE, PRODUCT_FINEREPORT);
            params.put(REG_FROM, PRODUCT_FINEREPORT);
            String result = HttpToolbox.post(SMS_REGISTER_API, params);
            JSONObject response = JSONFactory.createJSON(JSON.OBJECT, result);
            int status = response.optInt(STATUS);
            if (status < 0) {
                return DesignerLoginResult.create(status, StringUtils.EMPTY, StringUtils.EMPTY, StringUtils.EMPTY, false, StringUtils.EMPTY);
            }
            JSONObject data = response.optJSONObject(DATA);
            if (data != null) {
                JSONObject client = data.optJSONObject(CLIENT);
                if (client != null) {
                    int uid = client.optInt(UID);
                    if (uid > 0) {
                        return DesignerLoginResult.create(uid, client.optString(USERNAME), client.optString(APP_ID), data.optString(REFRESH_TOKEN), false, StringUtils.EMPTY);
                    }
                }
            }
        } catch (Exception e) {
            FineLoggerFactory.getLogger().error(e.getMessage(), e);
        }
        return DesignerLoginResult.create(INTERNAL_ERROR, StringUtils.EMPTY, StringUtils.EMPTY, StringUtils.EMPTY, false, StringUtils.EMPTY);
    }
}
