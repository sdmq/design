package com.fr.design.login.service;

/**
 * @author Lanlan
 * @version 10.0
 * Created by Lanlan on 2021/5/28
 */
public class DesignerLoginResult {

    private int uid;

    private String username;

    private String appId;

    private String refreshToken;

    private boolean register;

    private String regToken;

    private DesignerLoginResult(int uid, String username, String appId, String refreshToken, boolean register, String regToken) {
        this.uid = uid;
        this.username = username;
        this.appId = appId;
        this.refreshToken = refreshToken;
        this.regToken = regToken;
        this.register = register;
    }

    public static DesignerLoginResult create(int uid, String username, String appId, String refreshToken, boolean register, String regToken) {
        return new DesignerLoginResult(uid, username, appId, refreshToken, register, regToken);
    }

    public int getUid() {
        return uid;
    }

    public void setUid(int uid) {
        this.uid = uid;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getAppId() {
        return appId;
    }

    public void setAppId(String appId) {
        this.appId = appId;
    }

    public String getRefreshToken() {
        return refreshToken;
    }

    public void setRefreshToken(String refreshToken) {
        this.refreshToken = refreshToken;
    }

    public boolean isRegister() {
        return register;
    }

    public void setRegister(boolean register) {
        this.register = register;
    }

    public String getRegToken() {
        return regToken;
    }

    public void setRegToken(String regToken) {
        this.regToken = regToken;
    }
}
