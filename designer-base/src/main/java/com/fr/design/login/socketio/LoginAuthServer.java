package com.fr.design.login.socketio;

import com.fr.design.DesignerEnvManager;
import com.fr.design.login.DesignerLoginType;
import com.fr.design.login.bean.BBSAccountLogin;
import com.fr.design.upm.event.CertificateEvent;
import com.fr.event.EventDispatcher;
import com.fr.log.FineLoggerFactory;
import com.fr.third.socketio.AckRequest;
import com.fr.third.socketio.Configuration;
import com.fr.third.socketio.SocketIOClient;
import com.fr.third.socketio.SocketIOServer;
import com.fr.third.socketio.listener.DataListener;
import java.net.URLDecoder;

/**
 * @author Lanlan
 * @version 10.0
 * Created by Lanlan on 2021/6/3
 */
public class LoginAuthServer {

    private SocketIOServer server;

    private static final String HOSTNAME = "localhost";
    private static final int PORT = 41925;

    private static volatile LoginAuthServer instance = null;

    public static LoginAuthServer getInstance() {
        if (instance == null) {
            synchronized (LoginAuthServer.class) {
                if (instance == null) {
                    instance = new LoginAuthServer();
                }
            }
        }
        return instance;
    }

    private LoginAuthServer() {
        Configuration config = new Configuration();
        config.setHostname(HOSTNAME);
        config.setPort(PORT);
        server = new SocketIOServer(config);
        initEventListener();
    }

    public void start() {
        try {
            server.start();
        } catch (Exception e) {
            FineLoggerFactory.getLogger().error(e.getMessage(), e);
        }
    }

    public void stop() {
        try {
            server.stop();
        } catch (Exception e) {
            FineLoggerFactory.getLogger().error(e.getMessage(), e);
        }
    }

    private void initEventListener() {
        server.addEventListener("bbsAccountLogin", BBSAccountLogin.class, new DataListener<BBSAccountLogin>() {
            @Override
            public void onData(SocketIOClient client, BBSAccountLogin data, AckRequest ackRequest) throws Exception {
                // 保存登录信息到.FineReport100配置中
                int uid = data.getUid();
                if (uid > 0) {
                    String username = URLDecoder.decode(data.getUsername(), "UTF-8");
                    DesignerEnvManager manager = DesignerEnvManager.getEnvManager();
                    manager.setDesignerLoginUid(data.getUid());
                    manager.setDesignerLoginUsername(username);
                    manager.setDesignerLoginAppId(data.getAppId());
                    manager.setDesignerLoginRefreshToken(data.getRefreshToken());
                    manager.setDesignerLastLoginTime(System.currentTimeMillis());
                    manager.setLastLoginType(DesignerLoginType.NORMAL_LOGIN);
                    manager.setLastLoginAccount(username);
                    DesignerEnvManager.getEnvManager().saveXMLFile();
                    EventDispatcher.fire(CertificateEvent.LOGIN, username);
                }
            }
        });
    }
}
