package com.fr.design.login.utils;

import com.fr.design.DesignerEnvManager;
import com.fr.design.login.config.DefaultLoginKeys;
import com.fr.design.mainframe.toast.DesignerToastMsgUtil;
import com.fr.general.CloudCenter;
import com.fr.general.CloudCenterConfig;
import com.fr.general.GeneralContext;
import com.fr.general.http.HttpToolbox;
import com.fr.general.log.MessageFormatter;
import com.fr.json.JSONObject;
import com.fr.log.FineLoggerFactory;
import com.fr.stable.StringUtils;
import com.fr.third.org.bouncycastle.util.encoders.Hex;
import java.awt.Window;
import java.security.SecureRandom;
import java.util.HashMap;
import java.util.Map;
import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

/**
 * @author Lanlan
 * @version 10.0
 * Created by Lanlan on 2021/5/21
 */
public class DesignerLoginUtils {

    private static final String PRODUCT_FINEREPORT = "product-finereport";

    public static Map<String, String> renderMap() {
        Map<String, String> map4Tpl = new HashMap<>();
        map4Tpl.put("language", GeneralContext.getLocale().toString());
        return map4Tpl;
    }

    public static void showPluginRemindOnFirstLaunch(Window parent) {
        DesignerEnvManager manager = DesignerEnvManager.getEnvManager();
        int uid = manager.getDesignerLoginUid();
        if (uid > 0) {
            boolean pluginRemindOnFirstLaunch = manager.isPluginRemindOnFirstLaunch();
            if (pluginRemindOnFirstLaunch) {
                DesignerToastMsgUtil.toastWarning(
                        com.fr.design.i18n.Toolkit.i18nText("Fine-Designer_Plugin_Tip"),
                        parent
                );
                manager.setPluginRemindOnFirstLaunch(false);
            }
        }
    }

    public static String generateDesignerSSOUrl(String referrer) {
        if (!DesignerLoginUtils.isOnline()) {
            return referrer;
        }
        String ssoTemplate = CloudCenter.getInstance().acquireUrlByKind("designer.sso.api", "https://id.fanruan.com/api/app/?code={}&referrer={}");
        try {
            String code = generateLoginCode();
            MessageFormatter.FormattingTuple tuple = MessageFormatter.arrayFormat(ssoTemplate, new String[]{code, referrer});
            return tuple.getMessage();
        } catch (Exception e) {
            FineLoggerFactory.getLogger().error(e.getMessage(), e);
        }
        return referrer;
    }

    public static boolean isOnline() {
        if (CloudCenterConfig.getInstance().isOnline()) {
            String ping = CloudCenter.getInstance().acquireConf("ping", StringUtils.EMPTY);
            if (StringUtils.isNotEmpty(ping)) {
                try {
                    return StringUtils.isEmpty(HttpToolbox.get(ping));
                } catch (Exception ignore) {
                }
            }
        }
        return false;
    }

    private static String generateLoginCode() throws Exception {
        DesignerEnvManager manager = DesignerEnvManager.getEnvManager();
        JSONObject jo = JSONObject.create();
        jo.put("uid", manager.getDesignerLoginUid());
        jo.put("username", manager.getDesignerLoginUsername());
        jo.put("source", PRODUCT_FINEREPORT);
        byte[] iv = randomIv();
        return new String(Hex.encode(iv)) + encrypt(
                jo.toString(),
                DefaultLoginKeys.getInstance().getKey("Fine-Designer_Login").getBytes(),
                iv
        );
    }

    private static byte[] randomIv() {
        byte[] salt = new byte[16];
        SecureRandom secureRandom = new SecureRandom();
        secureRandom.setSeed(System.currentTimeMillis());
        secureRandom.nextBytes(salt);
        return salt;
    }

    private static String encrypt(String content, byte[] key, byte[] iv) throws Exception {
        SecretKeySpec secretKeySpec = new SecretKeySpec(key, "AES");
        Cipher cipher = Cipher.getInstance("AES/CTR/PKCS5Padding");
        IvParameterSpec ivParameterSpec = new IvParameterSpec(iv);
        cipher.init(Cipher.ENCRYPT_MODE, secretKeySpec, ivParameterSpec);
        byte[] resultBytes = cipher.doFinal(content.getBytes());
        return new String(Hex.encode(resultBytes));
    }
}
