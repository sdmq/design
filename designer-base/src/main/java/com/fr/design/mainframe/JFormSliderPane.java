package com.fr.design.mainframe;

import com.fr.base.BaseUtils;
import com.fr.design.gui.ibutton.UIButton;
import com.fr.design.gui.ilable.UILabel;
import com.fr.design.gui.islider.UISlider;
import com.fr.design.gui.itextfield.UINumberField;
import com.fr.design.utils.gui.GUICoreUtils;

import javax.swing.BorderFactory;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.plaf.basic.BasicSliderUI;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.FlowLayout;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.math.BigDecimal;

/**
 * Created by MoMeak on 2017/7/13.
 */
public class JFormSliderPane extends JPanel {
    public static final Image APPFIT_V0 = BaseUtils.readImage("com/fr/design/images/control/icon_thumb_normal.png");

    private static final double ONEPOINTEIGHT = 1.8;
    private static final int SIX = 6;
    private static final int TEN = 10;
    private static final int HALF_HUNDRED = 50;
    private static final int HUNDRED = 100;
    private static final int TWO_HUNDRED = 200;
    private static final int FOUR_HUNDRED = 400;
    private static final int SHOWVALBUTTON_WIDTH = 35;
    private static final int SHOWVALBUTTON_HEIGHTH = 20;
    private static final String SUFFIX = "%";
    private static final int TOOLTIP_Y = 30;
    private static final Color BACK_COLOR = new Color(245, 245, 247);
    public int showValue = 100;
    private UINumberField showValField;
    private UISlider slider;
    private int times;
    private int sliderValue;
    private UIButton downButton;
    private UIButton upButton;
    //拖动条处理和button、直接输入不一样
    private boolean isButtonOrIsTxt = true;


    public JFormSliderPane() {
        this.setLayout(new BorderLayout());
        initSlider();
        initDownUpButton();
        initShowValField();
        JPanel panel = new JPanel(new FlowLayout(FlowLayout.CENTER, 0, 0));
        panel.add(downButton);
        panel.add(slider);
        panel.add(upButton);
        panel.add(showValField);
        UILabel uiLabel = new UILabel(SUFFIX);
        uiLabel.setBorder(BorderFactory.createEmptyBorder(0, 4, 0, 4));
        panel.add(uiLabel);
        panel.setBackground(BACK_COLOR);
        this.add(panel, BorderLayout.NORTH);
    }

    public static JFormSliderPane getInstance() {
        return new JFormSliderPane();
    }

    private void initSlider() {
        slider = new UISlider(0, HUNDRED, HALF_HUNDRED) {
            public Point getToolTipLocation(MouseEvent event) {
                return new Point(event.getX(), event.getY() - TOOLTIP_Y);
            }
        };
        slider.setValue(HALF_HUNDRED);
        slider.setUI(new JSliderPaneUI(slider));
        slider.addChangeListener(listener);
        slider.setPreferredSize(new Dimension(220, 20));
        //去掉虚线框
        slider.setFocusable(false);
        slider.setToolTipText(com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Basic_Scale_Slider"));
    }


    private void initDownUpButton() {
        downButton = new UIButton(BaseUtils.readIcon("com/fr/design/images/data/source/normalDown20.png"), BaseUtils.readIcon("com/fr/design/images/data/source/hoverDown20.png"), BaseUtils.readIcon("com/fr/design/images/data/source/hoverDown20.png")) {
            public Point getToolTipLocation(MouseEvent event) {
                return new Point(event.getX(), event.getY() - TOOLTIP_Y);
            }
        };
        downButton.setOpaque(false);
        downButton.setBorderPainted(false);
        downButton.setToolTipText(com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Basic_Scale_Down"));
        upButton = new UIButton(BaseUtils.readIcon("com/fr/design/images/data/source/normalUp20.png"), BaseUtils.readIcon("com/fr/design/images/data/source/hoverUp20.png"), BaseUtils.readIcon("com/fr/design/images/data/source/hoverUp20.png")) {
            public Point getToolTipLocation(MouseEvent event) {
                return new Point(event.getX(), event.getY() - TOOLTIP_Y);
            }
        };
        upButton.setOpaque(false);
        upButton.setBorderPainted(false);
        upButton.setToolTipText(com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Basic_Scale_Up"));
        downButton.setActionCommand("less");
        upButton.setActionCommand("more");
        downButton.addActionListener(buttonActionListener);
        upButton.addActionListener(buttonActionListener);
    }

    private void initShowValField() {
        showValField = new UINumberField();
        showValField.setValue(showValue);
        showValField.setPreferredSize(new Dimension(SHOWVALBUTTON_WIDTH, SHOWVALBUTTON_HEIGHTH));
        showValField.addKeyListener(new KeyListener() {
            @Override
            public void keyTyped(KeyEvent e) {

            }

            @Override
            public void keyPressed(KeyEvent e) {

            }

            @Override
            public void keyReleased(KeyEvent evt) {
                int code = evt.getKeyCode();

                if (code == KeyEvent.VK_ENTER) {
                    showValFieldChange((int) showValField.getValue());
                }
            }
        });
        showValField.addFocusListener(new FocusListener() {
            @Override
            public void focusGained(FocusEvent e) {

            }

            @Override
            public void focusLost(FocusEvent e) {
                showValFieldChange((int) showValField.getValue());
            }
        });
    }



    private void showValFieldChange(int value) {
        isButtonOrIsTxt = true;
        showValue = getPreferredValue(value);
        refreshShowValueFieldText();
        refreshSlider();
    }

    private int getPreferredValue(int value){
        if (value > FOUR_HUNDRED) {
            value = FOUR_HUNDRED;
        }
        if (value < TEN) {
            value = TEN;
        }
        return value;
    }

    private void refreshShowValueFieldText(){
        showValField.setValue(showValue);
        setAdjustButtonStatus();
    }

    //定义一个监听器，用于监听所有滑动条
    private ChangeListener listener = new ChangeListener() {
        public void stateChanged(ChangeEvent event) {
            //取出滑动条的值，并在文本中显示出来
            if (!isButtonOrIsTxt) {
                EventQueue.invokeLater(new Runnable() {
                    public void run() {
                        sliderValue = slider.getValue();
                        getTimes(sliderValue);
                        showValue = times;
                        refreshShowValueFieldText();
                    }
                });
            } else {
                isButtonOrIsTxt = false;
            }
        }
    };


    private void refreshSlider() {
        slider.setValue(calSliderValue(showValue));
    }

    private void setAdjustButtonStatus(){
        this.downButton.setEnabled(this.showValue > TEN);
        this.upButton.setEnabled(this.showValue < FOUR_HUNDRED);
    }

    private int calSliderValue(int value) {
        int result;
        if (value > HUNDRED) {
            result = (value + TWO_HUNDRED) / SIX;
        } else if (value < HUNDRED) {
            result = (int) ((value - TEN) / ONEPOINTEIGHT);
        } else {
            result = HALF_HUNDRED;
        }
        return result;
    }


    public static double divide(double v1, double v2, int scale) {
        BigDecimal b1 = new BigDecimal(Double.toString(v1));
        BigDecimal b2 = new BigDecimal(Double.toString(v2));
        return b1.divide(b2, scale).doubleValue();
    }

    private ActionListener buttonActionListener = new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
            isButtonOrIsTxt = true;
            if ("less".equals(e.getActionCommand())) {
                int newDownVal = showValue - TEN;
                if (newDownVal >= TEN) {
                    showValue = newDownVal;
                } else {
                    showValue = TEN;
                }
                refreshShowValueFieldText();
                refreshSlider();
            }
            if ("more".equals(e.getActionCommand())) {
                int newUpVal = showValue + TEN;
                if (newUpVal <= FOUR_HUNDRED) {
                    showValue = newUpVal;
                } else {
                    showValue = FOUR_HUNDRED;
                }
                refreshShowValueFieldText();
                refreshSlider();
            }
            isButtonOrIsTxt = true;
        }
    };


    private void getTimes(int value) {
        if (value == HALF_HUNDRED) {
            times = HUNDRED;
        } else if (value < HALF_HUNDRED) {
            times = (int) Math.round(ONEPOINTEIGHT * value + TEN);
        } else {
            times = SIX * value - TWO_HUNDRED;
        }
    }


    public int getShowValue() {
        return this.showValue;
    }

    public void setShowValue(int value) {
        showValFieldChange(value);
    }


    public void addValueChangeListener(ChangeListener changeListener){
        this.slider.addChangeListener(changeListener);
    }

    class JSliderPaneUI extends BasicSliderUI {

        private static final int THUMB_XOFFSET = 8;
        private static final int THUMB_YOFFSET = 3;
        private static final int FOUR = 4;
        private static final int FIVE = 5;
        private static final int SIX = 6;
        private static final int MID_X_SHIFT = 2;  // 中点标记的水平位置偏移

        public JSliderPaneUI(UISlider b) {
            super(b);
        }

        /**
         * 绘制指示物
         */
        public void paintThumb(Graphics g) {
            Rectangle knobBounds = thumbRect;
            Graphics2D g2d = (Graphics2D) g;
            g2d.drawImage(APPFIT_V0, knobBounds.x - THUMB_XOFFSET, knobBounds.y + THUMB_YOFFSET, null);
            g2d.dispose();
        }

        /**
         * 绘制刻度轨迹
         */
        public void paintTrack(Graphics g) {
            int cy, cw;
            Rectangle trackBounds = trackRect;
            if (slider.getOrientation() == UISlider.HORIZONTAL) {
                Graphics2D g2 = (Graphics2D) g;
                cy = (trackBounds.height / 2);
                cw = trackBounds.width;
                g2.setPaint(BACK_COLOR);
                g2.fillRect(0, -cy, cw + 10, cy * 4);
                g.setColor(new Color(216, 216, 216));
                g.drawLine(0, cy, cw + 3, cy);
                g.drawLine(MID_X_SHIFT + cw / 2, cy - FOUR, MID_X_SHIFT + cw / 2, cy + FOUR);
            } else {
                super.paintTrack(g);
            }
        }

        public void setThumbLocation(int x, int y) {
            super.setThumbLocation(x, y);
            slider.repaint();
        }

    }

    public static void main(String[] args) {
        JFrame jf = new JFrame("test");
        jf.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        JPanel content = (JPanel) jf.getContentPane();
        content.setLayout(new BorderLayout());
        content.add(JFormSliderPane.getInstance(), BorderLayout.CENTER);
        GUICoreUtils.centerWindow(jf);
        jf.setSize(400, 80);
        jf.setVisible(true);

    }
}

