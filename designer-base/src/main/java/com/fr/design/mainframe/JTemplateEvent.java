package com.fr.design.mainframe;

import com.fr.event.Event;

/**
 * Created by kerry on 2020-12-11
 */
public enum JTemplateEvent implements Event<JTemplate> {
    /**
     * 模板初始化之前
     */
    BEFORE_TEMPLATE_INIT,

    /**
     * 模板激活之前
     */
    BEFORE_TEMPLATE_ACTIVE

}
