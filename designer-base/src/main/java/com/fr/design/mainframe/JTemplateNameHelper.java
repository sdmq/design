package com.fr.design.mainframe;

import com.fr.design.file.TemplateTreePane;
import com.fr.design.gui.itree.filetree.TemplateFileTree;
import com.fr.stable.StringUtils;
import org.jetbrains.annotations.Nullable;

import javax.swing.tree.DefaultMutableTreeNode;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * @author shine
 * @version 10.0
 * Created by shine on 2021/5/7
 */
public class JTemplateNameHelper {

    private static final int PREFIX_NUM = 3000;

    private static short currentIndex = 0;// 此变量用于多次新建模板时，让名字不重复

    public static String newTemplateNameByIndex(String prefix) {
        // 用于获取左侧模板的文件名，如左侧已包含"WorkBook1.cpt, WorkBook12.cpt, WorkBook177.cpt"
        // 那么新建的文件名将被命名为"WorkBook178.cpt",即取最大数+1
        TemplateFileTree tt = TemplateTreePane.getInstance().getTemplateFileTree();
        DefaultMutableTreeNode gen = (DefaultMutableTreeNode) tt.getModel().getRoot();
        String[] str = new String[gen.getChildCount()];

        List<BigInteger> reportNum = new ArrayList<>();
        for (int j = 0; j < gen.getChildCount(); j++) {
            str[j] = gen.getChildAt(j).toString();
            //返回文件名中的index(算法中没有再匹配文件后缀了，因为DefaultMutableTreeNode中已经匹配过了)
            BigInteger index = getFileNameIndex(prefix, str[j]);
            if (index != null) {
                reportNum.add(index);
            }
        }
        Collections.sort(reportNum);
        BigInteger idx = reportNum.size() > 0 ? reportNum.get(reportNum.size() - 1).add(BigInteger.valueOf(1)) : BigInteger.valueOf(1);
        idx = idx.add(BigInteger.valueOf(currentIndex));
        currentIndex++;
        return prefix + idx;
    }

    /**
     * @return java.lang.Integer WorkBook11.cpt则返回11，如果没有找到index返回null
     * @Description 返回文件名中的index
     * @param: prefix 前缀
     * @param: fileName 文件名称全名
     * @Author Henry.Wang
     * @Date 2021/4/9 11:13
     **/
    @Nullable
    private static BigInteger getFileNameIndex(String prefix, String fileName) {
        //如果文件名长度小于等于前缀长度或者匹配前缀失败，直接返回就可以了
        if ((prefix.length() >= fileName.length()) || (!StringUtils.equals(prefix, fileName.substring(0, prefix.length())))) {
            return null;
        }
        BigInteger integer = null;
        integer = matchFileNameIndex(prefix, fileName);
        return integer;
    }

    /**
     * 匹配文件名称的数字后缀Index
     * @param prefix 前缀
     * @param fileName 文件名称全名
     * @return 返回对应的数字后缀Index
     */
    @Nullable
    private static BigInteger matchFileNameIndex(String prefix, String fileName) {
        StringBuilder result = new StringBuilder();
        for (int i = prefix.length(); i < fileName.length(); i++) {
            char c = fileName.charAt(i);
            if (isDot(c)) {
                break;
            } else {
                if (isNotNumber(c)) {
                    return null;
                }
                result.append(c);
            }
        }
        if (StringUtils.isBlank(result.toString())) {
            return null;
        }
        return new BigInteger(result.toString(), 10);
    }

    /**
     * 是否不属于数字0-9
     * @param c 用于判断的char
     * @return 返回对应判断结果
     */
    private static boolean isNotNumber(char c) {
        return c < 48 || c > 57;
    }

    /**
     * 是否属于'.'
     * @param c 用于判断的char
     * @return 返回对应判断结果
     */
    private static boolean isDot(char c) {
        return c == '.';
    }
}
