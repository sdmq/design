package com.fr.design.mainframe;

import com.fr.design.dialog.link.MessageWithLink;
import com.fr.design.file.HistoryTemplateListCache;
import com.fr.design.gui.ilable.UILabel;
import com.fr.design.i18n.Toolkit;
import com.fr.general.IOUtils;
import com.fr.stable.StringUtils;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.LayoutManager;
import javax.swing.JPanel;
import javax.swing.event.HyperlinkEvent;
import javax.swing.event.HyperlinkListener;

/**
 * @author hades
 * @version 10.0
 * Created by hades on 2021/4/20
 */
public class OpenFailedPane extends JPanel {

    private  UILabel label;
    private MessageWithLink link;
    private String defaultFailedText;

    public OpenFailedPane() {
        this.setLayout(new LayoutManager() {
            @Override
            public void addLayoutComponent(String name, Component comp) {

            }

            @Override
            public void removeLayoutComponent(Component comp) {

            }

            @Override
            public Dimension preferredLayoutSize(Container parent) {
                return parent.getPreferredSize();
            }

            @Override
            public Dimension minimumLayoutSize(Container parent) {
                return null;
            }

            @Override
            public void layoutContainer(Container parent) {
                int width = parent.getParent().getWidth();
                int height = parent.getParent().getHeight();
                int labelWidth = label.getPreferredSize().width;
                int labelHeight = label.getPreferredSize().height;
                int labelX = (width - labelWidth) / 2;
                int labelY = (height - labelHeight) / 2;
                int linkWidth = link.getPreferredSize().width;
                int linkHeight = link.getPreferredSize().height;
                int linkX = (width - linkWidth) / 2;
                int linkY = (height - labelHeight) / 2 + labelHeight;
                label.setBounds(labelX, labelY, labelWidth, labelHeight);
                link.setBounds(linkX, linkY, linkWidth, linkHeight);
            }
        });
        this.setBackground(Color.WHITE);
        label = new UILabel(IOUtils.readIcon("/com/fr/design/images/mainframe/open_failed.png"), UILabel.CENTER);
        link = new MessageWithLink(Toolkit.i18nText("Fine-Design_Open_Failed_Tip"), Toolkit.i18nText("Fine-Design_Open_Failed_Retry"), StringUtils.EMPTY, Color.WHITE) {
            @Override
            protected void initListener(String link) {
                addHyperlinkListener(new HyperlinkListener() {
                    @Override
                    public void hyperlinkUpdate(HyperlinkEvent e) {
                        if (e.getEventType().equals(HyperlinkEvent.EventType.ACTIVATED)) {
                            // 重试
                            JTemplate<?, ?> template = HistoryTemplateListCache.getInstance().getCurrentEditingTemplate();
                            template.whenClose();
                            template = JTemplateFactory.createJTemplate(template.getEditingFILE());
                            HistoryTemplateListCache.getInstance().replaceCurrentEditingTemplate(template);
                            DesignerContext.getDesignerFrame().addAndActivateJTemplate(template);
                        }
                    }
                });
            }
        };
        this.defaultFailedText = link.getText();
        link.setBackground(Color.WHITE);
        this.add(label);
        this.add(link);
    }

    public void setFailedTip(String text) {
        if (StringUtils.isEmpty(text)) {
            this.link.setText(defaultFailedText);
        } else {
            this.link.setText(text);
        }
    }

}
