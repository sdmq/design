package com.fr.design.mainframe;

import com.fr.log.FineLoggerFactory;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

/**
 *  保证监听运行出错也不影响其他功能正常使用
 *
 * @author hades
 * @version 10.0
 * Created by hades on 2021/6/30
 */
public class SafeWindowListener implements WindowListener {

    private final WindowListener windowListener;

    public SafeWindowListener(WindowListener windowListener) {
        this.windowListener = windowListener;
    }

    @Override
    public void windowOpened(WindowEvent e) {
        try {
            windowListener.windowOpened(e);
        } catch (Throwable throwable) {
            FineLoggerFactory.getLogger().debug(throwable.getMessage(), throwable);
        }
    }

    @Override
    public void windowClosing(WindowEvent e) {
        try {
            windowListener.windowClosing(e);
        } catch (Throwable throwable) {
            FineLoggerFactory.getLogger().debug(throwable.getMessage(), throwable);
        }
    }

    @Override
    public void windowClosed(WindowEvent e) {
        try {
            windowListener.windowClosed(e);
        } catch (Throwable throwable) {
            FineLoggerFactory.getLogger().debug(throwable.getMessage(), throwable);
        }
    }

    @Override
    public void windowIconified(WindowEvent e) {
        try {
            windowListener.windowIconified(e);
        } catch (Throwable throwable) {
            FineLoggerFactory.getLogger().debug(throwable.getMessage(), throwable);
        }
    }

    @Override
    public void windowDeiconified(WindowEvent e) {
        try {
            windowListener.windowDeiconified(e);
        } catch (Throwable throwable) {
            FineLoggerFactory.getLogger().debug(throwable.getMessage(), throwable);
        }
    }

    @Override
    public void windowActivated(WindowEvent e) {
        try {
            windowListener.windowActivated(e);
        } catch (Throwable throwable) {
            FineLoggerFactory.getLogger().debug(throwable.getMessage(), throwable);
        }
    }

    @Override
    public void windowDeactivated(WindowEvent e) {
        try {
            windowListener.windowDeactivated(e);
        }  catch (Throwable throwable) {
            FineLoggerFactory.getLogger().debug(throwable.getMessage(), throwable);
        }
    }
}