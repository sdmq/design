package com.fr.design.mainframe.check;

import com.fr.base.BaseUtils;
import com.fr.design.dialog.FineJOptionPane;
import com.fr.design.gui.ibutton.UIButton;
import com.fr.design.gui.ilable.UILabel;
import com.fr.design.i18n.DesignSizeI18nManager;
import com.fr.design.i18n.Toolkit;
import com.fr.design.mainframe.DesignerContext;
import com.fr.design.mainframe.JTemplate;
import com.fr.design.worker.save.CallbackSaveWorker;
import com.fr.file.FILE;
import com.fr.file.FileNodeFILE;
import com.fr.general.IOUtils;
import com.fr.log.FineLoggerFactory;
import com.fr.rpc.ExceptionHandler;
import com.fr.rpc.RPCInvokerExceptionInfo;
import com.fr.workspace.WorkContext;
import com.fr.workspace.server.check.TemplateChecker;

import javax.swing.BoxLayout;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import javax.swing.SwingWorker;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.Set;
import java.util.concurrent.ExecutionException;

import static com.fr.design.dialog.FineJOptionPane.showConfirmDialog;
import static javax.swing.JOptionPane.OK_CANCEL_OPTION;
import static javax.swing.JOptionPane.OK_OPTION;
import static javax.swing.JOptionPane.WARNING_MESSAGE;

public class CheckButton extends UIButton {

    private UILabel message;
    private UIButton okButton;
    private JDialog dialog;
    private UILabel imageLabel;

    public CheckButton() {
        this.setIcon(BaseUtils.readIcon("/com/fr/design/images/buttonicon/check.png"));
        this.setToolTipText(Toolkit.i18nText("Fine_Designer_Check_Font"));
        this.set4ToolbarButton();
        this.addActionListener(checkListener);
    }

    private ActionListener checkListener = new ActionListener() {

        @Override
        public void actionPerformed(ActionEvent e) {

            // Try check
            final SwingWorker<Set<String>, Void> checkThread = new SwingWorker<Set<String>, Void>() {
                @Override
                protected Set<String> doInBackground() throws Exception {
                    // 返回校验结果
                    return check(DesignerContext.getDesignerFrame().getSelectedJTemplate());
                }

                @Override
                protected void done() {
                    try {
                        Set<String> set = get();
                        if (set == null) {
                            return;
                        }
                        if (set.isEmpty()) {
                            okButton.setEnabled(true);
                            imageLabel.setIcon(BaseUtils.readIcon("com/fr/design/images/correct.png"));
                            message.setText("<html>" + Toolkit.i18nText("Fine_Designer_Check_Font_Success") + "</html>");
                        } else {
                            if (dialog != null) {
                                dialog.dispose();
                            }
                            StringBuilder textBuilder = new StringBuilder();
                            textBuilder.append(Toolkit.i18nText("Fine_Designer_Check_Font_Missing_Font")).append("\n");
                            for (String font : set) {
                                textBuilder.append(font).append("\n");
                            }
                            String areaText = textBuilder.toString();
                            CheckFontInfoDialog dialog = new CheckFontInfoDialog(DesignerContext.getDesignerFrame(), areaText);
                            dialog.setVisible(true);
                        }
                    } catch (InterruptedException | ExecutionException e) {
                        FineLoggerFactory.getLogger().error(e.getMessage(), e);
                    }
                }
            };

            JTemplate<?, ?> jtemplate = DesignerContext.getDesignerFrame().getSelectedJTemplate();
            if (jtemplate == null || jtemplate.getEditingFILE() == null) {
                return;
            }
            FILE currentTemplate = jtemplate.getEditingFILE();
            if (currentTemplate instanceof FileNodeFILE) {
                // 判断下模板是否存在 不存在先提示
                if (!currentTemplate.exists()) {
                    int selVal = showConfirmDialog(
                            DesignerContext.getDesignerFrame(),
                            Toolkit.i18nText("Fine-Design_Basic_Web_Preview_Message"),
                            Toolkit.i18nText("Fine_Designer_Check_Font"),
                            OK_CANCEL_OPTION,
                            WARNING_MESSAGE
                    );
                    if (OK_OPTION == selVal) {
                        CallbackSaveWorker worker = jtemplate.saveAs();
                        worker.addSuccessCallback(new Runnable() {
                            @Override
                            public void run() {
                                startCheck(checkThread);
                            }
                        });
                        worker.start(jtemplate.getRuntimeId());
                    }
                } else {
                    if (!jtemplate.isSaved()) {
                        CallbackSaveWorker worker = jtemplate.save();
                        worker.addSuccessCallback(new Runnable() {
                            @Override
                            public void run() {
                                startCheck(checkThread);
                            }
                        });
                        worker.start(jtemplate.getRuntimeId());
                    } else {
                        startCheck(checkThread);
                    }
                }
            } else {
                //模板不在报表环境下，提示保存
                int selVal = FineJOptionPane.showConfirmDialog(
                        DesignerContext.getDesignerFrame(),
                        Toolkit.i18nText("Fine-Design_Basic_Web_Preview_Message"),
                        Toolkit.i18nText("Fine_Designer_Check_Font"),
                        OK_CANCEL_OPTION,
                        WARNING_MESSAGE);
                if (OK_OPTION == selVal) {
                    CallbackSaveWorker worker = jtemplate.saveAs2Env();
                    worker.addSuccessCallback(new Runnable() {
                        @Override
                        public void run() {
                            startCheck(checkThread);
                        }
                    });
                    worker.start(jtemplate.getRuntimeId());
                }
            }
        }

        private void startCheck(SwingWorker<Set<String>, Void> checkThread) {
            initDialogPane();
            dialog.addWindowListener(new WindowAdapter() {
                public void windowClosed(WindowEvent e) {
                    checkThread.cancel(true);
                }
            });
            checkThread.execute();
            dialog.setVisible(true);
        }
    };

    private Set<String> check(JTemplate jtemplate) {
        String path = jtemplate.getEditingFILE().getEnvFullName();
        Set<String> fontSet = WorkContext.getCurrent().get(TemplateChecker.class, new ExceptionHandler<Void>() {

            @Override
            public Void callHandler(RPCInvokerExceptionInfo rpcInvokerExceptionInfo) {
                imageLabel.setIcon(BaseUtils.readIcon("com/fr/design/images/error.png"));
                message.setText("<html>" + Toolkit.i18nText("Fine_Designer_Check_Font_Upgrade") + "</html>");
                okButton.setEnabled(true);
                return null;
            }
        }).checkFont(path);
        return fontSet;
    }

    private void initDialogPane() {
        message = new UILabel();
        message.setText(Toolkit.i18nText("Fine-Designer_Check_Font_Checking") + "...");
        imageLabel = new UILabel();
        okButton = new UIButton(Toolkit.i18nText("Fine-Design_Report_OK"));
        okButton.setEnabled(false);
        okButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                dialog.dispose();
            }
        });
        dialog = new JDialog();
        dialog.setTitle(Toolkit.i18nText("Fine_Designer_Check_Font"));
        dialog.setModal(true);
        dialog.setSize(DesignSizeI18nManager.getInstance().i18nDimension(this.getClass().getName()));
        JPanel jp = new JPanel();
        JPanel upPane = new JPanel();
        JPanel downPane = new JPanel();
        imageLabel = new UILabel(IOUtils.readIcon("com/fr/design/images/waiting.png"));
        upPane.setLayout(new FlowLayout(FlowLayout.LEFT, 10, 10));
        upPane.add(imageLabel);
        upPane.add(message);
        downPane.setLayout(new FlowLayout(FlowLayout.CENTER, 6, 0));
        downPane.add(okButton);
        jp.setLayout(new BoxLayout(jp, BoxLayout.Y_AXIS));
        jp.add(upPane);
        jp.add(downPane);
        dialog.add(jp);
        dialog.setResizable(false);
        dialog.setLocationRelativeTo(SwingUtilities.getWindowAncestor(this));
    }
}
