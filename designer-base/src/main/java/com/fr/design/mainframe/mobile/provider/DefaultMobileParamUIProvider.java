package com.fr.design.mainframe.mobile.provider;

import com.fr.design.beans.BasicBeanPane;
import com.fr.design.fun.impl.AbstractMobileParamUIProvider;
import com.fr.design.mainframe.mobile.ui.DefaultMobileParamDefinePane;
import com.fr.form.ui.mobile.MobileParamStyle;
import com.fr.form.ui.mobile.impl.DefaultMobileParameterStyle;
import com.fr.locale.InterProviderFactory;

/**
 * @author hades
 * @version 10.0
 * Created by hades on 2021/1/4
 */
public class DefaultMobileParamUIProvider extends AbstractMobileParamUIProvider {

    @Override
    public Class<? extends MobileParamStyle> classForMobileParamStyle() {
        return DefaultMobileParameterStyle.class;
    }

    @Override
    public Class<? extends BasicBeanPane<MobileParamStyle>> classForMobileParamAppearance() {
        return DefaultMobileParamDefinePane.class;
    }

    @Override
    public String displayName() {
        return InterProviderFactory.getProvider().getLocText("Fine-Engine_Report_DEFAULT");
    }


}
