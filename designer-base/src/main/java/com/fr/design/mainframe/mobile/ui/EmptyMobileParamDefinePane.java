package com.fr.design.mainframe.mobile.ui;

import com.fr.design.beans.BasicBeanPane;
import com.fr.design.i18n.Toolkit;
import com.fr.design.layout.FRGUIPaneFactory;
import com.fr.form.ui.mobile.MobileParamStyle;
import com.fr.report.fun.MobileParamStyleProvider;
import com.fr.report.mobile.EmptyMobileParamStyle;
import java.awt.BorderLayout;
import javax.swing.JPanel;

/**
 * @author hades
 * @version 10.0
 * Created by hades on 2021/1/4
 */
public class EmptyMobileParamDefinePane extends BasicBeanPane<MobileParamStyle> {

    private final MobileParamStyleProvider styleProvider;

    public EmptyMobileParamDefinePane(MobileParamStyleProvider styleProvider) {
        this.styleProvider = styleProvider;
        initComponents();
    }

    private void initComponents() {
        this.setLayout(new BorderLayout());
        JPanel centerPane = FRGUIPaneFactory.createTitledBorderPane(Toolkit.i18nText("Fine-Design_Report_Set"));
        this.add(centerPane);
    }


    @Override
    public void populateBean(MobileParamStyle ob) {

    }

    @Override
    public MobileParamStyle updateBean() {
        return new EmptyMobileParamStyle(styleProvider);
    }

    @Override
    protected String title4PopupWindow() {
        return null;
    }
}
