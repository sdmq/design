package com.fr.design.mainframe.mobile.ui;

import com.fr.base.BaseUtils;
import com.fr.design.constants.LayoutConstants;
import com.fr.design.gui.ibutton.UIColorButton;
import com.fr.design.gui.ibutton.UIToggleButton;
import com.fr.design.gui.icombobox.UIComboBox;
import com.fr.design.gui.icombobox.UIComboBoxRenderer;
import com.fr.design.i18n.Toolkit;
import com.fr.design.utils.gui.GUICoreUtils;
import com.fr.general.FRFont;
import com.fr.stable.Constants;
import com.fr.stable.StringUtils;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.util.Vector;

public class MobileStyleFontConfigPane extends JPanel {
    public static final int FONT_NONE = 0;
    private static final int MAX_FONT_SIZE = 18;
    private static final int MIN_FONT_SIZE = 12;
    private static final Dimension BUTTON_SIZE = new Dimension(20, 18);

    public static Vector<Integer> getFontSizes() {
        Vector<Integer> FONT_SIZES = new Vector<Integer>();
        for (int i = MIN_FONT_SIZE; i <= MAX_FONT_SIZE; i++) {
            FONT_SIZES.add(i);
        }
        return FONT_SIZES;
    }

    private UIComboBox fontSizeComboBox;
    private UIColorButton color;
    private UIToggleButton italic;
    private UIToggleButton bold;

    public MobileStyleFontConfigPane() {
        this.initComponent();
    }

    private void initComponent() {

        fontSizeComboBox = new UIComboBox();
        fontSizeComboBox.setModel(new DefaultComboBoxModel(getFontSizes()));
        fontSizeComboBox.setSelectedItem(16);
        fontSizeComboBox.setPreferredSize(new Dimension(60, 20));
        fontSizeComboBox.setRenderer(new LineCellRenderer());
        color = new UIColorButton();
        italic = new UIToggleButton(BaseUtils.readIcon("/com/fr/design/images/m_format/cellstyle/italic.png"));
        bold = new UIToggleButton(BaseUtils.readIcon("/com/fr/design/images/m_format/cellstyle/bold.png"));

        this.setButtonsTips();
        this.setButtonsSize(BUTTON_SIZE);

        Component[] components_font = new Component[]{
                fontSizeComboBox, color, italic, bold
        };

        JPanel buttonPane = new JPanel(new BorderLayout());
        buttonPane.add(GUICoreUtils.createFlowPane(components_font, FlowLayout.LEFT, LayoutConstants.HGAP_LARGE));

        this.setLayout(new BorderLayout(0,0));
        this.add(buttonPane, BorderLayout.CENTER);
    }

    private void setButtonsTips() {
        color.setToolTipText(Toolkit.i18nText("Fine-Design_Report_Foreground"));
        italic.setToolTipText(com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Report_Italic"));
        bold.setToolTipText(com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Report_Bold"));
    }

    private void setButtonsSize(Dimension size) {
        color.setPreferredSize(size);
        italic.setPreferredSize(size);
        bold.setPreferredSize(size);
    }


    public void populateBean(FRFont frFont) {
        fontSizeComboBox.setSelectedItem(frFont.getSize());
        color.setColor(frFont.getForeground());
        bold.setSelected(frFont.isBold());
        italic.setSelected(frFont.isItalic());
    }

    public FRFont updateBean() {
        int style = Font.PLAIN;
        style += this.bold.isSelected() ? Font.BOLD : Font.PLAIN;
        style += this.italic.isSelected() ? Font.ITALIC : Font.PLAIN;
        return FRFont.getInstance(
                FRFont.DEFAULT_FONTNAME,
                style,
                Float.parseFloat(fontSizeComboBox.getSelectedItem().toString()),
                color.getColor(),
                Constants.LINE_NONE
        );
    }

    private class LineCellRenderer extends UIComboBoxRenderer {
        public LineCellRenderer() {
            super();
        }

        public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected, boolean cellHasFocus) {
            JLabel renderer =(JLabel) super.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);
            int currentValue = ((Integer) value).intValue();
            if (currentValue == MobileStyleFontConfigPane.FONT_NONE) {
                renderer.setText(StringUtils.BLANK + com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Report_None"));
            }
            return renderer;
        }
    }

}
