package com.fr.design.mainframe.predefined.ui;

import com.fr.config.ServerPreferenceConfig;
import com.fr.config.predefined.PredefinedStyle;
import com.fr.design.dialog.BasicPane;
import com.fr.design.event.ChangeListener;
import com.fr.design.gui.icontainer.UIScrollPane;
import com.fr.design.layout.FRGUIPaneFactory;

import javax.swing.BorderFactory;
import javax.swing.JPanel;
import java.util.Iterator;
import java.awt.BorderLayout;
import java.awt.Dimension;

/**
 * Created by kerry on 2020-08-26
 */
public class PredefinedStyleSelectPane extends BasicPane {
    private PredefinedStyleBlock selectedBlock;
    private boolean editable;
    private JPanel contentPane;
    private String currentApplicateStyle;
    private ChangeListener changeListener;


    public PredefinedStyleSelectPane(String currentApplicateStyle, boolean editable) {
        this.editable = editable;
        this.currentApplicateStyle = currentApplicateStyle;
        initPane();
    }

    public void registerChangeListener(ChangeListener changeListener) {
        this.changeListener = changeListener;

    }


    private void initPane() {
        this.setLayout(FRGUIPaneFactory.createBorderLayout());
        contentPane = FRGUIPaneFactory.createBoxFlowInnerContainer_S_Pane(5, 8);
        createContentPane();
        UIScrollPane scrollPane = new UIScrollPane(contentPane);
        scrollPane.setPreferredSize(new Dimension(630, 480));
        scrollPane.setBorder(BorderFactory.createEmptyBorder());
        this.add(scrollPane, BorderLayout.CENTER);
    }


    public void createContentPane() {
        contentPane.removeAll();
        Iterator<PredefinedStyle> iterator = ServerPreferenceConfig.getInstance().getPreferenceStyleConfig().getPredefinedStyleIterator();
        int rowCount = (ServerPreferenceConfig.getInstance().getPreferenceStyleConfig().getPredefinedSize() +2)/ 3;
        contentPane.setPreferredSize(new Dimension(618, 220 * rowCount));
        while (iterator.hasNext()) {
            PredefinedStyle tmpStyle = iterator.next();

            if (tmpStyle != null) {
                PredefinedStyleBlock tmpPanel =
                        new PredefinedStyleBlock(tmpStyle, this, this.editable);
                contentPane.add(tmpPanel);
            }
        }
    }


    public String getCurrentApplicateStyle() {
        return currentApplicateStyle;
    }

    public void refreshPane() {
        createContentPane();
        this.validate();
        this.repaint();
    }

    @Override
    protected String title4PopupWindow() {
        return null;
    }

    public void setSelectedPreviewPane(PredefinedStyleBlock selectedPreviewPane) {
        this.selectedBlock = selectedPreviewPane;
        if (changeListener != null) {
            changeListener.fireChanged(null);
        }
        this.repaint();
    }

    public PredefinedStyleBlock getSelectedPreviewPane() {
        return selectedBlock;
    }

    public PredefinedStyle update() {
        if (this.selectedBlock == null){
            return null;
        }
        return this.selectedBlock.update();
    }


}
