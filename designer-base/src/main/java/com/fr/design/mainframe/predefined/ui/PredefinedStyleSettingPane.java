package com.fr.design.mainframe.predefined.ui;

import com.fr.config.predefined.PredefinedNameStyleProvider;
import com.fr.design.file.HistoryTemplateListCache;
import com.fr.design.gui.frpane.AbstractAttrNoScrollPane;
import com.fr.design.gui.frpane.AttributeChangeListener;
import com.fr.design.gui.ibutton.UIRadioButton;
import com.fr.design.gui.ilable.UILabel;
import com.fr.design.layout.FRGUIPaneFactory;
import com.fr.design.mainframe.JTemplate;
import com.fr.design.mainframe.predefined.ui.preview.StyleSettingPreviewPane;
import javax.swing.ButtonGroup;
import javax.swing.JPanel;
import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by kerry on 2020-09-02
 */
public abstract class PredefinedStyleSettingPane<T> extends AbstractAttrNoScrollPane {
    protected StyleSettingPreviewPane previewPane;
    protected UIRadioButton predefinedRadioBtn;
    private UIRadioButton customRadioBtn;
    private JPanel customDetailPane;
    private JPanel predefinedSettingPane;
    private CardLayout tabbedPane;
    private JPanel center;
    private boolean isPopulating = false;


    public void setPopulating(boolean populating) {
        isPopulating = populating;
    }

    protected void initContentPane() {
        leftContentPane = createContentPane();
        this.add(leftContentPane, BorderLayout.CENTER);
    }

    @Override
    protected JPanel createContentPane() {
        JPanel contentPane = FRGUIPaneFactory.createBorderLayout_S_Pane();
        previewPane = createPreviewPane();
        JPanel previewTitlePane = FRGUIPaneFactory.createTitledBorderPane(com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Basic_Preview"));
        if (previewPane != null) {
            previewTitlePane.setPreferredSize(new Dimension(407, 527));
            previewTitlePane.add(previewPane);
            contentPane.add(previewTitlePane, BorderLayout.WEST);
        }

        customDetailPane = createCustomDetailPane();
        predefinedSettingPane = createPredefinedSettingPane();

        JPanel centerPane = FRGUIPaneFactory.createBorderLayout_S_Pane();
        JPanel jPanel = FRGUIPaneFactory.createBoxFlowInnerContainer_S_Pane(10, 20, 10);
        jPanel.add(new UILabel(com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Basic_Style")));
        predefinedRadioBtn = new UIRadioButton(com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Basic_Preference_Predefined"));
        customRadioBtn = new UIRadioButton(com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Basic_Custom"));

        tabbedPane = new CardLayout();
        center = new JPanel(tabbedPane);
        center.add(predefinedSettingPane, com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Basic_Preference_Predefined"));
        center.add(customDetailPane, com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Basic_Custom"));
        predefinedRadioBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                populateCustomPane();
                tabbedPane.show(center, com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Basic_Preference_Predefined"));
            }
        });
        customRadioBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                tabbedPane.show(center, com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Basic_Custom"));
            }
        });
        jPanel.add(predefinedRadioBtn);
        jPanel.add(customRadioBtn);

        ButtonGroup layoutBG = new ButtonGroup();
        layoutBG.add(predefinedRadioBtn);
        layoutBG.add(customRadioBtn);
        centerPane.add(jPanel, BorderLayout.NORTH);
        centerPane.add(center, BorderLayout.CENTER);
        contentPane.add(centerPane, BorderLayout.CENTER);
        this.addAttributeChangeListener(new AttributeChangeListener() {
            @Override
            public void attributeChange() {
                if (isPopulating) {
                    return;
                }
                if (previewPane != null) {
                    previewPane.refresh();
                }
            }
        });
        return contentPane;
    }


    protected abstract StyleSettingPreviewPane createPreviewPane();

    protected abstract JPanel createCustomDetailPane();

    protected JPanel createPredefinedSettingPane() {
        return new JPanel();
    }

    protected void populate(PredefinedNameStyleProvider nameStyle) {
        this.predefinedRadioBtn.setSelected(nameStyle.usePredefinedStyle());
        this.customRadioBtn.setSelected(!nameStyle.usePredefinedStyle());
        if (nameStyle.usePredefinedStyle()) {
            tabbedPane.show(center, com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Basic_Preference_Predefined"));
        } else {
            tabbedPane.show(center, com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Basic_Custom"));
        }

    }

    protected String getPredefinedStyleName() {
        JTemplate template = HistoryTemplateListCache.getInstance().getCurrentEditingTemplate();
        return template.getTemplatePredefinedStyle();
    }


    /**
     * 用于在切换到预定义样式后重置自定义样式的设置
     */
    protected abstract void populateCustomPane();


    /**
     * 展示数据
     *
     * @param ob 待展示的对象
     */
    public abstract void populateBean(T ob);

    /**
     * 保存数据
     *
     * @return 待保存的对象
     */
    public abstract T updateBean();

    /**
     * 保存数据
     *
     * @param ob 待保存的对象
     */
    public void updateBean(T ob) {

    }


}
