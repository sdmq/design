package com.fr.design.mainframe.predefined.ui.detail.background;

import com.fr.design.event.UIObserverListener;
import com.fr.design.mainframe.backgroundpane.BackgroundQuickPane;
import com.fr.general.Background;

import javax.swing.event.ChangeListener;

/**
 * Created by kerry on 2020-09-14
 */
public abstract class AbstractBackgroundDetailPane<T extends Background> extends BackgroundQuickPane {
    @Override
    public boolean accept(Background background) {
        return false;
    }

    @Override
    public void populateBean(Background background) {
        this.populate((T) background);
    }

    @Override
    public Background updateBean() {
        return this.update();
    }

    @Override
    public String title4PopupWindow() {
        return null;
    }

    @Override
    public void reset() {

    }

    @Override
    public void registerChangeListener(UIObserverListener listener) {

    }

    public abstract void populate(T background);

    public abstract T update();

    public void addChangeListener(ChangeListener changeListener) {

    }

}
