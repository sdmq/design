package com.fr.design.mainframe.predefined.ui.detail.chart;

import com.fr.config.predefined.PredefinedChartStyle;
import com.fr.design.constants.LayoutConstants;
import com.fr.design.dialog.BasicPane;
import com.fr.design.gui.icontainer.UIScrollPane;
import com.fr.design.layout.FRGUIPaneFactory;
import com.fr.design.layout.TableLayout;
import com.fr.design.layout.TableLayoutHelper;

import javax.swing.BorderFactory;
import javax.swing.JPanel;
import java.awt.BorderLayout;
import java.awt.Component;

/**
 * @author Bjorn
 * @version 10.0
 * Created by Bjorn on 2020-09-17
 */
public abstract class AbstractChartStylePane extends BasicPane {

    public AbstractChartStylePane() {
        initComponents();
        initPane();
    }

    protected abstract void initComponents();

    protected void initPane() {
        this.setLayout(FRGUIPaneFactory.createBorderLayout());
        double e = 155;
        double p = TableLayout.PREFERRED;
        double[] columnSize = {p, e};
        JPanel gapTableLayoutPane = TableLayoutHelper.createGapTableLayoutPane(getComponent(), getRows(p), columnSize, 20, LayoutConstants.VGAP_LARGE);
        gapTableLayoutPane.setBorder(BorderFactory.createEmptyBorder(0, 10, 0, 0));
        UIScrollPane rightTopPane = new UIScrollPane(gapTableLayoutPane);
        rightTopPane.setBorder(BorderFactory.createEmptyBorder());
        this.add(rightTopPane, BorderLayout.CENTER);
    }

    protected abstract Component[][] getComponent();

    protected abstract double[] getRows(double p);

    public abstract void populate(PredefinedChartStyle chartStyle);

    public abstract void update(PredefinedChartStyle chartStyle);
}
