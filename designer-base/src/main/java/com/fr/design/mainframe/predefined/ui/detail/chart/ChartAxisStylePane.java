package com.fr.design.mainframe.predefined.ui.detail.chart;

import com.fr.config.predefined.PredefinedChartStyle;
import com.fr.design.gui.ilable.UILabel;
import com.fr.design.i18n.Toolkit;
import com.fr.design.style.color.ColorSelectBox;

import java.awt.Component;

/**
 * @author Bjorn
 * @version 10.0
 * Created by Bjorn on 2020-09-16
 */
public class ChartAxisStylePane extends AbstractChartStylePane {

    //轴标题字体样式
    private ChartFontPane titleFontPane;

    //轴标签字体样式
    private ChartFontPane labelFontPane;

    //轴线颜色
    private ColorSelectBox axisLineColor;

    protected void initComponents() {
        titleFontPane = new ChartFontPane() {
            public String getUILabelText() {
                return Toolkit.i18nText("Fine-Design_Chart_Axis_Title_Character");
            }
        };
        labelFontPane = new ChartFontPane() {
            public String getUILabelText() {
                return Toolkit.i18nText("Fine-Design_Chart_Axis_Label_Character");
            }
        };
        axisLineColor = new ColorSelectBox(100);
    }

    protected Component[][] getComponent() {
        return new Component[][]{
                new Component[]{titleFontPane, null},
                new Component[]{labelFontPane, null},
                new Component[]{new UILabel(Toolkit.i18nText("Fine-Design_Chart_Axis_Line_Color")), axisLineColor}
        };
    }

    protected double[] getRows(double p) {
        return new double[]{p, p, p};
    }

    @Override
    protected String title4PopupWindow() {
        return com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Chart_Axis");
    }

    public void populate(PredefinedChartStyle chartStyle) {
        titleFontPane.populate(chartStyle.getAxisTitleFont());
        labelFontPane.populate(chartStyle.getAxisLabelFont());
        axisLineColor.setSelectObject(chartStyle.getAxisLineColor());
    }


    public void update(PredefinedChartStyle chartStyle) {
        chartStyle.setAxisTitleFont(titleFontPane.update());
        chartStyle.setAxisLabelFont(labelFontPane.update());
        chartStyle.setAxisLineColor(axisLineColor.getSelectObject());
    }
}
