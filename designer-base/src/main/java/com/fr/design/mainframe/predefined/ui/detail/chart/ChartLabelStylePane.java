package com.fr.design.mainframe.predefined.ui.detail.chart;

import com.fr.config.predefined.PredefinedChartStyle;
import com.fr.design.gui.ibutton.UIButtonGroup;
import com.fr.design.gui.ilable.UILabel;
import com.fr.design.i18n.Toolkit;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * @author Bjorn
 * @version 10.0
 * Created by Bjorn on 2020-09-16
 */
public class ChartLabelStylePane extends AbstractChartStylePane {

    private UIButtonGroup<Integer> autoButton;
    //字体样式
    private ChartFontPane chartFontPane;

    protected void initComponents() {
        autoButton = new UIButtonGroup<>(new String[]{Toolkit.i18nText("Fine-Design_Chart_Auto"),
                Toolkit.i18nText("Fine-Design_Chart_Custom")});
        chartFontPane = new ChartFontPane() {
            public String getUILabelText() {
                return Toolkit.i18nText("Fine-Design_Chart_Label_Character");
            }
        };
        initListener();
    }

    protected Component[][] getComponent() {
        return new Component[][]{
                new Component[]{null, null},
                new Component[]{new UILabel(Toolkit.i18nText("Fine-Design_Chart_Style_Setting")), autoButton},
                new Component[]{chartFontPane, null}
        };
    }

    protected double[] getRows(double p) {
        return new double[]{p, p, p};
    }

    private void initListener() {
        autoButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                checkPreButton();
            }
        });
    }

    private void checkPreButton() {
        chartFontPane.setVisible(autoButton.getSelectedIndex() == 1);
        chartFontPane.setPreferredSize(autoButton.getSelectedIndex() == 1 ? new Dimension(0, 60) : new Dimension(0, 0));
    }


    @Override
    protected String title4PopupWindow() {
        return com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Chart_Label");
    }

    public void populate(PredefinedChartStyle chartStyle) {
        autoButton.setSelectedIndex(chartStyle.isAutoLabelFont() ? 0 : 1);
        chartFontPane.populate(chartStyle.getLabelFont());
        checkPreButton();
    }


    public void update(PredefinedChartStyle chartStyle) {
        chartStyle.setAutoLabelFont(autoButton.getSelectedIndex() == 0);
        chartStyle.setLabelFont(chartFontPane.update());
    }
}
