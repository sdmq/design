package com.fr.design.mainframe.predefined.ui.detail.component;

import com.fr.base.BaseUtils;
import com.fr.base.Utils;
import com.fr.config.predefined.PredefinedComponentStyle;
import com.fr.design.constants.LayoutConstants;
import com.fr.design.constants.UIConstants;
import com.fr.design.formula.TinyFormulaPane;
import com.fr.design.gui.ibutton.UIButtonGroup;
import com.fr.design.gui.ibutton.UIColorButton;
import com.fr.design.gui.ibutton.UIToggleButton;
import com.fr.design.gui.icombobox.LineComboBox;
import com.fr.design.gui.icombobox.UIComboBox;
import com.fr.design.gui.icontainer.UIScrollPane;
import com.fr.design.gui.ilable.UILabel;
import com.fr.design.gui.style.BackgroundNoImagePane;
import com.fr.design.gui.style.FRFontPane;
import com.fr.design.layout.FRGUIPaneFactory;
import com.fr.design.layout.TableLayout;
import com.fr.design.layout.TableLayoutHelper;
import com.fr.design.mainframe.backgroundpane.GradientBackgroundQuickPane;
import com.fr.design.utils.gui.GUICoreUtils;
import com.fr.form.ui.LayoutBorderStyle;
import com.fr.form.ui.WidgetTitle;
import com.fr.general.FRFont;
import com.fr.general.act.BorderPacker;
import com.fr.general.act.TitlePacker;
import com.fr.stable.ArrayUtils;
import com.fr.stable.Constants;
import com.fr.stable.StringUtils;

import javax.swing.BorderFactory;
import javax.swing.Icon;
import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;

/**
 * Created by kerry on 2020-09-01
 */
public class ComponentTitleStylePane extends ComponentStylePane {
    private final static Dimension BUTTON_SIZE = new Dimension(24, 20);
    //标题内容
    private TinyFormulaPane formulaPane;
    //标题格式
    private UIComboBox fontNameComboBox;
    private UIComboBox fontSizeComboBox;
    private UIColorButton colorSelectPane;
    private UIToggleButton bold;
    private UIToggleButton italic;
    private UIToggleButton underline;
    private LineComboBox underlineCombo;
    //对齐方式
    private UIButtonGroup hAlignmentPane;
    //标题背景
    private BackgroundNoImagePane titleBackgroundPane;

    public static ComponentTitleStylePane createPredefinedSettingPane(){
        return new ComponentTitleStylePane(true);
    }

    public static ComponentTitleStylePane createStyleSettingPane(){
        return new ComponentTitleStylePane(false);
    }

    private ComponentTitleStylePane(boolean isPredefined) {
        initPane(isPredefined);
    }

    protected void initPane(boolean isPredefined) {
        this.setLayout(FRGUIPaneFactory.createBorderLayout());
        this.setBorder(BorderFactory.createEmptyBorder(6, 0,0,0));
        formulaPane = new TinyFormulaPane();
        fontSizeComboBox = new UIComboBox(FRFontPane.FONT_SIZES);
        fontNameComboBox = new UIComboBox(Utils.getAvailableFontFamilyNames4Report());
        fontNameComboBox.setPreferredSize(new Dimension(105 , 20));
        JPanel fontSizeTypePane = new JPanel(new BorderLayout(3, 0));
        fontSizeTypePane.add(fontSizeComboBox, BorderLayout.CENTER);
        fontSizeTypePane.add(fontNameComboBox, BorderLayout.EAST);

        Icon[] hAlignmentIconArray = {BaseUtils.readIcon("/com/fr/design/images/m_format/cellstyle/h_left_normal.png"),
                BaseUtils.readIcon("/com/fr/design/images/m_format/cellstyle/h_center_normal.png"),
                BaseUtils.readIcon("/com/fr/design/images/m_format/cellstyle/h_right_normal.png"),};
        Integer[] hAlignment = new Integer[]{Constants.LEFT, Constants.CENTER, Constants.RIGHT};
        hAlignmentPane = new UIButtonGroup<Integer>(hAlignmentIconArray, hAlignment);
        hAlignmentPane.setAllToolTips(new String[]{com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Form_StyleAlignment_Left")
                , com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Form_StyleAlignment_Center"),
                com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Form_StyleAlignment_Right")});
        JPanel hPaneContainer = new JPanel(new FlowLayout(FlowLayout.LEFT, 0, 0));
        hPaneContainer.add(hAlignmentPane);

        titleBackgroundPane = new BackgroundNoImagePane();

        double p = TableLayout.PREFERRED;
        double[] rowSize = {p, p, p, p, p, p, p, p};
        double[] columnSize = {p, 157};
        JComponent[][] jComponents = new JComponent[][]{
                {new UILabel(com.fr.design.i18n.Toolkit.i18nText(com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Basic_Char"))), fontSizeTypePane},
                {new UILabel(""), initFontButtonPane()},
                {new UILabel(com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Form_Alignment-Style")), hAlignmentPane},
                {new UILabel(com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Form_Widget_Style_Title_Background")), titleBackgroundPane}};

        JComponent[][] displayComponents = createDisplayComponentArray(isPredefined, jComponents);
        JPanel rightBottomContentPane = TableLayoutHelper.createCommonTableLayoutPane(displayComponents, rowSize, columnSize, 10);

        UIScrollPane jPanel = new UIScrollPane(rightBottomContentPane);
        jPanel.setBorder(BorderFactory.createEmptyBorder());
        this.add(jPanel, BorderLayout.CENTER);
    }

    private JComponent[][] createDisplayComponentArray(boolean isPredefined, JComponent[][] baseComponents) {
        if (isPredefined) {
            return baseComponents;
        }
        JComponent[][] titleComponent = new JComponent[][]{{new UILabel(com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Form_Widget_Style_Title_Content")), formulaPane}};
        return ArrayUtils.addAll(titleComponent, baseComponents);

    }

    protected JPanel initFontButtonPane() {
        colorSelectPane = new UIColorButton();
        bold = new UIToggleButton(BaseUtils.readIcon("/com/fr/design/images/m_format/cellstyle/bold.png"));
        italic = new UIToggleButton(BaseUtils.readIcon("/com/fr/design/images/m_format/cellstyle/italic.png"));
        underline = new UIToggleButton(BaseUtils.readIcon("/com/fr/design/images/m_format/cellstyle/underline.png"));
        bold.setPreferredSize(BUTTON_SIZE);
        italic.setPreferredSize(BUTTON_SIZE);
        underline.setPreferredSize(BUTTON_SIZE);
        underline.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                underlineCombo.setVisible(underline.isSelected());
            }
        });
        underlineCombo = new LineComboBox(UIConstants.BORDER_LINE_STYLE_ARRAY);
        Component[] components_font = new Component[]{
                colorSelectPane, italic, bold, underline
        };
        JPanel buttonPane = new JPanel(new BorderLayout());
        buttonPane.add(GUICoreUtils.createFlowPane(components_font, FlowLayout.LEFT, LayoutConstants.HGAP_SMALL));
        JPanel combinePane = FRGUIPaneFactory.createBorderLayout_S_Pane();
        combinePane.add(buttonPane, BorderLayout.WEST);
        combinePane.add(underlineCombo, BorderLayout.CENTER);
        initAllNames();
        setToolTips();
        return combinePane;

    }

    protected void initAllNames() {
        fontNameComboBox.setGlobalName(com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Form_FRFont_Family"));
        fontSizeComboBox.setGlobalName(com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Form_FRFont_Size"));
        colorSelectPane.setGlobalName(com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Form_FRFont_Foreground"));
        italic.setGlobalName(com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Form_FRFont_Italic"));
        bold.setGlobalName(com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Form_FRFont_Bold"));
        underline.setGlobalName(com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Form_FRFont_Underline"));
        underlineCombo.setGlobalName(com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Form_FRFont_Line_Style"));
    }

    protected void setToolTips() {
        colorSelectPane.setToolTipText(com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Form_FRFont_Foreground"));
        italic.setToolTipText(com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Form_FRFont_Italic"));
        bold.setToolTipText(com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Form_FRFont_Bold"));
        underline.setToolTipText(com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Form_FRFont_Underline"));
    }


    @Override
    protected String title4PopupWindow() {
        return com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Form_Title");
    }

    @Override
    public void populate(PredefinedComponentStyle componentStyle) {
        BorderPacker borderStyle = componentStyle.getBorderStyle();
        TitlePacker widgetTitle = borderStyle == null ? new WidgetTitle() : borderStyle.getTitle();
        widgetTitle = widgetTitle == null ? new WidgetTitle() : widgetTitle;

        populateFormula(widgetTitle);
        populateFont(widgetTitle);

        hAlignmentPane.setSelectedItem(widgetTitle.getPosition());

        titleBackgroundPane.populateBean(widgetTitle.getBackground());
    }

    private void populateFormula(TitlePacker widgetTitle) {
        this.formulaPane.populateBean(widgetTitle.getTextObject().toString());
    }

    protected void populateFont(TitlePacker widgetTitle) {
        FRFont frFont = widgetTitle.getFrFont();
        this.fontSizeComboBox.setSelectedItem(frFont.getSize());
        this.fontNameComboBox.setSelectedItem(frFont.getFamily());
        this.colorSelectPane.setColor(frFont.getForeground());
        this.colorSelectPane.repaint();
        bold.setSelected(frFont.isBold());
        italic.setSelected(frFont.isItalic());
        int line = frFont.getUnderline();
        if (line == Constants.LINE_NONE) {
            underline.setSelected(false);
            underlineCombo.setVisible(false);
        } else {
            underline.setSelected(true);
            underlineCombo.setVisible(true);
            this.underlineCombo.setSelectedLineStyle(line);
        }
    }

    @Override
    public void update(PredefinedComponentStyle componentStyle) {
        BorderPacker style = componentStyle.getBorderStyle();
        TitlePacker title = style.getTitle() == null ? new WidgetTitle() : style.getTitle();
        String titleText = formulaPane.updateBean();
        title.setTextObject(titleText);
        style.setType(StringUtils.isEmpty(titleText) ? LayoutBorderStyle.STANDARD : LayoutBorderStyle.TITLE);
        FRFont frFont = title.getFrFont();
        frFont = frFont.applySize((Integer) fontSizeComboBox.getSelectedItem());
        frFont = frFont.applyName(fontNameComboBox.getSelectedItem().toString());
        frFont = frFont.applyForeground(colorSelectPane.getColor());
        frFont = updateItalicBold(frFont);
        int line = underline.isSelected() ? this.underlineCombo.getSelectedLineStyle() : Constants.LINE_NONE;
        frFont = frFont.applyUnderline(line);
        title.setFrFont(frFont);
        title.setPosition((Integer) hAlignmentPane.getSelectedItem());
        title.setBackground(titleBackgroundPane.update());
        style.setTitle(title);
    }

    private FRFont updateItalicBold(FRFont frFont) {
        int italic_bold = frFont.getStyle();
        boolean isItalic = italic_bold == Font.ITALIC || italic_bold == (Font.BOLD + Font.ITALIC);
        boolean isBold = italic_bold == Font.BOLD || italic_bold == (Font.BOLD + Font.ITALIC);
        if (italic.isSelected() && !isItalic) {
            italic_bold += Font.ITALIC;
        } else if (!italic.isSelected() && isItalic) {
            italic_bold -= Font.ITALIC;
        }
        frFont = frFont.applyStyle(italic_bold);
        if (bold.isSelected() && !isBold) {
            italic_bold += Font.BOLD;
        } else if (!bold.isSelected() && isBold) {
            italic_bold -= Font.BOLD;
        }
        frFont = frFont.applyStyle(italic_bold);
        return frFont;
    }
}
