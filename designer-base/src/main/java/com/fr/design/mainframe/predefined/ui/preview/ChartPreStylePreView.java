package com.fr.design.mainframe.predefined.ui.preview;

import com.fr.base.chart.BaseChartCollection;
import com.fr.base.chart.BaseChartPainter;
import com.fr.base.chart.chartdata.CallbackEvent;
import com.fr.base.chart.result.WebChartIDInfo;
import com.fr.chart.base.ChartPreStyleProvider;
import com.fr.chart.chartattr.ChartCollection;
import com.fr.config.predefined.PredefinedStyle;
import com.fr.design.gui.chart.MiddleChartComponent;
import com.fr.general.Background;
import com.fr.plugin.chart.vanchart.VanChart;
import com.fr.script.Calculator;
import com.fr.stable.core.PropertyChangeListener;

import javax.swing.BorderFactory;
import java.util.List;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;

/**
 * @author Bjorn
 * @version 10.0
 * Created by Bjorn on 2020-09-28
 */
public class ChartPreStylePreView extends MiddleChartComponent {

    private ChartCollection chartCollection;

    private double scaleX = 1.0;
    private double scaleY = 1.0;

    private CallbackEvent callbackEvent;

    private static final int BOUNDS = 10;

    private Background componentBackground;

    public ChartPreStylePreView() {
    }

    public ChartPreStylePreView(ChartCollection cc) {
        this(cc, 1.0, 1.0);
    }

    public ChartPreStylePreView(ChartCollection cc, double scaleX, double scaleY) {
        this.scaleX = scaleX;
        this.scaleY = scaleY;
        this.setBorder(BorderFactory.createEmptyBorder(5, 10, 10, 10));
        populate(cc);
    }

    public void setCallbackEvent(CallbackEvent callbackEvent) {
        this.callbackEvent = callbackEvent;
    }

    public void paintComponent(Graphics g) {
        Graphics2D g2d = (Graphics2D) g;
        g2d.translate(BOUNDS, BOUNDS);
        g2d.scale(scaleX, scaleY);
        super.paintComponent(g);
        drawChart(g2d);
        g2d.scale(1 / scaleX, 1 / scaleY);
        g2d.translate(-BOUNDS, -BOUNDS);
    }

    private void drawChart(Graphics2D g2d) {
        Dimension d = getBounds().getSize();
        int chartWidth = (int) (d.width / scaleX) - BOUNDS * 2;
        int chartHeight = (int) (d.height / scaleX) - BOUNDS * 2;
        if (componentBackground != null) {
            Graphics clipg = g2d.create(0, 0, chartWidth, chartHeight);
            componentBackground.paint(clipg, clipg.getClipBounds());
        }
        BaseChartPainter painter = chartCollection.createResultChartPainterWithOutDealFormula(Calculator.createCalculator(),
                WebChartIDInfo.createEmptyDesignerInfo(), chartWidth, chartHeight);
        painter.paint(g2d, chartWidth, chartHeight, 0, null, callbackEvent);
    }

    public void refresh(PredefinedStyle style) {
        VanChart vanChart = chartCollection.getSelectedChartProvider(VanChart.class);
        List<ChartPreStyleProvider> chartPreStyleProviders = vanChart.getChartPreStyleProvider();
        for (ChartPreStyleProvider chartPreStyleProvider : chartPreStyleProviders) {
            chartPreStyleProvider.updatePreDefinedStyle(style);
        }
        componentBackground = style.getComponentStyle().getBorderStyle().getBackground();
        vanChart.attrChange();
    }

    @Override
    public void populate(BaseChartCollection cc) {
        this.chartCollection = (ChartCollection) cc;
    }

    @Override
    public BaseChartCollection update() {
        return this.chartCollection;
    }

    @Override
    public void addStopEditingListener(PropertyChangeListener l) {

    }


    @Override
    public void reset() {

    }
}
