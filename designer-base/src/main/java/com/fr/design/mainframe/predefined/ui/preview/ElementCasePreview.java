package com.fr.design.mainframe.predefined.ui.preview;

import com.fr.base.ScreenResolution;
import com.fr.base.Style;
import com.fr.config.predefined.PredefinedCellStyle;
import com.fr.config.predefined.PredefinedCellStyleConfig;
import com.fr.config.predefined.PredefinedStyle;
import com.fr.design.i18n.Toolkit;
import com.fr.design.layout.FRGUIPaneFactory;
import com.fr.log.FineLoggerFactory;
import com.fr.third.javax.annotation.Nonnull;
import javax.swing.JComponent;
import javax.swing.JPanel;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by kerry on 2020-09-04
 */
public class ElementCasePreview extends ComponentPreviewPane {
    private static final List<String[]> PREVIEW_DATA_LIST = new ArrayList<>();
    private static final String BLANK_CHAR = " ";
    private List<GridRowPane> gridRowPanes;

    static {
        readPreviewData();
    }

    private static void readPreviewData() {
        try {
            InputStream inputStream = ElementCasePreview.class.getResourceAsStream("/com/fr/design/mainframe/predefined/previewData");
            BufferedReader br = new BufferedReader(new InputStreamReader(inputStream,
                    StandardCharsets.UTF_8));
            String lineTxt = null;
            while ((lineTxt = br.readLine()) != null) {
                String[] data = lineTxt.split(BLANK_CHAR);
                PREVIEW_DATA_LIST.add(data);
            }
            br.close();
        } catch (Exception e) {
            FineLoggerFactory.getLogger().error(e.getMessage(), e);
        }
    }


    @Override
    protected JPanel createContentPane() {
        gridRowPanes = new ArrayList<>();
        JPanel jPanel = FRGUIPaneFactory.createY_AXISBoxInnerContainer_S_Pane();
        jPanel.setOpaque(false);
        jPanel.setBackground(null);
        for (int i = 0; i < PREVIEW_DATA_LIST.size(); i++) {
            GridRowPane gridRowPane = new GridRowPane(PREVIEW_DATA_LIST.get(i), Style.DEFAULT_STYLE);
            gridRowPanes.add(gridRowPane);
            jPanel.add(gridRowPane);
        }
        return jPanel;
    }


    public void refresh(PredefinedStyle style) {
        super.refresh(style);
        PredefinedCellStyleConfig cellStyleConfig = style.getCellStyleConfig();
        for (int i = 0; i < gridRowPanes.size(); i++) {
            Style renderStyle = getMainContentStyle(cellStyleConfig);
            if (i == 0) {
                renderStyle = getReportHeaderStyle(cellStyleConfig);
            }
            if (i == PREVIEW_DATA_LIST.size() - 1) {
                renderStyle = getHighLightStyle(cellStyleConfig);
            }
            gridRowPanes.get(i).preview(renderStyle);
        }
    }


    private Style getReportHeaderStyle(PredefinedCellStyleConfig config) {
        return getCellStyle(config, Toolkit.i18nText("Fine-Design_Basic_Predefined_Style_Header"));
    }

    private Style getMainContentStyle(PredefinedCellStyleConfig config) {
        return getCellStyle(config, Toolkit.i18nText("Fine-Design_Basic_Predefined_Style_Main_Text"));
    }

    private Style getHighLightStyle(PredefinedCellStyleConfig config) {
        return getCellStyle(config, Toolkit.i18nText("Fine-Design_Basic_Predefined_Style_Highlight_Text"));
    }

    @Nonnull
    private Style getCellStyle(PredefinedCellStyleConfig config, String styleName) {
        PredefinedCellStyle cellStyle = config.getStyle(styleName);
        if (cellStyle == null) {
            return Style.DEFAULT_STYLE;
        }
        return cellStyle.getStyle();
    }


    class GridRowPane extends JPanel {
        private List<GridPreview> gridPreviews = new ArrayList<>();

        public GridRowPane(String[] data, Style style) {
            this.setOpaque(false);
            this.setBackground(null);
            this.setLayout(FRGUIPaneFactory.createBorderLayout());
            JPanel panel = FRGUIPaneFactory.createNColumnGridInnerContainer_Pane(4, 0, 0);
            panel.setOpaque(false);
            panel.setBackground(null);
            for (String text : data) {
                GridPreview gridPreview = new GridPreview(text);
                gridPreviews.add(gridPreview);
                panel.add(gridPreview);
            }
            this.add(panel, BorderLayout.CENTER);
            preview(style);
        }

        public void preview(Style style) {
            for (GridPreview grid : gridPreviews) {
                grid.preview(style);
            }
        }
    }


    private static class GridPreview extends JComponent {

        private Style style = Style.DEFAULT_STYLE;
        private String value;

        public GridPreview(String value) {
            this.value = value;
            setPreferredSize(new Dimension(125, 30));
        }

        public void preview(Style style) {
            this.style = style;
        }

        public void paint(Graphics g) {
            Graphics2D g2d = (Graphics2D) g;
            int resolution = ScreenResolution.getScreenResolution();

            if (style == Style.DEFAULT_STYLE) {
                Style.paintContent(g2d, value, style, getWidth() - 3, getHeight() - 3, resolution);
                return;
            }

            Style.paintBackground(g2d, style, getWidth(), getHeight());

            Style.paintContent(g2d, value, style, getWidth() - 3, getHeight() - 3, resolution);

            Style.paintBorder(g2d, style, getWidth() , getHeight() );
        }


        @Override
        public Dimension getMinimumSize() {
            return getPreferredSize();
        }
    }

}
