package com.fr.design.mainframe.reuse;

import com.fr.design.notification.SnapChatKey;

/**
 * created by Harrison on 2020/03/24
 **/
public enum SnapChatKeys implements SnapChatKey {

    /**
     * 组件
     */
    COMPONENT("components"),

    /**
     * 模板
     */
    TEMPLATE("template");

    private String sign;

    private static final String PREFIX = "com.fr.component.share";

    SnapChatKeys(String sign) {
        this.sign = sign;
    }

    @Override
    public String calc() {
        return PREFIX + "-" + sign;
    }
}
