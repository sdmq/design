package com.fr.design.mainframe.share.collect;

import com.fr.base.io.XMLReadHelper;
import com.fr.config.MarketConfig;
import com.fr.design.DesignerEnvManager;
import com.fr.design.mainframe.reuse.ComponentReuseNotificationInfo;
import com.fr.form.share.DefaultSharableWidget;
import com.fr.form.share.SharableWidgetProvider;
import com.fr.form.share.constants.ComponentPath;
import com.fr.form.share.group.DefaultShareGroupManager;
import com.fr.form.share.Group;
import com.fr.form.share.group.filter.DirFilter;
import com.fr.form.share.group.filter.ReuFilter;
import com.fr.general.ComparatorUtils;
import com.fr.general.GeneralUtils;
import com.fr.json.JSON;
import com.fr.json.JSONArray;
import com.fr.json.JSONException;
import com.fr.json.JSONFactory;
import com.fr.json.JSONObject;
import com.fr.log.FineLoggerFactory;
import com.fr.plugin.context.PluginContexts;
import com.fr.stable.ProductConstants;
import com.fr.stable.StableUtils;
import com.fr.stable.StringUtils;
import com.fr.stable.xml.XMLPrintWriter;
import com.fr.stable.xml.XMLTools;
import com.fr.stable.xml.XMLable;
import com.fr.stable.xml.XMLableReader;
import com.fr.third.javax.xml.stream.XMLStreamException;
import com.fr.third.joda.time.DateTime;
import com.fr.third.joda.time.Days;
import com.fr.third.org.apache.commons.io.FileUtils;
import com.fr.workspace.WorkContext;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.Iterator;

/**
 * created by Harrison on 2020/03/25
 **/
public class ComponentCollector implements XMLable {
    private static final long ONE_MINUTE = 60 * 1000L;

    private static final int REUSE_INFO_FIRST_POPUP = 1;

    private static final int REUSE_INFO_SECOND_POPUP = 2;

    private static final String SIMPLE_DATE_PATTERN = "yyyy-MM-dd";

    private static final String XML = "ComponentCollector";

    private static final String ACTIVATE_VALUE = "activateValue";

    private static final String COMP_PKT_CLICK = "cmpPktClick";

    private static final String DOWNLOAD_PKT_NUM = "downloadPktNum";

    private static final String DOWNLOAD_CMP = "downloadCmp";

    private static final int ACTIVATE_INITIAL_VALUE = 1;

    private static final String ACTIVATE_DATE = "date";

    private static final String GENERATE_CMP_RECORD_NAME = "name";

    private static final String GENERATE_CMP_RECORD_TYPE = "type";

    private static final String GENERATE_CMP_RECORD_UUID = "uuid";

    private static final String HELP_CONFIG_INFO = "helpConfigInfo";

    private static final String HELP_CONFIG_USE_INFO = "helpConfigUseInfo";

    private static final String TEMPLATE_ID = "templateId";

    private static final String SHOW_COUNT = "showCount";

    private static final String USE_COUNT = "useCount";

    private static final String GROUPING_DETAIL = "groupingDetail";

    private static final String GROUP_NAME = "groupName";

    private static final String CONTAIN_AMOUNT = "containAmount";

    private static final String SEARCH_CONTENT = "searchContent";

    private static final String FILTER_CONTENT = "filterContent";

    private static final String SORT_TYPE = "sortType";

    private static final String MARKET_CLICK = "marketClick";

    private static final String PROMPT_JUMP = "promptJump";

    private static final String TOOLBAR_JUMP = "toolbarJump";

    private static final String POPUP_JUMP = "popupJump";

    private static final String uuid = DesignerEnvManager.getEnvManager().getUUID();

    private int localCmpNumber = 0;

    private int remoteCmpNumber = 0;

    private int generateCmpNumber = 0;

    private int uploadCmpNumber = 0;

    private int cmpBoardClick = 0;

    private int promptJump = 0;

    private int toolbarJump = 0;

    private int popupJump = 0;

    private JSONArray activateRecord = JSONFactory.createJSON(JSON.ARRAY);

    private JSONArray generateCmpRecord = JSONFactory.createJSON(JSON.ARRAY);

    private JSONArray helpConfigInfo = JSONFactory.createJSON(JSON.ARRAY);

    private JSONArray searchContent = JSONFactory.createJSON(JSON.ARRAY);

    private JSONArray filterContent = JSONFactory.createJSON(JSON.ARRAY);

    private JSONArray sortType = JSONFactory.createJSON(JSON.ARRAY);

    private String startTime = StringUtils.EMPTY;

    private String lastTime = StringUtils.EMPTY;

    private JSONArray helpConfigUseInfo = JSONFactory.createJSON(JSON.ARRAY);

    private static class ComponentCollectorHolder {

        private static ComponentCollector collector = new ComponentCollector();
    }

    public static ComponentCollector getInstance() {
        return ComponentCollectorHolder.collector;
    }

    private ComponentCollector() {

        loadFromFile();
        if (StringUtils.isEmpty(startTime)) {
            startTime = DateTime.now().toString(SIMPLE_DATE_PATTERN);
        }
    }

    public void collectGenerateCmpNumber() {

        generateCmpNumber++;
        saveInfo();
    }

    public void collectUploadCmpNumber() {

        uploadCmpNumber++;
        saveInfo();
    }

    public void collectTepMenuEnterClick() {

        saveInfo();
    }

    public void collectCmpBoardClick() {
        collectActivateRecord();
        cmpBoardClick++;
        saveInfo();
    }

    public void collectCmpNumber() {
        int count = 0;
        //默认分组组件数量
        String[] reus = WorkContext.getWorkResource().list(ComponentPath.SHARE_PATH.path(), new ReuFilter());
        count += reus.length;

        //其他分组组件数量
        String[] groups = WorkContext.getWorkResource().list(ComponentPath.SHARE_PATH.path(), new DirFilter());
        for (String groupName : groups) {
            String relativePath = StableUtils.pathJoin(ComponentPath.SHARE_PATH.path(), groupName);
            String[] groupReus = WorkContext.getWorkResource().list(relativePath, new ReuFilter());
            count += groupReus.length;
        }

        if (WorkContext.getCurrent().isLocal()) {
            localCmpNumber = count;
        } else {
            remoteCmpNumber = count;
        }
        saveInfo();
    }

    public void collectCmpPktClick() {
        collectAttrActiveCount(COMP_PKT_CLICK);
        saveInfo();
    }

    public void collectDownloadPktNum() {
        collectAttrActiveCount(DOWNLOAD_PKT_NUM);
        saveInfo();
    }

    public void collectMarkerClick() {
        collectAttrActiveCount(MARKET_CLICK);
    }

    public void clearActiveRecord() {
        String currentDate = DateTime.now().toString(SIMPLE_DATE_PATTERN);
        Iterator<Object> iterator = activateRecord.iterator();
        while (iterator.hasNext()) {
            JSONObject jo = (JSONObject) iterator.next();
            if (!ComparatorUtils.equals(currentDate, jo.getString(ACTIVATE_DATE))) {
                iterator.remove();
            }
        }
    }

    public void collectGenerateCmpRecord(SharableWidgetProvider bindInfo) {
        JSONObject jo = JSONFactory.createJSON(JSON.OBJECT);
        jo.put(GENERATE_CMP_RECORD_NAME, bindInfo.getName())
                .put(GENERATE_CMP_RECORD_TYPE, ((DefaultSharableWidget) bindInfo).getChildClassify())
                .put(GENERATE_CMP_RECORD_UUID, bindInfo.getId());
        generateCmpRecord.add(jo);
    }

    public void clearGenerateCmpRecord() {
        generateCmpRecord = JSONFactory.createJSON(JSON.ARRAY);
    }

    public void collectCmpDownLoad(String uuid) {
        String currentDate = DateTime.now().toString(SIMPLE_DATE_PATTERN);
        for (int i = 0; i < activateRecord.size(); i++) {
            JSONObject jo = activateRecord.getJSONObject(i);
            if (ComparatorUtils.equals(currentDate, jo.getString(ACTIVATE_DATE))) {
                JSONArray downloadComp = jo.containsKey(DOWNLOAD_CMP) ? jo.getJSONArray(DOWNLOAD_CMP) : JSONFactory.createJSON(JSON.ARRAY);
                downloadComp.add(uuid);
                jo.put(DOWNLOAD_CMP, downloadComp);
                saveInfo();
                return;
            }
        }
        JSONObject jo = JSONFactory.createJSON(JSON.OBJECT);
        jo.put(ACTIVATE_DATE, currentDate).put(DOWNLOAD_CMP, JSONFactory.createJSON(JSON.ARRAY).add(uuid));
        activateRecord.add(jo);
        saveInfo();
    }

    public JSONArray getActivateRecord() {
        return activateRecord;
    }

    public JSONArray getGenerateCmpRecord() {
        return generateCmpRecord;
    }

    private void collectActivateRecord() {
        collectAttrActiveCount(ACTIVATE_VALUE);
    }

    private void collectAttrActiveCount(String attrName) {
        String currentDate = DateTime.now().toString(SIMPLE_DATE_PATTERN);
        for (int i = 0; i < activateRecord.size(); i++) {
            JSONObject jo = activateRecord.getJSONObject(i);
            if (ComparatorUtils.equals(currentDate, jo.getString(ACTIVATE_DATE))) {
                int attrNum = jo.getInt(attrName);
                attrNum++;
                jo.put(attrName, attrNum);
                return;
            }
        }
        JSONObject jo = JSONFactory.createJSON(JSON.OBJECT);
        jo.put(ACTIVATE_DATE, currentDate).put(attrName, ACTIVATE_INITIAL_VALUE);
        activateRecord.add(jo);
    }


    private JSONArray getGroupingDetail() {
        JSONArray ja = JSONFactory.createJSON(JSON.ARRAY);
        try {
            DefaultShareGroupManager.getInstance().refresh();
            Group[] groups = DefaultShareGroupManager.getInstance().getAllGroup();
            for(Group group : groups) {
                JSONObject jo = JSONFactory.createJSON(JSON.OBJECT);
                jo.put(GROUP_NAME, group.getGroupName());
                jo.put(CONTAIN_AMOUNT, group.getAllBindInfoList().length);
                ja.add(jo);
            }
        } catch (Exception e) {
            FineLoggerFactory.getLogger().error(e.getMessage());
        }
        return ja;
    }

    public void collectHelpConfigInfo(String templateId, int showCount, int useCount) {
        JSONObject jo = JSONFactory.createJSON(JSON.OBJECT);
        jo.put(TEMPLATE_ID, templateId);
        jo.put(SHOW_COUNT, showCount);
        jo.put(USE_COUNT, useCount);
        helpConfigInfo.add(jo);
        saveInfo();
    }

    public void clearHelpConfigInfo() {
        helpConfigInfo = JSONFactory.createJSON(JSON.ARRAY);
    }

    public void collectSearchContent(String search) {
        searchContent.add(search);
    }

    public void clearSearchContent() {
        searchContent = JSONFactory.createJSON(JSON.ARRAY);
    }

    public void collectFilterContent(String filter) {
        filterContent.add(filter);
        saveInfo();
    }

    public void clearFilterContent() {
        filterContent = JSONFactory.createJSON(JSON.ARRAY);
    }

    public void collectSortType(String type) {
        sortType.add(type);
        saveInfo();
    }

    public void collectPromptJumpWhenJump(){
        if (ComponentReuseNotificationInfo.getInstance().getNotifiedNumber() == REUSE_INFO_FIRST_POPUP) {
            this.promptJump = 1;
            saveInfo();
        }else if(ComponentReuseNotificationInfo.getInstance().getNotifiedNumber() == REUSE_INFO_SECOND_POPUP){
            this.promptJump = 2;
            saveInfo();
        }
    }


    public void collectPromptJumpWhenShow() {
        if (ComponentReuseNotificationInfo.getInstance().getNotifiedNumber() == REUSE_INFO_SECOND_POPUP) {
            this.promptJump = -1;
            saveInfo();
        }
    }

    public void collectToolbarJump() {
        if (this.toolbarJump == 0) {
            this.toolbarJump = 1;
            saveInfo();
        }

    }

    public void collectPopupJump() {
        long currentTime = System.currentTimeMillis();
        long lastGuidePopUpTime = ComponentReuseNotificationInfo.getInstance().getLastGuidePopUpTime();
        if (currentTime - lastGuidePopUpTime <= ONE_MINUTE && this.popupJump == 0) {
            this.popupJump = 1;
            saveInfo();
        }
    }

    public void clearSortType() {
        sortType = JSONFactory.createJSON(JSON.ARRAY);
    }

    private int cmpBoardClickDaily() {

        DateTime dateTime = DateTime.parse(startTime);
        DateTime currTime = DateTime.now();
        int days = (Days.daysBetween(dateTime, currTime).getDays() + 1);
        return cmpBoardClick / days;
    }

    public String getLastTime() {
        return lastTime;
    }

    public void setLastTime(String lastTime) {
        this.lastTime = lastTime;
    }

    /**
     * 保存埋点信息到文件中
     */
    public void saveInfo() {

        try {
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            XMLTools.writeOutputStreamXML(this, out);
            out.flush();
            out.close();
            String fileContent = new String(out.toByteArray(), StandardCharsets.UTF_8);
            FileUtils.writeStringToFile(getInfoFile(), fileContent, StandardCharsets.UTF_8);
        } catch (Exception ex) {
            FineLoggerFactory.getLogger().error(ex.getMessage());
        }
    }

    /**
     * 从文件中读取埋点信息
     */
    private void loadFromFile() {

        if (!getInfoFile().exists()) {
            return;
        }
        XMLableReader reader = null;
        try (InputStream in = new FileInputStream(getInfoFile())) {
            // XMLableReader 还是应该考虑实现 Closable 接口的，这样就能使用 try-with 语句了
            reader = XMLReadHelper.createXMLableReader(in, XMLPrintWriter.XML_ENCODER);
            if (reader == null) {
                return;
            }
            reader.readXMLObject(this);
        } catch (FileNotFoundException e) {
            // do nothing
        } catch (XMLStreamException | IOException e) {
            FineLoggerFactory.getLogger().error(e.getMessage(), e);
        } finally {
            try {
                if (reader != null) {
                    reader.close();
                }
            } catch (XMLStreamException e) {
                FineLoggerFactory.getLogger().error(e.getMessage(), e);
            }
        }
    }


    private File getInfoFile() {

        File file = new File(StableUtils.pathJoin(ProductConstants.getEnvHome(), "component.info"));
        try {
            if (!file.exists()) {
                file.createNewFile();
            }
        } catch (Exception ex) {
            FineLoggerFactory.getLogger().error(ex.getMessage(), ex);
        }
        return file;
    }

    @Override
    public void readXML(XMLableReader reader) {

        String tagName = reader.getTagName();
        if (tagName.equals(XML)) {
            this.cmpBoardClick = reader.getAttrAsInt("cmpBoardClick", 0);
            this.startTime = reader.getAttrAsString("startTime", StringUtils.EMPTY);
            this.lastTime = reader.getAttrAsString("lastTime", StringUtils.EMPTY);
            this.localCmpNumber = reader.getAttrAsInt("localCmpNumber", 0);
            this.remoteCmpNumber = reader.getAttrAsInt("remoteCmpNumber", 0);
            this.generateCmpNumber = reader.getAttrAsInt("generateCmpNumber", 0);
            this.uploadCmpNumber = reader.getAttrAsInt("uploadCmpNumber", 0);

            String activateRecordStr = reader.getAttrAsString("activateRecord", StringUtils.EMPTY);
            activateRecord = parseJSONArray(activateRecordStr);
            String generateCmpRecordStr = reader.getAttrAsString("generateCmpRecord", StringUtils.EMPTY);
            generateCmpRecord = parseJSONArray(generateCmpRecordStr);

            this.helpConfigInfo = parseJSONArray(reader.getAttrAsString(HELP_CONFIG_INFO, StringUtils.EMPTY));
            this.helpConfigUseInfo = parseJSONArray(reader.getAttrAsString(HELP_CONFIG_USE_INFO, StringUtils.EMPTY));
            this.searchContent = parseJSONArray(reader.getAttrAsString(SEARCH_CONTENT,StringUtils.EMPTY));
            this.filterContent = parseJSONArray(reader.getAttrAsString(FILTER_CONTENT, StringUtils.EMPTY));
            this.sortType = parseJSONArray(reader.getAttrAsString(SORT_TYPE, StringUtils.EMPTY));
            this.promptJump = reader.getAttrAsInt(PROMPT_JUMP, 0);
            this.toolbarJump = reader.getAttrAsInt(TOOLBAR_JUMP, 0);
            this.popupJump = reader.getAttrAsInt(POPUP_JUMP, 0);

        }
    }

    private JSONArray parseJSONArray(String value) {
        JSONArray ja;
        try {
            ja = new JSONArray(value);
        } catch (JSONException e) {
            ja = JSONFactory.createJSON(JSON.ARRAY);

        }
        return ja;
    }


    @Override
    public void writeXML(XMLPrintWriter writer) {

        writer.startTAG(XML)
                .attr("cmpBoardClick", cmpBoardClick)
                .attr("startTime", startTime)
                .attr("lastTime", lastTime)
                .attr("localCmpNumber", localCmpNumber)
                .attr("remoteCmpNumber", remoteCmpNumber)
                .attr("uploadCmpNumber", uploadCmpNumber)
                .attr("generateCmpNumber", generateCmpNumber)
                .attr("activateRecord", activateRecord.toString())
                .attr("generateCmpRecord", generateCmpRecord.toString())
                .attr(HELP_CONFIG_INFO, helpConfigInfo.toString())
                .attr(HELP_CONFIG_USE_INFO, helpConfigUseInfo.toString())
                .attr(SEARCH_CONTENT, searchContent.toString())
                .attr(FILTER_CONTENT, filterContent.toString())
                .attr(SORT_TYPE, sortType.toString())
                .attr(PROMPT_JUMP, promptJump)
                .attr(TOOLBAR_JUMP, toolbarJump)
                .attr(POPUP_JUMP, popupJump)
                .end();
    }

    /**
     * 累计信息
     */
    public String generateTotalInfo() {
        collectCmpNumber();
        JSONObject jo = JSONObject.create();
        jo.put("userId", MarketConfig.getInstance().getBBSAttr().getBbsUid());
        jo.put("uuid", uuid);
        jo.put("cmpBoardClickDaily", cmpBoardClickDaily());
        jo.put("pluginVersion", GeneralUtils.readBuildNO());
        jo.put("localCmpNumber", localCmpNumber);
        jo.put("remoteCmpNumber", remoteCmpNumber);
        jo.put("uploadCmpNumber", uploadCmpNumber);
        jo.put("generateCmpNumber", generateCmpNumber);
        jo.put("activateRecord", getValidActivateRecord());
        jo.put("generateCmpRecord", generateCmpRecord.toString());
        jo.put(HELP_CONFIG_INFO, helpConfigInfo.toString());
        jo.put(GROUPING_DETAIL, getGroupingDetail().toString());
        jo.put(SEARCH_CONTENT, searchContent.toString());
        jo.put(FILTER_CONTENT, filterContent.toString());
        jo.put(SORT_TYPE, sortType.toString());
        jo.put("guideInfo", assembleGuideInfo());
        return jo.toString();
    }

    private String assembleGuideInfo() {
        JSONObject jo = JSONFactory.createJSON(JSON.OBJECT);
        jo.put(PROMPT_JUMP, promptJump)
                .put(TOOLBAR_JUMP, toolbarJump)
                .put(POPUP_JUMP, popupJump);
        return jo.toString();
    }

    public String getValidActivateRecord() {
        JSONArray result = JSONFactory.createJSON(JSON.ARRAY);
        String currentDate = DateTime.now().toString(SIMPLE_DATE_PATTERN);
        for (Object o : activateRecord) {
            JSONObject jo = (JSONObject) o;
            if (!ComparatorUtils.equals(currentDate, jo.getString(ACTIVATE_DATE))) {
                result.add(jo);
            }
        }
        return result.toString();
    }

    @Override
    public Object clone() throws CloneNotSupportedException {

        return null;
    }

    public void setHelpConfigUseInfo(String templateId, String widgetId) {
        for (int i = 0; i < helpConfigUseInfo.size(); i++) {
            JSONObject jo = helpConfigUseInfo.getJSONObject(i);
            if (ComparatorUtils.equals(templateId, jo.getString(TEMPLATE_ID))) {
                JSONArray useInfo = jo.getJSONArray(HELP_CONFIG_USE_INFO);
                if (!useInfo.contains(widgetId)) {
                    useInfo.add(widgetId);
                }
                jo.put(HELP_CONFIG_USE_INFO, useInfo);
                return;
            }
        }
        JSONObject jo = JSONFactory.createJSON(JSON.OBJECT);
        JSONArray ja = JSONFactory.createJSON(JSON.ARRAY);
        ja.add(widgetId);
        jo.put(TEMPLATE_ID, templateId);
        jo.put(HELP_CONFIG_USE_INFO, ja);
        helpConfigUseInfo.add(jo);
        saveInfo();
    }

    public JSONArray getHelpConfigUseInfoWithTemplate(String templateId) {
        for (int i = 0; i < helpConfigUseInfo.size(); i++) {
            JSONObject jo = helpConfigUseInfo.getJSONObject(i);
            if (ComparatorUtils.equals(templateId, jo.getString(TEMPLATE_ID))) {
                return jo.getJSONArray(HELP_CONFIG_USE_INFO);
            }
        }
        return JSONFactory.createJSON(JSON.ARRAY);
    }

    public void clear(){
        clearActiveRecord();
        clearGenerateCmpRecord();
        clearFilterContent();
        clearHelpConfigInfo();
        clearSearchContent();
        clearSortType();
        saveInfo();
    }
}
