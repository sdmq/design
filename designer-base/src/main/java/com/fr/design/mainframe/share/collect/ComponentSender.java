package com.fr.design.mainframe.share.collect;

import com.fr.design.mainframe.SiteCenterToken;
import com.fr.general.ComparatorUtils;
import com.fr.general.http.HttpToolbox;
import com.fr.json.JSONObject;
import com.fr.log.FineLoggerFactory;

import java.util.HashMap;
import java.util.Map;

/**
 * created by Harrison on 2020/03/25
 **/
public class ComponentSender {
    
    private static final String CLOUD_REUSE_URL = "https://cloud.fanruan.com/api/monitor/record_of_reusePlugin/single";
    
    public static boolean send() {
        String content = ComponentCollector.getInstance().generateTotalInfo();
        return sendInfo(CLOUD_REUSE_URL, content);
    }
    
    private static boolean sendInfo(String url, String content) {
        
        Map<String, Object> para = new HashMap<>();
        para.put("token", SiteCenterToken.generateToken());
        para.put("content", content);
        
        try {
            String res = HttpToolbox.post(url, para);
            return ComparatorUtils.equals(new JSONObject(res).get("status"), "success");
        } catch (Throwable e) {
            // 客户不需要关心，错误等级为 debug 就行了
            FineLoggerFactory.getLogger().debug(e.getMessage(), e);
        }
        return false;
    }
}
