package com.fr.design.mainframe.template.info;

import com.fr.json.JSONObject;

/**
 * Created by kerry on 2020-05-08
 * @deprecated moved to Cloud Ops plugin
 */
@Deprecated
public interface TemplateOperate {
    /**
     * 获取模板操作类型
     * @return 操作类型
     */
    String getOperateType();

    /**
     * 将模板操作信息转换成json格式
     * @return jsonObject
     */
    JSONObject toJSONObject();
}
