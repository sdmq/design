package com.fr.design.mainframe.template.info;

import com.fr.base.Style;
import com.fr.base.io.BaseBook;
import com.fr.chart.chartattr.ChartCollection;
import com.fr.chartx.attr.ChartProvider;
import com.fr.json.JSONArray;

/**
 * Created by plough on 2017/3/17.
 *
 * @deprecated moved to cloud ops plugin
 */

@Deprecated
public abstract class TemplateProcessInfo<T extends BaseBook> {

    protected T template;

    public TemplateProcessInfo(T template) {
        this.template = template;
    }

    // 获取模板类型。0 代表普通报表，1 代表聚合报表，2 代表表单
    public abstract int getReportType();

    // 获取模板格子数
    public abstract int getCellCount();

    // 获取模板悬浮元素个数
    public abstract int getFloatCount();

    // 获取模板聚合块个数
    public abstract int getBlockCount();

    // 获取模板控件数
    public abstract int getWidgetCount();

    //是否是测试模板
    public abstract boolean isTestTemplate();

    //是否使用参数面板
    public abstract boolean useParaPane();

    //获取组件信息
    public abstract JSONArray getComponentsInfo();

    //获取重用组件数
    public abstract JSONArray getReuseCmpList();


    public abstract void updateTemplateOperationInfo(TemplateOperate templateOption);


    protected boolean isTestCell(Object value, Style style) {
        if (value instanceof ChartCollection && isTestChartCollection((ChartCollection) value)) {
            return true;
        }
        return value == null && Style.getInstance().equals(style);
    }

    protected boolean isTestChartCollection(ChartCollection chartCollection) {
        int chartCount = chartCollection.getChartCount();
        if (chartCount == 0) {
            return true;
        }
        for (int i = 0; i < chartCount; i++) {
            ChartProvider chart = chartCollection.getChart(i, ChartProvider.class);
            if (chart.isTestChart()) {
                return true;
            }
        }
        return false;
    }

}
