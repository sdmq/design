package com.fr.design.mainframe.toolbar;

import com.fr.design.gui.itoolbar.UIToolbar;
import com.fr.design.menu.MenuDef;
import com.fr.design.menu.ShortCut;
import com.fr.design.menu.ToolBarDef;

import javax.swing.JComponent;
import javax.swing.JPanel;

public interface ToolBarMenuDockPlus {
    /**
     * 模板的工具
     *
     * @return 工具
     */
    ToolBarDef[] toolbars4Target();

    /**
     * 文件菜单的子菜单
     *
     * @return 子菜单
     */
    ShortCut[] shortcut4FileMenu();

    /**
     * 目标的菜单
     *
     * @return 菜单
     */
    MenuDef[] menus4Target();

    /**
     * 表单的工具栏
     *
     * @return 表单工具栏
     */
    JPanel[] toolbarPanes4Form();

    /**
     * 表单的工具按钮
     *
     * @return 工具按钮
     */
    JComponent[] toolBarButton4Form();

    /**
     * 权限细粒度状态下的工具面板
     *
     * @return 工具面板
     */
    JComponent toolBar4Authority();

    int getMenuState();

    int getToolBarHeight();

    /**
     * 是否含有工具栏 包含：预览按钮 复制粘贴那一行 模板标签页那一行 字体颜色那一行
     *
     * @return 默认返回true
     */
    default boolean hasToolBarPane() {
        return true;
    }

    /**
     * 往 复制粘贴那一行工具栏 插入 工具栏按钮
     */
    default void insertToCombineUpToolbar(UIToolbar combineUp) {
    }


}