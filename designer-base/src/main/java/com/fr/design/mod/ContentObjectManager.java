package com.fr.design.mod;

import com.fr.base.Formula;
import com.fr.base.headerfooter.FormulaHFElement;
import com.fr.base.present.FormulaPresent;
import com.fr.chart.web.ChartHyperRelateCellLink;
import com.fr.chart.web.ChartHyperRelateFloatLink;
import com.fr.data.SimpleDSColumn;
import com.fr.data.condition.FormulaCondition;
import com.fr.data.impl.FormulaDictionary;
import com.fr.data.impl.NameTableData;
import com.fr.design.mod.impl.repalce.JavaScriptContentReplacer;
import com.fr.design.mod.impl.repalce.VanChartHtmlLabelContentReplacer;
import com.fr.form.main.FormHyperlink;
import com.fr.form.ui.CardSwitchButton;
import com.fr.form.ui.WidgetTitle;
import com.fr.invoke.ClassHelper;
import com.fr.js.JavaScriptImpl;
import com.fr.plugin.chart.base.VanChartHtmlLabel;
import com.fr.report.cell.cellattr.CellExpandAttr;
import com.fr.report.cell.cellattr.CellGUIAttr;
import com.fr.report.cell.cellattr.core.RichChar;
import com.fr.report.cell.cellattr.core.group.DSColumn;
import com.fr.report.cell.cellattr.core.group.FunctionGrouper;
import com.fr.report.cell.cellattr.core.group.SelectCount;
import com.fr.stable.Filter;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import org.jetbrains.annotations.Nullable;

/**
 * 管理所有需要替换内容的对象
 *
 * @author hades
 * @version 10.0
 * Created by hades on 2021/4/28
 */
public class ContentObjectManager {

    private static ContentObjectManager INSTANCE = new ContentObjectManager();

    public static ContentObjectManager getInstance() {
        return INSTANCE;
    }

    /**
     * 放置所有需要替换内容的对象
     */
    private Map<String, Collection<Object>> objectMap;

    private final Set<String> set = new HashSet<>();

    private final Map<String, ContentReplacer> map = new HashMap<>();

    private ContentObjectManager() {
        set.add(Formula.class.getName());
        set.add(JavaScriptImpl.class.getName());
        set.add(ChartHyperRelateCellLink.class.getName());
        set.add(ChartHyperRelateFloatLink.class.getName());
        set.add(VanChartHtmlLabel.class.getName());
        set.add(NameTableData.class.getName());
        set.add(SimpleDSColumn.class.getName());
        set.add(DSColumn.class.getName());
        set.add(FormHyperlink.class.getName());
        set.add(CellExpandAttr.class.getName());
        set.add(FormulaCondition.class.getName());
        set.add(FormulaDictionary.class.getName());
        set.add(FormulaHFElement.class.getName());
        set.add(FormulaPresent.class.getName());
        set.add(RichChar.class.getName());
        set.add(CardSwitchButton.class.getName());
        set.add(CellGUIAttr.class.getName());
        set.add(SelectCount.class.getName());
        set.add(WidgetTitle.class.getName());
        set.add(FunctionGrouper.class.getName());
        map.put(JavaScriptImpl.class.getName(), new JavaScriptContentReplacer());
        map.put(VanChartHtmlLabel.class.getName(), new VanChartHtmlLabelContentReplacer());
    }

    public void searchObject(Object ob) {
        objectMap = ClassHelper.searchObject(ob, set, ModClassFilter.getInstance());
    }

    public void searchObject(Object ob, Filter<String> filter) {
        objectMap = ClassHelper.searchObject(ob, set, filter);
    }

    public void searchObject(Object ob, Set<String> set, Filter<String> filter) {
        objectMap = ClassHelper.searchObject(ob, set, filter);
    }
    public void clearObject() {
        if (objectMap != null) {
            objectMap.clear();
        }
        objectMap = null;
    }

    @Nullable
    public Map<String, Collection<Object>> getObjectMap() {
        return objectMap;
    }

    public boolean needContentTip(Object ob, Set<String> nameSet) {
        objectMap = ClassHelper.searchObject(ob, set, ModClassFilter.getInstance());
        for (Map.Entry<String, Collection<Object>> entry : objectMap.entrySet()) {
            for (Object o : entry.getValue()) {
                for (String name : nameSet) {
                    ContentReplacer contentReplacer = map.get(entry.getKey());
                    if (contentReplacer!= null && contentReplacer.contain(o, name)) {
                        clearObject();
                        return true;
                    }
                }
            }
        }
        clearObject();
        return false;
    }

}
