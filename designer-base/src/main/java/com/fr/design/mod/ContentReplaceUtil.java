package com.fr.design.mod;

import com.fr.parser.FRFormulaTransformer;
import com.fr.stable.StringUtils;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author hades
 * @version 10.0
 * Created by hades on 2021/5/31
 */
public class ContentReplaceUtil {

    public static final String EQ_STRING = "=";

    public static final String LEFT_BRACKET = "${";

    public static final String RIGHT_BRACKET = "}";

    public static String replaceContent(String content, String oldName, String newName) {
        String oldNameUpper = oldName.toUpperCase();
        Matcher m  = Pattern.compile(oldName + "|" + oldNameUpper).matcher(content);
        StringBuilder sb = new StringBuilder();
        int last = 0;
        while (m.find()) {
            sb.append(content, last, m.start());
            if (oldNameUpper.equals(m.group())) {
                // 处理大写情况
                sb.append(newName.toUpperCase());
            } else {
                // 默认情况
                sb.append(newName);
            }
            last = m.end();
        }
        sb.append(content.substring(last));

        return sb.toString();
    }

    private static String generateStr(String str) {
        return  "\"" + str + "\"";
    }

    public static String getFormulaPureContent(String content) {
        if (content.startsWith(EQ_STRING)) {
            return content.substring(1);
        } else {
            return content;
        }
    }

    /**
     * 通过公式编辑框框输入的且带有"="
     *
     * @param content
     * @param oldName
     * @param newName
     * @return
     */
    public static String replaceFormulaContent4WidgetName(String content, String oldName, String newName) {
        if (StringUtils.isNotEmpty(content)) {
           return replaceFormulaContent4WidgetName0(content, oldName, newName);
        }
        return content;
    }

    public static String replaceFormulaContent4TableDataName(String content, String oldName, String newName) {
        if (StringUtils.isNotEmpty(content)) {
            return replaceFormulaContent4TableDataName0(content, oldName, newName);
        }
        return content;
    }

    private static String replaceFormulaContent4WidgetName0(String content, String oldName, String newName) {
        content = getFormulaPureContent(content);
        FRFormulaTransformer frFormulaTransformer = new FRFormulaTransformer();
        frFormulaTransformer.addRenamedWidget(oldName, newName);
        return EQ_STRING + frFormulaTransformer.transform(content);
    }

    private static String replaceFormulaContent4TableDataName0(String content, String oldName, String newName) {
        content = getFormulaPureContent(content);
        FRFormulaTransformer frFormulaTransformer = new FRFormulaTransformer();
        frFormulaTransformer.addRenamedDataset(oldName, newName);
        return EQ_STRING + frFormulaTransformer.transform(content);
    }

    public static String getRichCharFormulaPureContent(String content) {
        if (content.startsWith(LEFT_BRACKET)) {
            return content.substring(LEFT_BRACKET.length() + 1, content.length() - 1);
        } else {
            return content;
        }
    }

    public static String replaceRichCharFormulaContent4TableDataName(String content, String oldName, String newName) {
        if (StringUtils.isNotEmpty(content)) {
            content = getRichCharFormulaPureContent(content);
            FRFormulaTransformer frFormulaTransformer = new FRFormulaTransformer();
            frFormulaTransformer.addRenamedDataset(oldName, newName);
            return LEFT_BRACKET + EQ_STRING + frFormulaTransformer.transform(content) + RIGHT_BRACKET;
        }
        return content;
    }

    public static String replaceRichCharFormulaContent4WidgetName(String content, String oldName, String newName) {
        if (StringUtils.isNotEmpty(content)) {
            content = getRichCharFormulaPureContent(content);
            FRFormulaTransformer frFormulaTransformer = new FRFormulaTransformer();
            frFormulaTransformer.addRenamedWidget(oldName, newName);
            return LEFT_BRACKET + EQ_STRING + frFormulaTransformer.transform(content) + RIGHT_BRACKET;
        }
        return content;
    }

    private static boolean isFormula(String content) {
        return StringUtils.isNotEmpty(content) && content.startsWith(EQ_STRING);
    }

    /**
     * 适用于替换非显示使用的公式的地方 有可能只是文本 需要先判断下
     * 可通过使用位置是否使用公式编辑框区分
     *
     * @param content
     * @param oldName
     * @param newName
     * @return
     */
    public static String replaceFormulaString4WidgetName(String content, String oldName, String newName) {
        if (isFormula(content)) {
            return replaceFormulaContent4WidgetName0(content, oldName, newName);
        }
        return content;
    }

    public static String replaceFormulaString4TableDataName(String content, String oldName, String newName ) {
        if (isFormula(content)) {
            return replaceFormulaContent4TableDataName0(content, oldName, newName);
        }
        return content;
    }

    /**
     * 替换纯公式内容 无 "="
     *
     * @param content
     * @param oldName
     * @param newName
     * @return
     */
    public static String replacePureFormula4WidgetName(String content, String oldName, String newName ) {
        if (StringUtils.isNotEmpty(content)) {
            FRFormulaTransformer frFormulaTransformer = new FRFormulaTransformer();
            frFormulaTransformer.addRenamedWidget(oldName, newName);
            return frFormulaTransformer.transform(content);
        }
        return content;
    }

    public static String replacePureFormula4TableDataName(String content, String oldName, String newName ) {
        if (StringUtils.isNotEmpty(content)) {
            FRFormulaTransformer frFormulaTransformer = new FRFormulaTransformer();
            frFormulaTransformer.addRenamedDataset(oldName, newName);
            return frFormulaTransformer.transform(content);
        }
        return content;
    }

    public static boolean containsName(String content, String name) {
        if (StringUtils.isNotEmpty(content)) {
            return content.contains(name);
        }
        return false;
    }


}
