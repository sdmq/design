package com.fr.design.mod;

/**
 *
 * @author hades
 * @version 10.0
 * Created by hades on 2021/5/27
 */
public interface ContentReplacer<T> {

    void replace(T t, String oldName, String newName);


    default boolean contain(T t, String name) {
        return false;
    }

}
