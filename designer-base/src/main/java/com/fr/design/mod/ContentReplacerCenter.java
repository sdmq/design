package com.fr.design.mod;

import com.fr.design.mod.bean.ChangeItem;
import com.fr.design.mod.bean.ContentChangeItem;
import com.fr.design.mod.event.TableDataModifyEvent;
import com.fr.design.mod.event.WidgetNameModifyEvent;
import com.fr.design.mod.impl.change.ChartHyperRelateCellLinkContentChange;
import com.fr.design.mod.impl.change.ChartHyperRelateFloatLinkContentChange;
import com.fr.design.mod.impl.change.DSColumnContentChange;
import com.fr.design.mod.impl.change.FormHyperlinkContentChange;
import com.fr.design.mod.impl.change.formula.CardSwitchButtonContentChange;
import com.fr.design.mod.impl.change.formula.CellExpandAttrContentChange;
import com.fr.design.mod.impl.change.formula.CellGUIAttrContentChange;
import com.fr.design.mod.impl.change.formula.FormulaConditionContentChange;
import com.fr.design.mod.impl.change.formula.FormulaContentChange;
import com.fr.design.mod.impl.change.JavaScriptContentChange;
import com.fr.design.mod.impl.change.NameTableDataContentChange;
import com.fr.design.mod.impl.change.SimpleDSColumnContentChange;
import com.fr.design.mod.impl.change.VanChartHtmlLabelContentChange;
import com.fr.design.mod.impl.change.formula.FormulaDictionaryContentChange;
import com.fr.design.mod.impl.change.formula.FormulaHFElementContentChange;
import com.fr.design.mod.impl.change.formula.FormulaPresentContentChange;
import com.fr.design.mod.impl.change.formula.FunctionGrouperContentChange;
import com.fr.design.mod.impl.change.formula.RichCharContentChange;
import com.fr.design.mod.impl.change.formula.SelectCountContentChange;
import com.fr.design.mod.impl.change.formula.WidgetTitleContentChange;
import com.fr.event.Event;
import com.fr.event.EventDispatcher;
import com.fr.event.Listener;
import com.fr.log.FineLoggerFactory;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 默认联动内容替换器实现
 *
 * 当前替换顺序：组件名-> 数据集名
 *
 * @author hades
 * @version 10.0
 * Created by hades on 2021/5/27
 */
public class ContentReplacerCenter {

    private static final ContentReplacerCenter INSTANCE = new ContentReplacerCenter();

    public static ContentReplacerCenter getInstance() {
        return INSTANCE;
    }

    private List<ContentChange> contentChangeList = new ArrayList<>();
    private Map<ChangeItem, ContentChangeItem> itemsMap = new HashMap<>();

    private ContentReplacerCenter() {

        EventDispatcher.listen(WidgetNameModifyEvent.INSTANCE, new Listener<ContentChangeItem>() {
            @Override
            public void on(Event event, ContentChangeItem param) {
                if (param.getChangeMap().isEmpty()) {
                    return;
                }
                itemsMap.put(param.getChangeItem(), param);
            }
        });

        EventDispatcher.listen(TableDataModifyEvent.INSTANCE, new Listener<ContentChangeItem>() {
            @Override
            public void on(Event event, ContentChangeItem param) {
                itemsMap.put(param.getChangeItem(), param);
                long start = System.currentTimeMillis();
                ContentObjectManager.getInstance().searchObject(param.getObject());
                FineLoggerFactory.getLogger().debug("search object spend {} ms", (System.currentTimeMillis() - start));
                FineLoggerFactory.getLogger().debug("search result: {}", ContentObjectManager.getInstance().getObjectMap() == null
                                                                            ? null : ContentObjectManager.getInstance().getObjectMap().keySet());
                List<ContentChangeItem> itemsCopy = new ArrayList<>(itemsMap.values());
                itemsMap.clear();
                onRename(itemsCopy, contentChangeList);
            }
        });

    }

    public void register() {
        contentChangeList.add(new ChartHyperRelateCellLinkContentChange());
        contentChangeList.add(new ChartHyperRelateFloatLinkContentChange());
        contentChangeList.add(new FormulaContentChange());
        contentChangeList.add(new JavaScriptContentChange());
        contentChangeList.add(new VanChartHtmlLabelContentChange());
        contentChangeList.add(new NameTableDataContentChange());
        contentChangeList.add(new SimpleDSColumnContentChange());
        contentChangeList.add(new DSColumnContentChange());
        contentChangeList.add(new FormHyperlinkContentChange());
        contentChangeList.add(new CellExpandAttrContentChange());
        contentChangeList.add(new FormulaConditionContentChange());
        contentChangeList.add(new FormulaDictionaryContentChange());
        contentChangeList.add(new FormulaHFElementContentChange());
        contentChangeList.add(new FormulaPresentContentChange());
        contentChangeList.add(new RichCharContentChange());
        contentChangeList.add(new CardSwitchButtonContentChange());
        contentChangeList.add(new CellGUIAttrContentChange());
        contentChangeList.add(new SelectCountContentChange());
        contentChangeList.add(new WidgetTitleContentChange());
        contentChangeList.add(new FunctionGrouperContentChange());
    }

    private void onRename(List<ContentChangeItem> contentChangeItemList, List<ContentChange> contentChangeList) {
        Map<String, Collection<Object>> objectMap = ContentObjectManager.getInstance().getObjectMap();
        if (objectMap != null) {
            long start = System.currentTimeMillis();
            for (ContentChange contentChange : contentChangeList) {
                Collection<Object> objects = objectMap.get(contentChange.type());
                // 所有需要处理的js等对象
                if (objects != null) {
                    for (Object ob : objects) {
                        fireChange(ob, contentChange, contentChangeItemList);
                    }
                }
            }
            objectMap.clear();
            FineLoggerFactory.getLogger().debug("replace all content spend {} ms", (System.currentTimeMillis() - start));
        }
    }

    private void fireChange(Object o, ContentChange contentChange, List<ContentChangeItem> itemList) {
        // 当前两项存在两项： 数据集名称和组件名称
        for (ContentChangeItem contentChangeItem : itemList) {
            Map<ChangeItem, ContentReplacer> map =  contentChange.changeInfo();
            if (map.containsKey(contentChangeItem.getChangeItem())) {
                // 具体重命名取决于复用组件存在多少个组件或数据集
                for (Map.Entry<String, String> entry : contentChangeItem.getChangeMap().entrySet()) {
                    map.get(contentChangeItem.getChangeItem()).replace(o, entry.getKey(), entry.getValue());
                }
            }
        }
    }
}
