package com.fr.design.mod.impl.change;

import com.fr.chart.web.ChartHyperRelateCellLink;

/**
 * @author hades
 * @version 10.0
 * Created by hades on 2021/5/28
 */
public class ChartHyperRelateCellLinkContentChange extends ChartHyperRelateLinkContentChange {

    @Override
    public String type() {
        return ChartHyperRelateCellLink.class.getName();
    }
}
