package com.fr.design.mod.impl.change;

import com.fr.chart.web.ChartHyperRelateFloatLink;

/**
 * @author hades
 * @version 10.0
 * Created by hades on 2021/5/28
 */
public class ChartHyperRelateFloatLinkContentChange extends ChartHyperRelateLinkContentChange{

    @Override
    public String type() {
        return ChartHyperRelateFloatLink.class.getName();
    }
}
