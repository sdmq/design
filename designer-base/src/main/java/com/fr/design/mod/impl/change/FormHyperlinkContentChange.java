package com.fr.design.mod.impl.change;

import com.fr.design.mod.ContentChange;
import com.fr.design.mod.ContentReplacer;
import com.fr.design.mod.bean.ChangeItem;
import com.fr.design.mod.impl.repalce.FormHyperlinkContentReplacer;
import com.fr.form.main.FormHyperlink;
import java.util.HashMap;
import java.util.Map;

/**
 * @author hades
 * @version 10.0
 * Created by hades on 2021/6/2
 */
public class FormHyperlinkContentChange implements ContentChange<FormHyperlink> {

    private final Map<ChangeItem, ContentReplacer<FormHyperlink>> map;

    public FormHyperlinkContentChange() {
        map = new HashMap<>();
        map.put(ChangeItem.WIDGET_NAME, new FormHyperlinkContentReplacer());
    }

    @Override
    public String type() {
        return FormHyperlink.class.getName();
    }

    @Override
    public Map<ChangeItem, ContentReplacer<FormHyperlink>> changeInfo() {
        return map;
    }
}
