package com.fr.design.mod.impl.change;

import com.fr.data.impl.NameTableData;
import com.fr.design.mod.ContentChange;
import com.fr.design.mod.ContentReplacer;
import com.fr.design.mod.bean.ChangeItem;
import com.fr.design.mod.impl.repalce.NameTableDataContentReplacer;
import java.util.HashMap;
import java.util.Map;

/**
 * @author hades
 * @version 10.0
 * Created by hades on 2021/6/2
 */
public class NameTableDataContentChange implements ContentChange<NameTableData> {

    private final Map<ChangeItem, ContentReplacer<NameTableData>> map;

    public NameTableDataContentChange() {
        map = new HashMap<>();
        map.put(ChangeItem.TABLE_DATA_NAME, new NameTableDataContentReplacer());
    }

    @Override
    public String type() {
        return NameTableData.class.getName();
    }

    @Override
    public Map<ChangeItem, ContentReplacer<NameTableData>> changeInfo() {
        return map;
    }
}
