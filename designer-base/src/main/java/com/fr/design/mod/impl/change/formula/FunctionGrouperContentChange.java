package com.fr.design.mod.impl.change.formula;

import com.fr.design.mod.ContentChange;
import com.fr.design.mod.ContentReplacer;
import com.fr.design.mod.bean.ChangeItem;
import com.fr.design.mod.impl.repalce.FormulaReplacer;
import com.fr.report.cell.cellattr.core.group.FunctionGrouper;
import java.util.HashMap;
import java.util.Map;

/**
 * @author hades
 * @version 10.0
 * Created by hades on 2021/6/3
 */
public class FunctionGrouperContentChange implements ContentChange<FunctionGrouper> {

    private final Map<ChangeItem, ContentReplacer<FunctionGrouper>> map;

    public FunctionGrouperContentChange() {
        map = new HashMap<>();
        map.put(ChangeItem.WIDGET_NAME, FormulaReplacer.FunctionGrouper4WidgetNameContentReplacer);
        map.put(ChangeItem.TABLE_DATA_NAME, FormulaReplacer.FunctionGrouper4TableDataNameContentReplacer);
    }

    @Override
    public String type() {
        return FunctionGrouper.class.getName();
    }

    @Override
    public Map<ChangeItem, ContentReplacer<FunctionGrouper>> changeInfo() {
        return map;
    }
}
