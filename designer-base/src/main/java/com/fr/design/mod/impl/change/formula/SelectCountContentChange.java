package com.fr.design.mod.impl.change.formula;

import com.fr.design.mod.ContentChange;
import com.fr.design.mod.ContentReplacer;
import com.fr.design.mod.bean.ChangeItem;
import com.fr.design.mod.impl.repalce.FormulaReplacer;
import com.fr.report.cell.cellattr.core.group.SelectCount;
import java.util.HashMap;
import java.util.Map;

/**
 * @author hades
 * @version 10.0
 * Created by hades on 2021/6/3
 */
public class SelectCountContentChange implements ContentChange<SelectCount> {

    private final Map<ChangeItem, ContentReplacer<SelectCount>> map;

    public SelectCountContentChange() {
        map = new HashMap<>();
        map.put(ChangeItem.WIDGET_NAME, FormulaReplacer.SelectCount4WidgetNameContentReplacer);
        map.put(ChangeItem.TABLE_DATA_NAME, FormulaReplacer.SelectCount4TableDataNameContentReplacer);
    }

    @Override
    public String type() {
        return SelectCount.class.getName();
    }

    @Override
    public Map<ChangeItem, ContentReplacer<SelectCount>> changeInfo() {
        return map;
    }
}
