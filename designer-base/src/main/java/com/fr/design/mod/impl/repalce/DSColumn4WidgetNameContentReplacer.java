package com.fr.design.mod.impl.repalce;

import com.fr.design.mod.ContentReplaceUtil;
import com.fr.design.mod.ContentReplacer;
import com.fr.report.cell.cellattr.core.group.DSColumn;

/**
 * @author hades
 * @version 10.0
 * Created by hades on 2021/6/3
 */
public class DSColumn4WidgetNameContentReplacer implements ContentReplacer<DSColumn> {

    @Override
    public void replace(DSColumn dsColumn, String oldName, String newName) {
        dsColumn.setResult(ContentReplaceUtil.replacePureFormula4WidgetName(dsColumn.getResult(), oldName, newName));
        dsColumn.setSortFormula(ContentReplaceUtil.replacePureFormula4WidgetName(dsColumn.getSortFormula(), oldName, newName));
    }
}
