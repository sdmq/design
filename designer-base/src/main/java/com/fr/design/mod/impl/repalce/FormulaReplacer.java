package com.fr.design.mod.impl.repalce;

import com.fr.base.Formula;
import com.fr.base.headerfooter.FormulaHFElement;
import com.fr.base.present.FormulaPresent;
import com.fr.data.condition.FormulaCondition;
import com.fr.data.impl.FormulaDictionary;
import com.fr.design.mod.ContentReplaceUtil;
import com.fr.design.mod.ContentReplacer;
import com.fr.form.ui.CardSwitchButton;
import com.fr.form.ui.WidgetTitle;
import com.fr.report.cell.cellattr.CellExpandAttr;
import com.fr.report.cell.cellattr.CellGUIAttr;
import com.fr.report.cell.cellattr.core.RichChar;
import com.fr.report.cell.cellattr.core.group.FunctionGrouper;
import com.fr.report.cell.cellattr.core.group.SelectCount;

/**
 * 持有公式内容对象汇总
 *
 * @author hades
 * @version 10.0
 * Created by hades on 2021/6/2
 */
public class FormulaReplacer {

    /**
     * 扩展后排序公式
     */
    public static final ContentReplacer<CellExpandAttr> CellExpandAttr4TableDataNameContentReplacer = new ContentReplacer<CellExpandAttr>() {
        @Override
        public void replace(CellExpandAttr cellExpandAttr, String oldName, String newName) {
            cellExpandAttr.setSortFormula(ContentReplaceUtil.replaceFormulaContent4TableDataName(cellExpandAttr.getSortFormula(), oldName, newName));
        }
    };

    public static final ContentReplacer<CellExpandAttr> CellExpandAttr4WidgetNameContentReplacer = new ContentReplacer<CellExpandAttr>() {
        @Override
        public void replace(CellExpandAttr cellExpandAttr, String oldName, String newName) {
            cellExpandAttr.setSortFormula(ContentReplaceUtil.replaceFormulaContent4WidgetName(cellExpandAttr.getSortFormula(), oldName, newName));
        }
    };

    /**
     * 典型单元格公式
     */
    public static final ContentReplacer<Formula> Formula4TableDataNameContentReplacer = new ContentReplacer<Formula>() {
        @Override
        public void replace(Formula formula, String oldName, String newName) {
            formula.setContent(ContentReplaceUtil.replaceFormulaContent4TableDataName(formula.getPureContent(), oldName, newName));
        }
    };


    public static final ContentReplacer<Formula> Formula4WidgetNameContentReplacer = new ContentReplacer<Formula>() {
        @Override
        public void replace(Formula formula, String oldName, String newName) {
            formula.setContent(ContentReplaceUtil.replaceFormulaContent4WidgetName(formula.getPureContent(), oldName, newName));
        }
    };

    /**
     * 公式条件
     */
    public static final ContentReplacer<FormulaCondition> FormulaCondition4TableDataNameContentReplacer = new ContentReplacer<FormulaCondition>() {
        @Override
        public void replace(FormulaCondition formulaCondition, String oldName, String newName) {
            formulaCondition.setFormula(ContentReplaceUtil.replaceFormulaContent4TableDataName(formulaCondition.getFormula(), oldName, newName));
        }
    };

    public static final ContentReplacer<FormulaCondition> FormulaCondition4WidgetNameContentReplacer = new ContentReplacer<FormulaCondition>() {
        @Override
        public void replace(FormulaCondition formulaCondition, String oldName, String newName) {
            formulaCondition.setFormula(ContentReplaceUtil.replaceFormulaContent4WidgetName(formulaCondition.getFormula(), oldName, newName));
        }
    };

    /**
     * 数据字典——公式
     */

    public static final ContentReplacer<FormulaDictionary> FormulaDictionary4TableDataNameContentReplacer = new ContentReplacer<FormulaDictionary>() {
        @Override
        public void replace(FormulaDictionary formulaDictionary, String oldName, String newName) {
            formulaDictionary.setExcuteFormula(ContentReplaceUtil.replaceFormulaContent4TableDataName(formulaDictionary.getExcuteFormula(), oldName, newName));
            formulaDictionary.setProduceFormula(ContentReplaceUtil.replaceFormulaContent4TableDataName(formulaDictionary.getProduceFormula(), oldName, newName));
        }
    };

    public static final ContentReplacer<FormulaDictionary> FormulaDictionary4WidgetNameContentReplacer = new ContentReplacer<FormulaDictionary>() {
        @Override
        public void replace(FormulaDictionary formulaDictionary, String oldName, String newName) {
            formulaDictionary.setExcuteFormula(ContentReplaceUtil.replaceFormulaContent4WidgetName(formulaDictionary.getExcuteFormula(), oldName, newName));
            formulaDictionary.setProduceFormula(ContentReplaceUtil.replaceFormulaContent4WidgetName(formulaDictionary.getProduceFormula(), oldName, newName));
        }
    };

    /**
     * 页面/页脚——公式
     */

    public static final ContentReplacer<FormulaHFElement> FormulaHFElement4TableDataNameContentReplacer = new ContentReplacer<FormulaHFElement>() {
        @Override
        public void replace(FormulaHFElement formulaHFElement, String oldName, String newName) {
            formulaHFElement.setFormulaContent(ContentReplaceUtil.replaceFormulaContent4TableDataName(formulaHFElement.getFormulaContent(), oldName, newName));
        }
    };

    public static final ContentReplacer<FormulaHFElement> FormulaHFElement4WidgetNameContentReplacer = new ContentReplacer<FormulaHFElement>() {
        @Override
        public void replace(FormulaHFElement formulaHFElement, String oldName, String newName) {
            formulaHFElement.setFormulaContent(ContentReplaceUtil.replaceFormulaContent4WidgetName(formulaHFElement.getFormulaContent(), oldName, newName));
        }
    };

    /**
     * 公式形态
     */

    public static final ContentReplacer<FormulaPresent> FormulaPresent4TableDataNameContentReplacer = new ContentReplacer<FormulaPresent>() {
        @Override
        public void replace(FormulaPresent formulaPresent, String oldName, String newName) {
            formulaPresent.setFormulaContent(ContentReplaceUtil.replaceFormulaContent4TableDataName(formulaPresent.getFormulaContent(), oldName, newName));
        }
    };

    public static final ContentReplacer<FormulaPresent> FormulaPresent4WidgetNameContentReplacer = new ContentReplacer<FormulaPresent>() {
        @Override
        public void replace(FormulaPresent formulaPresent, String oldName, String newName) {
            formulaPresent.setFormulaContent(ContentReplaceUtil.replaceFormulaContent4WidgetName(formulaPresent.getFormulaContent(), oldName, newName));
        }
    };

    /**
     * 富文本公式
     */
    public static final ContentReplacer<RichChar> RichChar4TableDataNameContentReplacer = new ContentReplacer<RichChar>() {
        @Override
        public void replace(RichChar richChar, String oldName, String newName) {
            if (richChar.isFormula()) {
                richChar.setText(ContentReplaceUtil.replaceRichCharFormulaContent4TableDataName(richChar.getText(), oldName, newName));
            }
        }
    };

    public static final ContentReplacer<RichChar> RichChar4WidgetNameContentReplacer = new ContentReplacer<RichChar>() {
        @Override
        public void replace(RichChar richChar, String oldName, String newName) {
            if (richChar.isFormula()) {
                richChar.setText(ContentReplaceUtil.replaceRichCharFormulaContent4WidgetName(richChar.getText(), oldName, newName));
            }
        }
    };

    /**
     * 公式分组
     *
     */
    public static final ContentReplacer<FunctionGrouper> FunctionGrouper4TableDataNameContentReplacer = new ContentReplacer<FunctionGrouper>() {
        @Override
        public void replace(FunctionGrouper functionGrouper, String oldName, String newName) {
            functionGrouper.setFormulaContent(ContentReplaceUtil.replacePureFormula4TableDataName(functionGrouper.getFormulaContent(), oldName, newName));
        }
    };

    public static final ContentReplacer<FunctionGrouper> FunctionGrouper4WidgetNameContentReplacer = new ContentReplacer<FunctionGrouper>() {
        @Override
        public void replace(FunctionGrouper functionGrouper, String oldName, String newName) {
            functionGrouper.setFormulaContent(ContentReplaceUtil.replacePureFormula4WidgetName(functionGrouper.getFormulaContent(), oldName, newName));
        }
    };

    /**
     * 结果集筛选
     */
    public static final ContentReplacer<SelectCount> SelectCount4TableDataNameContentReplacer = new ContentReplacer<SelectCount>() {
        @Override
        public void replace(SelectCount selectCount, String oldName, String newName) {
            selectCount.setFormulaCount(ContentReplaceUtil.replacePureFormula4TableDataName(selectCount.getFormulaCount(), oldName, newName));
        }
    };
    public static final ContentReplacer<SelectCount> SelectCount4WidgetNameContentReplacer = new ContentReplacer<SelectCount>() {
        @Override
        public void replace(SelectCount selectCount, String oldName, String newName) {
            selectCount.setFormulaCount(ContentReplaceUtil.replacePureFormula4WidgetName(selectCount.getFormulaCount(), oldName, newName));
        }
    };

    /**
     * tab块标题
     */
    public static final ContentReplacer<CardSwitchButton> CardSwitchButton4TableDataNameContentReplacer = new ContentReplacer<CardSwitchButton>() {
        @Override
        public void replace(CardSwitchButton cardSwitchButton, String oldName, String newName) {
            cardSwitchButton.setText(ContentReplaceUtil.replaceFormulaString4TableDataName(cardSwitchButton.getText(), oldName, newName));
        }
    };

    public static final ContentReplacer<CardSwitchButton> CardSwitchButton4WidgetNameContentReplacer = new ContentReplacer<CardSwitchButton>() {
        @Override
        public void replace(CardSwitchButton cardSwitchButton, String oldName, String newName) {
            cardSwitchButton.setText(ContentReplaceUtil.replaceFormulaString4WidgetName(cardSwitchButton.getText(), oldName, newName));
        }
    };

    /**
     * 悬浮提示
     */
    public static final ContentReplacer<CellGUIAttr> CellGUIAttr4TableDataNameContentReplacer = new ContentReplacer<CellGUIAttr>() {
        @Override
        public void replace(CellGUIAttr cellGUIAttr, String oldName, String newName) {
            cellGUIAttr.setTooltipText(ContentReplaceUtil.replaceFormulaString4TableDataName(cellGUIAttr.getTooltipText(), oldName, newName));
        }
    };

    public static final ContentReplacer<CellGUIAttr> CellGUIAttr4WidgetNameContentReplacer = new ContentReplacer<CellGUIAttr>() {
        @Override
        public void replace(CellGUIAttr cellGUIAttr, String oldName, String newName) {
            cellGUIAttr.setTooltipText(ContentReplaceUtil.replaceFormulaString4WidgetName(cellGUIAttr.getTooltipText(), oldName, newName));
        }
    };

    /**
     * 图表/报表块等标题
     */
    public static final ContentReplacer<WidgetTitle> WidgetTitle4TableDataNameContentReplacer = new ContentReplacer<WidgetTitle>() {
        @Override
        public void replace(WidgetTitle widgetTitle, String oldName, String newName) {
            if (widgetTitle.getTextObject() instanceof String) {
                String content = (String) widgetTitle.getTextObject();
                widgetTitle.setTextObject(ContentReplaceUtil.replaceFormulaString4TableDataName(content, oldName, newName));
            }
        }
    };

    public static final ContentReplacer<WidgetTitle> WidgetTitle4WidgetNameContentReplacer = new ContentReplacer<WidgetTitle>() {
        @Override
        public void replace(WidgetTitle widgetTitle, String oldName, String newName) {
            if (widgetTitle.getTextObject() instanceof String) {
                String content = (String) widgetTitle.getTextObject();
                widgetTitle.setTextObject(ContentReplaceUtil.replaceFormulaString4WidgetName(content, oldName, newName));
            }
        }
    };

}
