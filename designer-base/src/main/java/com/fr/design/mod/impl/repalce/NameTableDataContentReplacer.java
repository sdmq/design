package com.fr.design.mod.impl.repalce;

import com.fr.data.impl.NameTableData;
import com.fr.design.mod.ContentReplacer;

/**
 * @author hades
 * @version 10.0
 * Created by hades on 2021/6/2
 */
public class NameTableDataContentReplacer implements ContentReplacer<NameTableData> {

    @Override
    public void replace(NameTableData nameTableData, String oldName, String newName) {
        nameTableData.rename(oldName, newName);
    }
}
