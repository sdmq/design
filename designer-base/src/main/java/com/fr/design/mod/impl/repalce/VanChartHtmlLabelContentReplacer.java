package com.fr.design.mod.impl.repalce;

import com.fr.design.mod.ContentReplacer;
import com.fr.design.mod.ContentReplaceUtil;
import com.fr.plugin.chart.base.VanChartHtmlLabel;
import com.fr.stable.StringUtils;

/**
 * @author hades
 * @version 10.0
 * Created by hades on 2021/5/31
 */
public class VanChartHtmlLabelContentReplacer implements ContentReplacer<VanChartHtmlLabel> {

    @Override
    public void replace(VanChartHtmlLabel vanChartHtmlLabel, String oldName, String newName) {
        if (StringUtils.isNotEmpty(vanChartHtmlLabel.getCustomText())) {
            vanChartHtmlLabel.setCustomText(
                    ContentReplaceUtil.replaceContent(vanChartHtmlLabel.getCustomText(), oldName, newName));
        }
    }

    @Override
    public boolean contain(VanChartHtmlLabel vanChartHtmlLabel, String name) {
        return ContentReplaceUtil.containsName(vanChartHtmlLabel.getCustomText(), name);
    }
}
