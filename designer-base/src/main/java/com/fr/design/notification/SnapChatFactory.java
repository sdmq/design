package com.fr.design.notification;

import com.fr.plugin.context.PluginContext;

/**
 * @Author: Yuan.Wang
 * @Date: 2020/10/10
 */
public class SnapChatFactory {
    public static SnapChat createSnapChat(boolean defaultStatus, SnapChatKey snapChatKey) {
        return createSnapChat(defaultStatus, snapChatKey, null);
    }

    public static SnapChat createSnapChat(boolean defaultStatus, SnapChatKey snapChatKey, PluginContext context) {
        return new AbstractSnapChat() {
            @Override
            public boolean defaultStatus() {
                return defaultStatus;
            }

            @Override
            public SnapChatKey key() {
                return snapChatKey;
            }

            @Override
            public String calcKey() {
                return context == null ? key().calc() : key().calc(context);
            }
        };
    }
}
