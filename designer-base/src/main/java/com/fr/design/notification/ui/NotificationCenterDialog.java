package com.fr.design.notification.ui;

import com.fr.design.gui.ilable.UILabel;
import com.fr.design.layout.FRGUIPaneFactory;
import com.fr.design.mainframe.DesignerContext;
import com.fr.design.notification.Notification;
import com.fr.design.notification.NotificationCenter;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.Toolkit;
import java.awt.Window;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.ArrayList;
import javax.swing.JDialog;
import javax.swing.JPanel;

public class NotificationCenterDialog extends JDialog {

    private ArrayList<NotificationPane> notificationNeedShow;
    private JPanel centerPanel;
    private UILabel deleteLabel;
    private static final int NOTIFICATIONCOUNT = 5;


    public NotificationCenterDialog(Frame parent) {
        super(parent);
        setTitle(com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Basic_Notification"));
        setResizable(false);
        notificationNeedShow = new ArrayList<>();
        addWindowFocusListener(new WindowAdapter() {
            @Override
            public void windowLostFocus(WindowEvent e) {
                hideDialog();
            }
        });
        initComponents();
    }

    private void initComponents() {
        centerPanel = FRGUIPaneFactory.createNColumnGridInnerContainer_S_Pane(1);
        addNotification();
        deleteLabel = new UILabel(com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Basic_Clear_Notifications") + "(" + NotificationCenter.getInstance().getNotificationsCount() + ")");
        deleteLabel.setForeground(Color.BLUE);

        deleteLabel.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                NotificationCenter.getInstance().clearAllNotifications();
                centerPanel.removeAll();
                addNotification();
                pack();
                deleteLabel.setText(com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Basic_Clear_Notifications") + "(" + NotificationCenter.getInstance().getNotificationsCount() + ")");
                if (notificationNeedShow.size() == 0) {
                    hideDialog();
                }
            }
        });
        JPanel deletePane = new JPanel(FRGUIPaneFactory.createCenterFlowLayout());
        deletePane.add(deleteLabel);
        deletePane.setPreferredSize(new Dimension(240, 30));
        add(centerPanel, BorderLayout.CENTER);
        add(deletePane, BorderLayout.SOUTH);
        pack();
        centerWindow();
    }

    public ArrayList<NotificationPane> getNotificationNeedShow() {
        return notificationNeedShow;
    }

    public JPanel getCenterPanel() {
        return centerPanel;
    }

    public UILabel getDeleteLabel() {
        return deleteLabel;
    }


    public void hideDialog() {
        this.dispose();
    }

    public void showDialog() {
       this.setVisible(true);
    }

    public void addNotification() {
        notificationNeedShow.clear();

        int size = NotificationCenter.getInstance().getNotificationsCount();
        for (int i = NOTIFICATIONCOUNT; i > 0; i--) {
            int j = size - i;
            if (j >= 0) {
                Notification notification = NotificationCenter.getInstance().getNotification(j);
                NotificationPane notificationPane = new NotificationPane(this, notification.getMessageId(), notification.getType(), notification.getMessage(), i, notification.getNotificationDialogAction());
                notificationNeedShow.add(notificationPane);
            }
        }
        size = notificationNeedShow.size();
        for (int i = size - 1; i >= 0; i--) {
            centerPanel.add(notificationNeedShow.get(i));
        }
    }

    private void centerWindow() {
        Window win = this;
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();

        Dimension winSize = win.getSize();

        if (winSize.height > screenSize.height) {
            winSize.height = screenSize.height;
        }
        if (winSize.width > screenSize.width) {
            winSize.width = screenSize.width;
        }
        win.setLocation((DesignerContext.getDesignerFrame().getWidth() - winSize.width - 100 + DesignerContext.getDesignerFrame().getX()),
                DesignerContext.getDesignerFrame().getY()  + winSize.height);
    }
}
