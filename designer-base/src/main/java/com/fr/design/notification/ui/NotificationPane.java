package com.fr.design.notification.ui;

import com.fr.design.dialog.NotificationDialogAction;
import com.fr.design.gui.ilable.UILabel;
import com.fr.design.layout.FRGUIPaneFactory;
import com.fr.design.notification.NotificationCenter;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import javax.swing.BorderFactory;
import javax.swing.Icon;
import javax.swing.JPanel;
import javax.swing.UIManager;

/**
 * @author Lanlan
 * @version 10.0
 * Created by Lanlan on 2021/6/21
 */
public class NotificationPane extends JPanel {
    private NotificationCenterDialog parent;
    private String messageId;
    private int index;
    private UILabel messageLabel;
    private UILabel messageIcon;
    private NotificationDialogAction notificationDialogAction;

    public NotificationPane(NotificationCenterDialog parent, String messageId, int type, String message, int index, NotificationDialogAction notificationDialogAction) {
        this.parent = parent;
        this.index = index;
        this.messageId = messageId;
        this.notificationDialogAction = notificationDialogAction;
        this.setLayout(FRGUIPaneFactory.createBorderLayout());
        setTypeIcon(getIconForType(type));
        setNotification(message);
        setDeleteIcon();
    }

    public void setTypeIcon(Icon icon) {
        messageIcon = new UILabel(icon);
        messageIcon.addMouseListener(messageAndIconListener);
        JPanel messageIconPanel = FRGUIPaneFactory.createBorderLayout_S_Pane();
        messageIconPanel.setBorder(BorderFactory.createEmptyBorder(0, 8, 0, 8));
        messageIconPanel.add(messageIcon);
        add(messageIconPanel, BorderLayout.WEST);
    }

    public void setDeleteIcon() {
        UILabel deleteIcon = new UILabel(UIManager.getIcon("OptionPane.deleteIcon"));
        JPanel deleteIconPane = FRGUIPaneFactory.createBorderLayout_S_Pane();
        deleteIconPane.setBorder(BorderFactory.createEmptyBorder(0, 5, 0, 7));
        deleteIconPane.add(deleteIcon);
        deleteIconPane.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                NotificationCenter.getInstance().removeNotification(NotificationCenter.getInstance().getNotificationsCount() - index);
                parent.getCenterPanel().removeAll();
                parent.addNotification();
                parent.getDeleteLabel().setText(com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Basic_Clear_Notifications") + "(" + NotificationCenter.getInstance().getNotificationsCount() + ")");
                parent.pack();
                if (parent.getNotificationNeedShow().size() == 0) {
                    parent.hideDialog();
                }
                NotificationCenterPane.getNotificationCenterPane().refreshButton();
            }

            @Override
            public void mouseEntered(MouseEvent e) {
                deleteIcon.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));

            }

            @Override
            public void mouseExited(MouseEvent e) {
                deleteIcon.setCursor(Cursor.getDefaultCursor());
            }
        });
        add(deleteIconPane, BorderLayout.EAST);
    }

    public int getIndex() {
        return index;
    }

    public String getMessageId() {
        return messageId;
    }

    public NotificationDialogAction getNotificationDialogAction() {
        return notificationDialogAction;
    }

    public void setNotification(String message) {
        messageLabel = new UILabel("<html>" + message + "</html>");
        messageLabel.addMouseListener(messageAndIconListener);
        JPanel labelPanel = FRGUIPaneFactory.createBorderLayout_S_Pane();
        labelPanel.add(messageLabel);
        this.add(labelPanel, BorderLayout.CENTER);
    }

    protected Icon getIconForType(int messageType) {
        String propertyName;
        switch (messageType) {
            case 0:
                propertyName = "OptionPane.circularErrorIcon";
                break;
            case 1:
                propertyName = "OptionPane.newMessageIcon";
                break;
            case 2:
                propertyName = "OptionPane.circularWarningIcon";
                break;
            default:
                return null;
        }
        return UIManager.getIcon(propertyName);
    }

    private void performTargetAction() {
        parent.hideDialog();
        notificationDialogAction.doClick();
    }

    private MouseListener messageAndIconListener = new MouseAdapter() {
        @Override
        public void mouseClicked(MouseEvent e) {
            performTargetAction();
        }

        @Override
        public void mouseEntered(MouseEvent e) {
            messageLabel.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            messageIcon.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            messageLabel.setForeground(new Color(250, 170, 57));

        }

        @Override
        public void mouseExited(MouseEvent e) {
            messageLabel.setCursor(Cursor.getDefaultCursor());
            messageIcon.setCursor(Cursor.getDefaultCursor());
            messageLabel.setForeground(Color.BLACK);
        }
    };
}
