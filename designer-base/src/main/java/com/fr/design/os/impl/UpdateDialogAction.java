package com.fr.design.os.impl;

import com.fr.design.mainframe.DesignerContext;
import com.fr.design.update.ui.dialog.UpdateMainDialog;
import com.fr.stable.os.support.OSBasedAction;

/**
 * 更新升级窗口
 * @author pengda
 * @date 2019/10/9
 */
public class UpdateDialogAction implements OSBasedAction {
    
    @Override
    public void execute(Object... objects) {
        UpdateMainDialog dialog = new UpdateMainDialog(DesignerContext.getDesignerFrame());
        dialog.showDialog();
    }
}
