package com.fr.design.port;

import com.fr.design.fun.DesignerPortProvider;
import com.fr.stable.bridge.StableFactory;

/**
 * @author hades
 * @version 10.0
 * Created by hades on 2021/1/27
 */
public class DesignerPortContext {

    private static int messagePort = -1;

    private static int embeddedServerPort = -1;

    static {
        DesignerPortProvider designerPortProvider = StableFactory.getMarkedInstanceObjectFromClass(DesignerPortProvider.MARK_STRING, DesignerPortProvider.class);
        if (designerPortProvider != null) {
            messagePort = designerPortProvider.messagePort();
            embeddedServerPort = designerPortProvider.embeddedServerPort();
        }
    }

    public static int getMessagePort() {
        return messagePort;
    }

    public static int getEmbeddedServerPort() {
        return embeddedServerPort;
    }
}
