package com.fr.design.report.fit.menupane;

import com.fr.design.i18n.Toolkit;
import com.fr.design.mainframe.JTemplate;
import com.fr.design.report.fit.BaseFitAttrPane;
import com.fr.design.report.fit.FitAttrModel;
import com.fr.general.ComparatorUtils;
import com.fr.report.fit.FitProvider;
import com.fr.report.fit.ReportFitAttr;

import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;


public class TemplateFitAttrPane extends BaseFitAttrPane {

    private  JTemplate jwb;

    public TemplateFitAttrPane(JTemplate jwb) {
        this.jwb = jwb;
        initComponents();
    }

    @Override
    protected void initComponents() {
        super.initComponents();
        for (FitAttrModel fitAttrModel : fitAttrModelList) {
            if (fitAttrModel.isAvailable(jwb)) {
                populateModel(fitAttrModel);
                break;
            }
        }
    }

    @Override
    public void populateBean(ReportFitAttr reportFitAttr) {
        if (reportFitAttr == null) {
            itemChoose.setSelectedItem(Toolkit.i18nText("Fine-Design_Report_Using_Server_Report_View_Settings"));
        } else {
            itemChoose.setSelectedItem(Toolkit.i18nText("Fine-Design_Report_I_Want_To_Set_Single"));
        }
        populate(reportFitAttr);
    }

    public void populate(ReportFitAttr reportFitAttr) {
        if (reportFitAttr == null) {
            reportFitAttr = fitAttrModel.getGlobalReportFitAttr();
        }

        super.setEnabled(isTemplateSingleSet());
        super.populateBean(reportFitAttr);
    }


    public ReportFitAttr updateBean() {
        if (!isTemplateSingleSet()) {
            return null;
        } else {
            return super.updateBean();
        }
    }

    @Override
    protected String[] getItemNames() {
        return new String[]{Toolkit.i18nText("Fine-Design_Report_Using_Server_Report_View_Settings"),
                Toolkit.i18nText("Fine-Design_Report_I_Want_To_Set_Single")};
    }

    @Override
    protected ItemListener getItemListener() {
        return new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent e) {
                if (e.getStateChange() == ItemEvent.SELECTED) {
                    if(isTemplateSingleSet()){
                        if (jwb != null) {
                            FitProvider wbTpl = (FitProvider) jwb.getTarget();
                            ReportFitAttr fitAttr = wbTpl.getReportFitAttr();
                            populate(fitAttr);
                        }
                    }else {
                        populate(fitAttrModel.getGlobalReportFitAttr());
                    }
                }
            }
        };
    }

    private boolean isTemplateSingleSet() {
        return ComparatorUtils.equals(Toolkit.i18nText("Fine-Design_Report_I_Want_To_Set_Single"), itemChoose.getSelectedItem());
    }
}