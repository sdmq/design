package com.fr.design.ui.compatible;

import com.fr.design.ui.ModernUIPane;
import com.teamdev.jxbrowser.browser.callback.InjectJsCallback;
import com.teamdev.jxbrowser.chromium.events.LoadListener;
import com.teamdev.jxbrowser.chromium.events.ScriptContextListener;
import com.teamdev.jxbrowser.event.Observer;

/**
 * 封装jxbrwoser v6/v7的构建方式的差异
 *
 * @author hades
 * @version 10.0
 * Created by hades on 2021/6/13
 */
public interface BuilderDiff<T> {

    ModernUIPane.Builder<T> prepareForV6(ScriptContextListener contextListener);

    ModernUIPane.Builder<T> prepareForV6(LoadListener loadListener);

    ModernUIPane.Builder<T> prepareForV7(InjectJsCallback callback);

    ModernUIPane.Builder<T> prepareForV7(Class event, Observer listener);

}
