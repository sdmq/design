package com.fr.design.update.utils;

import com.fr.design.update.factory.UpdateFileFactory;
import com.fr.stable.StableUtils;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Bryant
 * @version 10.0
 * Created by Bryant on 2020-09-25
 */
public class UpdateFileUtils {
    /**
     * 列出过滤后的文件
     *
     * @return String数组
     */
    public static String[] listBackupVersions() {
        File[] versionBackup = UpdateFileFactory.getBackupVersions();
        List<String> versions = new ArrayList<>();
        if (versionBackup != null) {
            for (File file : versionBackup) {
                if (UpdateFileFactory.isBackupVersionsValid(file.getAbsolutePath())) {
                    versions.add(file.getName());
                } else {
                    StableUtils.deleteFile(file);
                }
            }
        }
        String[] result = new String[versions.size()];
        return versions.toArray(result);
    }
}
