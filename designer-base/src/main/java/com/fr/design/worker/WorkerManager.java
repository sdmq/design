package com.fr.design.worker;

import java.util.HashMap;
import java.util.Map;
import javax.swing.SwingWorker;
import org.jetbrains.annotations.Nullable;

/**
 * @author hades
 * @version 10.0
 * Created by hades on 2021/4/2
 */
public class WorkerManager {

    private static final WorkerManager INSTANCE = new WorkerManager();

    private Map<String, SwingWorker> workerMap = new HashMap<>();

    public static WorkerManager getInstance() {
        return INSTANCE;
    }

    @Nullable
    public SwingWorker getWorker(String taskName) {
        return workerMap.get(taskName);
    }

    public boolean isCompleted(String taskName) {
        SwingWorker worker = getWorker(taskName);
        return worker == null || worker.isDone();
    }

    public void registerWorker(String taskName, SwingWorker worker) {
        workerMap.put(taskName, worker);
    }

    public void removeWorker(String taskName) {
        workerMap.remove(taskName);
    }

    public void cancelWorker(String taskName) {
        SwingWorker worker = getWorker(taskName);
        if (worker != null && !worker.isDone()) {
            worker.cancel(true);
            removeWorker(taskName);
        }
    }

}
