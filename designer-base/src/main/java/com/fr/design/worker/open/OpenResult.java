package com.fr.design.worker.open;

import com.fr.base.io.BaseBook;

/**
 * @author hades
 * @version 10.0
 * Created by hades on 2021/4/13
 */
public class OpenResult<T extends BaseBook, R> {

    private final T  baseBook;

    private final R ref;

    public OpenResult(T baseBook, R r) {
        this.baseBook = baseBook;
        this.ref = r;
    }

    public T getBaseBook() {
        return baseBook;
    }

    public R getRef() {
        return ref;
    }
}
