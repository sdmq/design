package com.fr.design.worker.save;

import com.fr.common.util.Collections;
import com.fr.design.mainframe.JTemplate;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.Callable;

/**
 * 保存之后需要做些外部回调
 *
 * @author hades
 * @version 10.0
 * Created by hades on 2021/4/8
 */
public class CallbackSaveWorker extends SaveWorker {

    private List<Runnable> successRunnableList;

    private List<Runnable> failRunnableList;

    public CallbackSaveWorker(Callable<Boolean> callable, JTemplate<?, ?> template) {
        super(callable, template);
    }

    @Override
    protected void done() {
        super.done();

        if (success) {
            fireRunnable(successRunnableList);
        } else {
            fireRunnable(failRunnableList);
        }
        successRunnableList = null;
        failRunnableList = null;
    }

    private void fireRunnable(List<Runnable> list) {
        if (Collections.isEmpty(list)) {
            return;
        }
        for (Runnable runnable : list) {
            runnable.run();
        }
    }

    private void addCallback(List<Runnable> runnableList, Runnable runnable) {
        if (runnableList == null) {
            runnableList = new LinkedList<>();
        }
        if (runnable != null) {
            runnableList.add(runnable);
        }
    }

    public void addSuccessCallback(Runnable successRunnable) {
        if (successRunnableList == null) {
            successRunnableList = new LinkedList<>();
        }
        if (successRunnable != null) {
            successRunnableList.add(successRunnable);
        }
    }

    public void addFailCallback(Runnable failRunnable) {
        if (failRunnableList == null) {
            failRunnableList = new LinkedList<>();
        }
        if (failRunnable != null) {
            failRunnableList.add(failRunnable);
        }
    }

}
