package com.fr.env;

import com.fr.design.RestartHelper;
import com.fr.design.dialog.link.MessageWithLink;
import com.fr.design.gui.ibutton.UIButton;
import com.fr.design.gui.ilable.UILabel;
import com.fr.design.gui.itextarea.UITextArea;
import com.fr.design.i18n.DesignSizeI18nManager;
import com.fr.design.i18n.Toolkit;
import com.fr.design.layout.FRGUIPaneFactory;
import com.fr.design.mainframe.DesignerContext;
import com.fr.design.utils.gui.GUICoreUtils;
import com.fr.general.CloudCenter;
import com.fr.general.ComparatorUtils;
import com.fr.general.GeneralContext;
import com.fr.general.IOUtils;
import com.fr.json.JSONArray;
import com.fr.json.JSONObject;
import com.fr.stable.StringUtils;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.Locale;
import javax.swing.BorderFactory;
import javax.swing.Icon;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.UIManager;

/**
 * @author pengda
 * @version 10.0
 * Created on 2021-06-02
 */
public class SyncFailedPluginsDialog extends JDialog {
    private UILabel detailsLabel;
    private JScrollPane scrollPane;
    private UITextArea detailsTextArea;
    private JFrame frame = DesignerContext.getDesignerFrame();
    private RestartHelper restartHelper = new RestartHelper();
    private UIButton restartButton;
    private boolean show = false;
    public SyncFailedPluginsDialog(JFrame parent, JSONArray syncFailedPlugins) {
        super(parent, true);
        JPanel body = FRGUIPaneFactory.createBorderLayout_L_Pane();

        JPanel northPane = FRGUIPaneFactory.createBorderLayout_L_Pane();
        northPane.setBorder(BorderFactory.createEmptyBorder(0,0,5,0));
        JPanel imagePanel = new JPanel();
        Icon icon = IOUtils.readIcon("com/fr/design/icon/versioncheck/warning.png");
        JLabel imageLabel = new JLabel();
        imageLabel.setIcon(icon);
        imagePanel.add(imageLabel);
        imagePanel.setPreferredSize(new Dimension(20, 20));

        JPanel messagePane = FRGUIPaneFactory.createBorderLayout_S_Pane();
        MessageWithLink messageWithLink = new MessageWithLink(Toolkit.i18nText("Fine-Design_Basic_Sync_Plugin_Fail_Suggestion"),Toolkit.i18nText("Fine-Design_Basic_Sync_Deal_Immediately"),
                CloudCenter.getInstance().acquireUrlByKind("help.installplugins", "https://help.fanruan.com/finereport/doc-view-2198.html"));
        messageWithLink.setPreferredSize(DesignSizeI18nManager.getInstance().i18nDimension("com.fr.env.SyncFailedPluginsDialog.messageWithLink"));

        messagePane.add(messageWithLink);
        messagePane.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 0));

        northPane.add(imageLabel, BorderLayout.WEST);
        northPane.add(messagePane, BorderLayout.CENTER);

        JPanel centerPanel = FRGUIPaneFactory.createBorderLayout_S_Pane();
        detailsLabel = new UILabel(Toolkit.i18nText("Fine_Designer_Look_Detail"));
        detailsLabel.setIcon(UIManager.getIcon("OptionPane.narrow.down"));
        detailsLabel.addMouseListener(detailsLabelClickListener);
        JPanel detailsTitlePanel = FRGUIPaneFactory.createLeftFlowZeroGapBorderPane();
        detailsTitlePanel.add(detailsLabel);
        detailsTitlePanel.setBorder(BorderFactory.createEmptyBorder(0,0,5,0));

        detailsTextArea = new UITextArea();
        detailsTextArea.setEditable(false);
        showSyncFailedPluginsInfo(syncFailedPlugins);
        scrollPane = new JScrollPane(detailsTextArea);
        centerPanel.add(detailsTitlePanel,BorderLayout.NORTH);
        centerPanel.add(scrollPane,BorderLayout.CENTER);

        JPanel southPane = FRGUIPaneFactory.createRightFlowInnerContainer_S_Pane();
        restartButton = new UIButton(Toolkit.i18nText("Fine-Design_Updater_Restart_Designer"));
        restartButton.addMouseListener(restartButtonClickListener);
        southPane.add(restartButton);

        body.add(northPane,BorderLayout.NORTH);
        body.add(centerPanel,BorderLayout.CENTER);
        body.add(southPane,BorderLayout.SOUTH);

        this.setTitle(Toolkit.i18nText("Fine-Design_Basic_Tool_Tips"));
        this.setResizable(false);
        this.add(body, BorderLayout.CENTER);
        this.setSize(new Dimension(GeneralContext.getLocale().equals(Locale.US) ? 400 : 380, 225));
        this.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                hideDialog();
            }
        });
        GUICoreUtils.centerWindow(this);
    }

    public void showSyncFailedPluginsInfo(JSONArray syncFailedPlugins){
        if(null == syncFailedPlugins){
            return;
        }
        StringBuilder detailsText = new StringBuilder(StringUtils.EMPTY);
        for (int i = 0; i < syncFailedPlugins.size(); i++) {
            JSONObject plugin = syncFailedPlugins.getJSONObject(i);
            detailsText.append(plugin.getString("name")).append(",").append(Toolkit.i18nText("Fine-Design_Basic_Sync_Server_Version")).append(plugin.getString("version")).append("\n");
        }
        detailsTextArea.setText(detailsText.toString());
        if(!ComparatorUtils.equals(detailsTextArea.getText(),StringUtils.EMPTY)){
            show = true;
        }
    }

    public void showDialog(){
        if(show){
            this.setVisible(true);
        }
    }

    private MouseListener detailsLabelClickListener = new MouseAdapter() {
        @Override
        public void mouseClicked(MouseEvent e) {
            if(scrollPane.isVisible()){
                scrollPane.setVisible(false);
                detailsLabel.setIcon(UIManager.getIcon("OptionPane.narrow.right"));
            }else{
                scrollPane.setVisible(true);
                detailsLabel.setIcon(UIManager.getIcon("OptionPane.narrow.down"));
            }
        }
    };

    public boolean restartClicked(){
        return !restartButton.isEnabled();
    }

    private void hideDialog(){
        this.dispose();
    }

    private MouseListener restartButtonClickListener = new MouseAdapter() {
        @Override
        public void mouseClicked(MouseEvent e) {
            hideDialog();
            restartButton.setEnabled(false);
            restartHelper.restartForUpdate(frame);
        }
    };
}
