package com.fr.env.handler;

/**
 *
 * @author hades
 * @version 10.0
 * Created by hades on 2021/8/11
 */
public class RefWrapper {

    private final Throwable throwable;

    private final String link;


    public RefWrapper(Throwable throwable, String link) {
        this.throwable = throwable;
        this.link = link;
    }

    public Throwable getThrowable() {
        return throwable;
    }

    public String getLink() {
        return link;
    }
}
