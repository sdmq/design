package com.fr.env.handler;

/**
 * @author hades
 * @version 10.0
 * Created by hades on 2021/8/5
 */
public class ResultWrapper {

    private final boolean next;

    private final Throwable throwable;

    public ResultWrapper(Throwable throwable) {
        this(true, throwable);
    }

    public ResultWrapper(boolean next, Throwable e) {
        this.next = next;
        this.throwable = e;
    }

    public boolean isNext() {
        return next;
    }

    public Throwable getThrowable() {
        return throwable;
    }
}
