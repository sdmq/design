package com.fr.env.handler;

import com.fr.base.exception.ExceptionDescriptor;
import com.fr.design.EnvChangeEntrance;
import com.fr.design.dialog.FineJOptionPane;
import com.fr.design.env.DesignerWorkspaceInfo;
import com.fr.design.env.DesignerWorkspaceType;
import com.fr.design.i18n.Toolkit;
import com.fr.env.RemoteWorkspaceURL;
import com.fr.env.handler.impl.CancelHandler;
import com.fr.env.handler.impl.CommonHandler;
import com.fr.env.handler.impl.ExecutionHandler;
import com.fr.env.handler.impl.UnexpectedHandler;
import com.fr.log.FineLoggerFactory;
import com.fr.stable.StringUtils;
import java.util.ArrayList;
import java.util.List;
import javax.swing.UIManager;


import static javax.swing.JOptionPane.ERROR_MESSAGE;

/**
 * @author hades
 * @version 10.0
 * Created by hades on 2021/8/5
 */
public class WorkspaceExceptionHandler {

    private static final WorkspaceExceptionHandler INSTANCE = new WorkspaceExceptionHandler();

    public static WorkspaceExceptionHandler getInstance() {
        return INSTANCE;
    }

    private final List<Handler<RefWrapper, ResultWrapper>> testList = new ArrayList<>();

    private final List<Handler<RefWrapper, ResultWrapper>> switchList = new ArrayList<>();

    private WorkspaceExceptionHandler() {
        // 要保证顺序
        testList.add(new CancelHandler());
        testList.add(new ExecutionHandler());
        testList.add(new UnexpectedHandler());
        testList.add(new CommonHandler(false));

        switchList.add(new CancelHandler());
        switchList.add(new ExecutionHandler());
        switchList.add(new UnexpectedHandler());
        switchList.add(new CommonHandler(true));
    }

    public void handle(Throwable e, List<Handler<RefWrapper, ResultWrapper>> list, DesignerWorkspaceInfo workspaceInfo) {
        Throwable throwable = e;
        ResultWrapper wrapper;
        String link = workspaceInfo.getConnection().getUrl() + RemoteWorkspaceURL.SYSTEM_LOGIN_PATH;
        for (Handler<RefWrapper, ResultWrapper> handler : list) {
            wrapper = handler.handle(new RefWrapper(throwable, link));
            throwable = wrapper.getThrowable();
            if (!wrapper.isNext()) {
                break;
            }
        }
        FineLoggerFactory.getLogger().error(throwable.getMessage(), throwable);
    }

    public void handleInSwitch(Throwable e, DesignerWorkspaceInfo workspaceInfo) {
        if (workspaceInfo == null || workspaceInfo.getType() == DesignerWorkspaceType.Local) {
            FineLoggerFactory.getLogger().error(e.getMessage(), e);
            if (e instanceof ExceptionDescriptor) {
                new CommonHandler(true).handle(new RefWrapper(e, StringUtils.EMPTY));
            } else {
                FineJOptionPane.showMessageDialog(EnvChangeEntrance.getInstance().getDialog(),
                                                  Toolkit.i18nText("Fine-Design_Basic_Switch_Workspace_Failed"),
                                                  Toolkit.i18nText("Fine-Design_Basic_Tool_Tips"),
                                                  ERROR_MESSAGE,
                                                  UIManager.getIcon("OptionPane.errorIcon"));
            }
            return;
        }
        handle(e, switchList, workspaceInfo);
    }

    public void handleInStart(Throwable e, DesignerWorkspaceInfo workspaceInfo) {
        if (workspaceInfo == null || workspaceInfo.getType() == DesignerWorkspaceType.Local) {
            FineLoggerFactory.getLogger().error(e.getMessage(), e);
            new CommonHandler(false).handle(new RefWrapper(e, StringUtils.EMPTY));
            return;
        }
        handle(e, testList, workspaceInfo);
    }

    public void handleInTest(Throwable e, DesignerWorkspaceInfo workspaceInfo) {
        handle(e, testList, workspaceInfo);
    }

}
