package com.fr.env.handler.impl;

import com.fr.env.handler.Handler;
import com.fr.env.handler.RefWrapper;
import com.fr.env.handler.ResultWrapper;
import java.util.concurrent.CancellationException;

/**
 * 取消测试连接时的处理器
 *
 * @author hades
 * @version 10.0
 * Created by hades on 2021/8/5
 */
public class CancelHandler implements Handler<RefWrapper, ResultWrapper> {

    @Override
    public ResultWrapper handle(RefWrapper wrapper) {
        Throwable e = wrapper.getThrowable();
        return new ResultWrapper(!(e instanceof CancellationException), e);
    }
}
