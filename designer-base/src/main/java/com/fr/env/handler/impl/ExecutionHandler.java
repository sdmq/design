package com.fr.env.handler.impl;

import com.fr.env.handler.Handler;
import com.fr.env.handler.RefWrapper;
import com.fr.env.handler.ResultWrapper;
import java.util.concurrent.ExecutionException;

/**
 * @author hades
 * @version 10.0
 * Created by hades on 2021/8/5
 */
public class ExecutionHandler implements Handler<RefWrapper, ResultWrapper> {

    @Override
    public ResultWrapper handle(RefWrapper wrapper) {
        Throwable e = wrapper.getThrowable();
        if (e instanceof ExecutionException) {
            return new ResultWrapper(e.getCause());
        }
        return new ResultWrapper(e);
    }
}
