package com.fr.env.utils;

import com.fr.common.annotations.Compatible;
import com.fr.general.IOUtils;
import com.fr.log.FineLoggerFactory;
import com.fr.stable.EncodeConstants;
import com.fr.stable.ProductConstants;
import com.fr.stable.StableUtils;
import com.fr.stable.StringUtils;
import com.fr.stable.xml.XMLPrintWriter;
import com.fr.stable.xml.XMLReadable;
import com.fr.stable.xml.XMLTools;
import com.fr.stable.xml.XMLWriter;
import com.fr.stable.xml.XMLableReader;
import com.fr.third.javax.xml.stream.XMLStreamException;

import java.io.BufferedWriter;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;

/**
 * @author Starryi
 * @version 10.0.18
 * Created by Starryi on 2021/7/7
 * 设计器访问和获取关键历史交互信息的持久化工具，该关键历史交互信息，
 * 如用户是否点击过某按钮，是否查看过某弹窗信息，上次选择过的文件所在目录等
 */
@Compatible
public class DesignerInteractionHistory implements XMLReadable, XMLWriter {

    private static final String FILE_NAME = "designer.ix.history.info";
    private static final String ROOT_TAG = "History";

    private static DesignerInteractionHistory history;
    public static DesignerInteractionHistory getInstance() {
        if (history == null) {
            history = new DesignerInteractionHistory();

            readXMLFile(history, history.getHistoryFile());
        }

        return history;
    }

    private File getHistoryFile() {
        return new File(StableUtils.pathJoin(ProductConstants.getEnvHome(), FILE_NAME));
    }

    private static void readXMLFile(XMLReadable xmlReadable, File xmlFile) {
        if (xmlFile == null || !xmlFile.exists()) {
            return;
        }
        String charset = EncodeConstants.ENCODING_UTF_8;
        try {
            String decodeContent = getFileContent(xmlFile);
            InputStream xmlInputStream = new ByteArrayInputStream(decodeContent.getBytes(charset));
            InputStreamReader inputStreamReader = new InputStreamReader(xmlInputStream, charset);

            XMLableReader xmlReader = XMLableReader.createXMLableReader(inputStreamReader);

            if (xmlReader != null) {
                xmlReader.readXMLObject(xmlReadable);
            }
            xmlInputStream.close();
        } catch (IOException | XMLStreamException e) {
            FineLoggerFactory.getLogger().error(e.getMessage(), e);
        }

    }

    private static String getFileContent(File xmlFile) throws FileNotFoundException, UnsupportedEncodingException {
        InputStream encodeInputStream = new FileInputStream(xmlFile);
        return IOUtils.inputStream2String(encodeInputStream);
    }

    private static void writeContentToFile(String fileContent, File file) {
        try (FileOutputStream fos = new FileOutputStream(file);
             OutputStreamWriter osw = new OutputStreamWriter(fos, StandardCharsets.UTF_8);
             BufferedWriter bw = new BufferedWriter(osw)) {
            bw.write(fileContent);
        } catch (Exception e) {
            FineLoggerFactory.getLogger().error(e.getMessage(), e);
        }
    }

    public void saveXMLFile() {
        File xmlFile = this.getHistoryFile();
        try {
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            XMLTools.writeOutputStreamXML(this, out);
            out.flush();
            out.close();
            String fileContent = new String(out.toByteArray(), StandardCharsets.UTF_8);
            writeContentToFile(fileContent, xmlFile);
        } catch (Exception e) {
            FineLoggerFactory.getLogger().error(e.getMessage(), e);
        }
    }


    private static final String HAS_SHOWN_SHIFT_DRAG_RESIZING_TOOLTIP = "hasShownShiftDragResizingTooltip";
    private static final String LAST_SELECTED_BORDER_IMAGE_DIR = "lastSelectedBorderImageDir";

    // 是否已展示过按下Shift键可锁定比例拖拽尺寸的Tooltip
    private boolean hasShownShiftDragResizingTooltip = false;
    // 用户上次通过文件选择器选择的边框图片所在目录
    private String lastSelectedBorderImageDir = StringUtils.EMPTY;

    public boolean isHasShownShiftDragResizingTooltip() {
        return hasShownShiftDragResizingTooltip;
    }

    public void setHasShownShiftDragResizingTooltip(boolean shown) {
        this.hasShownShiftDragResizingTooltip = shown;
    }

    public String getLastSelectedBorderImageDir() {
        return lastSelectedBorderImageDir;
    }

    public void setLastSelectedBorderImageDir(String dirPath) {
        this.lastSelectedBorderImageDir = dirPath;
    }

    @Override
    public void writeXML(XMLPrintWriter writer) {
        writer.startTAG(ROOT_TAG)
                .attr(HAS_SHOWN_SHIFT_DRAG_RESIZING_TOOLTIP, isHasShownShiftDragResizingTooltip())
                .attr(LAST_SELECTED_BORDER_IMAGE_DIR, getLastSelectedBorderImageDir())
                .end();
    }

    @Override
    public void readXML(XMLableReader reader) {
        setHasShownShiftDragResizingTooltip(reader.getAttrAsBoolean(HAS_SHOWN_SHIFT_DRAG_RESIZING_TOOLTIP, false));
        setLastSelectedBorderImageDir(reader.getAttrAsString(LAST_SELECTED_BORDER_IMAGE_DIR, StringUtils.EMPTY));
    }
}
