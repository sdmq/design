package com.fr.base;

import com.fr.form.main.Form;
import com.fr.invoke.ClassHelper;
import com.fr.main.impl.WorkBook;
import java.util.Set;
import junit.framework.TestCase;
import org.junit.Assert;


/**
 * @author hades
 * @version 10.0
 * Created by hades on 2020/12/28
 */
public class ClassHelperTest extends TestCase {


    public void testGetClassLoaders() {
        WorkBook workBook = new WorkBook();
        Set<ClassLoader> set = ClassHelper.getClassLoaders(workBook);
        Assert.assertFalse(set.isEmpty());
        Form form = new Form();
        Set<ClassLoader> set1 = ClassHelper.getClassLoaders(form);
        Assert.assertFalse(set1.isEmpty());

    }

}