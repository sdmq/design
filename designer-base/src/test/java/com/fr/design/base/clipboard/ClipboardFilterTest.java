package com.fr.design.base.clipboard;

import com.fr.design.ExtraDesignClassManager;
import com.fr.design.fun.ClipboardHandlerProvider;
import com.fr.plugin.injectable.PluginModule;
import com.fr.stable.fun.mark.Mutable;
import org.easymock.EasyMock;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.easymock.PowerMock;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.util.HashSet;
import java.util.Set;

@PrepareForTest(PluginModule.class)
@RunWith(PowerMockRunner.class)
public class ClipboardFilterTest {
    
    
    @Before
    public void setUp() throws Exception {
    
        Set<Mutable> providers = new HashSet<>();
        providers.add(new TestClipboardHandlerProvider<Object>());
    
        ExtraDesignClassManager designClassManager = EasyMock.mock(ExtraDesignClassManager.class);
        EasyMock.expect(designClassManager.getArray(ClipboardHandlerProvider.XML_TAG))
                .andReturn(providers)
                .anyTimes();
        EasyMock.replay(designClassManager);
    
        PowerMock.mockStatic(PluginModule.class);
        EasyMock.expect(PluginModule.getAgent(PluginModule.ExtraDesign))
                .andReturn(designClassManager)
                .anyTimes();
        PowerMock.replayAll();
    }
    
    @After
    public void tearDown() throws Exception {
        
        PowerMock.resetAll();
    }
    
    @Test
    public void testClipboard() {
        
        ClipboardFilter.cut("cut");
        String paste1 = ClipboardFilter.paste("paste");
        Assert.assertNull(paste1);
        
        ClipboardFilter.copy("copy");
        String paste2 = ClipboardFilter.paste("paste");
        Assert.assertNull(paste2);
    }
    
}