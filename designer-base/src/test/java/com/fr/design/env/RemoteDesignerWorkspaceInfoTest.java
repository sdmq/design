package com.fr.design.env;

import com.fr.invoke.Reflect;
import com.fr.stable.xml.XMLPrintWriter;
import com.fr.stable.xml.XMLReaderHelper;
import com.fr.stable.xml.XMLableReader;
import com.fr.workspace.WorkContext;
import com.fr.workspace.connect.WorkspaceClient;
import com.fr.workspace.connect.WorkspaceConnectionInfo;
import com.fr.workspace.connect.WorkspaceConnector;
import junit.framework.TestCase;
import org.junit.Assert;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;

/**
 * @author hades
 * @version 10.0
 * Created by hades on 2020/7/15
 */
public class RemoteDesignerWorkspaceInfoTest extends TestCase {

    public void testCheckValid() throws Exception {
        RemoteDesignerWorkspaceInfo workspaceInfo0 = RemoteDesignerWorkspaceInfo.create(new WorkspaceConnectionInfo("http://localhost:8075/webroot/decision", "admin", "123", "", "", true));
        RemoteDesignerWorkspaceInfo workspaceInfo1 = RemoteDesignerWorkspaceInfo.create(new WorkspaceConnectionInfo("http://127.0.0.1:8075/webroot/decision", "admin", "123", "", "", true));
        RemoteDesignerWorkspaceInfo workspaceInfo2 = RemoteDesignerWorkspaceInfo.create(new WorkspaceConnectionInfo("https://127.0.0.1:8075/webroot/decision", "admin", "123", "", "", true));
        RemoteDesignerWorkspaceInfo workspaceInfo3 = RemoteDesignerWorkspaceInfo.create(new WorkspaceConnectionInfo("https://localhost:8075/webroot/decision", "admin", "123", "", "", true));
        WorkspaceConnector connector = WorkContext.getConnector();

        WorkspaceConnector workspaceConnector = new WorkspaceConnector() {
            @Override
            public boolean testConnection(WorkspaceConnectionInfo connection) throws Exception {
                return false;
            }

            @Override
            public WorkspaceClient connect(WorkspaceConnectionInfo connection) throws Exception {
                return null;
            }
        };
        WorkContext.setConnector(workspaceConnector);

        Assert.assertFalse(workspaceInfo0.checkValid());
        Assert.assertFalse(workspaceInfo1.checkValid());
        Assert.assertFalse(workspaceInfo2.checkValid());
        Assert.assertFalse(workspaceInfo3.checkValid());
        WorkContext.setConnector(connector);
    }

    public void testReadXml() {
        String xml0 = "<Connection url=\"https://192.168.101.171:8075/webroot/decision\" username=\"1\" password=\"EclFl2zl5ti32lqQIh9r/7iCd0Vmhm5uuLol+zs+oi4JWN8F6JA2Ym4PYeQOAR5cjvIdzhUgwoG/\n" +
                "AJF1Ht1CFLfZqrztuJsWpP8GlMvqZb/Gi3TT2idJC/qHU6Am1jFe0qJoytyWUi5zGnZicTfVgDir\n" +
                "gE0kuw1ONOzFCkVfNbLrC+DXIIbVa7ramqbsVa6/LDEqOJvfAE36cBYJjWGHjOT91ixldaCyEX8m\n" +
                "0xmw5xvEuEiTgRUHufbKCEG0FOR1xvS3D3C9vlauoqDI1INOyUOawQvpuQ552AjZi/FH1v+eNt+q\n" +
                "eU7iCzsKb6P5KomaKUTZ8EPlSR/+Ihzg5VjAyg==\" certPath=\"\" certSecretKey=\"bofKLt/G93T2NudGR71Npb1GCPSUVeC34dtFGa+DHRl2GfP3WM4Nitb/LwdA/mml5BNl/S5f4txt\n" +
                "0jxK4r08BoWoleKt5r7C0Fapj6Ccm+qVP6ywaxN1PSC0ML8Fowo3VYhv2BVzdodMLLliIVngostk\n" +
                "76gqsiatzJb0XiPnzb0UpjWs7lpgCw+XDBk2Qfpv/U8fyPmLkAxA+085w55vzLluu7o2ilaveOUk\n" +
                "hmT3NfZinTTZd+dQJxfVfUgjKJdZ9C+EM/IkzY1gVYXYuZ8gl5wd5RkjC3VUOu36akmB09gXJ4S8\n" +
                "z8ZdHilA5L9tneuS5C05DMRJ6CdO+avGJgJAtw==\" rememberPwd=\"true\"/>";
        String xml1 = "<Connection url=\"https://192.168.101.171:8075/webroot/decision\" username=\"1\" password=\"EclFl2zl5ti32lqQIh9r/7iCd0Vmhm5uuLol+zs+oi4JWN8F6JA2Ym4PYeQOAR5cjvIdzhUgwoG/\n" +
                "AJF1Ht1CFLfZqrztuJsWpP8GlMvqZb/Gi3TT2idJC/qHU6Am1jFe0qJoytyWUi5zGnZicTfVgDir\n" +
                "gE0kuw1ONOzFCkVfNbLrC+DXIIbVa7ramqbsVa6/LDEqOJvfAE36cBYJjWGHjOT91ixldaCyEX8m\n" +
                "0xmw5xvEuEiTgRUHufbKCEG0FOR1xvS3D3C9vlauoqDI1INOyUOawQvpuQ552AjZi/FH1v+eNt+q\n" +
                "eU7iCzsKb6P5KomaKUTZ8EPlSR/+Ihzg5VjAyg==\" certPath=\"\" certSecretKey=\"xxxxxxx\" rememberPwd=\"true\"/>";
        byte[] bytes0 = xml0.getBytes();
        byte[] bytes1 = xml1.getBytes();
        RemoteDesignerWorkspaceInfo info = new RemoteDesignerWorkspaceInfo();
        ByteArrayInputStream in0 = new ByteArrayInputStream(bytes0);
        ByteArrayInputStream in1 = new ByteArrayInputStream(bytes1);

        try {
            XMLableReader reader0 = XMLReaderHelper.createXMLableReader(in0, XMLPrintWriter.XML_ENCODER);
            Reflect.on(reader0).set("state", 1);
            info.setNewCreated(true);
            info.readXML(reader0);
            Assert.assertEquals("xxxxxxx", info.getConnection().getCertSecretKey());

            XMLableReader reader1 = XMLReaderHelper.createXMLableReader(in1, XMLPrintWriter.XML_ENCODER);
            Reflect.on(reader1).set("state", 1);
            info.setNewCreated(false);
            info.readXML(reader1);
            Assert.assertEquals("xxxxxxx", info.getConnection().getCertSecretKey());
        } catch (Exception ignore) {

        }
    }

    public void testWriteXml() {
        ByteArrayOutputStream out0 = new ByteArrayOutputStream();
        XMLPrintWriter writer0 = XMLPrintWriter.create(out0);
        RemoteDesignerWorkspaceInfo info0 = RemoteDesignerWorkspaceInfo.create(new WorkspaceConnectionInfo("127.0.0.1", "1", "1", "/xxx", "xxxxx", true));
        info0.setNewCreated(true);
        info0.writeXML(writer0);
        writer0.close();
        String result0 = new String(out0.toByteArray());
        Assert.assertTrue(result0.contains("certSecretKey"));
        Assert.assertFalse(result0.contains("certSecretKey=\"xxxxx\""));

        ByteArrayOutputStream out1 = new ByteArrayOutputStream();
        XMLPrintWriter writer1 = XMLPrintWriter.create(out1);
        RemoteDesignerWorkspaceInfo info1 = RemoteDesignerWorkspaceInfo.create(new WorkspaceConnectionInfo("127.0.0.1", "1", "1", "/xxx", "xxxxx", true));
        info1.writeXML(writer1);
        writer1.close();
        String result1 = new String(out1.toByteArray());
        Assert.assertTrue(result1.contains("certSecretKey"));
        Assert.assertTrue(result1.contains("certSecretKey=\"xxxxx\""));

    }

}
