package com.fr.design.extra;

import com.fr.invoke.Reflect;
import org.junit.Assert;
import org.junit.Test;

/**
 * @author Lucian.Chen
 * @version 10.0
 * Created by Lucian.Chen on 2020/12/18
 */
public class PluginUtilsTest {

    @Test
    public void testIsCompatibleCurrentEnv() {
        Assert.assertFalse(Reflect.on(PluginUtils.class).call("isCompatibleCurrentEnv", "~9.0").get());
        Assert.assertTrue(Reflect.on(PluginUtils.class).call("isCompatibleCurrentEnv", "9.0").get());
        Assert.assertTrue(Reflect.on(PluginUtils.class).call("isCompatibleCurrentEnv", "9~").get());
        Assert.assertTrue(Reflect.on(PluginUtils.class).call("isCompatibleCurrentEnv", "10").get());
        Assert.assertFalse(Reflect.on(PluginUtils.class).call("isCompatibleCurrentEnv", "11").get());
    }

}
