package com.fr.design.formula;

import junit.framework.TestCase;
import org.junit.Assert;
import org.junit.Test;

import java.util.Arrays;


/**
 * @author hades
 * @version 10.0
 * Created by hades on 2020/5/20
 */
public class FormulaPaneTest extends TestCase {

    @Test
    public void testSimilarComparator() {
        String[] strs = new String[] {"ScriptEval", "SPLIT", "SUMPRECISE"};
        String[] result = new String[] {"SPLIT", "ScriptEval", "SUMPRECISE"};
        Arrays.sort(strs, new FormulaPane.SimilarComparator("sp"));
        Assert.assertArrayEquals(result, strs);
    }

    @Test
    public void test4SimilarComparator() {
        String[] strs = new String[]{"TESTB", "TESTACD", "Ftest", "Gtest", "TEST"};
        String[] result = new String[]{"TEST", "TESTACD", "TESTB", "Ftest", "Gtest"};
        Arrays.sort(strs, new FormulaPane.SimilarComparator("test"));
        Assert.assertArrayEquals(result, strs);
    }

}
