package com.fr.design.gui.itextfield;

import org.junit.Assert;
import org.junit.Test;

/**
 * @author Destiny.Lin
 * @version 10.0
 * created by Destiny.Lin on 2022-07-11
 */
public class UINumberFieldTest {

    @Test
    public void testUINumberFieldTest(){
        UINumberField uiNumberField = new UINumberField();
        uiNumberField.setFieldDocument();
        //异常输入测试
        uiNumberField.setText("-.1");
        Assert.assertEquals("",uiNumberField.getText());
        uiNumberField.setText(".-1");
        Assert.assertEquals("",uiNumberField.getText());
        uiNumberField.setText("1-");
        Assert.assertEquals("",uiNumberField.getText());
        uiNumberField.setText("1-1");
        Assert.assertEquals("",uiNumberField.getText());
        uiNumberField.setText("1   ");
        Assert.assertEquals("",uiNumberField.getText());
        uiNumberField.setText(".1");
        Assert.assertEquals("",uiNumberField.getText());
        uiNumberField.setText("1   -");
        Assert.assertEquals("",uiNumberField.getText());

        //正常输入测试
        uiNumberField.setText("0.1");
        Assert.assertEquals("0.1",uiNumberField.getText());
        uiNumberField.setText("1");
        Assert.assertEquals("1",uiNumberField.getText());
        uiNumberField.setText("-1.5");
        Assert.assertEquals("-1.5",uiNumberField.getText());
        uiNumberField.setText("-123.123");
        Assert.assertEquals("-123.123",uiNumberField.getText());

    }
}
