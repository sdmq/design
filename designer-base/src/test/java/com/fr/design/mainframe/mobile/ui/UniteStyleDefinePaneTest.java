package com.fr.design.mainframe.mobile.ui;

import com.fr.form.ui.container.cardlayout.WCardTagLayout;
import com.fr.general.cardtag.mobile.UniteStyle;
import com.fr.invoke.Reflect;
import junit.framework.TestCase;
import org.junit.Test;

import java.awt.*;

public class UniteStyleDefinePaneTest extends TestCase {

    @Test
    public void testDefaultConfig() {
        UniteStyleDefinePane definePane = new UniteStyleDefinePane(new WCardTagLayout());
        Reflect.on(definePane).call("initDefaultConfig");

        double paddingTop = Reflect.on(definePane).field("paddingTopSpinner").call("getValue").get();
        assertEquals((int)paddingTop, UniteStyle.DEFAULT_PADDING_TOP);
        double paddingBottom = Reflect.on(definePane).field("paddingBottomSpinner").call("getValue").get();
        assertEquals((int)paddingBottom, UniteStyle.DEFAULT_PADDING_BOTTOM);
        double paddingLeft = Reflect.on(definePane).field("paddingLeftSpinner").call("getValue").get();
        assertEquals((int)paddingLeft, UniteStyle.DEFAULT_PADDING_LEFT);
        double paddingRight = Reflect.on(definePane).field("paddingRightSpinner").call("getValue").get();
        assertEquals((int)paddingRight, UniteStyle.DEFAULT_PADDING_RIGHT);

        Color initialBackgroundColor = Reflect.on(definePane).field("initialBackgroundColorBox").call("getSelectObject").get();
        assertEquals(initialBackgroundColor, UniteStyle.DEFAULT_INITIAL_BACKGROUND_COLOR);
        Color selectedBackgroundColor = Reflect.on(definePane).field("selectedBackgroundColorBox").call("getSelectObject").get();
        assertEquals(selectedBackgroundColor, UniteStyle.DEFAULT_SELECTED_BACKGROUND_COLOR);

        int lineStyle = Reflect.on(definePane).field("borderWidthComboBox").call("getSelectedLineStyle").get();
        assertEquals(lineStyle, UniteStyle.DEFAULT_BORDER_LINE.lineStyle);
        Color borderColor = Reflect.on(definePane).field("borderColorBox").call("getSelectObject").get();
        assertEquals(borderColor, UniteStyle.DEFAULT_BORDER_LINE.color);
        double borderRadius = Reflect.on(definePane).field("borderRadiusSpinner").call("getValue").get();
        assertEquals((int)borderRadius, UniteStyle.DEFAULT_BORDER_RADIUS);
    }
}