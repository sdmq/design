package com.fr.design.update.utils;

import com.fr.decision.update.data.UpdateConstants;
import com.fr.stable.StableUtils;
import org.junit.Assert;
import org.junit.Test;

import java.io.File;

/**
 * @author Bryant
 * @version 10.0
 * Created by Bryant on 2020-09-25
 */
public class UpdateFileUtilsTest {

    @Test
    public void testListFilteredFiles() {
        File des = new File(StableUtils.pathJoin(StableUtils.getInstallHome(), UpdateConstants.DESIGNER_BACKUP_DIR, "test", UpdateConstants.BACKUPPATH, "test"));
        File env = new File(StableUtils.pathJoin(StableUtils.getInstallHome(), UpdateConstants.DESIGNER_BACKUP_DIR, "test", UpdateConstants.DESIGNERBACKUPPATH, "test"));
        StableUtils.mkdirs(des);
        StableUtils.mkdirs(env);
        String[] result = UpdateFileUtils.listBackupVersions();
        Assert.assertEquals(1, result.length);
        StableUtils.deleteFile(new File(StableUtils.pathJoin(StableUtils.getInstallHome(), UpdateConstants.DESIGNER_BACKUP_DIR)));
    }
}
