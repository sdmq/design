package com.fr.design.utils;

import com.fr.base.Parameter;
import junit.framework.TestCase;

/**
 * @Author: Yuan.Wang
 * @Date: 2020/8/11
 */
public class ParameterUtilsTest extends TestCase {
    public void testAnalyzeAndUnionParameters() {
        String[] paramTexts = {"${a}${b}${d}", ""};
        Parameter[] oldParameters = new Parameter[]{new Parameter("c"), new Parameter("b"), new Parameter("a")};
        Parameter[] rightResult = new Parameter[]{new Parameter("b"), new Parameter("a"), new Parameter("d")};
        Parameter[] result = ParameterUtils.analyzeAndUnionParameters(paramTexts, oldParameters);
        assertEquals(result.length, rightResult.length);
        for (int i = 0; i < rightResult.length; i++) {
            assertEquals(rightResult[i].getName(), result[i].getName());
        }

        paramTexts = new String[]{"${a}${b}${d}", ""};
        oldParameters = new Parameter[]{};
        rightResult = new Parameter[]{new Parameter("a"), new Parameter("b"), new Parameter("d")};
        result = ParameterUtils.analyzeAndUnionParameters(paramTexts, oldParameters);
        assertEquals(result.length,rightResult.length);

        paramTexts = new String[]{"${b}", ""};
        oldParameters = new Parameter[]{new Parameter("b"), new Parameter("a"), new Parameter("d")};
        rightResult = new Parameter[]{new Parameter("b")};
        result = ParameterUtils.analyzeAndUnionParameters(paramTexts, oldParameters);
        assertEquals(result.length,rightResult.length);
        assertEquals(result.length, rightResult.length);
        for (int i = 0; i < rightResult.length; i++) {
            assertEquals(rightResult[i].getName(), result[i].getName());
        }

    }
}
