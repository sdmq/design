!(function () {
    BI.constant("bi.constant.report.server.param_setting", [
        {
            text: BI.i18nText("Fine-Design_Report_WEB_Pagination_Setting"),
            cls: "tab-item",
            value: BICst.REPORT_SERVER_PARAM.SPLIT_PAGE_PREVIEW_SETTING
        }, {
            text: BI.i18nText("Fine-Design_Report_WEB_Write_Setting"),
            cls: "tab-item",
            value: BICst.REPORT_SERVER_PARAM.FORM_PAGE_SETTING
        }, {
            text: BI.i18nText("Fine-Design_Report_Data_Analysis_Settings"),
            cls: "tab-item",
            value: BICst.REPORT_SERVER_PARAM.DATA_ANALYSIS_SETTING
        }, {
            text: BI.i18nText("Fine-Design_Report_ReportServerP_Import_Css"),
            cls: "tab-item",
            value: BICst.REPORT_SERVER_PARAM.IMPORT_CSS
        }, {
            text: BI.i18nText("Fine-Design_Report_ReportServerP_Import_JavaScript"),
            cls: "tab-item",
            value: BICst.REPORT_SERVER_PARAM.IMPORT_JS
        }, {
            text: BI.i18nText("Fine-Design_Report_Error_Handler_Template"),
            cls: "tab-item",
            value: BICst.REPORT_SERVER_PARAM.ERROR_TEMPLATE_DEFINE
        }, {
            text: BI.i18nText("Fine-Design_Report_Print_Setting"),
            cls: "tab-item",
            value: BICst.REPORT_SERVER_PARAM.PRINT_SETTING
        }]
    );
})();