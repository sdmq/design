package com.fr.design.chart;

import com.fr.base.chart.chartdata.CallbackEvent;
import com.fr.chart.chartattr.ChartCollection;
import com.fr.design.chart.auto.AutoTypeCalculate;
import com.fr.design.data.DesignTableDataManager;
import com.fr.design.data.datapane.TableDataComboBox;
import com.fr.design.data.tabledata.wrapper.TableDataWrapper;
import com.fr.design.dialog.FineJOptionPane;
import com.fr.design.gui.ibutton.UIButton;
import com.fr.design.gui.icombocheckbox.UIComboCheckBox;
import com.fr.design.gui.ilable.UILabel;
import com.fr.design.gui.iprogressbar.AutoProgressBar;
import com.fr.design.i18n.Toolkit;
import com.fr.design.layout.FRGUIPaneFactory;
import com.fr.design.mainframe.chart.info.ChartInfoCollector;
import com.fr.design.utils.gui.GUICoreUtils;
import com.fr.general.GeneralUtils;
import com.fr.log.FineLoggerFactory;
import com.fr.plugin.chart.vanchart.VanChart;

import javax.swing.BorderFactory;
import javax.swing.DefaultListCellRenderer;
import javax.swing.DefaultListModel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.ListCellRenderer;
import javax.swing.SwingWorker;
import javax.swing.UIManager;
import javax.swing.event.ListSelectionListener;
import javax.swing.plaf.SplitPaneUI;
import javax.swing.plaf.basic.BasicSplitPaneUI;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CancellationException;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

/**
 * @author Bjorn
 * @version 10.0
 * Created by Bjorn on 2020-05-29
 */
public class AutoChartTypePane extends ChartWizardPane implements CallbackEvent {

    private JList chartViewList;
    private DefaultListModel chartResultModel;
    private UIButton refreshButton;

    private TableDataComboBox tableNameComboBox;
    private UIComboCheckBox dataFieldBox;

    private AutoProgressBar connectionBar;
    private SwingWorker worker;

    private static final String MESSAGE = Toolkit.i18nText("Fine-Design_Chart_Auto_No_Match");

    public AutoChartTypePane() {
        this.setLayout(FRGUIPaneFactory.createBorderLayout());
        initButtonGroup();
        initRefreshLabel();
        initDataFiledBox();
        JPanel contentPane = createContentPane();

        chartViewList = new JList();

        chartResultModel = new DefaultListModel();
        chartViewList.setModel(chartResultModel);
        chartViewList.setVisibleRowCount(0);
        chartViewList.setLayoutOrientation(JList.HORIZONTAL_WRAP);
        chartViewList.setCellRenderer(iconCellRenderer);

        JScrollPane subListPane = new JScrollPane(chartViewList);
        subListPane.setBorder(BorderFactory.createTitledBorder(Toolkit.i18nText("Fine-Design_Chart_Recommended_Chart")));

        JSplitPane splitPane = new JSplitPane(JSplitPane.VERTICAL_SPLIT, false, contentPane, subListPane);

        SplitPaneUI ui = splitPane.getUI();
        if (ui instanceof BasicSplitPaneUI) {
            ((BasicSplitPaneUI) ui).getDivider().setBorder(null);
        }
        splitPane.setDividerLocation(60);
        this.add(splitPane);
    }

    ListCellRenderer iconCellRenderer = new DefaultListCellRenderer() {
        public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected, boolean cellHasFocus) {
            super.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);
            this.setText("");

            AutoChartIcon chartIcon = (AutoChartIcon) value;
            this.setIcon(chartIcon);
            setHorizontalAlignment(UILabel.CENTER);
            if (isSelected) {
                // 深蓝色.
                this.setBackground(new Color(57, 107, 181));
                this.setBorder(GUICoreUtils.createTitledBorder(chartIcon.getChartName(), Color.WHITE));
            } else {
                this.setBorder(GUICoreUtils.createTitledBorder(chartIcon.getChartName()));
            }
            return this;
        }
    };

    private JPanel createContentPane() {
        JPanel panel = FRGUIPaneFactory.createNormalFlowInnerContainer_S_Pane();

        JPanel tableDataPane = FRGUIPaneFactory.createNormalFlowInnerContainer_S_Pane();
        panel.add(tableDataPane);
        tableDataPane.add(new UILabel(Toolkit.i18nText("Fine-Design_Chart_Table_Data") + ":"));
        tableNameComboBox.setPreferredSize(new Dimension(126, 20));
        tableDataPane.add(tableNameComboBox);

        JPanel areaNamePane = FRGUIPaneFactory.createNormalFlowInnerContainer_S_Pane();
        panel.add(areaNamePane);
        areaNamePane.add(new UILabel(Toolkit.i18nText("Fine-Design_Chart_Data_Field") + ":"));
        areaNamePane.add(dataFieldBox);
        panel.add(refreshButton);
        panel.setBorder(BorderFactory.createEmptyBorder(10, 0, 0, 0));
        return panel;
    }

    private void initButtonGroup() {
        dataFieldBox = new UIComboCheckBox(new Object[0]);
        dataFieldBox.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                checkButtonState();
            }
        });
    }

    private void initDataFiledBox() {
        tableNameComboBox = new TableDataComboBox(DesignTableDataManager.getEditingTableDataSource());
        tableNameComboBox.addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent e) {
                if (e.getStateChange() == ItemEvent.SELECTED) {
                    refreshBox();
                    checkButtonState();
                }
            }
        });
    }

    private void checkButtonState() {
        if (tableNameComboBox.getSelectedItem() != null && dataFieldBox.getSelectedValues().length > 0) {
            refreshButton.setEnabled(true);
        } else {
            refreshButton.setEnabled(false);
        }
    }

    public void registsListAction(ListSelectionListener listSelectionListener) {
        chartViewList.addListSelectionListener(listSelectionListener);
    }

    private void refreshBox() {
        TableDataWrapper dataWrap = tableNameComboBox.getSelectedItem();

        if (dataWrap == null) {
            return;
        }
        dataFieldBox.clearText();

        List<String> columnNameList = dataWrap.calculateColumnNameList();

        dataFieldBox.refreshCombo(columnNameList.toArray());
    }

    private void initRefreshLabel() {
        refreshButton = new UIButton(Toolkit.i18nText("Fine-Design_Chart_Recommend"));
        refreshButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                refreshButton.setEnabled(false);
                calculateAutoChart();
            }
        });
        refreshButton.setEnabled(false);
    }

    private void calculateAutoChart() {
        connectionBar = new AutoProgressBar(this, Toolkit.i18nText("Fine-Design_Chart_Generate_Recommended_Chart"), "", 0, 100) {
            public void doMonitorCanceled() {
                refreshButton.setEnabled(true);
                worker.cancel(true);
            }
        };
        setWorker();
        worker.execute();
    }

    private void setWorker() {

        worker = new SwingWorker<List<VanChart>, Void>() {
            protected List<VanChart> doInBackground() {
                connectionBar.start();
                chartResultModel.clear();
                List<String> columnList = new ArrayList<>();
                Object[] selectedValues = dataFieldBox.getSelectedValues();
                for (Object value : selectedValues) {
                    columnList.add(GeneralUtils.objectToString(value));
                }
                List<VanChart> vanChartList = AutoTypeCalculate.calculateType(tableNameComboBox.getSelectedItem().getTableDataName(), columnList);
                connectionBar.close();
                return vanChartList;
            }

            public void done() {
                try {
                    List<VanChart> vanChartList = get();
                    if (vanChartList != null && !vanChartList.isEmpty()) {
                        for (VanChart vanChart : vanChartList) {
                            ChartCollection chartCollection = new ChartCollection(vanChart);
                            AutoChartIcon autoChartIcon = new AutoChartIcon(chartCollection);
                            autoChartIcon.registerCallBackEvent(AutoChartTypePane.this);
                            chartResultModel.addElement(autoChartIcon);
                        }
                        chartViewList.setSelectedIndex(0);
                    } else {
                        FineJOptionPane.showMessageDialog(AutoChartTypePane.this, MESSAGE,
                                Toolkit.i18nText("Fine-Design_Basic_Message"), JOptionPane.INFORMATION_MESSAGE, UIManager.getIcon("OptionPane.informationIcon"));
                    }
                } catch (Exception e) {
                    if (!(e instanceof CancellationException)) {
                        FineLoggerFactory.getLogger().error(e.getMessage(), e);
                        FineJOptionPane.showMessageDialog(AutoChartTypePane.this, e.getMessage(),
                                Toolkit.i18nText("Fine-Design_Basic_Error"), JOptionPane.ERROR_MESSAGE, UIManager.getIcon("OptionPane.errorIcon"));
                    }
                } finally {
                    connectionBar.close();
                    refreshButton.setEnabled(true);
                }
            }
        };
    }

    @Override
    public void populate(ChartCollection cc) {

    }

    @Override
    public void update(ChartCollection cc) {
        update(cc, null);
    }

    public void populate(String tableName, String[] dataFields) {
        tableNameComboBox.setSelectedTableDataByName(tableName);
        Map<Object, Boolean> map = new HashMap();
        for (String dataField : dataFields) {
            map.put(dataField, true);
        }
        dataFieldBox.setSelectedValues(map);
        if (refreshButton.isEnabled()) {
            refreshButton.setEnabled(false);
            calculateAutoChart();
        }
    }

    public void update(ChartCollection cc, String createTime) {
        if (chartViewList.getSelectedIndex() < 0) {
            return;
        }
        AutoChartIcon chartIcon = (AutoChartIcon) chartViewList.getSelectedValue();
        VanChart vanChart = chartIcon.getChartCollection().getSelectedChartProvider(VanChart.class);
        if (cc.getChartCount() > 0) {
            VanChart selectedChartProvider = cc.getSelectedChartProvider(VanChart.class);
            if (selectedChartProvider.getChartUuid() != null) {
                vanChart.setUuid(selectedChartProvider.getChartUuid());
            }
            cc.setSelectChart(vanChart);
            ChartInfoCollector.getInstance().updateChartTypeTime(vanChart, null, true);
        } else {
            cc.addChart(vanChart);
            //记录埋点
            ChartInfoCollector.getInstance().collection(vanChart, createTime, false, true);
        }
    }

    @Override
    public void callback() {
        this.repaint();
    }
}
