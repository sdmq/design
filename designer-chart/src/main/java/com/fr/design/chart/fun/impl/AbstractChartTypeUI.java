package com.fr.design.chart.fun.impl;

import com.fr.design.chart.fun.ChartTypeUIProvider;
import com.fr.design.chartx.impl.AbstractDataPane;
import com.fr.design.gui.frpane.AttributeChangeListener;
import com.fr.design.mainframe.chart.AbstractChartAttrPane;
import com.fr.design.mainframe.chart.gui.type.AbstractChartTypePane;
import com.fr.design.mainframe.chart.mode.ChartEditContext;
import com.fr.stable.fun.impl.AbstractProvider;
import com.fr.stable.fun.mark.API;

/**
 * Created by shine on 2019/09/03.
 */
@API(level = ChartTypeUIProvider.CURRENT_API_LEVEL)
public abstract class AbstractChartTypeUI extends AbstractProvider implements ChartTypeUIProvider {

    @Override
    public AbstractChartTypePane getPlotTypePane() {
        return ChartEditContext.duchampMode() ? new InvisibleChartTypePane() : new DefaultChartTypePane();
    }

    @Override
    public abstract AbstractDataPane getChartDataPane(AttributeChangeListener listener);

    @Override
    public abstract AbstractChartAttrPane[] getAttrPaneArray(AttributeChangeListener listener);

    @Override
    public String[] getSubName() {
        return new String[]{getName()};
    }

    @Override
    public int currentAPILevel() {
        return CURRENT_API_LEVEL;
    }

    @Override
    public String mark4Provider() {
        return getClass().getName();
    }
}