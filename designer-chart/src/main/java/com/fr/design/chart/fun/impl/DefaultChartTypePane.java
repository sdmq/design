package com.fr.design.chart.fun.impl;

import com.fr.chart.charttypes.ChartTypeManager;
import com.fr.chartx.attr.ChartProvider;
import com.fr.design.ChartTypeInterfaceManager;
import com.fr.design.mainframe.chart.gui.type.AbstractChartTypePane;

/**
 * @author shine
 * @version 10.0
 * Created by shine on 2020/7/8
 */
public class DefaultChartTypePane<T extends ChartProvider> extends AbstractChartTypePane<T> {

    @Override
    protected String[] getTypeIconPath() {
        return ChartTypeInterfaceManager.getInstance().getDemoImagePath(this.getPlotID());
    }

    @Override
    protected String[] getTypeTipName() {
        return ChartTypeInterfaceManager.getInstance().getSubName(this.getPlotID());
    }

    public ChartProvider getDefaultChart() {
        return ChartTypeManager.getInstance().getChartTypes(this.getPlotID())[0];
    }

    public String title4PopupWindow() {
        return ChartTypeInterfaceManager.getInstance().getName(this.getPlotID());
    }

    @Override
    public void populateBean(T ob) {
    }


}
