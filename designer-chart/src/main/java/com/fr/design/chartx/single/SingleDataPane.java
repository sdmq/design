package com.fr.design.chartx.single;

import com.fr.chartx.data.AbstractDataDefinition;
import com.fr.design.beans.BasicBeanPane;
import com.fr.design.beans.FurtherBasicBeanPane;
import com.fr.design.chartx.data.DataLayoutHelper;
import com.fr.design.chartx.fields.AbstractCellDataFieldsPane;
import com.fr.design.chartx.fields.AbstractDataSetFieldsPane;
import com.fr.design.gui.frpane.UIComboBoxPane;
import com.fr.design.i18n.Toolkit;

import javax.swing.JPanel;
import java.awt.BorderLayout;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by shine on 2019/5/21.
 */
public class SingleDataPane extends BasicBeanPane<AbstractDataDefinition> {

    private UIComboBoxPane<AbstractDataDefinition> comboBoxPane;

    private DataSetPane dataSetPane;

    private CellDataPane cellDataPane;

    public SingleDataPane(AbstractDataSetFieldsPane dataSetFieldsPane, AbstractCellDataFieldsPane cellDataFieldsPane) {
        initComps(dataSetFieldsPane, cellDataFieldsPane);
    }

    private void initComps(AbstractDataSetFieldsPane dataSetFieldsPane, AbstractCellDataFieldsPane cellDataFieldsPane) {

        cellDataPane = new CellDataPane(cellDataFieldsPane);
        dataSetPane = new DataSetPane(dataSetFieldsPane);

        comboBoxPane = new UIComboBoxPane<AbstractDataDefinition>() {
            @Override
            protected List<FurtherBasicBeanPane<? extends AbstractDataDefinition>> initPaneList() {
                List<FurtherBasicBeanPane<? extends AbstractDataDefinition>> list = new ArrayList<FurtherBasicBeanPane<? extends AbstractDataDefinition>>();
                list.add(dataSetPane);
                list.add(cellDataPane);
                return list;
            }

            protected void initLayout() {
                this.setLayout(new BorderLayout(0, 6));
                JPanel northPane = DataLayoutHelper.createDataLayoutPane(Toolkit.i18nText("Fine-Design_Chart_Data_Source"), jcb);
                DataLayoutHelper.addNormalBorder(northPane);

                this.add(northPane, BorderLayout.NORTH);
                this.add(cardPane, BorderLayout.CENTER);
            }

            @Override
            protected String title4PopupWindow() {
                return null;
            }
        };


        this.setLayout(new BorderLayout());
        this.add(comboBoxPane, BorderLayout.CENTER);
    }

    @Override
    public void populateBean(AbstractDataDefinition ob) {
        comboBoxPane.populateBean(ob);
    }

    @Override
    public AbstractDataDefinition updateBean() {
        return comboBoxPane.updateBean();
    }

    @Override
    protected String title4PopupWindow() {
        return null;
    }
}
