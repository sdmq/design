/*
 * Copyright(c) 2001-2011, FineReport Inc, All Rights Reserved.
 */
package com.fr.design.mainframe;


import com.fr.base.BaseUtils;
import com.fr.base.chart.BaseChartCollection;
import com.fr.chart.chartattr.ChartCollection;
import com.fr.chart.charttypes.ChartTypeManager;
import com.fr.chartx.attr.ChartProvider;
import com.fr.design.ChartTypeInterfaceManager;
import com.fr.design.designer.TargetComponent;
import com.fr.design.gui.chart.BaseChartPropertyPane;
import com.fr.design.gui.chart.ChartEditPaneProvider;
import com.fr.design.gui.frpane.UITitlePanel;
import com.fr.design.mainframe.chart.ChartEditPane;
import com.fr.design.utils.gui.GUICoreUtils;

import javax.swing.BorderFactory;
import javax.swing.Icon;
import java.awt.BorderLayout;
import java.awt.Component;

public class ChartPropertyPane extends BaseChartPropertyPane {

    /**
     * 创建图表属性表实例.
     */
    private synchronized static ChartPropertyPane getInstance() {
        return new ChartPropertyPane();
    }

    protected TargetComponentContainer container = new TargetComponentContainer();

    protected ChartEditPane chartEditPane;

    private ChartPropertyPane() {
        initComponent();
    }

    protected void initComponent() {
        this.setLayout(new BorderLayout());
        this.setBorder(BorderFactory.createEmptyBorder(10, 0, 0, 0));
    }

    @Override
    public void updateChartEditPane(String plotID) {
        chartEditPane = ChartTypeInterfaceManager.getInstance().getChartEditPane(plotID);
        chartEditPane.setContainer(container);
        resetChartEditPane();
    }

    @Override
    public ChartEditPaneProvider getChartEditPane() {
        return chartEditPane;
    }


    protected void resetChartEditPane() {
        removeChartEditPane();
        addChartEditPane(this.chartEditPane);
        validate();
        repaint();
        revalidate();
    }

    /**
     * @Description 去除BorderLayout.CENTER位置的元素
     * @return void
     * @Author Henry.Wang
     * @Date 2021/4/9 13:54
     **/
    private void removeChartEditPane() {
        BorderLayout layout = (BorderLayout) this.getLayout();
        Component component = layout.getLayoutComponent(BorderLayout.CENTER);
        if (component != null)
            this.remove(component);
    }

    /**
     * @Description 把chartEditPane加到BorderLayout.CENTER中
     * @param: chartEditPane
     * @return void
     * @Author Henry.Wang
     * @Date 2021/4/9 13:55
     **/
    private void addChartEditPane(ChartEditPane chartEditPane) {
        this.add(chartEditPane, BorderLayout.CENTER);
    }

    /**
     * 感觉ChartCollection加载图表属性界面.
     * @param collection  收集图表
     * @param ePane  面板
     */
    public void populateChartPropertyPane(ChartCollection collection, TargetComponent<?> ePane) {
        String chartID = collection.getSelectedChartProvider(ChartProvider.class).getID();
        updateChartEditPane(collection.getSelectedChartProvider(ChartProvider.class).getID());
        setSupportCellData(true);
        this.container.setEPane(ePane);

        if (ChartTypeManager.getInstance().chartExit(chartID)) {
            chartEditPane.populate(collection);
        } else {
            GUICoreUtils.setEnabled(chartEditPane, false);
        }
    }

    /**
     * 感觉ChartCollection加载图表属性界面.
     * @param collection  收集图表
     * @param ePane  面板
     */
    public void populateChartPropertyPane(BaseChartCollection collection, TargetComponent<?> ePane) {
        if (collection instanceof ChartCollection) {
            populateChartPropertyPane((ChartCollection) collection, ePane);
        }
    }

    /**
     * 返回View的标题.
     */
    public String getViewTitle() {
        return com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Chart_Cell_Element_Property_Table");
    }

    /**
     * 返回View的Icon地址.
     */
    public Icon getViewIcon() {
        return BaseUtils.readIcon("/com/fr/design/images/m_report/qb.png");
    }

    /**
     *  预定义定位
     * @return 定位
     */
    public Location preferredLocation() {
        return Location.WEST_BELOW;
    }

    /**
     * 创建标题Panel
     * @return 标题panel
     */
    public UITitlePanel createTitlePanel() {
        return new UITitlePanel(this);
    }

    /**
     * 刷新Dockview
     */
    public void refreshDockingView() {
        // TODO Auto-generated method stub

    }

    /**
     * 设置是否支持单元格数据.
     */
    public void setSupportCellData(boolean supportCellData) {
        if (chartEditPane != null) {
            chartEditPane.setSupportCellData(supportCellData);
        }
    }
}