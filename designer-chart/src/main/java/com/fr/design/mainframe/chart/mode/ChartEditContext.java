package com.fr.design.mainframe.chart.mode;

import com.fr.common.annotations.Open;

/**
 * @author shine
 * @version 10.0
 * Created by shine on 2021/6/4
 */
@Open
public class ChartEditContext {

    private static ChartEditMode current = ChartEditMode.NORMAL;

    public static void switchTo(ChartEditMode mode) {
        current = mode;
    }

    public static boolean duchampMode() {
        return current == ChartEditMode.DUCHAMP;
    }

    public static boolean normalMode() {
        return current == ChartEditMode.NORMAL;
    }
}
