package com.fr.design.module;

import com.fr.base.ChartPreStyleConfig;
import com.fr.base.svg.IconUtils;
import com.fr.concurrent.NamedThreadFactory;
import com.fr.design.actions.UpdateAction;
import com.fr.design.dialog.BasicDialog;
import com.fr.design.dialog.DialogActionAdapter;
import com.fr.design.mainframe.DesignerContext;
import com.fr.design.mainframe.DesignerFrame;
import com.fr.design.menu.MenuKeySet;
import com.fr.module.ModuleContext;
import com.fr.transaction.CallBackAdaptor;
import com.fr.transaction.Configurations;
import com.fr.transaction.WorkerFacade;
import com.fr.van.chart.designer.component.VanChartFillStylePane;

import javax.swing.KeyStroke;
import java.util.concurrent.ExecutorService;
import java.awt.event.ActionEvent;

/**
 * 图表预定义样式Action.
 * @author kunsnat E-mail:kunsnat@gmail.com
 * @version 创建时间：2013-8-20 下午04:38:48
 */
public class ChartPreStyleAction extends UpdateAction {

	private static ExecutorService refreshDesignPool = ModuleContext.getExecutor().newFixedThreadPool(
			10, new NamedThreadFactory("refreshChartStylePane"));

	public ChartPreStyleAction() {
        this.setMenuKeySet(CHART_DEFAULT_STYLE);
        this.setName(getMenuKeySet().getMenuKeySetName()+ "...");
        this.setMnemonic(getMenuKeySet().getMnemonic());
		this.setSmallIcon("com/fr/design/images/chart/ChartType");
		this.generateAndSetSearchText(ChartPreStyleManagerPane.class.getName());
	}

    /**
     * 动作
     * @param e 事件
     */
	public void actionPerformed(ActionEvent e) {
		DesignerFrame designerFrame = DesignerContext.getDesignerFrame();
		final ChartPreStyleManagerPane pane = new ChartPreStyleManagerPane();
		BasicDialog dialog = pane.showWindow(designerFrame);
		dialog.addDialogActionListener(new DialogActionAdapter() {
			@Override
			public void doOk() {
				Configurations.modify(new WorkerFacade(ChartPreStyleConfig.class) {
					@Override
					public void run() {
						pane.updateBean();
					}

				}.addCallBack(new CallBackAdaptor() {
					@Override
					public void afterCommit() {
						DesignerFrame frame = DesignerContext.getDesignerFrame();
						if (frame != null) {
							frame.repaint();
						}
						if (refreshDesignPool.isTerminated()) {
							synchronized (refreshDesignPool) {
								if (refreshDesignPool.isTerminated()) {
									refreshDesignPool = ModuleContext.getExecutor().newFixedThreadPool(
											10, new NamedThreadFactory("refreshChartStylePane"));
								}
							}
						}
						refreshDesignPool.execute(new Runnable() {
							@Override
							public void run() {
								DesignerContext.getDesignerBean(VanChartFillStylePane.name).refreshBeanElement();
							}
						});
					}
				}));
			}
        });

		pane.populateBean();
		dialog.setVisible(true);

	}

	public static final MenuKeySet CHART_DEFAULT_STYLE = new MenuKeySet() {
		@Override
		public char getMnemonic() {
			return 'C';
		}

		@Override
		public String getMenuName() {
			return com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Chart_Server_PreStyle");
		}

		@Override
		public KeyStroke getKeyStroke() {
			return null;
		}
	};
}
