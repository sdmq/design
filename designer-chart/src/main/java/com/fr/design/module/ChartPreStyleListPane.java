package com.fr.design.module;

import com.fr.base.ChartColorMatching;
import com.fr.base.ChartPreStyleConfig;
import com.fr.base.Utils;
import com.fr.design.beans.BasicBeanPane;
import com.fr.design.dialog.FineJOptionPane;
import com.fr.design.gui.controlpane.JListControlPane;
import com.fr.design.gui.controlpane.NameObjectCreator;
import com.fr.design.gui.controlpane.NameableCreator;
import com.fr.design.gui.controlpane.ShortCut4JControlPane;
import com.fr.design.gui.ilist.JNameEdList;
import com.fr.design.gui.ilist.ModNameActionListener;
import com.fr.design.i18n.Toolkit;
import com.fr.design.menu.ShortCut;
import com.fr.general.ComparatorUtils;
import com.fr.general.NameObject;
import com.fr.stable.Nameable;
import com.fr.stable.StringUtils;

import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

/**
 * @author Bjorn
 * @version 10.0
 * Created by Bjorn on 2020-07-08
 */
public class ChartPreStyleListPane extends JListControlPane {

    ChartPreStyleManagerPane chartPreStyleManagerPane;

    public ChartPreStyleListPane(ChartPreStyleManagerPane chartPreStyleManagerPane) {
        super();
        this.chartPreStyleManagerPane = chartPreStyleManagerPane;
        initListener();
        addModNameActionListener((int index, String oldName, String newName) -> {
            if (ComparatorUtils.equals(oldName, newName)) {
                return;
            }
            String[] allNames = nameableList.getAllNames();
            allNames[index] = StringUtils.EMPTY;
            if (StringUtils.isEmpty(newName)) {
                showTipDialog(Toolkit.i18nText("Fine-Design_Chart_Fill_Style_Empty_Name_Tip"));
                nameableList.setNameAt(oldName, index);
                return;
            }
            if (isNameRepeated(new List[]{Arrays.asList(allNames)}, newName)) {
                showTipDialog(Toolkit.i18nText("Fine-Design_Chart_Fill_Style_Exist_Name_Tip", newName));
                nameableList.setNameAt(oldName, index);
                return;
            }
            populateSelectedValue();
        });
    }

    private void showTipDialog(String content) {
        FineJOptionPane.showMessageDialog(SwingUtilities.getWindowAncestor(ChartPreStyleListPane.this),
                content,
                Toolkit.i18nText("Fine-Design_Basic_Alert"),
                JOptionPane.WARNING_MESSAGE);
    }

    @Override
    protected JNameEdList createJNameList() {
        JNameEdList jNameList = super.createJNameList();
        jNameList.setReplaceEmptyName(false);
        return jNameList;
    }

    /**
     * 创建有名字的creator
     *
     * @return 有名字的creator数组
     */
    @Override
    public NameableCreator[] createNameableCreators() {
        return new NameableCreator[]{
                new NameObjectCreator(com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Chart_PreStyle_Duplicate"),
                        ChartColorMatching.class, ChartPreStylePane.class)
        };
    }

    @Override
    protected String title4PopupWindow() {
        return com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Chart_PreStyle");
    }

    @Override
    public BasicBeanPane createPaneByCreators(NameableCreator creator) {
        return new ChartPreStylePane() {
            @Override
            protected void refreshWhenStyleChange(ChartColorMatching preStyle) {
                super.refreshWhenStyleChange(preStyle);
                chartPreStyleManagerPane.refreshDefaultColorBox();
            }
        };
    }


    protected ShortCut4JControlPane[] createShortcuts() {
        return new ShortCut4JControlPane[]{
                shortCutFactory.addItemShortCut(),
                createRemoveItemShortCut(),
                shortCutFactory.copyItemShortCut(),
                shortCutFactory.moveUpItemShortCut(),
                shortCutFactory.moveDownItemShortCut(),
                shortCutFactory.sortItemShortCut()
        };
    }

    private ShortCut4JControlPane createRemoveItemShortCut() {
        ShortCut4JControlPane shortCut4JControlPane = shortCutFactory.removeItemShortCut();
        //替换删除按钮的check事件。
        ShortCut shortCut = shortCut4JControlPane.getShortCut();
        shortCut4JControlPane = new MoreThanOneShortCut(shortCut);
        return shortCut4JControlPane;
    }

    public void initListener() {
        nameableList.addListSelectionListener(new ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent e) {
                chartPreStyleManagerPane.refreshDefaultColorBox();
            }
        });
        nameableList.addModNameActionListener(new ModNameActionListener() {
            @Override
            public void nameModed(int index, String oldName, String newName) {
                chartPreStyleManagerPane.refreshDefaultColorBox(oldName, newName);
            }
        });
    }

    public void populateBean() {
        ChartPreStyleConfig config = ChartPreStyleConfig.getInstance().mirror();
        ArrayList list = new ArrayList();

        Iterator keys = config.names();
        while (keys.hasNext()) {
            Object key = keys.next();
            ChartColorMatching value = (ChartColorMatching) config.getPreStyle(key);

            list.add(new NameObject(Utils.objectToString(key), value));
        }

        Nameable[] values = (Nameable[]) list.toArray(new Nameable[list.size()]);
        populate(values);

        if (config.containsName(config.getCurrentStyle())) {
            this.setSelectedName(config.getCurrentStyle());
        }
    }

    public void updateBean() {
        ChartPreStyleConfig config = ChartPreStyleConfig.getInstance();

        Nameable[] values = update();
        config.clearAllPreStyle();

        for (Nameable value : values) {
            config.putPreStyle(value.getName(), ((NameObject) value).getObject());
        }
    }

    private class MoreThanOneShortCut extends ShortCut4JControlPane {
        public MoreThanOneShortCut(ShortCut shortCut) {
            this.shortCut = shortCut;
        }


        @Override
        public void checkEnable() {
            this.shortCut.setEnabled(nameableList.getModel().getSize() > 1);
        }
    }
}
