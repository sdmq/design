package com.fr.extended.chart;

import com.fr.design.gui.icombobox.UIComboBox;
import com.fr.design.i18n.Toolkit;
import com.fr.general.GeneralUtils;
import com.fr.stable.StringUtils;

import java.util.List;

/**
 * Created by shine on 2018/9/27.
 */
public class UIComboBoxWithNone extends UIComboBox {

    protected String getDefaultLocaleString() {
        return Toolkit.i18nText("Fine-Design_Chart_Use_None");
    }

    public UIComboBoxWithNone() {
        super();
        addDefaultItem();
        setDefaultSelectedItem();
    }

    protected void setDefaultSelectedItem() {
        // JComboBox.setSelectedItem(o)
        // if(o!=null)寻找o对应的下拉选项 如果找不到 还是选中之前选中的
        // 所以空字符串 不是选中空 是选中上次选中的
        // 所以这边 可以写setSelectedItem(null) or setSelectedIndex(-1)
        setSelectedIndex(-1);
    }

    @Override
    public void refreshBoxItems(List list) {
        super.refreshBoxItems(list);
        addDefaultItem();
    }

    @Override
    public void clearBoxItems() {
        super.clearBoxItems();
        addDefaultItem();
    }


    private void addDefaultItem() {
        addItem(getDefaultLocaleString());

    }

    @Override
    public void setSelectedItem(Object anObject) {
        super.setSelectedItem(anObject);

        //找不到的都选中无。中文的无 英文下是none。
        //改正：找不到的且anObject不是null的 全部选中无。
        //改正：找不到的且anObject不是null的且anObject不是空字符串的 全部选中无。
        if (getSelectedIndex() == -1 && StringUtils.isNotEmpty(GeneralUtils.objectToString(anObject))) {
            super.setSelectedItem(getDefaultLocaleString());
        }
    }

}
