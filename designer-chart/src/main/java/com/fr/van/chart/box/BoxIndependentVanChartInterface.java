package com.fr.van.chart.box;

import com.fr.chart.chartattr.Chart;
import com.fr.chart.chartattr.Plot;
import com.fr.design.beans.BasicBeanPane;
import com.fr.design.chart.fun.impl.InvisibleChartTypePane;
import com.fr.design.condition.ConditionAttributesPane;
import com.fr.design.gui.frpane.AttributeChangeListener;
import com.fr.design.i18n.Toolkit;
import com.fr.design.mainframe.chart.AbstractChartAttrPane;
import com.fr.design.mainframe.chart.gui.ChartDataPane;
import com.fr.design.mainframe.chart.gui.ChartStylePane;
import com.fr.design.mainframe.chart.gui.data.report.AbstractReportDataContentPane;
import com.fr.design.mainframe.chart.gui.data.table.AbstractTableDataContentPane;
import com.fr.design.mainframe.chart.gui.type.AbstractChartTypePane;
import com.fr.design.mainframe.chart.mode.ChartEditContext;
import com.fr.plugin.chart.attr.plot.VanChartPlot;
import com.fr.van.chart.box.data.report.BoxPlotReportDataContentPane;
import com.fr.van.chart.box.data.table.BoxPlotTableDataContentPane;
import com.fr.van.chart.designer.other.AutoRefreshPane;
import com.fr.van.chart.designer.other.AutoRefreshPaneWithoutTooltip;
import com.fr.van.chart.designer.other.VanChartInteractivePane;
import com.fr.van.chart.designer.other.VanChartOtherPane;
import com.fr.van.chart.designer.other.zoom.ZoomPane;
import com.fr.van.chart.designer.style.VanChartStylePane;
import com.fr.van.chart.vanchart.AbstractIndependentVanChartUI;

import java.awt.Component;

public class BoxIndependentVanChartInterface extends AbstractIndependentVanChartUI {

    public String getName() {
        return Toolkit.i18nText("Fine-Design_Chart_New_Box");
    }

    public String[] getSubName() {
        return new String[]{
                Toolkit.i18nText("Fine-Design_Chart_New_Box")
        };
    }

    public String[] getDemoImagePath() {
        return new String[]{
                "com/fr/plugin/chart/demo/image/box.png"
        };
    }

    public String getIconPath() {
        return "com/fr/design/images/form/toolbar/box.png";
    }

    public AbstractTableDataContentPane getTableDataSourcePane(Plot plot, ChartDataPane parent) {
        return new BoxPlotTableDataContentPane(plot, parent);
    }

    public AbstractReportDataContentPane getReportDataSourcePane(Plot plot, ChartDataPane parent) {
        return new BoxPlotReportDataContentPane(plot, parent);
    }

    public AbstractChartTypePane getPlotTypePane() {
        return ChartEditContext.duchampMode() ? new InvisibleChartTypePane() : new VanChartBoxPlotPane();
    }

    public ConditionAttributesPane getPlotConditionPane(Plot plot) {
        return new VanChartBoxConditionPane(plot);
    }

    public BasicBeanPane<Plot> getPlotSeriesPane(ChartStylePane parent, Plot plot) {
        return new VanChartBoxSeriesPane(parent, plot);
    }

    public AbstractChartAttrPane[] getAttrPaneArray(AttributeChangeListener listener) {
        VanChartStylePane stylePane = new VanChartBoxStylePane(listener);
        VanChartOtherPane otherPane = new VanChartOtherPane() {

            protected BasicBeanPane<Chart> createInteractivePane() {
                return new VanChartInteractivePane() {

                    protected Component[][] createToolBarComponents() {
                        return createToolBarComponentsWithOutSort();
                    }

                    protected AutoRefreshPane getMoreLabelPane(VanChartPlot plot) {
                        boolean isLargeModel = largeModel(plot);
                        return new AutoRefreshPaneWithoutTooltip(chart, isLargeModel);
                    }

                    protected ZoomPane createZoomPane() {
                        return new ZoomPane();
                    }
                };
            }
        };

        return new AbstractChartAttrPane[]{stylePane, otherPane};
    }

}
