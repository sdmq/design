package com.fr.van.chart.box;

import com.fr.chart.base.AttrAlpha;
import com.fr.chart.base.AttrBackground;
import com.fr.chart.chartattr.Plot;
import com.fr.design.chart.series.SeriesCondition.ChartConditionPane;
import com.fr.design.chart.series.SeriesCondition.DataSeriesConditionPane;
import com.fr.design.chart.series.SeriesCondition.LabelAlphaPane;
import com.fr.plugin.chart.base.AttrBorderWithWidth;
import com.fr.plugin.chart.box.VanChartAttrNormalMarker;
import com.fr.plugin.chart.box.VanChartAttrOutlierMarker;
import com.fr.plugin.chart.box.VanChartBoxPlot;
import com.fr.plugin.chart.type.ConditionKeyType;
import com.fr.van.chart.box.condition.VanChartBoxBorderConditionPane;
import com.fr.van.chart.box.condition.VanChartBoxNormalMarkerConditionPane;
import com.fr.van.chart.box.condition.VanChartBoxOutlierMarkerConditionPane;
import com.fr.van.chart.designer.other.condition.item.VanChartSeriesColorConditionPane;

import java.awt.Dimension;

public class VanChartBoxConditionPane extends DataSeriesConditionPane {

    public VanChartBoxConditionPane(Plot plot) {
        super(plot);
    }

    protected void initComponents() {
        super.initComponents();

        liteConditionPane.setPreferredSize(new Dimension(300, 400));
    }

    protected void addBasicAction() {
        classPaneMap.put(AttrBackground.class, new VanChartSeriesColorConditionPane(this));
        classPaneMap.put(AttrAlpha.class, new LabelAlphaPane(this));
        classPaneMap.put(AttrBorderWithWidth.class, new VanChartBoxBorderConditionPane(this));

        if (((VanChartBoxPlot) plot).isDetailed()) {
            classPaneMap.put(VanChartAttrNormalMarker.class, new VanChartBoxNormalMarkerConditionPane(this));
            classPaneMap.put(VanChartAttrOutlierMarker.class, new VanChartBoxOutlierMarkerConditionPane(this));
        }
    }

    protected void addStyleAction() {
    }

    protected ChartConditionPane createListConditionPane() {

        return new ChartConditionPane() {

            @Override
            protected ConditionKeyType[] conditionKeyTypes() {
                return ConditionKeyType.BOX_CONDITION_KEY_TYPES;
            }
        };
    }


    public Class<? extends Plot> class4Correspond() {
        return VanChartBoxPlot.class;
    }
}