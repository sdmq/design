package com.fr.van.chart.box;

import com.fr.design.ui.ModernUIPane;
import com.fr.van.chart.designer.component.richText.VanChartFieldAttrPane;
import com.fr.van.chart.designer.component.richText.VanChartFieldButton;
import com.fr.van.chart.designer.component.richText.VanChartRichEditorModel;

import javax.swing.JPanel;
import java.util.ArrayList;
import java.util.List;

public class VanChartBoxRichTextResultFieldListPane extends VanChartBoxRichTextDetailedFieldListPane {

    public VanChartBoxRichTextResultFieldListPane(VanChartFieldAttrPane fieldAttrPane, ModernUIPane<VanChartRichEditorModel> richEditorPane) {
        super(fieldAttrPane, richEditorPane);
    }

    protected void addDefaultFieldButton(JPanel fieldPane) {
        fieldPane.add(getCategoryNameButton());
        fieldPane.add(getSeriesNameButton());
        fieldPane.add(getRichTextMax());
        fieldPane.add(getRichTextQ3());
        fieldPane.add(getRichTextMedian());
        fieldPane.add(getRichTextQ1());
        fieldPane.add(getRichTextMin());
    }

    protected List<VanChartFieldButton> getDefaultFieldButtonList() {
        List<VanChartFieldButton> fieldButtonList = new ArrayList<>();

        fieldButtonList.add(getCategoryNameButton());
        fieldButtonList.add(getSeriesNameButton());
        fieldButtonList.add(getRichTextMax());
        fieldButtonList.add(getRichTextQ3());
        fieldButtonList.add(getRichTextMedian());
        fieldButtonList.add(getRichTextQ1());
        fieldButtonList.add(getRichTextMin());

        return fieldButtonList;
    }
}
