package com.fr.van.chart.box;

import com.fr.chart.chartattr.Plot;
import com.fr.design.i18n.Toolkit;
import com.fr.design.layout.TableLayout;
import com.fr.design.layout.TableLayoutHelper;
import com.fr.design.mainframe.chart.gui.ChartStylePane;
import com.fr.plugin.chart.box.VanChartBoxPlot;
import com.fr.van.chart.designer.TableLayout4VanChartHelper;
import com.fr.van.chart.designer.component.VanChartBeautyPane;
import com.fr.van.chart.designer.component.VanChartMarkerPane;
import com.fr.van.chart.designer.style.series.VanChartAbstractPlotSeriesPane;

import javax.swing.JPanel;
import java.awt.BorderLayout;
import java.awt.Component;

public class VanChartBoxSeriesPane extends VanChartAbstractPlotSeriesPane {

    private VanChartBoxBorderPane boxBorderPane;

    private JPanel normalMarker;
    private JPanel outlierMarker;

    private VanChartMarkerPane normalValuePane;
    private VanChartMarkerPane outlierValuePane;

    public VanChartBoxSeriesPane(ChartStylePane parent, Plot plot) {
        super(parent, plot);
    }

    protected JPanel getContentInPlotType() {
        double p = TableLayout.PREFERRED;
        double f = TableLayout.FILL;

        double[] columnSize = {f};
        double[] rowSize = {p, p, p};

        Component[][] components = new Component[][]{
                new Component[]{createBoxBorderPane()},
                new Component[]{createNormalValuePane()},
                new Component[]{createOutlierValuePane()}
        };

        contentPane = TableLayoutHelper.createTableLayoutPane(components, rowSize, columnSize);

        return contentPane;
    }

    private JPanel createBoxBorderPane() {
        boxBorderPane = new VanChartBoxBorderPane();
        return TableLayout4VanChartHelper.createExpandablePaneWithTitle(Toolkit.i18nText("Fine-Design_Chart_Border"), boxBorderPane);
    }

    private JPanel createNormalValuePane() {
        normalValuePane = new VanChartMarkerPane();
        normalMarker = TableLayout4VanChartHelper.createExpandablePaneWithTitle(Toolkit.i18nText("Fine-Design_Chart_Normal_Value"), normalValuePane);

        return normalMarker;
    }

    private JPanel createOutlierValuePane() {
        outlierValuePane = new VanChartMarkerPane();
        outlierMarker = TableLayout4VanChartHelper.createExpandablePaneWithTitle(Toolkit.i18nText("Fine-Design_Chart_Outlier_Value"), outlierValuePane);

        return outlierMarker;
    }

    private void checkMarkerPane(boolean isDetailed) {
        normalMarker.setVisible(isDetailed);
        outlierMarker.setVisible(isDetailed);
    }

    protected void setColorPaneContent(JPanel panel) {
        panel.add(createAlphaPane(), BorderLayout.SOUTH);
    }

    protected VanChartBeautyPane createStylePane() {
        return null;
    }

    public void populateBean(Plot plot) {
        if (plot == null) {
            return;
        }

        super.populateBean(plot);

        if (plot instanceof VanChartBoxPlot) {
            VanChartBoxPlot boxPlot = (VanChartBoxPlot) plot;

            boxBorderPane.populateBean(boxPlot.getBorder());
            normalValuePane.populate(boxPlot.getNormalValue());
            outlierValuePane.populate(boxPlot.getOutlierValue());

            checkMarkerPane(boxPlot.isDetailed());
        }
    }

    public void updateBean(Plot plot) {
        if (plot == null) {
            return;
        }

        if (plot instanceof VanChartBoxPlot) {
            VanChartBoxPlot boxPlot = (VanChartBoxPlot) plot;

            boxPlot.setBorder(boxBorderPane.updateBean());
            boxPlot.setNormalValue(normalValuePane.update());
            boxPlot.setOutlierValue(outlierValuePane.update());
        }

        super.updateBean(plot);
    }
}
