package com.fr.van.chart.box.data.report;

import com.fr.chart.chartattr.ChartCollection;
import com.fr.design.mainframe.chart.gui.ChartDataPane;
import com.fr.design.mainframe.chart.gui.data.report.CategoryPlotReportDataContentPane;
import com.fr.plugin.chart.box.data.VanBoxReportDefinition;
import com.fr.plugin.chart.box.data.VanBoxReportDetailedDefinition;

public class BoxPlotReportDetailedDataSeriesPane extends CategoryPlotReportDataContentPane {

    public BoxPlotReportDetailedDataSeriesPane(ChartDataPane parent) {
        super(parent);
    }

    public void populateBean(ChartCollection ob) {
        VanBoxReportDetailedDefinition definition = BoxReportDefinitionHelper.getBoxReportDetailedDefinition(ob);

        if (definition == null) {
            return;
        }

        this.populateDefinition(definition);
    }

    public void updateBean(ChartCollection ob) {
        VanBoxReportDefinition report = BoxReportDefinitionHelper.getBoxReportDefinition(ob);

        if (report == null) {
            return;
        }

        VanBoxReportDetailedDefinition detailedDefinition = new VanBoxReportDetailedDefinition();

        this.updateDefinition(detailedDefinition);

        report.setDetailedDefinition(detailedDefinition);
    }
}
