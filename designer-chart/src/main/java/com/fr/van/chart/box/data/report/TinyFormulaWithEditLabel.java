package com.fr.van.chart.box.data.report;

import com.fr.design.event.UIObserver;
import com.fr.design.event.UIObserverListener;
import com.fr.design.formula.TinyFormulaPane;
import com.fr.design.mainframe.chart.gui.UIEditLabel;

import javax.swing.JPanel;
import javax.swing.SwingConstants;
import java.awt.BorderLayout;
import java.awt.Dimension;

public abstract class TinyFormulaWithEditLabel extends JPanel implements UIObserver {

    private UIEditLabel editLabel;
    private TinyFormulaPane tinyFormulaPane;

    protected UIObserverListener listener;

    public TinyFormulaWithEditLabel(String text) {
        editLabel = new UIEditLabel(text, SwingConstants.LEFT) {
            protected void doAfterMousePress() {
                clearAllBackground();
            }

            protected boolean appendOriginalLabel() {
                return false;
            }
        };

        editLabel.setPreferredSize(new Dimension(75, 20));
        tinyFormulaPane = new TinyFormulaPane();

        this.setLayout(new BorderLayout(4, 0));
        this.add(editLabel, BorderLayout.WEST);
        this.add(tinyFormulaPane, BorderLayout.CENTER);
    }

    protected abstract void clearAllBackground();

    public void clearBackGround() {
        editLabel.resetNomalrBackground();
    }

    public boolean shouldResponseChangeListener() {
        return true;
    }

    public void registerChangeListener(UIObserverListener listener) {
        this.listener = listener;
        editLabel.registerChangeListener(listener);
    }

    public void populateFormula(Object ob) {
        if (ob != null) {
            tinyFormulaPane.populateBean(ob.toString());
        }
    }

    public Object updateFormula() {
        return tinyFormulaPane.getUITextField().getText();
    }

    public String getHeaderName() {
        return editLabel.getText();
    }

    public void setHeaderName(String text) {
        editLabel.setText(text);
    }
}
