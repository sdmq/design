package com.fr.van.chart.box.data.table;

import com.fr.chart.base.ChartConstants;
import com.fr.chart.chartattr.ChartCollection;
import com.fr.design.gui.icombobox.UIComboBox;
import com.fr.design.gui.ilable.UILabel;
import com.fr.design.i18n.Toolkit;
import com.fr.design.layout.TableLayout;
import com.fr.design.layout.TableLayoutHelper;
import com.fr.design.mainframe.chart.gui.data.table.AbstractTableDataContentPane;
import com.fr.plugin.chart.box.data.VanBoxTableDefinition;
import com.fr.plugin.chart.box.data.VanBoxTableResultDefinition;
import com.fr.stable.ArrayUtils;
import com.fr.stable.StringUtils;

import javax.swing.BorderFactory;
import javax.swing.JPanel;
import java.util.List;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;

public class BoxPlotTableResultDataSeriesPane extends AbstractTableDataContentPane {

    private UIComboBox category;
    private UIComboBox seriesName;

    private JPanel categoryPane;
    private JPanel seriesNamePane;

    private UIComboBoxWithEditLabel max;
    private UIComboBoxWithEditLabel q3;
    private UIComboBoxWithEditLabel median;
    private UIComboBoxWithEditLabel q1;
    private UIComboBoxWithEditLabel min;

    public BoxPlotTableResultDataSeriesPane() {

        initComboxSize();

        this.setLayout(new BorderLayout());
        this.add(createDataSeriesPane(), BorderLayout.CENTER);

        addItemListener();
    }

    private void initComboxSize() {
        category = new UIComboBox();
        seriesName = new UIComboBox();

        categoryPane = createUIComboBoxPane(category, Toolkit.i18nText("Fine-Design_Chart_Category"));
        seriesNamePane = createUIComboBoxPane(seriesName, Toolkit.i18nText("Fine-Design_Chart_Series_Name"));

        max = createUIComboBoxWithEditLabel(Toolkit.i18nText("Fine-Design_Chart_Data_Max"));
        q3 = createUIComboBoxWithEditLabel(Toolkit.i18nText("Fine-Design_Chart_Data_Q3"));
        median = createUIComboBoxWithEditLabel(Toolkit.i18nText("Fine-Design_Chart_Data_Median"));
        q1 = createUIComboBoxWithEditLabel(Toolkit.i18nText("Fine-Design_Chart_Data_Q1"));
        min = createUIComboBoxWithEditLabel(Toolkit.i18nText("Fine-Design_Chart_Data_Min"));

        addNoneItem();

        category.setSelectedItem(null);
        seriesName.setSelectedItem(null);
        median.populateComboBox(null);
    }

    private JPanel createUIComboBoxPane(UIComboBox comboBox, String title) {
        UILabel label = new UILabel(title);
        label.setPreferredSize(new Dimension(80, 20));
        comboBox.setPreferredSize(new Dimension(100, 20));

        JPanel panel = new JPanel();

        panel.setLayout(new BorderLayout(4, 0));
        panel.add(label, BorderLayout.WEST);
        panel.add(comboBox, BorderLayout.CENTER);

        return panel;
    }

    private UIComboBoxWithEditLabel createUIComboBoxWithEditLabel(String title) {

        return new UIComboBoxWithEditLabel(title) {
            protected void clearAllBackground() {
                clearAllLabelBackground();
            }
        };
    }

    private void clearAllLabelBackground() {
        UIComboBoxWithEditLabel[] editLabels = new UIComboBoxWithEditLabel[]{max, q3, median, q1, min};

        for (UIComboBoxWithEditLabel label : editLabels) {
            if (label != null) {
                label.clearBackGround();
            }
        }
    }

    private void addItemListener() {
        category.addItemListener(tooltipListener);
        seriesName.addItemListener(tooltipListener);
        max.addItemListener(tooltipListener);
        q3.addItemListener(tooltipListener);
        median.addItemListener(tooltipListener);
        q1.addItemListener(tooltipListener);
        min.addItemListener(tooltipListener);
    }

    private JPanel createDataSeriesPane() {
        double p = TableLayout.PREFERRED;
        double f = TableLayout.FILL;
        double[] row = {p, p, p, p, p, p, p};
        double[] col = {f};

        Component[][] components = new Component[][]{
                new Component[]{categoryPane},
                new Component[]{seriesNamePane},
                new Component[]{max},
                new Component[]{q3},
                new Component[]{median},
                new Component[]{q1},
                new Component[]{min}
        };

        JPanel panel = TableLayoutHelper.createTableLayoutPane(components, row, col);
        panel.setBorder(BorderFactory.createEmptyBorder(0, 24, 0, 15));

        return panel;
    }

    public void checkBoxUse(boolean hasUse) {
    }

    protected void refreshBoxListWithSelectTableData(List list) {
        refreshBoxItems(category, list);
        refreshBoxItems(seriesName, list);

        refreshBoxItems(max.getComboBox(), list);
        refreshBoxItems(q3.getComboBox(), list);
        refreshBoxItems(median.getComboBox(), list);
        refreshBoxItems(q1.getComboBox(), list);
        refreshBoxItems(min.getComboBox(), list);

        addNoneItem();
    }

    public void clearAllBoxList() {
        clearBoxItems(category);
        clearBoxItems(seriesName);

        clearBoxItems(max.getComboBox());
        clearBoxItems(q3.getComboBox());
        clearBoxItems(median.getComboBox());
        clearBoxItems(q1.getComboBox());
        clearBoxItems(min.getComboBox());

        addNoneItem();
    }

    private void addNoneItem() {
        category.addItem(Toolkit.i18nText("Fine-Design_Chart_Use_None"));
        seriesName.addItem(Toolkit.i18nText("Fine-Design_Chart_Use_None"));
        median.addItem(Toolkit.i18nText("Fine-Design_Chart_Use_None"));
    }

    public void populateBean(ChartCollection collection) {
        super.populateBean(collection);

        VanBoxTableResultDefinition definition = BoxTableDefinitionHelper.getBoxTableResultDefinition(collection);

        if (definition == null) {
            return;
        }

        combineCustomEditValue(category, definition.getCategoryName());
        combineCustomEditValue(seriesName, definition.getSeriesName());

        max.populateComboBox(definition.getMax());
        q3.populateComboBox(definition.getQ3());
        median.populateComboBox(definition.getMedian());
        q1.populateComboBox(definition.getQ1());
        min.populateComboBox(definition.getMin());

        max.setHeaderName(definition.getMaxLabel());
        q3.setHeaderName(definition.getQ3Label());
        median.setHeaderName(definition.getMedianLabel());
        q1.setHeaderName(definition.getQ1Label());
        min.setHeaderName(definition.getMinLabel());
    }

    public void updateBean(ChartCollection collection) {
        VanBoxTableDefinition table = BoxTableDefinitionHelper.getBoxTableDefinition(collection);

        VanBoxTableResultDefinition definition = new VanBoxTableResultDefinition();

        Object resultCategory = category.getSelectedItem();
        Object resultSeries = seriesName.getSelectedItem();

        Object resultMax = max.updateComboBox();
        Object resultQ3 = q3.updateComboBox();
        Object resultMedian = median.updateComboBox();
        Object resultQ1 = q1.updateComboBox();
        Object resultMin = min.updateComboBox();

        if (resultCategory == null || ArrayUtils.contains(ChartConstants.getNoneKeys(), resultCategory)) {
            definition.setCategoryName(StringUtils.EMPTY);
        } else {
            definition.setCategoryName(resultCategory.toString());
        }
        if (resultSeries == null || ArrayUtils.contains(ChartConstants.getNoneKeys(), resultSeries)) {
            definition.setSeriesName(StringUtils.EMPTY);
        } else {
            definition.setSeriesName(resultSeries.toString());
        }
        if (resultMax != null) {
            definition.setMax(resultMax.toString());
        }
        if (resultQ3 != null) {
            definition.setQ3(resultQ3.toString());
        }
        if (resultMedian != null) {
            definition.setMedian(resultMedian.toString());
        }
        if (resultQ1 != null) {
            definition.setQ1(resultQ1.toString());
        }
        if (resultMin != null) {
            definition.setMin(resultMin.toString());
        }

        definition.setMaxLabel(max.getHeaderName());
        definition.setQ3Label(q3.getHeaderName());
        definition.setMedianLabel(median.getHeaderName());
        definition.setQ1Label(q1.getHeaderName());
        definition.setMinLabel(min.getHeaderName());

        table.setResultDefinition(definition);
    }
}