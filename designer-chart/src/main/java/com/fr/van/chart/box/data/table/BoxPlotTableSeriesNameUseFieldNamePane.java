package com.fr.van.chart.box.data.table;

import com.fr.chart.chartdata.MoreNameCDDefinition;
import com.fr.design.mainframe.chart.gui.data.table.SeriesNameUseFieldNamePane;
import com.fr.plugin.chart.box.data.VanBoxMoreNameCDDefinition;

public class BoxPlotTableSeriesNameUseFieldNamePane extends SeriesNameUseFieldNamePane {

    protected MoreNameCDDefinition createMoreNameCDDefinition() {
        return new VanBoxMoreNameCDDefinition();
    }
}
