package com.fr.van.chart.box.data.table;

import com.fr.chart.chartdata.OneValueCDDefinition;
import com.fr.design.mainframe.chart.gui.ChartDataPane;
import com.fr.design.mainframe.chart.gui.data.table.SeriesNameUseFieldValuePane;
import com.fr.plugin.chart.box.data.VanBoxOneValueCDDefinition;

import java.awt.Dimension;

public class BoxPlotTableSeriesNameUseFieldValuePane extends SeriesNameUseFieldValuePane {

    protected OneValueCDDefinition createOneValueCDDefinition() {
        return new VanBoxOneValueCDDefinition();
    }

    protected Dimension getLabelDimension() {
        return new Dimension(ChartDataPane.LABEL_WIDTH, ChartDataPane.LABEL_HEIGHT);
    }
}
