package com.fr.van.chart.box.data.table;

import com.fr.base.chart.chartdata.TopDefinitionProvider;
import com.fr.chart.chartattr.Chart;
import com.fr.chart.chartattr.ChartCollection;
import com.fr.chart.chartdata.NormalTableDataDefinition;
import com.fr.plugin.chart.box.data.VanBoxTableDefinition;
import com.fr.plugin.chart.box.data.VanBoxTableResultDefinition;

public class BoxTableDefinitionHelper {

    public static VanBoxTableDefinition getBoxTableDefinition(ChartCollection collection) {
        if (collection != null) {

            Chart chart = collection.getSelectedChart();

            if (chart != null) {
                TopDefinitionProvider definitionProvider = chart.getFilterDefinition();

                if (definitionProvider instanceof VanBoxTableDefinition) {
                    return (VanBoxTableDefinition) definitionProvider;
                }
            }
        }

        return null;
    }

    public static VanBoxTableResultDefinition getBoxTableResultDefinition(ChartCollection collection) {
        VanBoxTableDefinition table = getBoxTableDefinition(collection);

        if (table != null) {
            return table.getResultDefinition();
        }

        return null;
    }

    public static NormalTableDataDefinition getBoxTableDetailedDefinition(ChartCollection collection) {
        VanBoxTableDefinition table = getBoxTableDefinition(collection);

        if (table != null) {
            return table.getDetailedDefinition();
        }

        return null;
    }

    public static boolean isDetailedTableDataType(ChartCollection collection) {
        VanBoxTableDefinition table = getBoxTableDefinition(collection);

        if (table != null) {
            return table.isDetailed();
        }

        return true;
    }
}

