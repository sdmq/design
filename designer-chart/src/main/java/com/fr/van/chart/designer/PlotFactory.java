package com.fr.van.chart.designer;

import com.fr.base.chart.chartdata.model.LargeDataModel;
import com.fr.chart.chartattr.Plot;
import com.fr.data.core.FormatField;
import com.fr.design.gui.icombobox.UIComboBoxRenderer;
import com.fr.design.gui.style.FormatPane;
import com.fr.general.ComparatorUtils;
import com.fr.log.FineLoggerFactory;
import com.fr.plugin.chart.PiePlot4VanChart;
import com.fr.plugin.chart.area.VanChartAreaPlot;
import com.fr.plugin.chart.bubble.VanChartBubblePlot;
import com.fr.plugin.chart.column.VanChartColumnPlot;
import com.fr.plugin.chart.drillmap.VanChartDrillMapPlot;
import com.fr.plugin.chart.funnel.VanChartFunnelPlot;
import com.fr.plugin.chart.gantt.VanChartGanttPlot;
import com.fr.plugin.chart.gauge.VanChartGaugePlot;
import com.fr.plugin.chart.heatmap.VanChartHeatMapPlot;
import com.fr.plugin.chart.line.VanChartLinePlot;
import com.fr.plugin.chart.map.VanChartMapPlot;
import com.fr.plugin.chart.multilayer.VanChartMultiPiePlot;
import com.fr.plugin.chart.radar.VanChartRadarPlot;
import com.fr.plugin.chart.scatter.VanChartScatterPlot;
import com.fr.plugin.chart.structure.VanChartStructurePlot;
import com.fr.plugin.chart.treemap.VanChartTreeMapPlot;
import com.fr.plugin.chart.wordcloud.VanChartWordCloudPlot;
import com.fr.van.chart.bubble.force.VanChartBubbleRefreshTooltipPane;
import com.fr.van.chart.column.VanChartColumnPlotLabelDetailPane;
import com.fr.van.chart.designer.component.VanChartLabelContentPane;
import com.fr.van.chart.designer.component.VanChartRefreshTooltipContentPane;
import com.fr.van.chart.designer.component.VanChartTooltipContentPane;
import com.fr.van.chart.designer.style.HeatMapRangeLegendPane;
import com.fr.van.chart.designer.style.MapRangeLegendPane;
import com.fr.van.chart.designer.style.VanChartPlotLegendPane;
import com.fr.van.chart.designer.style.VanChartRangeLegendPane;
import com.fr.van.chart.designer.style.VanChartStylePane;
import com.fr.van.chart.designer.style.VanLegendPaneWidthOutFixedCheck;
import com.fr.van.chart.designer.style.label.VanChartGaugePlotLabelPane;
import com.fr.van.chart.designer.style.label.VanChartPiePlotLabelPane;
import com.fr.van.chart.designer.style.label.VanChartPlotLabelDetailPane;
import com.fr.van.chart.designer.style.label.VanChartPlotLabelPane;
import com.fr.van.chart.designer.style.tooltip.VanChartPlotRefreshTooltipPane;
import com.fr.van.chart.designer.style.tooltip.VanChartPlotTooltipPane;
import com.fr.van.chart.funnel.designer.style.VanChartFunnelLabelContentPane;
import com.fr.van.chart.funnel.designer.style.VanChartFunnelRefreshTooltipContentPane;
import com.fr.van.chart.funnel.designer.style.VanChartFunnelTooltipContentPane;
import com.fr.van.chart.gantt.designer.style.label.VanChartGanttLabelContentPane;
import com.fr.van.chart.gantt.designer.style.tooltip.VanChartGanttPlotTooltipPane;
import com.fr.van.chart.gantt.designer.style.tooltip.VanChartGanttTooltipContentPane;
import com.fr.van.chart.gauge.VanChartGaugePlotRefreshTooltipPane;
import com.fr.van.chart.gauge.VanChartGaugePlotTooltipPane;
import com.fr.van.chart.map.designer.style.label.VanChartMapLabelContentPane;
import com.fr.van.chart.map.designer.style.tooltip.VanChartMapRefreshTooltipContentPane;
import com.fr.van.chart.map.designer.style.tooltip.VanChartMapTooltipContentPane;
import com.fr.van.chart.multilayer.style.VanChartMultiPieLabelContentPane;
import com.fr.van.chart.multilayer.style.VanChartMultiPiePlotTooltipPane;
import com.fr.van.chart.multilayer.style.VanChartMultiPieTooltipContentPane;
import com.fr.van.chart.multilayer.style.VanChartMutiPieRefreshTooltipContentPane;
import com.fr.van.chart.scatter.VanChartScatterLabelContentPane;
import com.fr.van.chart.scatter.VanChartScatterPlotTooltipPane;
import com.fr.van.chart.scatter.VanChartScatterRefreshTooltipContentPane;
import com.fr.van.chart.structure.desinger.style.VanChartStructureLabelContentPane;
import com.fr.van.chart.structure.desinger.style.VanChartStructureRefreshTooltipContentPane;
import com.fr.van.chart.structure.desinger.style.VanChartStructureTooltipContentPane;
import com.fr.van.chart.wordcloud.designer.style.VanChartWordCloudRefreshTooltipContentPane;
import com.fr.van.chart.wordcloud.designer.style.VanChartWordCloudTooltipContentPane;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import java.lang.reflect.Constructor;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.awt.Component;

/**
 * Created by Mitisky on 16/3/1.
 * 图表配置界面的工厂
 */
public class PlotFactory {

    private static Set<Class<? extends Plot>> autoAdjustLabelPlots = new HashSet<>();

    static {
        autoAdjustLabelPlots.add(VanChartColumnPlot.class);
        autoAdjustLabelPlots.add(VanChartLinePlot.class);
        autoAdjustLabelPlots.add(VanChartAreaPlot.class);
        autoAdjustLabelPlots.add(VanChartStructurePlot.class);
    }

    public static boolean plotAutoAdjustLabelPosition(Plot plot) {
        return autoAdjustLabelPlots.contains(plot.getClass());
    }

    private static Set<Class<? extends Plot>> supportAddTableFieldPlots = new HashSet<>();

    static {
        supportAddTableFieldPlots.add(PiePlot4VanChart.class);
        supportAddTableFieldPlots.add(VanChartColumnPlot.class);
        supportAddTableFieldPlots.add(VanChartLinePlot.class);
        supportAddTableFieldPlots.add(VanChartAreaPlot.class);
        supportAddTableFieldPlots.add(VanChartRadarPlot.class);
    }

    public static boolean plotSupportAddTableField(Plot plot) {
        return supportAddTableFieldPlots.contains(plot.getClass());
    }

    private static Set<Class<? extends Plot>> borderAndBackgroundLabelPlots = new HashSet<>();

    static {
        borderAndBackgroundLabelPlots.add(PiePlot4VanChart.class);
        borderAndBackgroundLabelPlots.add(VanChartColumnPlot.class);
        borderAndBackgroundLabelPlots.add(VanChartLinePlot.class);
        borderAndBackgroundLabelPlots.add(VanChartAreaPlot.class);
    }

    public static boolean hasBorderAndBackgroundPlotLabel(Plot plot) {
        return borderAndBackgroundLabelPlots.contains(plot.getClass());
    }

    /**
     * 图例Map
     */
    private static Map<Class<? extends Plot>, Class<? extends VanChartPlotLegendPane>> legendMap = new HashMap<Class<? extends Plot>, Class<? extends VanChartPlotLegendPane>>();

    static {
        legendMap.put(VanChartGaugePlot.class, VanLegendPaneWidthOutFixedCheck.class);
        legendMap.put(VanChartMultiPiePlot.class, VanLegendPaneWidthOutFixedCheck.class);
        legendMap.put(VanChartScatterPlot.class, VanChartRangeLegendPane.class);
        legendMap.put(VanChartBubblePlot.class, VanChartRangeLegendPane.class);
        legendMap.put(VanChartMapPlot.class, MapRangeLegendPane.class);
        legendMap.put(VanChartDrillMapPlot.class, MapRangeLegendPane.class);
        legendMap.put(VanChartHeatMapPlot.class, HeatMapRangeLegendPane.class);
        legendMap.put(VanChartWordCloudPlot.class, VanChartRangeLegendPane.class);
    }

    /**
     * 数据点提示Map
     */
    private static Map<Class<? extends Plot>, Class<? extends VanChartPlotTooltipPane>> toolTipMap = new HashMap<Class<? extends Plot>, Class<? extends VanChartPlotTooltipPane>>();

    static {
        toolTipMap.put(VanChartGaugePlot.class, VanChartGaugePlotTooltipPane.class);
        toolTipMap.put(VanChartScatterPlot.class, VanChartScatterPlotTooltipPane.class);
        toolTipMap.put(VanChartBubblePlot.class, VanChartScatterPlotTooltipPane.class);
        toolTipMap.put(VanChartMultiPiePlot.class, VanChartMultiPiePlotTooltipPane.class);
        toolTipMap.put(VanChartTreeMapPlot.class, VanChartMultiPiePlotTooltipPane.class);
        toolTipMap.put(VanChartGanttPlot.class, VanChartGanttPlotTooltipPane.class);
    }

    private static Map<Class<? extends Plot>, Class<? extends VanChartTooltipContentPane>> labelContentMap = new HashMap<Class<? extends Plot>, Class<? extends VanChartTooltipContentPane>>();

    static {
        labelContentMap.put(VanChartMapPlot.class, VanChartMapLabelContentPane.class);
        labelContentMap.put(VanChartDrillMapPlot.class, VanChartMapLabelContentPane.class);
        labelContentMap.put(VanChartMultiPiePlot.class, VanChartMultiPieLabelContentPane.class);
        labelContentMap.put(VanChartTreeMapPlot.class, VanChartMultiPieLabelContentPane.class);
        labelContentMap.put(VanChartFunnelPlot.class, VanChartFunnelLabelContentPane.class);
        labelContentMap.put(VanChartHeatMapPlot.class, VanChartMapLabelContentPane.class);
        labelContentMap.put(VanChartBubblePlot.class, VanChartScatterLabelContentPane.class);
        labelContentMap.put(VanChartScatterPlot.class, VanChartScatterLabelContentPane.class);
        labelContentMap.put(VanChartStructurePlot.class, VanChartStructureLabelContentPane.class);
        labelContentMap.put(VanChartGanttPlot.class, VanChartGanttLabelContentPane.class);
    }

    private static Map<Class<? extends Plot>, Class<? extends VanChartTooltipContentPane>> tooltipContentMap = new HashMap<Class<? extends Plot>, Class<? extends VanChartTooltipContentPane>>();

    static {
        tooltipContentMap.put(VanChartMapPlot.class, VanChartMapTooltipContentPane.class);
        tooltipContentMap.put(VanChartDrillMapPlot.class, VanChartMapTooltipContentPane.class);
        tooltipContentMap.put(VanChartMultiPiePlot.class, VanChartMultiPieTooltipContentPane.class);
        tooltipContentMap.put(VanChartTreeMapPlot.class, VanChartMultiPieTooltipContentPane.class);
        tooltipContentMap.put(VanChartFunnelPlot.class, VanChartFunnelTooltipContentPane.class);
        tooltipContentMap.put(VanChartHeatMapPlot.class, VanChartMapTooltipContentPane.class);
        tooltipContentMap.put(VanChartWordCloudPlot.class, VanChartWordCloudTooltipContentPane.class);
        tooltipContentMap.put(VanChartGanttPlot.class, VanChartGanttTooltipContentPane.class);
        tooltipContentMap.put(VanChartStructurePlot.class, VanChartStructureTooltipContentPane.class);
    }

    private static Map<Class<? extends Plot>, Class<? extends VanChartPlotLabelPane>> labelMap = new HashMap<>();

    static {
        labelMap.put(PiePlot4VanChart.class, VanChartPiePlotLabelPane.class);
        labelMap.put(VanChartGaugePlot.class, VanChartGaugePlotLabelPane.class);
    }

    /**
     * 监控刷新 自动数据点提示Map
     */


    private static Map<Class<? extends Plot>, Class<? extends VanChartPlotTooltipPane>> refreshToolTipMap = new HashMap<Class<? extends Plot>, Class<? extends VanChartPlotTooltipPane>>();

    static {
        refreshToolTipMap.put(VanChartGaugePlot.class, VanChartGaugePlotRefreshTooltipPane.class);
        refreshToolTipMap.put(VanChartBubblePlot.class, VanChartBubbleRefreshTooltipPane.class);

    }

    private static Map<Class<? extends Plot>, Class<? extends VanChartTooltipContentPane>> refreshTooltipContentMap = new HashMap<Class<? extends Plot>, Class<? extends VanChartTooltipContentPane>>();

    static {
        refreshTooltipContentMap.put(VanChartMapPlot.class, VanChartMapRefreshTooltipContentPane.class);
        refreshTooltipContentMap.put(VanChartHeatMapPlot.class, VanChartMapRefreshTooltipContentPane.class);
        refreshTooltipContentMap.put(VanChartDrillMapPlot.class, VanChartMapRefreshTooltipContentPane.class);
        refreshTooltipContentMap.put(VanChartScatterPlot.class, VanChartScatterRefreshTooltipContentPane.class);
        refreshTooltipContentMap.put(VanChartBubblePlot.class, VanChartScatterRefreshTooltipContentPane.class);
        refreshTooltipContentMap.put(VanChartMultiPiePlot.class, VanChartMutiPieRefreshTooltipContentPane.class);
        refreshTooltipContentMap.put(VanChartTreeMapPlot.class, VanChartMutiPieRefreshTooltipContentPane.class);
        refreshTooltipContentMap.put(VanChartFunnelPlot.class, VanChartFunnelRefreshTooltipContentPane.class);
        refreshTooltipContentMap.put(VanChartWordCloudPlot.class, VanChartWordCloudRefreshTooltipContentPane.class);
        refreshTooltipContentMap.put(VanChartStructurePlot.class, VanChartStructureRefreshTooltipContentPane.class);

    }

    /**
     * 根据图表类型创建标签界面
     *
     * @param plot      图表
     * @param stylePane 样式界面
     * @return 标签界面
     */
    public static VanChartPlotLabelPane createPlotLabelPane(Plot plot, VanChartStylePane stylePane) {
        return createPlotLabelPane(plot, stylePane, false);
    }

    public static VanChartPlotLabelPane createPlotLabelPane(Plot plot, VanChartStylePane stylePane, boolean inCondition) {
        Class<? extends Plot> key = plot.getClass();
        if (labelMap.containsKey(key)) {
            try {
                Class<? extends VanChartPlotLabelPane> cl = labelMap.get(key);
                Constructor<? extends VanChartPlotLabelPane> constructor = cl.getConstructor(Plot.class, VanChartStylePane.class, boolean.class);
                return constructor.newInstance(plot, stylePane, inCondition);
            } catch (Exception e) {
                FineLoggerFactory.getLogger().error(e.getMessage(), e);
            }
        }
        return new VanChartPlotLabelPane(plot, stylePane, inCondition);
    }

    public static VanChartPlotLabelDetailPane createPlotLabelDetailPane(Plot plot, VanChartStylePane stylePane, boolean inCondition) {
        if (plot instanceof VanChartColumnPlot) {
            return new VanChartColumnPlotLabelDetailPane(plot, stylePane, inCondition);
        }
        return new VanChartPlotLabelDetailPane(plot, stylePane, inCondition);
    }


    /**
     * 根据图表类型创建图例界面
     *
     * @param plot      图表
     * @param stylePane 样式界面
     * @return 图例界面
     */
    public static VanChartPlotLegendPane createPlotLegendPane(Plot plot, VanChartStylePane stylePane) {
        Class<? extends Plot> key = plot.getClass();
        if (legendMap.containsKey(key)) {
            try {
                Class<? extends VanChartPlotLegendPane> cl = legendMap.get(key);
                Constructor<? extends VanChartPlotLegendPane> constructor = cl.getConstructor(VanChartStylePane.class);
                return constructor.newInstance(stylePane);
            } catch (Exception e) {
                FineLoggerFactory.getLogger().error(e.getMessage(), e);
            }
        }
        return new VanChartPlotLegendPane(stylePane);
    }

    /**
     * 根据图表类型创建数据点提示界面
     *
     * @param plot      图表
     * @param stylePane 样式界面
     * @return 数据点提示界面
     */
    public static VanChartPlotTooltipPane createPlotTooltipPane(Plot plot, VanChartStylePane stylePane) {
        Class<? extends Plot> key = plot.getClass();
        if (toolTipMap.containsKey(key)) {
            try {
                Class<? extends VanChartPlotTooltipPane> cl = toolTipMap.get(key);
                Constructor<? extends VanChartPlotTooltipPane> constructor = cl.getConstructor(Plot.class, VanChartStylePane.class);
                return constructor.newInstance(plot, stylePane);
            } catch (Exception e) {
                FineLoggerFactory.getLogger().error(e.getMessage(), e);
            }
        }
        return new VanChartPlotTooltipPane(plot, stylePane);
    }

    /**
     * 根据图表类型创建标签的具体内容界面.分类名系列名等
     *
     * @param plot       图表
     * @param parent     样式界面
     * @param showOnPane formatpane用到
     * @return 标签的具体内容界面
     */
    public static VanChartTooltipContentPane createPlotLabelContentPane(Plot plot, VanChartStylePane parent, JPanel showOnPane, boolean inCondition) {
        Class<? extends Plot> key = plot.getClass();
        //力学气泡图特殊
        if (plot instanceof VanChartBubblePlot && ((VanChartBubblePlot) plot).isForceBubble()) {
            return new VanChartLabelContentPane(parent, showOnPane, inCondition);
        }
        if (labelContentMap.containsKey(key)) {
            try {
                Class<? extends VanChartTooltipContentPane> cl = labelContentMap.get(key);
                Constructor<? extends VanChartTooltipContentPane> constructor = cl.getConstructor(VanChartStylePane.class, JPanel.class, boolean.class);
                return constructor.newInstance(parent, showOnPane, inCondition);
            } catch (Exception e) {
                FineLoggerFactory.getLogger().error(e.getMessage(), e);
            }
        }
        return new VanChartLabelContentPane(parent, showOnPane, inCondition);
    }

    /**
     * 根据图表类型创建数据点提示的具体内容界面.分类名系列名等
     *
     * @param plot       图表
     * @param parent     样式界面
     * @param showOnPane formatpane用到
     * @return 数据点提示的具体内容界面
     */
    public static VanChartTooltipContentPane createPlotTooltipContentPane(Plot plot, VanChartStylePane parent, JPanel showOnPane) {
        Class<? extends Plot> key = plot.getClass();
        if (tooltipContentMap.containsKey(key)) {
            try {
                Class<? extends VanChartTooltipContentPane> cl = tooltipContentMap.get(key);
                Constructor<? extends VanChartTooltipContentPane> constructor = cl.getConstructor(VanChartStylePane.class, JPanel.class);
                return constructor.newInstance(parent, showOnPane);
            } catch (Exception e) {
                FineLoggerFactory.getLogger().error(e.getMessage(), e);
            }
        }
        return new VanChartTooltipContentPane(parent, showOnPane);
    }


    /**
     * 根据图表类型创建数据点提示界面
     *
     * @param plot 图表
     * @return 数据点提示界面
     */
    public static VanChartPlotTooltipPane createPlotRefreshTooltipPane(Plot plot) {
        Class<? extends Plot> key = plot.getClass();
        if (refreshToolTipMap.containsKey(key)) {
            try {
                Class<? extends VanChartPlotTooltipPane> cl = refreshToolTipMap.get(key);
                Constructor<? extends VanChartPlotTooltipPane> constructor = cl.getConstructor(Plot.class);
                return constructor.newInstance(plot);
            } catch (Exception e) {
                FineLoggerFactory.getLogger().error(e.getMessage(), e);
            }
        }
        return new VanChartPlotRefreshTooltipPane(plot);
    }

    /**
     * 根据图表类型创建监控刷新中数据点提示的具体内容界面.分类名系列名等
     *
     * @param plot       图表
     * @param parent     交互属性界面
     * @param showOnPane formatpane用到
     * @return 数据点提示的具体内容界面
     */
    public static VanChartTooltipContentPane createPlotRefreshTooltipContentPane(Plot plot, VanChartStylePane parent, JPanel showOnPane) {
        Class<? extends Plot> key = plot.getClass();
        if (refreshTooltipContentMap.containsKey(key)) {
            try {
                Class<? extends VanChartTooltipContentPane> cl = refreshTooltipContentMap.get(key);
                Constructor<? extends VanChartTooltipContentPane> constructor = cl.getConstructor(VanChartStylePane.class, JPanel.class);
                return constructor.newInstance(parent, showOnPane);
            } catch (Exception e) {
                FineLoggerFactory.getLogger().error(e.getMessage(), e);
            }
        }
        return new VanChartRefreshTooltipContentPane(parent, showOnPane);
    }

    public static FormatPane createAutoFormatPane() {
        FormatPane formatPane = new FormatPane() {
            protected Component[][] getComponent(JPanel fontPane, JPanel centerPane, JPanel typePane) {
                typePane.setBorder(BorderFactory.createEmptyBorder());
                return new Component[][]{
                        new Component[]{typePane, null},
                        new Component[]{centerPane, null},
                };
            }

            protected UIComboBoxRenderer createComBoxRender() {
                return new UIComboBoxRenderer() {
                    @Override
                    public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected, boolean cellHasFocus) {
                        JLabel label = (JLabel) super.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);
                        if (value instanceof Integer) {
                            String text = ComparatorUtils.equals(value, FormatField.FormatContents.NULL)
                                    ? com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Chart_Auto")
                                    : FormatField.getInstance().getName((Integer) value);
                            label.setText(" " + text);
                        }
                        return label;
                    }
                };
            }
        };

        formatPane.setComboBoxModel(true);

        return formatPane;
    }


    //大数据模式 恢复用注释。取消注释。
//    public static boolean largeDataModel(Plot plot) {
//        return plot != null && plot.convertDataProcessor().getMark() == LargeDataModel.MARK;
//    }

    //大数据模式 恢复用注释。删除下面2个方法 largeDataModel lineMapLargeModel。
    public static boolean largeDataModel(Plot plot) {
        return plot != null && plot.isLargeDataModel();
    }

    public static boolean lineMapLargeModel(Plot plot) {
        return plot instanceof VanChartMapPlot && ((VanChartMapPlot) plot).getLineMapDataProcessor().getMark() == LargeDataModel.MARK;
    }
}

