package com.fr.van.chart.designer.component;

import com.fr.chart.base.GradientStyle;
import com.fr.design.beans.BasicBeanPane;
import com.fr.design.gui.ibutton.UIButtonGroup;
import com.fr.design.gui.ilable.UILabel;
import com.fr.design.i18n.Toolkit;
import com.fr.design.layout.TableLayout;
import com.fr.plugin.chart.type.GradientType;
import com.fr.stable.StringUtils;
import com.fr.van.chart.designer.TableLayout4VanChartHelper;

import javax.swing.JPanel;
import java.awt.BorderLayout;
import java.awt.Component;

/**
 * Created by Mitisky on 15/9/8.
 */
//系列-风格
public class VanChartBeautyPane extends BasicBeanPane<GradientStyle> {

    private UIButtonGroup gradientTypeBox;

    public UIButtonGroup getGradientTypeBox() {
        return gradientTypeBox;
    }

    public VanChartBeautyPane() {
        this.setLayout(new BorderLayout());

        this.add(initGradientTypePane(), BorderLayout.CENTER);
    }

    private JPanel initGradientTypePane() {
        String[] names = getNameArray();

        gradientTypeBox = new UIButtonGroup(names);

        double p = TableLayout.PREFERRED;
        double f = TableLayout.FILL;
        double e = TableLayout4VanChartHelper.EDIT_AREA_WIDTH;
        double[] columnSize = {f, e};
        double[] rowSize = {p};

        Component[][] components = new Component[][]{
                new Component[]{new UILabel(labelName()), gradientTypeBox},
        };

        return TableLayout4VanChartHelper.createGapTableLayoutPane(components, rowSize, columnSize);
    }

    protected String[] getNameArray() {
        return new String[]{
                Toolkit.i18nText("Fine-Design_Chart_On"),
                Toolkit.i18nText("Fine-Design_Chart_Off")
        };
    }

    protected String labelName() {
        return Toolkit.i18nText("Fine-Design_Chart_Gradient_Style");
    }

    public void populateBean(GradientStyle gradientStyle) {
        gradientTypeBox.setSelectedIndex(this.convertGradientTypeToIndex(gradientStyle.getGradientType()));
    }

    @Override
    public GradientStyle updateBean() {
        GradientStyle gradientStyle = new GradientStyle();

        gradientStyle.setGradientType(this.convertIndexToGradientType(this.gradientTypeBox.getSelectedIndex()));

        return gradientStyle;
    }

    protected int convertGradientTypeToIndex(GradientType gradientType) {
        return gradientType == GradientType.NONE ? 1 : 0;
    }

    protected GradientType convertIndexToGradientType(int index) {
        return index == 1 ? GradientType.NONE : GradientType.AUTO;
    }

    protected String title4PopupWindow() {
        return StringUtils.EMPTY;
    }

}