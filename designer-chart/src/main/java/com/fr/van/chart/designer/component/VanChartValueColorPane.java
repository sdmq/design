package com.fr.van.chart.designer.component;

import com.fr.design.dialog.BasicPane;
import com.fr.design.gui.ibutton.UIButtonGroup;
import com.fr.design.gui.ilable.UILabel;
import com.fr.design.i18n.Toolkit;
import com.fr.design.layout.TableLayout;
import com.fr.design.layout.TableLayoutHelper;
import com.fr.design.mainframe.chart.PaneTitleConstants;
import com.fr.design.widget.FRWidgetFactory;
import com.fr.plugin.chart.range.VanChartRangeLegend;
import com.fr.plugin.chart.type.LegendType;
import com.fr.van.chart.designer.TableLayout4VanChartHelper;
import com.fr.van.chart.designer.style.VanChartStylePane;
import com.fr.van.chart.range.component.GradualLegendPane;
import com.fr.van.chart.range.component.SectionLegendPane;

import javax.swing.BorderFactory;
import javax.swing.JPanel;
import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * @author Bjorn
 * @version 10.0
 * Created by Bjorn on 2020-08-03
 */
public class VanChartValueColorPane extends BasicPane {
    //颜色类型切换按钮
    private UIButtonGroup<LegendType> valueColorTypeButton;

    //连续渐变面板
    private GradualLegendPane gradualLegendPane;
    //区域渐变面板
    private SectionLegendPane sectionLegendPane;

    private VanChartStylePane parent;

    private JPanel rangeLegendPane;

    public VanChartValueColorPane(VanChartStylePane parent) {
        this.parent = parent;
        initComponents();
    }

    public VanChartStylePane getVanChartStylePane() {
        return parent;
    }

    public void initComponents() {
        valueColorTypeButton = createLegendTypeButton();
        valueColorTypeButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                checkCardPane();
            }
        });
        valueColorTypeButton.setSelectedIndex(0);

        UILabel label = FRWidgetFactory.createLineWrapLabel(Toolkit.i18nText("Fine-Design_Chart_Color_Type"));
        Component[][] labelComponent = new Component[][]{
                new Component[]{label, valueColorTypeButton},
        };
        JPanel legendTypeButtonWithTilePane = TableLayout4VanChartHelper.createGapTableLayoutPane(labelComponent);
        legendTypeButtonWithTilePane.setBorder(BorderFactory.createEmptyBorder(10,0,0,0));
        //渐变色图例面板
        gradualLegendPane = createGradualLegendPane();
        //区域段图例面板
        sectionLegendPane = createSectionLegendPane();

        rangeLegendPane = new JPanel(new CardLayout()) {
            @Override
            public Dimension getPreferredSize() {
                if (valueColorTypeButton.getSelectedItem() == LegendType.GRADUAL) {
                    return gradualLegendPane.getPreferredSize();
                } else {
                    return sectionLegendPane.getPreferredSize();
                }
            }
        };

        rangeLegendPane.add(gradualLegendPane, LegendType.GRADUAL.getStringType());
        rangeLegendPane.add(sectionLegendPane, LegendType.SECTION.getStringType());

        double p = TableLayout.PREFERRED;
        double f = TableLayout.FILL;
        double[] col = {f};
        double[] row = {p, p};
        Component[][] components = new Component[][]{
                new Component[]{legendTypeButtonWithTilePane},
                new Component[]{rangeLegendPane}
        };
        JPanel tableLayoutPane = TableLayoutHelper.createTableLayoutPane(components, row, col);

        this.setLayout(new BorderLayout());
        this.add(tableLayoutPane, BorderLayout.CENTER);
    }

    private void checkCardPane() {
        CardLayout cardLayout = (CardLayout) rangeLegendPane.getLayout();
        cardLayout.show(rangeLegendPane, valueColorTypeButton.getSelectedItem().getStringType());
        this.validate();
        this.repaint();
    }

    private UIButtonGroup<LegendType> createLegendTypeButton() {
        return new UIButtonGroup<>(new String[]{
                Toolkit.i18nText("Fine-Design_Chart_Continuous_Gradient"),
                Toolkit.i18nText("Fine-Design_Chart_Area_Gradient")
        }, new LegendType[]{LegendType.GRADUAL, LegendType.SECTION});
    }

    protected GradualLegendPane createGradualLegendPane() {
        return new GradualLegendPane();
    }

    protected SectionLegendPane createSectionLegendPane() {
        return new SectionLegendPane(parent);
    }

    /**
     * 标题
     *
     * @return 标题
     */
    public String title4PopupWindow() {
        return PaneTitleConstants.CHART_STYLE_SERIES_TITLE;
    }

    public void updateBean(VanChartRangeLegend legend) {
        LegendType legendType = valueColorTypeButton.getSelectedItem();
        legend.setLegendType(legendType);
        if (legendType == LegendType.GRADUAL) {
            gradualLegendPane.update(legend.getGradualLegend());
        } else if (legendType == LegendType.SECTION) {
            sectionLegendPane.update(legend.getSectionLegend());
        }
    }

    public void populateBean(VanChartRangeLegend legend) {
        //范围图例部分
        if (legend.getLegendType() != LegendType.ORDINARY) {
            valueColorTypeButton.setSelectedItem(legend.getLegendType());
        }
        gradualLegendPane.populate(legend.getGradualLegend());
        sectionLegendPane.populate(legend.getSectionLegend());
        checkCardPane();
    }
}
