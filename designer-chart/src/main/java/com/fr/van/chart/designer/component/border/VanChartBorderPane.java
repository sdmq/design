package com.fr.van.chart.designer.component.border;


import com.fr.chart.base.AttrBorder;
import com.fr.chart.chartglyph.GeneralInfo;
import com.fr.design.dialog.BasicPane;
import com.fr.design.gui.ibutton.UIButtonGroup;
import com.fr.design.gui.icombobox.LineComboBox;
import com.fr.design.gui.ilable.UILabel;
import com.fr.design.i18n.Toolkit;
import com.fr.design.layout.TableLayout;
import com.fr.design.style.color.ColorSelectBox;
import com.fr.design.utils.gui.UIComponentUtils;
import com.fr.design.widget.FRWidgetFactory;
import com.fr.stable.CoreConstants;
import com.fr.van.chart.designer.TableLayout4VanChartHelper;

import javax.swing.JPanel;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

//线型 + 颜色
public class VanChartBorderPane extends BasicPane {
    private static final long serialVersionUID = -7770029552989609464L;

    private UIButtonGroup<Integer> autoButton;
    protected LineComboBox currentLineCombo;
    protected ColorSelectBox currentLineColorPane;

    protected JPanel colorPanel;

    public VanChartBorderPane() {
        this(false);
    }

    public VanChartBorderPane(boolean hasAuto) {
        if (hasAuto) {
            autoButton = new UIButtonGroup<>(new String[]{Toolkit.i18nText("Fine-Design_Chart_Automatic"),
                    Toolkit.i18nText("Fine-Design_Chart_Custom")});
            initListener();
        }
        initComponents();
        initColorPanel();
        initContent();
    }

    protected void initComponents() {
        currentLineCombo = new LineComboBox(CoreConstants.STRIKE_LINE_STYLE_ARRAY_4_CHART);
        currentLineColorPane = new ColorSelectBox(100);
    }

    protected void initContent() {
        double f = TableLayout.FILL;
        double e = TableLayout4VanChartHelper.EDIT_AREA_WIDTH;
        double[] columnSize = {f, e};
        Component[][] components = getUseComponent();
        JPanel panel = TableLayout4VanChartHelper.createGapTableLayoutPane(components, getRowSize(), columnSize);
        this.setLayout(new BorderLayout());
        this.add(panel, BorderLayout.CENTER);
    }

    protected void initColorPanel() {
        double p = TableLayout.PREFERRED;
        double f = TableLayout.FILL;
        double e = TableLayout4VanChartHelper.EDIT_AREA_WIDTH;
        double[] columnSize = {f, e};
        double[] rowSize;
        UILabel colorLabel = FRWidgetFactory.createLineWrapLabel(Toolkit.i18nText("Fine-Design_Chart_Color"));
        Component[][] components;
        if (autoButton != null) {
            rowSize = new double[]{p, p};
            components = new Component[][]{
                    new Component[]{colorLabel, autoButton},
                    new Component[]{null, currentLineColorPane}
            };
        } else {
            rowSize = new double[]{p};
            components = new Component[][]{
                    new Component[]{colorLabel, currentLineColorPane}
            };
        }
        colorPanel = TableLayout4VanChartHelper.createGapTableLayoutPane(components, rowSize, columnSize);
    }

    protected double[] getRowSize() {
        double p = TableLayout.PREFERRED;
        return new double[]{p, p, p, p};
    }

    protected Component[][] getUseComponent() {
        UILabel lineStyleLabel = FRWidgetFactory.createLineWrapLabel(Toolkit.i18nText("Fine-Design_Chart_Line_Style"));

        return new Component[][]{
                new Component[]{null, null},
                new Component[]{lineStyleLabel, UIComponentUtils.wrapWithBorderLayoutPane(currentLineCombo)},
                new Component[]{colorPanel, null},
        };
    }

    private void initListener() {
        autoButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                checkPreButton();
            }
        });
    }

    private void checkPreButton() {
        currentLineColorPane.setVisible(autoButton.getSelectedIndex() == 1);
        currentLineColorPane.setPreferredSize(autoButton.getSelectedIndex() == 1 ? new Dimension(0, 20) : new Dimension(0, 0));
    }

    /**
     * 标题
     *
     * @return 标题
     */
    public String title4PopupWindow() {
        return null;
    }

    public void populate(GeneralInfo attr) {
        if (attr == null) {
            return;
        }
        if (currentLineCombo != null) {
            currentLineCombo.setSelectedLineStyle(attr.getBorderStyle());
        }
        if (currentLineColorPane != null) {
            currentLineColorPane.setSelectObject(attr.getBorderColor());
        }

    }

    public void update(GeneralInfo attr) {
        if (attr == null) {
            attr = new GeneralInfo();
        }
        if (currentLineCombo != null) {
            attr.setBorderStyle(currentLineCombo.getSelectedLineStyle());
        }
        if (currentLineColorPane != null) {
            attr.setBorderColor(currentLineColorPane.getSelectObject());
        }

    }

    public void update(AttrBorder attrBorder) {
        if (attrBorder == null) {
            return;
        }
        if (autoButton != null) {
            attrBorder.setAutoColor(autoButton.getSelectedIndex() == 0);
        }
        if (currentLineCombo != null) {
            attrBorder.setBorderStyle(currentLineCombo.getSelectedLineStyle());
        }
        if (currentLineColorPane != null) {
            attrBorder.setBorderColor(currentLineColorPane.getSelectObject());
        }
    }

    public void populate(AttrBorder attr) {
        if (attr == null) {
            return;
        }
        if (autoButton != null) {
            autoButton.setSelectedIndex(attr.isAutoColor() ? 0 : 1);
            checkPreButton();
        }
        if (currentLineCombo != null) {
            currentLineCombo.setSelectedLineStyle(attr.getBorderStyle());
        }
        if (currentLineColorPane != null) {
            currentLineColorPane.setSelectObject(attr.getBorderColor());
        }
    }

    public AttrBorder update() {
        AttrBorder attr = new AttrBorder();
        if (autoButton != null) {
            attr.setAutoColor(autoButton.getSelectedIndex() == 0);
        }
        if (currentLineCombo != null) {
            attr.setBorderStyle(currentLineCombo.getSelectedLineStyle());
        }
        if (currentLineColorPane != null) {
            attr.setBorderColor(currentLineColorPane.getSelectObject());
        }

        return attr;
    }
}