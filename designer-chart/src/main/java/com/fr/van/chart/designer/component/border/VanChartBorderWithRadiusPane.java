package com.fr.van.chart.designer.component.border;

import com.fr.chart.base.AttrBorder;
import com.fr.chart.chartglyph.GeneralInfo;
import com.fr.design.gui.ilable.UILabel;
import com.fr.design.gui.ispinner.UISpinner;
import com.fr.design.gui.ispinner.chart.UISpinnerWithPx;
import com.fr.design.i18n.Toolkit;
import com.fr.design.utils.gui.UIComponentUtils;
import com.fr.design.widget.FRWidgetFactory;

import javax.swing.JSeparator;
import java.awt.BorderLayout;
import java.awt.Component;

/**
 * 线型 + 颜色 + 圆角半径
 */
public class VanChartBorderWithRadiusPane extends VanChartBorderPane {
    private static final long serialVersionUID = -3937853702118283803L;
    private UISpinner radius;

    public VanChartBorderWithRadiusPane() {
        super();
    }

    public VanChartBorderWithRadiusPane(boolean hasAuto) {
        super(hasAuto);
    }

    public UISpinner getRadius() {
        return radius;
    }

    @Override
    protected void initComponents() {
        super.initComponents();
        radius = new UISpinnerWithPx(0,1000,1,0);
    }

    @Override
    protected void initContent() {
        this.add(new JSeparator(), BorderLayout.SOUTH);
        super.initContent();
    }

    @Override
    protected Component[][] getUseComponent() {
        return new Component[][]{
                new Component[]{null,null},
                new Component[]{
                        FRWidgetFactory.createLineWrapLabel(Toolkit.i18nText("Fine-Design_Chart_Line_Style")),
                        UIComponentUtils.wrapWithBorderLayoutPane(currentLineCombo)
                },
                new Component[]{colorPanel, null},
                new Component[]{new UILabel(Toolkit.i18nText("Fine-Design_Report_Radius")),radius}
        } ;
    }

    public void populate(GeneralInfo attr) {
        super.populate(attr);
        if(attr == null) {
            return;
        }
        radius.setValue(attr.getRoundRadius());
    }

    public void update(GeneralInfo attr) {
        super.update(attr);
        attr.setRoundRadius((int)radius.getValue());
    }

    public void update(AttrBorder attrBorder) {
        super.update(attrBorder);
        attrBorder.setRoundRadius((int)radius.getValue());
    }

    public void populate(AttrBorder attr) {
        super.populate(attr);
        if(attr == null) {
            return;
        }
        radius.setValue(attr.getRoundRadius());
    }

    public AttrBorder update() {
        AttrBorder attr = super.update();
        attr.setRoundRadius((int)radius.getValue());
        return attr;
    }
}