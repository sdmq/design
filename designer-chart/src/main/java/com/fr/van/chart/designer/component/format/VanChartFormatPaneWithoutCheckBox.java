package com.fr.van.chart.designer.component.format;

import com.fr.van.chart.designer.style.VanChartStylePane;

import javax.swing.JPanel;

public abstract class VanChartFormatPaneWithoutCheckBox extends VanChartFormatPaneWithCheckBox {

    public VanChartFormatPaneWithoutCheckBox(VanChartStylePane parent, JPanel showOnPane) {
        super(parent, showOnPane);
    }

    protected boolean showSelectBox() {
        return false;
    }
}
