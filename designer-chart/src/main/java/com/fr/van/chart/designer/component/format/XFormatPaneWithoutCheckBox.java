package com.fr.van.chart.designer.component.format;

import com.fr.van.chart.designer.style.VanChartStylePane;

import javax.swing.JPanel;

public class XFormatPaneWithoutCheckBox extends VanChartFormatPaneWithoutCheckBox {

    public XFormatPaneWithoutCheckBox(VanChartStylePane parent, JPanel showOnPane) {
        super(parent, showOnPane);
    }

    protected String getCheckBoxText() {
        return "x";
    }
}
