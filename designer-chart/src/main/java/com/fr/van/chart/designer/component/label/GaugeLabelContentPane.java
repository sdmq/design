package com.fr.van.chart.designer.component.label;


import com.fr.design.i18n.Toolkit;
import com.fr.van.chart.designer.component.VanChartLabelContentPane;
import com.fr.van.chart.designer.style.VanChartStylePane;

import javax.swing.JPanel;

/**
 * Created by mengao on 2017/8/13.
 */
public class GaugeLabelContentPane extends VanChartLabelContentPane {

    public GaugeLabelContentPane(VanChartStylePane parent, JPanel showOnPane) {
        super(parent, showOnPane, false);
    }

    protected String getLabelContentTitle() {
        return Toolkit.i18nText("Fine-Design_Chart_Content");
    }

    protected JPanel getLabelContentPane(JPanel contentPane) {
        return contentPane;
    }

    protected boolean supportRichEditor() {
        return false;
    }

    protected boolean hasTextStylePane() {
        return false;
    }
}
