package com.fr.van.chart.designer.component.label;

import com.fr.design.i18n.Toolkit;
import com.fr.plugin.chart.base.AttrTooltipContent;
import com.fr.plugin.chart.gauge.attr.GaugeValueTooltipContent;
import com.fr.van.chart.designer.TableLayout4VanChartHelper;
import com.fr.van.chart.designer.component.format.TargetValueFormatPaneWithCheckBox;
import com.fr.van.chart.designer.component.format.ValueFormatPaneWithCheckBox;
import com.fr.van.chart.designer.style.VanChartStylePane;

import javax.swing.JPanel;
import java.awt.Component;

/**
 * 分类和值（仪表盘的值标签）
 */
public class LabelContentPaneWithCateValue extends GaugeLabelContentPane {

    private TargetValueFormatPaneWithCheckBox targetValueFormatPane;

    private static final long serialVersionUID = -8286902939543416431L;

    public LabelContentPaneWithCateValue(VanChartStylePane parent, JPanel showOnPane) {
        super(parent, showOnPane);
    }

    protected JPanel createTableLayoutPaneWithTitle(String title, JPanel panel) {
        return TableLayout4VanChartHelper.createTableLayoutPaneWithSmallTitle(title, panel);
    }

    @Override
    protected void initFormatPane(VanChartStylePane parent, JPanel showOnPane) {
        super.initFormatPane(parent, showOnPane);
        setValueFormatPane(new ValueFormatPaneWithCheckBox(parent, showOnPane) {
            @Override
            protected String getCheckBoxText() {
                return Toolkit.i18nText("Fine-Design_Chart_Value_Pointer");
            }
        });
        this.targetValueFormatPane = new TargetValueFormatPaneWithCheckBox(parent, showOnPane);
    }

    protected double[] getRowSize(double p) {
        return new double[]{p, p, p};
    }

    protected Component[][] getPaneComponents() {
        return new Component[][]{
                new Component[]{getCategoryNameFormatPane(), null},
                new Component[]{getValueFormatPane(), null},
                new Component[]{targetValueFormatPane, null},
        };
    }

    @Override
    protected void populateFormatPane(AttrTooltipContent attrTooltipContent) {
        super.populateFormatPane(attrTooltipContent);
        if (attrTooltipContent instanceof GaugeValueTooltipContent) {
            GaugeValueTooltipContent gaugeValueTooltipContent = (GaugeValueTooltipContent) attrTooltipContent;
            targetValueFormatPane.populate(gaugeValueTooltipContent.getTargetValueFormat());
        }
    }

    @Override
    protected void updateFormatPane(AttrTooltipContent attrTooltipContent) {
        super.updateFormatPane(attrTooltipContent);
        GaugeValueTooltipContent gaugeValueTooltipContent = (GaugeValueTooltipContent) attrTooltipContent;
        targetValueFormatPane.update(gaugeValueTooltipContent.getTargetValueFormat());
    }

    @Override
    public void setDirty(boolean isDirty) {
        super.setDirty(isDirty);
        targetValueFormatPane.setDirty(isDirty);
    }


    @Override
    public boolean isDirty() {
        return super.isDirty() || targetValueFormatPane.isDirty();
    }

    protected AttrTooltipContent createAttrTooltip() {
        return new GaugeValueTooltipContent();
    }
}