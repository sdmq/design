package com.fr.van.chart.designer.component.richText;

import com.fr.base.BaseUtils;
import com.fr.data.util.function.DataFunction;
import com.fr.design.gui.ibutton.UIButton;
import com.fr.design.gui.ibutton.UIButtonUI;
import com.fr.design.gui.ibutton.UIToggleButton;
import com.fr.design.layout.TableLayout;
import com.fr.design.layout.TableLayoutHelper;
import com.fr.design.utils.gui.GUIPaintUtils;
import com.fr.plugin.chart.base.FirstFunction;
import com.fr.plugin.chart.base.format.AttrTooltipDurationFormat;
import com.fr.plugin.chart.base.format.AttrTooltipFormat;
import com.fr.plugin.chart.base.format.IntervalTimeFormat;
import com.fr.stable.StringUtils;
import com.fr.van.chart.designer.TableLayout4VanChartHelper;

import javax.swing.Icon;
import javax.swing.JPanel;
import javax.swing.plaf.ButtonUI;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Graphics2D;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.text.Format;

public class VanChartFieldButton extends JPanel {

    private static final Icon ADD_ICON = BaseUtils.readIcon("/com/fr/base/images/cell/control/add.png");
    private static final Color HOVER_COLOR = new Color(232, 232, 232);

    private static final int W = 200;
    private static final int H = 24;

    private final String fieldName;
    private final String fieldId;

    private final AttrTooltipFormat tooltipFormat;
    private final boolean showDataFunction;
    private final boolean showIntervalTime;

    private UIToggleButton fieldButton;
    private UIButton addButton;

    private DataFunction dataFunction = new FirstFunction();

    public VanChartFieldButton(String fieldName, AttrTooltipFormat format, VanChartFieldListener listener) {
        this(fieldName, format, false, false, listener);
    }

    public VanChartFieldButton(String fieldName, AttrTooltipFormat format, boolean showDataFunction, VanChartFieldListener listener) {
        this(fieldName, format, showDataFunction, false, listener);
    }

    public VanChartFieldButton(String fieldName, AttrTooltipFormat format, boolean showDataFunction, boolean showIntervalTime, VanChartFieldListener listener) {
        this.fieldName = fieldName;
        this.tooltipFormat = format;

        this.showDataFunction = showDataFunction;
        this.showIntervalTime = showIntervalTime;

        this.fieldId = format == null ? StringUtils.EMPTY : format.getFormatJSONKey();

        initComponents(fieldName, listener);

        this.setLayout(new BorderLayout());
        this.add(getContentPane(), BorderLayout.CENTER);
    }

    public String getFieldName() {
        return fieldName;
    }

    public boolean isEnable() {
        return this.tooltipFormat.isEnable();
    }

    public void setEnable(boolean enable) {
        this.tooltipFormat.setEnable(enable);
    }

    public Format getFormat() {
        return tooltipFormat.getFormat();
    }

    public void setFormat(Format format) {
        this.tooltipFormat.setFormat(format);
    }

    public IntervalTimeFormat getIntervalTimeFormat() {
        if (tooltipFormat instanceof AttrTooltipDurationFormat) {
            return ((AttrTooltipDurationFormat) tooltipFormat).getIntervalTimeFormat();
        }

        return IntervalTimeFormat.DAY;
    }

    public void setIntervalTimeFormat(IntervalTimeFormat intervalTime) {
        if (tooltipFormat instanceof AttrTooltipDurationFormat) {
            ((AttrTooltipDurationFormat) tooltipFormat).setIntervalTimeFormat(intervalTime);
        }
    }

    public DataFunction getDataFunction() {
        return dataFunction;
    }

    public void setDataFunction(DataFunction dataFunction) {
        this.dataFunction = dataFunction;
    }

    public boolean isShowDataFunction() {
        return showDataFunction;
    }

    public boolean isShowIntervalTime() {
        return showIntervalTime;
    }

    public String getFormatJs() {
        return this.tooltipFormat.getJs();
    }

    private void initComponents(String fieldName, VanChartFieldListener listener) {
        fieldButton = new UIToggleButton(fieldName) {

            protected MouseListener getMouseListener() {

                return new MouseAdapter() {
                    public void mousePressed(MouseEvent e) {
                        setSelected(true);

                        listener.refreshSelectedPane(fieldName);
                        listener.setGlobalName(fieldName);
                        listener.populateFieldFormatPane();
                    }
                };
            }

            public ButtonUI getUI() {
                return new FieldButtonUI();
            }
        };

        addButton = new UIButton(ADD_ICON) {
            public ButtonUI getUI() {
                return new FieldButtonUI();
            }
        };

        addButton.addMouseListener(new MouseAdapter() {

            public void mousePressed(MouseEvent e) {
                super.mousePressed(e);

                listener.addSelectedField(fieldName, fieldId);
            }
        });

        fieldButton.setBorderPaintedOnlyWhenPressed(true);
        addButton.setBorderPaintedOnlyWhenPressed(true);
    }

    private JPanel getContentPane() {
        Component[][] components = new Component[][]{
                new Component[]{fieldButton, addButton}
        };

        double p = TableLayout.PREFERRED;
        double e = TableLayout4VanChartHelper.EDIT_AREA_WIDTH;
        double d = TableLayout4VanChartHelper.DESCRIPTION_AREA_WIDTH;

        double[] rowSize = {p};
        double[] columnSize = {e, d};

        JPanel content = TableLayoutHelper.createGapTableLayoutPane(components, rowSize, columnSize, 3, 0);
        content.setPreferredSize(new Dimension(W, H));

        return content;
    }

    public void setSelectedState(boolean selected) {
        fieldButton.setSelected(selected);
    }

    private static class FieldButtonUI extends UIButtonUI {

        protected void doExtraPainting(UIButton b, Graphics2D g2d, int w, int h, String selectedRoles) {
            if (isPressed(b) && b.isPressedPainted()) {
                GUIPaintUtils.fillPressed(g2d, 0, 0, w, h, b.isRoundBorder(), b.getRectDirection(), b.isDoneAuthorityEdited(selectedRoles));
            } else if (isRollOver(b)) {
                GUIPaintUtils.fillRollOver(g2d, 0, 0, w, h, b.isRoundBorder(), b.getRectDirection(), b.isDoneAuthorityEdited(selectedRoles), b.isPressedPainted(), HOVER_COLOR);
            } else if (b.isNormalPainted()) {
                GUIPaintUtils.fillNormal(g2d, 0, 0, w, h, b.isRoundBorder(), b.getRectDirection(), b.isDoneAuthorityEdited(selectedRoles), b.isPressedPainted());
            }
        }
    }
}
