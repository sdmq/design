package com.fr.van.chart.designer.component.richText;

import com.fr.design.beans.BasicBeanPane;
import com.fr.design.i18n.Toolkit;
import com.fr.design.layout.FRGUIPaneFactory;
import com.fr.design.ui.ModernUIPane;
import com.fr.plugin.chart.base.AttrTooltipContent;
import com.fr.stable.StringUtils;

import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import java.awt.BorderLayout;
import java.awt.Dimension;

// 标签提示中的富文本面板，包含字段设置和富文本编辑器
public class VanChartRichTextPane extends BasicBeanPane<AttrTooltipContent> {

    private static final int FIELD_PANE_W = 470;
    private static final int FIELD_PANE_H = 270;

    private static final int RICH_EDITOR_W = 940;
    private static final int RICH_EDITOR_H = 400;

    private VanChartFieldListPane fieldListPane;
    private VanChartFieldAttrPane fieldAttrPane;

    public VanChartRichTextPane(ModernUIPane<VanChartRichEditorModel> richEditor) {
        fieldAttrPane = new VanChartFieldAttrPane();
        fieldListPane = createFieldListPane(fieldAttrPane, richEditor);

        this.setLayout(new BorderLayout());
        this.add(createFieldContentPane(), BorderLayout.CENTER);
        this.add(createRichEditorPane(richEditor), BorderLayout.SOUTH);
    }

    public VanChartFieldListPane getFieldListPane() {
        return fieldListPane;
    }

    private JPanel createFieldContentPane() {
        JPanel fieldPane = new JPanel();
        fieldPane.setLayout(FRGUIPaneFactory.create2ColumnGridLayout());

        // 新增字段目录
        JPanel fieldListContent = new JPanel();
        fieldListContent.setLayout(new BorderLayout());
        fieldListContent.add(fieldListPane, BorderLayout.NORTH);

        JScrollPane fieldListScrollPane = new JScrollPane(fieldListContent);
        fieldListScrollPane.setPreferredSize(new Dimension(FIELD_PANE_W, FIELD_PANE_H));
        fieldListScrollPane.setHorizontalScrollBar(null);
        fieldListScrollPane.setBorder(BorderFactory.createTitledBorder(Toolkit.i18nText("Fine-Design_Report_Add_Field")));

        // 字段格式和汇总
        JScrollPane fieldAttrScrollPane = new JScrollPane(fieldAttrPane);
        fieldAttrScrollPane.setPreferredSize(new Dimension(FIELD_PANE_W, FIELD_PANE_H));
        fieldAttrScrollPane.setBorder(BorderFactory.createTitledBorder(Toolkit.i18nText("Fine-Design_Report_Field_Setting")));

        fieldPane.add(fieldListScrollPane);
        fieldPane.add(fieldAttrScrollPane);

        return fieldPane;
    }

    protected VanChartFieldListPane createFieldListPane(VanChartFieldAttrPane fieldAttrPane, ModernUIPane<VanChartRichEditorModel> richEditor) {
        return new VanChartFieldListPane(fieldAttrPane, richEditor);
    }

    private JPanel createRichEditorPane(JPanel richEditor) {
        JPanel richEditorPane = new JPanel();

        richEditorPane.setLayout(new BorderLayout());
        richEditorPane.setPreferredSize(new Dimension(RICH_EDITOR_W, RICH_EDITOR_H));
        richEditorPane.add(richEditor, BorderLayout.CENTER);

        return richEditorPane;
    }

    protected AttrTooltipContent getInitialTooltipContent() {
        return new AttrTooltipContent();
    }

    public void populateBean(AttrTooltipContent tooltipContent) {
        fieldListPane.populate(tooltipContent);
    }

    public AttrTooltipContent updateBean() {
        AttrTooltipContent content = getInitialTooltipContent();
        fieldListPane.update(content);
        return content;
    }

    protected String title4PopupWindow() {
        return StringUtils.EMPTY;
    }
}
