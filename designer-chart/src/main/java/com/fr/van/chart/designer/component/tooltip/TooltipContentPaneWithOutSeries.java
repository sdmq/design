package com.fr.van.chart.designer.component.tooltip;

import com.fr.design.i18n.Toolkit;
import com.fr.design.ui.ModernUIPane;
import com.fr.plugin.chart.base.AttrTooltipContent;
import com.fr.plugin.chart.base.format.AttrTooltipCategoryFormat;
import com.fr.plugin.chart.base.format.AttrTooltipFormat;
import com.fr.plugin.chart.base.format.AttrTooltipPercentFormat;
import com.fr.plugin.chart.base.format.AttrTooltipTargetValueFormat;
import com.fr.plugin.chart.base.format.AttrTooltipValueFormat;
import com.fr.plugin.chart.gauge.attr.GaugeValueTooltipContent;
import com.fr.van.chart.designer.component.VanChartTooltipContentPane;
import com.fr.van.chart.designer.component.format.TargetValueFormatPaneWithCheckBox;
import com.fr.van.chart.designer.component.format.ValueFormatPaneWithCheckBox;
import com.fr.van.chart.designer.component.richText.VanChartFieldAttrPane;
import com.fr.van.chart.designer.component.richText.VanChartFieldListPane;
import com.fr.van.chart.designer.component.richText.VanChartRichEditorModel;
import com.fr.van.chart.designer.component.richText.VanChartRichTextPane;
import com.fr.van.chart.designer.style.VanChartStylePane;

import javax.swing.JPanel;
import java.awt.Component;

/**
 * 仪表盘非多指的数据点提示（没有系列名）
 */
public class TooltipContentPaneWithOutSeries extends VanChartTooltipContentPane {

    private static final long serialVersionUID = -1973565663365672717L;

    private TargetValueFormatPaneWithCheckBox targetValueFormatPane;

    public TooltipContentPaneWithOutSeries(VanChartStylePane parent, JPanel showOnPane) {
        super(parent, showOnPane);
    }

    public TargetValueFormatPaneWithCheckBox getTargetValueFormatPane() {
        return targetValueFormatPane;
    }

    @Override
    protected void initFormatPane(VanChartStylePane parent, JPanel showOnPane) {
        super.initFormatPane(parent, showOnPane);
        setValueFormatPane(new ValueFormatPaneWithCheckBox(parent, showOnPane) {
            @Override
            protected String getCheckBoxText() {
                return Toolkit.i18nText("Fine-Design_Chart_Value_Pointer");
            }
        });
        this.targetValueFormatPane = new TargetValueFormatPaneWithCheckBox(parent, showOnPane);
    }

    protected double[] getRowSize(double p) {
        return new double[]{p, p, p, p};
    }

    protected Component[][] getPaneComponents() {
        return new Component[][]{
                new Component[]{getCategoryNameFormatPane(), null},
                new Component[]{getValueFormatPane(), null},
                new Component[]{targetValueFormatPane, null},
                new Component[]{getPercentFormatPane(), null}
        };
    }

    protected VanChartRichTextPane createRichTextPane(ModernUIPane<VanChartRichEditorModel> richEditorPane) {

        return new VanChartRichTextPane(richEditorPane) {

            protected VanChartFieldListPane createFieldListPane(VanChartFieldAttrPane fieldAttrPane, ModernUIPane<VanChartRichEditorModel> richEditor) {
                return new VanChartFieldListPaneWithOutSeries(fieldAttrPane, richEditor);
            }

            protected AttrTooltipContent getInitialTooltipContent() {
                return createAttrTooltip();
            }
        };
    }

    protected String[] getRichTextFieldNames() {
        return new String[]{
                Toolkit.i18nText("Fine-Design_Chart_Category_Use_Name"),
                Toolkit.i18nText("Fine-Design_Chart_Value_Pointer"),
                Toolkit.i18nText("Fine-Design_Chart_Target_Value"),
                Toolkit.i18nText("Fine-Design_Chart_Use_Percent")
        };
    }

    protected AttrTooltipFormat[] getRichTextFieldFormats() {
        return new AttrTooltipFormat[]{
                new AttrTooltipCategoryFormat(),
                new AttrTooltipValueFormat(),
                new AttrTooltipTargetValueFormat(),
                new AttrTooltipPercentFormat()
        };
    }

    @Override
    protected void populateFormatPane(AttrTooltipContent attrTooltipContent) {
        super.populateFormatPane(attrTooltipContent);
        if (attrTooltipContent instanceof GaugeValueTooltipContent) {
            GaugeValueTooltipContent gaugeValueTooltipContent = (GaugeValueTooltipContent) attrTooltipContent;
            targetValueFormatPane.populate(gaugeValueTooltipContent.getTargetValueFormat());
        }
    }

    @Override
    protected void updateFormatPane(AttrTooltipContent attrTooltipContent) {
        super.updateFormatPane(attrTooltipContent);
        GaugeValueTooltipContent gaugeValueTooltipContent = (GaugeValueTooltipContent) attrTooltipContent;
        targetValueFormatPane.update(gaugeValueTooltipContent.getTargetValueFormat());
    }

    protected void updateTooltipFormat(AttrTooltipContent target, AttrTooltipContent source) {
        super.updateTooltipFormat(target, source);

        if (target instanceof GaugeValueTooltipContent && source instanceof GaugeValueTooltipContent) {
            GaugeValueTooltipContent targetGauge = (GaugeValueTooltipContent) target;
            GaugeValueTooltipContent sourceGauge = (GaugeValueTooltipContent) source;
            targetGauge.setRichTextTargetValueFormat(sourceGauge.getRichTextTargetValueFormat());
        }
    }

    @Override
    public void setDirty(boolean isDirty) {
        super.setDirty(isDirty);
        targetValueFormatPane.setDirty(isDirty);
    }

    @Override
    public boolean isDirty() {
        return super.isDirty() || targetValueFormatPane.isDirty();
    }

    protected AttrTooltipContent createAttrTooltip() {
        GaugeValueTooltipContent gaugeValueTooltipContent = new GaugeValueTooltipContent();
        gaugeValueTooltipContent.setCustom(false);
        gaugeValueTooltipContent.getTargetValueFormat().setEnable(true);
        gaugeValueTooltipContent.getRichTextTargetValueFormat().setEnable(true);
        return gaugeValueTooltipContent;
    }
}