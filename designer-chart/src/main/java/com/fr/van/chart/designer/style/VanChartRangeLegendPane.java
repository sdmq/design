package com.fr.van.chart.designer.style;


import com.fr.design.layout.TableLayout;
import com.fr.design.layout.TableLayoutHelper;
import com.fr.plugin.chart.attr.VanChartLegend;
import com.fr.plugin.chart.range.SectionLegend;
import com.fr.plugin.chart.range.VanChartRangeLegend;
import com.fr.plugin.chart.type.LegendType;
import com.fr.van.chart.range.component.LegendLabelFormatPane;

import javax.swing.JPanel;
import java.awt.CardLayout;
import java.awt.Component;
import java.awt.Dimension;


/**
 * 属性表, 图表样式-图例 界面.
 */
public class VanChartRangeLegendPane extends VanChartPlotLegendPane {
    private static final long serialVersionUID = 1614283200308877353L;

    //散点图不同类型面板容器,容器布局管理
    private JPanel rangeLabelPane;
    //普通图例面板(因为普通图例没有新内容，故而为空)
    private JPanel ordinaryLabelPane;

    private LegendLabelFormatPane gradualLabelFormPane;

    private LegendLabelFormatPane sectionLabelFormPane;

    private LegendType legendType;

    public VanChartRangeLegendPane() {
        super();
    }

    public VanChartRangeLegendPane(VanChartStylePane parent) {
        super(parent);
    }

    private JPanel createRangeLabelPane() {
        ordinaryLabelPane = new JPanel();
        gradualLabelFormPane = new LegendLabelFormatPane();
        gradualLabelFormPane.setParentPane(this.getLegendPaneParent());
        sectionLabelFormPane = new LegendLabelFormatPane() {
            @Override
            protected void checkCustomLabelText() {
                setCustomFormatterText(SectionLegend.DEFAULT_LABEL_FUNCTION);
            }
        };
        sectionLabelFormPane.setParentPane(this.getLegendPaneParent());

        JPanel panel = new JPanel(new CardLayout()) {
            @Override
            public Dimension getPreferredSize() {
                if (legendType == LegendType.ORDINARY) {
                    return new Dimension(ordinaryLabelPane.getWidth(), 0);
                } else if (legendType == LegendType.GRADUAL) {
                    return gradualLabelFormPane.getPreferredSize();
                } else {
                    return sectionLabelFormPane.getPreferredSize();
                }
            }
        };

        panel.add(ordinaryLabelPane, LegendType.ORDINARY.getStringType());
        panel.add(gradualLabelFormPane, LegendType.GRADUAL.getStringType());
        panel.add(sectionLabelFormPane, LegendType.SECTION.getStringType());

        return panel;
    }

    protected JPanel createCommonLegendPane() {
        return super.createLegendPane();
    }

    @Override
    protected JPanel createLegendPane() {
        rangeLabelPane = createRangeLabelPane();

        //不包含新内容的普通面板内容
        JPanel commonLegendPane = this.createCommonLegendPane();

        double p = TableLayout.PREFERRED;
        double f = TableLayout.FILL;
        double[] col = {f};
        double[] row = {p, p};
        Component[][] components = new Component[][]{
                new Component[]{rangeLabelPane},
                new Component[]{commonLegendPane}
        };
        return TableLayoutHelper.createTableLayoutPane(components, row, col);
    }

    private void checkCardPane() {
        CardLayout cardLayout = (CardLayout) rangeLabelPane.getLayout();
        cardLayout.show(rangeLabelPane, legendType.getStringType());
    }

    @Override
    protected void checkAllUse() {
        checkBoxUse();
        checkLayoutPaneVisible();
        checkDisplayStrategyUse();
        checkCardPane();
        this.repaint();
    }

    protected boolean isVisibleLayoutPane() {

        return super.isVisibleLayoutPane() && legendType != LegendType.GRADUAL;
    }

    private void checkHighlightVisible() {
        JPanel highlightPane = this.getFixedCheckPane();

        if (highlightPane != null) {
            highlightPane.setVisible(legendType != LegendType.GRADUAL);
        }
    }

    @Override
    public void updateBean(VanChartLegend legend) {
        if (legend == null) {
            legend = new VanChartRangeLegend();
        }
        super.updateBean(legend);

        VanChartRangeLegend scatterLegend = (VanChartRangeLegend) legend;
        //范围图例部分
        if (legendType == LegendType.GRADUAL) {
            gradualLabelFormPane.update(scatterLegend.getGradualLegend().getLegendLabelFormat());
        } else if (legendType == LegendType.SECTION) {
            sectionLabelFormPane.update(scatterLegend.getSectionLegend().getLegendLabelFormat());
        }
    }

    @Override
    public void populateBean(VanChartLegend legend) {
        VanChartRangeLegend scatterLegend = (VanChartRangeLegend) legend;
        legendType = scatterLegend.getLegendType();
        if (scatterLegend != null) {
            //范围图例部分
            gradualLabelFormPane.populate(scatterLegend.getGradualLegend().getLegendLabelFormat());
            sectionLabelFormPane.populate(scatterLegend.getSectionLegend().getLegendLabelFormat());
            super.populateBean(scatterLegend);
        }
        checkAllUse();
        this.checkHighlightVisible();
    }
}