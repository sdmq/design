package com.fr.van.chart.designer.style.label;

import com.fr.chart.base.ChartConstants;
import com.fr.chart.chartattr.Plot;
import com.fr.chartx.TwoTuple;
import com.fr.design.gui.ibutton.UIButtonGroup;
import com.fr.design.gui.ilable.UILabel;
import com.fr.design.i18n.Toolkit;
import com.fr.design.layout.TableLayout;
import com.fr.design.layout.TableLayoutHelper;
import com.fr.design.mainframe.chart.gui.style.ChartTextAttrPane;
import com.fr.design.mainframe.chart.gui.style.ChartTextAttrPaneWithAuto;
import com.fr.plugin.chart.base.AttrLabelDetail;
import com.fr.plugin.chart.gauge.VanChartGaugePlot;
import com.fr.plugin.chart.type.FontAutoType;
import com.fr.plugin.chart.type.GaugeStyle;
import com.fr.stable.Constants;
import com.fr.van.chart.designer.TableLayout4VanChartHelper;
import com.fr.van.chart.designer.style.VanChartStylePane;

import javax.swing.JPanel;
import javax.swing.SwingConstants;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by mengao on 2017/8/13.
 */
public class VanChartGaugeLabelDetailPane extends VanChartPlotLabelDetailPane {

    private static final int TEXT_FONT_PANE_HEIGHT = 50;
    private GaugeStyle gaugeStyle;
    private UIButtonGroup<Integer> align;
    private JPanel alignPane;
    private Integer[] oldAlignValues;
    private UIButtonGroup<Integer> style;
    private ChartTextAttrPane textFontPane;

    public VanChartGaugeLabelDetailPane(Plot plot, VanChartStylePane parent) {
        super(plot, parent, false);
    }

    protected void initLabelDetailPane(Plot plot) {
        setGaugeStyle(((VanChartGaugePlot) plot).getGaugeStyle());
        super.initLabelDetailPane(plot);
    }

    public GaugeStyle getGaugeStyle() {
        return ((VanChartGaugePlot) this.getPlot()).getGaugeStyle();
    }

    public void setGaugeStyle(GaugeStyle gaugeStyle) {
        this.gaugeStyle = gaugeStyle;
    }

    protected JPanel createLabelStylePane(double[] row, double[] col, Plot plot) {
        style = new UIButtonGroup<Integer>(new String[]{Toolkit.i18nText("Fine-Design_Chart_Automatic"),
                Toolkit.i18nText("Fine-Design_Chart_Custom")});
        textFontPane = initTextFontPane();

        initStyleListener();

        return TableLayoutHelper.createTableLayoutPane(getLabelStyleComponents(plot), row, col);
    }

    protected void initStyleListener() {
        style.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                checkStyleUse();
            }
        });
    }

    protected boolean isFontSizeAuto() {
        return false;
    }

    //TODO Bjorn 仪表盘自动逻辑
    protected boolean isFontColorAuto() {
        return false;
        //return true;
    }

    private FontAutoType getFontAutoType() {
        if (isFontSizeAuto() && isFontColorAuto()) {
            return FontAutoType.SIZE_AND_COLOR;
        }
        if (isFontSizeAuto()) {
            return FontAutoType.SIZE;
        }
        if (isFontColorAuto()) {
            return FontAutoType.COLOR;
        }
        return FontAutoType.NONE;
    }

    protected ChartTextAttrPane initTextFontPane() {
        return new ChartTextAttrPaneWithAuto(getFontAutoType()) {
            protected double[] getRowSize() {
                double p = TableLayout.PREFERRED;
                return new double[]{p, p};
            }

            protected Component[][] getComponents(JPanel buttonPane) {
                UILabel text = new UILabel(Toolkit.i18nText("Fine-Design_Chart_Character"), SwingConstants.LEFT);
                return new Component[][]{
                        new Component[]{text, getFontNameComboBox()},
                        new Component[]{null, buttonPane}
                };
            }
        };
    }

    // 仪表盘标签内无布局tab
    protected JPanel getLabelLayoutPane(JPanel panel, String title) {
        return panel;
    }

    protected JPanel createTableLayoutPaneWithTitle(String title, JPanel panel) {
        return TableLayout4VanChartHelper.createGapTableLayoutPane(title, panel);
    }

    protected Component[][] getLabelPaneComponents(Plot plot, double p, double[] columnSize) {
        if (hasLabelAlignPane()) {
            return new Component[][]{
                    new Component[]{getDataLabelContentPane(), null},
                    new Component[]{createLabelPositionPane(getVerticalTitle(), plot), null},
                    new Component[]{createLabelAlignPane(), null},
                    new Component[]{createLabelStylePane(getLabelStyleRowSize(p), columnSize, plot), null},
            };
        } else {
            return new Component[][]{
                    new Component[]{getDataLabelContentPane(), null},
                    new Component[]{createLabelStylePane(getLabelStyleRowSize(p), columnSize, plot), null},
            };
        }
    }

    private JPanel createLabelAlignPane() {
        alignPane = new JPanel(new BorderLayout());
        checkAlignPane();
        return alignPane;
    }

    protected void checkAlignPane() {
        if (!hasLabelAlignPane()) {
            return;
        }
        if (!hasLabelAlign()) {
            alignPane.removeAll();
            return;
        }

        if (alignPane.getComponents().length > 0) {
            return;
        }
        TwoTuple<String[], Integer[]> result = getAlignNamesAndValues();
        String[] names = result.getFirst();
        Integer[] values = result.getSecond();
        align = new UIButtonGroup<>(names, values);

        Component[][] comps = new Component[2][2];

        comps[0] = new Component[]{null, null};
        comps[1] = new Component[]{new UILabel(Toolkit.i18nText("Fine-Design_Chart_Layout_Horizontal"), SwingConstants.LEFT), align};

        double[] row = new double[]{TableLayout.PREFERRED, TableLayout.PREFERRED, TableLayout.PREFERRED};
        double[] col = new double[]{TableLayout.FILL, TableLayout4VanChartHelper.EDIT_AREA_WIDTH};

        alignPane.add(getLabelPositionPane(comps, row, col), BorderLayout.CENTER);

        if (getParentPane() != null) {
            getParentPane().initListener(alignPane);
        }
    }

    private TwoTuple<String[], Integer[]> getAlignNamesAndValues() {
        String[] names = new String[]{
                Toolkit.i18nText("Fine-Design_Chart_Follow"),
                Toolkit.i18nText("Fine-Design_Chart_Align_Left"),
                Toolkit.i18nText("Fine-Design_Chart_StyleAlignment_Center"),
                Toolkit.i18nText("Fine-Design_Chart_Align_Right")
        };

        Integer[] values = new Integer[]{ChartConstants.AUTO_LABEL_POSITION, Constants.LEFT, Constants.CENTER, Constants.RIGHT};

        return new TwoTuple<>(names, values);
    }

    protected Component[][] getLabelStyleComponents(Plot plot) {
        return new Component[][]{
                new Component[]{textFontPane, null},
        };
    }

    protected void checkPane() {
        String verticalTitle = getVerticalTitle();

        checkPositionPane(verticalTitle);
        checkAlignPane();
    }

    private String getVerticalTitle() {
        return hasLabelAlign()
                ? Toolkit.i18nText("Fine-Design_Chart_Layout_Vertical")
                : Toolkit.i18nText("Fine-Design_Chart_Layout_Position");
    }

    protected void checkStyleUse() {
        textFontPane.setVisible(true);
        textFontPane.setPreferredSize(new Dimension(0, TEXT_FONT_PANE_HEIGHT));
    }

    protected boolean hasLabelAlign() {
        return getGaugeStyle() == GaugeStyle.THERMOMETER && !((VanChartGaugePlot) getPlot()).getGaugeDetailStyle().isHorizontalLayout();
    }

    protected boolean hasLabelAlignPane() {
        return getGaugeStyle() == GaugeStyle.THERMOMETER;
    }

    public void populate(AttrLabelDetail detail) {
        super.populate(detail);

        style.setSelectedIndex(1);
        textFontPane.populate(detail.getTextAttr());
        if (hasLabelAlign() && align != null) {
            align.setSelectedItem(detail.getAlign());
        }

        checkStyleUse();
    }

    public void update(AttrLabelDetail detail) {
        super.update(detail);

        detail.setCustom(true);
        if (textFontPane != null) {
            detail.setTextAttr(textFontPane.update());
        }
        if (align != null) {
            if (align.getSelectedItem() != null) {
                detail.setAlign(align.getSelectedItem());
            } else {
                align.setSelectedItem(detail.getAlign());
            }
        }
    }
}
