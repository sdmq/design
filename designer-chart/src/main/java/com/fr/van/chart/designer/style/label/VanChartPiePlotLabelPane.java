package com.fr.van.chart.designer.style.label;

import com.fr.chart.chartattr.Plot;
import com.fr.design.i18n.Toolkit;
import com.fr.plugin.chart.attr.plot.VanChartPlot;
import com.fr.plugin.chart.base.AttrLabel;
import com.fr.plugin.chart.base.AttrLabelDetail;
import com.fr.van.chart.designer.TableLayout4VanChartHelper;
import com.fr.van.chart.designer.style.VanChartStylePane;

import javax.swing.JPanel;
import java.awt.BorderLayout;

/**
 * @author Bjorn
 * @version 10.0
 * Created by Bjorn on 2020-12-09
 */
public class VanChartPiePlotLabelPane extends VanChartPlotLabelPane {

    private VanChartPlotLabelDetailPane pieCategoryLabelPane;

    public VanChartPiePlotLabelPane(Plot plot, VanChartStylePane parent, boolean inCondition) {
        super(plot, parent, inCondition);
    }

    protected void createLabelPane() {
        setLabelPane(new JPanel(new BorderLayout(0, 4)));
        setLabelDetailPane(new VanChartPieValueLabelDetailPane(getPlot(), getParentPane(), isInCondition()));
        JPanel valuePane = TableLayout4VanChartHelper.createExpandablePaneWithTitle(Toolkit.i18nText("Fine-Design_Chart_Value_Label"), getLabelDetailPane());
        getLabelPane().add(valuePane, BorderLayout.NORTH);

        if (!isInCondition()) {
            pieCategoryLabelPane = new VanChartPieCategoryLabelDetailPane(getPlot(), getParentPane(), isInCondition());
            JPanel categoryPane = TableLayout4VanChartHelper.createExpandablePaneWithTitle(Toolkit.i18nText("Fine-Design_Chart_Category_Label"), pieCategoryLabelPane);
            getLabelPane().add(categoryPane, BorderLayout.CENTER);
        }
    }

    public void populate(AttrLabel attrLabel) {
        if (attrLabel == null) {
            attrLabel = ((VanChartPlot) this.getPlot()).getDefaultAttrLabel();
        }
        super.populate(attrLabel);
        if (pieCategoryLabelPane != null) {
            AttrLabelDetail labelDetail = attrLabel.getSecondLabelDetail();
            pieCategoryLabelPane.populate(labelDetail);
        }
    }

    public AttrLabel update() {
        AttrLabel attrLabel = super.update();
        if (pieCategoryLabelPane != null) {
            AttrLabelDetail labelDetail = attrLabel.getSecondLabelDetail();
            pieCategoryLabelPane.update(labelDetail);
        } else {
            attrLabel.setSecondLabelDetail(null);
        }
        return attrLabel;
    }
}
