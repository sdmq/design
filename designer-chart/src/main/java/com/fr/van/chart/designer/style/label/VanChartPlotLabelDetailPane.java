package com.fr.van.chart.designer.style.label;

import com.fr.chart.chartattr.Plot;
import com.fr.chartx.TwoTuple;
import com.fr.design.beans.BasicBeanPane;
import com.fr.design.dialog.BasicPane;
import com.fr.design.gui.ibutton.UIButtonGroup;
import com.fr.design.gui.ibutton.UIToggleButton;
import com.fr.design.gui.ilable.UILabel;
import com.fr.design.i18n.Toolkit;
import com.fr.design.layout.TableLayout;
import com.fr.design.layout.TableLayoutHelper;
import com.fr.design.mainframe.chart.gui.style.ChartTextAttrPane;
import com.fr.design.style.color.ColorSelectBox;
import com.fr.general.ComparatorUtils;
import com.fr.plugin.chart.attr.plot.VanChartLabelPositionPlot;
import com.fr.plugin.chart.base.AttrLabelDetail;
import com.fr.plugin.chart.base.AttrTooltipContent;
import com.fr.stable.Constants;
import com.fr.van.chart.designer.PlotFactory;
import com.fr.van.chart.designer.TableLayout4VanChartHelper;
import com.fr.van.chart.designer.component.background.VanChartBackgroundWithOutImagePane;
import com.fr.van.chart.designer.component.border.VanChartBorderWithShapePane;
import com.fr.van.chart.designer.style.VanChartStylePane;

import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import java.awt.BorderLayout;
import java.awt.Component;

/**
 * Created by Mitisky on 15/12/7.
 */
public class VanChartPlotLabelDetailPane extends BasicPane {
    private static final long serialVersionUID = -22438250307946275L;

    public static final int HORIZONTAL_INDEX = 0;

    private BasicBeanPane<AttrTooltipContent> dataLabelContentPane;

    private UIButtonGroup<Integer> position;
    private UIButtonGroup<Boolean> autoAdjust;
    private UIButtonGroup<Integer> orientation;
    private UIToggleButton tractionLine;

    private ColorSelectBox backgroundColor;
    private VanChartBorderWithShapePane borderPane;
    private VanChartBackgroundWithOutImagePane backgroundPane;

    private JPanel tractionLinePane;
    private JPanel positionPane;
    private Integer[] oldPositionValues;

    private JPanel borderPaneWithTitle;
    private JPanel backgroundPaneWithTitle;

    private VanChartStylePane parent;
    private Plot plot;
    private boolean inCondition;

    public VanChartPlotLabelDetailPane(Plot plot, VanChartStylePane parent, boolean inCondition) {
        this.inCondition = inCondition;
        this.parent = parent;
        this.plot = plot;
        initLabelDetailPane(plot);
    }

    public boolean isInCondition() {
        return inCondition;
    }

    public BasicBeanPane<AttrTooltipContent> getDataLabelContentPane() {
        return dataLabelContentPane;
    }

    public void setDataLabelContentPane(BasicBeanPane<AttrTooltipContent> dataLabelContentPane) {
        this.dataLabelContentPane = dataLabelContentPane;
    }

    public VanChartStylePane getParentPane() {
        return parent;
    }

    public void setPosition(UIButtonGroup<Integer> position) {
        this.position = position;
    }

    public void setBorderPane(VanChartBorderWithShapePane borderPane) {
        this.borderPane = borderPane;
    }

    public void setBackgroundPane(VanChartBackgroundWithOutImagePane backgroundPane) {
        this.backgroundPane = backgroundPane;
    }


    protected void initLabelDetailPane(Plot plot) {
        this.setLayout(new BorderLayout());
        initToolTipContentPane(plot);
        JPanel contentPane = createLabelPane(plot);
        this.add(contentPane, BorderLayout.CENTER);
    }

    public Plot getPlot() {
        return plot;
    }

    public UIButtonGroup<Integer> getOrientation() {
        return orientation;
    }

    //默认从factory中取
    protected void initToolTipContentPane(Plot plot) {
        dataLabelContentPane = PlotFactory.createPlotLabelContentPane(plot, parent, VanChartPlotLabelDetailPane.this, inCondition);
    }

    private JPanel createLabelPane(Plot plot) {
        double p = TableLayout.PREFERRED;
        double f = TableLayout.FILL;
        double e = TableLayout4VanChartHelper.EDIT_AREA_WIDTH;
        double[] columnSize = {f, e};
        double[] rowSize = getLabelPaneRowSize(plot, p);

        Component[][] components = getLabelPaneComponents(plot, p, columnSize);
        return TableLayoutHelper.createTableLayoutPane(components, rowSize, columnSize);
    }

    protected Component[][] getLabelPaneComponents(Plot plot, double p, double[] columnSize) {
        if (hasLabelPosition(plot)) {

            // 仅饼图、柱形图、条形图、折线图、面积图含有边框和背景
            if (hasBorderAndBackground(plot)) {
                return new Component[][]{
                        new Component[]{dataLabelContentPane, null},
                        new Component[]{createLabelPositionPane(Toolkit.i18nText("Fine-Design_Chart_Layout_Position"), plot), null},
                        new Component[]{createBorderAndBackgroundPane(), null},
                };
            }

            return new Component[][]{
                    new Component[]{dataLabelContentPane, null},
                    new Component[]{createLabelPositionPane(Toolkit.i18nText("Fine-Design_Chart_Layout_Position"), plot), null}
            };
        }

        return new Component[][]{
                new Component[]{dataLabelContentPane, null}
        };
    }

    protected JPanel createBorderAndBackgroundPane() {
        JPanel jPanel = new JPanel();
        jPanel.setLayout(new BorderLayout(0, 5));
        jPanel.add(createLabelBorderPane(), BorderLayout.NORTH);
        jPanel.add(createLabelBackgroundPane(), BorderLayout.CENTER);
        return jPanel;
    }

    protected JPanel createLabelBorderPane() {
        borderPane = new VanChartBorderWithShapePane();
        borderPaneWithTitle = TableLayout4VanChartHelper.createExpandablePaneWithTitle(Toolkit.i18nText("Fine-Design_Chart_Border"), borderPane);

        return borderPaneWithTitle;
    }

    protected JPanel createLabelBackgroundPane() {
        backgroundPane = new VanChartBackgroundWithOutImagePane() {

            protected Component[][] getPaneComponents() {
                return new Component[][]{
                        new Component[]{null, null},
                        new Component[]{new UILabel(Toolkit.i18nText("Fine-Design_Report_Fill")), typeComboBox},
                        new Component[]{null, centerPane},
                        new Component[]{getTransparentLabel(), transparent},
                };
            }
        };

        backgroundPaneWithTitle = TableLayout4VanChartHelper.createExpandablePaneWithTitle(Toolkit.i18nText("Fine-Design_Chart_Background"), backgroundPane);

        return backgroundPaneWithTitle;
    }

    protected double[] getLabelStyleRowSize(double p) {
        return new double[]{p, p, p};
    }

    protected double[] getLabelPaneRowSize(Plot plot, double p) {
        return hasLabelPosition(plot) ? new double[]{p, p, p, p, p} : new double[]{p, p, p};
    }

    protected boolean hasLabelPosition(Plot plot) {
        return plot instanceof VanChartLabelPositionPlot;
    }

    private boolean hasBorderAndBackground(Plot plot) {
        return PlotFactory.hasBorderAndBackgroundPlotLabel(plot);
    }

    protected JPanel createTableLayoutPaneWithTitle(String title, JPanel panel) {
        return TableLayout4VanChartHelper.createExpandablePaneWithTitle(title, panel);
    }

    protected TwoTuple<String[], Integer[]> getPositionNamesAndValues() {
        if (plot instanceof VanChartLabelPositionPlot) {

            String[] names = ((VanChartLabelPositionPlot) plot).getLabelLocationNameArray();
            Integer[] values = ((VanChartLabelPositionPlot) plot).getLabelLocationValueArray();

            if (names == null || names.length == 0) {
                return null;
            }
            if (values == null || values.length == 0) {
                return null;
            }

            return new TwoTuple<>(names, values);
        }
        return null;
    }

    protected JPanel createLabelPositionPane(String title, Plot plot) {

        if (getPositionNamesAndValues() == null) {
            return new JPanel();
        }

        autoAdjust = new UIButtonGroup<Boolean>(new String[]{Toolkit.i18nText("Fine-Design_Chart_On"), Toolkit.i18nText("Fine-Design_Chart_Off")}, new Boolean[]{true, false});

        JPanel panel = new JPanel(new BorderLayout());

        positionPane = new JPanel();
        checkPositionPane(title);
        panel.add(positionPane, BorderLayout.NORTH);

        if (hasLabelOrientationPane()) {
            panel.add(createLabelOrientationPane(), BorderLayout.CENTER);
        }

        if (plot.isSupportLeadLine()) {
            tractionLine = new UIToggleButton(Toolkit.i18nText("Fine-Design_Chart_Show_Guideline"));
            tractionLinePane = TableLayout4VanChartHelper.createGapTableLayoutPane("", tractionLine);
            tractionLinePane.setBorder(BorderFactory.createEmptyBorder(0, 0, 5, 0));
            panel.add(tractionLinePane, BorderLayout.SOUTH);
            initPositionListener();
        } else if (PlotFactory.plotAutoAdjustLabelPosition(plot)) {
            panel.add(TableLayout4VanChartHelper.createGapTableLayoutPane(Toolkit.i18nText("Fine-Design_Chart_Auto_Adjust"), autoAdjust), BorderLayout.SOUTH);
        }

        return getLabelLayoutPane(panel, Toolkit.i18nText("Fine-Design_Form_Attr_Layout"));
    }

    protected JPanel getLabelLayoutPane(JPanel panel, String title) {
        return createTableLayoutPaneWithTitle(title, panel);
    }

    protected boolean hasLabelOrientationPane() {
        return false;
    }

    private JPanel createLabelOrientationPane() {
        orientation = new UIButtonGroup<>(new String[]{
                Toolkit.i18nText("Fine-Design_Chart_Direction_Horizontal"),
                Toolkit.i18nText("Fine-Design_Chart_Direction_Vertical"),
        });

        orientation.addChangeListener(new ChangeListener() {
            public void stateChanged(ChangeEvent e) {
                checkOrientation();
            }
        });

        return TableLayout4VanChartHelper.createGapTableLayoutPane(Toolkit.i18nText("Fine-Design_Chart_Text_Orientation"), orientation);
    }

    protected void checkPositionPane(String title) {
        if (positionPane == null) {
            return;
        }
        TwoTuple<String[], Integer[]> result = getPositionNamesAndValues();
        if (result == null) {
            return;
        }

        Integer[] values = result.getSecond();
        if (ComparatorUtils.equals(values, oldPositionValues)) {
            return;
        }
        oldPositionValues = values;

        position = new UIButtonGroup<Integer>(result.getFirst(), values);

        Component[][] comps = new Component[2][2];

        comps[0] = new Component[]{null, null};
        comps[1] = new Component[]{new UILabel(title, SwingConstants.LEFT), position};

        double[] row = new double[]{TableLayout.PREFERRED, TableLayout.PREFERRED, TableLayout.PREFERRED};
        double[] col = new double[]{TableLayout.FILL, TableLayout4VanChartHelper.EDIT_AREA_WIDTH};

        positionPane.removeAll();
        positionPane.setLayout(new BorderLayout());
        positionPane.add(getLabelPositionPane(comps, row, col), BorderLayout.CENTER);

        if (parent != null) {
            parent.initListener(positionPane);
        }
    }

    protected JPanel getLabelPositionPane(Component[][] comps, double[] row, double[] col) {
        return TableLayoutHelper.createTableLayoutPane(comps, row, col);
    }

    protected void initPositionListener() {
        position.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                checkPosition();
            }
        });
    }

    protected ChartTextAttrPane initTextFontPane() {
        return new ChartTextAttrPane() {
            protected Component[][] getComponents(JPanel buttonPane) {
                return new Component[][]{
                        new Component[]{null, null},
                        new Component[]{null, getFontNameComboBox()},
                        new Component[]{null, buttonPane}
                };
            }
        };
    }

    protected JPanel createBackgroundColorPane() {
        backgroundColor = new ColorSelectBox(100);
        return createTableLayoutPaneWithTitle(Toolkit.i18nText("Fine-Design_Chart_Background"), backgroundColor);
    }

    protected String title4PopupWindow() {
        return null;
    }

    private void checkAllUse() {
        if (tractionLine == null) {
            return;
        }
        checkPositionEnabled();
    }

    private void checkPosition() {
        tractionLine.setSelected(position.getSelectedItem() != null && position.getSelectedItem() == Constants.OUTSIDE);
        checkPositionEnabled();
    }

    protected void checkOrientation() {
        if (orientation != null && borderPaneWithTitle != null && backgroundPaneWithTitle != null) {
            boolean horizontal = orientation.getSelectedIndex() == HORIZONTAL_INDEX;

            borderPaneWithTitle.setVisible(horizontal);
            backgroundPaneWithTitle.setVisible(horizontal);
        }
    }

    private void checkPositionEnabled() {
        tractionLinePane.setVisible(position.getSelectedItem() != null && position.getSelectedItem() == Constants.OUTSIDE);
    }

    protected void checkPane() {
        checkPositionPane(Toolkit.i18nText("Fine-Design_Chart_Layout_Position"));
    }

    public void populate(AttrLabelDetail detail) {
        checkPane();
        dataLabelContentPane.populateBean(detail.getContent());
        if (position != null) {
            position.setSelectedItem(detail.getPosition());
        }
        if (orientation != null) {
            orientation.setSelectedIndex(detail.isHorizontal() ? 0 : 1);
        }
        if (tractionLine != null) {
            tractionLine.setSelected(detail.isShowGuidLine());
        }
        if (autoAdjust != null) {
            autoAdjust.setSelectedIndex(detail.isAutoAdjust() ? 0 : 1);
        }

        if (backgroundColor != null) {
            backgroundColor.setSelectObject(detail.getBackgroundColor());
        }
        if (borderPane != null) {
            borderPane.populate(detail.getBorder());
        }
        if (backgroundPane != null) {
            backgroundPane.populate(detail.getBackground());
        }

        checkAllUse();
        checkOrientation();
    }


    public void update(AttrLabelDetail detail) {
        detail.setContent(dataLabelContentPane.updateBean());

        if (position != null && position.getSelectedItem() != null) {
            detail.setPosition(position.getSelectedItem());

        } else if (position != null) {
            position.setSelectedItem(detail.getPosition());
        }

        detail.setAutoAdjust(autoAdjust != null && autoAdjust.getSelectedItem());

        if (orientation != null) {
            detail.setHorizontal(orientation.getSelectedIndex() == 0);
        }
        if (tractionLine != null) {
            detail.setShowGuidLine(tractionLine.isSelected() && detail.getPosition() == Constants.OUTSIDE);
        }
        if (backgroundColor != null) {
            detail.setBackgroundColor(backgroundColor.getSelectObject());
        }
        if (borderPane != null) {
            borderPane.update(detail.getBorder());
        }
        if (backgroundPane != null) {
            backgroundPane.update(detail.getBackground());
        }
    }

}