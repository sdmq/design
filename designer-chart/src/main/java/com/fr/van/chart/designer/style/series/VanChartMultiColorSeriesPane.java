package com.fr.van.chart.designer.style.series;

import com.fr.chart.chartattr.Plot;
import com.fr.design.gui.ibutton.UIButtonGroup;
import com.fr.design.gui.ilable.UILabel;
import com.fr.design.i18n.Toolkit;
import com.fr.design.layout.TableLayout;
import com.fr.design.layout.TableLayoutHelper;
import com.fr.design.mainframe.chart.gui.ChartStylePane;
import com.fr.design.widget.FRWidgetFactory;
import com.fr.van.chart.designer.TableLayout4VanChartHelper;

import javax.swing.BorderFactory;
import javax.swing.JPanel;
import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Component;

/**
 * @author Bjorn
 * @version 10.0
 * Created by Bjorn on 2021-05-19
 */
public abstract class VanChartMultiColorSeriesPane extends VanChartAbstractPlotSeriesPane {

    //颜色划分切换
    private UIButtonGroup<String> colorDivideButton;

    private JPanel colorDividePane;

    public VanChartMultiColorSeriesPane(ChartStylePane parent, Plot plot) {
        super(parent, plot);
    }

    public UIButtonGroup<String> getColorDivideButton() {
        return colorDivideButton;
    }

    //获取颜色面板
    protected JPanel getColorPane() {
        JPanel panel = new JPanel(new BorderLayout());
        JPanel colorChoosePane = createColorChoosePane();
        if (colorChoosePane != null) {
            panel.add(colorChoosePane, BorderLayout.CENTER);
        }

        stylePane = createStylePane();
        setColorPaneContent(panel);
        JPanel colorPane = TableLayout4VanChartHelper.createExpandablePaneWithTitle(Toolkit.i18nText("Fine-Design_Chart_Color"), panel);
        panel.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 0));
        return panel.getComponentCount() == 0 ? null : colorPane;
    }

    protected JPanel createColorChoosePane() {
        JPanel divideButtonPane = initDivideButtonPane();
        colorDividePane = createColorDividePane();

        double p = TableLayout.PREFERRED;
        double f = TableLayout.FILL;
        double[] col = {f};
        double[] row = {p, p, p, p};
        Component[][] components = new Component[][]{
                new Component[]{divideButtonPane},
                new Component[]{colorDividePane}
        };
        return TableLayoutHelper.createCommonTableLayoutPane(components, row, col, 0);
    }

    private JPanel initDivideButtonPane() {
        colorDivideButton = createDivideButton();
        colorDivideButton.addActionListener(e -> checkCardPane());
        colorDivideButton.setSelectedIndex(0);
        UILabel label = FRWidgetFactory.createLineWrapLabel(Toolkit.i18nText("Fine-Design_Chart_Color_Divide"));
        Component[][] labelComponent = new Component[][]{
                new Component[]{label, colorDivideButton},
        };
        JPanel gapTableLayoutPane = TableLayout4VanChartHelper.createGapTableLayoutPane(labelComponent);
        gapTableLayoutPane.setBorder(BorderFactory.createEmptyBorder(5, 0, 0, 0));
        return gapTableLayoutPane;
    }

    protected abstract UIButtonGroup<String> createDivideButton();

    protected abstract JPanel createColorDividePane();

    protected void checkCardPane() {
        CardLayout cardLayout = (CardLayout) colorDividePane.getLayout();
        cardLayout.show(colorDividePane, String.valueOf(colorDivideButton.getSelectedItem()));
        colorDividePane.validate();
        colorDividePane.repaint();
    }
}
