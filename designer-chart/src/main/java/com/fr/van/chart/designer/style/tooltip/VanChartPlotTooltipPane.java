package com.fr.van.chart.designer.style.tooltip;

import com.fr.chart.chartattr.Plot;
import com.fr.design.dialog.BasicPane;
import com.fr.design.gui.ibutton.UIButtonGroup;
import com.fr.design.gui.icheckbox.UICheckBox;
import com.fr.design.i18n.Toolkit;
import com.fr.design.layout.TableLayout;
import com.fr.design.layout.TableLayoutHelper;
import com.fr.design.utils.gui.UIComponentUtils;
import com.fr.design.widget.FRWidgetFactory;
import com.fr.plugin.chart.base.AttrTooltip;
import com.fr.van.chart.designer.PlotFactory;
import com.fr.van.chart.designer.TableLayout4VanChartHelper;
import com.fr.van.chart.designer.component.VanChartTooltipContentPane;
import com.fr.van.chart.designer.component.background.VanChartBackgroundWithOutImagePane;
import com.fr.van.chart.designer.component.border.VanChartBorderWithRadiusPane;
import com.fr.van.chart.designer.style.VanChartStylePane;

import javax.swing.JPanel;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class VanChartPlotTooltipPane extends BasicPane {
    private static final long serialVersionUID = 6087381131907589370L;

    // todo 使用private
    protected UICheckBox isTooltipShow;

    protected VanChartTooltipContentPane tooltipContentPane;

    protected VanChartBorderWithRadiusPane borderPane;

    protected VanChartBackgroundWithOutImagePane backgroundPane;

    protected UICheckBox showAllSeries;
    protected UIButtonGroup followMouse;

    protected VanChartStylePane parent;

    protected JPanel tooltipPane;

    public VanChartPlotTooltipPane(Plot plot, VanChartStylePane parent) {
        this.parent = parent;
        addComponents(plot);
    }

    protected  void addComponents(Plot plot) {
        isTooltipShow = new UICheckBox(Toolkit.i18nText("Fine-Design_Chart_Use_Tooltip"));
        tooltipPane = createTooltipPane(plot);

        double p = TableLayout.PREFERRED;
        double f = TableLayout.FILL;
        double[] columnSize = {f};
        double[] rowSize = {p,p};
        Component[][] components = new Component[][]{
                new Component[]{isTooltipShow},
                new Component[]{tooltipPane}
        };

        JPanel panel = TableLayoutHelper.createTableLayoutPane(components, rowSize, columnSize);
        this.setLayout(new BorderLayout());
        this.add(panel,BorderLayout.CENTER);

        isTooltipShow.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                checkBoxUse();
            }
        });
    }

    protected JPanel createTooltipPane(Plot plot) {
        borderPane = new VanChartBorderWithRadiusPane();
        backgroundPane = new VanChartBackgroundWithOutImagePane();

        initTooltipContentPane(plot);

        double p = TableLayout.PREFERRED;
        double f = TableLayout.FILL;
        double e = TableLayout4VanChartHelper.EDIT_AREA_WIDTH;
        double[] columnSize = {f, e};
        double[] rowSize = {p,p,p,p,p,p,p,p,p};

        Component[][] components = createComponents(plot);

        return TableLayoutHelper.createTableLayoutPane(components,rowSize,columnSize);
    }

    protected Component[][] createComponents(Plot plot) {
        Component[][] components = new Component[][]{
                new Component[]{tooltipContentPane,null},
                new Component[]{TableLayout4VanChartHelper.createExpandablePaneWithTitle(Toolkit.i18nText("Fine-Design_Chart_Border"),borderPane),null},
                new Component[]{TableLayout4VanChartHelper.createExpandablePaneWithTitle(Toolkit.i18nText("Fine-Design_Chart_Background"), backgroundPane),null},
                new Component[]{createDisplayStrategy(plot),null},
        };
        return components;
    }

    public VanChartStylePane getParentPane() {
        return parent;
    }

    protected void initTooltipContentPane(Plot plot){
        tooltipContentPane = PlotFactory.createPlotTooltipContentPane(plot, parent, VanChartPlotTooltipPane.this);
    }

    protected JPanel createDisplayStrategy(Plot plot) {
        showAllSeries = new UICheckBox(getShowAllSeriesLabelText());
        followMouse = new UIButtonGroup(new String[]{Toolkit.i18nText("Fine-Design_Chart_Follow_Mouse"),
                Toolkit.i18nText("Fine-Design_Chart_Not_Follow_Mouse")});
        double p = TableLayout.PREFERRED;
        double f = TableLayout.FILL;
        double e = TableLayout4VanChartHelper.EDIT_AREA_WIDTH;
        double[] columnSize = {f, e};
        double[] rowSize = { p,p,p};
        Component[][] components = new Component[3][2];
        components[0] = new Component[]{null,null};
        components[1] = new Component[]{FRWidgetFactory.createLineWrapLabel(Toolkit.i18nText("Fine-Design_Chart_Prompt_Box")), UIComponentUtils.wrapWithBorderLayoutPane(followMouse)};

        if(plot.isSupportTooltipSeriesType() && hasTooltipSeriesType()){
            components[2] = new Component[]{showAllSeries,null};
        }
        JPanel panel = TableLayout4VanChartHelper.createGapTableLayoutPane(components,rowSize,columnSize);
        return TableLayout4VanChartHelper.createExpandablePaneWithTitle(Toolkit.i18nText("Fine-Design_Chart_Display_Strategy"), panel);
    }

    protected String getShowAllSeriesLabelText() {
        return Toolkit.i18nText("Fine-Design_Chart_Show_All_Series");
    };

    protected boolean hasTooltipSeriesType() {
        return true;
    }

    @Override
    protected String title4PopupWindow() {
        return null;
    }

    private void checkAllUse() {
        checkBoxUse();
    }
    /**
     * 检查box使用.
     */
    private void checkBoxUse() {
        tooltipPane.setVisible(isTooltipShow.isSelected());
    }

    protected AttrTooltip getAttrTooltip() {
        return new AttrTooltip();
    }

    public void populate(AttrTooltip attr) {
        if(attr == null) {
            attr = getAttrTooltip();
        }

        isTooltipShow.setSelected(attr.isEnable());
        if (tooltipContentPane != null) {
            tooltipContentPane.populateBean(attr.getContent());
        }

        borderPane.populate(attr.getGeneralInfo());
        backgroundPane.populate(attr.getGeneralInfo());
        if(showAllSeries != null) {
            showAllSeries.setSelected(attr.isShowMutiSeries());
        }
        if(followMouse != null) {
            followMouse.setSelectedIndex(attr.isFollowMouse() ? 0 : 1);
        }

        checkAllUse();
    }

    public AttrTooltip update() {
        AttrTooltip attrTooltip = getAttrTooltip();

        attrTooltip.setEnable(isTooltipShow.isSelected());
        if (tooltipContentPane != null) {
            attrTooltip.setContent(tooltipContentPane.updateBean());
        }

        borderPane.update(attrTooltip.getGeneralInfo());
        backgroundPane.update(attrTooltip.getGeneralInfo());
        if(showAllSeries != null) {
            attrTooltip.setShowMutiSeries(showAllSeries.isSelected());
        }
        if(followMouse != null) {
            attrTooltip.setFollowMouse(followMouse.getSelectedIndex() == 0);
        }

        return attrTooltip;
    }
}
