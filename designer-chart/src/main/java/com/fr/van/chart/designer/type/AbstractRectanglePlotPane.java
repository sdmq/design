package com.fr.van.chart.designer.type;

import com.fr.chart.chartglyph.ConditionCollection;
import com.fr.log.FineLoggerFactory;
import com.fr.plugin.chart.PiePlot4VanChart;
import com.fr.plugin.chart.area.VanChartAreaPlot;
import com.fr.plugin.chart.attr.axis.VanChartAxis;
import com.fr.plugin.chart.attr.plot.VanChartPlot;
import com.fr.plugin.chart.attr.plot.VanChartRectanglePlot;
import com.fr.plugin.chart.bubble.VanChartBubblePlot;
import com.fr.plugin.chart.column.VanChartColumnPlot;
import com.fr.plugin.chart.line.VanChartLinePlot;
import com.fr.plugin.chart.scatter.VanChartScatterPlot;
import com.fr.plugin.chart.type.VanChartPlotType;
import com.fr.plugin.chart.vanchart.VanChart;
import com.fr.stable.StringUtils;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * @author Bjorn
 * @version 10.0
 * Created by Bjorn on 2020-08-24
 */
public abstract class AbstractRectanglePlotPane extends AbstractVanChartTypePane {

    private static Set<String> extendPlotIds = new HashSet<>();

    static {
        extendPlotIds.add(VanChartColumnPlot.VAN_CHART_COLUMN_PLOT_ID);
        extendPlotIds.add(VanChartColumnPlot.VAN_CHART_BAR_PLOT_ID);
        extendPlotIds.add(VanChartLinePlot.VAN_CHART_LINE_PLOT);
        extendPlotIds.add(VanChartAreaPlot.VAN_CHART_AREA_PLOT_ID);
        extendPlotIds.add(VanChartScatterPlot.VAN_CHART_SCATTER_PLOT_ID);
        extendPlotIds.add(PiePlot4VanChart.VAN_CHART_PIE_PLOT);
    }

    @Override
    protected VanChartPlot cloneOldPlot2New(VanChartPlot oldPlot, VanChartPlot newPlot) {
        try {
            VanChartRectanglePlot vanChartRectanglePlot = (VanChartRectanglePlot) newPlot;
            VanChartRectanglePlot clonePlot = (VanChartRectanglePlot) oldPlot.clone();
            clonePlot.setVanChartPlotType(vanChartRectanglePlot.getVanChartPlotType());
            //自定义类型的图形要增加一个y2轴，并且增加系列中的堆积条件，反之则要去掉y2轴和条件
            if (clonePlot.isCustomChart()) {
                List<VanChartAxis> valueAxisList = clonePlot.getValueAxisList();
                valueAxisList.add(vanChartRectanglePlot.getValueAxisList().get(1));
                clonePlot.setStackAndAxisCondition(vanChartRectanglePlot.getStackAndAxisCondition());
            } else {
                List<VanChartAxis> xAxisList = clonePlot.getXAxisList();
                List<VanChartAxis> yAxisList = clonePlot.getYAxisList();
                List<VanChartAxis> newXAxisList = new ArrayList<>();
                List<VanChartAxis> newYAxisList = new ArrayList<>();
                newXAxisList.add(xAxisList.get(0));
                newYAxisList.add(yAxisList.get(0));
                clonePlot.setXAxisList(newXAxisList);
                clonePlot.setYAxisList(newYAxisList);
                clonePlot.setStackAndAxisCondition(new ConditionCollection());
            }

            //百分比堆积图值轴的格式不保留
            if (clonePlot.getVanChartPlotType() == VanChartPlotType.STACK_BY_PERCENT ||
                    ((VanChartRectanglePlot) oldPlot).getVanChartPlotType() == VanChartPlotType.STACK_BY_PERCENT) {
                VanChartAxis cloneAxis = clonePlot.getValueAxisList().get(0);
                VanChartAxis vanChartAxis = vanChartRectanglePlot.getValueAxisList().get(0);
                cloneAxis.setFormat(vanChartAxis.getFormat());
                cloneAxis.setPercentage(vanChartAxis.isPercentage());
                cloneAxis.setCommonValueFormat(vanChartAxis.isCommonValueFormat());
                cloneAxis.setHtmlLabel(vanChartAxis.getHtmlLabel());
            }
            return clonePlot;
        } catch (CloneNotSupportedException ex) {
            FineLoggerFactory.getLogger().error("Error in change plot");
            return newPlot;
        }
    }

    //是否支持属性的继承
    @Override
    protected boolean supportExtendAttr(VanChart chart) {
        if (StringUtils.equals(VanChartBubblePlot.VAN_CHART_BUBBLE_PLOT_ID, chart.getID())) {
            VanChartBubblePlot vanChartBubblePlot = chart.getPlot();
            return !vanChartBubblePlot.isForceBubble();
        }
        return extendPlotIds.contains(chart.getID());
    }
}
