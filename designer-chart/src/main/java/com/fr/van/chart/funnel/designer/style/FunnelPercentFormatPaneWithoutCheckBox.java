package com.fr.van.chart.funnel.designer.style;

import com.fr.design.i18n.Toolkit;
import com.fr.van.chart.designer.component.format.PercentFormatPaneWithoutCheckBox;
import com.fr.van.chart.designer.style.VanChartStylePane;

import javax.swing.JPanel;

public class FunnelPercentFormatPaneWithoutCheckBox extends PercentFormatPaneWithoutCheckBox {

    public FunnelPercentFormatPaneWithoutCheckBox(VanChartStylePane parent, JPanel showOnPane) {
        super(parent, showOnPane);
    }

    protected String getCheckBoxText() {
        return Toolkit.i18nText("Fine-Design_Chart_Value_Conversion");
    }
}
