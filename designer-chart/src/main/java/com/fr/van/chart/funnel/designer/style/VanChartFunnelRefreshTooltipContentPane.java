package com.fr.van.chart.funnel.designer.style;

import com.fr.van.chart.designer.component.format.ChangedPercentFormatPaneWithCheckBox;
import com.fr.van.chart.designer.component.format.ChangedPercentFormatPaneWithoutCheckBox;
import com.fr.van.chart.designer.component.format.ChangedValueFormatPaneWithCheckBox;
import com.fr.van.chart.designer.component.format.ChangedValueFormatPaneWithoutCheckBox;
import com.fr.van.chart.designer.style.VanChartStylePane;

import javax.swing.JPanel;
import java.awt.Component;

/**
 * Created by mengao on 2017/6/9.
 */
public class VanChartFunnelRefreshTooltipContentPane extends VanChartFunnelTooltipContentPane {
    public VanChartFunnelRefreshTooltipContentPane(VanChartStylePane parent, JPanel showOnPane) {
        super(null, showOnPane);
    }

    protected boolean supportRichEditor() {
        return false;
    }

    @Override
    protected void initFormatPane(VanChartStylePane parent, JPanel showOnPane) {
        super.initFormatPane(parent, showOnPane);

        setChangedValueFormatPane(new ChangedValueFormatPaneWithCheckBox(parent, showOnPane));
        setChangedPercentFormatPane(new ChangedPercentFormatPaneWithCheckBox(parent, showOnPane));
    }

    protected double[] getRowSize(double p){
        return new double[]{p,p,p,p,p};
    }

    protected Component[][] getPaneComponents(){
        return new Component[][]{
                new Component[]{getSeriesNameFormatPane(), null},
                new Component[]{getValueFormatPane(), null},
                new Component[]{getChangedValueFormatPane(), null},
                new Component[]{getPercentFormatPane(), null},
                new Component[]{getChangedPercentFormatPane(), null},
        };
    }
}
