package com.fr.van.chart.gantt.designer.style.axis;

import com.fr.base.BaseUtils;
import com.fr.design.gui.ibutton.UIButtonGroup;
import com.fr.design.gui.ilable.UILabel;
import com.fr.design.i18n.Toolkit;
import com.fr.plugin.chart.gantt.attr.GanttAxisStyleAttr;
import com.fr.plugin.chart.gantt.attr.GanttAxisStyleAttrWithPosition;
import com.fr.stable.Constants;

import javax.swing.Icon;
import java.awt.Component;

/**
 * @author Bjorn
 * @version 10.0
 * Created by Bjorn on 2020-10-27
 */
public class GanttAxisStylePaneWithPosition extends GanttAxisStylePane {

    private UIButtonGroup<Integer> alignmentPane;

    protected void initComponents() {
        super.initComponents();
        Icon[] alignmentIconArray = {BaseUtils.readIcon("/com/fr/design/images/m_format/cellstyle/h_left_normal.png"),
                BaseUtils.readIcon("/com/fr/design/images/m_format/cellstyle/h_center_normal.png"),
                BaseUtils.readIcon("/com/fr/design/images/m_format/cellstyle/h_right_normal.png")};
        Integer[] alignment = new Integer[]{Constants.LEFT, Constants.CENTER, Constants.RIGHT};
        alignmentPane = new UIButtonGroup<>(alignmentIconArray, alignment);
    }

    protected Component[][] getUsedComponents() {
        return new Component[][]{
                new Component[]{getTextAttrPane(), null},
                new Component[]{new UILabel(Toolkit.i18nText("Fine-Design_Chart_Color")), getColorSelectBox4button()},
                new Component[]{new UILabel(Toolkit.i18nText("Fine-Design_Report_Alpha")), getTransparent()},
                new Component[]{new UILabel(Toolkit.i18nText("Fine-Design_Form_BorderLayout_Constraints")), alignmentPane}
        };
    }

    public void populateBean(GanttAxisStyleAttrWithPosition contentAttr) {
        super.populateBean(contentAttr);
        alignmentPane.setSelectedItem(contentAttr.getPosition());
    }

    public void updateBean(GanttAxisStyleAttrWithPosition contentAttr) {
        super.updateBean(contentAttr);
        contentAttr.setPosition(alignmentPane.getSelectedItem());
    }

    @Override
    public GanttAxisStyleAttr updateBean() {
        GanttAxisStyleAttrWithPosition styleAttr = new GanttAxisStyleAttrWithPosition();
        updateBean(styleAttr);
        return styleAttr;
    }
}
