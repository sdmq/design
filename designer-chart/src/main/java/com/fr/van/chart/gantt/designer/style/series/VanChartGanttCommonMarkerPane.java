package com.fr.van.chart.gantt.designer.style.series;

import com.fr.base.background.ColorBackground;
import com.fr.chart.chartglyph.Marker;
import com.fr.design.gui.ilable.UILabel;
import com.fr.design.i18n.Toolkit;
import com.fr.design.mainframe.chart.gui.ColorSelectBoxWithOutTransparent;
import com.fr.plugin.chart.base.VanChartAttrMarker;
import com.fr.plugin.chart.marker.type.MarkerType;
import com.fr.van.chart.designer.component.marker.VanChartCommonMarkerPane;

import java.awt.Color;
import java.awt.Component;

/**
 * Created by hufan on 2017/1/13.
 */
public class VanChartGanttCommonMarkerPane extends VanChartCommonMarkerPane {
    private static final int PREFERRED_WIDTH = 100;

    //甘特图的菱形是实心的，之前写的空心。
    //兼容：模板属性不做兼容。只是之前空心做界面兼容。前台展现实心空心一样的效果，所以不用做什么
    private static final MarkerType[] GANTT_TYPES = {
            MarkerType.MARKER_TRIANGLE,
            MarkerType.MARKER_DIAMOND,
            MarkerType.MARKER_STAR
    };

    private ColorSelectBoxWithOutTransparent colorSelect;

    @Override
    protected Marker[] getMarkers() {
        Marker[] result = new Marker[GANTT_TYPES.length];

        int i = 0;

        for (MarkerType markerType : GANTT_TYPES) {
            result[i++] = Marker.createMarker(markerType);
        }

        return result;
    }

    protected Component[][] getMarkerTypeComponent() {
        return new Component[][]{
                new Component[]{new UILabel(Toolkit.i18nText("Fine-Design_Chart_Shape")), getMarkerTypeComboBox()},
        };
    }

    protected Component[][] getMarkerConfigComponent() {
        colorSelect = new ColorSelectBoxWithOutTransparent(PREFERRED_WIDTH);

        return new Component[][]{
                new Component[]{new UILabel(Toolkit.i18nText("Fine-Design_Chart_Color")), colorSelect}
        };
    }

    @Override
    protected MarkerType populateMarkType(VanChartAttrMarker marker){
        return marker.getMarkerType() == MarkerType.MARKER_DIAMOND_HOLLOW ? MarkerType.MARKER_DIAMOND : marker.getMarkerType();
    }

    @Override
    protected void populateColor(VanChartAttrMarker marker) {
        colorSelect.setSelectObject(marker.getColorBackground().getColor());
    }

    @Override
    protected void updateColor(VanChartAttrMarker marker) {
        Color color = colorSelect.getSelectObject();
        color = color == null ? new Color(248, 182, 44) : color;

        marker.setColorBackground(ColorBackground.getInstance(color));
    }

    @Override
    public void setDefaultValue() {
        getMarkerTypeComboBox().setSelectedMarker(Marker.createMarker(MarkerType.MARKER_STAR));
        colorSelect.setSelectObject(new Color(248, 182, 44));
    }

}
