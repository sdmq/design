package com.fr.van.chart.heatmap.designer.type;

import com.fr.chart.chartattr.Chart;
import com.fr.chart.chartattr.Plot;
import com.fr.chartx.data.ChartDataDefinitionProvider;
import com.fr.design.gui.ilable.UILabel;
import com.fr.design.i18n.Toolkit;
import com.fr.design.utils.gui.GUICoreUtils;
import com.fr.log.FineLoggerFactory;
import com.fr.plugin.chart.attr.plot.VanChartPlot;
import com.fr.plugin.chart.base.VanChartTools;
import com.fr.plugin.chart.heatmap.HeatMapIndependentVanChart;
import com.fr.plugin.chart.heatmap.VanChartHeatMapPlot;
import com.fr.plugin.chart.map.server.CompatibleGEOJSONHelper;
import com.fr.plugin.chart.vanchart.VanChart;
import com.fr.van.chart.map.designer.type.GeoUrlPane;
import com.fr.van.chart.map.designer.type.VanChartMapPlotPane;
import com.fr.van.chart.map.designer.type.VanChartMapSourceChoosePane;

/**
 * Created by Mitisky on 16/10/20.
 */
public class VanChartHeatMapTypePane extends VanChartMapPlotPane {

    @Override
    protected String[] getTypeIconPath() {
        return new String[]{"/com/fr/van/chart/heatmap/images/heatmap.png"
        };
    }

    @Override
    protected VanChartMapSourceChoosePane createSourceChoosePane() {
        return new VanChartMapSourceChoosePane() {
            @Override
            protected void initGeoUrlPane() {
                setGeoUrlPane(new GeoUrlPane() {
                    @Override
                    protected UILabel createSourceTitleLabel() {
                        return new UILabel(Toolkit.i18nText("Fine-Design_Chart_Map_Area_And_Point"));
                    }
                });
            }
        };
    }

    //适用一种图表只有一种类型的
    public void populateBean(VanChart chart) {
        typeDemo.get(0).isPressing = true;
        VanChartHeatMapPlot plot = (VanChartHeatMapPlot) chart.getPlot();
        populateSourcePane(plot);

        boolean enabled = !CompatibleGEOJSONHelper.isDeprecated(plot.getGeoUrl());
        GUICoreUtils.setEnabled(this.getTypePane(), enabled);

        checkDemosBackground();
    }

    //热力地图不全屏
    @Override
    protected VanChartTools createVanChartTools() {
        VanChartTools tools = new VanChartTools();
        tools.setSort(false);
        tools.setExport(false);
        tools.setFullScreen(false);
        return tools;
    }

    protected Plot getSelectedClonedPlot() {
        Chart chart = getDefaultChart();
        VanChartHeatMapPlot newPlot = (VanChartHeatMapPlot) chart.getPlot();

        Plot cloned = null;
        try {
            cloned = (Plot) newPlot.clone();
        } catch (CloneNotSupportedException e) {
            FineLoggerFactory.getLogger().error(e.getMessage(), e);
        }
        return cloned;
    }

    protected Chart[] getDefaultCharts() {
        return HeatMapIndependentVanChart.HeatMapVanCharts;
    }

    public Chart getDefaultChart() {
        return HeatMapIndependentVanChart.HeatMapVanCharts[0];
    }

    @Override
    protected boolean acceptDefinition(ChartDataDefinitionProvider definition, VanChartPlot vanChartPlot) {
        return false;
    }
}
