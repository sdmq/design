package com.fr.van.chart.line;

import com.fr.chart.chartattr.Plot;
import com.fr.chart.chartglyph.Marker;
import com.fr.design.i18n.Toolkit;
import com.fr.design.layout.TableLayout;
import com.fr.design.layout.TableLayoutHelper;
import com.fr.design.mainframe.chart.gui.ChartStylePane;
import com.fr.plugin.chart.marker.type.MarkerType;
import com.fr.van.chart.designer.TableLayout4VanChartHelper;
import com.fr.van.chart.designer.component.VanChartBeautyPane;
import com.fr.van.chart.designer.component.VanChartMarkerPane;
import com.fr.van.chart.designer.component.marker.VanChartCommonMarkerPane;
import com.fr.van.chart.designer.style.series.VanChartAbstractPlotSeriesPane;

import javax.swing.JPanel;
import java.awt.Component;

/**
 * 折线图的系列界面
 */
public class VanChartLineSeriesPane extends VanChartAbstractPlotSeriesPane {
    private static final long serialVersionUID = 5595016643808487932L;

    public VanChartLineSeriesPane(ChartStylePane parent, Plot plot) {
        super(parent, plot);
    }

    protected JPanel getContentInPlotType() {
        double p = TableLayout.PREFERRED;
        double f = TableLayout.FILL;
        double[] row = {p, p, p, p, p, p, p, p};
        double[] col = {f};

        Component[][] components = new Component[][]{
                new Component[]{createLineTypePane()},
                new Component[]{createMarkerPane()},
                new Component[]{createStackedAndAxisPane()},
                //大数据模式 恢复用注释。下面1行删除。
                new Component[]{createLargeDataModelPane()},
                new Component[]{createTrendLinePane()},
        };

        contentPane = TableLayoutHelper.createTableLayoutPane(components, row, col);
        return contentPane;
    }

    @Override
    protected VanChartBeautyPane createStylePane() {
        return null;
    }

    protected JPanel createMarkerPane() {
        markerPane = new VanChartMarkerPane() {
            protected VanChartCommonMarkerPane createCommonMarkerPane() {

                return new VanChartCommonMarkerPane() {
                    protected Marker[] getMarkers() {
                        return getNormalMarkersWithCustom(new MarkerType[]{MarkerType.MARKER_AUTO, MarkerType.MARKER_NULL});
                    }
                };
            }
        };

        return TableLayout4VanChartHelper.createExpandablePaneWithTitle(Toolkit.i18nText("Fine-Design_Chart_Marker"), markerPane);
    }
}