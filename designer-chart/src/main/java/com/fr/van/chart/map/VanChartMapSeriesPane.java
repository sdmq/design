package com.fr.van.chart.map;

import com.fr.base.chart.chartdata.model.DataProcessor;
import com.fr.chart.chartattr.Plot;
import com.fr.chart.chartglyph.ConditionAttr;
import com.fr.design.constants.LayoutConstants;
import com.fr.design.gui.frpane.UINumberDragPane;
import com.fr.design.gui.frpane.UINumberDragPaneWithPercent;
import com.fr.design.gui.ibutton.UIButtonGroup;
import com.fr.design.gui.icombobox.UIComboBox;
import com.fr.design.gui.ilable.UILabel;
import com.fr.design.i18n.Toolkit;
import com.fr.design.layout.TableLayout;
import com.fr.design.layout.TableLayoutHelper;
import com.fr.design.mainframe.chart.gui.ChartStylePane;
import com.fr.design.style.color.ColorSelectBox;
import com.fr.design.utils.gui.GUICoreUtils;
import com.fr.design.utils.gui.UIComponentUtils;
import com.fr.design.widget.FRWidgetFactory;
import com.fr.general.ComparatorUtils;
import com.fr.general.GeneralUtils;
import com.fr.plugin.chart.VanChartAttrHelper;
import com.fr.plugin.chart.base.AttrBorderWithAlpha;
import com.fr.plugin.chart.base.AttrEffect;
import com.fr.plugin.chart.base.AttrMarkerAlpha;
import com.fr.plugin.chart.base.VanChartAttrMarker;
import com.fr.plugin.chart.bubble.attr.VanChartAttrBubble;
import com.fr.plugin.chart.drillmap.VanChartDrillMapPlot;
import com.fr.plugin.chart.map.VanChartMapPlot;
import com.fr.plugin.chart.map.attr.AttrMapLabel;
import com.fr.plugin.chart.map.line.condition.AttrCurve;
import com.fr.plugin.chart.map.line.condition.AttrLineEffect;
import com.fr.plugin.chart.type.MapMarkerType;
import com.fr.plugin.chart.type.MapType;
import com.fr.stable.StringUtils;
import com.fr.van.chart.bubble.component.VanChartBubblePane;
import com.fr.van.chart.designer.TableLayout4VanChartHelper;
import com.fr.van.chart.designer.component.border.VanChartBorderWithAlphaPane;
import com.fr.van.chart.designer.component.marker.VanChartImageMarkerPane;
import com.fr.van.chart.designer.other.VanChartInteractivePane;
import com.fr.van.chart.designer.style.series.VanChartColorValueSeriesPane;
import com.fr.van.chart.designer.style.series.VanChartEffectPane;
import com.fr.van.chart.map.designer.style.series.VanChartMapAnchorMarkerPane;
import com.fr.van.chart.map.designer.style.series.VanChartMapScatterMarkerPane;
import com.fr.van.chart.map.line.VanChartCurvePane;
import com.fr.van.chart.map.line.VanChartLineMapEffectPane;

import javax.swing.BorderFactory;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

/**
 * Created by Mitisky on 16/5/4.
 * 地图-系列界面
 */
public class VanChartMapSeriesPane extends VanChartColorValueSeriesPane {
    private static final String AREA_STRING = Toolkit.i18nText("Fine-Design_Chart_Region");
    private static final String POINT_STRING = Toolkit.i18nText("Fine-Design_Chart_Point");
    private static final String LINE_STRING = Toolkit.i18nText("Fine-Design_Chart_Flow");

    private static final String[] MARKER_TYPES = new String[]{MapMarkerType.ANCHOR.toLocalString(),
            MapMarkerType.COMMON.toLocalString(), MapMarkerType.BUBBLE.toLocalString(), MapMarkerType.IMAGE.toLocalString()};

    private static final String[] LARGE_MARKER_TYPES = new String[]{MapMarkerType.ANCHOR.toLocalString(),
            MapMarkerType.COMMON.toLocalString(), MapMarkerType.BUBBLE.toLocalString()};

    private static final String[] COMPATIBLE_MARKER_TYPES = new String[]{MapMarkerType.DEFAULT.toLocalString(),
            MapMarkerType.ANCHOR.toLocalString(), MapMarkerType.COMMON.toLocalString(),
            MapMarkerType.BUBBLE.toLocalString(), MapMarkerType.IMAGE.toLocalString()};

    private static final String[] COMPATIBLE_LARGE_MARKER_TYPES = new String[]{MapMarkerType.DEFAULT.toLocalString(),
            MapMarkerType.ANCHOR.toLocalString(), MapMarkerType.COMMON.toLocalString(),
            MapMarkerType.BUBBLE.toLocalString()};

    //custom
    private UIButtonGroup<Integer> areaPointAndLineGroup;

    //drill custom
    private UIButtonGroup<Integer> areaAndPointGroup;

    //area
    private VanChartBorderWithAlphaPane borderWithAlphaPane;

    private UIButtonGroup<Integer> nullValueAuto;
    private ColorSelectBox nullValueColorBox;

    //point
    private UIComboBox markerTypeCom;

    private VanChartMapAnchorMarkerPane anchorMarkerPane;
    private VanChartMapScatterMarkerPane commonMarkerPane;
    private VanChartBubblePane bubblePane;
    private VanChartImageMarkerPane imageMarkerPane;

    private CardLayout markerTypeLayout;
    private JPanel markerContentPane;

    private UINumberDragPane pointAlphaPane;
    private VanChartEffectPane pointEffectPane;

    //line
    private VanChartCurvePane curvePane;
    private VanChartLineMapEffectPane lineMapEffectPane;

    //大数据模式 恢复用注释。下面1行删除。
    private UIButtonGroup<DataProcessor> lineMapLargeDataModelGroup;//大数据模式

    private MapType mapType = MapType.AREA;

    public VanChartMapSeriesPane(ChartStylePane parent, Plot plot) {
        super(parent, plot);
    }

    //大数据模式 恢复用注释。删除下面4个方法 checkLarge lineMapLargeModel checkLineMapLarge createLineMapLargeDataModelPane。
    @Override
    protected void checkLarge() {
        if (largeModel(plot)) {
            if (plot instanceof VanChartMapPlot) {
                ConditionAttr defaultAttr = plot.getConditionCollection().getDefaultAttr();
                AttrMapLabel attrMapLabel = defaultAttr.getExisted(AttrMapLabel.class);
                if (attrMapLabel == null) {
                    attrMapLabel = new AttrMapLabel();
                    defaultAttr.addDataSeriesCondition(attrMapLabel);
                }
                attrMapLabel.getPointLabel().setEnable(false);

                VanChartInteractivePane.resetCustomCondition(((VanChartMapPlot) plot).getPointConditionCollection());
            }
        }

        checkPointCompsEnabledWithLarge(plot);
    }


    private boolean lineMapLargeModel() {
        return lineMapLargeDataModelGroup != null && lineMapLargeDataModelGroup.getSelectedIndex() == 0;
    }

    private void checkLineMapLarge() {
        if (lineMapLargeModel()) {
            if (plot instanceof VanChartMapPlot) {
                VanChartInteractivePane.resetCustomCondition(((VanChartMapPlot) plot).getLineConditionCollection());
            }
        }

        checkLineCompsEnabledWithLarge(plot);
    }

    private JPanel createLineMapLargeDataModelPane() {
        lineMapLargeDataModelGroup = createLargeDataModelGroup();
        lineMapLargeDataModelGroup.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                checkLineMapLarge();
            }
        });
        JPanel panel = TableLayout4VanChartHelper.createGapTableLayoutPane(Toolkit.i18nText("Fine-Design_Chart_Large_Model"), lineMapLargeDataModelGroup);
        return createLargeDataModelPane(panel);
    }


    protected void checkCompsEnabledWithLarge(Plot plot) {
        checkPointCompsEnabledWithLarge(plot);
        checkLineCompsEnabledWithLarge(plot);
    }

    private void checkPointCompsEnabledWithLarge(Plot plot) {
        checkPointEffectPane(plot);

        if (markerTypeCom == null) {
            return;
        }

        VanChartMapPlot mapPlot = (VanChartMapPlot) plot;
        boolean largeModel = largeModel(plot);
        refreshMarkerComboboxModel(mapPlot);
        if (largeModel) {
            checkLargeModelPlotSelectedItem(mapPlot);
        }
    }

    private void checkPointEffectPane(Plot plot) {
        if (pointEffectPane != null) {
            boolean largeModel = largeModel(plot);
            boolean imageMark = markerTypeCom == null ? false :
                    StringUtils.equals(MapMarkerType.IMAGE.toLocalString(), GeneralUtils.objectToString(markerTypeCom.getSelectedItem()));
            GUICoreUtils.setEnabled(pointEffectPane, !largeModel && !imageMark);
        }
    }

    private void checkLineCompsEnabledWithLarge(Plot plot) {
        if (lineMapEffectPane != null) {
            //大数据模式 恢复用注释。下面1行删除。
            GUICoreUtils.setEnabled(lineMapEffectPane, !lineMapLargeModel());
            //大数据模式 恢复用注释。取消注释。
            //GUICoreUtils.setEnabled(lineMapEffectPane, !largeModel(plot));
        }
    }

    private void checkLargeModelPlotSelectedItem(VanChartMapPlot plot) {
        String selected = plot.getMapMarkerType().toLocalString();

        if (ComparatorUtils.equals(MapMarkerType.IMAGE.toLocalString(), selected)) {
            markerTypeCom.setSelectedItem(MapMarkerType.COMMON.toLocalString());
        } else {
            markerTypeCom.setSelectedItem(selected);
        }
    }

    private boolean shouldRefreshComboboxModel(VanChartMapPlot plot) {
        if (markerTypeCom == null) {
            return false;
        }

        MapMarkerType selectedType = plot.getMapMarkerType();
        DefaultComboBoxModel model = (DefaultComboBoxModel) markerTypeCom.getModel();

        if (selectedType == MapMarkerType.DEFAULT) {
            return model.getIndexOf(MapMarkerType.DEFAULT.toLocalString()) < 0;
        }

        return model.getIndexOf(MapMarkerType.DEFAULT.toLocalString()) > -1;
    }

    private void refreshMarkerComboboxModel(VanChartMapPlot plot) {
        if (largeModel(plot)) {

            if (plot.getMapMarkerType() == MapMarkerType.DEFAULT) {
                markerTypeCom.setModel(new DefaultComboBoxModel<>(COMPATIBLE_LARGE_MARKER_TYPES));
            } else {
                markerTypeCom.setModel(new DefaultComboBoxModel<>(LARGE_MARKER_TYPES));
            }
        } else {

            if (plot.getMapMarkerType() == MapMarkerType.DEFAULT) {
                markerTypeCom.setModel(new DefaultComboBoxModel<>(COMPATIBLE_MARKER_TYPES));
            } else {
                markerTypeCom.setModel(new DefaultComboBoxModel<>(MARKER_TYPES));
            }
        }

        String selected = plot.getMapMarkerType().toLocalString();

        markerTypeCom.setSelectedItem(selected);
        if (markerTypeLayout != null && markerContentPane != null) {
            markerTypeLayout.show(markerContentPane, selected);
        }
    }

    @Override
    protected JPanel getContentPane(boolean custom) {
        JPanel panel = new JPanel(new BorderLayout());
        mapType = ((VanChartMapPlot) plot).getAllLayersMapType();
        if (mapType == MapType.CUSTOM || mapType == MapType.DRILL_CUSTOM) {
            JPanel pane = createColorChoosePane();
            pane.setBorder(BorderFactory.createEmptyBorder(0, 5, 0, 0));
            panel.add(pane, BorderLayout.NORTH);
        }
        panel.add(getContentInPlotType(), BorderLayout.CENTER);
        return panel;
    }

    //获取颜色面板
    private JPanel getColorPane(MapType paneType) {
        JPanel panel = new JPanel(new BorderLayout());
        stylePane = createStylePane();
        mapType = ((VanChartMapPlot) plot).getAllLayersMapType();
        if (mapType != MapType.CUSTOM && mapType != MapType.DRILL_CUSTOM) {
            JPanel pane = createColorChoosePane();
            if (pane != null) {
                panel.add(pane, BorderLayout.NORTH);
            }
        }
        setColorPaneContent(panel, paneType);
        JPanel colorPane = TableLayout4VanChartHelper.createExpandablePaneWithTitle(Toolkit.i18nText("Fine-Design_Chart_Color"), panel);
        panel.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 0));
        return panel.getComponentCount() == 0 ? null : colorPane;
    }

    /**
     * 在每个不同类型Plot, 得到不同类型的属性. 比如: 柱形的风格, 折线的线型曲线.
     */
    @Override
    protected JPanel getContentInPlotType() {
        mapType = ((VanChartMapPlot) plot).getAllLayersMapType();
        switch (mapType) {
            case AREA:
                return createAreaPane();
            case POINT:
                return createPointPane();
            case LINE:
                return createLinePane();
            default:
                return createCustomPane(plot);
        }
    }

    //设置色彩面板内容
    protected void setColorPaneContent(JPanel panel, MapType paneType) {
        switch (paneType) {
            case AREA:
                panel.add(createNullValueColorPane(), BorderLayout.CENTER);
                panel.add(createAlphaPane(), BorderLayout.SOUTH);
                break;
            case POINT:
                panel.add(createPointAlphaPane(), BorderLayout.CENTER);
            default:
                return;
        }
    }

    //不透明度
    protected JPanel createAlphaPane() {
        setTransparent(new UINumberDragPaneWithPercent(0, 100));
        double p = TableLayout.PREFERRED;
        double f = TableLayout.FILL;
        double[] columnSize = {f, TableLayout4VanChartHelper.EDIT_AREA_WIDTH};
        double[] rowSize = {p, p};
        UILabel label = FRWidgetFactory.createLineWrapLabel(Toolkit.i18nText("Fine-Design_Report_Alpha"));
        label.setVerticalAlignment(SwingConstants.TOP);
        Component[][] components = new Component[][]{
                new Component[]{null, null},
                new Component[]{label, UIComponentUtils.wrapWithBorderLayoutPane(getTransparent())},
        };
        return TableLayoutHelper.createGapTableLayoutPane(components, rowSize, columnSize, TableLayout4VanChartHelper.COMPONENT_INTERVAL, LayoutConstants.VGAP_LARGE);
    }

    protected JPanel createAreaPane() {
        //TODO Bjorn 地图背景自动
        //borderWithAlphaPane = new VanChartBorderWithAlphaPane(true);
        borderWithAlphaPane = new VanChartBorderWithAlphaPane();

        double p = TableLayout.PREFERRED;
        double f = TableLayout.FILL;
        double[] row = {p, p, p, p, p};
        double[] col = {f};

        Component[][] components = new Component[][]{
                new Component[]{getColorPane(MapType.AREA)},
                new Component[]{TableLayout4VanChartHelper.createExpandablePaneWithTitle(Toolkit.i18nText("Fine-Design_Chart_Border"), borderWithAlphaPane)},
        };

        return TableLayoutHelper.createTableLayoutPane(components, row, col);
    }

    protected JPanel createNullValueColorPane() {
        //TODO Bjorn 地图空值背景自动
       /* nullValueAuto = new UIButtonGroup<>(new String[]{Toolkit.i18nText("Fine-Design_Chart_Auto"),
                Toolkit.i18nText("Fine-Design_Chart_Custom")});
        initNullValueListener();
        nullValueColorBox = new ColorSelectBox(80);

        double f = TableLayout.FILL;
        double e = TableLayout4VanChartHelper.EDIT_AREA_WIDTH;
        double[] columnSize = {f, e};
        double p = TableLayout.PREFERRED;

        double[] rowSize = {p, p, p};
        UILabel text = new UILabel(Toolkit.i18nText("Fine-Design_Chart_NULL_Value_Color"), SwingConstants.LEFT);
        Component[][] components = {
                new Component[]{null, null},
                new Component[]{text, nullValueAuto},
                new Component[]{null, nullValueColorBox},
        };
        return TableLayout4VanChartHelper.createGapTableLayoutPane(components, rowSize, columnSize);*/

        nullValueColorBox = new ColorSelectBox(80);
        return TableLayout4VanChartHelper.createGapTableLayoutPane(Toolkit.i18nText("Fine-Design_Chart_NULL_Value_Color"), nullValueColorBox);
    }


    private void initNullValueListener() {
        nullValueAuto.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                checkNullValueButton();
            }
        });
    }

    private void checkNullValueButton() {
        nullValueColorBox.setVisible(nullValueAuto.getSelectedIndex() == 1);
        nullValueColorBox.setPreferredSize(nullValueAuto.getSelectedIndex() == 1 ? new Dimension(0, 20) : new Dimension(0, 0));
    }

    private JPanel createPointPane() {

        double p = TableLayout.PREFERRED;
        double f = TableLayout.FILL;
        double[] row = {p, p, p, p, p, p, p};
        double[] col = {f};

        Component[][] components = new Component[][]{
                new Component[]{getColorPane(MapType.POINT)},
                new Component[]{createMarkerComPane()},
                //大数据模式 恢复用注释。下面1行删除。
                new Component[]{createLargeDataModelPane()},
                new Component[]{createPointEffectPane()},
        };

        return TableLayoutHelper.createTableLayoutPane(components, row, col);
    }

    private JPanel createLinePane() {
        double p = TableLayout.PREFERRED;
        double f = TableLayout.FILL;
        double[] row = {p, p, p, p, p};
        double[] col = {f};

        curvePane = new VanChartCurvePane();

        Component[][] components = new Component[][]{
                new Component[]{getColorPane(MapType.LINE)},
                new Component[]{createCurvePane()},
                //大数据模式 恢复用注释。下面1行删除。
                new Component[]{createLineMapLargeDataModelPane()},
                new Component[]{createAnimationPane()}
        };

        return TableLayoutHelper.createTableLayoutPane(components, row, col);
    }

    private Component createCurvePane() {
        curvePane = new VanChartCurvePane();
        return TableLayout4VanChartHelper.createExpandablePaneWithTitle(Toolkit.i18nText("Fine-Design_Chart_Curve"), curvePane);
    }

    private Component createAnimationPane() {
        lineMapEffectPane = new VanChartLineMapEffectPane();
        return TableLayout4VanChartHelper.createExpandablePaneWithTitle(Toolkit.i18nText("Fine-Design_Chart_Animation"), lineMapEffectPane);
    }

    //不透明度
    private JPanel createPointAlphaPane() {
        pointAlphaPane = new UINumberDragPaneWithPercent(0, 100);
        return TableLayout4VanChartHelper.createGapTableLayoutPane(Toolkit.i18nText("Fine-Design_Report_Alpha"), pointAlphaPane);
    }

    private JPanel createPointEffectPane() {
        pointEffectPane = new VanChartEffectPane();
        return TableLayout4VanChartHelper.createExpandablePaneWithTitle(Toolkit.i18nText("Fine-Design_Chart_Animation"), pointEffectPane);
    }

    private JPanel createMarkerComPane() {
        markerTypeCom = new UIComboBox(COMPATIBLE_MARKER_TYPES);

        anchorMarkerPane = new VanChartMapAnchorMarkerPane();
        commonMarkerPane = new VanChartMapScatterMarkerPane();
        commonMarkerPane.setBorder(TableLayout4VanChartHelper.SECOND_EDIT_AREA_BORDER);
        bubblePane = new VanChartBubblePane() {
            protected JPanel getContentPane() {
                double p = TableLayout.PREFERRED;
                double f = TableLayout.FILL;
                double e = TableLayout4VanChartHelper.SECOND_EDIT_AREA_WIDTH;
                double[] row = {p, p, p, p, p};
                double[] col = {f, e};

                JPanel panel = TableLayoutHelper.createTableLayoutPane(getComponent(), row, col);
                panel.setBorder(TableLayout4VanChartHelper.SECOND_EDIT_AREA_BORDER);
                return panel;
            }
        };
        imageMarkerPane = new VanChartImageMarkerPane();

        final JPanel[] panes = new JPanel[]{new JPanel(), anchorMarkerPane, commonMarkerPane, bubblePane, imageMarkerPane};

        markerTypeLayout = new CardLayout();
        markerContentPane = new JPanel(markerTypeLayout) {
            @Override
            public Dimension getPreferredSize() {
                int index = MapMarkerType.parse((String) markerTypeCom.getSelectedItem()).ordinal();

                return panes[index].getPreferredSize();
            }
        };

        for (int i = 0, len = COMPATIBLE_MARKER_TYPES.length; i < len; i++) {
            markerContentPane.add(panes[i], COMPATIBLE_MARKER_TYPES[i]);
        }

        markerTypeCom.addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent e) {
                markerTypeLayout.show(markerContentPane, (String) markerTypeCom.getSelectedItem());
                checkPointEffectPane(plot);
            }
        });

        JPanel northPane = TableLayout4VanChartHelper.createGapTableLayoutPane(Toolkit.i18nText("Fine-Design_Chart_Point_Style"), markerTypeCom);
        JPanel markerPane = new JPanel(new BorderLayout(0, 6));
        markerPane.add(northPane, BorderLayout.NORTH);
        markerPane.add(markerContentPane, BorderLayout.CENTER);
        return TableLayout4VanChartHelper.createExpandablePaneWithTitle(Toolkit.i18nText("Fine-Design_Chart_Marker"), markerPane);
    }

    private JPanel createCustomPane(Plot plot) {
        JPanel areaPane = createAreaPane();
        JPanel pointPane = createPointPane();
        JPanel linePane = createLinePane();

        JPanel panel = createGroupPane(plot, areaPane, pointPane, linePane);

        return panel;
    }

    private JPanel createGroupPane(Plot plot, JPanel areaPane, JPanel pointPane, JPanel linePane) {
        JPanel panel;
        if (ComparatorUtils.equals(plot.getClass(), VanChartDrillMapPlot.class)) {
            panel = createDrillMapCustomGroupPane(areaPane, pointPane);
        } else {
            panel = createMapCustomGroupPane(areaPane, pointPane, linePane);
        }
        panel.setBorder(BorderFactory.createEmptyBorder(5, 0, 0, 0));
        return panel;
    }

    private JPanel createMapCustomGroupPane(JPanel areaPane, JPanel pointPane, JPanel linePane) {
        areaPointAndLineGroup = new UIButtonGroup<Integer>(new String[]{AREA_STRING, POINT_STRING, LINE_STRING});
        areaPointAndLineGroup.setSelectedIndex(0);

        final CardLayout cardLayout = new CardLayout();
        final JPanel centerPane = new JPanel(cardLayout);
        centerPane.add(areaPane, AREA_STRING);
        centerPane.add(pointPane, POINT_STRING);
        centerPane.add(linePane, LINE_STRING);

        areaPointAndLineGroup.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                if (areaPointAndLineGroup.getSelectedIndex() == 0) {
                    cardLayout.show(centerPane, AREA_STRING);
                } else if (areaPointAndLineGroup.getSelectedIndex() == 1) {
                    cardLayout.show(centerPane, POINT_STRING);
                } else {
                    cardLayout.show(centerPane, LINE_STRING);
                }
            }
        });

        JPanel panel = new JPanel(new BorderLayout(0, 4));
        panel.add(areaPointAndLineGroup, BorderLayout.NORTH);
        panel.add(centerPane, BorderLayout.CENTER);
        return panel;
    }

    private JPanel createDrillMapCustomGroupPane(JPanel areaPane, JPanel pointPane) {
        areaAndPointGroup = new UIButtonGroup<Integer>(new String[]{AREA_STRING, POINT_STRING});
        areaAndPointGroup.setSelectedIndex(0);

        final CardLayout cardLayout = new CardLayout();
        final JPanel centerPane = new JPanel(cardLayout);
        centerPane.add(areaPane, AREA_STRING);
        centerPane.add(pointPane, POINT_STRING);

        areaAndPointGroup.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                if (areaAndPointGroup.getSelectedIndex() == 0) {
                    cardLayout.show(centerPane, AREA_STRING);
                } else {
                    cardLayout.show(centerPane, POINT_STRING);
                }
            }
        });

        JPanel panel = new JPanel(new BorderLayout(0, 4));
        panel.add(areaAndPointGroup, BorderLayout.NORTH);
        panel.add(centerPane, BorderLayout.CENTER);
        return panel;
    }

    public void populateBean(Plot plot) {
        if (plot instanceof VanChartMapPlot) {
            VanChartMapPlot mapPlot = (VanChartMapPlot) plot;

            if (markerTypeCom != null) {

                if (shouldRefreshComboboxModel(mapPlot)) {
                    refreshMarkerComboboxModel(mapPlot);
                }

                markerTypeCom.setSelectedItem(mapPlot.getMapMarkerType().toLocalString());
            }
            if (nullValueColorBox != null) {
                //TODO Bjorn 地图空值背景自动
               /* if (((VanChartMapPlot) plot).isAutoNullValue()) {
                    nullValueAuto.setSelectedIndex(0);
                } else {
                    nullValueAuto.setSelectedIndex(1);
                }
                nullValueColorBox.setSelectObject(((VanChartMapPlot) plot).getNullValueColor());
                checkNullValueButton();*/
                nullValueColorBox.setSelectObject(mapPlot.getNullValueColor());
            }
            //大数据模式 恢复用注释。下面3行删除。
            if (lineMapLargeDataModelGroup != null) {
                lineMapLargeDataModelGroup.setSelectedItem(mapPlot.getLineMapDataProcessor());
            }
        }
        super.populateBean(plot);
    }

    public void updateBean(Plot plot) {
        if (plot instanceof VanChartMapPlot) {
            VanChartMapPlot mapPlot = (VanChartMapPlot) plot;

            if (markerTypeCom != null) {
                mapPlot.setMapMarkerType(MapMarkerType.parse((String) markerTypeCom.getSelectedItem()));

                if (shouldRefreshComboboxModel(mapPlot)) {
                    refreshMarkerComboboxModel(mapPlot);
                }
            }
            if (nullValueColorBox != null) {
                //TODO Bjorn 地图空值背景自动
                //((VanChartMapPlot) plot).setAutoNullValue(nullValueAuto.getSelectedIndex() == 0);
                mapPlot.setNullValueColor(nullValueColorBox.getSelectObject());
            }
            //大数据模式 恢复用注释。下面3行删除。
            if (lineMapLargeDataModelGroup != null) {
                mapPlot.setLineMapDataProcessor(lineMapLargeDataModelGroup.getSelectedItem());
            }
        }
        super.updateBean(plot);
    }

    @Override
    protected void populateCondition(ConditionAttr defaultAttr) {
        switch (mapType) {
            case AREA:
                populateArea(defaultAttr);
                break;
            case POINT:
                populatePoint(defaultAttr);
                break;
            case LINE:
                populateLine(defaultAttr);
                break;
            default:
                populatePoint(defaultAttr);
                populateArea(defaultAttr);
                populateLine(defaultAttr);
                break;
        }
    }

    protected void populateArea(ConditionAttr defaultAttr) {
        populateAlpha(defaultAttr);
        if (borderWithAlphaPane != null) {
            AttrBorderWithAlpha attrBorderWithAlpha = defaultAttr.getExisted(AttrBorderWithAlpha.class);
            borderWithAlphaPane.populate(attrBorderWithAlpha);
        }
    }


    private void populatePoint(ConditionAttr defaultAttr) {
        if (pointAlphaPane != null) {
            AttrMarkerAlpha attrAlpha = defaultAttr.getExisted(AttrMarkerAlpha.class);
            double alpha = VanChartAttrHelper.PERCENT * (attrAlpha == null ? 1 : attrAlpha.getAlpha());
            pointAlphaPane.populateBean(alpha);
        }

        if (pointEffectPane != null) {
            AttrEffect attrEffect = defaultAttr.getExisted(AttrEffect.class);
            if (attrEffect == null) {//老的模板做界面上的兼容
                attrEffect = new AttrEffect(3.2);
                attrEffect.setEnabled(false);
            }
            pointEffectPane.populateBean(attrEffect);
        }

        VanChartAttrMarker attrMarker = defaultAttr.getExisted(VanChartAttrMarker.class);

        if (anchorMarkerPane != null) {
            anchorMarkerPane.populateBean(attrMarker);
        }
        if (commonMarkerPane != null) {
            commonMarkerPane.populateBean(attrMarker);
        }
        if (imageMarkerPane != null) {
            imageMarkerPane.populateBean(attrMarker);
        }
        if (bubblePane != null) {
            VanChartAttrBubble attrBubble = defaultAttr.getExisted(VanChartAttrBubble.class);
            bubblePane.populateBean(attrBubble);
        }
    }

    private void populateLine(ConditionAttr defaultAttr) {
        if (curvePane != null) {
            if (defaultAttr.getExisted(AttrCurve.class) == null) {
                defaultAttr.addDataSeriesCondition(new AttrCurve());
            }
            curvePane.populateBean(defaultAttr.getExisted(AttrCurve.class));
        }
        if (lineMapEffectPane != null) {
            if (defaultAttr.getExisted(AttrLineEffect.class) == null) {
                defaultAttr.addDataSeriesCondition(new AttrLineEffect());
            }
            AttrLineEffect attrLineEffect = defaultAttr.getExisted(AttrLineEffect.class);
            lineMapEffectPane.populateBean(attrLineEffect);
        }
    }

    @Override
    protected void updateCondition(ConditionAttr defaultAttr) {
        switch (mapType) {
            case AREA:
                updateArea(defaultAttr);
                break;
            case POINT:
                updatePoint(defaultAttr);
                break;
            case LINE:
                updateLine(defaultAttr);
                break;
            default:
                updateArea(defaultAttr);
                updatePoint(defaultAttr);
                updateLine(defaultAttr);
                break;
        }
    }

    protected void checkoutMapType(Plot plot) {
        this.mapType = ((VanChartMapPlot) plot).getMapType();
    }

    protected void updateArea(ConditionAttr defaultAttr) {
        updateAlpha(defaultAttr);
        if (borderWithAlphaPane != null) {
            AttrBorderWithAlpha attrBorderWithAlpha = defaultAttr.getExisted(AttrBorderWithAlpha.class);
            defaultAttr.remove(attrBorderWithAlpha);
            defaultAttr.addDataSeriesCondition(borderWithAlphaPane.update());
        }
    }

    private void updatePoint(ConditionAttr defaultAttr) {
        if (pointAlphaPane != null) {
            AttrMarkerAlpha attrAlpha = defaultAttr.getExisted(AttrMarkerAlpha.class);
            if (attrAlpha == null) {
                attrAlpha = new AttrMarkerAlpha();
                defaultAttr.addDataSeriesCondition(attrAlpha);
            }
            attrAlpha.setAlpha((float) (pointAlphaPane.updateBean() / VanChartAttrHelper.PERCENT));
        }

        if (pointEffectPane != null) {
            AttrEffect attrEffect = defaultAttr.getExisted(AttrEffect.class);
            defaultAttr.remove(attrEffect);
            defaultAttr.addDataSeriesCondition(pointEffectPane.updateBean());
        }

        VanChartAttrMarker attrMarker = defaultAttr.getExisted(VanChartAttrMarker.class);
        defaultAttr.remove(attrMarker);
        VanChartAttrBubble attrBubble = defaultAttr.getExisted(VanChartAttrBubble.class);
        defaultAttr.remove(attrBubble);
        if (markerTypeCom != null) {
            String selected = (String) markerTypeCom.getSelectedItem();
            MapMarkerType markerType = MapMarkerType.parse(selected);

            if (markerType == MapMarkerType.ANCHOR) {
                defaultAttr.addDataSeriesCondition(anchorMarkerPane.updateBean());
            } else if (markerType == MapMarkerType.COMMON) {
                defaultAttr.addDataSeriesCondition(commonMarkerPane.updateBean());
            } else if (markerType == MapMarkerType.BUBBLE) {
                defaultAttr.addDataSeriesCondition(bubblePane.updateBean());
            } else if (markerType == MapMarkerType.IMAGE) {
                defaultAttr.addDataSeriesCondition(imageMarkerPane.updateBean());
            }
        }
    }

    private void updateLine(ConditionAttr defaultAttr) {
        if (curvePane != null) {
            AttrCurve attrCurve = defaultAttr.getExisted(AttrCurve.class);
            if (attrCurve != null) {
                defaultAttr.remove(AttrCurve.class);
            }
            attrCurve = curvePane.updateBean();
            defaultAttr.addDataSeriesCondition(attrCurve);
        }
        if (lineMapEffectPane != null) {
            AttrLineEffect attrLineEffect = defaultAttr.getExisted(AttrLineEffect.class);
            if (attrLineEffect != null) {
                defaultAttr.remove(AttrLineEffect.class);
            }
            attrLineEffect = (AttrLineEffect) lineMapEffectPane.updateBean();
            defaultAttr.addDataSeriesCondition(attrLineEffect);
        }
    }
}
