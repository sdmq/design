package com.fr.van.chart.map.designer.type;

import com.fr.base.Utils;
import com.fr.design.event.UIObserver;
import com.fr.design.event.UIObserverListener;
import com.fr.design.gui.ibutton.UIButtonGroup;
import com.fr.design.gui.icombobox.UIComboBox;
import com.fr.design.gui.ilable.UILabel;
import com.fr.design.i18n.Toolkit;
import com.fr.design.layout.TableLayout;
import com.fr.design.layout.TableLayoutHelper;
import com.fr.general.ComparatorUtils;
import com.fr.plugin.chart.base.GisLayer;
import com.fr.plugin.chart.map.VanChartMapPlot;
import com.fr.plugin.chart.map.server.MapLayerConfigManager;
import com.fr.plugin.chart.type.GISLayerType;
import com.fr.plugin.chart.type.GaoDeGisType;
import com.fr.plugin.chart.type.ZoomLevel;
import com.fr.van.chart.designer.TableLayout4VanChartHelper;

import javax.swing.BorderFactory;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JPanel;
import javax.swing.event.PopupMenuEvent;
import javax.swing.event.PopupMenuListener;
import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.ItemEvent;

/**
 * @author Bjorn
 * @version 10.0
 * Created by Bjorn on 2020-12-07
 */
public class GisLayerPane extends JPanel implements UIObserver {

    private UIButtonGroup gisButton;
    private JPanel layerPaneCheckPane;
    private UIComboBox gisGaoDeLayer;
    private UIComboBox gisLayer;
    private JPanel layerCardPane;

    private WMSLayerPane wmsLayerPane;
    private TileLayerPane tileLayerPane;

    private UIComboBox zoomLevel;

    private String[] layers = MapLayerConfigManager.getLayerItems();

    public GisLayerPane() {
        initComps();
    }

    private void initComps() {
        this.setLayout(new BorderLayout());
        this.add(createGISLayerPane(), BorderLayout.CENTER);
    }

    @Override
    public void registerChangeListener(UIObserverListener listener) {
        this.wmsLayerPane.registerChangeListener(listener);
    }

    @Override
    public boolean shouldResponseChangeListener() {
        return true;
    }

    public void registerZoomLevel(UIComboBox zoomLevel) {
        this.zoomLevel = zoomLevel;
    }

    private boolean isStandardGis() {
        return gisButton.getSelectedIndex() == 0;
    }


    private JPanel createGISLayerPane() {
        gisButton = new UIButtonGroup(new String[]{Toolkit.i18nText("Fine-Design_Form_Widget_Style_Standard"), Toolkit.i18nText("Fine-Design_Chart_Custom")});
        gisButton.setSelectedIndex(0);
        gisGaoDeLayer = new UIComboBox(MapLayerConfigManager.getGaoDeLayerItems());
        gisButton.addActionListener(event -> {
            refreshZoomLevel();
            checkLayerCardPane();
        });

        gisGaoDeLayer.addItemListener(event -> refreshZoomLevel());

        initCustomGISLayerPane();
        initLayerCardPane();

        layerPaneCheckPane = new JPanel(new CardLayout()) {
            @Override
            public Dimension getPreferredSize() {
                if (isStandardGis()) {
                    return gisGaoDeLayer.getPreferredSize();
                } else {
                    return gisLayer.getPreferredSize();
                }
            }
        };
        layerPaneCheckPane.add(gisGaoDeLayer, "standard");
        layerPaneCheckPane.add(gisLayer, "custom");


        double p = TableLayout.PREFERRED;
        double f = TableLayout.FILL;
        double e = TableLayout4VanChartHelper.EDIT_AREA_WIDTH;
        double[] row = {p, p, p};
        double[] col = {f, e};

        Component[][] components = new Component[][]{
                new Component[]{new UILabel(Toolkit.i18nText("Fine-Design_Chart_Gis_Layer")), gisButton},
                new Component[]{null, layerPaneCheckPane},
                new Component[]{layerCardPane, null},
        };

        JPanel panel = TableLayoutHelper.createTableLayoutPane(components, row, col);
        panel.setBorder(BorderFactory.createEmptyBorder(10, 0, 0, 0));
        return panel;
    }

    private void initLayerCardPane() {
        tileLayerPane = new TileLayerPane();
        wmsLayerPane = new WMSLayerPane();

        layerCardPane = new JPanel(new CardLayout()) {
            @Override
            public Dimension getPreferredSize() {
                if (isStandardGis()) {
                    return new Dimension(0, 0);
                }
                String itemName = Utils.objectToString(gisLayer.getSelectedItem());
                if (MapLayerConfigManager.isCustomLayer(itemName)) {
                    return tileLayerPane.getPreferredSize();
                } else if (MapLayerConfigManager.isCustomWmsLayer(itemName)) {
                    return wmsLayerPane.getPreferredSize();
                }
                return new Dimension(0, 0);
            }
        };

        for (String itemName : layers) {
            JPanel pane = new JPanel();
            if (MapLayerConfigManager.isCustomLayer(itemName)) {
                pane = tileLayerPane;
            } else if (MapLayerConfigManager.isCustomWmsLayer(itemName)) {
                pane = wmsLayerPane;
            }
            layerCardPane.add(pane, itemName);
        }
    }

    private void initCustomGISLayerPane() {
        gisLayer = new UIComboBox(layers);

        gisLayer.addItemListener(e ->
                {
                    if (e.getStateChange() == ItemEvent.SELECTED) {
                        checkCustomLayerCardPane();
                    }
                    refreshZoomLevel();
                }
        );

        gisLayer.addPopupMenuListener(new PopupMenuListener() {
            public void popupMenuCanceled(PopupMenuEvent e) {
            }

            public void popupMenuWillBecomeInvisible(PopupMenuEvent e) {
            }

            public void popupMenuWillBecomeVisible(PopupMenuEvent e) {

                String selected = Utils.objectToString(gisLayer.getSelectedItem());
                ZoomLevel zoomSelected = (ZoomLevel) zoomLevel.getSelectedItem();

                gisLayer.setModel(new DefaultComboBoxModel(MapLayerConfigManager.getLayerItems()));

                gisLayer.setSelectedItem(selected);
                zoomLevel.setSelectedItem(zoomSelected);
            }
        });
    }

    private void refreshZoomLevel() {
        //gis图层不同，对应的缩放等级不同。
        ZoomLevel[] levels;
        if (isStandardGis()) {
            if (gisGaoDeLayer.getSelectedIndex() == gisGaoDeLayer.getItemCount() - 1) {
                levels = MapStatusPane.ZOOM_LEVELS;
            } else {
                levels = MapStatusPane.GAODE_ZOOM_LEVELS;
            }
        } else {
            if (ComparatorUtils.equals(gisLayer.getSelectedItem(), Toolkit.i18nText("Fine-Design_Chart_Layer_Blue"))) {
                levels = MapStatusPane.BLUE_ZOOM_LEVELS;
            } else if (ComparatorUtils.equals(gisLayer.getSelectedItem(), Toolkit.i18nText("Fine-Design_Chart_Layer_GaoDe"))) {
                levels = MapStatusPane.GAODE_ZOOM_LEVELS;
            } else {
                levels = MapStatusPane.ZOOM_LEVELS;
            }
        }
        zoomLevel.setModel(new DefaultComboBoxModel(levels));
    }


    private void checkLayerCardPane() {
        CardLayout cardLayout = (CardLayout) layerPaneCheckPane.getLayout();
        cardLayout.show(layerPaneCheckPane, isStandardGis() ? "standard" : "custom");
    }

    private void checkCustomLayerCardPane() {
        CardLayout cardLayout = (CardLayout) layerCardPane.getLayout();
        cardLayout.show(layerCardPane, Utils.objectToString(gisLayer.getSelectedItem()));
    }

    public void resetGisLayer(VanChartMapPlot mapPlot) {
        //TODO Bjorn 地图gis图层自动逻辑
       /* mapPlot.getGisLayer().setGisLayerType(GISLayerType.AUTO);
        mapPlot.getGisLayer().setLayerName(GISLayerType.getLocString(GISLayerType.AUTO));*/

        GisLayer defaultGisLayer = mapPlot.getDefaultGisLayer();
        mapPlot.setGisLayer(defaultGisLayer);
        populate(defaultGisLayer);
    }

    public void populate(GisLayer layer) {
        switch (layer.getGisLayerType()) {
            case GAO_DE_API:
            case LAYER_NULL:
                populateStandardGis(layer);
                break;
            default:
                populateCustomGis(layer);
        }
        refreshZoomLevel();

        checkCustomLayerCardPane();
        checkLayerCardPane();
    }

    private void populateStandardGis(GisLayer layer) {
        gisButton.setSelectedIndex(0);
        if (layer.getGisLayerType() == GISLayerType.LAYER_NULL) {
            gisGaoDeLayer.setSelectedIndex(gisGaoDeLayer.getItemCount() - 1);
        } else {
            gisGaoDeLayer.setSelectedItem(layer.getGaoDeGisType().getTypeName());
        }
    }

    private void populateCustomGis(GisLayer layer) {
        gisButton.setSelectedIndex(1);
        gisLayer.setSelectedItem(layer.getShowItemName());

        switch (layer.getGisLayerType()) {
            case CUSTOM_WMS_LAYER:
                wmsLayerPane.populate(layer);
                break;
            case CUSTOM_TILE_LAYER:
                tileLayerPane.populate(layer);
                break;
        }
    }

    public void update(GisLayer layer) {
        if (isStandardGis()) {
            updateStandardGis(layer);
        } else {
            updateCustomGis(layer);
        }
    }

    private void updateStandardGis(GisLayer layer) {
        String layerName = Utils.objectToString(gisGaoDeLayer.getSelectedItem());
        layer.setLayerName(layerName);
        if (gisGaoDeLayer.getSelectedIndex() == gisGaoDeLayer.getItemCount() - 1) {
            layer.setGisLayerType(MapLayerConfigManager.getGisLayerType(layerName));
        } else {
            layer.setGisLayerType(GISLayerType.GAO_DE_API);
            layer.setGaoDeGisType(GaoDeGisType.parseByLocaleName(layerName));
        }
    }

    private void updateCustomGis(GisLayer layer) {
        String layerName = Utils.objectToString(gisLayer.getSelectedItem());
        layer.setLayerName(layerName);
        layer.setGisLayerType(MapLayerConfigManager.getGisLayerType(layerName));

        switch (layer.getGisLayerType()) {
            case CUSTOM_WMS_LAYER:
                wmsLayerPane.update(layer);
                break;
            case CUSTOM_TILE_LAYER:
                tileLayerPane.update(layer);
                break;
        }
    }
}
