package com.fr.van.chart.map.designer.type;

import com.fr.design.gui.ibutton.UIButtonGroup;
import com.fr.design.gui.icombobox.UIComboBox;
import com.fr.design.gui.ilable.UILabel;
import com.fr.design.gui.ispinner.UISpinner;
import com.fr.design.i18n.Toolkit;
import com.fr.design.layout.TableLayout;
import com.fr.plugin.chart.base.ViewCenter;
import com.fr.plugin.chart.map.VanChartMapPlot;
import com.fr.plugin.chart.type.ZoomLevel;
import com.fr.stable.ArrayUtils;
import com.fr.van.chart.designer.TableLayout4VanChartHelper;

import javax.swing.BorderFactory;
import javax.swing.JPanel;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;

/**
 * @author Bjorn
 * @version 10.0
 * Created by Bjorn on 2020-12-07
 */
public class MapStatusPane extends JPanel {

    public static final ZoomLevel[] ZOOM_LEVELS = new ZoomLevel[]{
            ZoomLevel.AUTO,
            ZoomLevel.ZERO, ZoomLevel.ZEROPOINTFIVE,
            ZoomLevel.ONE, ZoomLevel.ONEPOINTFIVE,
            ZoomLevel.TWO, ZoomLevel.TWOPOINTFIVE,
            ZoomLevel.THREE, ZoomLevel.THREEPOINTFIVE,
            ZoomLevel.FOUR, ZoomLevel.FOURPOINTFIVE,
            ZoomLevel.FIVE, ZoomLevel.FIVEPOINTFIVE,
            ZoomLevel.SIX, ZoomLevel.SIXPOINTFIVE,
            ZoomLevel.SEVEN, ZoomLevel.SEVENPOINTFIVE,
            ZoomLevel.EIGHT, ZoomLevel.EIGHTPOINTFIVE,
            ZoomLevel.NINE, ZoomLevel.NINEPOINTFIVE,
            ZoomLevel.TEN, ZoomLevel.TENPOINTFIVE,
            ZoomLevel.ELEVEN, ZoomLevel.ELEVENTPOINTFIVE,
            ZoomLevel.TWELVE, ZoomLevel.TWELVEPOINTFIVE,
            ZoomLevel.THIRTEEN, ZoomLevel.THIRTEENPOINTFIVE,
            ZoomLevel.FOURTEEN, ZoomLevel.FOURTEENPOINTFIVE,
            ZoomLevel.FIFTEEN, ZoomLevel.FIFTEENPOINTFIVE,
            ZoomLevel.SIXTEEN, ZoomLevel.SIXTEENPOINTFIVE,
            ZoomLevel.SEVENTEEN, ZoomLevel.SEVENTEENPOINTFIVE,
            ZoomLevel.EIGHTEEN
    };
    //深蓝和高德地图下拉框层级
    public static final ZoomLevel[] BLUE_ZOOM_LEVELS = ArrayUtils.subarray(ZOOM_LEVELS, 0, 34);
    public static final ZoomLevel[] GAODE_ZOOM_LEVELS = ArrayUtils.addAll(new ZoomLevel[]{ZoomLevel.AUTO}, (ZoomLevel[]) ArrayUtils.subarray(ZOOM_LEVELS, 7, 38));
    private static final String AUTO_CENTER_STRING = Toolkit.i18nText("Fine-Design_Chart_Automatic");
    private static final String CUSTOM_CENTER_STRING = Toolkit.i18nText("Fine-Design_Chart_Custom");


    private JPanel levelAndCenterPane;
    private JPanel longAndLatPane;

    private UIComboBox zoomLevel;
    private UIButtonGroup viewCenterCom;
    private UISpinner longitude;
    private UISpinner latitude;

    public MapStatusPane() {
        initComps();
    }

    public UIComboBox getZoomLevel() {
        return zoomLevel;
    }

    private void initComps() {
        zoomLevel = new UIComboBox(ZOOM_LEVELS);
        viewCenterCom = new UIButtonGroup(new String[]{AUTO_CENTER_STRING, CUSTOM_CENTER_STRING});
        longitude = new UISpinner(-Double.MAX_VALUE, Double.MAX_VALUE, 1, 0.0);
        latitude = new UISpinner(-Double.MAX_VALUE, Double.MAX_VALUE, 1, 0.0);

        double p = TableLayout.PREFERRED;
        double d = TableLayout4VanChartHelper.DESCRIPTION_AREA_WIDTH;
        double e = TableLayout4VanChartHelper.EDIT_AREA_WIDTH;
        double s = TableLayout4VanChartHelper.SECOND_EDIT_AREA_WIDTH;
        double[] rowSize = {p, p, p};
        double[] columnSize = {d, e};
        double[] column = {d, s};

        Component[][] comps = new Component[][]{
                new Component[]{null, null},
                new Component[]{new UILabel(Toolkit.i18nText("Fine-Design_Chart_Zoom_Layer")), zoomLevel},
                new Component[]{new UILabel(Toolkit.i18nText("Fine-Design_Chart_View_Center")), viewCenterCom},
        };
        levelAndCenterPane = TableLayout4VanChartHelper.createGapTableLayoutPane(comps, rowSize, columnSize);

        Component[][] longAndLatComps = new Component[][]{
                new Component[]{new UILabel(Toolkit.i18nText("Fine-Design_Chart_Longitude")), longitude},
                new Component[]{new UILabel(Toolkit.i18nText("Fine-Design_Chart_Latitude")), latitude}
        };
        longAndLatPane = TableLayout4VanChartHelper.createGapTableLayoutPane(longAndLatComps, rowSize, column);
        longAndLatPane.setBorder(BorderFactory.createEmptyBorder(0, 12, 0, 0));
        longAndLatPane.setVisible(false);

        this.setLayout(new BorderLayout(0, 6));

        this.add(levelAndCenterPane, BorderLayout.NORTH);
        this.add(longAndLatPane, BorderLayout.CENTER);

        viewCenterCom.addActionListener(event ->
                longAndLatPane.setVisible(!isAutoViewCenter()));
    }

    @Override
    public Dimension getPreferredSize() {
        if (longAndLatPane.isVisible()) {
            return super.getPreferredSize();
        } else {
            return levelAndCenterPane.getPreferredSize();
        }
    }

    private boolean isAutoViewCenter() {
        return viewCenterCom.getSelectedIndex() == 0;
    }


    public void resetViewCenter(VanChartMapPlot mapPlot) {
        mapPlot.setViewCenter(new ViewCenter());
        viewCenterCom.setSelectedIndex(0);
        longitude.setValue(0);
        latitude.setValue(0);
        longAndLatPane.setVisible(false);
    }

    public void resetZoomLevel(VanChartMapPlot mapPlot) {
        mapPlot.setZoomLevel(ZoomLevel.AUTO);
        zoomLevel.setSelectedItem(mapPlot.getZoomLevel());
    }

    public void populate(VanChartMapPlot mapPlot) {
        zoomLevel.setSelectedItem(mapPlot.getZoomLevel());

        ViewCenter viewCenter = mapPlot.getViewCenter();
        if (viewCenter.isAuto()) {
            viewCenterCom.setSelectedIndex(0);
            longitude.setValue(0);
            latitude.setValue(0);
        } else {
            viewCenterCom.setSelectedIndex(1);
            longitude.setValue(viewCenter.getLongitude());
            latitude.setValue(viewCenter.getLatitude());
        }

        longAndLatPane.setVisible(!isAutoViewCenter());
    }

    public void update(VanChartMapPlot mapPlot) {
        mapPlot.setZoomLevel((ZoomLevel) zoomLevel.getSelectedItem());

        ViewCenter viewCenter = mapPlot.getViewCenter();
        if (isAutoViewCenter()) {
            viewCenter.setAuto(true);
        } else {
            viewCenter.setAuto(false);
            viewCenter.setLongitude(longitude.getValue());
            viewCenter.setLatitude(latitude.getValue());
        }
    }
}
