package com.fr.van.chart.map.designer.type;

import com.fr.decision.webservice.v10.map.MapEditService;
import com.fr.design.dialog.FineJOptionPane;
import com.fr.design.event.UIObserver;
import com.fr.design.event.UIObserverListener;
import com.fr.design.gui.ibutton.UIButton;
import com.fr.design.gui.icheckbox.UICheckBox;
import com.fr.design.gui.ilable.UILabel;
import com.fr.design.gui.itextarea.UITextArea;
import com.fr.design.i18n.Toolkit;
import com.fr.design.layout.TableLayout;
import com.fr.design.layout.TableLayoutHelper;
import com.fr.plugin.chart.base.GisLayer;
import com.fr.plugin.chart.map.layer.WMSLayer;
import com.fr.van.chart.designer.TableLayout4VanChartHelper;

import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.SwingWorker;
import java.util.ArrayList;
import java.util.List;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

/**
 * @author Bjorn
 * @version 10.0
 * Created by Bjorn on 2020-12-07
 */
public class WMSLayerPane extends JPanel implements UIObserver {
    private static final double[] COLUMN_SIZE = {48, TableLayout.FILL, TableLayout.PREFERRED};

    private UITextArea wmsUrl;
    private UIButton connectButton;
    private JPanel wmsLayerPane;

    private List<UICheckBox> wmsLayerCheckBoxs = new ArrayList<>();

    private UIObserverListener listener;

    @Override
    public void registerChangeListener(UIObserverListener listener) {
        this.listener = listener;
    }

    @Override
    public boolean shouldResponseChangeListener() {
        return true;
    }


    public WMSLayerPane() {
        final double p = TableLayout.PREFERRED;
        double[] rowSize = {p};
        double[] COLUMN_SIZE = {TableLayout4VanChartHelper.DESCRIPTION_AREA_WIDTH, 84, 44};


        wmsUrl = new UITextArea();
        connectButton = new UIButton(Toolkit.i18nText("Fine-Design_Chart_Connect_WMP"));

        Component[][] comps = new Component[][]{
                new Component[]{new UILabel("url"), wmsUrl, connectButton}
        };
        JPanel northPane = TableLayout4VanChartHelper.createGapTableLayoutPane(comps, rowSize, COLUMN_SIZE);
        northPane.setBorder(TableLayout4VanChartHelper.SECOND_EDIT_AREA_BORDER);

        wmsLayerPane = new JPanel(new BorderLayout());
        resetWMSLayerPane(new ArrayList<>());

        this.setLayout(new BorderLayout(0, 6));
        this.add(northPane, BorderLayout.NORTH);
        this.add(wmsLayerPane, BorderLayout.CENTER);
        addListener();
    }

    private void addListener() {
        connectButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                new SwingWorker<Void, Double>() {
                    private List<WMSLayer> list = new ArrayList<>();

                    @Override
                    protected Void doInBackground() {
                        List<String> wmsNames = MapEditService.getInstance().getWMSNames(wmsUrl.getText());
                        list.clear();
                        for (String layer : wmsNames) {
                            list.add(new WMSLayer(layer, false));
                        }
                        return null;
                    }

                    @Override
                    protected void done() {
                        connectButton.setText(Toolkit.i18nText("Fine-Design_Chart_Connect_WMP"));
                        resetWMSLayerPane(list);
                        if (list == null || list.isEmpty()) {
                            FineJOptionPane.showMessageDialog(null, Toolkit.i18nText("Fine-Design_Chart_Invalid_WMS"));
                        }
                    }
                }.execute();
            }
        });

        connectButton.addMouseListener(new MouseAdapter() {
            @Override
            public void mousePressed(MouseEvent e) {
                connectButton.setText(Toolkit.i18nText("Fine-Design_Chart_Connecting_WMP"));
            }
        });
    }

    private void resetWMSLayerPane(java.util.List<WMSLayer> wmsLayers) {
        int size = wmsLayers.size();
        double[] rowSize = new double[size];
        Component[][] comps = new Component[size][2];
        wmsLayerCheckBoxs.clear();
        wmsLayerPane.removeAll();
        for (int i = 0; i < size; i++) {
            rowSize[i] = TableLayout.PREFERRED;
            comps[i][0] = i == 0 ? new UILabel(Toolkit.i18nText("Fine-Design_Chart_WMS_Layers"), SwingConstants.RIGHT) : null;
            WMSLayer layer = wmsLayers.get(i);
            UICheckBox checkBox = new UICheckBox(layer.getLayer());
            checkBox.registerChangeListener(listener);
            checkBox.setToolTipText(layer.getLayer());
            checkBox.setSelected(layer.isSelected());
            comps[i][1] = checkBox;
            wmsLayerCheckBoxs.add(checkBox);
        }

        wmsLayerPane.add(TableLayoutHelper.createCommonTableLayoutPane(comps, rowSize, COLUMN_SIZE, 0), BorderLayout.CENTER);

        updateUI();
    }

    public void populate(GisLayer layer) {
        wmsUrl.setText(layer.getWmsUrl());
        resetWMSLayerPane(layer.getWmsLayers());
    }

    public void update(GisLayer layer) {
        layer.setWmsUrl(wmsUrl.getText());
        layer.setWmsLayers(new ArrayList<>());
        for (UICheckBox checkBox : wmsLayerCheckBoxs) {
            layer.addWmsLayer(new WMSLayer(checkBox.getText(), checkBox.isSelected()));
        }
    }
}

