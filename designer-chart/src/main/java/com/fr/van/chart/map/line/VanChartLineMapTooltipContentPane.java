package com.fr.van.chart.map.line;

import com.fr.design.i18n.Toolkit;
import com.fr.design.ui.ModernUIPane;
import com.fr.plugin.chart.base.AttrTooltipContent;
import com.fr.plugin.chart.base.format.AttrTooltipFormat;
import com.fr.plugin.chart.base.format.AttrTooltipPercentFormat;
import com.fr.plugin.chart.base.format.AttrTooltipSeriesFormat;
import com.fr.plugin.chart.base.format.AttrTooltipStartAndEndNameFormat;
import com.fr.plugin.chart.base.format.AttrTooltipValueFormat;
import com.fr.van.chart.designer.component.VanChartTooltipContentPane;
import com.fr.van.chart.designer.component.format.PercentFormatPaneWithCheckBox;
import com.fr.van.chart.designer.component.format.SeriesNameFormatPaneWithCheckBox;
import com.fr.van.chart.designer.component.format.ValueFormatPaneWithCheckBox;
import com.fr.van.chart.designer.component.richText.VanChartFieldAttrPane;
import com.fr.van.chart.designer.component.richText.VanChartFieldListPane;
import com.fr.van.chart.designer.component.richText.VanChartRichEditorModel;
import com.fr.van.chart.designer.component.richText.VanChartRichTextPane;
import com.fr.van.chart.designer.style.VanChartStylePane;

import javax.swing.JPanel;

/**
 * Created by hufan on 2016/12/19.
 */
public class VanChartLineMapTooltipContentPane extends VanChartTooltipContentPane {
    public VanChartLineMapTooltipContentPane(VanChartStylePane parent, JPanel showOnPane) {
        super(parent, showOnPane);
    }

    @Override
    protected void initFormatPane(VanChartStylePane parent, JPanel showOnPane){
        setCategoryNameFormatPane(new StartAndEndNameFormatPaneWithCheckBox(parent, showOnPane));
        setSeriesNameFormatPane(new SeriesNameFormatPaneWithCheckBox(parent, showOnPane));
        setValueFormatPane(new ValueFormatPaneWithCheckBox(parent, showOnPane));
        setPercentFormatPane(new PercentFormatPaneWithCheckBox(parent, showOnPane));
    }

    protected VanChartRichTextPane createRichTextPane(ModernUIPane<VanChartRichEditorModel> richEditorPane) {

        return new VanChartRichTextPane(richEditorPane) {

            protected VanChartFieldListPane createFieldListPane(VanChartFieldAttrPane fieldAttrPane, ModernUIPane<VanChartRichEditorModel> richEditor) {
                return new VanChartLineMapRichTextFieldListPane(fieldAttrPane, richEditor);
            }

            protected AttrTooltipContent getInitialTooltipContent() {
                return createAttrTooltip();
            }
        };
    }

    protected String[] getRichTextFieldNames() {
        return new String[]{
                Toolkit.i18nText("Fine-Design_Chart_Start_And_End"),
                Toolkit.i18nText("Fine-Design_Chart_Series_Name"),
                Toolkit.i18nText("Fine-Design_Chart_Use_Value"),
                Toolkit.i18nText("Fine-Design_Chart_Use_Percent")
        };
    }

    protected AttrTooltipFormat[] getRichTextFieldFormats() {
        return new AttrTooltipFormat[]{
                new AttrTooltipStartAndEndNameFormat(),
                new AttrTooltipSeriesFormat(),
                new AttrTooltipValueFormat(),
                new AttrTooltipPercentFormat()
        };
    }

    @Override
    protected AttrTooltipContent createAttrTooltip() {
        AttrTooltipContent content = new AttrTooltipContent();
        content.setCategoryFormat(new AttrTooltipStartAndEndNameFormat());
        content.setValueFormat(new AttrTooltipValueFormat());
        content.setRichTextCategoryFormat(new AttrTooltipStartAndEndNameFormat());
        content.setRichTextValueFormat(new AttrTooltipValueFormat());
        return content;
    }

}
