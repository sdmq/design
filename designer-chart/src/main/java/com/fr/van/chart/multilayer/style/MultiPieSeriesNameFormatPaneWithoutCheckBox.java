package com.fr.van.chart.multilayer.style;

import com.fr.design.i18n.Toolkit;
import com.fr.van.chart.designer.component.format.SeriesNameFormatPaneWithoutCheckBox;
import com.fr.van.chart.designer.style.VanChartStylePane;

import javax.swing.JPanel;

public class MultiPieSeriesNameFormatPaneWithoutCheckBox extends SeriesNameFormatPaneWithoutCheckBox {

    public MultiPieSeriesNameFormatPaneWithoutCheckBox(VanChartStylePane parent, JPanel showOnPane) {
        super(parent, showOnPane);
    }

    protected String getCheckBoxText() {
        return Toolkit.i18nText("Fine-Design_Chart_MultiPie_Series_Name");
    }
}
