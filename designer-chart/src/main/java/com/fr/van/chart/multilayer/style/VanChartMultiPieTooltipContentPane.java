package com.fr.van.chart.multilayer.style;

import com.fr.design.i18n.Toolkit;
import com.fr.design.ui.ModernUIPane;
import com.fr.plugin.chart.base.AttrTooltipContent;
import com.fr.plugin.chart.base.format.AttrTooltipFormat;
import com.fr.plugin.chart.base.format.AttrTooltipPercentFormat;
import com.fr.plugin.chart.base.format.AttrTooltipSeriesFormat;
import com.fr.plugin.chart.base.format.AttrTooltipValueFormat;
import com.fr.plugin.chart.multilayer.style.AttrTooltipMultiLevelNameFormat;
import com.fr.van.chart.designer.component.VanChartTooltipContentPane;
import com.fr.van.chart.designer.component.format.PercentFormatPaneWithCheckBox;
import com.fr.van.chart.designer.component.format.ValueFormatPaneWithCheckBox;
import com.fr.van.chart.designer.component.richText.VanChartFieldAttrPane;
import com.fr.van.chart.designer.component.richText.VanChartFieldListPane;
import com.fr.van.chart.designer.component.richText.VanChartRichEditorModel;
import com.fr.van.chart.designer.component.richText.VanChartRichTextPane;
import com.fr.van.chart.designer.style.VanChartStylePane;

import javax.swing.JPanel;

/**
 * Created by Fangjie on 2016/6/15.
 */
public class VanChartMultiPieTooltipContentPane extends VanChartTooltipContentPane {
    public VanChartMultiPieTooltipContentPane(VanChartStylePane parent, JPanel showOnPane) {
        super(parent, showOnPane);
    }

    @Override
    protected void initFormatPane(VanChartStylePane parent, JPanel showOnPane) {
        setCategoryNameFormatPane(new MultiPieLevelNameFormatPaneWithCheckBox(parent, showOnPane));
        setSeriesNameFormatPane(new MultiPieSeriesNameFormatPaneWithCheckBox(parent, showOnPane));
        setValueFormatPane(new ValueFormatPaneWithCheckBox(parent, showOnPane));
        setPercentFormatPane(new PercentFormatPaneWithCheckBox(parent, showOnPane));
    }

    protected VanChartRichTextPane createRichTextPane(ModernUIPane<VanChartRichEditorModel> richEditorPane) {
        return new VanChartRichTextPane(richEditorPane) {

            protected VanChartFieldListPane createFieldListPane(VanChartFieldAttrPane fieldAttrPane, ModernUIPane<VanChartRichEditorModel> richEditor) {
                return new VanChartMultiPieRichTextFieldListPane(fieldAttrPane, richEditor);
            }

            protected AttrTooltipContent getInitialTooltipContent() {
                return createAttrTooltip();
            }
        };
    }

    protected String[] getRichTextFieldNames() {
        return new String[]{
                Toolkit.i18nText("Fine-Design_Chart_Level_Name"),
                Toolkit.i18nText("Fine-Design_Chart_MultiPie_Series_Name"),
                Toolkit.i18nText("Fine-Design_Chart_Use_Value"),
                Toolkit.i18nText("Fine-Design_Chart_Use_Percent")
        };
    }

    protected AttrTooltipFormat[] getRichTextFieldFormats() {
        return new AttrTooltipFormat[]{
                new AttrTooltipMultiLevelNameFormat(),
                new AttrTooltipSeriesFormat(),
                new AttrTooltipValueFormat(),
                new AttrTooltipPercentFormat()
        };
    }

    @Override
    protected AttrTooltipContent createAttrTooltip() {
        AttrTooltipContent content = new AttrTooltipContent();
        content.setCategoryFormat(new AttrTooltipMultiLevelNameFormat());
        content.setRichTextCategoryFormat(new AttrTooltipMultiLevelNameFormat());
        return content;
    }

}
