package com.fr.van.chart.pie.style;

import com.fr.design.i18n.Toolkit;
import com.fr.design.ui.ModernUIPane;
import com.fr.plugin.chart.base.AttrTooltipContent;
import com.fr.plugin.chart.base.format.AttrTooltipSummaryValueFormat;
import com.fr.plugin.chart.pie.attr.PieCategoryLabelContent;
import com.fr.van.chart.designer.component.richText.VanChartFieldAttrPane;
import com.fr.van.chart.designer.component.richText.VanChartFieldButton;
import com.fr.van.chart.designer.component.richText.VanChartFieldListPane;
import com.fr.van.chart.designer.component.richText.VanChartFieldListener;
import com.fr.van.chart.designer.component.richText.VanChartRichEditorModel;

import javax.swing.JPanel;
import java.util.ArrayList;
import java.util.List;

public class VanChartPieCategoryRichTextFieldListPane extends VanChartFieldListPane {

    private VanChartFieldButton summaryValueButton;

    public VanChartPieCategoryRichTextFieldListPane(VanChartFieldAttrPane fieldAttrPane, ModernUIPane<VanChartRichEditorModel> richEditorPane) {
        super(fieldAttrPane, richEditorPane);
    }

    protected void initDefaultFieldButton() {
        super.initDefaultFieldButton();

        VanChartFieldListener listener = getFieldListener();

        summaryValueButton = new VanChartFieldButton(Toolkit.i18nText("Fine-Design_Chart_Use_Summary_Value"),
                new AttrTooltipSummaryValueFormat(), false, listener);
    }

    protected boolean supportAddField() {
        return false;
    }

    protected void addDefaultFieldButton(JPanel fieldPane) {
        fieldPane.add(getCategoryNameButton());
        fieldPane.add(summaryValueButton);
    }

    protected List<VanChartFieldButton> getDefaultFieldButtonList() {
        List<VanChartFieldButton> fieldButtonList = new ArrayList<>();

        fieldButtonList.add(getCategoryNameButton());
        fieldButtonList.add(summaryValueButton);

        return fieldButtonList;
    }

    public void populateDefaultField(AttrTooltipContent tooltipContent) {
        super.populateDefaultField(tooltipContent);

        if (tooltipContent instanceof PieCategoryLabelContent) {
            PieCategoryLabelContent pieCategory = (PieCategoryLabelContent) tooltipContent;
            populateButtonFormat(summaryValueButton, pieCategory.getRichTextSummaryValueFormat());
        }
    }

    public void updateDefaultField(AttrTooltipContent tooltipContent) {
        super.updateDefaultField(tooltipContent);

        if (tooltipContent instanceof PieCategoryLabelContent) {
            PieCategoryLabelContent pieCategory = (PieCategoryLabelContent) tooltipContent;
            updateButtonFormat(summaryValueButton, pieCategory.getRichTextSummaryValueFormat());
        }
    }
}
