package com.fr.van.chart.range.component;

import com.fr.design.layout.TableLayout;
import com.fr.design.layout.TableLayoutHelper;
import com.fr.plugin.chart.range.GradualLegend;

import javax.swing.JPanel;
import java.awt.BorderLayout;
import java.awt.Component;

public class GradualLegendPane extends JPanel {
    private static final long serialVersionUID = 1614283200308877353L;

    private GradualIntervalConfigPane intervalConfigPane;


    public GradualLegendPane() {
        initComponents();
    }

    private void initComponents() {
        intervalConfigPane = createGradualIntervalConfigPane();

        double p = TableLayout.PREFERRED;
        double f = TableLayout.FILL;
        double[] col = {f};
        double[] row = {p};
        Component[][] components = new Component[][]{
                new Component[]{intervalConfigPane},
        };

        JPanel panel = TableLayoutHelper.createTableLayoutPane(components, row, col);
        this.setLayout(new BorderLayout());
        this.add(panel, BorderLayout.CENTER);
    }

    protected GradualIntervalConfigPane createGradualIntervalConfigPane() {
        return new GradualIntervalConfigPane();
    }

    public void populate(GradualLegend gradualLegend) {
        if (intervalConfigPane != null) {
            intervalConfigPane.populate(gradualLegend.getGradualIntervalConfig());
        }
    }

    public void update(GradualLegend gradualLegend) {
        if (intervalConfigPane != null) {
            intervalConfigPane.update(gradualLegend.getGradualIntervalConfig());
        }
    }
}