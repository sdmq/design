package com.fr.van.chart.scatter.large;

import com.fr.chart.chartglyph.Marker;
import com.fr.design.condition.ConditionAttributesPane;
import com.fr.plugin.chart.marker.type.MarkerType;
import com.fr.van.chart.designer.component.marker.VanChartCommonMarkerPane;
import com.fr.van.chart.designer.other.condition.item.VanChartMarkerConditionPane;

public class VanChartScatterLargeModelMarkerConditionPane extends VanChartMarkerConditionPane {

    public VanChartScatterLargeModelMarkerConditionPane(ConditionAttributesPane conditionAttributesPane) {
        super(conditionAttributesPane);
    }

    protected void initMarkerPane() {

        markerPane = new VanChartLargeModelMarkerPane() {
            protected VanChartCommonMarkerPane createCommonMarkerPane() {

                return new VanChartCommonMarkerPane() {
                    protected Marker[] getMarkers() {
                        return getNormalMarkersWithCustom(new MarkerType[]{MarkerType.MARKER_AUTO});
                    }
                };
            }

        };
    }
}
