!(function () {

    /**
     * HTML解析类，使用方法：
     * new HTMLParser().parse(htmlText, {
     *      startTag: function (tagName, attrs) {},
     *      endTag: function (tagName) {},
     *      text: function (text) {}
     * })
     */
    function HTMLParser () {

    }

    HTMLParser.prototype = {
        constructor: HTMLParser,

        startTagPattern: /<([-\w\d_]+)\s*([^>]*)\s*(\/?)>/,
        endTagPattern: /^<\/([-\w\d_]+)[^>]*>/,
        attrPattern: /([-\w\d_]+)\s*=\s*["']([^"']*)["']/g,
        // 特殊的字符
        specialCharsPatterns: [{
            pattern: /&nbsp;/g,
            subStr: " "
        }, {
            pattern: /&lt;/g,
            subStr: "<"
        }, {
            pattern: /&gt;/g,
            subStr: ">"
        }, {
            pattern: /&amp;/g,
            subStr: "&"
        }],

        handle: {
            // 以下为处理开始标签和文本的时候触发的函数，可以自定义
            startTag: function () {
            },
            endTag: function () {
            },
            text: function () {
            }
        },

        /**
         *
         * @param html
         * @param handle
         * @param isReplaceSpecialChars 是否需要替换HTML中的特殊字符
         */
        parse: function (html, handle, isReplaceSpecialChars) {
            var index, isText = false,
                match, last = html,
                me = this;

            isReplaceSpecialChars = isReplaceSpecialChars !== null ? isReplaceSpecialChars : true;

            if (BI.isPlainObject(handle)) {
                BI.isFunction(handle.startTag) && (this.handle.startTag = handle.startTag);
                BI.isFunction(handle.endTag) && (this.handle.endTag = handle.endTag);
                BI.isFunction(handle.text) && (this.handle.text = handle.text);
            }

            while (html) {
                isText = true;

                // 结束标签
                if (html.indexOf("</") === 0) {
                    match = html.match(this.endTagPattern);

                    if (match) {
                        html = html.substring(match[0].length);
                        match[0].replace(this.endTagPattern, function () {
                            me._parseEndTag.apply(me, arguments);
                        });
                        isText = false;
                    }
                } else if (html.indexOf("<") === 0) {
                    // 遇到起始的标签，提取标签名称和属性之类的
                    match = html.match(this.startTagPattern);

                    if (match) {
                        html = html.substring(match[0].length);
                        match[0].replace(this.startTagPattern, function () {
                            me._parseStartTag.apply(me, arguments);
                        });
                        isText = false;
                    }
                }

                // 仅仅是一行文本的情况
                if (isText) {
                    index = html.indexOf("<");

                    var text = index < 0 ? html : html.substring(0, index);
                    html = index < 0 ? "" : html.substring(index);

                    if (isReplaceSpecialChars) {
                        BI.each(this.specialCharsPatterns, function (idx, obj) {
                            text = text.replace(obj.pattern, obj.subStr);
                        });
                    }

                    this.handle.text(text);
                }

                if (html === last) {
                    throw "Parse Error: " + html;
                }

                last = html;
            }

            this._parseEndTag();
        },

        /**
         * 解析开始标签的字符串
         * @param all 整个需要解析的字符串
         * @param tagName
         * @param rest 除去tagNAme之后allString剩下的部分
         */
        _parseStartTag: function (all, tagName, rest) {
            tagName = tagName.toLowerCase();

            var attrs = [];

            rest.replace(this.attrPattern, function (all, attrName, value) {
                attrs.push({
                    name: attrName,
                    value: value,
                    escaped: value.replace(/(^|[^\\])"/g, "$1\\\"")
                });
            });

            tagName && this.handle.startTag(tagName, attrs);
        },

        /**
         * 解析关闭的标签
         */
        _parseEndTag: function (all, tagName) {
            tagName && this.handle.endTag(tagName);
        }
    };

    // 获取一个HTML字符串中image的属性
    function getImageAttr (html) {
        var htmlParse = new HTMLParser();
        var imageAttr;

        htmlParse.parse(html, {
            startTag: function (tagName, attrs) {
                if (tagName === "img") {
                    imageAttr = {};
                    BI.each(attrs, function (idx, attr) {
                        imageAttr[attr.name] = attr.value;
                    });
                }
            }
        });

        return imageAttr;
    }

    var Service = BI.inherit(BI.OB, {

        encode: function (params) {
            if (/[<>'$%{}"]/.test(params)) {
                return window.encodeURIComponent(params).replace(/'/g, escape);
            }
            return params;
        },

        decode: function (params) {
            try {
                var result = window.decodeURIComponent(params);
                if (/[<>'$%{}"]/.test(result)) {
                    return result;
                }
                return params;
            } catch (e) {
                return params;
            }
        },

        HTMLParser: HTMLParser,

        getImageAttr: getImageAttr,

        /**
         * 构造一个富文本中的参数图片
         */
        getEditorParamImage: function (param, fillStyle) {
            var attr = BI.DOM.getImage(param, fillStyle);
            return "<img class='rich-editor-param' height='16px'"
                + " alt='" + this.encode(attr.param)
                + "' name='" + this.encode(attr.param)
                + "' style='" + attr.style + "' />";
        },

        /**
         * 给image设置属性
         */
        appendImageAttr: function (html, altValue, attrName, attrValue, isCovered) {
            altValue = this.encode(altValue);
            attrName = this.encode(attrName);
            attrValue = this.encode(attrValue);
            isCovered = isCovered || false; // 属性是否可以覆盖

            return html.replaceAll("<img(.*?)>", function (imageStr) {
                var attrs = getImageAttr(imageStr);

                if (attrs && attrs["alt"] === altValue && (BI.isNull(attrs[attrName]) || isCovered)) {
                    attrs[attrName] = attrValue;
                    imageStr = "<img ";

                    BI.each(attrs, function (attrName, attrValue) {
                        imageStr += "" + attrName + "=\"" + attrValue + "\" ";
                    });

                    imageStr += "/>";
                }
                return imageStr;
            });
        },

        /**
         * 将普通的文本转化成富文本字符串
         */
        convertText2RichText: function (text, fontStyle, fontProcessFn) {
            fontStyle = fontStyle || {};

            if (BI.isEmptyString(text)) {
                return text;
            }

            var lines = text.split("\n"),
                pattern = "\\$\\{(.*?)\\}",
                richText = "",
                self = this,
                html = self._getHtmlTagStr(fontStyle, fontProcessFn);

            BI.each(lines, function (idx, lineText) {
                // 替换参数为image
                var newText = lineText.replaceAll(pattern, function (all, $1) {
                    return self.getEditorParamImage($1);
                });
                // 空字符串
                if (!lineText) {
                    newText += "<br />";
                }
                richText += html.startHtml + newText + html.endHtml;
            });

            return richText;
        },

        /**
         * 根据fontStyle构造标签
         */
        _getHtmlTagStr: function (fontStyle, fontProcessFn) {
            var tags = [{
                    tag: "p",
                    style: {}
                }],
                obj,
                startHtml = "",
                endHtml = "";

            // fontSize 为number值
            if (fontStyle.fontSize) {
                obj = {
                    tag: "font",
                    style: {"font-size": fontStyle.fontSize + "px"}
                };
                tags.push(obj);
            }

            // 字体一般用默认值，不需要处理，但是KPI指标卡例外
            if (BI.isFunction(fontProcessFn)) {
                tags.push(fontProcessFn());
            }

            // fontBold 为boolean值
            if (fontStyle.fontBold) {
                obj = {
                    tag: "b",
                    style: {}
                };
                tags.push(obj);
            }

            // italic 为boolean值
            if (fontStyle.italic) {
                obj = {
                    tag: "i",
                    style: {}
                };
                tags.push(obj);
            }

            // underline 为boolean值
            if (fontStyle.underline) {
                obj = {
                    tag: "u",
                    style: {}
                };
                tags.push(obj);
            }


            BI.each(tags, function (idx, item) {
                startHtml += "<" + item.tag;

                // set style
                if (BI.isNotEmptyObject(item.style)) {
                    startHtml += " style=\"";
                    BI.each(item.style, function (key, value) {
                        startHtml += key + ":" + value + ";";
                    });
                    startHtml += "\"";
                }

                // set attributes
                BI.each(item.attr, function (key, value) {
                    startHtml += " " + key + "=\"" + value + "\"";
                });
                startHtml += ">";
            });

            BI.each(tags.reverse(), function (idx, item) {
                endHtml += "</" + item.tag + ">";
            });

            return {
                startHtml: startHtml,
                endHtml: endHtml
            };
        },

        /**
         * 将富文本HTML转化成普通的文本，不需要记录fontStyle
         */
        convertRichText2Text: function (html) {
            var content = "",
                line = "",
                htmlFragments = [];

            new HTMLParser().parse(html, {
                startTag: function (tagName, attrs) {
                    // 只关心br、img
                    switch (tagName) {
                        case "br":
                            line += "\n";
                            htmlFragments.push(tagName);
                            break;
                        case "img":
                            BI.each(attrs, function (idx, obj) {
                                if (obj.name === "alt") {
                                    line += "${" + obj.value + "}";
                                }
                            });
                            break;
                        default:
                            break;
                    }
                },
                endTag: function (tagName) {
                    if (tagName === "div") {
                        // <div><br /></div>这样的情况下应该只代表一个回车符
                        htmlFragments[htmlFragments.length - 1] !== "br" && (line += "\n");
                        htmlFragments = [];
                    }

                    content += line;
                    line = "";
                },
                text: function (text) {
                    htmlFragments.push(text);
                    line += text;
                }
            });

            // 去掉最后的换行符
            content = content.replace(/\n$/, "");

            return content;
        },

        convertImage2Text: function (html) {
            if (BI.isNull(html)) {
                return html;
            }
            return html.replaceAll("<img.*?>", function (imageStr) {
                var matched = imageStr.match(/alt="(.*?)"/);
                var startHtml = "<span class=\"rich-editor-param\" style=\"background-color: #EAF2FD;\">";
                var endHtml = "</span>";
                return startHtml + (matched ? matched[1] : imageStr) + endHtml;
            });
        },

        isRichTextEqual: function (html1, html2) {
            if (BI.isNotNull(html1)) {
                html1 = this.convertImage2Text(html1);
            }
            if (BI.isNotNull(html2)) {
                html2 = this.convertImage2Text(html2);
            }

            return html1 === html2;
        },

        isRichTextNotEqual: function (html1, html2) {
            return !this.isRichTextEqual(html1, html2);
        },

        isBlankRichText: function (html) {
            html = html || "";
            html = this.convertRichText2Text(html);
            return BI.trim(html).length === 0;
        },

        // 显示整个富文本中缺失的字段
        showMissingFields: function (html, validDimensionIds) {
            var self = this;

            return html.replaceAll("<img.*?>", function (imageStr) {
                var attrs = getImageAttr(imageStr);
                return attrs && BI.contains(validDimensionIds, attrs[BICst.RICH_TEXT_INFO.DATA_ID])
                    ? imageStr
                    : self.getMissingFieldImage(
                        attrs[BICst.RICH_TEXT_INFO.DATA_ID],
                        attrs[BICst.RICH_TEXT_INFO.DATA_NAME],
                        attrs[BICst.RICH_TEXT_INFO.ALT]
                    );
            });
        },

        // 获取缺失元素的图片
        getMissingFieldImage: function (dId, name, fullName) {
            var missingText = "<!" + BI.i18nText("BI-Design_Missing_Field") + "!>";
            var missingImage = BI.DOM.getImage(missingText, "#ff0000");
            var html = this.getEditorParamImage(fullName, "#ff0000");

            // 将原来的dId、name、fullName保存起来
            html = this.appendImageAttr(html, fullName, BICst.RICH_TEXT_INFO.DATA_ORIGIN_ID, dId);
            html = this.appendImageAttr(html, fullName, BICst.RICH_TEXT_INFO.DATA_ORIGIN_NAME, name);
            html = this.appendImageAttr(html, fullName, BICst.RICH_TEXT_INFO.DATA_ORIGIN_FULL_NAME, fullName);
            html = this.appendImageAttr(html, fullName, BICst.RICH_TEXT_INFO.SRC, missingImage.src, true);
            // 标记出这个参数是一个“缺失元素”
            html = this.appendImageAttr(html, fullName, BICst.RICH_TEXT_INFO.DATA_IS_MISSING_FIELD, "true", true);
            html = this.appendImageAttr(html, fullName, "style", missingImage.style, true);
            return html;
        },

        // 将“缺失元素”替换成原来的字段
        restoreMissingField: function (html) {
            var self = this;

            return html.replaceAll("<img.*?>", function (imageStr) {
                var attrs = getImageAttr(imageStr);

                if (attrs && attrs[BICst.RICH_TEXT_INFO.DATA_ORIGIN_ID]) {
                    var fullName = attrs[BICst.RICH_TEXT_INFO.DATA_ORIGIN_FULL_NAME];
                    var image = self.getEditorParamImage(fullName);

                    image = self.appendImageAttr(image, fullName, BICst.RICH_TEXT_INFO.DATA_ID, attrs[BICst.RICH_TEXT_INFO.DATA_ORIGIN_ID]);
                    image = self.appendImageAttr(image, fullName, BICst.RICH_TEXT_INFO.DATA_NAME, attrs[BICst.RICH_TEXT_INFO.DATA_ORIGIN_NAME]);
                    return image;
                }

                if (attrs
                    && attrs[BICst.RICH_TEXT_INFO.DATA_IS_INSERT_PARAM]
                    && !attrs[BICst.RICH_TEXT_INFO.DATA_ID]
                ) {
                    imageStr = self.appendImageAttr(
                        imageStr,
                        attrs[BICst.RICH_TEXT_INFO.ALT],
                        BICst.RICH_TEXT_INFO.DATA_UN_VALID,
                        "true",
                        true
                    );
                }

                return imageStr;
            });
        },

        // 将富文本字符串中由用户主动插入的字段参数标记出来
        markTheInsertParamImages: function (html) {
            return html.replaceAll("<img.*?>", function (imageStr) {
                var attrs = getImageAttr(imageStr);

                // "缺失元素"和普通的字段参数直接过滤(从富文本编辑器中插入的参数只有data-id属性)
                if (BI.has(attrs, BICst.RICH_TEXT_INFO.DATA_NAME)
                    || BI.has(attrs, BICst.RICH_TEXT_INFO.DATA_ORIGIN_ID)) {
                    return imageStr;
                }

                attrs[BICst.RICH_TEXT_INFO.DATA_IS_INSERT_PARAM] = "true";
                imageStr = "<img ";

                BI.each(attrs, function (key, value) {
                    imageStr += key + "=\"" + value + "\" ";
                });

                imageStr += "/>";
                return imageStr;
            });
        },

        // editor在getValue时因为src太大去掉了，在展示之前需要set 一下src
        setImageSrc: function (content) {
            var self = this;

            return content && content.replaceAll("<img.*?>", function (imageStr) {
                var attrs = self.getImageAttr(imageStr);
                // 已有src
                if (BI.has(attrs, BICst.RICH_TEXT_INFO.SRC) && attrs[BICst.RICH_TEXT_INFO.SRC]) {
                    return imageStr;
                }
                var alt = self.decode(attrs[BICst.RICH_TEXT_INFO.ALT]);
                var image = BI.DOM.getImage(alt);
                imageStr = self.appendImageAttr(imageStr, alt, BICst.RICH_TEXT_INFO.SRC, image.src, true);
                imageStr = self.appendImageAttr(imageStr, alt, "style", image.style, true);
                return imageStr;
            });
        },

        isEmptyRichText: function (html) {
            return "" === html.replaceAll("<p.*?>|</p>|<br/?>", "");
        },

        // BI-23091 设置显示名之后更新参数名称
        getNewParamNameRichText: function (content) {
            if (!content) {
                return content;
            }
            var self = this;
            return content.replaceAll("<img.*?>", function (imageStr) {
                var attrs = self.getImageAttr(imageStr);
                // 缺失元素
                if (attrs[BICst.RICH_TEXT_INFO.DATA_IS_MISSING_FIELD]) {
                    return imageStr;
                }
                var alt = attrs[BICst.RICH_TEXT_INFO.ALT],
                    dId = attrs[BICst.RICH_TEXT_INFO.DATA_ID],
                    name = BI.Utils.getNotGeoDimensionFullName(dId);

                if (name !== alt) {
                    imageStr = self.appendImageAttr(imageStr, alt, BICst.RICH_TEXT_INFO.ALT, name, true);
                }
                return imageStr;
            });
        }
    });

    BI.service("bi.service.design.chart.common.editor", Service);

}());
