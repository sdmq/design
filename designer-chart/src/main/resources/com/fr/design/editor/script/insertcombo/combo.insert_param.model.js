/*
 * @Maintainers: xiaofu.qin
 */
!(function () {

    var Model = BI.inherit(Fix.Model, {
        context: ["dimensionIds"],

        state: function () {
            return {
                param: "",
                isSelectedParam: false
            };
        },

        computed: {
            items: function () {
                return BI.map(this.model.dimensionIds, function (idx, dId) {
                    var key = BI.keys(dId)[0];

                    return {
                        type: "bi.design.chart.common.editor.search",
                        text: key,
                        value: key
                    };
                });
            }
        },

        actions: {
            changeParam: function (param) {
                this.model.isSelectedParam = !this.model.isSelectedParam;
                this.model.param = param;
            }
        }
    });

    BI.model("bi.model.design.chart.common.editor.insert_param", Model);

} ());
