!(function () {

    var Widget = BI.inherit(BI.BasicButton, {
        props: {
            text: "",
            iconCls: "",
            extraCls: "bi-list-item-active"
        },

        render: function () {
            var o = this.options;
            return {
                type: "bi.vertical_adapt",
                height: 25,
                items: [{
                    type: "bi.center_adapt",
                    cls: o.iconCls,
                    items: [{
                        type: "bi.icon",
                        width: 16,
                        height: 16
                    }],
                    width: 16,
                    height: 16
                }, {
                    type: "bi.label",
                    textAlign: "left",
                    lgap: 5,
                    text: o.text,
                    value: o.text
                }]
            };
        }
    });

    BI.shortcut("bi.design.chart.common.editor.search", Widget);

}());