/**
 * 灰化toolbar的item
 */
!(function () {

    var Widget = BI.inherit(BI.Single, {
        props: {
            button: null
        },

        render: function () {
            return {
                type: "bi.vertical_adapt",
                disabled: true,
                items: [this.options.button]
            };
        }
    });

    BI.shortcut("bi.design.chart.common.editor.toolbar.disabled_item_wrapper", Widget);

}());