package com.fr.design.designer.beans.actions.behavior;

import com.fr.design.designer.beans.actions.FormWidgetEditAction;
import com.fr.design.mainframe.FormDesigner;

public class CutableEnable implements UpdateBehavior<FormWidgetEditAction> {
    @Override
    public void doUpdate(FormWidgetEditAction action) {
        FormDesigner designer = action.getEditingComponent();
        if (designer == null) {
            action.setEnabled(false);
            return;
        }
        action.setEnabled(designer.isCurrentComponentCutable());
    }
}
