package com.fr.design.designer.beans.adapters.layout;

import com.fr.design.designer.beans.models.AddingModel;
import com.fr.design.designer.creator.XCreator;
import com.fr.design.designer.creator.XLayoutContainer;
import com.fr.design.designer.creator.cardlayout.XWCardTagLayout;
import com.fr.design.dialog.FineJOptionPane;
import com.fr.design.file.HistoryTemplateListPane;
import com.fr.design.form.layout.FRBorderLayout;
import com.fr.design.mainframe.FormDesigner;
import com.fr.design.mainframe.JTemplate;
import com.fr.design.mainframe.WidgetPropertyPane;
import com.fr.general.ComparatorUtils;

import java.awt.BorderLayout;
import java.awt.Rectangle;

/**
 * cardMainBorderLayout适配器
 *
 * @author kerry
 * @date 2019/1/4
 */
public class FRCardMainBorderLayoutAdapter extends FRBorderLayoutAdapter {

    public FRCardMainBorderLayoutAdapter(XLayoutContainer container) {
        super(container);
    }

    /**
     * CardMainBorderLayout的title部分不能超出layout边界
     *
     * @param creator 组件
     */
    @Override
    public void fix(XCreator creator) {
        if (creator.acceptType(XWCardTagLayout.class)) {
            creator = (XCreator) creator.getParent();
        }
        boolean beyondBounds = calculateBeyondBounds(creator);
        if (!beyondBounds) {
            super.fix(creator);
        }
    }

    private boolean calculateBeyondBounds(XCreator creator) {
        FRBorderLayout layout = (FRBorderLayout) container.getFRLayout();
        Object constraints = layout.getConstraints(creator);
        Rectangle rectangle = creator.getBounds();
        //不能超出控件边界
        if (ComparatorUtils.equals(constraints, BorderLayout.NORTH) || ComparatorUtils.equals(constraints, BorderLayout.SOUTH)) {
            return isBeyondMinConstraint(rectangle.height, container.getHeight());
        } else if (ComparatorUtils.equals(constraints, BorderLayout.EAST) || ComparatorUtils.equals(constraints, BorderLayout.WEST)) {
            return isBeyondMinConstraint(rectangle.width, container.getWidth());
        }
        return false;
    }

    /**
     * 是否超出最小限制
     *
     * @param minConstraint 最小限制
     * @param value 数值
     * @return 是否超出最小限制
     */
    private boolean isBeyondMinConstraint(int minConstraint, int value) {
        if (minConstraint > value) {
            FineJOptionPane.showMessageDialog(null, com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Form_Beyond_Tablayout_Bounds"));
            JTemplate<?, ?> jt = HistoryTemplateListPane.getInstance().getCurrentEditingTemplate();
            if (jt != null) {
                jt.undoToCurrent();
            }

            FormDesigner formDesigner = WidgetPropertyPane.getInstance().getEditingFormDesigner();
            AddingModel model = formDesigner.getAddingModel();
            model.setAddedIllegal(true);
            return true;
        } else {
            return false;
        }
    }
}
