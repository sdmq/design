package com.fr.design.designer.beans.location;

import java.awt.Cursor;
import java.awt.Rectangle;

import com.fr.design.mainframe.FormDesigner;

public class Bottom extends AccessDirection {

    public Bottom() {
    }

    @Override
	public Rectangle getDraggedBounds(int dx, int dy, Rectangle current_bounds, FormDesigner designer,
			Rectangle oldbounds) {
		current_bounds.height = sorption(0, oldbounds.height + dy + oldbounds.y, current_bounds, designer)[1]
				- oldbounds.y;

        if (designer.getStateModel().isAspectRatioLocked()) {
            Rectangle backupBounds = designer.getSelectionModel().getSelection().getBackupBounds();
            current_bounds.width = (int) (backupBounds.width * current_bounds.height / backupBounds.height * 1.0);
        }

		return current_bounds;
	}

    @Override
    public int getCursor() {
        return Cursor.S_RESIZE_CURSOR;
    }

    @Override
    public String getTooltip() {
        return com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Form_Widget_Lock_Aspect_Ratio_Mouse_ToolTip");
    }

    @Override
    public int getActual() {
        return Direction.BOTTOM;
    }
}