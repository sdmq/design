package com.fr.design.designer.beans.location;

import java.awt.Cursor;
import java.awt.Rectangle;

import com.fr.design.designer.creator.XCreator;
import com.fr.design.mainframe.FormDesigner;
import com.fr.form.ui.Widget;

public class Right extends AccessDirection  {

    public Right() {
    }

	@Override
	public Rectangle getDraggedBounds(int dx, int dy, Rectangle current_bounds, FormDesigner designer,
			Rectangle oldbounds) {
		current_bounds.width = sorption(oldbounds.x + dx + oldbounds.width, 0, current_bounds, designer)[0]
				- oldbounds.x;

        if (designer.getStateModel().isAspectRatioLocked()) {
            Rectangle backupBounds = designer.getSelectionModel().getSelection().getBackupBounds();
            current_bounds.height = (int) (backupBounds.height * current_bounds.width / backupBounds.width * 1.0);
        }

		return current_bounds;
	}

    @Override
    public int getCursor() {
        return Cursor.E_RESIZE_CURSOR;
    }

    @Override
    public String getTooltip() {
        return com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Form_Widget_Lock_Aspect_Ratio_Mouse_ToolTip");
    }

     @Override
    public int getActual() {
        return Direction.RIGHT;
    }
}