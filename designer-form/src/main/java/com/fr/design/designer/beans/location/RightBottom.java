package com.fr.design.designer.beans.location;

import java.awt.Cursor;
import java.awt.Rectangle;

import com.fr.design.mainframe.FormDesigner;

public class RightBottom extends AccessDirection {

    public RightBottom() {
    }

    @Override
    public Rectangle getDraggedBounds(int dx, int dy, Rectangle current_bounds, FormDesigner designer,
			Rectangle oldbounds) {
		int[] xy = sorption(oldbounds.x + dx + oldbounds.width, oldbounds.height + dy + oldbounds.y, current_bounds, designer);
        current_bounds.width = xy[0] - oldbounds.x;
        current_bounds.height = xy[1] - oldbounds.y;

        if (designer.getStateModel().isAspectRatioLocked()) {
            Rectangle backupBounds = designer.getSelectionModel().getSelection().getBackupBounds();
            double current_diagonal = Math.pow(current_bounds.width, 2) + Math.pow(current_bounds.height, 2);
            double backup_diagonal = Math.pow(backupBounds.width, 2) + Math.pow(backupBounds.height, 2);

            int width = (int) (Math.sqrt((current_diagonal / backup_diagonal) * (Math.pow(backupBounds.width, 2))));
            int height = (int) (Math.sqrt((current_diagonal / backup_diagonal) * (Math.pow(backupBounds.height, 2))));

            current_bounds.width = width;
            current_bounds.height = height;
        }

        return current_bounds;
    }

    @Override
    public int getCursor() {
        return Cursor.SE_RESIZE_CURSOR;
    }

    @Override
    public String getTooltip() {
        return com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Form_Widget_Lock_Aspect_Ratio_Mouse_ToolTip");
    }

     @Override
    public int getActual() {
        return Direction.RIGHT_BOTTOM;
    }
}