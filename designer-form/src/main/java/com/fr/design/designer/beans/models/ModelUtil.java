package com.fr.design.designer.beans.models;

import com.fr.design.designer.creator.XCreator;
import com.fr.design.designer.creator.XWParameterLayout;
import com.fr.design.mod.bean.ChangeItem;
import com.fr.design.mod.bean.ContentChangeItem;
import com.fr.design.mod.event.WidgetNameModifyEvent;
import com.fr.event.EventDispatcher;
import com.fr.form.main.Form;
import com.fr.form.ui.Widget;
import com.fr.stable.StringUtils;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * created by Harrison on 2020/06/05
 **/
public abstract class ModelUtil {
    
    public static void renameWidgetName(Form form, XCreator xCreator) {
        
        Set<String> duplicated = new HashSet<>();
        recursiveRenameWidgetName(form, xCreator, duplicated);
    }
    
    private static void recursiveRenameWidgetName(Form form, XCreator xCreator, Set<String> duplicated) {
    
        Set<XCreator> nameRelatedCreators = new HashSet<>();
        //直接遍历出来目标值，然后按需处理
        xCreator.traversalNameRelatedXCreators(nameRelatedCreators);
        Map<String, String> renameMap = new HashMap<>();
        // 避免与旧名称重复
        initDuplicated(nameRelatedCreators, duplicated);
        for (XCreator target : nameRelatedCreators) {
            String uniqueName = uniqueName(form, target, duplicated);
            String oldName = target.toData().getWidgetName();
            if (StringUtils.isNotEmpty(oldName)) {
                renameMap.put(oldName, uniqueName);
            }
            target.resetCreatorName(uniqueName);
        }
        EventDispatcher.fire(WidgetNameModifyEvent.INSTANCE, new ContentChangeItem(renameMap, xCreator.toData(), ChangeItem.WIDGET_NAME));
    }

    private static void initDuplicated(Set<XCreator> nameRelatedCreators, Set<String> duplicated) {
        for (XCreator xCreator : nameRelatedCreators) {
            duplicated.add(xCreator.toData().getWidgetName());
        }
    }
    
    private static String uniqueName(Form form, XCreator xCreator, Set<String> duplicated) {
        
        if (xCreator.acceptType(XWParameterLayout.class)) {
            return xCreator.createDefaultName();
        }
        Widget widget = xCreator.toData();
        String widgetName = widget.getWidgetName();
        if (StringUtils.isEmpty(widgetName)) {
            widgetName = xCreator.createDefaultName();
        }
        //先保存默认名字
        String raw = widgetName;
        int i = 0;
        //先初始化加上索引。
        widgetName = widgetName + i;
        while (form.isNameExist(widgetName) || duplicated.contains(widgetName)) {
            i++;
            widgetName = raw + i;
        }
        //将名字加入重复集合中
        duplicated.add(widgetName);
        return widgetName;
    }
    
}
