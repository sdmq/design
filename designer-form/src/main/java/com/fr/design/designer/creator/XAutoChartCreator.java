package com.fr.design.designer.creator;

import com.fr.base.GraphHelper;
import com.fr.base.chart.BaseChartCollection;
import com.fr.common.annotations.Open;
import com.fr.design.dialog.DialogActionAdapter;
import com.fr.design.gui.chart.MiddleChartComponent;
import com.fr.design.gui.chart.MiddleChartDialog;
import com.fr.design.mainframe.BaseJForm;
import com.fr.design.mainframe.DesignerContext;
import com.fr.design.mainframe.EditingMouseListener;
import com.fr.design.mainframe.FormDesigner;
import com.fr.design.module.DesignModuleFactory;
import com.fr.form.ui.ChartAutoEditor;
import com.fr.general.IOUtils;
import com.fr.stable.Constants;
import com.fr.stable.bridge.StableFactory;

import javax.swing.JComponent;
import javax.swing.JPanel;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;

/**
 * @author Bjorn
 * @version 10.0
 * Created by Bjorn on 2020-05-26
 */
@Open
public class XAutoChartCreator extends XChartEditor {

    private MiddleChartDialog autoChartDialog;

    private EditingMouseListener editingMouseListener;

    public XAutoChartCreator(ChartAutoEditor editor, Dimension size) {
        super(editor, size);
    }

    /**
     * 返回组件默认名
     *
     * @return 组件类名(小写)
     */
    public String createDefaultName() {
        return "auto_chart";
    }

    @Override
    public ChartAutoEditor toData() {
        return (ChartAutoEditor) data;
    }


    /**
     * 点击选中的时候, 刷新界面
     * 右键 reset之后, 触发事件 populate此方法
     *
     * @param jform        表单
     * @param formDesigner 表单设计器
     * @return 控件.
     */
    public JComponent createToolPane(final BaseJForm jform, final FormDesigner formDesigner) {
        if (toData().isChartSelect()) {
            return super.createToolPane(jform, formDesigner);
        }
        if (isEditing) {
            if (autoChartDialog != null && autoChartDialog.isVisible()) {
                return new JPanel();
            }
            final BaseChartCollection chartCollection = (BaseChartCollection) StableFactory.createXmlObject(BaseChartCollection.XML_TAG);
            autoChartDialog = DesignModuleFactory.getAutoChartDialog(DesignerContext.getDesignerFrame());
            autoChartDialog.populate(chartCollection);
            autoChartDialog.addDialogActionListener(new DialogActionAdapter() {
                @Override
                public void doOk() {
                    initChart(autoChartDialog.getChartCollection());
                    formDesigner.fireTargetModified();
                }

                @Override
                public void doCancel() {
                    editingMouseListener.stopEditing();
                }
            });
            autoChartDialog.setVisible(true);
        }
        return toData().isChartSelect() ? super.createToolPane(jform, formDesigner) : new JPanel();
    }

    /**
     * 响应点击事件
     *
     * @param editingMouseListener 鼠标点击，位置处理器
     * @param e                    鼠标点击事件
     */
    public void respondClick(EditingMouseListener editingMouseListener, MouseEvent e) {
        this.editingMouseListener = editingMouseListener;
        super.respondClick(editingMouseListener, e);
    }


    @Override
    public void paintForeground(Graphics2D g) {
        BufferedImage bufferedImage = IOUtils.readImage("com/fr/design/form/images/auto_chart_preview.png");
        GraphHelper.paintImage(
                g, this.getWidth(), this.getHeight(), bufferedImage,
                Constants.IMAGE_ADJUST,
                0,
                0, -1, -1
        );
        super.paintForeground(g);
    }

    private void initChart(BaseChartCollection chartCollection) {
        ((MiddleChartComponent) getDesignerEditor().getEditorTarget()).populate(chartCollection);
        this.toData().resetChangeChartCollection(chartCollection);
        this.toData().setChartSelect(true);
    }
}
