/**
 *
 */
package com.fr.design.designer.creator;

import com.fr.design.designer.beans.LayoutAdapter;
import com.fr.design.designer.beans.adapters.layout.FRTitleLayoutAdapter;
import com.fr.design.form.layout.FRTitleLayout;
import com.fr.design.fun.WidgetPropertyUIProvider;
import com.fr.form.ui.Label;
import com.fr.form.ui.Widget;
import com.fr.form.ui.WidgetTitle;
import com.fr.form.ui.container.WAbsoluteLayout.BoundsWidget;
import com.fr.form.ui.container.WTitleLayout;
import com.fr.general.Background;
import com.fr.general.ComparatorUtils;

import java.awt.*;
import java.awt.event.ContainerEvent;
import java.awt.geom.Rectangle2D;

/**
 * 一些控件 如图表、报表块，有标题设置，且标题的高度字体等不变
 * @author jim
 * @date 2014-9-25
 */
public class XWTitleLayout extends DedicateLayoutContainer {

	/**
	 *
	 */
	private static final long serialVersionUID = 5274572473978467325L;

	private static final int INDEX = 0;

	protected Background titleBackground4Painting;	// 设计器预览界面中绘制title背景图
	protected double titleBackgroundOpacity4Painting;
	protected Background bodyBackground4Painting;	// 设计器预览界面中绘制body背景图
	protected double bodyBackgroundOpacity4Painting;

	public XWTitleLayout() {
		super(new WTitleLayout("titlePane"), new Dimension());
	}

	/**
	 * 容器构造函数
	 *
	 * @param widget 控件widget
	 * @param initSize 尺寸大小
	 */
	public XWTitleLayout(WTitleLayout widget, Dimension initSize) {
		super(widget, initSize);
	}

	public void setTitleBackground4Painting(Background background, double opacity) {
		this.titleBackground4Painting = background;
		this.titleBackgroundOpacity4Painting = opacity;
	}

	public void setBodyBackground4Painting(Background background, double opacity) {
		this.bodyBackground4Painting = background;
		this.bodyBackgroundOpacity4Painting = opacity;
	}

	@Override
	public void refreshBorderAndBackgroundStylePreviewEffect() {
		setBorder(null);
		setBorderImage4Painting(null, 0.0);
		setBackground4Painting(null, 0.0);
	}

	@Override
	protected void initXCreatorProperties() {
		super.initXCreatorProperties();

		// 初始化后（如打开旧模版)，需要重新调整组件/标题/边框的显示效果
		XCreator xCreator = getBodyCreator();
		if (xCreator instanceof XBorderStyleWidgetCreator) {
			((XBorderStyleWidgetCreator) xCreator).reshuffleBorderAndBackgroundPaintingEffectIfTitleExists();
		}
	}

	/**
	 * 初始化容器对应的布局 由于是只装一个需要保持原样高度的控件，布局设为absolute
	 */
	@Override
	protected void initLayoutManager() {
		this.setLayout(new FRTitleLayout());
	}

	/**
	 * 容器的渲染器
	 */
	@Override
	public LayoutAdapter getLayoutAdapter() {
		return new FRTitleLayoutAdapter(this);
	}

	public XCreator getEditingChildCreator(){
		return getXCreator(INDEX);
	}

	/**
	 * 返回容器对应的wlayout
	 *
	 * @return 同上
	 */
	@Override
	public WTitleLayout toData() {
		return (WTitleLayout) data;
	}

	/**
	 * 重置组件的名称
	 * @param name 名称
	 */
	public void resetCreatorName(String name) {
		super.resetCreatorName(name);
		// 有标题的话，标题的名字也要重置下
		if (getXCreatorCount() > 1) {
			getTitleCreator().toData().setWidgetName(WidgetTitle.TITLE_NAME_INDEX + name);
		}
	}

	/**
	 * 返回默认组件name
	 *
	 * @return 容器名
	 */
	@Override
	public String createDefaultName() {
		return "titlePanel";
	}

	/**
	 * 返回标题组件
	 * @return 标题组件
	 */
	public XCreator getTitleCreator() {
		for (int i=0; i < getXCreatorCount(); i++) {
			XCreator creator = getXCreator(i);
			if (!creator.hasTitleStyle()) {
				return creator;
			}
		}
		return null;
	}


	/**
	 * 返回body组件
	 * @return body组件
	 */
	public XCreator getBodyCreator() {
		for (int i=0; i < getXCreatorCount(); i++) {
			XCreator creator = getXCreator(i);
			if (creator.hasTitleStyle()) {
				return creator;
			}
		}
		return null;
	}

	/**
	 *  编辑状态的时候需要重新绘制下边框
	 *
	 */
	@Override
	public void paintBorder(Graphics g, Rectangle bounds) {
		XCreator childCreator = getEditingChildCreator();
		if (childCreator != null) {
			childCreator.paintBorder(g, bounds);
		}

	}

	@Override
	public void paintBackground(Graphics2D g2d) {
		if (getComponentCount() > 1) {
			paintTitleBackground(g2d);
			paintBodyBackground(g2d);
		}
	}

	private void paintTitleBackground(Graphics2D g2d) {
		if (titleBackground4Painting != null) {
			Composite oldComposite = g2d.getComposite();
			g2d.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_ATOP, (float) this.titleBackgroundOpacity4Painting));
			XCreator titleCreator = getTitleCreator();
			int titleHeight = titleCreator != null ? titleCreator.getHeight() : 0;

			Shape shape = new Rectangle2D.Double(0, 0, getWidth(), titleHeight);
			titleBackground4Painting.paint(g2d, shape);

			g2d.setComposite(oldComposite);
		}
	}

	private void paintBodyBackground(Graphics2D g2d) {
		if (bodyBackground4Painting != null) {
			Composite oldComposite = g2d.getComposite();
			g2d.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_ATOP, (float) this.bodyBackgroundOpacity4Painting));

			XCreator titleCreator = getTitleCreator();
			int titleHeight = titleCreator != null ? titleCreator.getHeight() : 0;
			// 兼容性考虑，组件样式背景不作用在标题范围内，只作用在控件整体，但同时不能遮挡作用于整体的边框图片
			// 所以考虑样式背景与边框图片都由XWTitleLayout绘制，但样式背景要向下偏移标题栏的高度
			Shape shape = new Rectangle2D.Double(0, titleHeight, getWidth(), getHeight() - titleHeight);
			bodyBackground4Painting.paint(g2d, shape);

			g2d.setComposite(oldComposite);
		}
	}

	/**
	 * 将WLayout转换为XLayoutContainer
	 */
	@Override
	public void convert() {
		isRefreshing = true;
		WTitleLayout layout = this.toData();
		this.removeAll();
		for (int i = 0, num = layout.getWidgetCount(); i < num; i++) {
			BoundsWidget bw = (BoundsWidget) layout.getWidget(i);
			if (bw != null) {
				Rectangle bounds = bw.getBounds();
				XWidgetCreator comp = (XWidgetCreator) XCreatorUtils.createXCreator(bw.getWidget());
				String constraint = bw.getWidget().acceptType(Label.class) ? WTitleLayout.TITLE : WTitleLayout.BODY;
				this.add(comp, constraint);
				comp.setBounds(bounds);
			}
		}
		isRefreshing = false;
	}

	/**
	 * 组件增加
	 *
	 * @param e 容器事件
	 */
	@Override
	public void componentAdded(ContainerEvent e) {
		if (isRefreshing) {
			return;
		}
		WTitleLayout layout = this.toData();
		XWidgetCreator creator = (XWidgetCreator) e.getChild();
		FRTitleLayout lay = (FRTitleLayout) getLayout();
		Object constraints = lay.getConstraints(creator);
		if (ComparatorUtils.equals(WTitleLayout.TITLE,constraints)) {
			layout.addTitle(creator.toData(), creator.getBounds());
		} else if (ComparatorUtils.equals(WTitleLayout.BODY,constraints)) {
			layout.addBody(creator.toData(), creator.getBounds());
		}
	}

	/**
	 * 组件删除
	 *
	 * @param e
	 *            容器事件
	 */
	@Override
	public void componentRemoved(ContainerEvent e) {
		if (isRefreshing) {
			return;
		}
		XWidgetCreator xwc = ((XWidgetCreator) e.getChild());
		Widget wgt = xwc.toData();
		WTitleLayout wlayout = this.toData();
		wlayout.removeWidget(wgt);
	}

	@Override
	public WidgetPropertyUIProvider[] getWidgetPropertyUIProviders() {
		XCreator creator = getPropertyDescriptorCreator();
		return creator.getWidgetPropertyUIProviders();
	}

	@Override
	public void setShareId(String shareId) {
		super.setShareId(shareId);
		this.getEditingChildCreator().setShareId(shareId);
	}
}
