package com.fr.design.designer.ui;

import com.fr.design.designer.beans.AdapterBus;
import com.fr.design.designer.beans.adapters.layout.FRAbsoluteLayoutAdapter;
import com.fr.design.designer.beans.events.DesignerEvent;
import com.fr.design.designer.creator.XCreator;
import com.fr.design.designer.creator.XWAbsoluteLayout;
import com.fr.design.designer.creator.XWTitleLayout;
import com.fr.design.designer.creator.cardlayout.XWCardMainBorderLayout;
import com.fr.design.gui.ilable.UILabel;
import com.fr.design.i18n.Toolkit;
import com.fr.design.layout.VerticalFlowLayout;
import com.fr.design.mainframe.CoverReportPane;
import com.fr.design.mainframe.EditingMouseListener;
import com.fr.design.mainframe.FormDesigner;
import com.fr.form.ui.Widget;
import com.fr.general.IOUtils;
import com.fr.stable.ArrayUtils;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;
import java.util.List;

/**
 * @author hades
 * @version 10.0
 * Created by hades on 2021/7/8
 */
public class PopupControlPanel extends JPanel {

    private static final int ARC_VALUE = 4;
    private static final Color FILLED_COLOR = new Color(60, 63, 65);
    private static final int BUTTON_SIZE = 16;
    private static final int BUTTON_MARGIN = 4;
    private static final int PANE_WIDTH = BUTTON_SIZE + BUTTON_MARGIN * 2; // 24

    private final Dimension defaultDimension = new Dimension(PANE_WIDTH, 0);
    private Rectangle rectangle;

    private final List<JComponent> buttons = new ArrayList<>();
    private final JButton editButton;
    private final JButton settingButton;
    private final JToggleButton toggleButton;
    private final XCreator creator;

    public PopupControlPanel(XCreator creator, FormDesigner designer) {
        setLayout(new VerticalFlowLayout(VerticalFlowLayout.TOP, 0, 0));
        setBorder(BorderFactory.createEmptyBorder());
        this.creator = creator;

        editButton = createEditButton(designer);
        toggleButton = createAspectRatioLockedButton(designer);
        settingButton = createSettingButton();

        addButton(editButton, 0);
        addButton(toggleButton, 1);
        addButton(settingButton, 2);

        setButtonVisible(editButton, false);
        setButtonVisible(toggleButton, false);
        setButtonVisible(settingButton, false);
    }

    private JButton createEditButton(FormDesigner designer) {
        JButton editButton =  createNormalButton(IOUtils.readIcon("/com/fr/design/images/control/show_edit.png"), Toolkit.i18nText("Fine-Design_Form_Edit_Widget"));
        editButton.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                int x = rectangle.x + rectangle.width / 2;
                int y = rectangle.y + rectangle.height / 2;
                XCreator childCreator = PopupControlPanel.this.creator.getEditingChildCreator();
                MouseListener[] listeners = designer.getMouseListeners();
                if (ArrayUtils.isNotEmpty(listeners) && listeners[0] instanceof EditingMouseListener) {
                    childCreator.respondClick(((EditingMouseListener) listeners[0]), new MouseEvent(childCreator, MouseEvent.MOUSE_CLICKED, e.getWhen(), e.getModifiers(), x, y, 2, false));
                }
            }
        });
        return editButton;
    }

    private JToggleButton createAspectRatioLockedButton(FormDesigner designer) {
        JToggleButton button = new JToggleButton(IOUtils.readIcon("/com/fr/design/images/control/aspect_ratio_unlock.png"));
        initButtonStyle(button);
        button.setSelectedIcon(IOUtils.readIcon("com/fr/design/images/control/aspect_ratio_lock.png"));
        button.setToolTipText(Toolkit.i18nText("Fine-Design_Form_UnLock_Widget_Ratio"));
        button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JToggleButton toggleBtn = (JToggleButton) e.getSource();
                String toolTipText = toggleBtn.isSelected() ? Toolkit.i18nText("Fine-Design_Form_Lock_Widget_Ratio") : Toolkit.i18nText("Fine-Design_Form_UnLock_Widget_Ratio");
                toggleBtn.setToolTipText(toolTipText);
                Widget widget = creator.toData();
                if (widget != null) {
                    Rectangle bounds = new Rectangle(creator.getBounds());
                    if (toggleBtn.isSelected() && bounds.width > 0 && bounds.height > 0) {
                        widget.setAspectRatioLocked(true);
                        widget.setAspectRatioBackup(1.0 * bounds.width / bounds.height);
                    } else {
                        widget.setAspectRatioLocked(false);
                        widget.setAspectRatioBackup(-1.0);
                    }
                }
                designer.getEditListenerTable().fireCreatorModified(creator, DesignerEvent.CREATOR_RESIZED);
            }
        });
        return button;
    }

    private JButton createSettingButton() {
        JButton settingButton = createNormalButton(IOUtils.readIcon("/com/fr/design/images/control/show_setting.png"), Toolkit.i18nText("Fine-Design_Share_Help_Settings"));

        settingButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                CoverReportPane.showShareConfig(creator.toData());
            }
        });
        return settingButton;
    }

    private void addButton(JComponent component, int index) {
        buttons.add(index, component);
        if (index > 0) {
            this.add(new SeparatorLabel(), index * 2 - 1);
        }
        this.add(component, index * 2);
    }

    private void setButtonVisible(JComponent component, boolean visible) {
        int index = buttons.indexOf(component);

        boolean hasVisibleButtonBeforeCurrent = false;
        for (int i = 0; i < index; i++) {
            if (buttons.get(i).isVisible()) {
                hasVisibleButtonBeforeCurrent = true;
                break;
            }
        }

        if (hasVisibleButtonBeforeCurrent) {
            buttons.get(index).setVisible(visible);
            getComponent(2 * index - 1).setVisible(visible);
            return;
        }

        // 在当前按钮之前没有可见的按钮了
        if (index > 0) {
            getComponent(2 * index - 1).setVisible(false);
        }
        buttons.get(index).setVisible(visible);

        if (!visible) {
            // 如果当前按钮设置为不可见，且在当前按钮之前的其他按钮也不可见，则下一个可见按钮前无分割线
            for (int i = index + 1; i < buttons.size(); i++) {
                if (buttons.get(i).isVisible()) {
                    if (i > 0) {
                        getComponent(2 * i - 1).setVisible(false);
                    }
                    break;
                }
            }
        }
    }

    private JButton createNormalButton(Icon icon, String toolTipText) {
        JButton button = new JButton(icon);
        button.setPreferredSize(new Dimension(BUTTON_SIZE + 2 * BUTTON_MARGIN, BUTTON_SIZE + 2 * BUTTON_MARGIN));
        button.setBorder(BorderFactory.createEmptyBorder(BUTTON_MARGIN, BUTTON_MARGIN, BUTTON_MARGIN, BUTTON_MARGIN));
        initButtonStyle(button);
        button.setToolTipText(toolTipText);
        return button;
    }

    private void initButtonStyle(AbstractButton button) {
        button.setPreferredSize(new Dimension(BUTTON_SIZE + 2 * BUTTON_MARGIN, BUTTON_SIZE + 2 * BUTTON_MARGIN));
        button.setBorder(BorderFactory.createEmptyBorder(BUTTON_MARGIN, BUTTON_MARGIN, BUTTON_MARGIN, BUTTON_MARGIN));
        button.setBorderPainted(false);
        button.setContentAreaFilled(false);
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        int w = this.getWidth();
        int h = this.getHeight();
        Graphics2D g2d = (Graphics2D) g;
        g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        g2d.setColor(FILLED_COLOR);
        g2d.fillRoundRect(0, 0, w - 1, h - 1, ARC_VALUE, ARC_VALUE);
        g2d.setColor(Color.WHITE);
        g2d.drawRoundRect(0, 0, w - 1, h - 1, ARC_VALUE, ARC_VALUE);
    }

    public Dimension getDefaultDimension() {
        return defaultDimension;
    }

    public void setRelativeBounds(Rectangle rectangle) {
        this.rectangle = rectangle;
    }

    public void updatePane(FormDesigner designer) {
        setButtonVisible(editButton, creator.acceptType(XWTitleLayout.class, XWCardMainBorderLayout.class) || creator.getClass().equals(XWAbsoluteLayout.class));
        setButtonVisible(settingButton, creator.isShared());
        setButtonVisible(toggleButton, AdapterBus.searchLayoutAdapter(designer, creator) instanceof FRAbsoluteLayoutAdapter);
        toggleButton.setSelected(creator.toData().isAspectRatioLocked());

        updateDimension();
    }

    public boolean hasVisibleButtons() {
        for (int i = 0; i < buttons.size(); i++) {
            if (buttons.get(i).isVisible()) {
                return true;
            }
        }
        return false;
    }

    private void updateDimension() {
        int height = 0;
        for (int i = 0; i < buttons.size(); i++) {
            JComponent component = buttons.get(i);
            if (component.isVisible()) {
                if (i > 0) {
                    height += 1;
                }
                height += component.getHeight();
            }
        }
        height += 2;

        defaultDimension.height = height;
    }

    private static class SeparatorLabel extends UILabel {
        public SeparatorLabel() {
            setPreferredSize(new Dimension(PANE_WIDTH, 1));
        }

        @Override
        public void paint(Graphics g) {
            super.paint(g);
            Graphics2D g2d = (Graphics2D) g;
            g2d.setColor(Color.WHITE);
            g2d.drawLine(BUTTON_MARGIN, 0, getWidth() - BUTTON_MARGIN, 0);
        }
    }
}
