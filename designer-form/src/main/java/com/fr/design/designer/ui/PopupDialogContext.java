package com.fr.design.designer.ui;

import com.fr.design.designer.creator.XCreator;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


/**
 * 把弹窗统一管理起来 防止出现异常情况下游离的弹窗
 *
 * @author hades
 * @version 10.0
 * Created by hades on 2021/11/02
 */
public class PopupDialogContext {

    private static List<SelectedPopupDialog> dialogs = new ArrayList<>();

    public static void add(SelectedPopupDialog selectedPopupDialog) {
        if (dialogs.contains(selectedPopupDialog)) {
            return;
        }
        dialogs.add(selectedPopupDialog);
    }

    public static void remove(SelectedPopupDialog selectedPopupDialog) {
        dialogs.remove(selectedPopupDialog);
    }

    public static void checkSelectedPop(XCreator creator) {
        Iterator<SelectedPopupDialog> iterator = dialogs.iterator();
        List<SelectedPopupDialog> removedDialog = new ArrayList<>();
        while (iterator.hasNext()) {
             SelectedPopupDialog dialog = iterator.next();
            if (dialog.isVisible() && creator != dialog.getCreator()) {
                iterator.remove();
                removedDialog.add(dialog);
            }
        }
        for (SelectedPopupDialog dialog : removedDialog) {
            dialog.setVisible(false);
        }
    }

}
