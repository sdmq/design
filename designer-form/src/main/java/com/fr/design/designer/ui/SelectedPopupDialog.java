package com.fr.design.designer.ui;

import com.fr.design.designer.creator.XCreator;
import com.fr.design.mainframe.DesignerContext;
import com.fr.design.mainframe.FormDesigner;
import com.fr.stable.os.OperatingSystem;

import java.awt.Rectangle;
import javax.swing.JDialog;
import javax.swing.JFrame;

/**
 * @author hades
 * @version 10.0
 * Created by hades on 2021/7/09
 */
public class SelectedPopupDialog extends JDialog {

    /**
     * 弹窗的相对组件的偏移
     */
    public static final int OFFSET_X = 5;

    private final PopupControlPanel controlPanel;

    private boolean canVisible = true;

    private final XCreator creator;

    public SelectedPopupDialog(XCreator creator, FormDesigner designer) {
        super(OperatingSystem.isMacos() ? new JFrame() : DesignerContext.getDesignerFrame());
        this.setUndecorated(true);
        this.setModal(false);
        this.setFocusableWindowState(false);
        this.creator = creator;
        controlPanel = new PopupControlPanel(creator, designer);
        this.getContentPane().add(controlPanel);
        this.setSize(controlPanel.getDefaultDimension());
    }

    public void updatePane(FormDesigner designer) {
        controlPanel.updatePane(designer);
        this.setSize(controlPanel.getDefaultDimension());
    }

    public boolean hasVisibleButtons() {
        return controlPanel.hasVisibleButtons();
    }

    @Override
    public void setVisible(boolean visible) {
        super.setVisible(visible);
        if (visible) {
            PopupDialogContext.add(this);
        } else {
            PopupDialogContext.remove(this);
        }
    }

    public void setRelativeBounds(Rectangle rectangle) {
        this.controlPanel.setRelativeBounds(rectangle);
    }

    public boolean isCanVisible() {
        return canVisible;
    }

    public void setCanVisible(boolean canVisible) {
        this.canVisible = canVisible;
    }

    public XCreator getCreator() {
        return this.creator;
    }
}
