package com.fr.design.form.util;

import com.fr.design.designer.creator.XCreator;
import com.fr.design.designer.creator.XWAbsoluteBodyLayout;
import com.fr.design.designer.creator.XWAbsoluteLayout;
import com.fr.design.designer.creator.XWFitLayout;
import com.fr.design.file.HistoryTemplateListCache;
import com.fr.design.mainframe.FormDesigner;
import com.fr.design.mainframe.JTemplate;
import com.fr.design.mainframe.template.info.ComponentCreateOperate;
import com.fr.design.mainframe.template.info.ComponentDeleteOperate;
import com.fr.form.ui.Widget;
import com.fr.form.ui.container.WFitLayout;

import java.awt.Container;


public class FormDesignerUtils {
    /**
     * body布局是否设置了手机重布局
     *
     * @param designer
     * @return
     */
    public static boolean isAppRelayout(FormDesigner designer) {
        if (!designer.getRootComponent().acceptType(XWFitLayout.class)) {
            return false;
        }
        return ((WFitLayout) designer.getRootComponent().toData()).isAppRelayout();
    }

    /**
     * body布局是否设置了绝对布局
     *
     * @param designer
     * @return
     */
    public static boolean isBodyAbsolute(FormDesigner designer) {
        if (!designer.getRootComponent().acceptType(XWFitLayout.class)) {
            return false;
        }
        WFitLayout root = ((WFitLayout) designer.getRootComponent().toData());
        return root.getBodyLayoutType() == com.fr.form.ui.container.WBodyLayoutType.ABSOLUTE;
    }


    /**
     * 添加控件
     * 被云端运维插件修改用于收集埋点
     */
    public static void addWidgetProcessInfo(Widget widget) {
        JTemplate jTemplate = HistoryTemplateListCache.getInstance().getCurrentEditingTemplate();
        jTemplate.getProcessInfo().updateTemplateOperationInfo(new ComponentCreateOperate(widget));
    }

    /**
     * 删除控件
     * 被云端运维插件修改用于收集埋点
     */
    public static void removeWidgetProcessInfo(Widget widget) {
        JTemplate jTemplate = HistoryTemplateListCache.getInstance().getCurrentEditingTemplate();
        jTemplate.getProcessInfo().updateTemplateOperationInfo(new ComponentDeleteOperate(widget));
    }

    /**
     * 判断当前UI组件是否在绝对画布块中
     *
     * @param xCreator
     * @return
     */
    public static boolean isInAbsoluteLayout(XCreator xCreator) {
        Container parent = xCreator.getParent();
        while (parent != null) {
            if (parent instanceof XWAbsoluteLayout && !(parent instanceof XWAbsoluteBodyLayout)) {
                return true;
            }
            parent = parent.getParent();
        }
        return false;
    }

}
