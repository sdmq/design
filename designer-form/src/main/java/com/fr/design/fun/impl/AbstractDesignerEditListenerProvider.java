package com.fr.design.fun.impl;

import com.fr.design.fun.DesignerEditListenerProvider;
import com.fr.stable.fun.impl.AbstractProvider;
import com.fr.stable.fun.mark.API;

/**
 * created by Harrison on 2020/05/14
 **/
@API(level = DesignerEditListenerProvider.CURRENT_LEVEL)
public abstract class AbstractDesignerEditListenerProvider extends AbstractProvider implements DesignerEditListenerProvider {
    
    @Override
    public int currentAPILevel() {
        return DesignerEditListenerProvider.CURRENT_LEVEL;
    }
}
