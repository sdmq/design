package com.fr.design.gui.controlpane;

import com.fr.design.designer.creator.XCreator;
import com.fr.design.designer.properties.EventPropertyTable;
import com.fr.design.i18n.Toolkit;
import com.fr.design.mainframe.FormDesigner;
import com.fr.design.widget.EventCreator;
import com.fr.form.event.Listener;
import com.fr.form.ui.Widget;
import com.fr.general.NameObject;
import com.fr.stable.Nameable;

/**
 * Created by kerry on 5/17/21
 */
public class EventPropertyPane extends UIListGroupControlPane {
    private XCreator creator;

    private FormDesigner designer;


    public EventPropertyPane(FormDesigner designer) {
        super();
        this.designer = designer;
    }


    /**
     * 刷新
     */
    public void refresh() {
        int selectionSize = designer.getSelectionModel().getSelection().size();
        if (selectionSize == 0 || selectionSize == 1) {
            this.creator = selectionSize == 0 ? designer.getRootComponent() : designer.getSelectionModel()
                    .getSelection().getSelectedCreator();
        } else {
            this.creator = null;
            this.getContentPane().removeAll();
            checkButtonEnabled();
            return;
        }
        Widget widget = creator.toData();
        refreshPane(widget, EventCreator.createEventCreator(widget.supportedEvents(), EventPropertyTable.WidgetEventListenerUpdatePane.class));
    }


    public void populateNameObjects() {
        Widget widget = creator.toData();
        populateNameObjects(widget);
    }

    /**
     * 更新控件事件
     *
     * @param creator 控件
     */
    public void updateWidgetListener(XCreator creator) {
        (creator.toData()).clearListeners();
        Nameable[] res = this.update();
        for (int i = 0; i < res.length; i++) {
            NameObject nameObject = (NameObject) res[i];
            (creator.toData()).addListener((Listener) nameObject.getObject());
        }

        designer.fireTargetModified();
        checkButtonEnabled();
    }


    @Override
    public NameableCreator[] createNameableCreators() {
        return new NameableCreator[]{
                new EventCreator(Widget.EVENT_STATECHANGE, EventPropertyTable.WidgetEventListenerUpdatePane.class)
        };
    }

    @Override
    public void saveSettings() {
        if (isPopulating()) {
            return;
        }
        updateWidgetListener(creator);
    }

    @Override
    protected String title4PopupWindow() {
        return "Event";
    }

    @Override
    public String getAddItemText() {
        return com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Report_Add_Event");
    }

    protected String getWrapperLabelText() {
        return Toolkit.i18nText("Fine-Design_Report_Event");
    }

}
