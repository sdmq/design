package com.fr.design.mainframe;

import java.awt.Point;

public class FormVerticalParallelLine extends AbstractFormParallelLine {
    public FormVerticalParallelLine(int parallelValue, int startPosition, int endPosition) {
        super(parallelValue, startPosition, endPosition);
    }

    @Override
    public Point getStartPointOnVerticalCenterLine() {
        Point point = new Point();
        point.setLocation(parallelValue, getCenterPosition());
        return point;
    }

    @Override
    public Point getEndPointOnVerticalCenterLine(int parallelValue) {
        Point point = new Point();
        point.setLocation(parallelValue, getCenterPosition());
        return point;
    }

    @Override
    public Point getExtendedLineStartPoint(AbstractFormParallelLine parallelLine) {
        Point point = new Point();
        if (parallelLine.isVerticalCenterLineBeforeTheParallelLine(this)) {
            point.setLocation(getParallelValue(), getStartPosition());
        } else if (parallelLine.isVerticalCenterLineBehindTheParallelLine(this)) {
            point.setLocation(getParallelValue(), getEndPosition());
        }
        return point;
    }

    @Override
    public Point getExtendedLineEndPoint(AbstractFormParallelLine parallelLine) {
        Point point = new Point();
        if (parallelLine.isVerticalCenterLineBeforeTheParallelLine(this)) {
            point.setLocation(getParallelValue(), parallelLine.getStartPosition());
        } else if (parallelLine.isVerticalCenterLineBehindTheParallelLine(this)) {
            point.setLocation(getParallelValue(), parallelLine.getEndPosition());
        }
        return point;
    }
}
