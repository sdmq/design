package com.fr.design.mainframe;

import com.fr.base.BaseUtils;
import com.fr.design.dialog.BasicPane;
import com.fr.design.gui.ibutton.UIHeadGroup;
import com.fr.design.gui.ilable.UILabel;
import com.fr.design.i18n.Toolkit;
import com.fr.design.layout.FRGUIPaneFactory;
import com.fr.design.locale.impl.ShowOnlineWidgetMark;
import com.fr.design.mainframe.reuse.ComponentReuseNotificationInfo;
import com.fr.design.mainframe.share.collect.ComponentCollector;
import com.fr.design.mainframe.share.ui.local.LocalWidgetRepoPane;
import com.fr.design.mainframe.share.ui.online.OnlineWidgetRepoPane;
import com.fr.general.locale.LocaleCenter;
import com.fr.general.locale.LocaleMark;

import javax.swing.BorderFactory;
import javax.swing.Icon;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.SwingConstants;
import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: zx
 * Date: 14-7-8
 * Time: 下午8:18
 */
public class FormWidgetDetailPane extends FormDockView{
    private static final int LOCAL_TAB = 0;
    private static final int ONLINE_TAB = 1;

    private JPanel centerPane;
    private UIHeadGroup headGroup;
    private List<BasicPane> paneList;

    private boolean isEmptyPane = false;

    public static FormWidgetDetailPane getInstance() {
        if (HOLDER.singleton == null) {
            HOLDER.singleton = new FormWidgetDetailPane();
        }
        return HOLDER.singleton;
    }

    private  FormWidgetDetailPane(){
        setLayout(FRGUIPaneFactory.createBorderLayout());
    }


    public static FormWidgetDetailPane getInstance(FormDesigner formEditor) {
        HOLDER.singleton.setEditingFormDesigner(formEditor);
        HOLDER.singleton.refreshDockingView();
        return HOLDER.singleton;
    }

    private static class HOLDER {
        private static FormWidgetDetailPane singleton = new FormWidgetDetailPane();
    }

    public String getViewTitle() {
        return Toolkit.i18nText("Fine-Design_Form_Widget_Tree_And_Table");
    }

    @Override
    public Icon getViewIcon() {
        return BaseUtils.readIcon("/com/fr/design/images/m_report/attributes.png");
    }

    /**
     * 初始化
     */
    public void refreshDockingView(){
        if (isEmptyPane) {
            return;
        }
        FormDesigner designer = this.getEditingFormDesigner();
        removeAll();
        if (designer == null) {
            clearDockingView();
            return;
        }

        initPaneList();
        this.setBorder(null);
        final CardLayout cardLayout = new CardLayout();
        centerPane = new JPanel(cardLayout);
        String[] paneNames = new String[paneList.size()];
        for (int i = 0; i < paneList.size(); i++) {
            String title =  paneList.get(i).getTitle();
            paneNames[i] = title;
            centerPane.add(paneList.get(i), title);
        }
         headGroup = new UIHeadGroup(paneNames) {
            protected void tabChanged(int newSelectedIndex) {
                //初始化还未展示的时候不需要收集其 marketClick
                if (this.isShowing() && newSelectedIndex == 1) {
                    ComponentCollector.getInstance().collectMarkerClick();
                }
                cardLayout.show(centerPane, paneList.get(newSelectedIndex).getTitle());
            }
        };
        headGroup.setSelectedIndex(isNeedSwitchToOnlineTab() ? ONLINE_TAB : LOCAL_TAB);
        this.add(headGroup, BorderLayout.NORTH);
        this.add(centerPane, BorderLayout.CENTER);
    }

    private boolean isNeedSwitchToOnlineTab() {
        return !ComponentReuseNotificationInfo.getInstance().isClickedWidgetLib() && isShowOnlineWidgetRepoPane();
    }

    public void resetEmptyPane(){
        this.isEmptyPane = false;
    }


    /**
     * 清除数据
     */
    public void clearDockingView() {
        JScrollPane psp = new JScrollPane();
        psp.setBorder(null);
        this.add(psp, BorderLayout.CENTER);
    }

    public void switch2Empty(){
        isEmptyPane = true;
        this.removeAll();
        JPanel panel = FRGUIPaneFactory.createVerticalFlowLayout_Pane(true, FlowLayout.LEADING, 0, 5);
        UILabel imagePanel = new UILabel(BaseUtils.readIcon("/com/fr/design/form/images/version_not_match.png"));
        imagePanel.setPreferredSize(new Dimension(240, 96));
        imagePanel.setHorizontalAlignment(SwingConstants.CENTER);
        panel.add(imagePanel);
        UILabel uiLabel1 = tipLabel(Toolkit.i18nText("Fine-Design_Share_Version_Not_Match_Tip1"));
        UILabel uiLabel2 = tipLabel(Toolkit.i18nText("Fine-Design_Share_Version_Not_Match_Tip2"));
        panel.add(uiLabel1);
        panel.add(uiLabel2);
        panel.setBorder(BorderFactory.createEmptyBorder(240, 0, 0, 0));
        this.add(panel, BorderLayout.CENTER);
    }

    private UILabel tipLabel(String text) {
        UILabel tipLabel = new UILabel(text);
        tipLabel.setHorizontalAlignment(SwingConstants.CENTER);
        tipLabel.setPreferredSize(new Dimension(240, 20));
        tipLabel.setForeground(Color.decode("#8F8F92"));
        return tipLabel;
    }


    public  void enterWidgetLib() {
        ComponentReuseNotifyUtil.enterWidgetLibExtraAction();
        EastRegionContainerPane.getInstance().switchTabTo(EastRegionContainerPane.KEY_WIDGET_LIB);
    }

    /**
     * 定位
     *
     * @return 位置
     */
    @Override
    public Location preferredLocation() {
        return Location.WEST_BELOW;
    }

    private void initPaneList() {
        paneList = new ArrayList<>();
        paneList.add(LocalWidgetRepoPane.getInstance());
        if (isShowOnlineWidgetRepoPane()) {
            paneList.add(OnlineWidgetRepoPane.getInstance());
        }
    }

    private boolean isShowOnlineWidgetRepoPane() {
        LocaleMark<Boolean> localeMark = LocaleCenter.getMark(ShowOnlineWidgetMark.class);
        return localeMark.getValue();
    }
}
