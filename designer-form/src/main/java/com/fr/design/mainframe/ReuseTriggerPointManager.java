

package com.fr.design.mainframe;

import com.fr.base.iofile.attr.ExtendSharableAttrMark;
import com.fr.design.DesignerEnvManager;
import com.fr.design.file.HistoryTemplateListCache;
import com.fr.design.gui.ilable.UILabel;
import com.fr.design.i18n.Toolkit;
import com.fr.design.layout.FRGUIPaneFactory;
import com.fr.design.mainframe.adaptve.config.TriggerPointProvider;

import com.fr.design.mainframe.adaptve.config.impl.CellStyleTriggerPoint;
import com.fr.design.mainframe.adaptve.config.impl.CellValueImageChangeTriggerPoint;
import com.fr.design.mainframe.adaptve.config.ReuseNotifyInfo;
import com.fr.design.mainframe.reuse.ComponentReuseNotificationInfo;
import com.fr.design.mainframe.share.collect.ComponentCollector;
import com.fr.design.mainframe.toast.DesignerToastMsgUtil;
import com.fr.event.Event;
import com.fr.event.EventDispatcher;
import com.fr.event.Listener;
import com.fr.event.Null;
import com.fr.form.main.Form;
import com.fr.form.main.WidgetGather;
import com.fr.form.ui.AbstractBorderStyleWidget;
import com.fr.form.ui.Widget;
import com.fr.stable.StringUtils;

import javax.swing.BorderFactory;
import javax.swing.JPanel;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * Created by kerry on 4/28/21
 */
public class ReuseTriggerPointManager {
    private static final long ONE_WEEK_TIME = 7 * 24 * 3600 * 1000L;

    private static class Holder {
        private static final ReuseTriggerPointManager HOLDER = new ReuseTriggerPointManager();
    }

    public static ReuseTriggerPointManager getInstance() {
        return Holder.HOLDER;
    }

    private Map<JForm, ReuseNotifyInfo> map = new HashMap<>();

    private List<Listener> listeners = new ArrayList<>();

    private ReuseTriggerPointManager() {
        if (!hasNotifiedTwice()) {
            List<TriggerPointProvider> list = getTriggerPoints();
            for (TriggerPointProvider triggerPoint : list) {
                Listener listener = new Listener<Null>() {
                    @Override
                    public void on(Event event, Null o) {
                        triggerPoint.triggerAction();
                    }
                };
                EventDispatcher.listen(triggerPoint.triggerEvent(), listener);
                listeners.add(listener);
            }

        }
    }

    private List<TriggerPointProvider> getTriggerPoints() {
        List<TriggerPointProvider> list = new ArrayList<>();
        list.add(new CellStyleTriggerPoint());
        list.add(new CellValueImageChangeTriggerPoint());
        return list;
    }


    public boolean hasNotifiedTwice() {
        return ComponentReuseNotificationInfo.getInstance().getNotifiedNumber() >= 2;
    }


    private void reCount() {
        //重新计次数
        Iterator<Map.Entry<JForm, ReuseNotifyInfo>> iterator = map.entrySet().iterator();
        while (iterator.hasNext()) {
            iterator.next().getValue().reset();
        }
    }

    private void writeTriggerInfo2xml() {
        int number = ComponentReuseNotificationInfo.getInstance().getNotifiedNumber() + 1;
        ComponentReuseNotificationInfo.getInstance().setNotifiedNumber(number);
        ComponentReuseNotificationInfo.getInstance().setLastNotifyTime(System.currentTimeMillis());
        DesignerEnvManager.getEnvManager().saveXMLFile();
        //如果已经提示过两次了
        if (hasNotifiedTwice()) {
            for (Listener listener : listeners) {
                EventDispatcher.stopListen(listener);
            }
            this.map.clear();
        }
    }

    public boolean needTrigger() {
        boolean result = true;
        if (ComponentReuseNotificationInfo.getInstance().getLastNotifyTime() > 0L) {
            result = System.currentTimeMillis() - ComponentReuseNotificationInfo.getInstance().getLastNotifyTime() > ONE_WEEK_TIME;
        }
        return !hasNotifiedTwice() && result;
    }


    public void registerJForm(JForm jForm) {
        if (!hasNotifiedTwice()) {
            this.map.put(jForm, new ReuseNotifyInfo());
        }

    }

    public void removeJForm(JForm jForm) {
        if (!hasNotifiedTwice()) {
            this.map.remove(jForm);
        }
    }


    public ReuseNotifyInfo getReuseNotifyInfo() {
        JTemplate currentJTemplate = HistoryTemplateListCache.getInstance().getCurrentEditingTemplate();
        if (!(currentJTemplate instanceof JForm && hasUseReuseComponent((JForm) currentJTemplate))
                && !ReuseTriggerPointManager.getInstance().needTrigger()) {
            return null;
        }
        return map.get(currentJTemplate);
    }

    public void reuseNotify(ReuseNotifyInfo notifyInfo) {
        if (notifyInfo.matchCondition()) {
            ReuseTriggerPointManager.getInstance().reCount();
            //弹出提示框
            JTemplate currentJTemplate = HistoryTemplateListCache.getInstance().getCurrentEditingTemplate();
            DesignerToastMsgUtil.toastPrompt(createReusePrompt((JForm) currentJTemplate));
            ReuseTriggerPointManager.getInstance().writeTriggerInfo2xml();
            ComponentCollector.getInstance().collectPromptJumpWhenShow();
        }
    }


    private boolean hasUseReuseComponent(JForm jForm) {
        Form form = jForm.getTarget();
        List<Widget> extendSharableWidgetList = new ArrayList<>();
        Form.traversalWidget(form.getContainer(), new WidgetGather() {
            @Override
            public void dealWith(Widget widget) {
                ExtendSharableAttrMark attrMark = ((AbstractBorderStyleWidget) widget).getWidgetAttrMark(ExtendSharableAttrMark.XML_TAG);
                if (attrMark != null && StringUtils.isNotEmpty(attrMark.getShareId())) {
                    extendSharableWidgetList.add(widget);
                }
            }

            @Override
            public boolean dealWithAllCards() {
                return true;
            }
        }, AbstractBorderStyleWidget.class);
        return extendSharableWidgetList.size() > 0;
    }


    private JPanel createReusePrompt(JForm jForm) {
        JPanel jPanel = FRGUIPaneFactory.createBorderLayout_S_Pane();
        jPanel.add(new UILabel(Toolkit.i18nText("Fine-Design_Component_Reuse_Try_Prompt")), BorderLayout.WEST);
        UILabel reuseLabel = new UILabel(Toolkit.i18nText("Fine-Design_Share_Component"));
        reuseLabel.addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {
                jForm.tabChanged(0);
                FormWidgetDetailPane.getInstance().enterWidgetLib();
                ComponentCollector.getInstance().collectPromptJumpWhenJump();
            }

            @Override
            public void mousePressed(MouseEvent e) {

            }

            @Override
            public void mouseReleased(MouseEvent e) {

            }

            @Override
            public void mouseEntered(MouseEvent e) {

            }

            @Override
            public void mouseExited(MouseEvent e) {

            }
        });
        reuseLabel.setBorder(BorderFactory.createEmptyBorder(0, 3, 0, 0));
        reuseLabel.setForeground(Color.BLUE);
        reuseLabel.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        jPanel.add(reuseLabel, BorderLayout.CENTER);
        return jPanel;
    }


}
