package com.fr.design.mainframe;

import com.fr.design.designer.beans.AdapterBus;
import com.fr.design.designer.beans.ComponentAdapter;
import com.fr.design.designer.creator.XCreator;
import com.fr.design.designer.creator.XCreatorUtils;
import com.fr.design.designer.creator.XLayoutContainer;
import com.fr.design.designer.creator.cardlayout.XWCardMainBorderLayout;

/**
 *  判断tab块是否能拖入
 *
 * @author hades
 * @version 10.0
 * Created by hades on 2021/6/29
 */
public class TabDragInner {

    /**
     * 悬浮停留的时间间隔
     */
    private static final long TIME_GAP = 1000;

    private FormDesigner designer;
    private XLayoutContainer belowXLayoutContainer;
    private long timer;
    private int oldX;
    private int oldY;

    public TabDragInner(FormDesigner designer) {
        this.designer = designer;
    }

    /**
     * 判断拖入
     *
     * @param creator 当前拖拽的组件下方所在布局最上层的组件
     * @param x
     * @param y
     */
    public void canDragIn(XCreator creator, int x, int y) {
        XLayoutContainer topLayout = XCreatorUtils.getHotspotContainer(creator).getTopLayout();
        boolean dragInAble = false;
        if (topLayout != null && topLayout.acceptType(XWCardMainBorderLayout.class) && belowXLayoutContainer == null) {
            belowXLayoutContainer = topLayout;
            timer = System.currentTimeMillis();
        } else if (topLayout == belowXLayoutContainer && topLayout != null && oldX == x && oldY == y) {
            if (System.currentTimeMillis() - timer > TIME_GAP) {
                dragInAble = true;
            }
        } else {
            timer = 0;
            belowXLayoutContainer = null;
        }
        if (topLayout != null) {
            topLayout.setDragInAble(dragInAble);
        }
        oldX = x;
        oldY = y;
    }

    /**
     * 尝试进入tab编辑
     */
    public void tryDragIn() {
        if (belowXLayoutContainer != null && belowXLayoutContainer.isDragInAble()) {
            EditingMouseListener editingMouseListener = new EditingMouseListener(designer);
            editingMouseListener.refreshTopXCreator();
            belowXLayoutContainer.setEditable(true);
            if (editingMouseListener.stopEditing() && belowXLayoutContainer != designer.getRootComponent()) {
                ComponentAdapter adapter = AdapterBus.getComponentAdapter(designer, belowXLayoutContainer);
                if (adapter != null) {
                    editingMouseListener.startEditing(belowXLayoutContainer, adapter.getDesignerEditor(), adapter);
                    belowXLayoutContainer.setDragInAble(false);
                }
            }
        }
    }


    public XLayoutContainer getBelowXLayoutContainer() {
        return belowXLayoutContainer;
    }
}
