package com.fr.design.mainframe.adaptve.config;

/**
 * Created by kerry on 5/7/21
 */
public class ReuseNotifyInfo {
    private static final int CELL_STYLE_MODIFY_MAX_NUMBER = 3;
    private static final int CELL_IMAGE_VALUE_MODIFY_MAX_NUMBER = 1;
    private int cellStyleModifiedNumber = 0;
    private int cellImageValueNumber = 0;

    public void addCellStyleModify() {
        cellStyleModifiedNumber++;
    }


    public void addCellImageValueModify() {
        cellImageValueNumber++;
    }

    public boolean matchCondition() {
        return cellStyleModifiedNumber >= CELL_STYLE_MODIFY_MAX_NUMBER
                || cellImageValueNumber >= CELL_IMAGE_VALUE_MODIFY_MAX_NUMBER;
    }

    public void reset() {
        this.cellImageValueNumber = 0;
        this.cellStyleModifiedNumber = 0;
    }
}
