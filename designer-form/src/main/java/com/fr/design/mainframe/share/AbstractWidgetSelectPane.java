package com.fr.design.mainframe.share;

import com.fr.design.mainframe.share.ui.base.PopupPreviewPane;
import com.fr.design.mainframe.share.ui.block.PreviewWidgetBlock;
import com.fr.design.utils.gui.GUICoreUtils;
import com.fr.general.ComparatorUtils;
import com.fr.stable.StringUtils;

import javax.swing.JPanel;
import java.awt.Container;

/**
 * @Author: Yuan.Wang
 * @Date: 2021/1/15
 */
public abstract class AbstractWidgetSelectPane extends JPanel {
    private static final int SCROLL_BAR_HEIGHT = 10;
    private final PopupPreviewPane previewPane;
    private String currentShowWidgetID;

    public AbstractWidgetSelectPane() {
        this.previewPane = new PopupPreviewPane();
    }

    public void showPreviewPane(PreviewWidgetBlock<?> comp, String showWidgetID) {
        if (ComparatorUtils.equals(currentShowWidgetID, showWidgetID)) {
            return;
        }
        popupPreviewPane(comp, showWidgetID);
    }

    public void hidePreviewPane() {
        if (previewPane != null && previewPane.isVisible()) {
            previewPane.setVisible(false);
        }
        this.currentShowWidgetID = StringUtils.EMPTY;
    }

    private void popupPreviewPane(PreviewWidgetBlock<?> comp, String showWidgetID) {
        if (previewPane.isVisible()) {
            previewPane.setVisible(false);
        }

        if (!previewPane.isVisible() && comp.getWidth() != 0 && comp.getHeight() != 0) {
            //父容器是GroupPane，要获得的是GroupPane的父容器
            Container parentContainer =getParentContainer();
            previewPane.setComp(comp.getPreviewImage());
            int popupPosY = comp.getLocationOnScreen().y - parentContainer.getLocationOnScreen().y;
            if (previewPane.getHeight() + popupPosY > parentContainer.getHeight() + SCROLL_BAR_HEIGHT) {
                popupPosY -= (previewPane.getHeight() + popupPosY - parentContainer.getHeight() - SCROLL_BAR_HEIGHT);
            }
            int popupPosX = -previewPane.getPreferredSize().width;
            if (parentContainer.getLocationOnScreen().x < previewPane.getPreferredSize().width) {
                popupPosX = parentContainer.getWidth();
            }
            GUICoreUtils.showPopupMenu(previewPane, parentContainer, popupPosX, popupPosY);
            this.currentShowWidgetID = showWidgetID;
        }
    }

    abstract protected Container getParentContainer();
}
