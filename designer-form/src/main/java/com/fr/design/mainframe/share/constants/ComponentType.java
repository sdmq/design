package com.fr.design.mainframe.share.constants;

import java.util.List;

/**
 * created by Harrison on 2020/04/20
 **/
public interface ComponentType {
    
    List<String> children(int device);
    
    /**
     * 返回子类型所有
     *
     * @return
     */
    List<String> types();
}
