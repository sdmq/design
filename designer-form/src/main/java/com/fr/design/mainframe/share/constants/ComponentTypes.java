package com.fr.design.mainframe.share.constants;

import com.fr.design.i18n.Toolkit;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

/**
 * created by Harrison on 2020/04/20
 **/
public enum ComponentTypes implements ComponentType {

    CHART("Fine-Design_Share_Type_Basic_Element") {
        @Override
        public List<String> children(int device) {

            return ChartChildTypes.support(device);
        }

        @Override
        public List<String> types() {
            List<String> list = new ArrayList<>();
            for (ChartChildTypes types : ChartChildTypes.values()) {
                list.add(types.getLocText());
            }
            return list;
        }
    },

    REPORT("Fine-Design_Share_Type_Report") {
        @Override
        public List<String> children(int device) {

            if (DisplayDevice.supportMobile(device)) {
                return ReportChildTypes.support(DisplayDevice.MOBILE.getType());
            }
            return ReportChildTypes.support(DisplayDevice.PC.getType());
        }

        @Override
        public List<String> types() {
            List<String> list = new ArrayList<>();
            for (ReportChildTypes types : ReportChildTypes.values()) {
                list.add(types.getLocText());
            }
            return list;
        }
    };

    /**
     * 所有类型
     *
     * @return
     */
    public static List<String> allTypes() {
        List<String> allTypes = new ArrayList<>();
        Set<String> set = new LinkedHashSet<>();
        for (ComponentTypes types : values()) {
            set.addAll(types.children(DisplayDevice.MOBILE.getType()));
            set.addAll(types.children(DisplayDevice.PC.getType()));
        }
        allTypes.addAll(set);
        return allTypes;
    }

    /**
     * 根据终端返回所有类型
     *
     * @param device
     * @return
     */
    public static List<String> allTypesByDevice(int device) {
        List<String> allTypes = new ArrayList<>();
        for (ComponentTypes types : values()) {
            allTypes.addAll(types.children(device));
        }
        return allTypes;
    }

    private enum ReportChildTypes {

        INDICATOR_CARD("Fine-Design_Share_Type_Indicator_Card"),

        TITLE("Fine-Design_Share_Type_Title"),

        DIMENSION_CHANGE("Fine-Design_Share_Type_Dimension_Change"),

        FILL("Fine-Design_Share_Type_Fill", DisplayDevice.MOBILE),

        Directory_Navigation("Fine-Design_Share_Type_Mobile_Directory_Navigation", DisplayDevice.MOBILE),


        SPECIAL_CARD("Fine-Design_Share_Type_Special_Card");

        private String locale;

        /**
         * @see DisplayDevice
         */
        private int support;

        ReportChildTypes(String locale) {
            this(locale, DisplayDevice.buildAll());
        }

        ReportChildTypes(String locale, DisplayDevice device) {
            this(locale, device.getType());
        }

        ReportChildTypes(String locale, int support) {
            this.locale = locale;
            this.support = support;
        }

        public static List<String> support(int device) {

            List<String> list = new ArrayList<>();
            ReportChildTypes[] values = ReportChildTypes.values();
            for (ReportChildTypes value : values) {
                //支持当前设备
                if (DisplayDevice.supportDevice(device, value.support)) {
                    list.add(value.getLocText());
                }
            }
            return list;
        }

        public String getLocText() {

            return Toolkit.i18nText(this.locale);
        }
    }

    private enum ChartChildTypes {

        COLUMN_CHART("Fine-Design_Share_Type_Chart_Column"),

        PIE_CHART("Fine-Design_Share_Type_Chart_Pie"),

        FOLD_LINE_CHART("Fine-Design_Share_Type_Chart_Fold_Line"),

        COMBINE_CHART("Fine-Design_Share_Type_Chart_Combine"),

        METER_CHART("Fine-Design_Share_Type_Chart_Meter"),

        MAP_CHART("Fine-Design_Share_Type_Chart_Map"),

        OTHERS("Fine-Design_Share_Type_Chart_Other"),

        DETAIL_LIST("Fine-Design_Share_Type_Detail_List"),

        BASIC_WIDGET("Fine-Design_Share_Type_Basic_Widget");

        private String locale;

        ChartChildTypes(String locale) {
            this.locale = locale;
        }

        public static List<String> support(int device) {

            List<String> list = new ArrayList<>();
            ChartChildTypes[] values = ChartChildTypes.values();
            for (ChartChildTypes value : values) {
                list.add(value.getLocText());
            }
            return list;
        }

        public String getLocText() {

            return Toolkit.i18nText(this.locale);
        }
    }

    private String locale;

    ComponentTypes(String locale) {
        this.locale = locale;
    }

    public String getLocText() {

        return Toolkit.i18nText(this.locale);
    }

    @Override
    public String toString() {
        return getLocText();
    }
}
