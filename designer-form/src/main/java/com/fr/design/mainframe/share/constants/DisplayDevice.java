package com.fr.design.mainframe.share.constants;

/**
 * created by Harrison on 2020/04/21
 **/
public enum DisplayDevice {
    
    NONE(0),
    
    MOBILE(1),
    
    PC(2);
    
    private int type;
    
    DisplayDevice(int type) {
        this.type = type;
    }
    
    public int getType() {
        return type;
    }
    
    public static int buildAll() {
    
        int support = 0;
        DisplayDevice[] values = DisplayDevice.values();
        for (DisplayDevice value : values) {
            support |= value.getType();
        }
        return support;
    }
    
    public static int buildDevice(Iterable<DisplayDevice> devices) {
    
        int support = 0;
        if (devices == null) {
            return support;
        }
        for (DisplayDevice device : devices) {
            support |= device.type;
        }
        return support;
    }
    
    public static boolean supportDevice(int device, int support) {
    
        return device == buildAll() || (support & device) == device;
    }
    
    public static boolean supportMobile(int device) {
    
        return (device & MOBILE.getType()) == MOBILE.getType();
    }
    
    public static boolean supportPC(int device) {
    
        return (device & PC.getType()) == PC.getType();
    }
    
    private static int calVal(DisplayDevice device) {
        
        return 1 << device.type;
    }
    
}
