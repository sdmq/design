package com.fr.design.mainframe.share.constants;

public class ShareEntryKey {
    static public final String SHARE_GENERATE = "ShareGeneratePane";
    static public final String SHARE_CONFIG = "ShareConfigPane";
}
