package com.fr.design.mainframe.share.encrypt.clipboard;

import com.fr.stable.StringUtils;

import java.util.concurrent.atomic.AtomicReference;

/**
 * 跨模版禁用
 * <p>
 * created by Harrison on 2020/05/14
 **/
public abstract class AbstractCrossClipBoardState implements CrossClipboardState {
    private final AtomicReference<String> sourceId = new AtomicReference<>();

    @Override
    public synchronized boolean isBan() {

        String sourceId = this.sourceId.get();
        if (StringUtils.isEmpty(sourceId)) {
            return false;
        }
        //这里只获取新的，不能更新
        //因为新的模板可能不是限制模板，然而剪贴板中的内容没有清空。
        //所以，直接在新的模板中再一次粘贴，就可以避过限制
        String targetId = get();
        return isCross(sourceId, targetId) && isRestrict(sourceId);
    }

    protected boolean isRestrict(String sourceId) {

        return true;
    }

    @Override
    public synchronized void mark() {

        update();
    }

    private String get() {

        return currentId();
    }

    private void update() {

        String templateId = currentId();
        sourceId.set(templateId);
    }

    private boolean isCross(String sourceId, String targetId) {

        //源 id 不等于 null
        //如果源 id 等于 null , 两种情况
        //1-之前没有
        //2-是从其他地方，复制过来，这样的话，直接通过就好了。
        return StringUtils.isNotEmpty(sourceId)
                && !StringUtils.equals(sourceId, targetId);
    }

    protected abstract String currentId();
}
