package com.fr.design.mainframe.share.encrypt.clipboard;

/**
 * created by Harrison on 2020/05/18
 **/
public interface CrossClipboardState {
    
    
    /**
     * 是否禁用
     *
     * @return y/n
     */
    boolean isBan();
    
    /**
     * 标记状态
     */
    void mark();
}


