package com.fr.design.mainframe.share.encrypt.clipboard.impl;

import com.fr.base.io.BaseBook;
import com.fr.base.iofile.attr.ExtendSharableAttrMark;
import com.fr.design.designer.beans.models.SelectionModel;
import com.fr.design.designer.creator.XCreator;
import com.fr.design.designer.creator.XCreatorUtils;
import com.fr.design.designer.creator.XLayoutContainer;
import com.fr.design.file.HistoryTemplateListCache;
import com.fr.design.mainframe.FormDesigner;
import com.fr.design.mainframe.FormSelection;
import com.fr.design.mainframe.WidgetPropertyPane;
import com.fr.form.main.Form;
import com.fr.design.mainframe.share.encrypt.clipboard.AbstractCrossClipBoardState;
import com.fr.base.iofile.attr.EncryptSharableAttrMark;
import com.fr.form.ui.AbstractBorderStyleWidget;
import com.fr.form.ui.container.WLayout;
import com.fr.stable.StringUtils;
import com.fr.stable.fun.IOFileAttrMark;
import org.jetbrains.annotations.Nullable;

import java.util.concurrent.atomic.AtomicReference;

/**
 * 只需要看是否跨布局去粘贴。
 * <p>
 * created by Harrison on 2020/06/04
 **/
public class CrossLayoutClipBoardState extends AbstractCrossClipBoardState {
    
    @Override
    protected String currentId() {
        //默认id
        final AtomicReference<String> finalId = new AtomicReference<>(StringUtils.EMPTY);
        WidgetPropertyPane pane = WidgetPropertyPane.getInstance();
        FormDesigner designer = pane.getEditingFormDesigner();
        if (designer == null) {
            return null;
        }
        Form target = designer.getTarget();
        BaseBook current = HistoryTemplateListCache.getInstance().getCurrentEditingTemplate().getTarget();
        //如果不是同一个
        if (!StringUtils.equals(target.getTemplateID(), current.getTemplateID())) {
            return null;
        }
        SelectionModel selectionModel = designer.getSelectionModel();
        if (selectionModel == null) {
            return null;
        }
        FormSelection selection = selectionModel.getSelection();
        if (selection == null) {
            return null;
        }
        XCreator[] xCreators = selection.getSelectedCreators();
        if (xCreators != null) {
            XCreator xCreator = xCreators[0];
            if (StringUtils.isEmpty(finalId.get())) {
                XLayoutContainer layout = XCreatorUtils.getParentXLayoutContainer(xCreator);
                if (layout != null) {
                    WLayout wLayout = layout.toData();
                    String encryptWidgetId = findEncryptWidgetId(wLayout);
                    boolean hasId = StringUtils.isNotEmpty(encryptWidgetId);
                    if (hasId) {
                        finalId.set(encryptWidgetId);
                    }
                }
            }
            
        }
        return finalId.get();
    }
    
    @Nullable
    private String findEncryptWidgetId(AbstractBorderStyleWidget widget) {
        
        IOFileAttrMark widgetAttrMark = widget.getWidgetAttrMark(EncryptSharableAttrMark.XML_TAG);
        boolean isEncrypt = widgetAttrMark != null;
        if (isEncrypt) {
            ExtendSharableAttrMark sharableAttrMark = widget.getWidgetAttrMark(ExtendSharableAttrMark.XML_TAG);
            if (sharableAttrMark != null) {
                return (sharableAttrMark.getShareId());
            }
        }
        return null;
    }
}
