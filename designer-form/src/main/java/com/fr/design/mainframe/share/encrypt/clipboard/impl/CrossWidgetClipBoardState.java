package com.fr.design.mainframe.share.encrypt.clipboard.impl;

import com.fr.base.io.BaseBook;
import com.fr.base.iofile.attr.ExtendSharableAttrMark;
import com.fr.design.designer.beans.models.SelectionModel;
import com.fr.design.file.HistoryTemplateListCache;
import com.fr.design.mainframe.FormDesigner;
import com.fr.design.mainframe.FormSelection;
import com.fr.design.mainframe.WidgetPropertyPane;
import com.fr.design.mainframe.share.encrypt.clipboard.AbstractCrossClipBoardState;
import com.fr.form.main.Form;
import com.fr.form.main.WidgetUtil;
import com.fr.base.iofile.attr.EncryptSharableAttrMark;
import com.fr.form.ui.AbstractBorderStyleWidget;
import com.fr.form.ui.Widget;
import com.fr.stable.StringUtils;
import com.fr.stable.fun.IOFileAttrMark;

/**
 * created by Harrison on 2020/05/18
 **/
public class CrossWidgetClipBoardState extends AbstractCrossClipBoardState {
    
    @Override
    protected String currentId() {
        
        final String[] finalIds = new String[]{null};
        WidgetPropertyPane pane = WidgetPropertyPane.getInstance();
        FormDesigner designer = pane.getEditingFormDesigner();
        if (designer == null) {
            return null;
        }
        Form target = designer.getTarget();
        BaseBook current = HistoryTemplateListCache.getInstance().getCurrentEditingTemplate().getTarget();
        //如果不是同一个
        if (!StringUtils.equals(target.getTemplateID(), current.getTemplateID())) {
            return null;
        }
        SelectionModel selectionModel = designer.getSelectionModel();
        if (selectionModel == null) {
            return null;
        }
        FormSelection selection = selectionModel.getSelection();
        if (selection == null) {
            return null;
        }
        Widget[] selectedWidgets = selection.getSelectedWidgets();
        if (selectedWidgets != null && selectedWidgets.length == 1) {
            final Widget selectedWidget = selectedWidgets[0];
            WidgetUtil.bfsTraversalWidget(selectedWidget, new WidgetUtil.BfsWidgetGather<AbstractBorderStyleWidget>() {
                @Override
                public boolean dealWith(AbstractBorderStyleWidget widget) {
                    
                    IOFileAttrMark widgetAttrMark = widget.getWidgetAttrMark(EncryptSharableAttrMark.XML_TAG);
                    boolean isEncrypt = widgetAttrMark != null;
                    if (isEncrypt) {
                        ExtendSharableAttrMark sharableAttrMark = widget.getWidgetAttrMark(ExtendSharableAttrMark.XML_TAG);
                        if (sharableAttrMark != null) {
                            finalIds[0] = sharableAttrMark.getShareId();
                        }
                    }
                    return isEncrypt;
                }
            }, AbstractBorderStyleWidget.class);
        }
        return finalIds[0];
    }
}
