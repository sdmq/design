package com.fr.design.mainframe.share.encrypt.clipboard.impl;

import com.fr.design.mainframe.share.encrypt.clipboard.CrossClipboardHandler;

import java.awt.datatransfer.Transferable;

/**
 * 单元格
 * <p>
 * created by Harrison on 2020/05/18
 **/
public class EncryptTransferableClipboardHandler extends CrossClipboardHandler<Transferable> {
    private static EncryptTransferableClipboardHandler transferableClipboardHandler;

    public static EncryptTransferableClipboardHandler getInstance() {
        if (transferableClipboardHandler == null) {
            transferableClipboardHandler = new EncryptTransferableClipboardHandler();
        }
        return transferableClipboardHandler;
    }
    
    public EncryptTransferableClipboardHandler() {
        
        super(new CrossTemplateClipBoardState(), new CrossWidgetClipBoardState());
    }
    
    @Override
    public boolean support(Object selection) {
        
        return selection instanceof Transferable;
    }
}
