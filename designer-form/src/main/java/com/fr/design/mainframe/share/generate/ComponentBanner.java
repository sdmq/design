package com.fr.design.mainframe.share.generate;

/**
 * created by Harrison on 2020/04/13
 **/
public interface ComponentBanner {
    
    /**
     * 加载文字
     *
     * @return 加载
     */
    String getLoadingText();
}
