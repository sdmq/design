package com.fr.design.mainframe.share.generate;

import com.fr.design.mainframe.JTemplate;
import com.fr.form.share.DefaultSharableWidget;
import com.fr.form.share.bean.ComponentReuBean;
import com.fr.form.ui.Widget;
import com.fr.stable.fun.mark.Immutable;

import java.util.Map;

/**
 * Created by kerry on 2020-07-30
 */
public interface ComponentCreatorProcessor extends ComponentBanner, Immutable {

    String MARK_STRING = "ComponentCreatorProcessor";
    int CURRENT_LEVEL = 1;

    ComponentReuBean create(JTemplate<?, ?> jt, Map<String, Object> paraMap, Widget widget, DefaultSharableWidget info) throws Exception;
}
