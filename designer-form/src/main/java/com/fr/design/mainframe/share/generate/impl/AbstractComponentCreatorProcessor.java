package com.fr.design.mainframe.share.generate.impl;

import com.fr.design.mainframe.share.generate.ComponentCreatorProcessor;
import com.fr.stable.fun.mark.API;

/**
 * Created by kerry on 2020-07-30
 */
@API(level = ComponentCreatorProcessor.CURRENT_LEVEL)
public abstract class AbstractComponentCreatorProcessor implements ComponentCreatorProcessor {


    @Override
    public int currentAPILevel() {
        return CURRENT_LEVEL;
    }

    @Override
    public int layerIndex() {
        return DEFAULT_LAYER_INDEX;
    }


}
