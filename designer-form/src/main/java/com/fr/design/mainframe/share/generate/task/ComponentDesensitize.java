package com.fr.design.mainframe.share.generate.task;

import com.fr.design.i18n.Toolkit;
import com.fr.design.mainframe.share.generate.ComponentBanner;
import com.fr.form.share.exception.NetWorkFailedException;
import com.fr.general.http.HttpRequest;
import com.fr.general.http.HttpToolbox;
import com.fr.log.FineLoggerFactory;
import com.fr.third.org.apache.http.HttpStatus;
import com.fr.third.org.apache.http.client.methods.CloseableHttpResponse;

/**
 * created by Harrison on 2020/04/22
 **/
public class ComponentDesensitize implements ComponentBanner {
    
    private static final String TEST_URL = "http://www.baidu.com";
    
    public void execute() {
    
        if (!testNet()) {
            throw new NetWorkFailedException();
        }
    }
    
    private boolean testNet() {
        
        try {
            HttpRequest httpRequest = HttpRequest.custom().url(TEST_URL).build();
            CloseableHttpResponse response = HttpToolbox.execute(httpRequest);
            if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
                return true;
            }
        } catch (Exception e) {
            FineLoggerFactory.getLogger().error(e.getMessage(), e);
        }
        return false;
    }
    
    @Override
    public String getLoadingText() {
        return Toolkit.i18nText("Fine-Design_Share_Desensitize");
    }
}
