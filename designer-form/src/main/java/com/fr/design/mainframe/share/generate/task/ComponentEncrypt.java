package com.fr.design.mainframe.share.generate.task;

import com.fr.design.i18n.Toolkit;
import com.fr.design.mainframe.share.generate.ComponentBanner;
import com.fr.form.share.bean.ComponentReuBean;
import com.fr.workspace.WorkContext;

/**
 * created by Harrison on 2020/04/22
 **/
public class ComponentEncrypt implements ComponentBanner {
    
    public ComponentReuBean execute(ComponentReuBean bean) throws Exception {
    
        try {
            return bean;
        } catch (Exception e) {
            String path = bean.getPath();
            WorkContext.getWorkResource().delete(path);
            throw e;
        }
    }
    
    @Override
    public String getLoadingText() {
        return Toolkit.i18nText("Fine-Design_Share_Encrypt");
    }
}
