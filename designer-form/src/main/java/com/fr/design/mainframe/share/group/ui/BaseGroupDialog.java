package com.fr.design.mainframe.share.group.ui;

import com.fr.base.BaseUtils;
import com.fr.design.dialog.UIDialog;
import com.fr.design.gui.ibutton.UIButton;
import com.fr.design.i18n.Toolkit;
import com.fr.design.utils.gui.GUICoreUtils;

import javax.swing.JDialog;
import java.awt.Dimension;
import java.awt.Frame;

/**
 * @Author: Yuan.Wang
 * @Date: 2020/12/9
 */
abstract public class BaseGroupDialog extends UIDialog {
    public BaseGroupDialog(Frame parent) {
        super(parent);
    }

    protected UIButton createConfirmButton() {
        UIButton confirmButton;

        confirmButton = new UIButton(Toolkit.i18nText("Fine-Design_Basic_Confirm"));
        confirmButton.setPreferredSize(new Dimension(60, 25));
        confirmButton.setEnabled(false);
        confirmButton.addActionListener(e -> confirmClose());
        return confirmButton;
    }

    protected UIButton createCancelButton() {
        UIButton cancelButton = new UIButton(Toolkit.i18nText("Fine-Design_Basic_Cancel"));
        cancelButton.setPreferredSize(new Dimension(60, 25));

        cancelButton.addActionListener(e -> dispose());
        return cancelButton;
    }

    /**
     * 点击确定后的处理方法
     */
    abstract protected void confirmClose();

    protected void initStyle() {
        this.setSize(340, 180);
        this.setResizable(false);
        this.setAlwaysOnTop(true);
        this.setIconImage(BaseUtils.readImage("/com/fr/base/images/oem/logo.png"));
        this.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
        GUICoreUtils.centerWindow(this);
        this.setVisible(true);
    }

    @Override
    public void checkValid() throws Exception {
        // do nothing
    }
}
