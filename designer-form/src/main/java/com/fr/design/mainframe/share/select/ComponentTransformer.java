package com.fr.design.mainframe.share.select;

import com.fr.design.designer.creator.XCreator;
import com.fr.design.mainframe.FormSelection;
import com.fr.form.ui.Widget;
import com.fr.third.org.apache.commons.lang3.tuple.Triple;

import java.awt.Rectangle;

/**
 * 组件选择器
 * <p>
 * created by Harrison on 2020/06/11
 **/
public interface ComponentTransformer {
    
    /**
     * 获取选择到组件
     *
     * @return 组件相关信息
     */
    Triple<Widget, XCreator, Rectangle> transform(FormSelection selection);
    
}

