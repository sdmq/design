package com.fr.design.mainframe.share.sort;

import com.fr.design.i18n.Toolkit;
import com.fr.form.share.bean.OnlineShareWidget;
import com.fr.form.share.bean.SortParameter;
import com.fr.form.share.utils.ShareUtils;
import com.fr.general.ComparatorUtils;
import com.fr.general.GeneralContext;

import java.text.Collator;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Date;
import java.util.Map;


/**
 * Created by kerry on 2020-10-22
 */
public enum OnlineWidgetSortType implements SortType<OnlineShareWidget> {
    COMPOSITE {
        @Override
        public void sort(OnlineShareWidget[] widgetProviders) {
            Map<String, SortParameter> parameterMap = ShareUtils.getCompositeSortPara();
            Arrays.sort(widgetProviders, new Comparator<OnlineShareWidget>() {
                @Override
                public int compare(OnlineShareWidget o1, OnlineShareWidget o2) {
                    double t1 = getSortValue(o1, parameterMap);
                    double t2 = getSortValue(o2, parameterMap);
                    // Comparator中比较double/float相等时，不允许误差. 否则会违背 "若x = y , y = z, 则x = z"的约定。
                    // 因为允许误差的情况下，x和y间的误差在允许范围内，被判定相等，y和间的误差在允许范围内，被判定相等，
                    // 但x和z间的误差可能超出允许范围，从而不相等，因此会违背上述约定。
                    // 产生IllegalArgumentException: Comparison method violates its general contract!
                    return Double.compare(t2, t1);
                }
            });
        }

        private double getSortValue(OnlineShareWidget o, Map<String, SortParameter> parameterMap) {
            double a1 = getParaValue("a1", parameterMap),
                    a2 = getParaValue("a2", parameterMap),
                    a3 = getParaValue("a3", parameterMap),
                    b1 = getParaValue("b1", parameterMap),
                    b2 = getParaValue("b2", parameterMap),
                    k = getParaValue("k", parameterMap),
                    q1 = getParaValue("q1", parameterMap),
                    q2 = getParaValue("q2", parameterMap);
            String[] hot2 = new String[]{"report-1", "report-2"};
            String[] hot1 = new String[]{"chart-1", "chart-2", "chart-3", "chart-4", "chart-5", "chart-6"};
            int commentNumber = o.getCommentNumber(), weight = o.getWeight(), downloadTimes = o.getDownloadTimes();

            double hotValue = a3;
            String cid = o.getCid();
            for (String str2 : hot2) {
                if (ComparatorUtils.equals(str2, cid)) {
                    hotValue = a2;
                    break;
                }
            }
            for (String str1 : hot1) {
                if (ComparatorUtils.equals(str1, cid)) {
                    hotValue = a1;
                    break;
                }
            }

            double t = (new Date().getTime() - o.getUploadTime().getTime()) / (3600 * 1000 * 24D);
            return q1 * hotValue * Math.pow(2.718, (0 - k) * t) + q2 * (b1 * commentNumber + b2 * downloadTimes) * Math.pow(2.718, (0 - k) * t) + weight;
        }

        private double getParaValue(String parameter, Map<String, SortParameter> parameterMap) {
            SortParameter sortParameter = parameterMap.get(parameter);
            if (sortParameter == null) {
                return 0.0D;
            }
            return sortParameter.getValue();
        }


        @Override
        public String getDisplayName() {
            return Toolkit.i18nText("Fine-Design_Share_Composite");
        }
    },


    SALES {
        @Override
        public void sort(OnlineShareWidget[] widgetProviders) {
            Arrays.sort(widgetProviders, new Comparator<OnlineShareWidget>() {
                @Override
                public int compare(OnlineShareWidget o1, OnlineShareWidget o2) {
                    int t1 = o1.getDownloadTimes();
                    int t2 = o2.getDownloadTimes();
                    int result = ComparatorUtils.compareCommonType(t2, t1);
                    if (result == 0) {
                        result = Collator.getInstance(GeneralContext.getLocale()).compare(o1.getName(), o2.getName());
                    }
                    return result;
                }
            });
        }

        @Override
        public String getDisplayName() {
            return Toolkit.i18nText("Fine-Design_Share_Sale");
        }
    },

    NEW_PRODUCT {
        @Override
        public void sort(OnlineShareWidget[] widgetProviders) {
            Arrays.sort(widgetProviders, new Comparator<OnlineShareWidget>() {
                @Override
                public int compare(OnlineShareWidget o1, OnlineShareWidget o2) {
                    long t1 = o1.getUploadTime().getTime();
                    long t2 = o2.getUploadTime().getTime();
                    int result = ComparatorUtils.compareCommonType(t2, t1);
                    if (result == 0) {
                        result = Collator.getInstance(GeneralContext.getLocale()).compare(o1.getName(), o2.getName());
                    }
                    return result;
                }
            });
        }

        @Override
        public String getDisplayName() {
            return Toolkit.i18nText("Fine-Design_Share_New_Product");
        }
    }
    //现在不用，以后可能会用，先注释掉
    /*,
    PRICE {
        @Override
        public void sort(OnlineShareWidget[] widgetProviders) {
            Arrays.sort(widgetProviders, new Comparator<OnlineShareWidget>() {
                @Override
                public int compare(OnlineShareWidget o1, OnlineShareWidget o2) {
                    double t1 = o1.getPrice();
                    double t2 = o2.getPrice();
                    int result = ComparatorUtils.compareCommonType(t2, t1);
                    if (result == 0) {
                        result = Collator.getInstance(GeneralContext.getLocale()).compare(o1.getName(), o2.getName());
                    }
                    return result;
                }
            });
        }

        @Override
        public String getDisplayName() {
            return Toolkit.i18nText("Fine-Design_Share_Price");
        }
    };*/

}
