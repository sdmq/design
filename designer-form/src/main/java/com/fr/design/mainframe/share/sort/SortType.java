package com.fr.design.mainframe.share.sort;

/**
 * @Author: Yuan.Wang
 * @Date: 2020/11/19
 */
public interface SortType<T> {

    void sort(T[] widgetProviders);

    String getDisplayName();
}
