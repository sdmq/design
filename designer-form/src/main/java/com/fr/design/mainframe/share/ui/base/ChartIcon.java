package com.fr.design.mainframe.share.ui.base;

import com.fr.base.chart.BaseChartCollection;
import com.fr.base.chart.BaseChartPainter;
import com.fr.base.chart.chartdata.CallbackEvent;
import com.fr.base.chart.result.WebChartIDInfo;
import com.fr.script.Calculator;

import javax.swing.Icon;
import java.awt.Color;
import java.awt.Component;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Paint;

public class ChartIcon implements Icon {
    private BaseChartCollection chartCollection;
    private CallbackEvent callbackEvent;

    private int width;
    private int height;

    /**
     * 构造Chart的缩略图Icon
     */
    public ChartIcon(BaseChartCollection chartCollection, int width, int height) {
        try {
            this.chartCollection = (BaseChartCollection) chartCollection.clone();
        } catch (CloneNotSupportedException e) {
            this.chartCollection = chartCollection;
        }
        this.width = width;
        this.height = height;
    }

    public void registerCallBackEvent(CallbackEvent callbackEvent) {
        this.callbackEvent = callbackEvent;
    }

    /**
     * 画出缩略图Icon
     *
     * @param g 图形的上下文
     * @param c 所在的Component
     * @param x 缩略图的起始坐标x
     * @param y 缩略图的起始坐标y
     */
    @Override
    public void paintIcon(Component c, Graphics g, int x, int y) {

        BaseChartPainter chartPainter = getChartPainter();

        Graphics2D g2d = (Graphics2D) g;
        Paint oldPaint = g2d.getPaint();
        g.translate(x, y);
        g2d.setPaint(Color.white);
        g2d.fillRect(0, 0, getIconWidth(), getIconHeight());

        chartPainter.paint(g2d, getIconWidth(), getIconHeight(), 0, null, callbackEvent);

        g.translate(-x, -y);
        g2d.setPaint(oldPaint);
    }

    protected BaseChartPainter getChartPainter() {
        BaseChartPainter painter = chartCollection.createResultChartPainterWithOutDealFormula(Calculator.createCalculator(),
                WebChartIDInfo.createEmptyDesignerInfo(), getIconWidth(), getIconHeight());
        return painter;
    }


    /**
     * 返回缩略图的宽度
     *
     * @return int 缩略图宽度
     */
    @Override
    public int getIconWidth() {
        return width;
    }

    /**
     * 返回缩略图的高度
     *
     * @return int 缩略图高度
     */
    @Override
    public int getIconHeight() {
        return height;
    }
}
