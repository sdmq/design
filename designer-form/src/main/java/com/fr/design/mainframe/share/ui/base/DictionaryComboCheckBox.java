package com.fr.design.mainframe.share.ui.base;

import com.fr.design.gui.icombocheckbox.UIComboCheckBox;
import com.fr.design.gui.itextfield.UITextField;
import com.fr.design.i18n.Toolkit;
import com.fr.stable.StringUtils;

import java.awt.Component;
import java.util.ArrayList;
import java.util.List;

/**
 * created by Harrison on 2020/04/21
 **/
public class DictionaryComboCheckBox extends UIComboCheckBox {
    
    private static final String DEFAULT_VALUE_SEPARATOR = ",";
    
    private static final String EDITOR_FIELD = "editor";
    
    private Object[] values;
    
    private Object[] displays;
    
    private String locale;
    
    private UITextField newEditor = null;
    
    public DictionaryComboCheckBox(Object[] values, String[] displays, String locale) {
        super(displays);
        init(values, displays);
        this.locale = locale;
        
        installUI();
    }
    
    private void init(Object[] keys, String[] displays) {
    
        this.displays = displays;
        this.values = keys;
    }
    
    /**
     * 这里是有问题的， 这里本身直接获取比较好。
     */
    @Deprecated
    private void installUI() {
    
        Component[] components = getComponents();
        UITextField editor = (UITextField) components[0];
        editor.setPlaceholder(Toolkit.i18nText(locale));
    }
    
    @Override
    public String getText() {
    
        Object[] selectedValues = getSelectedValues();
        StringBuilder builder = new StringBuilder();
        if (selectedValues != null) {
            for (Object value : selectedValues) {
                builder.append(value);
                builder.append(DEFAULT_VALUE_SEPARATOR);
            }
        }
        //去掉末尾多余的逗号
        return builder.length() > 0 ? builder.substring(0, builder.length() - 1) : StringUtils.EMPTY;
    }
    
    @Override
    public Object[] getSelectedValues() {
    
        Object[] selectedValues = super.getSelectedValues();
        List<Object> realValues = new ArrayList<>();
        for (Object selectedValue : selectedValues) {
            Object realValue = matchValue(selectedValue);
            if (realValue != null) {
                realValues.add(realValue);
            }
        }
        return realValues.toArray();
    }
    
    protected Object matchValue(Object key) {
    
        for (int i = 0; i < displays.length; i++) {
            if (displays[i].equals(key)) {
                return values[i];
            }
        }
        return null;
    }
}
