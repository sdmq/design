package com.fr.design.mainframe.share.ui.base;

import com.fr.design.gui.ilable.UILabel;
import com.fr.design.gui.iprogressbar.ModernUIProgressBarUI;
import com.fr.design.i18n.Toolkit;
import com.fr.design.layout.FRGUIPaneFactory;
import com.fr.general.IOUtils;

import javax.swing.BorderFactory;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;

/**
 * @Author: Yuan.Wang
 * @Date: 2020/12/24
 * 下载组件包的时候用到的进度条
 */
public class DownloadProgressPane extends JPanel {
    private static final int MAX_NUM = 1000;

    private final UILabel tipLabel;
    private final UILabel closeLabel;
    private final JProgressBar processBar;

    public DownloadProgressPane(MouseClickListener listener) {
        this.setLayout(new BorderLayout());
        this.setBorder(BorderFactory.createEmptyBorder(200, 25, 0, 25));

        processBar = createProgressBar();
        tipLabel = createTipLabel();

        JPanel panel = new JPanel(new BorderLayout());
        panel.setOpaque(false);
        panel.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
        panel.setPreferredSize(new Dimension(240, 60));
        panel.add(processBar, BorderLayout.CENTER);

        closeLabel = new UILabel(IOUtils.readIcon("/com/fr/base/images/share/close_small.png"));
        closeLabel.addMouseListener(listener);

        JPanel labelPane = FRGUIPaneFactory.createBorderLayout_S_Pane();
        labelPane.setOpaque(false);
        labelPane.add(tipLabel, BorderLayout.CENTER);
        labelPane.add(closeLabel, BorderLayout.EAST);
        labelPane.setBorder(BorderFactory.createEmptyBorder(0, 20, 0, 7));

        UILabel downloadTipLabel = new UILabel(Toolkit.i18nText("Fine-Design_Share_Package_Downloading_Tip"));
        downloadTipLabel.setPreferredSize(new Dimension(240, 90));
        downloadTipLabel.setBorder(BorderFactory.createEmptyBorder(0, 5, 0, 5));
        downloadTipLabel.setForeground(Color.WHITE);
        downloadTipLabel.setHorizontalTextPosition(UILabel.CENTER);


        JPanel centerPane = FRGUIPaneFactory.createBorderLayout_S_Pane();
        centerPane.setPreferredSize(new Dimension(240, 138));
        centerPane.add(labelPane, BorderLayout.NORTH);
        centerPane.add(panel, BorderLayout.CENTER);
        centerPane.add(downloadTipLabel, BorderLayout.SOUTH);
        centerPane.setOpaque(false);

        this.add(centerPane, BorderLayout.NORTH);
        this.setSize(new Dimension(240, 800));
        this.setBackground(new Color(0, 0, 0, 0));
        this.setOpaque(false);
    }

    private UILabel createTipLabel() {
        UILabel label = new UILabel(ProcessState.DOWNLOADING.tip);
        label.setPreferredSize(new Dimension(200, 20));
        label.setHorizontalAlignment(UILabel.CENTER);
        label.setOpaque(false);
        label.setForeground(Color.WHITE);
        return label;
    }

    private JProgressBar createProgressBar() {
        JProgressBar jProgressBar = new JProgressBar();
        jProgressBar.setUI(new ModernUIProgressBarUI());
        jProgressBar.setBorderPainted(false);
        jProgressBar.setOpaque(false);
        jProgressBar.setBorder(null);
        jProgressBar.setMaximum(MAX_NUM);
        return jProgressBar;
    }

    public void changeState() {
        tipLabel.setText(ProcessState.INSTALLING.tip);
        closeLabel.setVisible(false);
        this.validate();
        this.repaint();
    }

    public void updateProgress(double process) {
        if (process < 0 || process > 1) {
            throw new IllegalArgumentException();
        }
        processBar.setValue((int) (process * MAX_NUM));
    }


    private enum ProcessState {
        DOWNLOADING(Toolkit.i18nText("Fine-Design_Share_Package_Downloading")),
        INSTALLING(Toolkit.i18nText("Fine-Design_Share_Package_Installing"));

        private final String tip;

        ProcessState(String tip) {
            this.tip = tip;
        }
    }

    public static void main(String[] args) {
        JFrame frame = new JFrame();
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setBounds(600, 200, 240, 500);

        DownloadProgressPane pane = new DownloadProgressPane(null);
        pane.updateProgress(0.5);
        frame.add(pane);
        frame.setVisible(true);

    }
}


