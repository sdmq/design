package com.fr.design.mainframe.share.ui.base;

import javax.swing.JPanel;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Toolkit;

/**
 * Created by kerry on 2020-10-23
 */
public class ImagePanel extends JPanel {


    private Image image;

    public ImagePanel(String imagePath) {
        image = Toolkit.getDefaultToolkit().createImage(ImagePanel.class
                .getResource(imagePath));
    }

    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        if (image != null) {
            g.drawImage(image, 0, 0, 45, 45, this);
        }
    }
}
