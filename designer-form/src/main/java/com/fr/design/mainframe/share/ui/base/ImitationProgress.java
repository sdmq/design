package com.fr.design.mainframe.share.ui.base;


import java.util.concurrent.atomic.AtomicReference;

/**
 * @Author: Yuan.Wang
 * @Date: 2020/12/23
 */
public class ImitationProgress {
    private final com.fr.design.extra.Process<Double> process;
    private static final double TOTAL_NUM = 1000;
    private static final double FIRST_STAGE_RATE = 0.8;
    private static final int SLEEP_TIME = 600;

    private final AtomicReference<ImitationState> state = new AtomicReference<>(ImitationState.NEW);

    private final int num;
    private final double initSpeed;
    private int currentProgressRate = 0;

    public ImitationProgress(com.fr.design.extra.Process<Double> process, int num) {
        this.process = process;
        this.num = num;
        initSpeed = TOTAL_NUM * FIRST_STAGE_RATE / num;
    }

    public void start() {
        if (!state.compareAndSet(ImitationState.NEW, ImitationState.FIRST_STAGE)) {
            return;
        }
        firstState();
        if (state.compareAndSet(ImitationState.FIRST_STAGE, ImitationState.SECOND_STATE)) {
            secondState();
        }
        if (state.get() == ImitationState.COMPLETE) {
            thirdState();
        }
    }

    /**
     * 按照预先估计的速度跑完80%
     */
    private void firstState() {
        int i = 0;
        for (; i < num; i++) {
            try {
                Thread.sleep(SLEEP_TIME);
                currentProgressRate += initSpeed;
                process.process(currentProgressRate / TOTAL_NUM);
                if (state.get() != ImitationState.FIRST_STAGE) {
                    return;
                }
            } catch (InterruptedException ignore) {
            }
        }
    }

    /**
     * 第一阶段结束但是还没有下载完，则减慢速度跑第二阶段
     */
    private void secondState() {
        double speed = TOTAL_NUM * 0.1 / 30;
        //70%-90%,30s
        int i = 0;
        for (; i < 30; i++) {
            try {
                Thread.sleep(1000);
                currentProgressRate += speed;
                process.process(currentProgressRate / (TOTAL_NUM));
                if (state.get() != ImitationState.SECOND_STATE) {
                    return;
                }
            } catch (InterruptedException ignore) {
                return;
            }
        }
        //90%-95%,三分钟
        speed = TOTAL_NUM * 0.05 / 60;
        for (i = 0; i < 60; i++) {
            try {
                Thread.sleep(3000);
                currentProgressRate += speed;
                process.process(currentProgressRate / (TOTAL_NUM));
            } catch (InterruptedException ignore) {
                return;
            }
        }
        //线程睡眠1h
        try {
            Thread.sleep(1000 * 3600);
        } catch (InterruptedException ignore) {
        }
    }


    /**
     * 下载完，则跑第三阶段，即1s内跑完剩下所有进度
     */
    private void thirdState() {
        int localSpeed = (int) (TOTAL_NUM - currentProgressRate) / 10;
        for (int i = 0; i < 10; i++) {
            try {
                Thread.sleep(100);
                currentProgressRate += localSpeed;
                process.process((currentProgressRate) / TOTAL_NUM);
            } catch (InterruptedException ignore) {
                return;
            }
        }

    }

    public void completed() {
        state.set(ImitationState.COMPLETE);
    }

    public void stop() {
        state.set(ImitationState.STOP);
    }

    private enum ImitationState {
        NEW,
        FIRST_STAGE,
        SECOND_STATE,
        COMPLETE,
        STOP
    }
}
