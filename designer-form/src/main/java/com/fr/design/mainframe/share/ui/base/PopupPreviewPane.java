package com.fr.design.mainframe.share.ui.base;

import com.fr.design.constants.UIConstants;
import com.fr.general.IOUtils;

import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Toolkit;

/**
 * Created by kerry on 2020-06-23
 */
public class PopupPreviewPane extends JPopupMenu {
    private Container contentPane;
    private Image compImage;
    private static final int WIDTH = 400;
    private static final int STANDARD_DPI = 128;
    private static final int MAX_HEIGHT = 400;
    private static final int HEIGHT = 210;

    public PopupPreviewPane() {
        setFocusable(false);
        contentPane = new JPanel();
        contentPane.setBackground(Color.white);
        this.setLayout(new BorderLayout());
        this.add(contentPane, BorderLayout.CENTER);
        this.setOpaque(false);
        setPreferredSize(new Dimension(WIDTH, MAX_HEIGHT));
        setBorder(BorderFactory.createLineBorder(UIConstants.LINE_COLOR));
    }

    public void setComp(Image compImage) {
        try {
            this.compImage = compImage;
            this.updateSize();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void paint(Graphics g) {
        super.paint(g);
        if (compImage != null) {
            g.drawImage(compImage, 0, 0, getWidth(), getHeight(), null);
        }
    }

    @Override
    public void setVisible(boolean visible) {
        super.setVisible(visible);
    }

    public void menuSelectionChanged(boolean isIncluded) {
    }


    // 根据控件内容，更新弹出框大小
    private void updateSize() {
        int dpi = Toolkit.getDefaultToolkit().getScreenResolution();
        int width;
        int height;
        if (compImage == null) {
            compImage = IOUtils.readImage("com/fr/base/images/share/component_error.png");
            width = WIDTH;
            height = HEIGHT;
        } else {
            width = compImage.getWidth(null);
            height = compImage.getHeight(null);
        }
        double aspectRatio = (double) width / height;
        width = (WIDTH * dpi) / STANDARD_DPI;
        height = (int) (width / aspectRatio);
        this.setPreferredSize(new Dimension(width, height));
    }
}
