package com.fr.design.mainframe.share.ui.block;

import com.fr.form.share.utils.ShareUtils;
import com.fr.form.ui.Widget;
import com.fr.general.ComparatorUtils;
import org.jetbrains.annotations.NotNull;

import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.dnd.DragGestureEvent;
import java.awt.dnd.DragGestureListener;
import java.awt.dnd.DragSource;
import java.awt.dnd.DragSourceAdapter;
import java.awt.dnd.DragSourceDragEvent;

/**
 * @Author: Yuan.Wang
 * @Date: 2021/1/15
 */
public class DragAndDropDragGestureListener extends DragSourceAdapter implements DragGestureListener {
    private final PreviewWidgetBlock<?> block;

    public DragAndDropDragGestureListener(PreviewWidgetBlock<?> tt, int actions) {
        block = tt;
        DragSource source = new DragSource();
        source.createDefaultDragGestureRecognizer(tt, actions, this);
    }

    public void dragGestureRecognized(DragGestureEvent dge) {
        PreviewWidgetBlock<?> onlineWidgetBlock = (PreviewWidgetBlock<?>) dge.getComponent();
        if (onlineWidgetBlock != null) {
            String uuid = block.getWidgetUuid();
            Widget widget = ShareUtils.getElCaseEditorById(uuid);
            if (widget != null) {
                DragAndDropTransferable dragAndDropTransferable = new DragAndDropTransferable(widget);
                dge.startDrag(DragSource.DefaultCopyDrop, dragAndDropTransferable, this);
            }
        }
    }

    @Override
    public void dragEnter(DragSourceDragEvent dragSourceDragEvent) {

    }

    private static class DragAndDropTransferable implements Transferable {
        private final Widget widget;

        public DragAndDropTransferable(Widget widget) {
            this.widget = widget;
        }

        DataFlavor[] flavors = {new DataFlavor(Widget.class, "Widget")};

        public DataFlavor[] getTransferDataFlavors() {
            return flavors;
        }

        public boolean isDataFlavorSupported(DataFlavor flavor) {
            for (DataFlavor df : flavors) {
                if (ComparatorUtils.equals(df, flavor)) {
                    return true;
                }
            }
            return false;
        }
        @NotNull
        public Object getTransferData(DataFlavor df) {
            return widget;
        }
    }
}