package com.fr.design.mainframe.share.ui.block;

import com.fr.design.gui.ilable.UILabel;
import com.fr.design.i18n.Toolkit;
import com.fr.design.layout.FRGUIPaneFactory;
import com.fr.design.mainframe.share.ui.base.MouseClickListener;
import com.fr.design.mainframe.share.ui.online.widgetpackage.OnlineWidgetPackageSelectPane;
import com.fr.form.share.bean.OnlineShareWidget;
import com.fr.form.share.constants.ShareComponentConstants;

import javax.swing.JPanel;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.event.MouseEvent;

/**
 * Created by kerry on 2020-10-21
 * 商城组件包块
 */
public class OnlineWidgetPackageBlock extends AbstractOnlineWidgetBlock {
    private static final int IMAGE_HEIGHT = 167;
    private static final Color TEXT_COLOR = Color.decode("#419BF9");

    protected JPanel createSouthPane(final OnlineShareWidget widget, final OnlineWidgetPackageSelectPane parentPane) {
        JPanel southPane = FRGUIPaneFactory.createBorderLayout_S_Pane();
        southPane.setBackground(Color.WHITE);
        UILabel label = new UILabel(widget.getName(), UILabel.CENTER);
        label.setToolTipText(widget.getName());
        southPane.add(label, BorderLayout.CENTER);
        UILabel detailLabel = new UILabel(Toolkit.i18nText("Fine-Design_Share_Online_Package_Detail"));
        detailLabel.setForeground(TEXT_COLOR);
        detailLabel.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        detailLabel.addMouseListener(new MouseClickListener() {
            @Override
            public void mouseClicked(MouseEvent e) {
                parentPane.showWidgetDetailPane(widget.getId());
            }
        });
        southPane.add(detailLabel, BorderLayout.EAST);
        return southPane;
    }

    protected Dimension getCoverDimension() {
        return new Dimension(ShareComponentConstants.SHARE_PACKAGE_BLOCK_WIDTH, IMAGE_HEIGHT);
    }

    public OnlineWidgetPackageBlock(OnlineShareWidget widget, OnlineWidgetPackageSelectPane parentPane) {
        super(widget, parentPane);
        this.setPreferredSize(new Dimension(ShareComponentConstants.SHARE_PACKAGE_BLOCK_WIDTH, ShareComponentConstants.SHARE_PACKAGE_BLOCK_HEIGHT));
        this.add(createSouthPane(widget, parentPane), BorderLayout.SOUTH);
    }

}


