package com.fr.design.mainframe.share.ui.block;

import com.fr.design.mainframe.share.ui.online.OnlineWidgetSelectPane;
import com.fr.form.share.bean.OnlineShareWidget;

/**
 * @Author: Yuan.Wang
 * @Date: 2020/12/22
 * 只能显示，无其他功能的组件块
 */
public class SimpleWidgetBlock extends OnlineWidgetBlock {
    public SimpleWidgetBlock(OnlineShareWidget widget, OnlineWidgetSelectPane parentPane) {
        super(widget, parentPane);
        this.removeListener();
        this.setFocusable(false);
    }
}
