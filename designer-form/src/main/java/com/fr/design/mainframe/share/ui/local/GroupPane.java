package com.fr.design.mainframe.share.ui.local;

import com.fr.design.actions.UpdateAction;
import com.fr.design.constants.UIConstants;
import com.fr.design.dialog.FineJOptionPane;
import com.fr.design.gui.ilable.UILabel;
import com.fr.design.gui.imenu.UIPopupMenu;
import com.fr.design.i18n.Toolkit;
import com.fr.design.layout.FRGUIPaneFactory;
import com.fr.design.mainframe.DesignerContext;
import com.fr.design.mainframe.share.group.ui.GroupFileDialog;
import com.fr.design.mainframe.share.sort.SortType;
import com.fr.design.mainframe.share.sort.WidgetSortType;
import com.fr.design.mainframe.share.ui.base.PopupMenuItem;
import com.fr.design.mainframe.share.ui.widgetfilter.LocalWidgetFilter;
import com.fr.design.mainframe.share.util.ShareUIUtils;
import com.fr.design.utils.gui.GUICoreUtils;
import com.fr.form.share.SharableWidgetProvider;
import com.fr.form.share.group.DefaultShareGroupManager;
import com.fr.form.share.Group;
import com.fr.form.share.record.ShareWidgetInfoManager;
import com.fr.general.IOUtils;
import com.fr.stable.StringUtils;

import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.Icon;
import javax.swing.JPanel;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;

/**
 * @Author: Yuan.Wang
 * @Date: 2020/11/5
 */
class GroupPane extends JPanel {
    private static final Icon downIcon = IOUtils.readIcon("/com/fr/base/images/share/arrow_down.png");
    private static final Icon rightIcon = IOUtils.readIcon("/com/fr/base/images/share/arrow_right.png");
    private static final int DEFAULT_HEIGHT = 24;
    private final Group group;
    private LocalWidgetSelectPane contentPanel;
    private UIHeaderPane headerPanel;
    private final int headHeight;
    //是否展开
    private boolean expendStatus;
    private SharableWidgetProvider[] elCaseBindInfoList;
    private SortType sortType = WidgetSortType.INSTALL_TIME;

    private GroupPane(Group group) {
        this(group, DEFAULT_HEIGHT, group.isDefaultExpend());
    }

    private GroupPane(Group group, boolean expendStatus) {
        this(group, DEFAULT_HEIGHT, expendStatus);
    }

    private GroupPane(Group group, int headHeight, boolean expendStatus) {
        this.group = group;
        this.headHeight = headHeight;
        this.expendStatus = expendStatus;
        initComponents();
    }

    public boolean isComponentEditable() {
        return LocalWidgetRepoPane.getInstance().isEditable();
    }

    private void initComponents() {
        this.setLayout(new BorderLayout());
        this.setOpaque(false);

        headerPanel = new UIHeaderPane(group.getGroupName(), headHeight);
        JPanel head = new JPanel(new BorderLayout());
        head.add(headerPanel, BorderLayout.NORTH);
        head.setPreferredSize(new Dimension(240, 24));
        this.add(head, BorderLayout.NORTH);
        refreshShowPanel();
    }

    public void refreshShowPanel() {
        this.setVisible(true);
        refreshBindInfoList();

        //按照筛选条件进行过滤
        elCaseBindInfoList = LocalWidgetFilter.getInstance().filter(elCaseBindInfoList);
        boolean needExpendGroup = expendStatus || (isFiltering() && elCaseBindInfoList.length > 0);
        if (elCaseBindInfoList.length == 0 && isFiltering()) {
            this.setVisible(false);
        }

        //刷新面板的时候可能还有查询条件，如果有的话走一遍查询的逻辑
        String keyWord = LocalWidgetRepoPane.getInstance().getKeyWord4Searching();
        if (StringUtils.isNotEmpty(keyWord)) {
            searchByKeyword(keyWord);
            return;
        }
        ShareWidgetInfoManager.getInstance().checkInstallCompInfo(elCaseBindInfoList);
        sortType.sort(elCaseBindInfoList);
        reCreateShowPane(elCaseBindInfoList);
        expendGroup(needExpendGroup);
    }

    public void reCreateShowPane(SharableWidgetProvider[] widgets) {
        if (contentPanel != null) {
            contentPanel.hidePreviewPane();
            GroupPane.this.remove(contentPanel);
        }
        contentPanel = new LocalWidgetSelectPane(group, widgets, isComponentEditable());
        GroupPane.this.add(contentPanel, BorderLayout.CENTER);
        headerPanel.setMenuItemVisible(!isComponentEditable());
        validate();
        repaint();
        revalidate();
    }

    private void expendGroup(boolean expendStatus) {
        headerPanel.showExpand(expendStatus);
        contentPanel.setVisible(expendStatus);
    }

    protected void setExpendStatus(boolean expendStatus) {
        //在筛选或者搜索状态，调用这个方法直接返回
        if (isFiltering() || isSearching()) {
            return;
        }
        this.expendStatus = expendStatus;
    }

    public SharableWidgetProvider[] getAllBindInfoList() {
        SharableWidgetProvider[] widgetProviders = group.getAllBindInfoList();
        if (widgetProviders == null) {
            widgetProviders = new SharableWidgetProvider[0];
        }
        sortType.sort(widgetProviders);
        return widgetProviders;
    }

    public void sortWidget(WidgetSortType sortType) {
        this.sortType = sortType;
        sortType.sort(elCaseBindInfoList);
        reCreateShowPane(elCaseBindInfoList);
    }

    public void searchByKeyword(String text) {
        this.setVisible(true);
        SharableWidgetProvider[] sharableWidgetArr = elCaseBindInfoList;
        List<SharableWidgetProvider> widgets = new ArrayList<>();
        if (StringUtils.isNotEmpty(text)) {
            for (SharableWidgetProvider provider : elCaseBindInfoList) {
                if (provider.getName().toLowerCase().contains(text)) {
                    widgets.add(provider);
                }
            }
            sharableWidgetArr = widgets.toArray(new SharableWidgetProvider[widgets.size()]);
        }

        boolean needExpendGroup = expendStatus || (isSearching() && sharableWidgetArr.length > 0);
        if (isSearching() && sharableWidgetArr.length == 0) {
            setVisible(false);
        }
        reCreateShowPane(sharableWidgetArr);
        expendGroup(needExpendGroup);
    }

    public void refreshBindInfoList() {
        elCaseBindInfoList = getAllBindInfoList();
    }

    private boolean isFiltering() {
        return LocalWidgetRepoPane.getInstance().isFiltering();
    }

    private boolean isSearching() {
        return LocalWidgetRepoPane.getInstance().isSearching();
    }

    private class UIHeaderPane extends JPanel {
        private static final long serialVersionUID = 1L;

        private final UILabel arrow;
        private final UILabel menuItem;
        private final UILabel titleLabel;
        private final int headHeight;
        private boolean pressed;
        private boolean hover;

        private final MouseAdapter mouseAdapter = new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                if (e.getButton() == MouseEvent.BUTTON3 && group.isEditable() && !GroupPane.this.isComponentEditable()) {
                    UIPopupMenu popupMenu = new UIPopupMenu();
                    popupMenu.setOnlyText(true);
                    popupMenu.setBackground(UIConstants.DEFAULT_BG_RULER);
                    popupMenu.add(new PopupMenuItem(new RenameAction()));
                    popupMenu.add(new PopupMenuItem(new RemoveAction()));

                    GUICoreUtils.showPopupMenu(popupMenu, UIHeaderPane.this, e.getX(), e.getY());
                }
            }

            @Override
            public void mousePressed(MouseEvent e) {
                pressed = true;
            }

            @Override
            public void mouseReleased(MouseEvent e) {
                if (pressed && hover) {
                    dealClickAction(e);
                }
                pressed = false;
            }

            @Override
            public void mouseEntered(MouseEvent e) {
                hover = true;
            }

            @Override
            public void mouseExited(MouseEvent e) {
                hover = false;
            }
        };

        public UIHeaderPane(String title, int headHeight) {
            this.setLayout(new BorderLayout());
            this.setBorder(BorderFactory.createEmptyBorder(0, 10, 1, 2));
            JPanel panel = FRGUIPaneFactory.createBorderLayout_S_Pane();
            panel.setBackground(Color.decode("#EDEDEE"));
            this.headHeight = headHeight;
            arrow = new UILabel(downIcon);
            arrow.setOpaque(false);
            menuItem = createMenuItem();
            titleLabel = new UILabel(title);
            titleLabel.setOpaque(false);

            JPanel leftPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
            leftPanel.add(arrow);
            leftPanel.add(titleLabel);
            leftPanel.setOpaque(false);

            panel.add(leftPanel, BorderLayout.CENTER);

            if (group.isEditable()) {
                panel.add(menuItem, BorderLayout.EAST);
            }
            add(panel, BorderLayout.CENTER);
            addListener();
        }

        public void showExpand(boolean expend) {
            Icon showIcon = expend ? downIcon : rightIcon;
            arrow.setIcon(showIcon);
        }

        @Override
        public Dimension getPreferredSize() {
            return new Dimension(this.getWidth(), headHeight);
        }

        @Override
        public Dimension getSize() {
            return new Dimension(this.getWidth(), headHeight);
        }

        public UILabel createMenuItem() {
            UILabel label = new UILabel(IOUtils.readIcon("/com/fr/base/images/share/menu_item.png"));
            label.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                    UIPopupMenu popupMenu = new UIPopupMenu();
                    popupMenu.setOnlyText(true);
                    popupMenu.setBackground(UIConstants.DEFAULT_BG_RULER);
                    popupMenu.add(new PopupMenuItem(new RenameAction()));
                    popupMenu.add(new PopupMenuItem(new RemoveAction()));

                    GUICoreUtils.showPopupMenu(popupMenu, menuItem, 0, menuItem.getSize().height);
                }
            });
            label.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 5));
            label.setOpaque(false);
            return label;
        }

        protected void setMenuItemVisible(boolean visible) {
            menuItem.setVisible(visible);
        }

        private void addListener() {
            this.addMouseListener(mouseAdapter);
        }

        /**
         * 由鼠标释放时调用该方法来触发左键点击事件
         */
        private void dealClickAction(MouseEvent e) {
            if (e.getButton() == MouseEvent.BUTTON1) {
                boolean isShow = contentPanel.isShowing();
                contentPanel.setVisible(!isShow);
                GroupPane.this.setExpendStatus(!isShow);
                showExpand(!isShow);

                getParent().validate();
                getParent().repaint();
            }
        }

        private class RenameAction extends UpdateAction {
            public RenameAction() {
                this.putValue(Action.SMALL_ICON, null);
                this.setName(Toolkit.i18nText("Fine-Design_Share_Group_Rename"));
            }

            @Override
            public void actionPerformed(ActionEvent e) {
                new GroupFileDialog(DesignerContext.getDesignerFrame(), Toolkit.i18nText("Fine-Design_Share_Group_Rename", group.getGroupName())) {
                    @Override
                    protected void confirmClose() {
                        this.dispose();
                        String groupName = getFileName().replaceAll("[\\\\/:*?\"<>|]", StringUtils.EMPTY);
                        if (DefaultShareGroupManager.getInstance().renameGroup(group, groupName)) {
                            titleLabel.setText(groupName);
                            repaint();
                        } else {
                            ShareUIUtils.showErrorMessageDialog(Toolkit.i18nText("Fine-Design_Share_Group_Rename_Failure"));
                        }
                    }

                    @Override
                    protected boolean isDuplicate(String fileName) {
                        return DefaultShareGroupManager.getInstance().exist(fileName);
                    }
                };

            }
        }

        private class RemoveAction extends UpdateAction {
            public RemoveAction() {
                this.putValue(Action.SMALL_ICON, null);
                this.setName(Toolkit.i18nText("Fine-Design_Share_Group_Remove"));
            }

            @Override
            public void actionPerformed(ActionEvent e) {
                int rv = FineJOptionPane.showConfirmDialog(
                        DesignerContext.getDesignerFrame(),
                        Toolkit.i18nText("Fine-Design_Share_Group_Remove_Info"),
                        Toolkit.i18nText("Fine-Design_Share_Group_Confirm"),
                        FineJOptionPane.YES_NO_OPTION
                );
                if (rv == FineJOptionPane.YES_OPTION) {
                    if (DefaultShareGroupManager.getInstance().removeGroup(group)) {
                        LocalWidgetRepoPane.getInstance().removeGroup(group.getGroupName());
                    } else {
                        ShareUIUtils.showErrorMessageDialog(Toolkit.i18nText("Fine-Design_Share_Group_Remove_Failure"));
                    }
                }
            }
        }
    }

    public enum GroupCreateStrategy {
        DEFAULT {
            @Override
            public GroupPane creteGroupPane(Group group) {
                return new GroupPane(group);
            }
        },
        CLOSURE {
            @Override
            public GroupPane creteGroupPane(Group group) {
                return new GroupPane(group, false);
            }
        };

        abstract public GroupPane creteGroupPane(Group group);
    }
}
