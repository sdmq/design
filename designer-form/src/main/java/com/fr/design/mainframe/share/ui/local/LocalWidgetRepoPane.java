package com.fr.design.mainframe.share.ui.local;

import com.fr.design.dialog.BasicPane;
import com.fr.design.gui.icontainer.UIScrollPane;
import com.fr.design.i18n.Toolkit;
import com.fr.design.layout.FRGUIPaneFactory;
import com.fr.design.layout.VerticalFlowLayout;
import com.fr.design.mainframe.FormWidgetDetailPane;
import com.fr.design.mainframe.share.sort.WidgetSortType;
import com.fr.design.mainframe.share.ui.widgetfilter.LocalWidgetFilter;
import com.fr.design.mainframe.share.util.InstallComponentHelper;
import com.fr.design.mainframe.share.util.ShareComponentUtils;
import com.fr.form.share.group.DefaultShareGroupManager;
import com.fr.form.share.Group;
import com.fr.log.FineLoggerFactory;
import com.fr.stable.StringUtils;

import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.SwingWorker;
import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * Created by kerry on 2020-10-16
 */
public class LocalWidgetRepoPane extends BasicPane {

    private JPanel centerPane;
    private UIScrollPane groupsScrollPane;
    private JPanel groupsPane;
    private ManagePane managePane;

    private final Map<String, GroupPane> groupPaneMap = new HashMap<>();
    private final AtomicBoolean isRefreshing = new AtomicBoolean(false);
    private boolean editable;
    private CardLayout cardLayout;
    private String keyWord4Searching = StringUtils.EMPTY;

    private LocalWidgetRepoPane() {
        setLayout(FRGUIPaneFactory.createBorderLayout());
        initPane();
    }

    public static LocalWidgetRepoPane getInstance() {
        return HOLDER.singleton;
    }

    private static class HOLDER {
        private static final LocalWidgetRepoPane singleton = new LocalWidgetRepoPane();
    }

    public boolean isEditable() {
        return editable;
    }

    public String getKeyWord4Searching() {
        return keyWord4Searching;
    }

    /**
     * 初始化
     */
    public void initPane() {
        //新用户预装组件
        InstallComponentHelper.installPreComponent();
        JPanel componentLibPanel = FRGUIPaneFactory.createBorderLayout_S_Pane();
        componentLibPanel.setBorder(BorderFactory.createEmptyBorder(0, 0, 15, 0));

        ToolbarPane toolbarPane = new ToolbarPane();
        managePane = new ManagePane();

        JPanel northPane = FRGUIPaneFactory.createBorderLayout_S_Pane();
        northPane.add(toolbarPane, BorderLayout.NORTH);
        northPane.add(managePane, BorderLayout.CENTER);

        cardLayout = new CardLayout();
        centerPane = new JPanel(cardLayout);
        centerPane.add(LocalPaneStatus.NO_MATCH.getPanel(), LocalPaneStatus.NO_MATCH.name());
        centerPane.add(LocalPaneStatus.EMPTY.getPanel(), LocalPaneStatus.EMPTY.name());
        centerPane.add(LocalPaneStatus.LOADING.getPanel(), LocalPaneStatus.LOADING.name());
        centerPane.add(LocalPaneStatus.INSTALLING.getPanel(), LocalPaneStatus.INSTALLING.name());

        componentLibPanel.add(northPane, BorderLayout.NORTH);
        componentLibPanel.add(centerPane, BorderLayout.CENTER);
        add(componentLibPanel, BorderLayout.CENTER);
        cardLayout.show(centerPane, LocalPaneStatus.LOADING.name());
        toolbarPane.addFilterPopupStateChangeListener(state -> setWidgetPaneScrollEnable(!state));

        doRefresh();
    }

    public void refreshAllGroupPane() {
        refreshAllGroupPane(GroupPane.GroupCreateStrategy.DEFAULT);
    }

    public void refreshAllGroupPane(GroupPane.GroupCreateStrategy createStrategy) {
        editable = false;
        groupPaneMap.clear();
        if (groupsScrollPane != null) {
            centerPane.remove(groupsScrollPane);
        }
        groupsPane = FRGUIPaneFactory.createVerticalFlowLayout_Pane(false, VerticalFlowLayout.TOP, 0, 5);

        Group[] groups = DefaultShareGroupManager.getInstance().getAllGroup();
        for (Group group : groups) {
            GroupPane groupPane = createStrategy.creteGroupPane(group);
            groupPaneMap.put(group.getGroupName(), groupPane);
            groupsPane.add(groupPane, BorderLayout.NORTH);
        }
        groupsScrollPane = new UIScrollPane(groupsPane);
        groupsScrollPane.setBorder(null);
        groupsScrollPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);

        centerPane.add(groupsScrollPane, LocalPaneStatus.NORMAL.name());
        switchPane();
    }

    public void refreshPane() {
        managePane.switchPanel(false);
        switchPane(LocalPaneStatus.LOADING);
        doRefresh();
    }

    public void switch2InstallingPane() {
        switchPane(LocalPaneStatus.INSTALLING);
    }

    public void refreshShowPanel(boolean isEdit) {
        this.editable = isEdit;
        refreshShowPanel();
    }

    public void refreshShowPanel() {
        for (GroupPane groupPane : groupPaneMap.values()) {
            groupPane.refreshShowPanel();
        }
        switchPane();
    }

    public void refreshShowPanel(Group group) {
        if (groupPaneMap.containsKey(group.getGroupName())) {
            groupPaneMap.get(group.getGroupName()).refreshShowPanel();
        }
        switchPane();
    }

    public void setWidgetPaneScrollEnable(boolean enable) {
        groupsScrollPane.setWheelScrollingEnabled(enable);
    }

    public void setManageButtonEnable(boolean enable) {
        managePane.setButtonEnable(enable);
    }

    public void removeGroup(String groupName) {
        JPanel jPanel = groupPaneMap.remove(groupName);
        groupsPane.remove(jPanel);
        switchPane();
    }

    public void addGroup(Group group) {
        GroupPane groupPane = GroupPane.GroupCreateStrategy.DEFAULT.creteGroupPane(group);
        groupPaneMap.put(group.getGroupName(), groupPane);
        groupsPane.add(groupPane);
        switchPane();
    }

    public void searchByKeyword(String text) {
        keyWord4Searching = text;
        for (GroupPane groupPane : groupPaneMap.values()) {
            groupPane.searchByKeyword(text);
        }
        switchPane();
    }

    public void sortWidget(WidgetSortType sortType) {
        for (GroupPane groupPane : groupPaneMap.values()) {
            groupPane.sortWidget(sortType);
        }
    }


    private void doRefresh() {
        if (isRefreshing.get()) {
            return;
        }
        new SwingWorker<Boolean, Void>() {
            @Override
            protected Boolean doInBackground() throws Exception {
                if (isRefreshing.compareAndSet(false, true)) {
                    boolean isRefresh = DefaultShareGroupManager.getInstance().refresh();
                    if (isRefresh) {
                        ShareComponentUtils.checkReadMe();
                    }
                    return isRefresh;
                }
                return false;
            }

            @Override
            public void done() {
                try {
                    if (get()) {
                        refreshAllGroupPane();
                    }
                } catch (InterruptedException e) {
                    FineLoggerFactory.getLogger().error(e.getMessage(), e);
                    Thread.currentThread().interrupt();
                } catch (ExecutionException e) {
                    FineLoggerFactory.getLogger().error(e.getMessage(), e);
                    FormWidgetDetailPane.getInstance().switch2Empty();
                } finally {
                    isRefreshing.set(false);
                }
            }
        }.execute();
    }

    /**
     * 切换为要显示的面板
     */
    private void switchPane() {
        switchPane(getStatus());
    }

    private void switchPane(LocalPaneStatus status) {
        cardLayout.show(centerPane, status.name());
        centerPane.validate();
        centerPane.repaint();
    }

    private LocalPaneStatus getStatus() {
        return isEmpty() ? LocalPaneStatus.EMPTY : isNoMatch() ? LocalPaneStatus.NO_MATCH : LocalPaneStatus.NORMAL;
    }

    public boolean isFiltering() {
        return LocalWidgetFilter.getInstance().hasFilter();
    }

    public boolean isSearching() {
        return StringUtils.isNotEmpty(keyWord4Searching);
    }

    private boolean isNoMatch() {
        for (GroupPane groupPane : groupPaneMap.values()) {
            if (groupPane.isVisible()) {
                return false;
            }
        }
        return true;
    }

    private boolean isEmpty() {
        Group[] groups = DefaultShareGroupManager.getInstance().getAllGroup();
        return groups.length == 1 && groups[0].getAllBindInfoList().length == 0;
    }

    @Override
    protected String title4PopupWindow() {
        return Toolkit.i18nText("Fine-Design_Share_Local_Widget");
    }
}
