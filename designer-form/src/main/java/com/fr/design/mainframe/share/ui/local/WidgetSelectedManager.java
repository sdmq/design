package com.fr.design.mainframe.share.ui.local;

import com.fr.form.share.group.DefaultShareGroupManager;
import com.fr.form.share.Group;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * @Author: Yuan.Wang
 * @Date: 2021/1/15
 */
public class WidgetSelectedManager {
    private final Map<String, List<String>> selectedWidgetMap = new HashMap<>();

    private WidgetSelectedManager() {

    }

    private static class Holder {
        private static final WidgetSelectedManager INSTANCE = new WidgetSelectedManager();
    }

    public static WidgetSelectedManager getInstance() {
        return Holder.INSTANCE;
    }

    /**
     * 添加选中组件
     */
    public synchronized void addSelect(String groupName, String uuid) {
        if (selectedWidgetMap.containsKey(groupName)) {
            selectedWidgetMap.get(groupName).add(uuid);
        } else {
            selectedWidgetMap.put(groupName, Stream.of(uuid).collect(Collectors.toList()));
        }
    }

    /**
     * 删除选中组件
     */
    public synchronized void removeSelect(String groupName, String uuid) {
        if (selectedWidgetMap.containsKey(groupName)) {
            selectedWidgetMap.get(groupName).remove(uuid);
        }
    }

    /**
     * 卸载单个选中组件
     */
    public synchronized boolean unInstallSelect(String groupName, String uuid) {
        Group group = DefaultShareGroupManager.getInstance().getGroup(groupName);
        return group != null && group.unInstallSelect(Stream.of(uuid).collect(Collectors.toList()));
    }

    /**
     * 卸载所有选中组件
     */
    public synchronized boolean unInstallSelect() {
        boolean result = true;
        for (String groupName : selectedWidgetMap.keySet()) {
            Group group = DefaultShareGroupManager.getInstance().getGroup(groupName);
            result &= group != null && group.unInstallSelect(selectedWidgetMap.get(groupName));
        }
        selectedWidgetMap.clear();
        return result;
    }

    /**
     * 是否选中某组件
     */
    public synchronized boolean isSelected(String groupName, String uuid) {
        return selectedWidgetMap.containsKey(groupName) && selectedWidgetMap.get(groupName).contains(uuid);
    }

    /**
     * 清空选中组件
     */
    public synchronized void clearSelect() {
        selectedWidgetMap.clear();
    }

    /**
     * 被选中是否为空
     */
    public synchronized boolean isSelectEmpty() {
        return selectedWidgetMap.isEmpty() || selectedWidgetMap.values().stream().noneMatch(list -> (list.size() > 0));
    }

    /**
     * 移动所有选中到某分组
     */
    public synchronized boolean moveSelect(String toGroupName) {
        boolean result = true;
        Group toGroup = DefaultShareGroupManager.getInstance().getGroup(toGroupName);
        if (toGroup == null) {
            return false;
        }
        for (String groupName : selectedWidgetMap.keySet()) {
            Group fromGroup = DefaultShareGroupManager.getInstance().getGroup(groupName);
            result &= fromGroup != null && fromGroup.moveModule(toGroup, selectedWidgetMap.get(groupName));
        }
        selectedWidgetMap.clear();
        return result;
    }

    /**
     * 移动单独组件到某分组
     */
    public synchronized boolean moveSelect(String formGroupName, String toGroupName, String uuid) {
        Group fromGroup = DefaultShareGroupManager.getInstance().getGroup(formGroupName);
        Group toGroup = DefaultShareGroupManager.getInstance().getGroup(toGroupName);
        if (fromGroup == null || toGroup == null) {
            return false;
        }
        return fromGroup.moveModule(toGroup, Stream.of(uuid).collect(Collectors.toList()));
    }
}
