package com.fr.design.mainframe.share.ui.online;

import javax.swing.SwingWorker;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by kerry on 2020-12-10
 */
public class OnlineResourceManager {
    private static class HOLDER {
        private static final OnlineResourceManager singleton = new OnlineResourceManager();
    }

    private OnlineResourceManager(){
        
    }

    public static OnlineResourceManager getInstance() {
        return HOLDER.singleton;
    }

    private SwingWorker<Boolean,Void> swingWorker;

    private final List<ResourceLoader> loaderList = new ArrayList<>();

    public void cancelLoad() {
        if (swingWorker != null) {
            swingWorker.cancel(true);
        }
        this.loaderList.clear();
    }

    public void addLoader(ResourceLoader loader) {
        this.loaderList.add(loader);
    }

    public void loadImage() {
        swingWorker = new SwingWorker<Boolean, Void>() {
            @Override
            protected Boolean doInBackground() {
                for (ResourceLoader loader : loaderList) {
                    loader.load();
                }
                return true;
            }
        };
        swingWorker.execute();

    }


}
