package com.fr.design.mainframe.share.ui.online;

import com.fr.base.BaseUtils;
import com.fr.design.dialog.BasicPane;
import com.fr.design.gui.ilable.UILabel;
import com.fr.design.i18n.Toolkit;
import com.fr.design.layout.FRGUIPaneFactory;
import com.fr.design.mainframe.share.ui.base.LoadingPane;
import com.fr.design.mainframe.share.ui.base.MouseClickListener;
import com.fr.form.share.bean.OnlineShareWidget;
import com.fr.form.share.utils.ShareUtils;
import com.fr.log.FineLoggerFactory;
import com.fr.stable.StringUtils;

import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.SwingWorker;
import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;


/**
 * Created by kerry on 2020-10-16
 */
public class OnlineWidgetRepoPane extends BasicPane {
    private static final String MARKET_URL = "https://market.fanruan.com/reuse";
    private OnlineWidgetTabPane componentTabPane;
    private JPanel centerPane;
    private boolean isShowPackagePanel = false;
    private CardLayout cardLayout;

    enum Status {LOADING, DISCONNECTED, NORMAL}

    public static OnlineWidgetRepoPane getInstance() {
        return OnlineWidgetRepoPane.HOLDER.singleton;
    }

    private static class HOLDER {
        private static final OnlineWidgetRepoPane singleton = new OnlineWidgetRepoPane();
    }

    @Override
    protected String title4PopupWindow() {
        return Toolkit.i18nText("Fine-Design_Share_Online");
    }

    public OnlineWidgetRepoPane() {
        initPane();
    }

    public boolean isShowPackagePanel() {
        return isShowPackagePanel;
    }

    public void setShowPackagePanel(boolean showPackagePanel) {
        isShowPackagePanel = showPackagePanel;
    }

    private void initPane() {
        cardLayout = new CardLayout();
        this.setLayout(cardLayout);
        this.setBorder(BorderFactory.createEmptyBorder(10, 0, 0, 0));
        this.centerPane = FRGUIPaneFactory.createBorderLayout_S_Pane();

        this.add(new LoadingPane(), Status.LOADING.name());
        this.add(this.centerPane, Status.NORMAL.name());
        this.add(createInternetErrorPane(), Status.DISCONNECTED.name());

        switchPane(Status.LOADING);
        synchronizedLoadingContent();
    }

    private void addCenterPane() {
        this.centerPane.removeAll();
        this.centerPane.add(this.componentTabPane, BorderLayout.CENTER);
        this.switchPane(Status.NORMAL);
    }

    public void switch2InternetErrorPane() {
        switchPane(Status.DISCONNECTED);
    }

    private void switchPane(Status status) {
        cardLayout.show(this, status.name());
    }

    private void synchronizedLoadingContent() {
        new SwingWorker<Boolean, Void>() {
            @Override
            protected Boolean doInBackground() throws Exception {
                return initContentPane();
            }

            @Override
            protected void done() {
                try {
                    if (get()) {
                        addCenterPane();
                    }
                } catch (InterruptedException e) {
                    FineLoggerFactory.getLogger().error(e.getMessage(), e);
                    Thread.currentThread().interrupt();
                } catch (ExecutionException e) {
                    FineLoggerFactory.getLogger().error(e.getMessage(), e);
                }
            }
        }.execute();
    }

    private boolean initContentPane() {
        List<OnlineShareWidget> sharableWidgets = new ArrayList<>();
        List<OnlineShareWidget> sharableWidgetPackage = new ArrayList<>();

        try {
            ShareUtils.getAllSharableWidgetsFromShop(sharableWidgets, sharableWidgetPackage);
        } catch (Exception e) {
            FineLoggerFactory.getLogger().error(e.getMessage(), e);
            switchPane(Status.DISCONNECTED);
            return false;
        }
        this.componentTabPane = new OnlineWidgetTabPane(sharableWidgets.toArray(new OnlineShareWidget[sharableWidgets.size()]),
                sharableWidgetPackage.toArray(new OnlineShareWidget[sharableWidgetPackage.size()]));

        return true;
    }

    private void reload() {
        this.removeAll();
        initPane();
        this.validate();
        this.repaint();
    }

    private JPanel createInternetErrorPane() {
        JPanel panel = FRGUIPaneFactory.createVerticalFlowLayout_Pane(true, FlowLayout.LEADING, 0, 5);
        UILabel imagePanel = new UILabel(BaseUtils.readIcon("/com/fr/base/images/share/internet_error.png"));
        imagePanel.setPreferredSize(new Dimension(240, 96));
        imagePanel.setHorizontalAlignment(SwingConstants.CENTER);
        panel.add(imagePanel);
        UILabel uiLabel = tipLabel(Toolkit.i18nText("Fine-Design_Share_Internet_Connect_Failed"));
        uiLabel.setForeground(Color.decode("#8F8F92"));
        UILabel reloadLabel = tipLabel(Toolkit.i18nText("Fine-Design_Share_Online_Reload"));
        reloadLabel.setForeground(Color.decode("#419BF9"));
        reloadLabel.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        reloadLabel.addMouseListener(new MouseClickListener() {
            @Override
            public void mouseClicked(MouseEvent e) {
                super.mouseClicked(e);
                reload();
            }
        });


        UILabel tipLabel1 = tipLabel(Toolkit.i18nText("Fine-Design_Share_Internet_Connect_Failed_Tip1"));
        tipLabel1.setForeground(Color.decode("#8F8F92"));
        UILabel tipLabel2 = tipLabel(Toolkit.i18nText("Fine-Design_Share_Internet_Connect_Failed_Tip2"));
        tipLabel2.setForeground(Color.decode("#8F8F92"));

        JTextField tipLabel3 = new JTextField(MARKET_URL);
        tipLabel3.setHorizontalAlignment(JTextField.CENTER);
        tipLabel3.setPreferredSize(new Dimension(240, 20));
        tipLabel3.setEditable(false);
        tipLabel3.setForeground(Color.decode("#8F8F92"));
        tipLabel3.setBackground(null);
        tipLabel3.setBorder(null);

        UILabel emptyLabel = tipLabel(StringUtils.EMPTY);
        panel.add(uiLabel);
        panel.add(reloadLabel);
        panel.add(emptyLabel);
        panel.add(tipLabel1);
        panel.add(tipLabel2);
        panel.add(tipLabel3);
        panel.setBorder(BorderFactory.createEmptyBorder(240, 0, 0, 0));
        return panel;
    }

    private UILabel tipLabel(String text) {
        UILabel tipLabel = new UILabel(text);
        tipLabel.setHorizontalAlignment(SwingConstants.CENTER);
        tipLabel.setPreferredSize(new Dimension(240, 20));
        return tipLabel;
    }

}
