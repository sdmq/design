package com.fr.design.mainframe.share.ui.online;

import com.fr.design.gui.icontainer.UIScrollPane;
import com.fr.design.layout.FRGUIPaneFactory;
import com.fr.design.mainframe.share.AbstractWidgetSelectPane;
import com.fr.design.mainframe.share.ui.base.LoadingPane;
import com.fr.design.mainframe.share.ui.base.NoMatchPane;
import com.fr.design.mainframe.share.ui.base.PagingFiledPane;
import com.fr.design.mainframe.share.ui.block.OnlineWidgetBlock;
import com.fr.design.mainframe.share.ui.block.PreviewWidgetBlock;
import com.fr.design.mainframe.share.ui.widgetfilter.FilterPane;
import com.fr.form.share.base.DataLoad;
import com.fr.form.share.bean.OnlineShareWidget;
import com.fr.form.share.constants.ShareComponentConstants;
import com.fr.form.share.utils.ShareUtils;
import com.fr.log.FineLoggerFactory;
import com.fr.stable.ArrayUtils;

import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.SwingWorker;
import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.util.concurrent.ExecutionException;

/**
 * Created by kerry on 2020-10-19
 */
public class OnlineWidgetSelectPane extends AbstractWidgetSelectPane {
    protected static final int H_GAP = 5;
    protected static final int V_GAP = 10;

    enum PaneStatue {NORMAL, NO_MATCH, LOADING, DISCONNECTED}

    private OnlineShareWidget[] sharableWidgetProviders;
    private PagingFiledPane pagingFiledPane;
    private JPanel contentPane;
    private UIScrollPane scrollPane;
    private FilterPane filterPane;
    private final int widgetsPerNum;
    private CardLayout cardLayout;

    public OnlineWidgetSelectPane(OnlineShareWidget[] providers, FilterPane filterPane, int widgetsPerNum) {
        this(providers, widgetsPerNum);
        this.filterPane = filterPane;
    }

    public OnlineWidgetSelectPane(final DataLoad<OnlineShareWidget> dataLoad, FilterPane filterPane, final int widgetsPerNum) {
        this(dataLoad, widgetsPerNum);
        this.filterPane = filterPane;
    }

    public OnlineWidgetSelectPane(OnlineShareWidget[] providers, int widgetsPerNum) {
        this.widgetsPerNum = widgetsPerNum;
        sharableWidgetProviders = providers;
        init();
        initPagingPane();
        switchPane(createComponents());
    }

    public OnlineWidgetSelectPane(final DataLoad<OnlineShareWidget> dataLoad, final int widgetsPerNum) {
        this.widgetsPerNum = widgetsPerNum;
        init();
        //异步获取组件信息
        new SwingWorker<PaneStatue, Void>() {
            @Override
            protected PaneStatue doInBackground() {
                sharableWidgetProviders = dataLoad.load();
                initPagingPane();
                return createComponents();
            }

            @Override
            protected void done() {
                try {
                    switchPane(get());
                } catch (InterruptedException e) {
                    FineLoggerFactory.getLogger().error(e.getMessage(), e);
                    Thread.currentThread().interrupt();
                } catch (ExecutionException e) {
                    FineLoggerFactory.getLogger().error(e.getMessage(), e);
                }
            }
        }.execute();
    }

    private void init() {
        cardLayout = new CardLayout();
        this.setLayout(cardLayout);
        // 设置面板的边框 ，距离上、左、下、右 的距离
        this.setBorder(BorderFactory.createEmptyBorder(0, 5, 0, 0));
        contentPane = FRGUIPaneFactory.createBorderLayout_S_Pane();
        this.add(new LoadingPane(), PaneStatue.LOADING.name());
        this.add(new NoMatchPane(), PaneStatue.NO_MATCH.name());
        this.add(contentPane, PaneStatue.NORMAL.name());
        switchPane(PaneStatue.LOADING);
    }

    public int getSharableWidgetNum() {
        return sharableWidgetProviders == null ? 0 : sharableWidgetProviders.length;
    }

    public OnlineShareWidget[] getSharableWidgetProviders() {
        return sharableWidgetProviders;
    }

    /**
     * 切换需要显示的面板
     */
    private void switchPane(PaneStatue statue) {
        if (statue == PaneStatue.DISCONNECTED) {
            OnlineWidgetRepoPane.getInstance().switch2InternetErrorPane();
            return;
        }
        cardLayout.show(this, statue.name());
        if (statue == PaneStatue.NORMAL) {
            //异步加载组件缩略图
            OnlineResourceManager.getInstance().loadImage();
        }
    }

    private void synchronizedLoadingContent() {
        new SwingWorker<PaneStatue, Void>() {
            @Override
            protected PaneStatue doInBackground() {
                return createComponents();
            }

            @Override
            protected void done() {
                try {
                    switchPane(get());
                } catch (InterruptedException e) {
                    FineLoggerFactory.getLogger().error(e.getMessage(), e);
                    Thread.currentThread().interrupt();
                } catch (ExecutionException e) {
                    FineLoggerFactory.getLogger().error(e.getMessage(), e);
                }
            }
        }.execute();
    }

    private void initPagingPane() {
        this.pagingFiledPane = new PagingFiledPane(sharableWidgetProviders.length, widgetsPerNum);
        this.pagingFiledPane.registerChangeListener(event -> {
            OnlineWidgetSelectPane.this.switchPane(PaneStatue.LOADING);
            synchronizedLoadingContent();
        });
        pagingFiledPane.setEnable(pagePaneEnable());
    }

    private PaneStatue createComponents() {
        if (ArrayUtils.isEmpty(sharableWidgetProviders)) {
            return PaneStatue.NO_MATCH;
        }
        if (!ShareUtils.testConnection()) {
            return PaneStatue.DISCONNECTED;
        }
        OnlineResourceManager.getInstance().cancelLoad();

        contentPane.removeAll();
        scrollPane = createScrollPane();

        contentPane.add(scrollPane, BorderLayout.CENTER);
        contentPane.add(this.pagingFiledPane, BorderLayout.SOUTH);
        return PaneStatue.NORMAL;
    }

    private UIScrollPane createScrollPane() {
        OnlineShareWidget[] showWidgets = this.pagingFiledPane.getShowItems(this.sharableWidgetProviders);
        JPanel widgetPane = createWidgetPane();
        widgetPane.setLayout(new FlowLayout(FlowLayout.LEFT, H_GAP, V_GAP));
        for (OnlineShareWidget provider : showWidgets) {
            PreviewWidgetBlock<OnlineShareWidget> widgetButton = createWidgetBlock(provider);
            widgetPane.add(widgetButton);
        }
        widgetPane.setPreferredSize(new Dimension(240, getPaneHeight(showWidgets.length)));

        UIScrollPane scrollPane = new UIScrollPane(createContentPane(widgetPane));
        setScrollPaneStyle(scrollPane);
        return scrollPane;
    }

    protected JPanel createWidgetPane() {
        return new JPanel();
    }

    protected JPanel createContentPane(JPanel widgetPane) {
        JPanel panel = FRGUIPaneFactory.createBorderLayout_S_Pane();
        panel.add(widgetPane, BorderLayout.CENTER);
        return panel;
    }

    protected boolean pagePaneEnable() {
        return true;
    }

    protected void setScrollPaneStyle(UIScrollPane scrollPane) {
        scrollPane.setBorder(null);
        scrollPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
        scrollPane.setWheelScrollingEnabled(filterPane == null || !filterPane.isShowPopup());
    }

    protected PreviewWidgetBlock<OnlineShareWidget> createWidgetBlock(OnlineShareWidget provider) {
        return new OnlineWidgetBlock(provider, this);
    }

    protected int getPaneHeight(int count) {
        return (count + 1) / 2 * (ShareComponentConstants.SHARE_BLOCK_HEIGHT + V_GAP);
    }

    public void setWidgetPaneScrollEnable(boolean enable) {
        if (scrollPane != null) {
            scrollPane.setWheelScrollingEnabled(enable);
        }
    }

    protected Container getParentContainer() {
        return this.getParent();
    }
}
