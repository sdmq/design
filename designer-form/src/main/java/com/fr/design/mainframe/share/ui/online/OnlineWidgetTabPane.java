package com.fr.design.mainframe.share.ui.online;

import com.fr.design.gui.ibutton.UITabGroup;
import com.fr.design.layout.FRGUIPaneFactory;
import com.fr.design.mainframe.share.collect.ComponentCollector;
import com.fr.design.mainframe.share.ui.online.widgetpackage.OnlineWidgetPackagesShowPane;
import com.fr.form.share.bean.OnlineShareWidget;

import javax.swing.BorderFactory;
import javax.swing.JPanel;
import java.awt.BorderLayout;
import java.awt.CardLayout;

/**
 * Created by kerry on 2020-10-19
 */
public class OnlineWidgetTabPane extends JPanel {
    private static final String COMPONENT = com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Share");
    private static final String COMPONENT_PACKAGE = com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Share_Package");
    private CardLayout cardLayout;
    private JPanel centerPane;
    private boolean packagePaneCreated = false;

    public OnlineWidgetTabPane(OnlineShareWidget[] sharableWidgets, OnlineShareWidget[] sharableWidgetPackage) {
        initPane(sharableWidgets, sharableWidgetPackage);
    }

    private void initPane(OnlineShareWidget[] sharableWidgets, OnlineShareWidget[] sharableWidgetPackages) {
        this.setLayout(FRGUIPaneFactory.createBorderLayout());
        this.cardLayout = new CardLayout();
        this.centerPane = new JPanel(cardLayout);
        this.centerPane.add(new OnlineWidgetShowPane(sharableWidgets), COMPONENT);
        //延迟组件包面板的初始化，防止组件面板里组件的缩略图和组件包面板里组件的缩略图一起加载
        UITabGroup headGroup = new UITabGroup(new String[]{COMPONENT, COMPONENT_PACKAGE}) {
            public void tabChanged(int newSelectedIndex) {
                OnlineWidgetRepoPane.getInstance().setShowPackagePanel(newSelectedIndex != 0);
                if (newSelectedIndex == 0) {
                    cardLayout.show(centerPane, COMPONENT);
                } else {
                    ComponentCollector.getInstance().collectCmpPktClick();
                    //延迟组件包面板的初始化，防止组件面板里组件和缩略图和组件包面板里组件的缩略图一起加载
                    if (!packagePaneCreated) {
                        centerPane.add(new OnlineWidgetPackagesShowPane(sharableWidgetPackages), COMPONENT_PACKAGE);
                        packagePaneCreated = true;
                    }
                    cardLayout.show(centerPane, COMPONENT_PACKAGE);
                }
            }
        };
        headGroup.setSelectedIndex(0);
        this.centerPane.setBorder(BorderFactory.createEmptyBorder(10, 0, 0, 0));
        JPanel jPanel = FRGUIPaneFactory.createBorderLayout_S_Pane();
        jPanel.setBorder(BorderFactory.createEmptyBorder(0, 10, 0, 10));
        jPanel.add(headGroup, BorderLayout.CENTER);
        this.add(jPanel, BorderLayout.NORTH);
        this.add(centerPane, BorderLayout.CENTER);
    }
}
