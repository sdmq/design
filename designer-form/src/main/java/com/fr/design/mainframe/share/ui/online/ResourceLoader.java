package com.fr.design.mainframe.share.ui.online;

/**
 * Created by kerry on 2020-12-10
 * todo 后面看看能不能和DataLoad合并起来
 */
public interface ResourceLoader {
    /**
     * 加载资源文件
     */
    void load();

}
