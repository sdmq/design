package com.fr.design.mainframe.share.ui.online.widgetpackage;


import com.fr.design.DesignerEnvManager;
import com.fr.design.dialog.FineJOptionPane;
import com.fr.design.gui.ilable.UILabel;
import com.fr.design.i18n.Toolkit;
import com.fr.design.layout.FRGUIPaneFactory;
import com.fr.design.login.DesignerLoginHelper;
import com.fr.design.login.DesignerLoginSource;
import com.fr.design.mainframe.DesignerContext;
import com.fr.design.mainframe.share.ui.base.MouseClickListener;
import com.fr.design.mainframe.share.ui.online.AbstractOnlineWidgetShowPane;
import com.fr.design.mainframe.share.ui.online.OnlineDownloadPackagePane;
import com.fr.design.mainframe.share.ui.online.OnlineWidgetSelectPane;
import com.fr.design.mainframe.share.ui.widgetfilter.FilterPane;
import com.fr.form.share.base.DataLoad;
import com.fr.form.share.bean.OnlineShareWidget;
import com.fr.form.share.utils.ShareUtils;
import com.fr.stable.StringUtils;

import javax.swing.BorderFactory;
import javax.swing.JPanel;
import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by kerry on 2020-10-19
 */
public class OnlineWidgetPackagesShowPane extends AbstractOnlineWidgetShowPane {
    private static final Color TEXT_COLOR = Color.decode("#419BF9");
    private static final String WIDGETS_INFO = "WIDGETS_INFO";
    private static final String WIDGET_DETAIL = "WIDGET_DETAIL";

    private CardLayout cardLayout;
    private JPanel centerPane;
    private JPanel detailPane;
    private String currentPackageId;
    private OnlineWidgetSelectPane onlineWidgetSelectPane;
    private UILabel downloadLabel;
    private final Map<String, OnlineWidgetSelectPane> cachePanelMap = new HashMap<>();

    public OnlineWidgetPackagesShowPane(OnlineShareWidget[] sharableWidgetProviders) {
        super(sharableWidgetProviders);
    }

    protected JPanel initContentPane() {
        JPanel firstPane = super.initContentPane();
        cardLayout = new CardLayout();
        centerPane = new JPanel(cardLayout);
        centerPane.add(firstPane, WIDGETS_INFO);

        JPanel secondPane = FRGUIPaneFactory.createBorderLayout_S_Pane();
        UILabel backLabel = createLabel(Toolkit.i18nText("Fine-Design_Report_AlphaFine_Back"), new MouseClickListener() {
            @Override
            public void mouseClicked(MouseEvent e) {
                cardLayout.show(centerPane, WIDGETS_INFO);
            }
        });
        downloadLabel = createLabel(Toolkit.i18nText("Fine-Design_Share_Download_All_Component"), new MouseClickListener() {
            @Override
            public void mouseClicked(MouseEvent e) {
                String userName = DesignerEnvManager.getEnvManager().getDesignerLoginUsername();
                if (StringUtils.isEmpty(userName)) {
                    DesignerLoginHelper.showLoginDialog(DesignerLoginSource.NORMAL);
                    return;
                }
                String message = Toolkit.i18nText("Fine-Design_Share_Download_All_Component_Message")
                        + "\n"
                        + Toolkit.i18nText("Fine-Design_Share_Total")
                        + onlineWidgetSelectPane.getSharableWidgetNum()
                        + Toolkit.i18nText("Fine-Design_Share_Piece");
                int returnValue = FineJOptionPane.showConfirmDialog(DesignerContext.getDesignerFrame(), message, Toolkit.i18nText("Fine-Design_Basic_Confirm"), FineJOptionPane.YES_NO_OPTION);
                if (returnValue == FineJOptionPane.OK_OPTION && onlineWidgetSelectPane.getSharableWidgetNum() != 0) {
                    downLoadPackage();
                }
            }
        });

        JPanel downloadPane = FRGUIPaneFactory.createBorderLayout_S_Pane();
        downloadPane.setBorder(BorderFactory.createEmptyBorder(0, 130, 0, 0));
        downloadPane.add(downloadLabel, BorderLayout.CENTER);

        JPanel labelPane = FRGUIPaneFactory.createBorderLayout_S_Pane();
        labelPane.setBorder(BorderFactory.createEmptyBorder(0, 10, 0, 10));
        labelPane.add(backLabel, BorderLayout.WEST);
        labelPane.add(downloadPane, BorderLayout.CENTER);

        secondPane.add(labelPane, BorderLayout.NORTH);
        detailPane = FRGUIPaneFactory.createBorderLayout_S_Pane();
        detailPane.add(new OnlineWidgetSelectPane(new OnlineShareWidget[]{}, 50), BorderLayout.CENTER);
        secondPane.add(detailPane, BorderLayout.CENTER);
        centerPane.add(secondPane, WIDGET_DETAIL);
        cardLayout.show(centerPane, WIDGETS_INFO);
        return centerPane;
    }

    private void downLoadPackage() {
        downloadLabel.setVisible(false);
        detailPane.removeAll();
        OnlineDownloadPackagePane widgetSelectPane = new OnlineDownloadPackagePane(this, onlineWidgetSelectPane.getSharableWidgetProviders(), 50);
        detailPane.add(widgetSelectPane, BorderLayout.CENTER);
        cardLayout.show(centerPane, WIDGET_DETAIL);

        cachePanelMap.put(currentPackageId, widgetSelectPane);

        for (OnlineShareWidget onlineShareWidget : getSharableWidgetProviders()) {
            if (StringUtils.equals(onlineShareWidget.getId(), currentPackageId)) {
                widgetSelectPane.downloadWidget(onlineShareWidget);
                break;
            }
        }
    }

    private UILabel createLabel(String i18nText, MouseClickListener clickListener) {
        UILabel label = new UILabel(i18nText);
        label.setForeground(TEXT_COLOR);
        label.addMouseListener(clickListener);
        label.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        return label;
    }

    protected void initNorthPane(JPanel jPanel, JPanel northPane) {
        //暂时隐藏，以后会展示出来
    }


    protected OnlineWidgetSelectPane createOnlineWidgetSelectPane(OnlineShareWidget[] sharableWidgetProviders) {
        return new OnlineWidgetPackageSelectPane(filterPackageWidget(sharableWidgetProviders), 10, this);
    }

    protected OnlineWidgetSelectPane createOnlineWidgetSelectPane(DataLoad<OnlineShareWidget> dataLoad) {
        return new OnlineWidgetPackageSelectPane(dataLoad, 10, this);
    }

    private OnlineShareWidget[] filterPackageWidget(OnlineShareWidget[] sharableWidgetProviders) {
        List<OnlineShareWidget> onlineShareWidgets = new ArrayList<>();
        for (OnlineShareWidget widget : sharableWidgetProviders) {
            if (widget.isWidgetPackage()) {
                onlineShareWidgets.add(widget);
            }
        }
        return onlineShareWidgets.toArray(new OnlineShareWidget[onlineShareWidgets.size()]);
    }

    public void showWidgetDetailPane(final String id) {
        currentPackageId = id;
        boolean containsCache = cachePanelMap.containsKey(id);
        onlineWidgetSelectPane = containsCache ? cachePanelMap.get(id) : new OnlineWidgetSelectPane(() -> ShareUtils.getPackageWidgets(id), 50);
        downloadLabel.setVisible(!containsCache);
        showWidgetDetailPane(onlineWidgetSelectPane);
    }


    public void resetWidgetDetailPane(String id, final OnlineShareWidget[] onlineShareWidgets) {
        if (StringUtils.equals(id, currentPackageId)) {
            showWidgetDetailPane(new OnlineWidgetSelectPane(onlineShareWidgets, 50));
            setDownloadLabelVisible(true);
        }
        removeCachePane(id);
    }

    private void showWidgetDetailPane(OnlineWidgetSelectPane onlineWidgetSelectPane) {
        detailPane.removeAll();
        detailPane.add(onlineWidgetSelectPane, BorderLayout.CENTER);
        cardLayout.show(centerPane, WIDGET_DETAIL);
    }

    public void setDownloadLabelVisible(boolean visible) {
        downloadLabel.setVisible(visible);
    }

    public String getCurrentPackageId() {
        return currentPackageId;
    }

    public void removeCachePane(String packageId) {
        cachePanelMap.remove(packageId);
    }

    protected FilterPane createFilterPane() {
        return FilterPane.createOnlinePackageFilterPane();
    }
}
