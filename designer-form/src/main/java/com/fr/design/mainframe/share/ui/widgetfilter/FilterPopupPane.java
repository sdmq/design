package com.fr.design.mainframe.share.ui.widgetfilter;

import com.fr.design.gui.icheckbox.UICheckBox;
import com.fr.design.gui.ilable.UILabel;
import com.fr.design.i18n.Toolkit;
import com.fr.design.layout.FRGUIPaneFactory;
import com.fr.design.layout.VerticalFlowLayout;
import com.fr.form.share.bean.WidgetFilterInfo;
import com.fr.form.share.bean.WidgetFilterTypeInfo;
import com.fr.general.ComparatorUtils;
import com.fr.stable.StringUtils;

import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.swing.event.ChangeEvent;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.ArrayList;
import java.util.List;

/**
 * @Author: Yuan.Wang
 * @Date: 2020/11/11
 */
public abstract class FilterPopupPane extends JPanel {
    private static final Color BORDER_COLOR = Color.decode("#D9DADD");
    private static final String FILTER_ALL_ID = "0";

    FilterPane filterPane;
    private List<WidgetFilterInfo> filterList = new ArrayList<>();
    private final List<UICheckBox> checkBoxList = new ArrayList<>();
    private boolean reset = false;


    public FilterPopupPane(FilterPane filterPane, List<WidgetFilterTypeInfo> widgetFilterCategories) {
        this.filterPane = filterPane;
        initPane(widgetFilterCategories);
    }

    private void initPane(List<WidgetFilterTypeInfo> widgetFilterCategories) {
        this.setBackground(Color.WHITE);
        this.setBorder(BorderFactory.createLineBorder(BORDER_COLOR));
        this.setForeground(Color.decode("#FFFFFF"));
        this.setLayout(new BorderLayout());
        this.add(createVerticalFlowPane(widgetFilterCategories), BorderLayout.CENTER);
    }

    public boolean hasFilter() {
        return filterList.size() > 0;
    }

    protected List<WidgetFilterInfo> getFilterList() {
        return filterList;
    }

    private JPanel createVerticalFlowPane(List<WidgetFilterTypeInfo> widgetFilterCategories) {

        List<WidgetFilterTypeInfo> topWidgetTypeFilter = new ArrayList<>();
        List<WidgetFilterTypeInfo> widgetTypeFilter = new ArrayList<>();
        List<WidgetFilterTypeInfo> otherWidgetTypeFilter = new ArrayList<>();

        for (WidgetFilterTypeInfo info : widgetFilterCategories) {
            if (ComparatorUtils.equals(FilterPane.CHART_FILTER_KEY, info.getKey())
                    || ComparatorUtils.equals(FilterPane.REPORT_FILTER_KEY, info.getKey())) {
                widgetTypeFilter.add(info);
            } else if (ComparatorUtils.equals(FilterPane.SOURCE_FILTER_KEY, info.getKey())) {
                topWidgetTypeFilter.add(info);
            } else {
                otherWidgetTypeFilter.add(info);
            }
        }

        JPanel verticalFlowPane = new JPanel();
        verticalFlowPane.setBackground(Color.WHITE);
        VerticalFlowLayout layout = new VerticalFlowLayout(FlowLayout.LEADING, 0, 10);
        layout.setAlignLeft(true);
        verticalFlowPane.setLayout(layout);

        verticalFlowPane.setBackground(Color.WHITE);
        verticalFlowPane.setBorder(BorderFactory.createEmptyBorder(0, 5, 0, 5));

        for (WidgetFilterTypeInfo info : topWidgetTypeFilter) {
            verticalFlowPane.add(createOtherCategoryPane(info));
        }
        if (widgetTypeFilter.size() > 0) {
            verticalFlowPane.add(createWidgetTypeFilterPane(widgetTypeFilter));
        }
        for (WidgetFilterTypeInfo info : otherWidgetTypeFilter) {
            verticalFlowPane.add(createOtherCategoryPane(info));
        }
        return verticalFlowPane;
    }

    private JPanel createWidgetTypeFilterPane(List<WidgetFilterTypeInfo> widgetTypeFilter) {
        JPanel jPanel = FRGUIPaneFactory.createBorderLayout_S_Pane();
        jPanel.setBackground(Color.WHITE);
        UILabel titleLabel = new UILabel(Toolkit.i18nText("Fine-Design_Basic_Type"));
        titleLabel.setBorder(BorderFactory.createEmptyBorder(0, 5, 0, 5));
        titleLabel.setPreferredSize(new Dimension(226, 20));
        titleLabel.setOpaque(true);
        titleLabel.setBackground(Color.decode("#EDEDEE"));
        jPanel.add(titleLabel, BorderLayout.NORTH);

        JPanel contentPane = FRGUIPaneFactory.createVerticalFlowLayout_Pane(true, FlowLayout.LEADING, 0, 10);
        contentPane.setBackground(Color.WHITE);
        for (WidgetFilterTypeInfo info : widgetTypeFilter) {
            contentPane.add(createTypeFilterPane(info));
        }
        jPanel.add(contentPane, BorderLayout.CENTER);
        return jPanel;
    }

    private JPanel createTypeFilterPane(WidgetFilterTypeInfo widgetFilterTypeInfo) {
        JPanel jPanel = FRGUIPaneFactory.createBorderLayout_S_Pane();
        jPanel.setBackground(Color.WHITE);
        UILabel titleLabel = new UILabel(widgetFilterTypeInfo.getTitle() + ":");
        titleLabel.setBorder(BorderFactory.createEmptyBorder(0, 5, 0, 5));
        titleLabel.setFont(new Font(titleLabel.getFont().getName(), Font.BOLD, titleLabel.getFont().getSize()));
        jPanel.add(titleLabel, BorderLayout.NORTH);
        jPanel.add(createCategoryDetailPane(widgetFilterTypeInfo), BorderLayout.CENTER);
        return jPanel;
    }

    private JPanel createOtherCategoryPane(WidgetFilterTypeInfo filterTypeInfo) {
        JPanel jPanel = FRGUIPaneFactory.createBorderLayout_S_Pane();
        jPanel.setBackground(Color.WHITE);
        UILabel titleLabel = new UILabel(filterTypeInfo.getTitle());
        titleLabel.setBorder(BorderFactory.createEmptyBorder(0, 5, 0, 5));
        titleLabel.setPreferredSize(new Dimension(226, 20));
        titleLabel.setOpaque(true);
        titleLabel.setBackground(Color.decode("#EDEDEE"));
        jPanel.add(titleLabel, BorderLayout.NORTH);
        jPanel.add(createCategoryDetailPane(filterTypeInfo), BorderLayout.CENTER);
        return jPanel;
    }

    private JPanel createCategoryDetailPane(WidgetFilterTypeInfo filterTypeInfo) {
        JPanel contentPane = FRGUIPaneFactory.createNColumnGridInnerContainer_S_Pane(2);
        contentPane.setBorder(BorderFactory.createEmptyBorder(5, 0, 0, 0));
        contentPane.setBackground(Color.WHITE);
        int displayCount = 0;
        for (final WidgetFilterInfo filterInfo : filterTypeInfo.getFilterItems()) {
            if (!ComparatorUtils.equals(FILTER_ALL_ID, filterInfo.getId())) {
                displayCount++;
            }
        }
        int contentPaneHeight = ((displayCount + 1) / 2) * 27;
        contentPane.setPreferredSize(new Dimension(228, contentPaneHeight));
        for (final WidgetFilterInfo filterInfo : filterTypeInfo.getFilterItems()) {
            if (ComparatorUtils.equals(FILTER_ALL_ID, filterInfo.getId())) {
                continue;
            }
            final UICheckBox checkBox = new UICheckBox(filterInfo.getName());
            checkBox.setBackground(Color.WHITE);

            checkBox.addItemListener(new ItemListener() {
                @Override
                public void itemStateChanged(ItemEvent e) {
                    if (reset) {
                        return;
                    }
                    if (checkBox.isSelected()) {
                        filterList.add(filterInfo);
                    } else if (filterList.contains(filterInfo)) {
                        filterList.remove(filterInfo);
                    }
                    checkFilterButtonStatus();
                    filterPane.fireChangeListener(new ChangeEvent(assembleFilter()));
                }
            });


            checkBoxList.add(checkBox);
            contentPane.add(checkBox);
        }
        return contentPane;
    }

    protected abstract String assembleFilter();

    private void checkFilterButtonStatus() {
        filterPane.changeFilterButtonStatus(hasFilter());
    }

    public void reset() {
        reset = true;
        for (UICheckBox checkBox : checkBoxList) {
            checkBox.setSelected(false);
        }
        filterList.clear();
        checkFilterButtonStatus();
        filterPane.fireChangeListener(new ChangeEvent(StringUtils.EMPTY));
        reset = false;
    }

}
