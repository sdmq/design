package com.fr.design.mainframe.share.ui.widgetfilter;

import com.fr.form.share.bean.WidgetFilterTypeInfo;
import com.fr.form.share.utils.ShareUtils;

import java.util.List;

/**
 * @Author: Yuan.Wang
 * @Date: 2020/11/11
 */
public class OnlineFilterPopupPane extends FilterPopupPane {

    public OnlineFilterPopupPane(FilterPane filterPane, List<WidgetFilterTypeInfo> widgetFilterCategories) {
        super(filterPane, widgetFilterCategories);
    }

    protected String assembleFilter() {
        return ShareUtils.assembleFilter(getFilterList());
    }


}
