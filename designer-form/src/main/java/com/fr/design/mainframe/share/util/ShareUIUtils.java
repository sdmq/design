package com.fr.design.mainframe.share.util;

import com.fr.design.dialog.FineJOptionPane;
import com.fr.design.gui.ilable.UILabel;
import com.fr.design.i18n.Toolkit;
import com.fr.design.mainframe.DesignerContext;
import com.fr.invoke.Reflect;
import com.fr.log.FineLoggerFactory;
import com.fr.stable.ArrayUtils;
import com.fr.third.net.sf.cglib.proxy.Callback;
import com.fr.third.net.sf.cglib.proxy.CallbackFilter;
import com.fr.third.net.sf.cglib.proxy.Enhancer;
import com.fr.third.net.sf.cglib.proxy.MethodInterceptor;
import com.fr.third.net.sf.cglib.proxy.MethodProxy;
import com.fr.third.net.sf.cglib.proxy.NoOp;
import org.jetbrains.annotations.NotNull;

import javax.swing.AbstractButton;
import javax.swing.JComponent;
import javax.swing.SwingConstants;
import javax.swing.plaf.ComponentUI;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.lang.reflect.Constructor;
import java.lang.reflect.Method;

/**
 * created by Harrison on 2020/04/20
 **/
public class ShareUIUtils {

    private static final String PAINT_METHOD = "paint";

    private static final String widthStyle = "<html><body style='width: %d'>%s</body></html>";

    /**
     * 停止当前线程 xx s
     *
     * @param limit 限制
     * @throws InterruptedException 中断异常
     */
    public static void wait(int limit) throws InterruptedException {

        long start = System.currentTimeMillis();
        while (System.currentTimeMillis() - start < limit) {
            Thread.sleep(50);
        }
    }

    public static String formatWidthString(String msg, int width) {

        return String.format(widthStyle, width, msg);
    }

    public static UILabel createTipsLabel(String msg) {

        String tips = "<html><font color=gray>" + msg + "</font></html>";
        return new UILabel(tips);
    }

    public static UILabel createTopRightUILabel(String msg) {

        UILabel label = new UILabel(msg, SwingConstants.RIGHT);
        label.setVerticalAlignment(SwingConstants.TOP);
        return label;
    }

    public static UILabel createCenterRightUILabel(String msg) {

        UILabel label = new UILabel(msg, SwingConstants.RIGHT);
        label.setVerticalAlignment(SwingConstants.CENTER);
        return label;
    }

    public static UILabel createHyperlinkLabel(String msg) {

        String hyperlink = ("<html><font color=blue><u>") + msg + "</u></font></html>";
        return new UILabel(hyperlink);
    }

    public static String convertStateChange(int stateChange) {

        return String.valueOf(stateChange);
    }

    public static void simulateAction(AbstractButton button) {

        ActionEvent event = new ActionEvent(button,
                ActionEvent.ACTION_PERFORMED,
                button.getActionCommand());
        button.dispatchEvent(event);
    }

    public static <T extends JComponent> T wrapUI(final ComponentUI newUI, final T component, Object... constructArgs) {

        try {
            return warpUI0(newUI, component, constructArgs);
        } catch (Exception e) {
            //虽然没渲染上，但是不影响使用。 使用 warn 吧
            FineLoggerFactory.getLogger().warn(e.getMessage(), e);
            return component;
        }
    }

    @SuppressWarnings("unchecked")
    private static <T extends JComponent> T warpUI0(final ComponentUI newUI, final T component, Object... constructArgs) throws NoSuchMethodException {

        Enhancer enhancer = new Enhancer();
        enhancer.setSuperclass(component.getClass());
        enhancer.setCallbacks(new Callback[]{
                NoOp.INSTANCE,
                new MethodInterceptor() {
                    @Override
                    public Object intercept(Object o, Method method, Object[] objects, MethodProxy methodProxy) throws Throwable {

                        Object value = methodProxy.invokeSuper(o, objects);
                        newUI.paint((Graphics) objects[0], component);
                        return value;
                    }
                }
        });
        enhancer.setCallbackFilter(new CallbackFilter() {
            @Override
            public int accept(Method method) {
                return method.getName().equals(PAINT_METHOD) ? 1 : 0;
            }
        });


        Constructor<?> constructor = constructor(component.getClass(), constructArgs);
        if (constructArgs.length == 0 || constructor == null) {
            return (T) enhancer.create();
        }
        return (T) enhancer.create(constructor.getParameterTypes(), constructArgs);
    }


    public static Constructor<?> constructor(Class<?> type, Object... args) {

        Class<?>[] types = argumentTypes(args);
        try {
            return type.getDeclaredConstructor(types);
        } catch (NoSuchMethodException e) {
            for (Constructor<?> constructor : type.getDeclaredConstructors()) {
                if (match(constructor.getParameterTypes(), types)) {
                    return constructor;
                }
            }
        }
        return null;
    }

    public static void showErrorMessageDialog(String message) {
        FineJOptionPane.showMessageDialog(DesignerContext.getDesignerFrame(),
                message,
                Toolkit.i18nText("Fine-Design_Share_Dialog_Message"),
                FineJOptionPane.ERROR_MESSAGE);
    }

    private static boolean match(Class<?>[] declaredTypes, Class<?>[] actualTypes) {
        if (declaredTypes.length == actualTypes.length) {
            for (int i = 0; i < actualTypes.length; i++) {
                if (Reflect.wrapper(declaredTypes[i]).isAssignableFrom(Reflect.wrapper(actualTypes[i]))) {
                    continue;
                }
                return false;
            }
            return true;
        } else {
            return false;
        }
    }

    @NotNull
    private static Class[] argumentTypes(Object[] constructArgs) {

        if (ArrayUtils.isEmpty(constructArgs)) {
            return new Class[0];
        }
        int length = constructArgs.length;
        Class[] classes = new Class[length];
        for (int i = 0; i < length; i++) {
            classes[i] = constructArgs[i].getClass();
        }
        return classes;
    }


}
