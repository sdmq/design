package com.fr.design.mainframe.template.info;

import com.fr.form.ui.Widget;
import com.fr.json.JSONObject;

/**
 * Created by kerry on 2020-05-08
 */
public class ComponentDeleteOperate extends ComponentOperate {
    public static final String OPERATE_TYPE = "componentDelete";
    private long deleteTime = 0L;

    public ComponentDeleteOperate(Widget widget) {
        super(widget);
        this.deleteTime = System.currentTimeMillis();
    }

    @Override
    public String getOperateType() {
        return OPERATE_TYPE;
    }

    @Override
    public JSONObject toJSONObject() {
        JSONObject jo = super.toJSONObject();
        jo.put(ATTR_DELETE_TIME, deleteTime);
        return jo;
    }
}
