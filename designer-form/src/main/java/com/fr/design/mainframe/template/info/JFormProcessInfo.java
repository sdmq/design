package com.fr.design.mainframe.template.info;

import com.fr.base.iofile.attr.ExtendSharableAttrMark;
import com.fr.chart.chartattr.ChartCollection;
import com.fr.form.FormElementCaseProvider;
import com.fr.form.main.Form;
import com.fr.form.main.WidgetGather;
import com.fr.form.main.WidgetUtil;
import com.fr.form.ui.AbstractBorderStyleWidget;
import com.fr.form.ui.BaseChartEditor;
import com.fr.form.ui.CardSwitchButton;
import com.fr.form.ui.ElementCaseEditor;
import com.fr.form.ui.ElementCaseEditorProvider;
import com.fr.form.ui.LayoutBorderStyle;
import com.fr.form.ui.PaddingMargin;
import com.fr.form.ui.Widget;
import com.fr.form.ui.container.WAbsoluteLayout;
import com.fr.form.ui.container.WLayout;
import com.fr.general.ComparatorUtils;
import com.fr.json.JSONArray;
import com.fr.json.JSONObject;
import com.fr.report.cell.DefaultTemplateCellElement;

import java.util.Iterator;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by plough on 2017/3/17.
 *
 * @deprecated moved to cloud ops plugin
 */
@Deprecated
public class JFormProcessInfo extends TemplateProcessInfo<Form> {

    private static final String REGEX = "^" + com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Form_Title") + "\\d+$";

    private static final Pattern PATTERN = Pattern.compile(REGEX);

    private Map<String, JSONObject> componentProcessInfoMap = new ConcurrentHashMap<String, JSONObject>();

    public JFormProcessInfo(Form form) {
        super(form);
    }

    // 获取模板类型
    public int getReportType() {
        return 2;
    }

    // 获取模板格子数
    public int getCellCount() {
        return 0;
    }

    // 获取模板悬浮元素个数
    public int getFloatCount() {
        return 0;
    }

    // 获取模板聚合块个数
    public int getBlockCount() {
        return 0;
    }

    // 获取模板控件数
    public int getWidgetCount() {
        final AtomicInteger widgetCount = new AtomicInteger();
        Form.traversalWidget(this.template.getContainer(), new WidgetGather() {
            @Override
            public void dealWith(Widget widget) {
                if (ComponentOperate.ComponentType.supportComponent(widget)) {
                    widgetCount.incrementAndGet();
                }
            }

            @Override
            public boolean dealWithAllCards() {
                return true;
            }
        }, null);
        return widgetCount.get();
    }

    @Override
    public boolean isTestTemplate() {
        Iterator<String> it = this.template.getTableDataNameIterator();
        if (!it.hasNext() || getWidgetCount() <= 1 ||
                hasEmptyParaPane() || hasTestAbsolutePane()
                || hasTestTabLayout() || hasTestECReport() || hasTestChart()) {
            return true;
        }
        return false;
    }

    private boolean hasTestECReport() {
        ElementCaseEditorProvider[] elementCaseEditorProviders = this.template.getElementCases();
        for (ElementCaseEditorProvider elementCaseEditorProvider : elementCaseEditorProviders) {
            if (isTestECReport(elementCaseEditorProvider)) {
                return true;
            }
        }
        return false;
    }

    private boolean isTestECReport(ElementCaseEditorProvider elementCaseEditorProvider) {
        FormElementCaseProvider elementCase = elementCaseEditorProvider.getElementCase();
        if (!isTestElementCaseEditor((ElementCaseEditor) elementCaseEditorProvider)) {
            return false;
        }
        Iterator it = elementCase.cellIterator();
        if (!it.hasNext()) {
            return true;
        }
        while (it.hasNext()) {
            DefaultTemplateCellElement ce = (DefaultTemplateCellElement) it.next();
            Object value = ce.getValue();
            if (!isTestCell(value, ce.getStyle())) {
                return false;
            }
        }
        return true;
    }

    private boolean isTestElementCaseEditor(ElementCaseEditor editor) {
        return editor.getToolBars().length == 0 && editor.getListenerSize() == 0
                && ComparatorUtils.equals(editor.getBorderStyle(), new LayoutBorderStyle())
                && ComparatorUtils.equals(editor.getMargin(), new PaddingMargin())
                && editor.getBackground() == null;

    }

    private boolean hasTestChart() {
        final boolean[] hasTestChart = {false};
        Form.traversalWidget(this.template.getContainer(), new WidgetGather() {
            @Override
            public void dealWith(Widget widget) {
                ChartCollection chartCollection = (ChartCollection) ((BaseChartEditor) widget).getChartCollection();
                if (isTestChartCollection(chartCollection)) {
                    hasTestChart[0] = true;
                }
            }

            @Override
            public boolean dealWithAllCards() {
                return true;
            }
        }, BaseChartEditor.class);
        return hasTestChart[0];
    }


    private boolean hasTestTabLayout() {
        final boolean[] hasTestTabLayout = {false};
        Form.traversalWidget(this.template.getContainer(), new WidgetGather() {
            @Override
            public void dealWith(Widget widget) {
                String title = ((CardSwitchButton) widget).getText();
                Matcher matcher = PATTERN.matcher(title);
                if (matcher.find()) {
                    hasTestTabLayout[0] = true;
                }
            }

            @Override
            public boolean dealWithAllCards() {
                return true;
            }
        }, CardSwitchButton.class);
        return hasTestTabLayout[0];
    }

    private boolean hasTestAbsolutePane() {
        final boolean[] hasTestAbsolutePane = {false};
        Form.traversalWidget(this.template.getContainer(), new WidgetGather() {
            @Override
            public void dealWith(Widget widget) {
                if (((WAbsoluteLayout) widget).getWidgetCount() == 0) {
                    hasTestAbsolutePane[0] = true;
                }
            }

            @Override
            public boolean dealWithAllCards() {
                return true;
            }
        }, WAbsoluteLayout.class);
        return hasTestAbsolutePane[0];
    }

    private boolean hasEmptyParaPane() {
        WLayout paraContainer = this.template.getParaContainer();
        return paraContainer != null && paraContainer.getWidgetCount() == 0;
    }

    @Override
    public boolean useParaPane() {
        return this.template.getParaContainer() != null;
    }

    @Override
    public JSONArray getComponentsInfo() {
        JSONArray ja = new JSONArray();
        for (JSONObject value : componentProcessInfoMap.values()) {
            ja.put(value);
        }
        this.componentProcessInfoMap.clear();
        return ja;
    }

    @Override
    public JSONArray getReuseCmpList() {

        final JSONArray jo = JSONArray.create();
        WLayout container = this.template.getContainer();
        WidgetUtil.bfsTraversalWidget(container, new WidgetUtil.BfsWidgetGather<AbstractBorderStyleWidget>() {
            @Override
            public boolean dealWith(AbstractBorderStyleWidget styleWidget) {
                ExtendSharableAttrMark widgetAttrMark = styleWidget.getWidgetAttrMark(ExtendSharableAttrMark.XML_TAG);
                if (widgetAttrMark != null) {
                    String shareId = widgetAttrMark.getShareId();
                    jo.add(shareId);
                    return true;
                }
                return false;
            }
        }, AbstractBorderStyleWidget.class);
        return jo;
    }

    @Override
    public void updateTemplateOperationInfo(TemplateOperate templateOperate) {
        if (ComparatorUtils.equals(ComponentCreateOperate.OPERATE_TYPE, templateOperate.getOperateType())) {
            addComponentCreateInfo(templateOperate.toJSONObject());
        }
        if (ComparatorUtils.equals(ComponentDeleteOperate.OPERATE_TYPE, templateOperate.getOperateType())) {
            addComponentRemoveInfo(templateOperate.toJSONObject());
        }
    }

    private void addComponentCreateInfo(JSONObject jsonObject) {
        String componentID = jsonObject.getString("componentID");
        componentProcessInfoMap.put(componentID, jsonObject);
    }

    private void addComponentRemoveInfo(JSONObject jsonObject) {
        String componentID = jsonObject.getString("componentID");
        if (componentID == null) {
            return;
        }
        JSONObject info = componentProcessInfoMap.get(componentID);
        if (info == null) {
            info = jsonObject;
            componentProcessInfoMap.put(componentID, jsonObject);
        }
        info.put("deleteTime", System.currentTimeMillis());
    }
}
