package com.fr.design.mainframe.widget.arrangement.buttons;

import com.fr.design.i18n.Toolkit;
import com.fr.design.mainframe.ArrangementType;
import com.fr.design.mainframe.MultiSelectionArrangement;
import com.fr.general.IOUtils;

import javax.swing.Icon;

public class BottomAlignButton extends AbstractMultiSelectionArrangementButton {
    private static final long serialVersionUID = 2397455240682353024L;

    public BottomAlignButton(MultiSelectionArrangement arrangement) {
        super(arrangement);
    }

    @Override
    public Icon getIcon() {
        return IOUtils.readIcon("/com/fr/design/images/buttonicon/multi_selection_bottom_align.png");
    }

    @Override
    public String getTipText() {
        return Toolkit.i18nText("Fine-Design_Multi_Selection_Bottom_Align");
    }

    @Override
    public ArrangementType getArrangementType() {
        return ArrangementType.BOTTOM_ALIGN;
    }
}
