package com.fr.design.mainframe.widget.propertypane;

import com.fr.design.designer.properties.ItemWrapper;

public class BrowserFitWrapper extends ItemWrapper {
    public BrowserFitWrapper() {
        super(new BrowserFitAlignmentItems());
    }
}
