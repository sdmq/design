package com.fr.design.mainframe.widget.ui;

import com.fr.design.base.mode.DesignModeContext;
import com.fr.design.data.DataCreatorUI;
import com.fr.design.designer.beans.events.DesignerEvent;
import com.fr.design.designer.creator.XCreator;
import com.fr.design.designer.creator.XCreatorUtils;
import com.fr.design.designer.creator.XLayoutContainer;
import com.fr.design.designer.creator.XWAbsoluteBodyLayout;
import com.fr.design.designer.creator.XWAbsoluteLayout;
import com.fr.design.designer.creator.XWFitLayout;
import com.fr.design.designer.creator.XWParameterLayout;
import com.fr.design.designer.creator.XWScaleLayout;
import com.fr.design.designer.creator.XWTitleLayout;
import com.fr.design.designer.creator.cardlayout.XWCardTagLayout;
import com.fr.design.file.HistoryTemplateListCache;
import com.fr.design.foldablepane.UIExpandablePane;
import com.fr.design.gui.frpane.AttributeChangeListener;
import com.fr.design.gui.itextfield.UITextField;
import com.fr.design.i18n.Toolkit;
import com.fr.design.layout.FRGUIPaneFactory;
import com.fr.design.mainframe.DesignerContext;
import com.fr.design.mainframe.EastRegionContainerPane;
import com.fr.design.mainframe.FormDesigner;
import com.fr.design.mainframe.JForm;
import com.fr.design.mainframe.JTemplate;
import com.fr.design.widget.DataModify;
import com.fr.design.widget.FormWidgetDefinePaneFactoryBase;
import com.fr.design.widget.Operator;
import com.fr.design.widget.ui.designer.component.WidgetAbsoluteBoundPane;
import com.fr.design.widget.ui.designer.component.WidgetBoundPane;
import com.fr.design.widget.ui.designer.component.WidgetCardTagBoundPane;
import com.fr.form.ui.ChartEditor;
import com.fr.form.ui.Widget;
import com.fr.form.ui.container.WScaleLayout;
import com.fr.form.ui.container.WTitleLayout;
import com.fr.form.ui.widget.CRBoundsWidget;
import com.fr.general.ComparatorUtils;
import com.fr.general.IOUtils;
import com.fr.stable.StringUtils;

import javax.swing.BorderFactory;
import javax.swing.JComponent;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import java.awt.BorderLayout;

public class FormSingleWidgetCardPane extends FormWidgetCardPane {
    private AttributeChangeListener listener;

    //当前的编辑器属性定义面板
    private DataModify<Widget> currentEditorDefinePane;
    private FormBasicPropertyPane widgetPropertyPane;
    private JPanel attriCardPane;

    private XCreator xCreator;
    private WidgetBoundPane widgetBoundPane;


    public FormSingleWidgetCardPane(FormDesigner designer) {
        super(designer);
    }

    public void initPropertyPane() {
        this.xCreator = findXcreator(designer);
        initComponents();
        initDefinePane();
    }

    @Deprecated
    public XLayoutContainer getParent(XCreator source) {
        return XCreatorUtils.getParent(source);
    }

    public WidgetBoundPane createWidgetBoundPane(XCreator xCreator) {
        XLayoutContainer xLayoutContainer = getParent(xCreator);
        if (xLayoutContainer == null || xCreator.acceptType(XWParameterLayout.class) || xCreator.acceptType(XWAbsoluteBodyLayout.class)) {
            return null;
        } else if (xLayoutContainer.acceptType(XWAbsoluteLayout.class)) {
            return new WidgetAbsoluteBoundPane(xCreator);
        } else if (xCreator.acceptType(XWCardTagLayout.class)) {
            return new WidgetCardTagBoundPane(xCreator);
        }
        return new WidgetBoundPane(xCreator);
    }

    public XCreator findXcreator(FormDesigner designer) {
        XCreator creator = designer.getSelectionModel().getSelection().getSelectedCreator();
        return creator != null ? creator : designer.getRootComponent();
    }

    /**
     * 后台初始化所有事件.
     */
    @Override
    public void initAllListeners() {

    }

    /**
     * 后台初始化所有事件.
     */
    public void reinitAllListeners() {
        initListener(this);
    }

    @Override
    protected void initContentPane() {

    }

    private void initComponents() {
        XCreator innerCreator = XCreatorUtils.getXCreatorInnerWidget(this.xCreator);

        attriCardPane = FRGUIPaneFactory.createBorderLayout_S_Pane();
        content.add(attriCardPane, BorderLayout.CENTER);
        content.setBorder(BorderFactory.createEmptyBorder(0, 10, 0, 10));

        final boolean isExtraWidget = FormWidgetDefinePaneFactoryBase.isExtraXWidget(innerCreator.toData());
        this.listener = new AttributeChangeListener() {
            @Override
            public void attributeChange() {
                if (!isExtraWidget) {
                    updateCreator();
                }
                updateWidgetBound();
                firePropertyEdit();
            }
        };

        freshPropertyMode(innerCreator);
        if (isExtraWidget) {
            // REPORT-55603: 仅对于插件控件，将尺寸*位置面板放置在definePane下方，其余控件将尺寸*位置面板放置在definePane上方
            widgetBoundPane = createWidgetBoundPane(xCreator);
            if (widgetBoundPane != null) {
                attriCardPane.add(widgetBoundPane, BorderLayout.CENTER);
            }
            return;
        }

        widgetPropertyPane = WidgetBasicPropertyPaneFactory.createBasicPropertyPane(innerCreator);

        UIExpandablePane uiExpandablePane = new UIExpandablePane(com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Report_Basic"), 280, 20, widgetPropertyPane);

        content.add(uiExpandablePane, BorderLayout.NORTH);

        widgetBoundPane = createWidgetBoundPane(xCreator);
        if (widgetBoundPane != null) {
            attriCardPane.add(widgetBoundPane, BorderLayout.NORTH);
        }
    }

    private static void freshPropertyMode(XCreator xCreator) {
        JTemplate jTemplate = HistoryTemplateListCache.getInstance().getCurrentEditingTemplate();
        if (!(jTemplate instanceof JForm) && jTemplate.isUpMode() && !DesignModeContext.isAuthorityEditing()) {
            if (xCreator instanceof XWParameterLayout) {
                EastRegionContainerPane.getInstance().switchMode(EastRegionContainerPane.PropertyMode.REPORT_PARA);
            } else {
                EastRegionContainerPane.getInstance().switchMode(EastRegionContainerPane.PropertyMode.REPORT_PARA_WIDGET);
            }
        }
    }

    private void initDefinePane() {
        currentEditorDefinePane = null;
        XCreator creator = XCreatorUtils.getXCreatorInnerWidget(this.xCreator);
        FormWidgetDefinePaneFactoryBase.RN rn = FormWidgetDefinePaneFactoryBase.createWidgetDefinePane(creator, designer, creator.toData(), new Operator() {
            @Override
            public void did(DataCreatorUI ui, String cardName) {
                //todo
            }
        });
        DataModify<Widget> definePane = rn.getDefinePane();

        JComponent definePaneComponent = definePane.toSwingComponent();
        boolean isExtraWidget = FormWidgetDefinePaneFactoryBase.isExtraXWidget(creator.toData());

        if (isExtraWidget) {
            // REPORT-55603: 仅对于插件控件，将尺寸*位置面板放置在definePane下方，其余控件将尺寸*位置面板放置在definePane上方
            attriCardPane.add(definePaneComponent, BorderLayout.NORTH);
        } else {
            // 使用单独的JPane和BorderLayout.North进行包装，避免出现CENTER嵌套CENTER后，内容高度变大的情况
            JPanel definePaneWrapContent = FRGUIPaneFactory.createBorderLayout_S_Pane();
            definePaneWrapContent.add(definePaneComponent, BorderLayout.NORTH);
            attriCardPane.add(definePaneWrapContent, BorderLayout.CENTER);
        }
        currentEditorDefinePane = definePane;
    }

    @Override
    public String title4PopupWindow() {
        return "Widget";
    }

    public void populate() {
        //populate之前先移除所有的监听
        removeAttributeChangeListener();
        Widget cellWidget = xCreator.toData();
        if (widgetBoundPane != null) {
            widgetBoundPane.populate();
        }
        Widget innerWidget = cellWidget;
        if (cellWidget.acceptType(WScaleLayout.class)) {
            Widget crBoundsWidget = ((WScaleLayout) cellWidget).getBoundsWidget();
            innerWidget = ((CRBoundsWidget) crBoundsWidget).getWidget();
        } else if (cellWidget.acceptType(WTitleLayout.class)) {
            CRBoundsWidget crBoundsWidget = ((WTitleLayout) cellWidget).getBodyBoundsWidget();
            innerWidget = crBoundsWidget.getWidget();
        }
        if (currentEditorDefinePane != null) {
            currentEditorDefinePane.populateBean(innerWidget);
        }
        if (widgetPropertyPane != null) {
            widgetPropertyPane.populate(innerWidget);
        }
        reinitAllListeners();
        this.addAttributeChangeListener(listener);
    }


    public void updateCreator() {
        currentEditorDefinePane.setGlobalName(getGlobalName());
        Widget widget = currentEditorDefinePane.updateBean();
        if (ComparatorUtils.equals(getGlobalName(), Toolkit.i18nText("Fine-Design_Report_Basic")) && widgetPropertyPane != null) {
            UITextField widgetNameField = widgetPropertyPane.getWidgetNameField();
            String toSetWidgetName = widgetNameField.getText();
            String currentWidgetName = widget.getWidgetName();
            if (toSetWidgetName.isEmpty()) {
                widgetNameField.setText(currentWidgetName);
                return;
            }

            boolean exist = designer.getTarget().isNameExist(toSetWidgetName, widget) && !ComparatorUtils.equals(toSetWidgetName, currentWidgetName);
            if (exist) {
                widgetNameField.setText(currentWidgetName);
                showNameInvalidDialog(Toolkit.i18nText("Fine-Design_Form_Widget_Rename_Failure"));
                return;
            }

            //图表名称的合法性检查
            if (widget instanceof ChartEditor && toSetWidgetName.startsWith("Chart")) {
                widgetNameField.setText(currentWidgetName);
                showNameInvalidDialog(Toolkit.i18nText("Fine-Design_Form_Chart_Widget_Rename_Failure"));
                return;
            }
            widgetPropertyPane.update(widget);
            // 上面一行更新了组件 这里必须重新调用getWidgetName
            xCreator.resetCreatorName(widget.getWidgetName());
            xCreator.resetVisible(widget.isVisible());
            designer.getEditListenerTable().fireCreatorModified(xCreator, DesignerEvent.CREATOR_RENAMED);
            return;
        }
        fireValueChanged();
    }

    private void showNameInvalidDialog(String message) {
        JOptionPane.showMessageDialog(DesignerContext.getDesignerFrame(), message, Toolkit.i18nText("Fine-Design_Form_Joption_News"), JOptionPane.ERROR_MESSAGE, IOUtils.readIcon("com/fr/design/form/images/joption_failure.png"));
    }

    public void updateWidgetBound() {
        if (widgetBoundPane != null && ComparatorUtils.equals(getGlobalName(), com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Form_Coords_And_Size"))) {
            widgetBoundPane.update();
            designer.getEditListenerTable().fireCreatorModified(DesignerEvent.CREATOR_RESIZED);
        }
        designer.refreshDesignerUI();
    }


    @Override
    /**
     *检查
     */
    public void checkValid() throws Exception {
        currentEditorDefinePane.checkValid();
    }

    public void fireValueChanged() {
        XCreator creator = XCreatorUtils.getXCreatorInnerWidget(this.xCreator);
        creator.firePropertyChange();
    }

    @Override
    public String getIconPath() {
        return StringUtils.EMPTY;
    }

    public void firePropertyEdit() {
        designer.getEditListenerTable().fireCreatorModified(DesignerEvent.CREATOR_EDITED);
    }
}
