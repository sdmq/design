package com.fr.design.widget.ui.designer.mobile;

import com.fr.design.constants.LayoutConstants;
import com.fr.design.designer.creator.XCreator;
import com.fr.design.foldablepane.UIExpandablePane;
import com.fr.design.gui.frpane.AttributeChangeListener;
import com.fr.design.gui.ilable.UILabel;
import com.fr.design.gui.ispinner.UISpinner;
import com.fr.design.layout.FRGUIPaneFactory;
import com.fr.design.layout.TableLayout;
import com.fr.design.layout.TableLayoutHelper;
import com.fr.design.mainframe.DesignerContext;
import com.fr.design.mainframe.FormDesigner;
import com.fr.form.ui.ButtonGroup;

import javax.swing.*;
import java.awt.*;


public class ButtonGroupDefinePane extends MobileWidgetDefinePane {

    private XCreator xCreator; // 当前选中控件的xCreator
    private UISpinner maxShowRowsSpinner;

    public ButtonGroupDefinePane(XCreator xCreator) {
        this.xCreator = xCreator;
    }

    @Override
    public void initPropertyGroups(Object source) {
        this.setLayout(FRGUIPaneFactory.createBorderLayout());
        UILabel maxShowRowsLabel = new UILabel(com.fr.design.i18n.Toolkit.i18nText("Fine-Design-Mobile_Max_Show_Rows"), SwingConstants.LEFT);
        this.maxShowRowsSpinner = new UISpinner(0, Integer.MAX_VALUE, 1, 5);
        Component[][] components = new Component[][]{
               new Component[] {maxShowRowsLabel, maxShowRowsSpinner}
        };
        double f = TableLayout.FILL;
        double p = TableLayout.PREFERRED;
        double[] rowSize = {p};
        double[] columnSize = {p,f};
        int[][] rowCount = {{1, 1}};
        final JPanel panel =  TableLayoutHelper.createGapTableLayoutPane(components, rowSize, columnSize, rowCount, 30, LayoutConstants.VGAP_LARGE);
        panel.setBorder(BorderFactory.createEmptyBorder(10, 0, 0, 0));
        final JPanel panelWrapper = FRGUIPaneFactory.createBorderLayout_S_Pane();
        panelWrapper.add(panel, BorderLayout.NORTH);
        UIExpandablePane folderPane = new UIExpandablePane(com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Report_Advanced"), 280, 20, panelWrapper);
        this.add(folderPane, BorderLayout.NORTH);
        this.repaint();
    }

    private void bindListeners2Widgets() {
        reInitAllListeners();
        AttributeChangeListener changeListener = new AttributeChangeListener() {
            @Override
            public void attributeChange() {
                update();
            }
        };
        this.addAttributeChangeListener(changeListener);
    }

    /**
     * 后台初始化所有事件.
     */
    private void reInitAllListeners() {
        initListener(this);
    }

    @Override
    public void populate(FormDesigner designer) {
        ButtonGroup buttonGroup = (ButtonGroup)xCreator.toData();
        this.maxShowRowsSpinner.setValue(buttonGroup.getMaxShowRows());
        // 数据 populate 完成后，再设置监听
        this.bindListeners2Widgets();
    }

    @Override
    public void update() {
        ButtonGroup buttonGroup = (ButtonGroup)xCreator.toData();
        buttonGroup.setMaxShowRows((int) maxShowRowsSpinner.getValue());
        DesignerContext.getDesignerFrame().getSelectedJTemplate().fireTargetModified(); // 触发设计器保存按钮亮起来
    }
}
