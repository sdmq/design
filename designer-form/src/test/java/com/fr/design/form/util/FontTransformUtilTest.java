package com.fr.design.form.util;

import com.fr.design.ExtraDesignClassManager;
import com.fr.design.fun.FormAdaptiveConfigUIProcessor;
import com.fr.stable.Constants;
import org.easymock.EasyMock;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.easymock.PowerMock;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

/**
 * Created by kerry on 2020-05-14
 */
@RunWith(PowerMockRunner.class)
@PrepareForTest(ExtraDesignClassManager.class)
public class FontTransformUtilTest {

    @Test
    public void testGetDesignerFontResolution() {
        mockEnvironment(Constants.DEFAULT_FONT_PAINT_RESOLUTION);
        Assert.assertEquals(Constants.DEFAULT_FONT_PAINT_RESOLUTION, FontTransformUtil.getDesignerFontResolution());

        mockEnvironment(Constants.DEFAULT_WEBWRITE_AND_SCREEN_RESOLUTION);
        Assert.assertEquals(Constants.DEFAULT_WEBWRITE_AND_SCREEN_RESOLUTION, FontTransformUtil.getDesignerFontResolution());

    }

    @Test
    public void testPt2px() {
        mockEnvironment(Constants.DEFAULT_FONT_PAINT_RESOLUTION);
        Assert.assertEquals(12, (int) FontTransformUtil.pt2px(12));

        mockEnvironment(Constants.DEFAULT_WEBWRITE_AND_SCREEN_RESOLUTION);
        Assert.assertEquals(16, (int) FontTransformUtil.pt2px(12));
    }

    @Test
    public void testPx2pt() {
        mockEnvironment(Constants.DEFAULT_FONT_PAINT_RESOLUTION);
        Assert.assertEquals(12, (int) FontTransformUtil.px2pt(12));

        mockEnvironment(Constants.DEFAULT_WEBWRITE_AND_SCREEN_RESOLUTION);
        Assert.assertEquals(9, (int) FontTransformUtil.px2pt(12));
    }


    private void mockEnvironment(int dpi) {
        ExtraDesignClassManager mockDesignManager = EasyMock.mock(ExtraDesignClassManager.class);
        EasyMock.expect(mockDesignManager.getSingle(FormAdaptiveConfigUIProcessor.MARK_STRING))
                .andReturn(mockProcessor(dpi)).anyTimes();
        EasyMock.replay(mockDesignManager);

        PowerMock.mockStatic(ExtraDesignClassManager.class);
        EasyMock.expect(ExtraDesignClassManager.getInstance()).andReturn(mockDesignManager).once();
        PowerMock.replayAll(ExtraDesignClassManager.class);
    }

    private FormAdaptiveConfigUIProcessor mockProcessor(int dpi) {
        FormAdaptiveConfigUIProcessor processor = EasyMock.mock(FormAdaptiveConfigUIProcessor.class);
        EasyMock.expect(processor.fontResolution()).andReturn(dpi).once();
        EasyMock.replay(processor);
        return processor;
    }

}
