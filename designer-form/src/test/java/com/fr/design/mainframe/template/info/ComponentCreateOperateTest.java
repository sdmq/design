package com.fr.design.mainframe.template.info;

import com.fr.form.ui.CardAddButton;
import com.fr.form.ui.CardSwitchButton;
import com.fr.form.ui.ChartEditor;
import com.fr.form.ui.ElementCaseEditor;
import com.fr.form.ui.FreeButton;
import com.fr.form.ui.TextEditor;
import com.fr.form.ui.Widget;
import com.fr.form.ui.container.WAbsoluteBodyLayout;
import com.fr.form.ui.container.WAbsoluteLayout;
import com.fr.form.ui.container.WCardLayout;
import com.fr.form.ui.container.WScaleLayout;
import com.fr.form.ui.container.WTitleLayout;
import com.fr.form.ui.container.cardlayout.WCardMainBorderLayout;
import com.fr.form.ui.widget.CRBoundsWidget;
import com.fr.general.ComparatorUtils;
import com.fr.json.JSONObject;
import org.junit.Assert;
import org.junit.Test;

import java.awt.Rectangle;

/**
 * Created by kerry on 2020-05-08
 */
public class ComponentCreateOperateTest {

    @Test
    public void testConstructor() {
        ElementCaseEditor caseEditor = new ElementCaseEditor();
        caseEditor.setWidgetName("report0");
        caseEditor.setWidgetID("xxxx0");
        WTitleLayout wTitleLayout = new WTitleLayout();
        wTitleLayout.addBody(caseEditor, new Rectangle());
        ComponentOperate componentOperate0 = new ComponentCreateOperate(caseEditor);
        ComponentOperate componentOperate1 = new ComponentCreateOperate(wTitleLayout);
        ComparatorUtils.equals(componentOperate0.toJSONObject(), componentOperate1.toJSONObject());

        WScaleLayout scaleLayout = new WScaleLayout();
        TextEditor textEditor = new TextEditor();
        scaleLayout.addWidget(new CRBoundsWidget(textEditor, new Rectangle()));
        ComponentOperate componentOperate3 = new ComponentCreateOperate(scaleLayout);
        ComponentOperate componentOperate4 = new ComponentCreateOperate(textEditor);
        ComparatorUtils.equals(componentOperate3.toJSONObject(), componentOperate4.toJSONObject());


        WCardMainBorderLayout wCardMainBorderLayout = new WCardMainBorderLayout();
        WCardLayout wCardLayout = new WCardLayout();
        wCardMainBorderLayout.addCenter(wCardLayout);
        ComponentOperate componentOperate5 = new ComponentCreateOperate(wCardMainBorderLayout);
        ComponentOperate componentOperate6 = new ComponentCreateOperate(wCardLayout);
        ComparatorUtils.equals(componentOperate5.toJSONObject(), componentOperate6.toJSONObject());

    }

    @Test
    public void testToJSONObject() {
        Widget button = new FreeButton();
        button.setWidgetName("button1");
        ComponentCreateOperate info = new ComponentCreateOperate(button);
        button.setWidgetID("xxxxxxx");
        JSONObject jo = info.toJSONObject();
        Assert.assertEquals("xxxxxxx", jo.optString("componentID"));
        Assert.assertEquals("button1", jo.optString("componentName"));
        Assert.assertEquals("Widget", jo.optString("componentType"));
        Assert.assertEquals("0", jo.optString("deleteTime"));
    }

    @Test
    public void testComponentType() {
        Assert.assertEquals(ComponentOperate.ComponentType.Widget, ComponentOperate.ComponentType.parseType(new FreeButton()));
        Assert.assertEquals(ComponentOperate.ComponentType.Report, ComponentOperate.ComponentType.parseType(new ElementCaseEditor()));
        Assert.assertEquals(ComponentOperate.ComponentType.Chart, ComponentOperate.ComponentType.parseType(new ChartEditor()));
        Assert.assertEquals(ComponentOperate.ComponentType.Absolute, ComponentOperate.ComponentType.parseType(new WAbsoluteLayout()));
        Assert.assertEquals(ComponentOperate.ComponentType.TabLayout, ComponentOperate.ComponentType.parseType(new WCardLayout()));
    }

    @Test
    public void testSupportComponent() {
        Assert.assertTrue(ComponentOperate.ComponentType.supportComponent(new FreeButton()));
        Assert.assertTrue(ComponentOperate.ComponentType.supportComponent(new ElementCaseEditor()));
        Assert.assertTrue(ComponentOperate.ComponentType.supportComponent(new ChartEditor()));
        Assert.assertTrue(ComponentOperate.ComponentType.supportComponent(new WAbsoluteLayout()));
        Assert.assertTrue(ComponentOperate.ComponentType.supportComponent(new WCardLayout()));
        Assert.assertFalse(ComponentOperate.ComponentType.supportComponent(new CardSwitchButton()));
        Assert.assertFalse(ComponentOperate.ComponentType.supportComponent(new CardAddButton()));
        Assert.assertFalse(ComponentOperate.ComponentType.supportComponent(new WAbsoluteBodyLayout()));
    }
}
