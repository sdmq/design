package com.fr.design.mainframe.alphafine;

import com.fr.design.DesignerEnvManager;
import com.fr.design.mainframe.alphafine.search.manager.impl.ProductNewsSearchManager;
import com.fr.stable.ArrayUtils;
import com.fr.stable.StringUtils;
import java.util.Set;

/**
 * @author hades
 * @version 11.0
 * Created by hades on 2022/4/20
 */
public class AlphaFineUtil {

    public static String highLightModelName(String modelName, String[] strings) {
        if (strings == null || ArrayUtils.isEmpty(strings)) {
            return modelName;
        }
        for (String string : strings) {
            // 高亮分词 跳过高亮颜色本身的字符
            boolean skipHighLight = modelName.contains(AlphaFineConstants.HIGH_LIGHT_COLOR) && AlphaFineConstants.HIGH_LIGHT_COLOR.contains(string);
            if (skipHighLight) {
                continue;
            }
            String primaryStr = getReplacedString(modelName, string);
            if (StringUtils.isNotEmpty(primaryStr)) {
                modelName = modelName.replaceAll("(?i)" + primaryStr, "|<font color=" + AlphaFineConstants.HIGH_LIGHT_COLOR + ">" + primaryStr + "</font>|");
            }
        }
        modelName = "<html><head><style> .style{" +
                "overflow: hidden;" +
                "text-overflow: ellipsis;" +
                "white-space: nowrap;}" +
                "</style></head><body class=\"style\">" + modelName.replaceAll("\\|", StringUtils.EMPTY) + "</body></HTML>";
        return modelName;
    }


    private static String getReplacedString(String modelName, String string) {
        // 如果是直接包含了高亮字符 返回
        if (StringUtils.contains(modelName, string)) {
            return string;
        }
        //需要考虑modelName有空格的情况
        //比如现在是work boo k 搜索词是workb,应该要替换的部分是work b
        //先去掉已经匹配替换过的部分,因为考虑到分词的情况,可能会进行多次替换
        final String regex = "\\|<font.*?</font>\\|";
        modelName = modelName.replaceAll(regex, StringUtils.EMPTY);
        //再去掉空格进行匹配
        String noBlackName = modelName.replaceAll(StringUtils.BLANK, StringUtils.EMPTY).toLowerCase();
        int index = noBlackName.indexOf(string.toLowerCase());
        if (index == -1) {
            return StringUtils.EMPTY;
        }
        StringBuilder result = new StringBuilder();
        int count = 0;
        while (count < string.length()) {
            char pos = modelName.charAt(index++);
            result.append(pos);
            count += pos == ' ' ? 0 : 1;
        }
        return result.toString();
    }

    public static String escapeExprSpecialWord(String keyword) {
        if (StringUtils.isNotBlank(keyword)) {
            String[] fbsArr = { "\\", "$", "(", ")", "*", "+", ".", "[", "]", "?", "^", "{", "}", "|" };
            for (String key : fbsArr) {
                if (keyword.contains(key)) {
                    keyword = keyword.replace(key, "\\" + key);
                }
            }
        }
        return keyword;
    }

    public static boolean unread() {
        Set<Long> readSet = DesignerEnvManager.getEnvManager().getAlphaFineConfigManager().getReadSet();
        Set<Long> idSet = ProductNewsSearchManager.getInstance().getIdSet();
        return !idSet.isEmpty() && !readSet.containsAll(idSet);
    }
}
