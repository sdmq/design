package com.fr.design.mainframe.alphafine;

import com.fr.stable.StringUtils;

/**
 * Created by XiaXiang on 2017/4/27.
 */
public enum CellType {
    RECOMMEND(0),
    ACTION(1, "action", "actionResult", false),
    DOCUMENT(2, "document", "documentResult", true),
    FILE(3, "file", "fileResult", false),
    PLUGIN(4, "plugin", "pluginResult", true),
    REUSE(5),
    NO_RESULT(6, "noResult", StringUtils.EMPTY, false),
    MORE(7),
    RECOMMEND_ROBOT(8),
    BOTTOM(9),
    ROBOT(10),
    PRODUCT_NEWS(11, "productNews", "productNewsResult", true);

    private int typeValue;

    /**
     * 标志位 索引无搜索结果面板
     *
     * 空字符表示不涉及索引该项
     */
    private String flagStr4None;

    /**
     * 标志位 索引有搜索结果面板
     */
    private String flagStr4Result;

    private boolean needNetWork = true;

    CellType(int type, String flagStr4None, String flagStr4Result, boolean needNetWork) {
        this.typeValue = type;
        this.flagStr4None = flagStr4None;
        this.flagStr4Result = flagStr4Result;
        this.needNetWork = needNetWork;
    }

    CellType(int type) {
        this(type, StringUtils.EMPTY, StringUtils.EMPTY, true);
    }

    public static CellType parse(int typeValue) {
        for (CellType type : CellType.values()) {
            if (type.getTypeValue() == typeValue) {
                return type;
            }
        }
        return FILE;

    }

    public int getTypeValue() {
        return typeValue;
    }

    public void setTypeValue(int typeValue) {
        this.typeValue = typeValue;
    }

    public String getFlagStr4None() {
        return flagStr4None;
    }

    public String getFlagStr4Result() {
        return flagStr4Result;
    }

    public boolean isNeedNetWork() {
        return needNetWork;
    }
}

