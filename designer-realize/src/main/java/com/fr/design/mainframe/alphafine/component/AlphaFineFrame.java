package com.fr.design.mainframe.alphafine.component;

import com.fr.base.svg.IconUtils;
import com.fr.base.svg.SVGLoader;
import com.fr.design.DesignerEnvManager;
import com.fr.design.actions.help.alphafine.AlphaFineConfigManager;
import com.fr.design.actions.help.alphafine.AlphaFineShortCutUtil;
import com.fr.design.constants.UIConstants;
import com.fr.design.gui.borders.UITextFieldBorder;
import com.fr.design.gui.ibutton.UIButton;
import com.fr.design.gui.ilable.UILabel;
import com.fr.design.mainframe.alphafine.AlphaFineConstants;
import com.fr.design.mainframe.alphafine.AlphaFineHelper;
import com.fr.design.mainframe.alphafine.CellType;
import com.fr.design.mainframe.alphafine.cell.model.AlphaCellModel;
import com.fr.design.mainframe.alphafine.model.ProductNews;
import com.fr.design.mainframe.alphafine.preview.DefaultProductNewsPane;
import com.fr.design.mainframe.alphafine.preview.HelpDocumentNoResultPane;
import com.fr.design.mainframe.alphafine.preview.LoadingRightSearchResultPane;
import com.fr.design.mainframe.alphafine.preview.NetWorkFailedPane;
import com.fr.design.mainframe.alphafine.preview.NoResultPane;
import com.fr.design.mainframe.alphafine.preview.NoResultWithLinkPane;
import com.fr.design.mainframe.alphafine.preview.SearchLoadingPane;
import com.fr.design.mainframe.alphafine.preview.SimpleRightSearchResultPane;
import com.fr.design.mainframe.alphafine.question.QuestionWindow;
import com.fr.design.mainframe.alphafine.search.ProductNewsSearchWorkerManager;
import com.fr.design.mainframe.alphafine.search.SearchTextBean;
import com.fr.design.mainframe.alphafine.search.SearchWorkerManager;
import com.fr.design.mainframe.alphafine.search.manager.impl.ActionSearchManager;
import com.fr.design.mainframe.alphafine.search.manager.impl.DocumentSearchManager;
import com.fr.design.mainframe.alphafine.search.manager.impl.FileSearchManager;
import com.fr.design.mainframe.alphafine.search.manager.impl.PluginSearchManager;
import com.fr.design.mainframe.alphafine.search.manager.impl.ProductNewsSearchManager;
import com.fr.design.mainframe.alphafine.search.manager.impl.SegmentationManager;
import com.fr.design.utils.DesignUtils;
import com.fr.design.utils.gui.GUICoreUtils;
import com.fr.general.ComparatorUtils;
import com.fr.stable.StringUtils;
import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Insets;
import java.awt.RenderingHints;
import java.awt.Toolkit;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.swing.BorderFactory;
import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.SwingConstants;
import javax.swing.Timer;
import javax.swing.event.PopupMenuEvent;
import javax.swing.event.PopupMenuListener;

/**
 * @author hades
 * @version 11.0
 * Created by hades on 2022/04/06
 */
public class AlphaFineFrame extends JFrame {

    private static final String ADVANCED_SEARCH_MARK = "k:";
    private static final String ACTION_MARK_SHORT = "k:1 ";
    private static final String ACTION_MARK = "k:setting ";
    private static final String DOCUMENT_MARK_SHORT = "k:2 ";
    private static final String DOCUMENT_MARK = "k:help ";
    private static final String FILE_MARK_SHORT = "k:3 ";
    private static final String FILE_MARK = "k:reportlets ";
    private static final String CPT_MARK = "k:cpt ";
    private static final String FRM_MARK = "k:frm ";
    private static final String DS_MARK = "k:ds ";
    private static final String DS_NAME = "dsname=\"";
    private static final String PLUGIN_MARK_SHORT = "k:4 ";
    private static final String PLUGIN_MARK = "k:shop ";

    private static final int TIMER_DELAY = 300;

    private static final String PLACE_HOLDER = com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Report_AlphaFine");

    private static final String SETTING = com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Report_Set");

    private static final String NO_RESULT = com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Report_No_Result");

    private static final String SKILLS = com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Report_AlphaFine_Skills");

    private static final String SEARCH_TERM = com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Report_AlphaFine_Search_Term");

    private static final String SEARCH = com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Report_AlphaFine_Search");

    private static final String GO_FORUM = com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Report_AlphaFine_Go_Forum");

    private static final String TEMPLATES = com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Report_Templates");

    public static final String PRODUCT_NEWS = com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Report_AlphaFine_Product_News");

    private static final String HELP = com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Report_Community_Help");

    private static final String PLUGIN = com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Report_Plugin_Addon");

    private static final String ONE_CLICK_READ = com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Report_AlphaFine_One_Click_Read");

    private static final String NO_SEARCH_RESULT = com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Report_AlphaFine_NO_Result");

    private static final String PRODUCT_DYNAMICS = com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Report_AlphaFine_Product_Dynamics");

    private static final Image SEARCH_IMAGE = SVGLoader.load("/com/fr/design/mainframe/alphafine/images/search.svg");

    private static final Color BORDER_COLOR = new Color(232, 232, 233);

    private final CardLayout cardLayout = new CardLayout();

    private final JPanel resultPane = new JPanel(cardLayout);

    private String storeText;

    private String[] segmentationResult;

    private UILabel useTipLabel;

    private UILabel tipIconLabel;

    private AlphaFineTextField searchTextField;

    private AlphaFineList searchResultList;

    private SearchLoadingPane searchLoadingPane;

    private JPanel searchTextFieldWrapperPane;

    private UILabel clearLabel;

    private JPanel tabPane;

    private CellType selectedType;

    private String beforeSearchStr = StringUtils.EMPTY;

    private SearchWorkerManager settingSearchWorkerManager;

    private SearchWorkerManager fileSearchWorkerManager;

    private SearchWorkerManager documentWorkerManager;

    private SearchWorkerManager pluginSearchWorkerManager;

    private SearchWorkerManager currentSearchWorkerManager;

    private ProductNewsSearchWorkerManager productNewsSearchWorkerManager;

    public AlphaFineFrame() {
        this.setTitle(AlphaFineConstants.TITLE);
        setUndecorated(true);
        setSize(AlphaFineConstants.FIELD_SIZE);
        initComponents();
        centerWindow(this);
        initSearchManager();
    }

    private void initSearchManager() {

        this.productNewsSearchWorkerManager = new ProductNewsSearchWorkerManager(
                CellType.PRODUCT_NEWS,
                searchTextBean -> {
                    return ProductNewsSearchManager.getInstance().getSearchResult(searchTextBean.getSegmentation());
                },
                this
        );

        this.settingSearchWorkerManager = new SearchWorkerManager(
                CellType.ACTION,
                searchTextBean -> ActionSearchManager.getInstance().getSearchResult(searchTextBean),
                this,
                new SimpleRightSearchResultPane(new NoResultPane(NO_RESULT, AlphaFineConstants.NO_RESULT_ICON))
        );
        fileSearchWorkerManager = new SearchWorkerManager(
                CellType.FILE,
                searchTextBean -> FileSearchManager.getInstance().getSearchResult(searchTextBean),
                this,
                new LoadingRightSearchResultPane()
        );
        documentWorkerManager = new SearchWorkerManager(
                CellType.DOCUMENT,
                searchTextBean -> DocumentSearchManager.getInstance().getSearchResult(searchTextBean),
                this,
                new SimpleRightSearchResultPane(new JPanel())
        );

        pluginSearchWorkerManager = new SearchWorkerManager(
                CellType.PLUGIN,
                searchTextBean -> PluginSearchManager.getInstance().getSearchResult(searchTextBean),
                this,
                new LoadingRightSearchResultPane()
        );

    }

    /**
     * 初始化全部组件
     */
    private void initComponents() {

        add(createTopPane(), BorderLayout.NORTH);
        initSearchTextField();
        add(createSearchPane(), BorderLayout.CENTER);
        add(createShowPane(), BorderLayout.SOUTH);
        this.getContentPane().setBackground(Color.WHITE);
        this.setIconImage(SEARCH_IMAGE);
        this.setSize(AlphaFineConstants.FULL_SIZE);
    }

    private JPanel createTopPane() {
        JPanel topPane = new JPanel(new BorderLayout());
        topPane.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
        topPane.setBackground(Color.WHITE);
        JPanel topLeftPane = new JPanel(new FlowLayout(FlowLayout.LEFT));
        topLeftPane.setBackground(Color.WHITE);
        UILabel alphaFineLabel = new UILabel(AlphaFineConstants.TITLE);
        alphaFineLabel.setFont(new Font("Arial Black", Font.PLAIN, 20));
        alphaFineLabel.setForeground(UIConstants.FLESH_BLUE);
        topLeftPane.add(alphaFineLabel);
        topPane.add(topLeftPane, BorderLayout.WEST);

        JPanel topRightPane = new JPanel(new FlowLayout(FlowLayout.RIGHT, 10, 10));
        topRightPane.setBackground(Color.WHITE);
        JPanel tipPane = new JPanel(new BorderLayout());
        tipPane.setBackground(Color.WHITE);
        tipIconLabel = new UILabel(AlphaFineConstants.BULB_ICON);
        tipIconLabel.addMouseListener(tipMouseListener);
        useTipLabel = new UILabel(SKILLS);
        useTipLabel.addMouseListener(tipMouseListener);
        useTipLabel.setForeground(AlphaFineConstants.FOREGROUND_COLOR_6);
        tipPane.add(tipIconLabel, BorderLayout.WEST);
        tipPane.add(useTipLabel, BorderLayout.CENTER);
        topRightPane.add(tipPane);
        UIButton minimizeButton = createButton(IconUtils.readIcon("/com/fr/design/mainframe/alphafine/images/minimize.svg"));
        minimizeButton.addActionListener(e -> AlphaFineFrame.this.setExtendedState(JFrame.ICONIFIED));
        topRightPane.add(minimizeButton);
        UIButton closeButton = createButton(IconUtils.readIcon("/com/fr/design/mainframe/alphafine/images/close.svg"));
        closeButton.addActionListener(e -> AlphaFineFrame.this.dispose());
        topRightPane.add(closeButton);
        topPane.add(topRightPane, BorderLayout.EAST);
        return topPane;
    }

    private MouseAdapter tipMouseListener = new MouseAdapter() {

        private JPopupMenu popupMenu;

        @Override
        public void mouseEntered(MouseEvent e) {
            tipIconLabel.setIcon(AlphaFineConstants.YELLOW_BULB_ICON);
        }

        @Override
        public void mouseExited(MouseEvent e) {
            if (popupMenu == null || !popupMenu.isShowing()) {
                useTipLabel.setForeground(AlphaFineConstants.FOREGROUND_COLOR_6);
                tipIconLabel.setIcon(AlphaFineConstants.BULB_ICON);
            }
        }

        @Override
        public void mousePressed(MouseEvent e) {
            useTipLabel.setForeground(UIConstants.FLESH_BLUE);
            tipIconLabel.setIcon(AlphaFineConstants.LIGHT_YELLOW_BULB_ICON);
            popupMenu = createTipPop();
            GUICoreUtils.showPopupMenu(popupMenu, e.getComponent(), e.getComponent().getX() - 60, e.getComponent().getY() + 20);
        }
    };


    private JPopupMenu createTipPop() {
        JPanel panel = new JPanel(new BorderLayout());
        String toolTip = AlphaFineShortCutUtil.getDisplayShortCut(com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Report_AlphaFine_Short_Cut", DesignerEnvManager.getEnvManager().getAlphaFineConfigManager().getShortcuts()));
        UILabel label = new UILabel(toolTip);
        label.setForeground(AlphaFineConstants.FOREGROUND_COLOR_8);
        label.setBackground(Color.WHITE);
        panel.add(label);
        panel.setBackground(Color.WHITE);
        JPopupMenu popupMenu = new JPopupMenu();
        popupMenu.setBorder(BorderFactory.createEmptyBorder(20, 5, 10, 5));
        popupMenu.add(panel);
        popupMenu.setBackground(Color.WHITE);
        popupMenu.addPopupMenuListener(new PopupMenuListener() {
            @Override
            public void popupMenuWillBecomeVisible(PopupMenuEvent e) {
              // do nothing
            }

            @Override
            public void popupMenuWillBecomeInvisible(PopupMenuEvent e) {
                useTipLabel.setForeground(AlphaFineConstants.FOREGROUND_COLOR_6);
                tipIconLabel.setIcon(AlphaFineConstants.BULB_ICON);
            }

            @Override
            public void popupMenuCanceled(PopupMenuEvent e) {
                // do nothing
            }
        });
        return popupMenu;
    }

    private JPanel createSearchPane() {
        JPanel searchPane = new JPanel(new BorderLayout());
        searchPane.setBorder(BorderFactory.createEmptyBorder(0, 20, 0, 20));
        searchTextFieldWrapperPane = new JPanel(new BorderLayout()) {
            @Override
            protected void paintBorder(Graphics g) {
                g.setColor(BORDER_COLOR);
                g.drawRoundRect(0, 0, getWidth() - 1, getHeight() - 1, 5, 5);
            }
        };
        searchTextFieldWrapperPane.setBorder(new UITextFieldBorder(new Insets(2, 3, 2, 3)));
        searchTextFieldWrapperPane.setBackground(Color.WHITE);
        searchTextFieldWrapperPane.add(searchTextField, BorderLayout.CENTER);
        clearLabel = new UILabel(IconUtils.readIcon("/com/fr/design/mainframe/alphafine/images/clear.svg"));
        clearLabel.setVisible(false);
        clearLabel.addMouseListener(new MouseAdapter() {
            @Override
            public void mousePressed(MouseEvent e) {
                searchTextField.setText(StringUtils.EMPTY);
                beforeSearchStr = StringUtils.EMPTY;
                clearLabel.setVisible(false);
            }
        });
        searchTextFieldWrapperPane.add(clearLabel, BorderLayout.EAST);
        searchPane.add(searchTextFieldWrapperPane, BorderLayout.CENTER);
        JButton searchButton = new JButton(SEARCH) {
            @Override
            public void paintComponent(Graphics g) {
                Graphics2D g2d = (Graphics2D) g;
                g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
                g2d.setColor(UIConstants.FLESH_BLUE);
                g2d.fillRoundRect(0, 0, getWidth(), getHeight(), 4, 4);
                super.paintComponent(g2d);
            }
        };
        searchButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                fireSearch();
            }
        });
        searchButton.setPreferredSize(new Dimension(70, 60));
        searchButton.setForeground(Color.WHITE);
        searchButton.setBorderPainted(false);
        searchButton.setContentAreaFilled(false);
        searchPane.add(searchButton, BorderLayout.EAST);
        searchPane.setBackground(Color.WHITE);
        return searchPane;
    }

    private JPanel createShowPane() {
        JPanel showPane = new JPanel(new BorderLayout());
        resultPane.add(new DefaultProductNewsPane(), CellType.PRODUCT_NEWS.getFlagStr4None());
        resultPane.add(new NoResultWithLinkPane(GO_FORUM, AlphaFineConstants.NO_RESULT_ICON), CellType.NO_RESULT.getFlagStr4None());
        resultPane.add(new NoResultPane(SEARCH_TERM, AlphaFineConstants.NO_RESULT_ICON), CellType.ACTION.getFlagStr4None());
        resultPane.add(new NoResultPane(SEARCH_TERM, AlphaFineConstants.NO_RESULT_ICON), CellType.FILE.getFlagStr4None());
        resultPane.add(new NoResultPane(SEARCH_TERM, AlphaFineConstants.NO_RESULT_ICON), CellType.PLUGIN.getFlagStr4None());
        resultPane.add(new HelpDocumentNoResultPane(SEARCH_TERM, AlphaFineConstants.NO_RESULT_ICON), CellType.DOCUMENT.getFlagStr4None());
        resultPane.add(new NetWorkFailedPane(this::reSearch), AlphaFineConstants.NETWORK_ERROR);

        JPanel labelPane = new JPanel(new BorderLayout());
        labelPane.setBorder(BorderFactory.createEmptyBorder(0, 20, 0, 20));
        labelPane.setBackground(Color.WHITE);
        JPanel labelContentPane = new JPanel(new BorderLayout());
        UILabel tabLabel = new UILabel(PRODUCT_DYNAMICS);
        tabLabel.setForeground(AlphaFineConstants.FOREGROUND_COLOR_6);
        tabLabel.setBorder(BorderFactory.createEmptyBorder(0, 10, 0, 0));
        tabLabel.setPreferredSize(new Dimension(100, 30));
        JPanel westPane = new JPanel(new FlowLayout(FlowLayout.LEFT, 0, 0));
        westPane.add(tabLabel);
        labelContentPane.add(westPane, BorderLayout.WEST);
        JPanel eastPane = new JPanel(new FlowLayout(FlowLayout.RIGHT, 0, 0));
        UILabel readLabel = new UILabel(ONE_CLICK_READ);
        readLabel.setHorizontalAlignment(SwingConstants.RIGHT);
        readLabel.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 10));;
        readLabel.setPreferredSize(new Dimension(100, 30));
        readLabel.setForeground(UIConstants.FLESH_BLUE);
        readLabel.addMouseListener(new MouseAdapter() {
            @Override
            public void mousePressed(MouseEvent e) {
                fireOneClickRead();
                showPane.repaint();
            }
        });
        eastPane.add(readLabel);
        labelContentPane.add(eastPane, BorderLayout.EAST);
        labelContentPane.setBackground(new Color(245, 245, 247));
        labelPane.add(labelContentPane);
        labelPane.setPreferredSize(new Dimension(AlphaFineConstants.FULL_SIZE.width, 30));

        tabPane = new JPanel(new FlowLayout(FlowLayout.LEFT, 20, 10));
        tabPane.setBackground(Color.WHITE);
        List<SelectedLabel> selectedLabelList = createSelectedLabelList();
        selectedType = selectedLabelList.get(0).getCellType();
        // 第一个tab 非产品动态
        if (selectedType != CellType.PRODUCT_NEWS) {
            tabLabel.setText(selectedLabelList.get(0).getText());
            readLabel.setVisible(false);
        }
        for (SelectedLabel selectedLabel : selectedLabelList) {
            selectedLabel.addMouseListener(createMouseListener(selectedLabelList, selectedLabel, tabPane, tabLabel, readLabel));
            tabPane.add(selectedLabel);
        }
        showPane.add(tabPane, BorderLayout.NORTH);
        showPane.add(labelPane, BorderLayout.CENTER);
        showPane.add(resultPane, BorderLayout.SOUTH);
        return showPane;
    }

    private MouseAdapter createMouseListener(List<SelectedLabel> selectedLabelList, SelectedLabel selectedLabel,
                                             JPanel tabPane, UILabel tabLabel, UILabel readLabel) {
        return new MouseAdapter() {
            @Override
            public void mousePressed(MouseEvent e) {
                for (SelectedLabel label : selectedLabelList) {
                    label.setSelected(false);
                    label.setForeground(AlphaFineConstants.FOREGROUND_COLOR_8);
                }
                selectedLabel.setSelected(true);
                // 处理产品动态 tab与下方文字展示不一致
                if (ComparatorUtils.equals(selectedLabel.getText().trim(), PRODUCT_NEWS)) {
                    tabLabel.setText(PRODUCT_DYNAMICS);
                } else {
                    tabLabel.setText(selectedLabel.getText());
                }
                readLabel.setVisible(false);
                tabPane.repaint();
                switch (selectedLabel.getCellType()) {
                    case PRODUCT_NEWS:
                        readLabel.setVisible(true);
                        switchType(CellType.PRODUCT_NEWS);
                        break;
                    case ACTION:
                        currentSearchWorkerManager = settingSearchWorkerManager;
                        switchType(CellType.ACTION);
                        break;
                    case FILE:
                        currentSearchWorkerManager = fileSearchWorkerManager;
                        switchType(CellType.FILE);
                        break;
                    case DOCUMENT:
                        currentSearchWorkerManager = documentWorkerManager;
                        switchType(CellType.DOCUMENT);
                        break;
                    case PLUGIN:
                        currentSearchWorkerManager = pluginSearchWorkerManager;
                        switchType(CellType.PLUGIN);
                        break;
                }
                if (currentSearchWorkerManager != null) {
                    AlphaFineList alphaFineList = currentSearchWorkerManager.getSearchResultList();
                    if (alphaFineList != null) {
                        alphaFineList.setSelectedIndex(0);
                    }
                }
            }

            private Color defaultColor;

            @Override
            public void mouseEntered(MouseEvent e) {
                defaultColor = selectedLabel.getForeground();
                selectedLabel.setForeground(AlphaFineConstants.SUSPENDED_COLOR);
            }

            @Override
            public void mouseExited(MouseEvent e) {
                selectedLabel.setForeground(defaultColor);
            }
        };
    }

    private List<SelectedLabel> createSelectedLabelList() {
        List<SelectedLabel> selectedLabelList = new ArrayList<>();
        AlphaFineConfigManager alphaFineConfigManager = DesignerEnvManager.getEnvManager().getAlphaFineConfigManager();
        if (alphaFineConfigManager.isProductDynamics()) {
            selectedLabelList.add(new SelectedLabel(PRODUCT_NEWS, CellType.PRODUCT_NEWS, true));
        }
        if (alphaFineConfigManager.isContainAction()) {
            selectedLabelList.add(new SelectedLabel(SETTING, CellType.ACTION));
        }
        if (alphaFineConfigManager.isContainFileContent() || alphaFineConfigManager.isContainTemplate()) {
            selectedLabelList.add(new SelectedLabel(TEMPLATES, CellType.FILE));
        }
        if (alphaFineConfigManager.isContainDocument()) {
            selectedLabelList.add(new SelectedLabel(HELP, CellType.DOCUMENT));
        }
        if (alphaFineConfigManager.isContainPlugin()) {
            selectedLabelList.add(new SelectedLabel(PLUGIN, CellType.PLUGIN));
        }
        return selectedLabelList;
    }

    private void fireOneClickRead() {
        List<ProductNews> productNewsList = ProductNewsSearchManager.getInstance().getCachedProductNewsList();
        Set<Long> readSet = DesignerEnvManager.getEnvManager().getAlphaFineConfigManager().getReadSet();
        for (ProductNews productNews : productNewsList) {
            readSet.add(productNews.getId());
        }
    }

    private void switchType(CellType cellType) {
        this.selectedType = cellType;
        if (StringUtils.isEmpty(searchTextField.getText())) {
            cardLayout.show(resultPane, cellType.getFlagStr4None());
        } else {
            // 当前搜索未结束 不切换loading
            if (!checkSearchLoading()) {
                return;
            }
            // 所有都搜索都结束 移除loading
            if (isAllSearchOver()) {
                resultPane.remove(searchLoadingPane);
            }

            // 网络异常
            if (checkNetworkError()) {
                return;
            }

            cardLayout.show(resultPane, cellType.getFlagStr4Result());
            checkSearchResult();
        }

    }

    private boolean checkNetworkError() {
        boolean networkError;
        if (selectedType == CellType.PRODUCT_NEWS) {
            networkError = productNewsSearchWorkerManager.isNetWorkError();
        } else {
            networkError = currentSearchWorkerManager.isNetWorkError();
        }
        cardLayout.show(resultPane, AlphaFineConstants.NETWORK_ERROR);
        return networkError;
    }

    private boolean checkSearchLoading() {
        boolean searchOver;
        if (selectedType == CellType.PRODUCT_NEWS) {
            searchOver = productNewsSearchWorkerManager.isSearchOver();
        } else {
            searchOver = currentSearchWorkerManager.isSearchOver();
        }
        cardLayout.show(resultPane, AlphaFineConstants.LOADING);
        return searchOver;
    }

    private boolean isAllSearchOver() {
        return productNewsSearchWorkerManager.isSearchOver()
                && pluginSearchWorkerManager.isSearchOver()
                && fileSearchWorkerManager.isSearchOver()
                && settingSearchWorkerManager.isSearchOver()
                && documentWorkerManager.isSearchOver();
    }

    private void checkSearchResult() {
        if (currentSearchWorkerManager == null) {
            return;
        }
        searchResultList = currentSearchWorkerManager.getSearchResultList();
        if (searchResultList != null) {
            searchResultList.requestFocus();
        }
        boolean hasSearchResult = true;
        if (selectedType == CellType.PRODUCT_NEWS) {
            hasSearchResult = productNewsSearchWorkerManager.hasSearchResult();
        } else {
            hasSearchResult = currentSearchWorkerManager.hasSearchResult();
        }

        if (!hasSearchResult) {
            cardLayout.show(resultPane, CellType.NO_RESULT.getFlagStr4None());
        }

    }

    private void initSearchTextField() {
        searchTextField = new AlphaFineTextField(PLACE_HOLDER);
        initTextFieldListener();
        searchTextField.setFont(DesignUtils.getDefaultGUIFont().applySize(14));
        searchTextField.setBackground(Color.WHITE);
        searchTextField.setPreferredSize(new Dimension(300, 60));
        searchTextField.setBorder(null);
    }


    private void initTextFieldListener() {
        searchTextField.addKeyListener(new KeyAdapter() {
            @Override
            public void keyPressed(KeyEvent e) {
                // 搜索提示框
                if (StringUtils.isNotEmpty(searchTextField.getText())) {
                    clearLabel.setVisible(true);
                    SearchTooltipPopup.getInstance().show(searchTextFieldWrapperPane);
                } else {
                    beforeSearchStr = StringUtils.EMPTY;
                }
                AlphaFineToolTipList alphaFineToolTipList = SearchTooltipPopup.getInstance().getAlphaFineToolTipList();
                if (e.getKeyCode() == KeyEvent.VK_ENTER) {
                    if (!alphaFineToolTipList.isSelectionEmpty()) {
                        fireSearch(alphaFineToolTipList.getSelectedValue());
                        return;
                    }
                    fireSearch();
                }  else if (e.getKeyCode() == KeyEvent.VK_DOWN) {
                    if (alphaFineToolTipList.getSelectedIndex() == alphaFineToolTipList.getModel().getSize() - 1) {
                        alphaFineToolTipList.setSelectedIndex(0);
                    }
                    alphaFineToolTipList.setSelectedIndex(alphaFineToolTipList.getSelectedIndex() + 1);
                } else if (e.getKeyCode() == KeyEvent.VK_UP) {
                    alphaFineToolTipList.setSelectedIndex(alphaFineToolTipList.getSelectedIndex() - 1);
                }

            }
        });

        searchTextField.addFocusListener(new FocusListener() {
            @Override
            public void focusGained(FocusEvent e) {
                if (StringUtils.isNotEmpty(searchTextField.getText())) {
                    SearchTooltipPopup.getInstance().show(searchTextFieldWrapperPane);
                }
            }

            @Override
            public void focusLost(FocusEvent e) {
                if (e.getOppositeComponent() != SearchTooltipPopup.getInstance().getAlphaFineToolTipList()) {
                    SearchTooltipPopup.getInstance().hide();
                }
            }
        });

        startSearchTextFieldTimer();

    }

    private void startSearchTextFieldTimer() {
        Timer timer = new Timer(TIMER_DELAY, e -> {
            // 坑 isShowing返回false 即使textField有内容 getText返回的也是空
            if (searchTextField.isShowing() && StringUtils.isEmpty(searchTextField.getText())) {
                SearchTooltipPopup.getInstance().hide();
                clearLabel.setVisible(false);
                switchType(selectedType);
                beforeSearchStr = StringUtils.EMPTY;
            } else if (searchTextField.hasFocus()) {
                clearLabel.setVisible(true);
                SearchTooltipPopup.getInstance().show(searchTextFieldWrapperPane);
            }
            tabPane.repaint();

        });
        timer.start();
    }

    public void fireSearch(String text) {
        searchTextField.setText(text);
        fireSearch();
    }

    private void fireSearch() {
        // 焦点转移
        AlphaFineFrame.this.requestFocus();
        if (ComparatorUtils.equals(beforeSearchStr, searchTextField.getText())) {
            return;
        }
        if (StringUtils.isEmpty(searchTextField.getText())) {
            beforeSearchStr = StringUtils.EMPTY;
            return;
        }
        String lowerCaseSearchText = preProcessSearchText(searchTextField.getText().toLowerCase());
        if (DesignerEnvManager.getEnvManager().getAlphaFineConfigManager().isNeedSegmentationCheckbox()) {
            dealSegmentationSearch(lowerCaseSearchText);
        } else {
            dealNormalSearch();
        }
        DesignerEnvManager.getEnvManager().getAlphaFineConfigManager().getHistorySearch().push(searchTextField.getText());
        doSearch(lowerCaseSearchText);
        beforeSearchStr = searchTextField.getText();
        SearchTooltipPopup.getInstance().hide();
    }

    /**
     * 普通搜索
     */
    private void dealNormalSearch() {
        String searchText = preProcessSearchText(searchTextField.getText());
        if (StringUtils.isEmpty(getRealSearchText(searchText))) {
            segmentationResult = null;
        } else {
            segmentationResult = new String[]{getRealSearchText(searchText)};
        }
    }

    /**
     * 分词搜索
     */
    private void dealSegmentationSearch(String lowerCaseSearchText) {
        //是高级搜索
        if (searchTextField.getText().toLowerCase().startsWith(ADVANCED_SEARCH_MARK)) {
            segmentationResult = SegmentationManager.getInstance().startSegmentation(getStoreText(lowerCaseSearchText));
        }
        //是普通搜索
        else {
            segmentationResult = SegmentationManager.getInstance().startSegmentation(lowerCaseSearchText);
        }
    }

    /**
     * 文本预处理 限制长度
     *
     * @param text
     * @return
     */
    private String preProcessSearchText(String text) {
        if (text.length() > AlphaFineConstants.LEN_LIMIT) {
            return text.substring(0, AlphaFineConstants.LEN_LIMIT);
        } else {
            return text;
        }
    }

    private void dealWithSearchResult() {
        final AlphaCellModel model = searchResultList.getSelectedValue();
        if (model != null) {
            model.doAction();
        }
    }

    public void showResult(String flag) {
        cardLayout.show(resultPane, flag);
    }

    public void addResult(JPanel panel, String flag) {
        resultPane.add(panel, flag);
    }

    public void removeSearchResultPane(JPanel panel) {
        resultPane.remove(panel);
    }



    private void doSearch(String text) {
        initSearchLoadingPane();
        SearchTextBean searchTextBean = generateSearchTextBean(text);
        this.productNewsSearchWorkerManager.doSearch(searchTextBean);
        this.settingSearchWorkerManager.doSearch(searchTextBean);
        this.fileSearchWorkerManager.doSearch(searchTextBean);
        this.documentWorkerManager.doSearch(searchTextBean);
        this.pluginSearchWorkerManager.doSearch(searchTextBean);
    }

    private SearchTextBean generateSearchTextBean(String searchText) {
        if (searchText.startsWith(ACTION_MARK_SHORT) || searchText.startsWith(ACTION_MARK)
                || searchText.startsWith(DOCUMENT_MARK_SHORT) || searchText.startsWith(DOCUMENT_MARK)) {
            return new SearchTextBean(StringUtils.EMPTY, new String[]{getStoreText(searchText)});
        } else if (searchText.startsWith(FILE_MARK_SHORT) || searchText.startsWith(FILE_MARK)
                || searchText.startsWith(CPT_MARK) || searchText.startsWith(FRM_MARK)
                || searchText.startsWith(PLUGIN_MARK_SHORT) || searchText.startsWith(PLUGIN_MARK)) {
            return new SearchTextBean(getStoreText(searchText), new String[]{getStoreText(searchText)});
        } else if (searchText.startsWith(DS_MARK)) {
            return new SearchTextBean(getStoreText(searchText), new String[]{DS_NAME + getStoreText(searchText)});
        }   else {
            return new SearchTextBean(searchText, segmentationResult == null ? new String[]{} : segmentationResult);
        }
    }

    /**
     * 仅搜索依赖网络的搜索项
     *
     */
    private void reSearch() {
        String text = preProcessSearchText(this.searchTextField.getText().toLowerCase());
        if (StringUtils.isEmpty(text)) {
            return;
        }
        searchLoadingPane = new SearchLoadingPane();
        SearchTextBean searchTextBean = new SearchTextBean(text, segmentationResult);
        this.productNewsSearchWorkerManager.doSearch(searchTextBean);
        this.documentWorkerManager.doSearch(searchTextBean);
        this.pluginSearchWorkerManager.doSearch(searchTextBean);
    }

    private void initSearchLoadingPane() {
        if (searchLoadingPane == null) {
            searchLoadingPane = new SearchLoadingPane();
        }
        resultPane.add(searchLoadingPane, AlphaFineConstants.LOADING);
        cardLayout.show(resultPane, AlphaFineConstants.LOADING);
    }

    public String getSearchText() {
        return searchTextField.getText();
    }


    public CellType getSelectedType() {
        return selectedType;
    }

    public void setStoreText(String storeText) {
        this.storeText = storeText;
    }

    /**
     * 截取字符串中关键词
     *
     * @param searchText
     * @return
     */
    private String getStoreText(String searchText) {
        //这里也需要先做一个去除不需要空格的处理
        setStoreText((searchText.substring(searchText.indexOf(StringUtils.BLANK) + 1)).replaceAll(StringUtils.BLANK, StringUtils.EMPTY));
        return storeText;
    }


    /**
     * 去除特殊字符，空格等
     */
    private String getRealSearchText(String searchText) {
        searchText = searchText.toLowerCase();
        Pattern p = Pattern.compile(AlphaFineConstants.SPECIAL_CHARACTER_REGEX);
        Matcher m = p.matcher(searchText);
        searchText = m.replaceAll(StringUtils.EMPTY).trim().replaceAll(StringUtils.BLANK, StringUtils.EMPTY);
        if (searchText.length() == 0) {
            return null;
        }
        return searchText;
    }

    private UIButton createButton(Icon icon) {
        UIButton button = new UIButton() {
            @Override
            public void paintComponent(Graphics g) {
                g.setColor(Color.WHITE);
                g.fillRect(0, 0, getSize().width, getSize().height);
                super.paintComponent(g);
            }
        };
        button.setPreferredSize(new Dimension(20, 20));
        button.setIcon(icon);
        button.set4ToolbarButton();
        button.setBorderPainted(false);
        button.setRolloverEnabled(false);
        return button;
    }

    /**
     * 设置面板位置
     *
     * @param win
     */
    private void centerWindow(Window win) {
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();

        Dimension winSize = win.getSize();

        if (winSize.height > screenSize.height) {
            winSize.height = screenSize.height;
        }
        if (winSize.width > screenSize.width) {
            winSize.width = screenSize.width;
        }
        //这里设置位置：水平居中，竖直偏上
        win.setLocation((screenSize.width - winSize.width) / 2, (screenSize.height - winSize.height) / AlphaFineConstants.SHOW_SIZE);
    }

    @Override
    public void setVisible(boolean b) {
        super.setVisible(b);
        QuestionWindow.getInstance().setVisible(!b);
        if (!b) {
            AlphaFineHelper.resetAlphaFineDialog();
        }
    }

    @Override
    public void dispose() {
        super.dispose();
        AlphaFineHelper.resetAlphaFineDialog();
        QuestionWindow.getInstance().setVisible(true);
    }

}
