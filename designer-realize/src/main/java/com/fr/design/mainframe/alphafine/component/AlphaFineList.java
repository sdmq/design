package com.fr.design.mainframe.alphafine.component;

import com.fr.design.mainframe.alphafine.AlphaFineConstants;
import com.fr.design.mainframe.alphafine.AlphaFineHelper;
import com.fr.design.mainframe.alphafine.cell.model.AlphaCellModel;
import com.fr.design.mainframe.alphafine.cell.model.DocumentModel;
import com.fr.design.mainframe.alphafine.preview.ResultShowPane;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.JList;

/**
 * @author hades
 * @version 11.0
 * Created by hades on 2022/4/18
 */
public class AlphaFineList extends JList<AlphaCellModel> {

    private ResultShowPane resultShowPane;

    public AlphaFineList() {
        addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                AlphaCellModel selectedValue = getSelectedValue();
                if (e.getClickCount() == AlphaFineConstants.DEFAULT_CLICK_COUNT && selectedValue.hasAction()) {
                    // 点击搜索结果 主页面移动到后面
                    if (!(selectedValue instanceof DocumentModel)) {
                        // 帮助文档不跳转
                        AlphaFineHelper.getAlphaFineDialog().toBack();
                    }
                    dealWithSearchResult();
                }
            }
        });

        addListSelectionListener(e -> {
            if (!e.getValueIsAdjusting() && getSelectedValue() != null) {
                if (resultShowPane != null) {
                    resultShowPane.showResult(getSelectedValue());
                }
            }
        });


        addKeyListener(new KeyAdapter() {
            @Override
            public void keyPressed(KeyEvent e) {
                if (e.getKeyCode() == KeyEvent.VK_ENTER) {
                    AlphaCellModel selectedValue = getSelectedValue();
                    if (selectedValue.hasAction() && !(selectedValue instanceof DocumentModel)) {
                        AlphaFineHelper.getAlphaFineDialog().toBack();
                    }
                    dealWithSearchResult();
                }
            }
        });
    }

    public void setResultShowPane(ResultShowPane resultShowPane) {
        this.resultShowPane = resultShowPane;
    }

    @Override
    public void setSelectedIndex(int index) {
        super.setSelectedIndex(index);
        AlphaCellModel alphaCellModel = getSelectedValue();
        if (resultShowPane != null && alphaCellModel != null) {
            resultShowPane.showResult(getSelectedValue());
        }
        ensureIndexIsVisible(getSelectedIndex());
    }

    private void dealWithSearchResult() {
        final AlphaCellModel model = this.getSelectedValue();
        if (model != null) {
            model.doAction();
        }
    }


}