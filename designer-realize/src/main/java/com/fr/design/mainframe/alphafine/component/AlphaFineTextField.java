package com.fr.design.mainframe.alphafine.component;

import com.fr.design.gui.itextfield.UITextField;
import com.fr.design.mainframe.alphafine.AlphaFineConstants;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;


/**
 * Created by XiaXiang on 2017/3/21.
 */
public class AlphaFineTextField extends UITextField {

    private static final int PLACE_HOLDER_GAP = 3;

    private String placeHolder;


    public AlphaFineTextField(String placeHolder) {
        this.placeHolder = placeHolder;
    }


    public AlphaFineTextField() {
        this.placeHolder = null;
    }

    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);

        if (super.getText().length() > 0 || placeHolder == null) {
            return;
        }

        Graphics2D g2 = (Graphics2D) g;
        g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        g2.setColor(AlphaFineConstants.FOREGROUND_COLOR_5);
        g2.drawString(placeHolder, getInsets().left, g.getFontMetrics().getMaxAscent() + getInsets().top + PLACE_HOLDER_GAP);
    }

    @Override
    protected void paintBorder(Graphics g) {
        // do nothing
    }

    public String getPlaceHolder() {
        return placeHolder;
    }
}
