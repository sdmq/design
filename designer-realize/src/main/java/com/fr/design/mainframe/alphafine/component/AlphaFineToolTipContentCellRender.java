package com.fr.design.mainframe.alphafine.component;

import com.fr.base.svg.IconUtils;
import com.fr.design.gui.ilable.UILabel;
import com.fr.design.mainframe.alphafine.AlphaFineConstants;
import com.fr.design.mainframe.alphafine.AlphaFineUtil;
import com.fr.general.ComparatorUtils;
import com.fr.stable.StringUtils;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import javax.swing.BorderFactory;
import javax.swing.Icon;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.ListCellRenderer;

/**
 * @author hades
 * @version 11.0
 * Created by hades on 2022/4/22
 */
public class AlphaFineToolTipContentCellRender implements ListCellRenderer<String> {

    private static final Color SELECTED_COLOR = new Color(65, 155, 249, 26);

    private static final Icon HOT_SEARCH_ICON = IconUtils.readIcon("/com/fr/design/mainframe/alphafine/images/hot_search.svg");

    private static final Icon SEARCH_ICON = IconUtils.readIcon("/com/fr/design/mainframe/alphafine/images/search.svg");

    private static final Icon HISTORY_SEARCH_ICON = IconUtils.readIcon("/com/fr/design/mainframe/alphafine/images/history_search.svg");


    @Override
    public Component getListCellRendererComponent(JList<? extends String> list, String value, int index,
                                                  boolean isSelected, boolean cellHasFocus) {

        if (StringUtils.isEmpty(value)) {
            return new LineCellRender().getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);
        }

        JPanel panel = new JPanel(new BorderLayout());
        panel.setBackground(null);

        UILabel iconLabel = new UILabel();
        iconLabel.setBorder(BorderFactory.createEmptyBorder(0, 10, 5, 0));
        iconLabel.setForeground(AlphaFineConstants.FOREGROUND_COLOR_8);
        iconLabel.setText(value);
        if (ComparatorUtils.equals(value, AlphaFineConstants.HOT_SEARCH)) {
            iconLabel.setIcon(HOT_SEARCH_ICON);
        } else if (AlphaFineConstants.HOT_SEARCH_SET.contains(value)) {
            iconLabel.setIcon(SEARCH_ICON);
        } else {
            iconLabel.setIcon(HISTORY_SEARCH_ICON);
        }

        if (isSelected && !ComparatorUtils.equals(value, AlphaFineConstants.HOT_SEARCH)) {
            iconLabel.setText(AlphaFineUtil.highLightModelName(value, new String[]{value}));
            panel.setBackground(SELECTED_COLOR);
        }
        panel.add(iconLabel, BorderLayout.WEST);
        panel.setBorder(BorderFactory.createEmptyBorder(5, 0, 0, 0));
        panel.setPreferredSize(new Dimension(640, 32));

        return panel;
    }
}
