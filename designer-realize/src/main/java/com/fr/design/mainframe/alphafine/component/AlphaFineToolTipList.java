package com.fr.design.mainframe.alphafine.component;

import com.fr.design.mainframe.alphafine.AlphaFineConstants;
import com.fr.design.mainframe.alphafine.AlphaFineHelper;
import com.fr.general.ComparatorUtils;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.JList;

/**
 * @author hades
 * @version 11.0
 * Created by hades on 2022/4/22
 */
public class AlphaFineToolTipList extends JList<String> {

    public AlphaFineToolTipList() {
        addKeyListener(new KeyAdapter() {
            @Override
            public void keyPressed(KeyEvent e) {
                if (e.getKeyCode() == KeyEvent.VK_ENTER) {
                    AlphaFineHelper.getAlphaFineDialog().fireSearch(getSelectedValue());
                }
            }
        });

        addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                if (e.getClickCount() == 2) {
                    if (ComparatorUtils.equals(getSelectedValue(), AlphaFineConstants.HOT_SEARCH)) {
                        return;
                    }
                    AlphaFineHelper.getAlphaFineDialog().fireSearch(getSelectedValue());
                }
            }
        });
    }

}
