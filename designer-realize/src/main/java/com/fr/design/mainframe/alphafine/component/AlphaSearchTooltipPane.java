package com.fr.design.mainframe.alphafine.component;

import com.fr.design.DesignerEnvManager;
import com.fr.design.gui.icontainer.UIScrollPane;
import com.fr.design.mainframe.alphafine.AlphaFineConstants;
import java.awt.Color;
import java.awt.Dimension;
import java.util.Stack;
import javax.swing.DefaultListModel;
import javax.swing.JPanel;

/**
 * @author hades
 * @version 11.0
 * Created by hades on 2022/4/22
 */
public class AlphaSearchTooltipPane extends JPanel {

    private AlphaFineToolTipList alphaFineToolTipList;

    public AlphaSearchTooltipPane() {
        alphaFineToolTipList = new AlphaFineToolTipList();
        alphaFineToolTipList.setCellRenderer(new AlphaFineToolTipContentCellRender());
        alphaFineToolTipList.setModel(getDefaultListModel());
        UIScrollPane scrollPane = new UIScrollPane(alphaFineToolTipList);
        scrollPane.setBorder(null);
        scrollPane.setBackground(Color.WHITE);
        this.add(scrollPane);
        this.setPreferredSize(new Dimension(640, 250));
        this.setBackground(Color.WHITE);
    }

    public AlphaFineToolTipList getAlphaFineToolTipList() {
        return alphaFineToolTipList;
    }

    private DefaultListModel<String> getDefaultListModel() {
        DefaultListModel<String> defaultListModel = new DefaultListModel<>();
        defaultListModel.addElement(AlphaFineConstants.HOT_SEARCH);
        for (String content : AlphaFineConstants.HOT_SEARCH_SET) {
            defaultListModel.addElement(content);
        }
        return defaultListModel;
    }

    public void refreshHistory() {
        Stack<String> stack = DesignerEnvManager.getEnvManager().getAlphaFineConfigManager().getHistorySearch();
        if (stack.isEmpty()) {
            return;
        }
        DefaultListModel<String> defaultListModel = new DefaultListModel<>();
        for (int i = stack.size() - 1; i >= 0; i--) {
            defaultListModel.addElement(stack.get(i));
        }
//        defaultListModel.addElement(StringUtils.EMPTY);
        defaultListModel.addElement(AlphaFineConstants.HOT_SEARCH);
        for (String content : AlphaFineConstants.HOT_SEARCH_SET) {
            defaultListModel.addElement(content);
        }
        alphaFineToolTipList.setModel(defaultListModel);
    }
}
