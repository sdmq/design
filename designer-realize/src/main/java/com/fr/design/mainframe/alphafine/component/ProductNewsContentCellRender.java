package com.fr.design.mainframe.alphafine.component;

import com.fr.design.constants.UIConstants;
import com.fr.design.gui.ilable.UILabel;
import com.fr.design.mainframe.alphafine.AlphaFineConstants;
import com.fr.design.mainframe.alphafine.AlphaFineUtil;
import com.fr.design.mainframe.alphafine.model.ProductNews;
import com.fr.design.utils.DesignUtils;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Graphics;
import java.text.SimpleDateFormat;
import javax.swing.BorderFactory;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.ListCellRenderer;

/**
 * @author hades
 * @version 11.0
 * Created by hades on 2022/4/19
 */
public class ProductNewsContentCellRender implements ListCellRenderer<Object> {

    private static final String FINE_REPORT = "FineReport";

    private static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("yyyy/MM/dd");

    /**
     * 透明灰色背景 Y方向偏移
     */
    private static final int GRAY_BACKGROUND_Y_GAP  = 39;

    /**
     * 透明灰色背景 高度
     */
    private static final int GRAY_BACKGROUND_HEIGHT = 23;

    /**
     * 单行产品动态的高度与宽度尺寸
     */
    private static final Dimension DEFAULT_DIMENSION = new Dimension(500, 100);

    private String[] segmentationResult;

    private ProductNewsList productNewsList;

    public ProductNewsContentCellRender(String[] segmentationResult, ProductNewsList productNewsList) {
        this.segmentationResult = segmentationResult;
        this.productNewsList = productNewsList;
    }

    public ProductNewsContentCellRender(ProductNewsList productNewsList) {
        this(null, productNewsList);
    }

    @Override
    public Component getListCellRendererComponent(JList<?> list, Object value, int index, boolean isSelected,
                                                  boolean cellHasFocus) {
        ProductNews productNews = (ProductNews) value;
        JPanel panel = new JPanel(new BorderLayout());
        panel.setBorder(BorderFactory.createEmptyBorder(0, 0, 10, 0));
        panel.setBackground(Color.WHITE);

        panel.add(new ProductNewsImagePanel(productNews), BorderLayout.WEST);
        JPanel textPane = new JPanel(new BorderLayout());
        textPane.setBorder(BorderFactory.createEmptyBorder(0, 10, 10, 0));
        UILabel titleLabel = new UILabel(AlphaFineUtil.highLightModelName(productNews.getTitle(), segmentationResult));
        titleLabel.setFont(DesignUtils.getDefaultGUIFont().applySize(20));
        if (productNewsList.getHoverIndex() == index) {
            titleLabel.setForeground(UIConstants.FLESH_BLUE);
        }

        textPane.add(titleLabel, BorderLayout.NORTH);
        JPanel infoPane = new JPanel(new BorderLayout());
        UILabel productLabel = new UILabel(FINE_REPORT) {
            @Override
            protected void paintComponent(Graphics g) {
                g.setColor(AlphaFineConstants.BACKGROUND_COLOR);
                g.fillRect(0, getHeight() - GRAY_BACKGROUND_Y_GAP, getWidth(), GRAY_BACKGROUND_HEIGHT);
                super.paintComponent(g);
            }
        };
        productLabel.setForeground(AlphaFineConstants.FOREGROUND_COLOR_6);
        infoPane.add(productLabel, BorderLayout.WEST);

        UILabel dateLabel = new UILabel(DATE_FORMAT.format(productNews.getPushDate()));
        dateLabel.setForeground(AlphaFineConstants.FOREGROUND_COLOR_6);
        dateLabel.setBorder(BorderFactory.createEmptyBorder(0, 10, 0, 0));
        infoPane.setBackground(Color.WHITE);
        infoPane.add(dateLabel, BorderLayout.CENTER);
        textPane.setBackground(Color.WHITE);
        textPane.add(infoPane, BorderLayout.CENTER);
        panel.add(textPane, BorderLayout.CENTER);
        panel.setPreferredSize(DEFAULT_DIMENSION);
        return panel;
    }
}
