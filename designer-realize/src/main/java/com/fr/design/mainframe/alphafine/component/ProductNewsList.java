package com.fr.design.mainframe.alphafine.component;

import com.fr.design.DesignerEnvManager;
import com.fr.design.mainframe.alphafine.AlphaFineConstants;
import com.fr.design.mainframe.alphafine.AlphaFineHelper;
import com.fr.design.mainframe.alphafine.model.ProductNews;
import com.fr.log.FineLoggerFactory;

import java.awt.Cursor;
import java.awt.Desktop;
import java.awt.Point;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;
import java.net.URI;
import javax.swing.DefaultListModel;
import javax.swing.JList;
import javax.swing.ListModel;

/**
 * @author hades
 * @version 11.0
 * Created by hades on 2022/4/21
 */
public class ProductNewsList extends JList<ProductNews> {

    private int hoverIndex = -1;

    public ProductNewsList(ListModel<ProductNews> dataModel) {
        super(dataModel);
        addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                if (e.getClickCount() == AlphaFineConstants.DEFAULT_CLICK_COUNT) {
                    dealWithClick();
                }
            }

            @Override
            public void mouseExited(MouseEvent e) {
                hoverIndex = -1;
                ProductNewsList.this.repaint();
            }
        });

        addMouseMotionListener(new MouseMotionAdapter() {
            @Override
            public void mouseMoved(MouseEvent e) {
                Point p = new Point(e.getX(), e.getY());
                int index = ProductNewsList.this.locationToIndex(p);
                setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
                if (index != hoverIndex) {
                    hoverIndex = index;
                    ProductNewsList.this.repaint();
                }
            }
        });
    }

    public ProductNewsList() {
        this(new DefaultListModel<>());
    }

    private void dealWithClick() {
        ProductNews productNews = getSelectedValue();
        try {
            Desktop.getDesktop().browse(new URI(productNews.getUrl()));
        } catch (Exception e) {
            FineLoggerFactory.getLogger().error(e.getMessage(), e);
        }
        DesignerEnvManager.getEnvManager().getAlphaFineConfigManager().getReadSet().add(productNews.getId());
        AlphaFineHelper.getAlphaFineDialog().repaint();
    }

    public int getHoverIndex() {
        return hoverIndex;
    }
}
