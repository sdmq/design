package com.fr.design.mainframe.alphafine.component;

import com.fr.design.gui.icontainer.UIScrollPane;
import com.fr.design.mainframe.alphafine.AlphaFineConstants;

import java.awt.BorderLayout;
import java.awt.Color;
import javax.swing.BorderFactory;
import javax.swing.JPanel;

/**
 * @author hades
 * @version 11.0
 * Created by hades on 2022/4/21
 */
public class ProductNewsSearchResultPane extends JPanel {

    private ProductNewsList productNewsList;

    public ProductNewsSearchResultPane(String[] segmentationResult) {

        productNewsList = new ProductNewsList();
        UIScrollPane scrollPane = new UIScrollPane(productNewsList);
        scrollPane.setBackground(Color.WHITE);
        scrollPane.setBorder(BorderFactory.createEmptyBorder(10, 20, 0, 20));
        productNewsList.setCellRenderer(new ProductNewsContentCellRender(segmentationResult, productNewsList));
        this.setLayout(new BorderLayout());
        this.setBackground(Color.WHITE);
        this.add(scrollPane);
        this.setPreferredSize(AlphaFineConstants.PREVIEW_SIZE);
    }

    public ProductNewsList getProductNewsList() {
        return productNewsList;
    }
}
