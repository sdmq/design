package com.fr.design.mainframe.alphafine.component;

import com.fr.design.gui.icontainer.UIScrollPane;
import com.fr.design.mainframe.alphafine.AlphaFineHelper;
import com.fr.design.mainframe.alphafine.cell.model.AlphaCellModel;
import com.fr.design.mainframe.alphafine.model.SearchResult;
import javax.swing.BorderFactory;
import javax.swing.DefaultListModel;

/**
 * @author hades
 * @version 11.0
 * Created by hades on 2022/4/18
 */
public class SearchListModel extends DefaultListModel<AlphaCellModel> {

    private static final int MAX_SHOW_SIZE = 12;
    private static final long serialVersionUID = 7230585307439551228L;


    private SearchResult myDelegate;

    /**
     * 第一有效的项是否被选中
     */
    private boolean isValidSelected;

    private UIScrollPane leftSearchResultPane;

    private AlphaFineList searchResultList;

    public SearchListModel(SearchResult searchResult, AlphaFineList searchResultList, UIScrollPane leftSearchResultPane) {
        this.myDelegate = searchResult;
        this.searchResultList = searchResultList;
        this.leftSearchResultPane = leftSearchResultPane;
    }

    @Override
    public void addElement(AlphaCellModel element) {
        AlphaFineHelper.checkCancel();
        int index = myDelegate.size();
        myDelegate.add(element);
        fireContentsChanged(this, index, index);
        fireSelectedStateChanged(element, index);

    }

    @Override
    protected void fireContentsChanged(Object source, int index0, int index1) {
        if (myDelegate.size() > MAX_SHOW_SIZE) {
            leftSearchResultPane.getVerticalScrollBar().setBorder(BorderFactory.createEmptyBorder(0, 2, 0, 0));
            leftSearchResultPane.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 2));
        } else {
            leftSearchResultPane.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
        }
        super.fireContentsChanged(source, index0, index1);
    }

    /**
     * 触发选中第一有效的项
     *
     * @param element
     * @param index
     */
    private void fireSelectedStateChanged(AlphaCellModel element, int index) {
        if (element.hasAction() && !isValidSelected()) {
            searchResultList.setSelectedIndex(index);
            setValidSelected(true);
        }
    }

    @Override
    public AlphaCellModel getElementAt(int index) {
        return myDelegate.get(index);
    }

    @Override
    public void add(int index, AlphaCellModel element) {
        myDelegate.add(index, element);
        fireIntervalAdded(this, index, index);
    }

    @Override
    public AlphaCellModel remove(int index) {
        AlphaCellModel object = myDelegate.get(index);
        myDelegate.remove(object);
        fireContentsChanged(this, index, index);
        return object;
    }

    @Override
    public int getSize() {
        return this.myDelegate.size();
    }

    @Override
    public void removeAllElements() {
        this.myDelegate.clear();
    }

    /**
     * 重置选中状态
     */
    public void resetSelectedState() {
        setValidSelected(false);
    }

    private boolean isValidSelected() {
        return isValidSelected;
    }

    private void setValidSelected(boolean selected) {
        isValidSelected = selected;
    }

    @Override
    public boolean isEmpty() {
        return myDelegate.isEmpty();
    }

    public void resetState() {
        for (int i = 0; i < getSize(); i++) {
            getElementAt(i).resetState();
        }
    }
}