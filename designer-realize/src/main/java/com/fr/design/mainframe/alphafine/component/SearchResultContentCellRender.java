package com.fr.design.mainframe.alphafine.component;

import com.fr.design.gui.ilable.UILabel;
import com.fr.design.mainframe.alphafine.AlphaFineConstants;
import com.fr.design.mainframe.alphafine.AlphaFineUtil;
import com.fr.design.mainframe.alphafine.cell.model.AlphaCellModel;
import com.fr.general.IOUtils;
import com.fr.stable.StringUtils;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import javax.swing.BorderFactory;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.ListCellRenderer;

/**
 * @author hades
 * @version 11.0
 * Created by hades on 2022/4/19
 */
public class SearchResultContentCellRender implements ListCellRenderer<Object> {

    private static final int OFFSET = 45;
    private static final String SELECTED_PATH = AlphaFineConstants.IMAGE_URL + "selected";
    private static final String CELL_PATH = AlphaFineConstants.IMAGE_URL + "alphafine";
    private static final String SUFFIX = ".png";

    private String[] segmentationResult;

    public SearchResultContentCellRender(String[] segmentationResult) {
        this.segmentationResult = segmentationResult;
    }

    @Override
    public Component getListCellRendererComponent(JList<?> list, Object value, int index, boolean isSelected,
                                                  boolean cellHasFocus) {


        AlphaCellModel model = (AlphaCellModel) value;
        JPanel panel = new JPanel(new BorderLayout());
        panel.setBackground(null);
        panel.setBorder(BorderFactory.createEmptyBorder(0, 10, 0, 0));
        // 图标icon 样式
        UILabel iconLabel = new UILabel();
        if (isSelected) {
            iconLabel.setText(StringUtils.BLANK + model.getName());
            String iconUrl = SELECTED_PATH + model.getType().getTypeValue() + SUFFIX;
            panel.setBackground(AlphaFineConstants.BLUE);
            iconLabel.setForeground(Color.WHITE);
            iconLabel.setIcon(IOUtils.readIcon(iconUrl));
        } else {
            iconLabel.setText(AlphaFineUtil.highLightModelName(model.getName(), segmentationResult));
            String iconUrl = CELL_PATH + model.getType().getTypeValue() + SUFFIX;
            iconLabel.setIcon(IOUtils.readIcon(iconUrl));
        }
        iconLabel.setFont(AlphaFineConstants.MEDIUM_FONT);


        // 内容详情label 样式
        UILabel detailLabel = new UILabel();
        String description = model.getDescription();
        if (StringUtils.isNotBlank(description)) {
            detailLabel.setText("-" + description);
            detailLabel.setForeground(AlphaFineConstants.LIGHT_GRAY);
            panel.add(detailLabel, BorderLayout.CENTER);
            int width = (int) (iconLabel.getPreferredSize().getWidth() + detailLabel.getPreferredSize().getWidth());
            if (width > AlphaFineConstants.LEFT_WIDTH - OFFSET) {
                int nameWidth = (int) (AlphaFineConstants.LEFT_WIDTH - detailLabel.getPreferredSize().getWidth() - OFFSET);
                iconLabel.setPreferredSize(new Dimension(nameWidth, AlphaFineConstants.CELL_HEIGHT));
            }
        } else {
            iconLabel.setPreferredSize(new Dimension(AlphaFineConstants.LEFT_WIDTH - OFFSET, AlphaFineConstants.CELL_HEIGHT));
        }

        panel.add(iconLabel, BorderLayout.WEST);
        panel.setPreferredSize(new Dimension(list.getFixedCellWidth(), AlphaFineConstants.CELL_HEIGHT));
        return panel;
    }


}
