package com.fr.design.mainframe.alphafine.component;

import com.fr.design.gui.icontainer.UIScrollPane;
import com.fr.design.mainframe.alphafine.AlphaFineConstants;
import com.fr.design.mainframe.alphafine.model.SearchResult;
import com.fr.design.mainframe.alphafine.preview.ResultShowPane;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import javax.swing.JPanel;

/**
 * @author hades
 * @version 11.0
 * Created by hades on 2022/4/22
 */
public class SearchResultPane extends JPanel {

    private AlphaFineList searchResultList;

    private SearchListModel searchListModel;

    private UIScrollPane leftSearchResultPane;


    public SearchResultPane(String[] segmentationResult, ResultShowPane rightSearchResultPane) {
        searchResultList = new AlphaFineList();
        searchResultList.setFixedCellHeight(AlphaFineConstants.CELL_HEIGHT);
        leftSearchResultPane = new UIScrollPane(searchResultList);
        leftSearchResultPane.setBorder(null);
        leftSearchResultPane.setBackground(Color.WHITE);
        leftSearchResultPane.setPreferredSize(new Dimension(AlphaFineConstants.LEFT_WIDTH, AlphaFineConstants.CONTENT_HEIGHT));
        searchListModel = new SearchListModel(new SearchResult(), searchResultList, leftSearchResultPane);
        searchResultList.setModel(searchListModel);
        searchResultList.setCellRenderer(new SearchResultContentCellRender(segmentationResult));
        searchResultList.setResultShowPane(rightSearchResultPane);
        this.setPreferredSize(AlphaFineConstants.CONTENT_SIZE);
        this.setLayout(new BorderLayout());
        this.add(leftSearchResultPane, BorderLayout.WEST);
        this.add(rightSearchResultPane, BorderLayout.EAST);
        this.setPreferredSize(AlphaFineConstants.PREVIEW_SIZE);
    }

    public AlphaFineList getSearchResultList() {
        return searchResultList;
    }

    public UIScrollPane getLeftSearchResultPane() {
        return leftSearchResultPane;
    }
}
