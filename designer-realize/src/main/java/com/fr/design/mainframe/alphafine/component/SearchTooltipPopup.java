package com.fr.design.mainframe.alphafine.component;

import java.awt.Component;
import java.awt.Point;
import javax.swing.Popup;
import javax.swing.PopupFactory;

/**
 * @author hades
 * @version 11.0
 * Created by hades on 2022/4/22
 */
public class SearchTooltipPopup {

    private static final SearchTooltipPopup INSTANCE = new SearchTooltipPopup();

    public static SearchTooltipPopup getInstance() {
        return INSTANCE;
    }

    private AlphaSearchTooltipPane alphaSearchTooltipPane;

    private SearchTooltipPopup() {
        alphaSearchTooltipPane = new AlphaSearchTooltipPane();
    }

    private boolean showPopup;

    private Popup popup;

    public void show(Component owner) {
        if (popup == null || !showPopup) {
            PopupFactory pf = PopupFactory.getSharedInstance();
            Point point = owner.getLocationOnScreen();
            alphaSearchTooltipPane.refreshHistory();
            popup = pf.getPopup(owner, alphaSearchTooltipPane, point.x, point.y + owner.getHeight());
        }
        if (!showPopup) {
            alphaSearchTooltipPane.repaint();
            popup.show();
            getAlphaFineToolTipList().clearSelection();
            showPopup = true;
        }
    }

    public AlphaFineToolTipList getAlphaFineToolTipList() {
        return alphaSearchTooltipPane.getAlphaFineToolTipList();
    }

    public void hide() {
        if (popup != null) {
            popup.hide();
        }
        showPopup = false;
    }


}
