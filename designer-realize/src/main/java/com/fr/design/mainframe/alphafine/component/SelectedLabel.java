package com.fr.design.mainframe.alphafine.component;

import com.fr.design.constants.UIConstants;
import com.fr.design.gui.ilable.UILabel;
import com.fr.design.mainframe.alphafine.AlphaFineConstants;
import com.fr.design.mainframe.alphafine.AlphaFineUtil;
import com.fr.design.mainframe.alphafine.CellType;

import javax.swing.BorderFactory;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;

/**
 * @author hades
 * @version 11.0
 * Created by hades on 2022/4/07
 */
public class SelectedLabel extends UILabel {

    private static final int WIDTH = 4;
    private static final int HEIGHT = 4;
    private static final int GAP = 0;
    private static final int BORDER_RIGHT = 5;
    private static final int BORDER_TOP = 2;

    private boolean selected;
    private CellType cellType;

    public SelectedLabel(String text, CellType cellType, boolean selected) {
        super(text);
        this.setForeground(AlphaFineConstants.FOREGROUND_COLOR_8);
        this.setBorder(BorderFactory.createEmptyBorder(BORDER_TOP, 0, 0, BORDER_RIGHT));
        this.selected = selected;
        this.cellType = cellType;
    }

    public SelectedLabel(String text, CellType cellType) {
        this(text, cellType, false);
    }

    @Override
    public void paintComponent(Graphics g) {
        Graphics2D g2d = (Graphics2D) g;
        g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
                           RenderingHints.VALUE_ANTIALIAS_ON);
        if (selected) {
            g2d.setColor(UIConstants.FLESH_BLUE);
            setForeground(UIConstants.FLESH_BLUE);
            g2d.drawLine(0, this.getHeight() - 1, this.getWidth() - BORDER_RIGHT, this.getHeight() - 1);
        }
        super.paintComponent(g);
    }


    @Override
    protected void paintBorder(Graphics g) {
        super.paintBorder(g);
        Graphics2D g2d = (Graphics2D) g;
        g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        if (cellType == CellType.PRODUCT_NEWS && AlphaFineUtil.unread()) {
            Color oldColor = g.getColor();
            g2d.setColor(Color.RED);
            g2d.fillOval(getWidth() - WIDTH, GAP, WIDTH, HEIGHT);
            g2d.setColor(oldColor);
        }
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    public CellType getCellType() {
        return cellType;
    }
}
