package com.fr.design.mainframe.alphafine.model;

import com.fr.design.i18n.Toolkit;
import java.awt.Image;
import java.util.Date;

/**
 * 产品动态
 *
 * @author hades
 * @version 11.0
 * Created by hades on 2022/4/05
 */
public class ProductNews {

    private long id;
    private String title;

    private Tag tag;
    private Target target;

    private Status status;
    private String url;
    private Image image;


    private Date pushDate;

    /**
     * 创建cid的用户
     */
    private int creator;

    public long getId() {
        return id;
    }

    public ProductNews setId(long id) {
        this.id = id;
        return this;
    }

    public String getTitle() {
        return title;
    }

    public ProductNews setTitle(String title) {
        this.title = title;
        return this;
    }

    public Tag getTag() {
        return tag;
    }

    public ProductNews setTag(Tag tag) {
        this.tag = tag;
        return this;
    }

    public Target getTarget() {
        return target;
    }

    public ProductNews setTarget(Target target) {
        this.target = target;
        return this;
    }

    public Status getStatus() {
        return status;
    }

    public ProductNews setStatus(Status status) {
        this.status = status;
        return this;
    }

    public String getUrl() {
        return url;
    }

    public ProductNews setUrl(String url) {
        this.url = url;
        return this;
    }

    public Image getImage() {
        return image;
    }

    public ProductNews setImage(Image image) {
        this.image = image;
        return this;
    }

    public Date getPushDate() {
        return pushDate;
    }

    public ProductNews setPushDate(Date pushDate) {
        this.pushDate = pushDate;
        return this;
    }

    public int getCreator() {
        return creator;
    }

    public ProductNews setCreator(int creator) {
        this.creator = creator;
        return this;
    }

    interface CodeParser {
        int getCode();
    }

    public enum Status implements CodeParser {
        STOP(0), START(1);

        private final int code;

        Status(int code) {
            this.code = code;
        }

        @Override
        public int getCode() {
            return code;
        }

        public static Status parseCode(int code) {
            for (Status status : values()) {
                if (code == status.code) {
                    return status;
                }
            }
            throw new IllegalArgumentException();
        }
    }

    public enum Tag {
        SOLUTION(1, Toolkit.i18nText("Fine-Design_Report_AlphaFine_Solution")),
        MATERIAL(2, Toolkit.i18nText("Fine-Design_Report_AlphaFine_Material")),
        NEW_PRODUCT(3, Toolkit.i18nText("Fine-Design_Report_AlphaFine_New_Product"));

        private final int code;

        private final String desc;

        Tag(int code, String desc) {
            this.code = code;
            this.desc = desc;
        }

        public static Tag parseCode(int code) {
            for (Tag tag :values()) {
                if (tag.code == code) {
                    return tag;
                }
            }
            throw new IllegalArgumentException();
        }

        public int getCode() {
            return code;
        }

        public String getDesc() {
            return desc;
        }
    }

    public enum Target {
        ALL_USER(0);

        private final int code;

        Target(int code) {
            this.code = code;
        }

        public int getCode() {
            return code;
        }

        public static Target parseCode(int code) {
            for (Target target : values()) {
                if (target.code == code) {
                    return target;
                }
             }
            throw new IllegalArgumentException();
        }

    }


}
