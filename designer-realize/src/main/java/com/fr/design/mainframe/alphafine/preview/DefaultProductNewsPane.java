package com.fr.design.mainframe.alphafine.preview;

import com.fr.design.gui.icontainer.UIScrollPane;
import com.fr.design.gui.ilable.UILabel;
import com.fr.design.i18n.Toolkit;
import com.fr.design.mainframe.alphafine.AlphaFineConstants;
import com.fr.design.mainframe.alphafine.AlphaFineHelper;
import com.fr.design.mainframe.alphafine.component.ProductNewsContentCellRender;
import com.fr.design.mainframe.alphafine.component.ProductNewsList;
import com.fr.design.mainframe.alphafine.exception.AlphaFineNetworkException;
import com.fr.design.mainframe.alphafine.model.ProductNews;
import com.fr.design.mainframe.alphafine.search.manager.impl.ProductNewsSearchManager;
import com.fr.design.utils.DesignUtils;
import com.fr.log.FineLoggerFactory;
import java.awt.BorderLayout;
import java.awt.Color;
import java.util.List;
import javax.swing.BorderFactory;
import javax.swing.DefaultListModel;
import javax.swing.ImageIcon;
import javax.swing.JPanel;
import javax.swing.SwingWorker;

/**
 * @author hades
 * @version 11.0
 * Created by hades on 2022/4/14
 */
public class DefaultProductNewsPane extends JPanel {


    private static final String LOADING = Toolkit.i18nText("Fine-Design_Report_AlphaFine_Loading");
    private static final ImageIcon LOADING_ICON = new ImageIcon(DefaultProductNewsPane.class.getResource("/com/fr/web/images/loading-local.gif"));

    private SwingWorker<List<ProductNews>, Void> worker;

    public DefaultProductNewsPane() {

        setLayout(new BorderLayout());
        this.add(createLoadingPane());
        this.setPreferredSize(AlphaFineConstants.PREVIEW_SIZE);
        this.worker = createWorker();
        this.worker.execute();
    }


    private JPanel createLoadingPane() {
        JPanel loadingPane = new JPanel(new BorderLayout());
        UILabel loadingLabel = new UILabel(LOADING);
        loadingLabel.setForeground(AlphaFineConstants.MEDIUM_GRAY);
        loadingLabel.setFont(DesignUtils.getDefaultGUIFont().applySize(14));
        loadingLabel.setBorder(BorderFactory.createEmptyBorder(0, 280, 0, 0));
        UILabel loadingIconLabel = new UILabel(LOADING_ICON);
        loadingIconLabel.setBorder(BorderFactory.createEmptyBorder(100, 0, 0, 0));
        loadingPane.add(loadingIconLabel, BorderLayout.NORTH);
        loadingPane.add(loadingLabel, BorderLayout.CENTER);
        loadingPane.setBackground(Color.WHITE);
        return loadingPane;
    }

    private SwingWorker<List<ProductNews>, Void> createWorker() {
        if (this.worker != null && !this.worker.isDone()) {
            this.worker.cancel(true);
            this.worker = null;
        }
        return new SwingWorker<List<ProductNews>, Void>() {

            @Override
            protected List<ProductNews> doInBackground() throws Exception {
                if (!AlphaFineHelper.isNetworkOk()) {
                    throw new AlphaFineNetworkException();
                }
                return ProductNewsSearchManager.getInstance().getProductNewsList();
            }

            @Override
            protected void done() {
                DefaultProductNewsPane.this.removeAll();
                try {
                    DefaultProductNewsPane.this.add(createContentPane(get()));
                } catch (Exception e) {
                    FineLoggerFactory.getLogger().error(e.getMessage(), e);
                    if (e.getCause() instanceof AlphaFineNetworkException) {
                        DefaultProductNewsPane.this.add(new NetWorkFailedPane(() -> {
                            DefaultProductNewsPane.this.removeAll();
                            add(createLoadingPane());
                            refresh();
                            worker = createWorker();
                            worker.execute();
                        }));
                    }
                }
                refresh();
            }
        };

    }

    private void refresh() {
        this.validate();
        this.repaint();
        if (AlphaFineHelper.getAlphaFineDialog() != null) {
            AlphaFineHelper.getAlphaFineDialog().repaint();
        }
    }

    private UIScrollPane createContentPane(List<ProductNews> productNewsList) {
        DefaultListModel<ProductNews> productNewsDefaultListModel = new DefaultListModel<>();
        for (ProductNews productNews : productNewsList) {
            productNewsDefaultListModel.addElement(productNews);
        }
        ProductNewsList productNewsJList = new ProductNewsList(productNewsDefaultListModel);
        productNewsJList.setBackground(Color.WHITE);
        productNewsJList.setCellRenderer(new ProductNewsContentCellRender(productNewsJList));
        UIScrollPane scrollPane = new UIScrollPane(productNewsJList);
        scrollPane.setBackground(Color.WHITE);
        scrollPane.setBorder(BorderFactory.createEmptyBorder(10, 20, 0, 20));
        return scrollPane;
    }

}
