package com.fr.design.mainframe.alphafine.preview;

import com.fr.design.constants.UIConstants;
import com.fr.design.gui.ilable.UILabel;
import com.fr.design.i18n.Toolkit;
import com.fr.design.mainframe.alphafine.AlphaFineConstants;
import com.fr.design.mainframe.alphafine.AlphaFineHelper;
import com.fr.design.utils.BrowseUtils;
import com.fr.design.utils.DesignUtils;
import com.fr.json.JSON;
import com.fr.json.JSONArray;
import com.fr.json.JSONFactory;
import com.fr.json.JSONObject;
import com.fr.log.FineLoggerFactory;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.LinkedHashMap;
import java.util.Map;
import javax.swing.BorderFactory;
import javax.swing.Icon;
import javax.swing.JPanel;
import javax.swing.JSeparator;
import javax.swing.SwingConstants;
import javax.swing.SwingWorker;

/**
 * @author hades
 * @version 11.0
 * Created by hades on 2022/4/13
 */
public class HelpDocumentNoResultPane extends JPanel {

    private static final String RECOMMEND = Toolkit.i18nText("Fine-Design_Report_AlphaFine_Recommend_For_You");

    private static final Color DOT_COLOR = new Color(200, 201, 205);

    private SwingWorker<Boolean, Void> worker;

    private String title;

    private Icon icon;

    private Map<String, String> linkMap;

    public HelpDocumentNoResultPane(String title, Icon icon) {
        this.title = title;
        this.icon = icon;
        this.linkMap = generateMap();
        setLayout(new BorderLayout());
        worker = createWorker();
        worker.execute();
    }

    private SwingWorker<Boolean, Void> createWorker() {
        if (this.worker != null && !this.worker.isDone()) {
            this.worker.cancel(true);
            this.worker = null;
        }
        return new SwingWorker<Boolean, Void>() {

            @Override
            protected Boolean doInBackground() throws Exception {
                return AlphaFineHelper.isNetworkOk();
            }

            @Override
            protected void done() {
                HelpDocumentNoResultPane.this.removeAll();
                try {
                    if (get()) {
                        add(new NoResultPane(title, icon, 150), BorderLayout.CENTER);
                        add(createRecommendPane(linkMap), BorderLayout.EAST);
                    } else {
                        add(new NetWorkFailedPane(() -> {
                            worker = createWorker();
                            worker.execute();
                        }));
                    }
                } catch (Exception e) {
                    FineLoggerFactory.getLogger().error(e.getMessage(), e);
                }
                refresh();
            }
        };
    }


    private void refresh() {
        this.validate();
        this.repaint();
    }


    private Map<String, String> generateMap() {
        JSONArray jsonArray = JSONFactory.createJSON(JSON.ARRAY, AlphaFineConstants.ALPHA_HELP_RECOMMEND);
        Map<String, String> linkMap = new LinkedHashMap<>();
        for (int i = 0, len = jsonArray.size(); i < len; i++) {
            JSONObject json = jsonArray.getJSONObject(i);
            linkMap.put(json.getString("name"), json.getString("link"));
        }
        return linkMap;
    }

    private JPanel createRecommendPane(Map<String, String> linkMap) {
        JPanel wrapRecommendPane = new JPanel(new BorderLayout());
        wrapRecommendPane.setPreferredSize(new Dimension(200, 305));
        JPanel recommendPane = new JPanel();
        recommendPane.setLayout(new GridLayout(0, 1));
        recommendPane.setBorder(BorderFactory.createEmptyBorder(0, 5, 130, 0));
        recommendPane.setBackground(Color.WHITE);
        recommendPane.add(new UILabel(RECOMMEND));
        for (Map.Entry<String, String> entry : linkMap.entrySet()) {
            recommendPane.add(createListLabel(entry.getKey(), entry.getValue()));
        }
        // 分割线
        JSeparator sep = new JSeparator();
        sep.setOrientation(JSeparator.VERTICAL);
        sep.setLayout(new GridLayout(0, 1));
        sep.setPreferredSize(new Dimension(1, 285));
        sep.setBorder(BorderFactory.createEmptyBorder(10, 0, 10, 0));
        sep.add(new UILabel());
        wrapRecommendPane.add(sep, BorderLayout.WEST);
        wrapRecommendPane.add(recommendPane, BorderLayout.CENTER);
        return wrapRecommendPane;
    }

    private JPanel createListLabel(String text, String link) {
        UILabel listLabel = new UILabel(String.format("%s", text));
        listLabel.setForeground(UIConstants.FLESH_BLUE);
        listLabel.setBorder(BorderFactory.createEmptyBorder(10, 0, 0, 0));
        listLabel.addMouseListener(new MouseAdapter() {
            @Override
            public void mousePressed(MouseEvent e) {
                responseClick(link);
            }
        });
        listLabel.setCursor(new Cursor(Cursor.HAND_CURSOR));

        JPanel listPane = new JPanel(new BorderLayout());
        listPane.setBackground(Color.WHITE);
        UILabel dotLabel = new UILabel("·");
        dotLabel.setBorder(BorderFactory.createEmptyBorder(10, 0, 0, 0));
        dotLabel.setFont(DesignUtils.getDefaultGUIFont().applySize(14));
        dotLabel.setHorizontalAlignment(SwingConstants.LEADING);
        dotLabel.setForeground(DOT_COLOR);
        listPane.add(dotLabel, BorderLayout.WEST);
        listPane.add(listLabel, BorderLayout.CENTER);
        listPane.setPreferredSize(new Dimension(100, 20));
        return listPane;
    }

    /**
     * 方便记录埋点
     *
     * @param link
     */
    private void responseClick(String link) {
        BrowseUtils.browser(link);
    }

}
