package com.fr.design.mainframe.alphafine.preview;

import com.fr.design.gui.ilable.UILabel;
import com.fr.design.mainframe.alphafine.AlphaFineConstants;
import com.fr.design.mainframe.alphafine.CellType;
import com.fr.design.mainframe.alphafine.cell.model.AlphaCellModel;
import com.fr.design.mainframe.alphafine.cell.model.FileModel;
import com.fr.design.mainframe.alphafine.cell.model.PluginModel;
import com.fr.log.FineLoggerFactory;
import com.fr.stable.project.ProjectConstants;
import com.fr.workspace.WorkContext;
import com.fr.workspace.server.exporter.LocalExportOperator;
import com.fr.workspace.server.exporter.TemplateExportOperator;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.net.URL;
import java.util.concurrent.ExecutionException;
import javax.imageio.ImageIO;
import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.SwingWorker;

/**
 * @author hades
 * @version 11.0
 * Created by hades on 2022/4/20
 */
public class LoadingRightSearchResultPane extends ResultShowPane {

    private SwingWorker<BufferedImage, Void> showWorker;

    public LoadingRightSearchResultPane() {

        this.setBackground(Color.WHITE);
        this.setPreferredSize(new Dimension(AlphaFineConstants.RIGHT_WIDTH - 1, AlphaFineConstants.CONTENT_HEIGHT));
        initLoadingLabel();

    }

    private void initLoadingLabel() {
        UILabel label = new UILabel(new ImageIcon(getClass().getResource("/com/fr/design/mainframe/alphafine/images/opening.gif")));
        label.setBorder(BorderFactory.createEmptyBorder(120, 0, 0, 0));
        this.add(label, BorderLayout.CENTER);
    }

    private void showDefaultPreviewPane() {
        this.removeAll();
        initLoadingLabel();
        validate();
        repaint();
        revalidate();
    }

    @Override
    public void showResult(AlphaCellModel selectedValue) {
        showDefaultPreviewPane();
        checkWorker();
        if (selectedValue.getType() == CellType.FILE) {
            fileShowWorker(selectedValue);
        }

        if (selectedValue.getType() == CellType.PLUGIN) {
            pluginShowWorker(selectedValue);
        }
        this.showWorker.execute();
    }

    private void fileShowWorker(AlphaCellModel selectedValue) {
        this.showWorker = new SwingWorker<BufferedImage, Void>() {
            @Override
            protected BufferedImage doInBackground() throws Exception {
                final String fileName = ((FileModel) selectedValue).getFilePath().substring(ProjectConstants.REPORTLETS_NAME.length() + 1);
                if (fileName.endsWith(ProjectConstants.FRM_SUFFIX))  {
                    return frmToImage(fileName);
                } else if (fileName.endsWith(ProjectConstants.CPT_SUFFIX)) {
                    return cptToImage(fileName);
                } else {
                    return null;
                }
            }

            @Override
            protected void done() {
                if (!isCancelled()) {
                    LoadingRightSearchResultPane.this.removeAll();
                    try {
                        LoadingRightSearchResultPane.this.add(new FilePreviewPane(get()));
                    } catch (InterruptedException | ExecutionException e) {
                        FineLoggerFactory.getLogger().error(e.getMessage(), e);
                    }
                    validate();
                    repaint();
                }
            }
        };
    }

    private void pluginShowWorker(AlphaCellModel selectedValue) {
        this.showWorker = new SwingWorker<BufferedImage, Void>() {
            @Override
            protected BufferedImage doInBackground() {
                BufferedImage bufferedImage = null;
                try {
                    bufferedImage = ImageIO.read(new URL(((PluginModel) selectedValue).getImageUrl()));
                } catch (IOException e) {
                    try {
                        bufferedImage = ImageIO.read(getClass().getResource("/com/fr/design/mainframe/alphafine/images/default_product.png"));
                    } catch (IOException e1) {
                        FineLoggerFactory.getLogger().error(e.getMessage(), e);
                    }
                }
                return bufferedImage;
            }

            @Override
            protected void done() {
                try {
                    if (!isCancelled()) {
                        LoadingRightSearchResultPane.this.removeAll();
                        LoadingRightSearchResultPane.this.add(new PluginPreviewPane((selectedValue).getName(), get(), ((PluginModel) selectedValue).getVersion(), ((PluginModel) selectedValue).getJartime(), ((PluginModel) selectedValue).getType(), ((PluginModel) selectedValue).getPrice()));
                        validate();
                        repaint();
                    }
                } catch (Exception e) {
                    FineLoggerFactory.getLogger().error(e.getMessage(), e);
                }
            }
        };
    }


    private BufferedImage frmToImage(String fileName) throws Exception {
        byte[] bytes = null;
        try {
            bytes = WorkContext.getCurrent().get(TemplateExportOperator.class).exportFormAsImageData(fileName);
        } catch (Exception ignored) {
            // 兼容下老版本
            bytes = new LocalExportOperator().exportFormAsImageData(fileName);
        }
        return TemplateExportOperator.byteDataToImage(bytes);
    }


    private BufferedImage cptToImage(String fileName) throws Exception {
        byte[] bytes = null;
        try {
            bytes = WorkContext.getCurrent().get(TemplateExportOperator.class).exportWorkBookAsImageData(fileName);
        } catch (Exception ignored) {
            // 兼容下老版本
            bytes = new LocalExportOperator().exportWorkBookAsImageData(fileName);
        }
        return TemplateExportOperator.byteDataToImage(bytes);
    }


    private void checkWorker() {
        if (this.showWorker != null && !this.showWorker.isDone()) {
            this.showWorker.cancel(true);
            this.showWorker = null;
        }
    }


}
