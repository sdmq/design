package com.fr.design.mainframe.alphafine.preview;

import com.fr.design.gui.ilable.UILabel;
import com.fr.design.i18n.Toolkit;
import com.fr.design.layout.FRGUIPaneFactory;
import com.fr.design.mainframe.alphafine.AlphaFineConstants;
import com.fr.design.mainframe.share.ui.base.MouseClickListener;
import com.fr.general.IOUtils;

import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.FlowLayout;
import java.awt.event.MouseEvent;

/**
 * @author hades
 * @version 11.0
 * Created by hades on 2022/4/28
 */
public class NetWorkFailedPane extends JPanel {

    private Runnable reload;

    public NetWorkFailedPane() {
        this(() -> {});
    }

    public NetWorkFailedPane(Runnable reload) {
        this.reload = reload;
        this.setLayout(new BorderLayout());
        this.add(createInternetErrorPane());
        this.setPreferredSize(AlphaFineConstants.PREVIEW_SIZE);
        this.setBackground(Color.WHITE);
    }

    private JPanel createInternetErrorPane() {
        JPanel panel = FRGUIPaneFactory.createVerticalFlowLayout_Pane(true, FlowLayout.LEADING, 0, 5);
        panel.setBackground(Color.WHITE);
        UILabel imagePanel = new UILabel(IOUtils.readIcon("/com/fr/base/images/share/internet_error.png"));
        imagePanel.setBorder(BorderFactory.createEmptyBorder(50, 280, 0, 0));
        panel.add(imagePanel);
        UILabel uiLabel = tipLabel(Toolkit.i18nText("Fine-Design_Share_Internet_Connect_Failed"));
        uiLabel.setBorder(BorderFactory.createEmptyBorder(0, 300, 0, 0));
        uiLabel.setForeground(Color.decode("#8F8F92"));
        UILabel reloadLabel = tipLabel(Toolkit.i18nText("Fine-Design_Share_Online_Reload"));
        reloadLabel.setBorder(BorderFactory.createEmptyBorder(0, 310, 0, 0));
        reloadLabel.setForeground(Color.decode("#419BF9"));
        reloadLabel.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        reloadLabel.addMouseListener(new MouseClickListener() {
            @Override
            public void mousePressed(MouseEvent e) {
                reload.run();
            }
        });
        panel.add(uiLabel);
        panel.add(reloadLabel);
        return panel;
    }

    private UILabel tipLabel(String text) {
        UILabel tipLabel = new UILabel(text);
        tipLabel.setHorizontalAlignment(SwingConstants.CENTER);
        return tipLabel;
    }
}
