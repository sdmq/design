package com.fr.design.mainframe.alphafine.preview;

import com.fr.design.gui.ilable.UILabel;
import com.fr.design.mainframe.alphafine.AlphaFineConstants;
import com.fr.design.utils.DesignUtils;

import java.awt.Component;
import javax.swing.BorderFactory;
import javax.swing.Icon;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;

/**
 * Created by XiaXiang on 2017/8/14.
 */
public class NoResultPane extends JPanel {
    public NoResultPane(String title, Icon icon) {
        this(title, icon, AlphaFineConstants.LEFT_WIDTH);
    }

    public NoResultPane(String title, Icon icon, int width) {
        setLayout(new BorderLayout());
        setBackground(Color.white);
        setPreferredSize(new Dimension(width, 305));
        UILabel image = new UILabel();
        image.setPreferredSize(new Dimension(150, 111));
        image.setHorizontalAlignment(SwingConstants.CENTER);
        image.setIcon(icon);
        image.setBorder(BorderFactory.createEmptyBorder(30, 0, 0, 0));
        add(image, BorderLayout.CENTER);
        add(generateDescription(title), BorderLayout.SOUTH);
    }

    protected Component generateDescription(String title) {
        UILabel description = new UILabel(title);
        description.setForeground(AlphaFineConstants.MEDIUM_GRAY);
        description.setFont(DesignUtils.getDefaultGUIFont().applySize(14));
        description.setBorder(BorderFactory.createEmptyBorder(0, 0, 135, 0));
        description.setHorizontalAlignment(SwingConstants.CENTER);
        return description;
    }
}
