package com.fr.design.mainframe.alphafine.preview;

import com.fr.design.dialog.link.MessageWithLink;
import com.fr.design.mainframe.alphafine.AlphaFineConstants;
import com.fr.design.utils.BrowseUtils;
import com.fr.design.utils.DesignUtils;
import java.awt.Color;
import java.awt.Component;
import javax.swing.BorderFactory;
import javax.swing.Icon;
import javax.swing.event.HyperlinkEvent;
import javax.swing.event.HyperlinkListener;

/**
 * 带跳转链接的无结果面板
 *
 * @author hades
 * @version 11.0
 * Created by hades on 2022/4/12
 */
public class NoResultWithLinkPane extends NoResultPane {

    private static final String TAG_A_START = "<a>";
    private static final String TAG_A_END = "</a>";

    public NoResultWithLinkPane(String title, Icon icon) {
        super(title, icon);
    }

    @Override
    protected Component generateDescription(String title) {
        String[] para1 = title.split(TAG_A_START);
        String[] para2 = para1[1].split(TAG_A_END);

        MessageWithLink messageWithLink = new MessageWithLink(para1[0], para2[0], AlphaFineConstants.ALPHA_GO_TO_FORUM, para2[1], Color.WHITE, DesignUtils.getDefaultGUIFont().applySize(14), AlphaFineConstants.MEDIUM_GRAY) {
            @Override
            protected void initListener(String link) {

                addHyperlinkListener(new HyperlinkListener() {
                    @Override
                    public void hyperlinkUpdate(HyperlinkEvent e) {
                        if (e.getEventType().equals(HyperlinkEvent.EventType.ACTIVATED)) {
                            jumpToForum(link);
                        }
                    }
                });
            }
        };
        messageWithLink.setBorder(BorderFactory.createEmptyBorder(0, AlphaFineConstants.LEFT_WIDTH - 30, 135, 0));
        return messageWithLink;
    }


    /**
     * 方便记录埋点
     *
     * @param link
     */
    private void jumpToForum(String link) {
        BrowseUtils.browser(link);
    }

}
