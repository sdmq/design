package com.fr.design.mainframe.alphafine.preview;

import com.fr.design.gui.ilable.UILabel;
import com.fr.design.mainframe.alphafine.AlphaFineConstants;

import java.awt.BorderLayout;
import java.awt.Color;
import javax.swing.ImageIcon;
import javax.swing.JPanel;

/**
 * @author hades
 * @version 11.0
 * Created by hades on 2022/4/21
 */
public class SearchLoadingPane extends JPanel {

    private static final ImageIcon LOADING_ICON = new ImageIcon(SearchLoadingPane.class.getResource("/com/fr/design/mainframe/alphafine/images/opening.gif"));

    public SearchLoadingPane() {
        setLayout(new BorderLayout());
        this.add(new UILabel(LOADING_ICON));
        this.setPreferredSize(AlphaFineConstants.PREVIEW_SIZE);
        this.setBackground(Color.WHITE);
    }

}
