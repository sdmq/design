package com.fr.design.mainframe.alphafine.preview;

import com.fr.design.mainframe.alphafine.AlphaFineConstants;
import com.fr.design.mainframe.alphafine.CellType;
import com.fr.design.mainframe.alphafine.cell.model.AlphaCellModel;
import java.awt.Color;
import java.awt.Dimension;
import javax.swing.JPanel;

/**
 * @author hades
 * @version 11.0
 * Created by hades on 2022/4/20
 */
public class SimpleRightSearchResultPane extends ResultShowPane {

    public SimpleRightSearchResultPane(JPanel contentPane) {
        this.add(contentPane);
        this.setBackground(Color.WHITE);
        this.setPreferredSize(new Dimension(AlphaFineConstants.RIGHT_WIDTH - 1, AlphaFineConstants.CONTENT_HEIGHT));
    }

    @Override
    public void showResult(AlphaCellModel selectedValue) {
        if (selectedValue.getType() == CellType.DOCUMENT) {
            this.removeAll();
            this.add(new DocumentPreviewPane((selectedValue).getName(), (selectedValue).getContent()));
            validate();
            repaint();
        }
    }
}
