package com.fr.design.mainframe.alphafine.question;

import com.fr.base.svg.SVGIcon;
import com.fr.base.svg.SVGLoader;
import com.fr.base.svg.SystemScaleUtils;
import com.fr.design.DesignerEnvManager;
import com.fr.design.mainframe.alphafine.AlphaFineUtil;
import com.fr.design.utils.SvgPaintUtils;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import javax.swing.JPanel;

/**
 * @author hades
 * @version 11.0
 * Created by hades on 2022/4/15
 */
public class QuestionPane extends JPanel {

    private static final Image NEW_MESSAGE_IMAGE = SVGLoader.load("/com/fr/design/mainframe/alphafine/images/group_new.svg");

    private static final Image QUESTION_IMAGE = SVGLoader.load("/com/fr/design/mainframe/alphafine/images/group.svg");

    private static final Image QUESTION_BACKGROUND_IMAGE = SVGLoader.load("/com/fr/design/mainframe/alphafine/images/groupbackgroud.svg");

    public QuestionPane() {
        this.setBackground(new Color(0, 0, 0, 0));
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        Graphics2D g2 = (Graphics2D) g;
        SvgPaintUtils.beforePaint(g2);
        // 宽高保持
        int width = SystemScaleUtils.isJreHiDPIEnabled() ? (int) (getWidth() * SVGIcon.SYSTEM_SCALE) : getWidth();
        int height = SystemScaleUtils.isJreHiDPIEnabled() ? (int) (getHeight() * SVGIcon.SYSTEM_SCALE) : getHeight();

        if (AlphaFineUtil.unread() && DesignerEnvManager.getEnvManager().getAlphaFineConfigManager().isProductDynamics()) {
            g2.drawImage(NEW_MESSAGE_IMAGE, 0, 0, this);
        } else {
            g2.drawImage(QUESTION_BACKGROUND_IMAGE, 0, 0, this);
        }

        int imageWidth = QUESTION_IMAGE.getWidth(this);
        int imageHeight =  QUESTION_IMAGE.getHeight(this);
        g2.drawImage(QUESTION_IMAGE, (width - imageWidth) / 2 - 2, (height - imageHeight) / 2 - 2,this);
        SvgPaintUtils.afterPaint(g2);
    }


    @Override
    public Dimension getPreferredSize() {
        return new Dimension(40, 40);
    }

}
