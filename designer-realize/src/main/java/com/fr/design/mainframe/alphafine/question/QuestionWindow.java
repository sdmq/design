package com.fr.design.mainframe.alphafine.question;

import com.fr.design.DesignerEnvManager;
import com.fr.design.i18n.Toolkit;
import com.fr.design.mainframe.DesignerContext;
import com.fr.design.mainframe.alphafine.AlphaFineHelper;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import javax.swing.JWindow;

/**
 * @author hades
 * @version 11.0
 * Created by hades on 2022/4/15
 */
public class QuestionWindow extends JWindow {

    private static final QuestionWindow INSTANCE = new QuestionWindow();
    private final QuestionPane questionPane = new QuestionPane();
    private int pressX;
    private int pressY;
    private QuestionWindow() {
        this.setBackground(new Color(0, 0, 0, 0));
        questionPane.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                AlphaFineHelper.showAlphaFineDialog(true);
            }

            @Override
            public void mousePressed(MouseEvent e) {
                pressX = e.getX();
                pressY = e.getY();
            }
        });
        questionPane.addMouseMotionListener(new MouseMotionAdapter() {
            @Override
            public void mouseDragged(MouseEvent e) {
                int left = getLocation().x;
                int top = getLocation().y;
                setLocation(left + e.getX() - pressX, top + e.getY() - pressY);
            }
        });

        DesignerContext.getDesignerFrame().addWindowListener(new WindowAdapter() {

            @Override
            public void windowActivated(WindowEvent e) {
                QuestionWindow.getInstance().setVisible(true);
            }

            @Override
            public void windowDeactivated(WindowEvent e) {
                QuestionWindow.getInstance().dispose();
                QuestionWindow.getInstance().setVisible(false);
            }
        });
        questionPane.setToolTipText(Toolkit.i18nText("Fine-Design_Report_AlphaFine_Learn_More_About"));
        this.setContentPane(questionPane);
        this.setSize(new Dimension(40, 40));
        // 这个地方可以设置alwaysOnTop  弹窗会跟随主页面失去激活状态而隐藏 不会与其他弹窗冲突
        this.setAlwaysOnTop(true);
        this.setLocation(DesignerContext.getDesignerFrame().getWidth() - 100,
                         DesignerContext.getDesignerFrame().getHeight() - 100);
    }

    @Override
    public void setVisible(boolean visible) {
        if (visible && !DesignerEnvManager.getEnvManager().getAlphaFineConfigManager().isEnabled()) {
            return;
        }
        super.setVisible(visible);
    }

    public static QuestionWindow getInstance() {
        return INSTANCE;
    }

}
