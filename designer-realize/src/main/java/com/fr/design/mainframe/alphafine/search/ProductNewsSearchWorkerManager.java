package com.fr.design.mainframe.alphafine.search;

import com.fr.design.mainframe.alphafine.AlphaFineConstants;
import com.fr.design.mainframe.alphafine.AlphaFineHelper;
import com.fr.design.mainframe.alphafine.CellType;
import com.fr.design.mainframe.alphafine.component.AlphaFineFrame;
import com.fr.design.mainframe.alphafine.component.ProductNewsSearchResultPane;
import com.fr.design.mainframe.alphafine.model.ProductNews;
import com.fr.log.FineLoggerFactory;
import java.util.List;
import java.util.function.Function;
import javax.swing.DefaultListModel;
import javax.swing.SwingWorker;

/**
 * @author hades
 * @version 11.0
 * Created by hades on 2022/4/22
 */
public class ProductNewsSearchWorkerManager implements SearchManager {

    private final CellType cellType;

    private SwingWorker<DefaultListModel<ProductNews>, Void> searchWorker;

    private Function<SearchTextBean, List<ProductNews>> searchFunction;

    private ProductNewsSearchResultPane searchResultPane;

    private AlphaFineFrame alphaFineFrame;

    private volatile boolean hasSearchResult = true;

    private volatile boolean searchOver = false;

    private volatile boolean networkError = false;

    public ProductNewsSearchWorkerManager(CellType cellType, Function<SearchTextBean, List<ProductNews>> searchFunction, AlphaFineFrame alphaFineFrame) {
        this.cellType = cellType;
        this.searchFunction = searchFunction;
        this.alphaFineFrame = alphaFineFrame;
    }



    @Override
    public void doSearch(SearchTextBean searchTextBean) {
        checkSearchWork();
        searchOver = false;
        networkError = false;
        if (searchResultPane != null) {
            alphaFineFrame.removeSearchResultPane(searchResultPane);
        }
        searchResultPane = new ProductNewsSearchResultPane(searchTextBean.getSegmentation());
        alphaFineFrame.addResult(searchResultPane, cellType.getFlagStr4Result());

        this.searchWorker = new SwingWorker<DefaultListModel<ProductNews>, Void>() {
            @Override
            protected DefaultListModel<ProductNews> doInBackground() throws Exception {
                DefaultListModel<ProductNews> productNewsDefaultListModel = new DefaultListModel<>();
                if (!AlphaFineHelper.isNetworkOk() && cellType.isNeedNetWork()) {
                    networkError = true;
                    FineLoggerFactory.getLogger().warn("alphaFine network error");
                    return productNewsDefaultListModel;
                }
                List<ProductNews> productNewsList = searchFunction.apply(searchTextBean);
                for (ProductNews productNews : productNewsList) {
                    productNewsDefaultListModel.addElement(productNews);
                }
                return productNewsDefaultListModel;
            }

            @Override
            protected void done() {
                searchOver = true;
                if (!isCancelled()) {
                    try {
                        if (networkError) {
                            alphaFineFrame.showResult(AlphaFineConstants.NETWORK_ERROR);
                            return;
                        }
                        DefaultListModel<ProductNews> productNewsDefaultListModel = get();
                        hasSearchResult = !productNewsDefaultListModel.isEmpty();
                        searchResultPane.getProductNewsList().setModel(get());
                        if (alphaFineFrame.getSelectedType() == cellType) {
                            if (!hasSearchResult) {
                                alphaFineFrame.showResult(CellType.NO_RESULT.getFlagStr4None());
                                return;
                            }
                            alphaFineFrame.showResult(cellType.getFlagStr4Result());
                            searchResultPane.getProductNewsList().setSelectedIndex(0);
                        }
                    } catch (Exception e) {
                        FineLoggerFactory.getLogger().error(e.getMessage(), e);
                    }
                }
            }
        };
        this.searchWorker.execute();
    }

    public ProductNewsSearchResultPane getSearchResultPane() {
        return searchResultPane;
    }

    @Override
    public boolean hasSearchResult() {
        return hasSearchResult;
    }

    @Override
    public boolean isSearchOver() {
        return searchOver;
    }

    private void checkSearchWork() {
        if (this.searchWorker != null && !this.searchWorker.isDone()) {
            this.searchWorker.cancel(true);
            this.searchWorker = null;
        }
    }

    @Override
    public boolean isNetWorkError() {
        return networkError;
    }
}
