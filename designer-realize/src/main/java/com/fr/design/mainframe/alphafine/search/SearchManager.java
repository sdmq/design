package com.fr.design.mainframe.alphafine.search;

/**
 * @author hades
 * @version 11.0
 * Created by hades on 2022/4/17
 */
public interface SearchManager {

    void  doSearch(SearchTextBean searchTextBean);

    boolean hasSearchResult();

    boolean isSearchOver();

    boolean isNetWorkError();
}
