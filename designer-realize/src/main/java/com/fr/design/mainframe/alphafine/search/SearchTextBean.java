package com.fr.design.mainframe.alphafine.search;

/**
 * @author hades
 * @version 11.0
 * Created by hades on 2022/4/17
 */
public class SearchTextBean {

    private String searchText;

    /**
     * 分词搜索
     */
    private String[] segmentation;

    public SearchTextBean(String searchText, String[] segmentation) {
        this.searchText = searchText;
        this.segmentation = segmentation;
    }

    public String getSearchText() {
        return searchText;
    }

    public void setSearchText(String searchText) {
        this.searchText = searchText;
    }

    public String[] getSegmentation() {
        return segmentation;
    }

    public void setSegmentation(String[] segmentation) {
        this.segmentation = segmentation;
    }
}
