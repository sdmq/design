package com.fr.design.mainframe.alphafine.search;

import com.fr.design.mainframe.alphafine.AlphaFineConstants;
import com.fr.design.mainframe.alphafine.AlphaFineHelper;
import com.fr.design.mainframe.alphafine.CellType;
import com.fr.design.mainframe.alphafine.cell.model.AlphaCellModel;
import com.fr.design.mainframe.alphafine.component.AlphaFineFrame;
import com.fr.design.mainframe.alphafine.component.AlphaFineList;
import com.fr.design.mainframe.alphafine.component.SearchListModel;
import com.fr.design.mainframe.alphafine.component.SearchResultPane;
import com.fr.design.mainframe.alphafine.model.SearchResult;
import com.fr.design.mainframe.alphafine.preview.ResultShowPane;
import com.fr.log.FineLoggerFactory;
import java.util.function.Function;
import javax.swing.SwingWorker;
import org.jetbrains.annotations.Nullable;

/**
 * @author hades
 * @version 11.0
 * Created by hades on 2022/4/16
 */
public class SearchWorkerManager implements SearchManager {

    private final CellType cellType;

    private SwingWorker<SearchListModel, Void> searchWorker;

    private Function<SearchTextBean, SearchResult> searchResultFunction;

    private SearchResultPane searchResultPane;

    private AlphaFineFrame alphaFineFrame;

    private ResultShowPane resultShowPane;

    private volatile boolean hasSearchResult = true;

    private volatile boolean searchOver = false;

    private volatile boolean networkError = false;

    public SearchWorkerManager(CellType cellType, Function<SearchTextBean, SearchResult> function, AlphaFineFrame alphaFineFrame, ResultShowPane resultShowPane) {
        this.cellType = cellType;
        this.searchResultFunction = function;
        this.alphaFineFrame = alphaFineFrame;
        this.resultShowPane = resultShowPane;
    }

    private void initSearchResult(SearchTextBean searchTextBean) {
        if (searchResultPane != null) {
            alphaFineFrame.removeSearchResultPane(searchResultPane);
        }
        searchResultPane = new SearchResultPane(searchTextBean.getSegmentation(), resultShowPane);
        alphaFineFrame.addResult(searchResultPane, cellType.getFlagStr4Result());

    }

    private void checkSearchWork() {
        if (this.searchWorker != null && !this.searchWorker.isDone()) {
            this.searchWorker.cancel(true);
            this.searchWorker = null;
        }
    }

    private void initSearchWorker(SearchTextBean searchTextBean) {
        this.searchOver = false;
        this.networkError = false;
        this.searchWorker = new SwingWorker<SearchListModel, Void>() {
            @Override
            protected SearchListModel doInBackground() throws Exception {
                SearchListModel searchListModel = new SearchListModel(new SearchResult(), searchResultPane.getSearchResultList(), searchResultPane.getLeftSearchResultPane());
                if (!AlphaFineHelper.isNetworkOk() && cellType.isNeedNetWork()) {
                    networkError = true;
                    FineLoggerFactory.getLogger().warn("alphaFine network error");
                    return searchListModel;
                }
                SearchResult searchResult = searchResultFunction.apply(searchTextBean);
                for (AlphaCellModel object : searchResult) {
                    AlphaFineHelper.checkCancel();
                    searchListModel.addElement(object);
                }
                return searchListModel;
            }

            @Override
            protected void done() {
                searchOver = true;
                if (!isCancelled()) {
                    try {
                        if (networkError) {
                            alphaFineFrame.showResult(AlphaFineConstants.NETWORK_ERROR);
                            return;
                        }
                        SearchListModel searchListModel = get();
                        hasSearchResult = !searchListModel.isEmpty();
                        searchResultPane.getSearchResultList().setModel(get());
                        if (alphaFineFrame.getSelectedType() == cellType) {
                            if (!hasSearchResult) {
                                alphaFineFrame.showResult(CellType.NO_RESULT.getFlagStr4None());
                                return;
                            }
                            alphaFineFrame.showResult(cellType.getFlagStr4Result());
                            searchResultPane.getSearchResultList().setSelectedIndex(0);
                            searchResultPane.getSearchResultList().requestFocus();
                        }
                    } catch (Exception e) {
                        FineLoggerFactory.getLogger().error(e.getMessage(), e);
                    }
                }
            }
        };
    }

    @Override
    public void doSearch(SearchTextBean searchTextBean) {
        initSearchResult(searchTextBean);
        checkSearchWork();
        initSearchWorker(searchTextBean);
        this.searchWorker.execute();
    }

    @Override
    public boolean hasSearchResult() {
        return hasSearchResult;
    }

    @Override
    public boolean isSearchOver() {
        return searchOver;
    }

    @Override
    public boolean isNetWorkError() {
        return networkError;
    }

    @Nullable
    public AlphaFineList getSearchResultList() {
        if (searchResultPane != null) {
            return searchResultPane.getSearchResultList();
        }
        return null;
    }

}
