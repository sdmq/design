package com.fr.design.mainframe.alphafine.search.manager.fun;

import com.fr.design.mainframe.alphafine.model.SearchResult;
import com.fr.design.mainframe.alphafine.search.SearchTextBean;

/**
 * Created by XiaXiang on 2017/3/27.
 */
public interface AlphaFineSearchProvider {
    /**
     * 获取默认显示条数
     *
     * @param searchText
     * @return
     */
    SearchResult getLessSearchResult(String[] searchText);

    /**
     * 获取剩余条数
     *
     * @return
     */
    SearchResult getMoreSearchResult(String searchText);


    /**
     * 获取所有搜索结果 取决于具体实现
     *
     * @param searchTextBean
     * @return
     */
    default SearchResult getSearchResult(SearchTextBean searchTextBean) {
        return new SearchResult();
    }
}
