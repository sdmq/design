package com.fr.design.mainframe.alphafine.search.manager.impl;

import com.fr.concurrent.NamedThreadFactory;
import com.fr.design.mainframe.alphafine.AlphaFineConstants;
import com.fr.design.mainframe.alphafine.AlphaFineHelper;
import com.fr.design.mainframe.alphafine.model.ProductNews;
import com.fr.general.http.HttpToolbox;
import com.fr.json.JSON;
import com.fr.json.JSONArray;
import com.fr.json.JSONFactory;
import com.fr.json.JSONObject;
import com.fr.log.FineLoggerFactory;
import java.awt.Image;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import javax.imageio.ImageIO;
import org.jetbrains.annotations.Nullable;

public class ProductNewsSearchManager {

    private static final ProductNewsSearchManager INSTANCE = new ProductNewsSearchManager();
    private static final int TIME_GAP = 12;
    private List<ProductNews> productNewsResultList;

    private List<ProductNews> productNewsList = new ArrayList<>();

    /**
     * 单独记录一份cid的唯一id 用来判断是否已读
     */
    private Set<Long> idSet = new HashSet<>();


    private ScheduledExecutorService service;

    private ProductNewsSearchManager() {
        service = Executors.newSingleThreadScheduledExecutor(new NamedThreadFactory("ProductNewsSearchManager", true));
        service.scheduleAtFixedRate(new Runnable() {
            @Override
            public void run() {
              try {
                  getProductNewsList();
              } catch (Exception e) {
                  FineLoggerFactory.getLogger().error(e.getMessage(), e);
              }
            }
        }, TIME_GAP, TIME_GAP, TimeUnit.HOURS);
    }

    public static ProductNewsSearchManager getInstance() {
        return INSTANCE;
    }

    public List<ProductNews> getSearchResult(String[] searchText) {
        productNewsResultList = new ArrayList<>();
        try {
            List<ProductNews> productNewsList = getProductNewsList();
            for (ProductNews productNews : productNewsList) {
                for (String str : searchText) {
                    if (productNews.getTitle().contains(str)) {
                        productNewsResultList.add(productNews);
                        break;
                    }
                }
            }
        } catch (Exception e) {
            FineLoggerFactory.getLogger().error(e.getMessage(), e);
        }
        return productNewsResultList;
    }


    public List<ProductNews> getProductNewsList() throws Exception {
        productNewsList = new ArrayList<>();
        idSet = new HashSet<>();
        String jsonStr = HttpToolbox.get(AlphaFineConstants.ALPHA_CID);
        AlphaFineHelper.checkCancel();
        JSONObject cidJSON = JSONFactory.createJSON(JSON.OBJECT, jsonStr);
        JSONArray jsonArray = cidJSON.getJSONArray("data");
        for (int i = 0, size = jsonArray.size(); i < size; i++) {
            JSONObject obj = jsonArray.getJSONObject(i);
            ProductNews productNews = new ProductNews().
                    setId(obj.getLong("id")).setTitle(obj.getString("title")).
                    setImage(getCoverImage(obj.getString("pic"))).
                    setUrl(obj.getString("url")).setTag(ProductNews.Tag.parseCode(obj.getInt("tag"))).
                    setStatus(ProductNews.Status.parseCode(obj.getInt("status"))).setTarget(
                    ProductNews.Target.parseCode(obj.getInt("target"))).
                    setCreator(obj.getInt("creator")).setPushDate(new Date(obj.getLong("push_time")));
            Date currentDate = new Date(System.currentTimeMillis());
            // 推送时间check
            if (productNews.getPushDate().before(currentDate)) {
                productNewsList.add(productNews);
                idSet.add(productNews.getId());
            }
        }
        return productNewsList;
    }

    public List<ProductNews> getCachedProductNewsList() {
        return productNewsList;
    }

    public Set<Long> getIdSet() {
        return idSet;
    }

    @Nullable
    private Image getCoverImage(String url) {
        try {
            return ImageIO.read(new URL(url));
        } catch (Exception e) {
            FineLoggerFactory.getLogger().warn("get image failed from {}", url);
            FineLoggerFactory.getLogger().warn(e.getMessage(), e);
        }
        return null;
    }
}
