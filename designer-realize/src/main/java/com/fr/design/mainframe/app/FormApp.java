package com.fr.design.mainframe.app;

import com.fr.base.Parameter;
import com.fr.base.chart.exception.ChartNotFoundException;
import com.fr.base.io.XMLEncryptUtils;
import com.fr.design.DesignerEnvManager;
import com.fr.design.i18n.Toolkit;
import com.fr.design.mainframe.AbstractAppProvider;
import com.fr.design.mainframe.BaseJForm;
import com.fr.design.mainframe.DecodeDialog;
import com.fr.design.mainframe.JForm;
import com.fr.design.mainframe.JTemplate;
import com.fr.design.mainframe.TemplateLockedHandler;
import com.fr.design.worker.open.OpenResult;
import com.fr.design.worker.open.OpenWorker;
import com.fr.exception.DecryptTemplateException;
import com.fr.exception.RemoteDesignPermissionDeniedException;
import com.fr.exception.TplLockedException;
import com.fr.file.FILE;
import com.fr.form.main.Form;
import com.fr.form.ui.container.WBorderLayout;
import com.fr.general.ComparatorUtils;
import com.fr.log.FineLoggerFactory;
import com.fr.report.util.ReadXmlType;
import com.fr.report.util.ReadXmlTypeLocalManager;
import com.fr.stable.Constants;
import com.fr.stable.bridge.StableFactory;
import org.jetbrains.annotations.Nullable;

import java.util.HashMap;
import java.util.concurrent.Callable;

/**
 * Created by juhaoyu on 2018/6/27.
 */
class FormApp extends AbstractAppProvider {

    @Override
    public String[] defaultExtensions() {

        return new String[]{"frm", "form"};
    }

    @Override
    @SuppressWarnings("unchecked")
    public JTemplate<Form, ?> openTemplate(FILE tplFile) {

        HashMap<String, Class> classType = new HashMap<String, Class>();
        classType.put(Constants.ARG_0, Form.class);
        classType.put(Constants.ARG_1, FILE.class);
        FineLoggerFactory.getLogger().info(Toolkit.i18nText("Fine-Design_Report_Template_Opening_And_Waiting", tplFile.getName()) + "...");
        return compatibleForm(tplFile, classType);
    }

    /**
     * 兼容外部注册的frm类型
     *
     * @return
     */
    private JTemplate<Form, ?> compatibleForm(FILE tplFile, HashMap<String, Class> classType) {
        if (ComparatorUtils.equals(StableFactory.getRegisteredClass(BaseJForm.XML_TAG), JForm.class)) {
            classType.put(Constants.ARG_2, Parameter[].class);
            JForm emptyForm = new JForm(new Form(new WBorderLayout("form")), tplFile);
            OpenWorker<OpenResult<Form, Parameter[]>> worker = new OpenWorker<>(
                    new Callable<OpenResult<Form, Parameter[]>>() {
                        @Override
                        public OpenResult<Form, Parameter[]> call() throws Exception {
                            Form form = getForm(tplFile);
                            return new OpenResult<>(form, form.getParameters());
                        }
                    }, emptyForm);
            worker.addCallBack(new Callable<JTemplate<?, ?>>() {
                @Override
                public JTemplate<?, ?> call() throws Exception {
                    OpenResult<Form, Parameter[]> result = worker.getResult();
                    return getJForm(result.getBaseBook(), tplFile, result.getRef());
                }
            });
            worker.start(tplFile.getPath());
            OpenResult<Form, Parameter[]> result = worker.getResult();
            if (result != null) {
                return getJForm(result.getBaseBook(), tplFile, result.getRef());
            }
            return emptyForm;
        } else {
            return (JTemplate<Form, ?>) StableFactory.getMarkedInstanceObjectFromClass(BaseJForm.XML_TAG,
                    new Object[]{getForm(tplFile), tplFile}, classType, BaseJForm.class);
        }
    }

    @Nullable
    private JForm getJForm(Form form, FILE file, Parameter[] parameters) {
        try {
            return new JForm(form, file, parameters);
        } catch (Exception e) {
            FineLoggerFactory.getLogger().error(e, e.getMessage());
            return null;
        }
    }

    @Nullable
    private Form getForm(FILE tplFile) {
        ReadXmlTypeLocalManager.setReadXmlType(ReadXmlType.DESIGN);
        Form form = asIOFile(tplFile);
        ReadXmlTypeLocalManager.remove();
        if (form != null) {
            DesignerAppUtils.dealWithTemplateIOError(tplFile.getPath());
        }
        return form;
    }


    @Override
    public Form asIOFile(FILE file) {

        if (XMLEncryptUtils.isCptEncoded() &&
                !XMLEncryptUtils.checkVaild(DesignerEnvManager.getEnvManager().getEncryptionKey())) {
            if (!new DecodeDialog(file).isPwdRight()) {
                FineLoggerFactory.getLogger().error(com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Report_Engine_ECP_Error_Password"));
                return new Form();
            }
        }

        // peter:打开新报表.
        Form tpl = new Form();
        try {
            tpl.readStream(file.asInputStream());
        } catch (DecryptTemplateException | ChartNotFoundException e) {
            throw e;
        } catch (RemoteDesignPermissionDeniedException exp) {
            FineLoggerFactory.getLogger().error(Toolkit.i18nText("Fine-Design_Basic_Template_Permission_Denied") + file, exp);
        } catch (TplLockedException exp) {
            FineLoggerFactory.getLogger().error(file + Toolkit.i18nText("Fine-Design_Basic_Template_Status_Locked"), exp);
            TemplateLockedHandler.generateTipAndRefresh();
        } catch (Exception exp) {
            FineLoggerFactory.getLogger().error("Failed to generate frm from " + file, exp);
            return null;
        }
        return tpl;
    }
}
