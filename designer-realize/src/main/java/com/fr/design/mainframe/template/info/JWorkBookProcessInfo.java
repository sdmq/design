package com.fr.design.mainframe.template.info;


import com.fr.base.Style;
import com.fr.base.parameter.ParameterUI;
import com.fr.chart.chartattr.ChartCollection;
import com.fr.json.JSONArray;
import com.fr.main.impl.WorkBook;
import com.fr.main.parameter.ReportParameterAttr;
import com.fr.report.cell.TemplateCellElement;
import com.fr.report.cellcase.CellCase;
import com.fr.report.elementcase.TemplateElementCase;
import com.fr.report.report.Report;
import com.fr.report.worksheet.WorkSheet;

import java.util.Iterator;

/**
 * Created by plough on 2017/3/17.
 *
 * @deprecated moved to cloud ops plugin
 */
@Deprecated
public class JWorkBookProcessInfo extends TemplateProcessInfo<WorkBook> {

    public JWorkBookProcessInfo(WorkBook wb) {
        super(wb);
    }

    // 获取模板类型
    public int getReportType() {
        return template.isElementCaseBook() ? 0 : 1;
    }

    // 获取模板格子数
    public int getCellCount() {
        int cellCount = 0;
        if (template.isElementCaseBook()) {  // 如果是普通报表
            for (int i = 0; i < template.getReportCount(); i++) {
                WorkSheet r = (WorkSheet) template.getReport(i);
                CellCase cc = r.getBlock().getCellCase();
                for (int j = 0; j < cc.getRowCount(); j++) {
                    Iterator iter = cc.getRow(j);
                    while (iter.hasNext()) {
                        cellCount++;
                        iter.next();
                    }
                }
            }
        }
        return cellCount;
    }

    // 获取模板悬浮元素个数
    public int getFloatCount() {
        int chartCount = 0;
        if (template.isElementCaseBook()) {  // 如果是普通报表
            for (int i = 0; i < template.getReportCount(); i++) {
                WorkSheet r = (WorkSheet) template.getReport(i);
                Iterator fiter = r.getBlock().floatIterator();
                while (fiter.hasNext()) {
                    chartCount++;
                    fiter.next();
                }
            }
        }
        return chartCount;
    }

    // 获取模板聚合块个数
    public int getBlockCount() {
        return 0;
    }

    // 获取模板控件数
    public int getWidgetCount() {
        ReportParameterAttr attr = template.getReportParameterAttr();
        if (attr == null) {
            return 0;
        }

        ParameterUI pui = attr.getParameterUI();
        return pui == null ? 0 : (pui.getAllWidgets().length - 1);
    }

    @Override
    public boolean isTestTemplate() {
        Iterator<String> it = this.template.getTableDataNameIterator();
        if (!it.hasNext() || isTestECReport() || getCellCount() <= 5) {
            return true;
        }
        return false;
    }

    protected boolean isTestECReport() {
        int count = this.template.getReportCount();
        for (int m = 0; m < count; m++) {
            Report report = this.template.getReport(m);
            Iterator iterator = report.iteratorOfElementCase();
            while (iterator.hasNext()) {
                TemplateElementCase templateElementCase = (TemplateElementCase) iterator.next();
                int columnLength = templateElementCase.getColumnCount();
                int rowLength = templateElementCase.getRowCount();
                for (int i = 0; i < columnLength; i++) {
                    for (int j = 0; j < rowLength; j++) {
                        TemplateCellElement templateCellElement = templateElementCase.getTemplateCellElement(i, j);
                        if (templateCellElement == null) {
                            continue;
                        }
                        Object value = templateCellElement.getValue();
                        if (isTestCell(value, templateCellElement.getStyle())) {
                            return true;
                        }

                    }
                }
            }
        }
        return false;
    }

    protected boolean isTestCell(Object value, Style style) {
        if (value instanceof ChartCollection && isTestChartCollection((ChartCollection) value)) {
            return true;
        }
        return false;
    }


    @Override
    public boolean useParaPane() {
        ReportParameterAttr parameterAttr = this.template.getReportParameterAttr();
        if (parameterAttr == null) {
            return false;
        }
        return parameterAttr.getParameterUI() != null;
    }


    @Override
    public JSONArray getComponentsInfo() {
        return new JSONArray();
    }

    @Override
    public JSONArray getReuseCmpList() {
        return JSONArray.create();
    }

    @Override
    public void updateTemplateOperationInfo(TemplateOperate templateOption) {

    }
}
