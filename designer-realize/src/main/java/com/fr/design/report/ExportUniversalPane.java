package com.fr.design.report;

import com.fr.base.CustomConfig;
import com.fr.config.Configuration;
import com.fr.design.constants.UIConstants;
import com.fr.design.dialog.BasicPane;
import com.fr.design.gui.icheckbox.UICheckBox;
import com.fr.design.gui.ilable.ActionLabel;
import com.fr.design.gui.ilable.UILabel;
import com.fr.design.i18n.Toolkit;
import com.fr.design.layout.FRGUIPaneFactory;
import com.fr.design.utils.BrowseUtils;
import com.fr.general.CloudCenter;
import com.fr.transaction.Configurations;
import com.fr.transaction.WorkerFacade;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.BorderFactory;
import javax.swing.JPanel;

/**
 * @author hades
 * @version 11.0
 * Created by hades on 2022/5/26
 */
public class ExportUniversalPane extends BasicPane {

    private static final String HELP_URL = CloudCenter.getInstance().acquireUrlByKind("help.alt_font.zh_CN", "https://help.fanruan.com/finereport/doc-view-4707.html");

    private UICheckBox specialCharacterExport;

    public ExportUniversalPane() {
        this.setLayout(FRGUIPaneFactory.createBorderLayout());
        this.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
        JPanel outerNorthPane =FRGUIPaneFactory.createTitledBorderPane(Toolkit.i18nText("Fine-Design_Report_Universal_Export_Config"));
        JPanel northPane = FRGUIPaneFactory.createY_AXISBoxInnerContainer_M_Pane();
        JPanel specialCharacterExportPane =FRGUIPaneFactory.createNormalFlowInnerContainer_M_Pane();
        specialCharacterExport = new UICheckBox(Toolkit.i18nText("Fine-Design_Report_Universal_Export_Special_Character"));
        specialCharacterExport.setSelected(true);
        specialCharacterExportPane.add(specialCharacterExport);
        northPane.add(specialCharacterExportPane);
        JPanel labelPane = new JPanel(new BorderLayout());
        labelPane.setBorder(BorderFactory.createEmptyBorder(0, 20, 0, 0));
        UILabel centerLabel = new UILabel(Toolkit.i18nText("Fine-Design_Report_Universal_Export_Special_Character_Tip"));
        centerLabel.setForeground(Color.GRAY);
        ActionLabel rightLabel = new ActionLabel(Toolkit.i18nText("Fine-Design_Report_Universal_Export_More_Alternative_Fonts"), UIConstants.FLESH_BLUE);
        rightLabel.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                BrowseUtils.browser(HELP_URL);
            }
        });
        labelPane.add(centerLabel, BorderLayout.CENTER);
        labelPane.add(rightLabel, BorderLayout.EAST);
        northPane.add(labelPane);
        outerNorthPane.add(northPane);
        this.add(outerNorthPane);
    }

    @Override
    protected String title4PopupWindow() {
        return "ExportUniversalPane";
    }

    public void populate() {
        this.specialCharacterExport.setSelected(CustomConfig.getInstance().isOptimizedSpecialCharacterExport());
    }

    public void update() {
        Configurations.modify(new WorkerFacade(CustomConfig.class) {
            @Override
            public void run() {
                CustomConfig.getInstance().setOptimizedSpecialCharacterExport(specialCharacterExport.isSelected());
            }
        });
    }


}
