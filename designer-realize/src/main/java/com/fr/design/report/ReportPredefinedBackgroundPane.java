package com.fr.design.report;

import com.fr.config.predefined.PredefinedNameStyleProvider;
import com.fr.form.ui.NameBackground;
import com.fr.design.gui.icheckbox.UICheckBox;
import com.fr.design.layout.FRGUIPaneFactory;
import com.fr.design.mainframe.predefined.ui.PredefinedStyleSettingPane;
import com.fr.design.mainframe.predefined.ui.detail.background.BackgroundSettingPane;
import com.fr.design.mainframe.predefined.ui.preview.StyleSettingPreviewPane;
import com.fr.general.Background;
import com.fr.page.ReportSettingsProvider;
import com.fr.report.stable.ReportSettings;

import javax.swing.JPanel;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.geom.Rectangle2D;

/**
 * Created by kerry on 2020-09-02
 */
public class ReportPredefinedBackgroundPane extends PredefinedStyleSettingPane<ReportSettingsProvider> {
    private UICheckBox isPrintBackgroundCheckBox;
    private UICheckBox isExportBackgroundCheckBox;
    private BackgroundSettingPane backgroundPane;


    public ReportPredefinedBackgroundPane() {
        super();
        JPanel sourth = new JPanel();
        isPrintBackgroundCheckBox = new UICheckBox(com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Report_Print_Background"));
        isExportBackgroundCheckBox = new UICheckBox(com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Report_Export_Background"));
        sourth.add(isExportBackgroundCheckBox);
        sourth.add(isPrintBackgroundCheckBox);
        this.add(sourth, BorderLayout.SOUTH);
    }

    @Override
    public String title4PopupWindow() {
        return com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Report_Background");
    }

    @Override
    protected StyleSettingPreviewPane createPreviewPane() {
        return new PreviewPane();
    }

    @Override
    protected JPanel createCustomDetailPane() {
        JPanel jPanel = FRGUIPaneFactory.createBorderLayout_S_Pane();
        backgroundPane = new BackgroundSettingPane();
        backgroundPane.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                previewPane.refresh();
            }
        });
        jPanel.add(backgroundPane, BorderLayout.CENTER);
        return jPanel;
    }


    @Override
    public void populateBean(ReportSettingsProvider reportSettings) {
        this.setPopulating(true);
//        PredefinedNameStyleProvider nameBackground = reportSettings.getNameBackground();
//        super.populate(nameBackground);
        this.backgroundPane.populateBean(reportSettings.getBackground());
        this.isPrintBackgroundCheckBox.setSelected(reportSettings.isPrintBackground());
        this.isExportBackgroundCheckBox.setSelected(reportSettings.isExportBackground());
        this.previewPane.refresh();
        this.setPopulating(false);
    }

    @Override
    public ReportSettingsProvider updateBean() {
        return null;
    }


    public void updateBean(ReportSettingsProvider reportSettings) {
        if (this.predefinedRadioBtn.isSelected()) {
//            ((ReportSettings) reportSettings).setNameBackground(updatePredefinedStyle());
        } else {
            reportSettings.setBackground(this.backgroundPane.updateBean());
        }
        reportSettings.setPrintBackground(this.isPrintBackgroundCheckBox.isSelected());
        reportSettings.setExportBackground(this.isExportBackgroundCheckBox.isSelected());
    }

    private NameBackground updatePredefinedStyle() {
        return NameBackground.createPredefinedStyle(getPredefinedStyleName());
    }

    protected void populateCustomPane() {
        this.backgroundPane.populateBean(updatePredefinedStyle().createRealStyle());
    }

    private Background getCurrentValue() {
        if (this.predefinedRadioBtn.isSelected()) {
            NameBackground nameReportBackground = NameBackground.createPredefinedStyle(getPredefinedStyleName());
            return nameReportBackground.createRealStyle();
        } else {
            return this.backgroundPane.updateBean();
        }
    }

    class PreviewPane extends StyleSettingPreviewPane {
        private Background background;

        public PreviewPane() {
            this.setLayout(FRGUIPaneFactory.createBorderLayout());
            this.setPreferredSize(new Dimension(390, 478));
            this.setBackground(Color.WHITE);
        }


        public void refresh() {
            background = getCurrentValue();
            this.repaint();
        }

        @Override
        public void paint(Graphics g) {
            super.paint(g);
            if (background != null) {
                background.paint(g, new Rectangle2D.Double(0, 0, this.getWidth(), this.getHeight()));
            }
        }
    }


}
