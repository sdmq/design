package com.fr.design.share.effect;

import com.fr.design.beans.BasicBeanPane;
import com.fr.design.gui.controlpane.NameableCreator;
import com.fr.design.share.effect.source.SourceNode;
import com.fr.stable.StringUtils;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

public class BaseEffectItem<T> implements EffectItem<T>{
    private String name = StringUtils.EMPTY;
    private String description = StringUtils.EMPTY;
    private NameableCreator creator;
    private T object;
    private SourceNode sourceNode;

    @Override
    public SourceNode getSourceNode() {
        return sourceNode;
    }

    @Override
    public String getConfigPath() {
        return getSourceNode().toString();
    }

    public void setSourceNode(SourceNode sourceNode) {
        this.sourceNode = sourceNode;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public void setDescription(String description) {
        this.description = description;
    }


    @Override
    public void save() {

    }

    @Override
    public Object getPopulateBean(){
        return getObject();
    }

    @Override
    public void updateBean(Object bean) {

    }

    @Override
    public T getObject() {
        return object;
    }

    @Override
    public void setObject(T ob) {
        this.object = ob;
    }

    @Override
    public NameableCreator creator() {
        return this.creator;
    }

    public void setNameableCreator(NameableCreator creator) {
        this.creator = creator;
    }

    @Override
    public BasicBeanPane createPaneByCreators(NameableCreator creator) {
        try {
            return creator.getUpdatePane().newInstance();
        } catch (InstantiationException e) {
            throw new RuntimeException(e);
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public BasicBeanPane createPaneByCreators(NameableCreator creator, String string) {
        Constructor constructor;
        try {
            constructor = creator.getUpdatePane().getDeclaredConstructor(new Class[]{String.class});
            constructor.setAccessible(true);
            return (BasicBeanPane) constructor.newInstance(string);
        } catch (NoSuchMethodException e) {
            throw new RuntimeException(e);
        } catch (InstantiationException e) {
            throw new RuntimeException(e);
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        } catch (InvocationTargetException e) {
            throw new RuntimeException(e);
        }
    }
}
