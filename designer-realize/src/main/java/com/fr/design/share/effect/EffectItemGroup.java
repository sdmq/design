package com.fr.design.share.effect;

import com.fr.chart.chartattr.Chart;
import com.fr.chart.chartattr.ChartCollection;
import com.fr.chart.chartattr.Plot;
import com.fr.chart.chartglyph.ConditionCollection;
import com.fr.chartx.attr.ChartProvider;
import com.fr.design.share.effect.source.SourceNode;
import com.fr.design.share.effect.source.SourceNodeUtils;
import com.fr.form.FormElementCaseProvider;
import com.fr.form.event.Listener;
import com.fr.form.ui.ChartEditor;
import com.fr.form.ui.ElementCaseEditor;
import com.fr.form.ui.Widget;
import com.fr.form.ui.container.WTitleLayout;
import com.fr.js.NameJavaScript;
import com.fr.js.NameJavaScriptGroup;
import com.fr.plugin.chart.attr.axis.VanChartAlertValue;
import com.fr.plugin.chart.attr.axis.VanChartAxis;
import com.fr.plugin.chart.attr.plot.VanChartPlot;
import com.fr.plugin.chart.attr.plot.VanChartRectanglePlot;
import com.fr.plugin.chart.custom.VanChartCustomPlot;
import com.fr.report.cell.DefaultTemplateCellElement;
import com.fr.report.cell.cellattr.highlight.DefaultHighlight;
import com.fr.report.cell.cellattr.highlight.HighlightGroup;
import com.fr.report.web.util.ReportEngineEventMapping;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;

public class EffectItemGroup {
    private List<ConditionEffectItem> conditionEffectItems = new ArrayList<ConditionEffectItem>();
    private List<HighlightEffectItem> highlightEffectItems = new ArrayList<HighlightEffectItem>();
    private List<HyperlinkEffectItem> hyperlinkEffectItems = new ArrayList<HyperlinkEffectItem>();
    private List<ListenerEffectItem> listenerEffectItems = new ArrayList<ListenerEffectItem>();
    private List<StackAndAxisConditionEffectItem> stackAndAxisConditionEffectItems = new ArrayList<StackAndAxisConditionEffectItem>();
    private List<VanChartAlertValueEffectItem> vanChartAlertValueEffectItems = new ArrayList<VanChartAlertValueEffectItem>();
    private List<EffectItem> effectItems = new ArrayList<EffectItem>();
    private LinkedHashMap<String, List<? extends EffectItem>> effectItemsMap;
    private Widget widget;

    public EffectItemGroup() {

    }

    public EffectItemGroup(Widget widget) {
        this.widget = widget;
        initListener(widget);
        if (widget instanceof WTitleLayout) {
            WTitleLayout wTitleLayout = (WTitleLayout) widget;
            this.widget = wTitleLayout.getBodyBoundsWidget().getWidget();
            if (this.widget instanceof ElementCaseEditor) {
                initElementCase();
            } else if (this.widget instanceof ChartEditor) {
                initChartEditor();
            }
        }
    }

    public Widget getWidget() {
        return widget;
    }

    private void initListener(Widget widget) {
        int listenerSize = widget.getListenerSize();
        for (int index = 0; index < listenerSize; index ++) {
            Listener listener = widget.getListener(index);
            SourceNode sourceNode = SourceNodeUtils.createSourceNode(widget, null);
            this.add(new ListenerEffectItem(listener, sourceNode));
        }
    }

    private String switchLang(String eventName) {
        // 在 properties 文件中找到相应的 key 值
        String localeKey = ReportEngineEventMapping.getLocaleName(eventName);
        return com.fr.design.i18n.Toolkit.i18nText(localeKey);
    }

    private void initElementCase() {
        ElementCaseEditor editor = (ElementCaseEditor) this.widget;
        FormElementCaseProvider elementCase = editor.getElementCase();
        Iterator cellIterator = elementCase.cellIterator();

        while(cellIterator.hasNext()) {
            DefaultTemplateCellElement cellElement = (DefaultTemplateCellElement) cellIterator.next();
            SourceNode cellSourceNode = SourceNodeUtils.createSourceNode(cellElement, null);

            // 条件属性
            if (cellElement.getHighlightGroup() != null) {
                HighlightGroup highlightGroup = cellElement.getHighlightGroup();
                for (int index = 0; index < highlightGroup.size(); index ++) {
                    DefaultHighlight highlight=(DefaultHighlight) highlightGroup.getHighlight(index);
                    this.add(new HighlightEffectItem(highlight, cellSourceNode));
                }
            }

            // 超链
            if (cellElement.getNameHyperlinkGroup() != null) {
                NameJavaScriptGroup nameHyperlinkGroup = cellElement.getNameHyperlinkGroup();
                for (int index = 0; index < nameHyperlinkGroup.size(); index ++) {
                    NameJavaScript nameJavaScript = nameHyperlinkGroup.getNameHyperlink(index);
                    this.add(new HyperlinkEffectItem(nameJavaScript, cellSourceNode));
                }
            }

            // 图表内部
            Object value = cellElement.getValue();
            if (value instanceof ChartCollection) {
                ChartCollection chartCollection = (ChartCollection) value;
                initChartEditorByChartCollection(chartCollection, cellSourceNode);

            }
        }

    }

    private void initChartEditor() {
        ChartEditor editor = (ChartEditor) this.widget;
        ChartCollection chartCollection = (ChartCollection) editor.getChartCollection();
        initChartEditorByChartCollection(chartCollection, null);

    }

    private void initChartEditorByChartCollection(ChartCollection chartCollection, SourceNode sourceNode) {
        for (int index = 0; index < chartCollection.getChartCount(); index++) {
            ChartProvider chartProvider = chartCollection.getChart(index, ChartProvider.class);
            if (chartProvider instanceof Chart) {
                Chart chart= (Chart) chartCollection.getChart(index, ChartProvider.class);
                Plot plot = chart.getPlot();
                if (plot == null) {
                    continue;
                }
                SourceNode plotSourceNode = SourceNodeUtils.createSourceNode(plot, chartCollection.getChartName(index), sourceNode);
                initChartPlot(plot, plotSourceNode, false);
            }
        }
    }

    private void initChartPlot(Plot plot, SourceNode sourceNode, boolean isSubChart) {
        if (plot.getConditionCollection() != null) {
            // 图表条件属性
            ConditionCollection conditionCollection = plot.getConditionCollection();
            for (int j = 0; j < conditionCollection.getConditionAttrSize(); j ++) {
                this.add(new ConditionEffectItem(conditionCollection.getConditionAttr(j), sourceNode, plot));
            }
        }

        if (plot.getHotHyperLink() != null) {
            // 图表超链
            NameJavaScriptGroup nameHyperlinkGroup = plot.getHotHyperLink();
            for (int j = 0; j < nameHyperlinkGroup.size(); j ++) {
                NameJavaScript nameJavaScript = nameHyperlinkGroup.getNameHyperlink(j);
                this.add(new HyperlinkEffectItem(nameJavaScript, sourceNode));
            }
        }

        if (plot instanceof VanChartRectanglePlot) {
            VanChartRectanglePlot rectanglePlot = (VanChartRectanglePlot) plot;
            // 堆积和坐标轴
            ConditionCollection stackAndAxisCondition= rectanglePlot.getStackAndAxisCondition();

            for(int index  = 0; index < stackAndAxisCondition.getConditionAttrSize(); index ++) {
                this.add(new StackAndAxisConditionEffectItem(stackAndAxisCondition.getConditionAttr(index), sourceNode, (VanChartRectanglePlot) plot));
            }
            if (!isSubChart) {
                // 警戒线
                List<VanChartAxis> xAxisList = rectanglePlot.getXAxisList();
                List<VanChartAxis> yAxisList = rectanglePlot.getYAxisList();
                for(VanChartAxis axis : xAxisList) {
                    for(VanChartAlertValue alertValue: axis.getAlertValues()) {
                        this.add(new VanChartAlertValueEffectItem(alertValue, sourceNode, (VanChartRectanglePlot) plot));
                    }
                }
                for(VanChartAxis axis : yAxisList) {
                    for(VanChartAlertValue alertValue: axis.getAlertValues()) {
                        this.add(new VanChartAlertValueEffectItem(alertValue, sourceNode, (VanChartRectanglePlot) plot));
                    }
                }
            }
        }

        if (plot instanceof VanChartCustomPlot) {
            VanChartCustomPlot customPlot = (VanChartCustomPlot) plot;
            List<VanChartPlot> plotList= customPlot.getCustomPlotList();
            for(VanChartPlot innerPlot : plotList) {
                initChartPlot(innerPlot, SourceNodeUtils.createSourceNode(innerPlot, sourceNode), true);
            }
        }
    }

    public List<ConditionEffectItem> getConditionEffectItems() {
        return conditionEffectItems;
    }

    public List<HighlightEffectItem> getHighlightEffectItems() {
        return highlightEffectItems;
    }

    public List<HyperlinkEffectItem> getHyperlinkEffectItems() {
        return hyperlinkEffectItems;
    }

    public List<ListenerEffectItem> getListenerEffectItems() {
        return listenerEffectItems;
    }

    public List<StackAndAxisConditionEffectItem> getStackAndAxisConditionEffectItems() {
        return stackAndAxisConditionEffectItems;
    }

    public List<VanChartAlertValueEffectItem> getVanChartAlertLineEffectItems() {
        return vanChartAlertValueEffectItems;
    }

    public List<EffectItem> getEffectItems() {
        return effectItems;
    }

    public void add(ConditionEffectItem conditionEffectItem) {
        conditionEffectItems.add(conditionEffectItem);
        effectItems.add(conditionEffectItem);
    }

    public void add(HighlightEffectItem highlightEffectItem) {
        highlightEffectItems.add(highlightEffectItem);
        effectItems.add(highlightEffectItem);
    }

    public void add(HyperlinkEffectItem hyperlinkEffectItem) {
        hyperlinkEffectItems.add(hyperlinkEffectItem);
        effectItems.add(hyperlinkEffectItem);
    }

    public void add(ListenerEffectItem listenerEffectItem) {
        listenerEffectItems.add(listenerEffectItem);
        effectItems.add(listenerEffectItem);
    }

    public void add(StackAndAxisConditionEffectItem stackAndAxisConditionEffectItem) {
        stackAndAxisConditionEffectItems.add(stackAndAxisConditionEffectItem);
        effectItems.add(stackAndAxisConditionEffectItem);
    }

    public void add(VanChartAlertValueEffectItem vanChartAlertLineEffectItem) {
        vanChartAlertValueEffectItems.add(vanChartAlertLineEffectItem);
        effectItems.add(vanChartAlertLineEffectItem);
    }

    public LinkedHashMap<String, List<? extends EffectItem>> getEffectItemsWithClassify() {
        if (effectItemsMap != null) {
            return effectItemsMap;
        }
        effectItemsMap = new LinkedHashMap<String, List<? extends EffectItem>>();

        if (listenerEffectItems.size() > 0) {
            effectItemsMap.put(com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Report_Event"), listenerEffectItems);
        }
        if (highlightEffectItems.size() > 0 || conditionEffectItems.size() > 0) {
            List<EffectItem> tmpEffectItems = new ArrayList<>();
            tmpEffectItems.addAll(highlightEffectItems);
            tmpEffectItems.addAll(conditionEffectItems);
            effectItemsMap.put(com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Basic_Condition_Attributes"), tmpEffectItems);
        }
        if (hyperlinkEffectItems.size() > 0) {
            effectItemsMap.put(com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Report_Hyperlink"), hyperlinkEffectItems);
        }
        if (vanChartAlertValueEffectItems.size() > 0 || stackAndAxisConditionEffectItems.size() > 0) {
            List<EffectItem> tmpEffectItems = new ArrayList<>();
            tmpEffectItems.addAll(vanChartAlertValueEffectItems);
            tmpEffectItems.addAll(stackAndAxisConditionEffectItems);
            effectItemsMap.put(com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Report_Other"), tmpEffectItems);
        }
        return effectItemsMap;
    }

    public void save() {
        for (EffectItem effectItem : this.effectItems) {
            effectItem.save();
        }
    }
}
