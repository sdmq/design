package com.fr.design.share.effect;

import com.fr.design.designer.properties.EventPropertyTable;
import com.fr.design.share.effect.source.SourceNode;
import com.fr.design.widget.EventCreator;
import com.fr.form.event.Listener;
import com.fr.form.ui.Widget;

public class ListenerEffectItem extends BaseEffectItem<Listener> {
    private Listener listener;

    public ListenerEffectItem(Listener listener, SourceNode sourceNode) {
        this.listener = listener;
        this.setSourceNode(sourceNode);
        init();
    }

    private void init() {
        Listener object;
        try {
            object = (Listener) listener.clone();
        } catch (CloneNotSupportedException e) {
            object = new Listener(listener.getTargetWidget(), listener.getEventName(), listener.getAction(), listener.isInvokeOnce());
        }
        setObject(object);

        setName(listener.getName());
        this.setNameableCreator(new EventCreator(Widget.EVENT_STATECHANGE, EventPropertyTable.WidgetEventListenerUpdatePane.class));
    }

    @Override
    public void updateBean(Object bean) {
        setObject((Listener) bean);
    }

    public void setName(String name) {
        super.setName(name);
        getObject().setName(name);
    }

    public void save() {
        listener.setName(getObject().getName());
        listener.setEventName(getObject().getEventName());
        listener.setAction(getObject().getAction());
        listener.setInvokeOnce(getObject().isInvokeOnce());
        listener.setTargetWidget(getObject().getTargetWidget());
    }
}
