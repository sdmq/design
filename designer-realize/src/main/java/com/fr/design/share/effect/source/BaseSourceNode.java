package com.fr.design.share.effect.source;

public class BaseSourceNode<T> implements SourceNode<T> {
    private String path;
    private SourceNode parent;
    private T target;

    @Override
    public String toString() {
        if (getParent() != null) {
            return getParent().toString() + "-" + path;
        }
        return path;
    }

    @Override
    public T getTarget() {
        return target;
    }

    public void setTarget(T target) {
        this.target = target;
    }

    @Override
    public SourceNode getParent() {
        return parent;
    }

    @Override
    public void setParent(SourceNode parent) {
        this.parent = parent;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getPath() {
        return path;
    }
}
