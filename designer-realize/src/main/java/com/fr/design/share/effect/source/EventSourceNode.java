package com.fr.design.share.effect.source;

import com.fr.form.ui.Widget;

public class EventSourceNode extends BaseSourceNode<Widget> {
    static final private String DEFAULT_PATH = com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Report_Event");

    public EventSourceNode(Widget widget) {
        this(widget, DEFAULT_PATH, null);
    }

    public EventSourceNode(Widget widget, String path) {
        this(widget, path, null);
    }

    public EventSourceNode(Widget widget, SourceNode parent) {
        this(widget, DEFAULT_PATH, parent);
    }

    public EventSourceNode(Widget widget, String path, SourceNode parent) {
        this.setTarget(widget);
        this.setPath(path);
        this.setParent(parent);
    }
}
