package com.fr.design.share.effect.source;

import com.fr.chart.chartattr.Plot;
import com.fr.form.ui.Widget;
import com.fr.report.cell.DefaultTemplateCellElement;

public class SourceNodeUtils {
    static public SourceNode createSourceNode(Widget widget, SourceNode parent) {
        return new EventSourceNode(widget, parent);
    }

    static public SourceNode createSourceNode(DefaultTemplateCellElement cellElement, SourceNode parent) {
        return new CellSourceNode(cellElement, parent);
    }


    static public SourceNode createSourceNode(Plot plot, SourceNode parent) {
        return new PlotSourceNode(plot, parent);
    }

    static public SourceNode createSourceNode(Plot plot, String chartName, SourceNode parent) {
        return new PlotSourceNode(plot, chartName, parent);
    }

    static public SourceNode getCellSourceNode(SourceNode sourceNode) {
        if (sourceNode instanceof CellSourceNode) {
            return sourceNode;
        }
        if (sourceNode.getParent() != null) {
            return getCellSourceNode(sourceNode.getParent());
        }
        return null;
    }
}
