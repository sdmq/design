package com.fr.design.share.ui.config.table;

import com.fr.design.dialog.DialogActionAdapter;
import com.fr.design.gui.ilable.UILabel;
import com.fr.design.layout.FRGUIPaneFactory;
import com.fr.design.layout.VerticalFlowLayout;
import com.fr.design.share.effect.EffectItem;
import com.fr.design.share.ui.effect.EffectControlUpdatePane;
import com.fr.design.share.ui.effect.EffectPopupEditDialog;
import com.fr.design.share.ui.table.EffectItemEditor;
import com.fr.design.share.utils.ShareDialogUtils;
import com.fr.general.IOUtils;
import com.fr.locale.InterProviderFactory;

import javax.swing.BorderFactory;
import javax.swing.Icon;
import javax.swing.JPanel;
import javax.swing.JTable;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;
import java.util.List;

public class ExpandEffectTable extends JPanel {
    private static final Icon downIcon = IOUtils.readIcon("com/fr/base/images/share/arrow_down.png");
    private static final Icon rightIcon = IOUtils.readIcon("com/fr/base/images/share/arrow_left.png");
    private static final int WIDTH = 460;
    private List<? extends EffectItem> effectItems;
    private String title;
    private boolean expand;
    private UILabel arrow;
    private JPanel contentPane;
    private EffectPopupEditDialog effectPopupEditDialog;
    private EffectControlUpdatePane effectControlUpdatePane;
    private JTable table;

    public ExpandEffectTable(List<? extends EffectItem> effectItems, String title) {
        this.effectItems = effectItems;
        this.title = title;

        initComponent();
    }

    private void initComponent() {
        VerticalFlowLayout layout = new VerticalFlowLayout(VerticalFlowLayout.TOP, 0, 4);
        this.setLayout(layout);
        this.expand = true;

        JPanel headerPane = createHeader();
        contentPane = createContent();

        this.add(headerPane);
        this.add(contentPane);

        this.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                setExpand(!expand);
            }
        });
    }

    private JPanel createHeader() {
        JPanel headerPane = FRGUIPaneFactory.createBorderLayout_S_Pane();
        headerPane.setPreferredSize(new Dimension(WIDTH, 24));
        headerPane.setBackground(new Color(232,232,232));
        UILabel headerTitle = new UILabel(this.title);
        headerTitle.setBorder(BorderFactory.createEmptyBorder(0, 5, 0, 0));

        arrow = new UILabel(downIcon);
        arrow.setOpaque(false);
        arrow.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 5));

        headerPane.add(headerTitle, BorderLayout.WEST);
        headerPane.add(arrow, BorderLayout.EAST);
        return headerPane;
    }


    private JPanel createContent() {
        JPanel content = FRGUIPaneFactory.createBorderLayout_S_Pane();

        // 效果列表
        Object[] columnNames = {
                com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Share_From"),
                com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Share_Rename"),
                com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Share_Edit")
        };
        ConfigTableModel tableModel = new ConfigTableModel(effectItems, columnNames);
        table = new JTable(tableModel);

        table.setRowHeight(24);
        table.getColumnModel().getColumn(0).setPreferredWidth(180);
        table.getColumnModel().getColumn(1).setPreferredWidth(252);
        table.getColumnModel().getColumn(2).setPreferredWidth(33);

        table.setDefaultEditor(ConfigTableModel.class, new EffectItemEditor(table));
        table.setDefaultRenderer(ConfigTableModel.class, new ConfigItemRender());
        table.getTableHeader().setResizingAllowed(false);
        table.getTableHeader().setReorderingAllowed(false);
        table.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                int selectColumn = table.getSelectedColumn();
                if (selectColumn == 2) {
                    int selectRow = table.getSelectedRow();
                    EffectItem selectEffectItem = effectItems.get(selectRow);

                    effectControlUpdatePane = EffectControlUpdatePane.newInstance(selectEffectItem);
                    effectControlUpdatePane.populate();
                    effectPopupEditDialog = EffectPopupEditDialog.newInstance(ShareDialogUtils.getInstance().getConfigDialog(), effectControlUpdatePane);
                    effectPopupEditDialog.setTitle(selectEffectItem.getName());
                    effectPopupEditDialog.addDialogActionListener(new DialogActionAdapter() {
                        @Override
                        public void doOk() {
                            effectControlUpdatePane.update();
                        }
                    });
                    Dimension d = Toolkit.getDefaultToolkit().getScreenSize();
                    effectPopupEditDialog.setLocation((d.width - effectPopupEditDialog.getSize().width) / 2, (d.height - effectPopupEditDialog.getSize().height) / 2);
                    effectPopupEditDialog.setModal(true);
                    effectPopupEditDialog.setVisible(true);
                }
            }
            public void mouseExited(MouseEvent e) {
                int row = table.rowAtPoint(e.getPoint());
                int col = table.columnAtPoint(e.getPoint());
                if ((e.getPoint().getY() < 0) || row < 0 || col < 0) {
                    table.clearSelection();
                }
            }
        });
        table.addMouseMotionListener(new MouseMotionAdapter() {
            @Override
            public void mouseMoved(MouseEvent e) {
                int row = table.rowAtPoint(e.getPoint());
                int col = table.columnAtPoint(e.getPoint());
                table.setRowSelectionInterval(row, row);
                table.setColumnSelectionInterval(col, col);
            }
        });
        content.add(table.getTableHeader(), BorderLayout.NORTH);
        content.add(table,BorderLayout.CENTER);
        return content;
    }

    public void setExpand(boolean isExpand) {
        this.expand = isExpand;
        arrow.setIcon(isExpand ? downIcon : rightIcon);
        contentPane.setVisible(isExpand);
    }

    public JTable getTable() {
        return table;
    }

    public boolean isExpand() {
        return expand;
    }
}
