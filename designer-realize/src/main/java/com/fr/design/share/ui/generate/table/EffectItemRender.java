package com.fr.design.share.ui.generate.table;

import com.fr.base.BaseUtils;
import com.fr.design.gui.ilable.UILabel;
import com.fr.design.i18n.Toolkit;
import com.fr.design.layout.FRGUIPaneFactory;
import com.fr.stable.StringUtils;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.border.Border;
import javax.swing.table.DefaultTableCellRenderer;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Image;
import java.awt.Rectangle;

public class EffectItemRender extends DefaultTableCellRenderer {
    static final private Color HEADER_COLOR = new Color(221, 221, 221);
    static final private Color PLACEHOLDER_COLOR = new Color(193, 193, 193);
    static final private Color NO_EDITABLE_BG = new Color(240, 240, 241);
    static final private Border DEFAULT_NO_FOCUS_BORDER = BorderFactory.createCompoundBorder(BorderFactory.createEmptyBorder(1, 5, 1, 5), BorderFactory.createEmptyBorder(0, 4, 0, 4));
    static final private Border SELECTED_BORDER = BorderFactory.createLineBorder(new Color(160,190,240));

    private PreviewPanel preViewPanel;

    public EffectItemRender() {
        setBorder(BorderFactory.createEmptyBorder(0, 5, 0, 10));
        setLayout(new FlowLayout());
        preViewPanel = new PreviewPanel(StringUtils.EMPTY);
    }

    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
        setText(StringUtils.EMPTY);
        setToolTipText(null);
        setBorder(DEFAULT_NO_FOCUS_BORDER);
        setBackground(null);
        if (column > 0) {
            if (StringUtils.isNotEmpty(value.toString())) {
                setValue(value);
                setToolTipText(value);
                if (!table.isCellEditable(row, column)) {
                    setForeground(PLACEHOLDER_COLOR);
                    setBackground(NO_EDITABLE_BG);
                    setToolTipText(Toolkit.i18nText("Fine-Design_Share_Not_Support_Rename"));
                } else {
                    setForeground(Color.black);
                }

                if (table.getSelectedRow() == row && table.getSelectedColumn() == column && table.isCellEditable(row, column) ) {
                    setBorder(SELECTED_BORDER);
                    setBackground(null);
                    table.setCursor(Cursor.getPredefinedCursor(Cursor.TEXT_CURSOR));
                } else {
                    if (isSelected) {
                        setBackground(table.getSelectionBackground());
                    }

                    table.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
                }
            } else {
                setValue(Toolkit.i18nText("Fine-Design_Share_Need_Rename"));
                setForeground(PLACEHOLDER_COLOR);
            }
            return this;
        } else {
            Rectangle rect = table.getCellRect(row, column, false);
            preViewPanel.populate(value.toString());
            preViewPanel.getTextLabel().setPreferredSize(new Dimension(rect.width - preViewPanel.ICON_SIZE - 20, rect.height));
            return preViewPanel;
        }
    }

    public void setValue(Object value) {
        setText((value == null) ? "" : value.toString());
    }

    private void setToolTipText(Object value) { setToolTipText((value == null) ? "" : value.toString()); }

    private class PreviewPanel extends JPanel {
        public static final int ICON_SIZE = 10;
        private UILabel textLabel;

        public PreviewPanel(String text) {
            init(text);
        }

        private void init(String text) {
            this.setLayout(FRGUIPaneFactory.createBorderLayout());
            this.setBackground(HEADER_COLOR);
            textLabel = new UILabel(text);
            textLabel.setToolTipText(text);
            textLabel.setBorder(BorderFactory.createEmptyBorder(0,5,0,0));
            Image image = BaseUtils.readImage("com/fr/base/images/share/view.png");
            image.getScaledInstance(ICON_SIZE, ICON_SIZE, Image.SCALE_DEFAULT);
            UILabel imageLabel = new UILabel(new ImageIcon(image));
            imageLabel.setBorder(BorderFactory.createEmptyBorder(0,10,0,10));
            this.add(textLabel, BorderLayout.WEST);
            this.add(imageLabel, BorderLayout.EAST);
        }

        public UILabel getTextLabel() {
            return textLabel;
        }

        public void populate(String text) {
            textLabel.setText(text);
            this.setToolTipText(text);
        }
    }
}
