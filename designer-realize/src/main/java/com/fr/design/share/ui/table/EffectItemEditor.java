package com.fr.design.share.ui.table;

import com.fr.design.gui.itextfield.UITextField;

import javax.swing.AbstractCellEditor;
import javax.swing.JTable;
import javax.swing.table.TableCellEditor;
import java.awt.Component;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;

public class EffectItemEditor extends AbstractCellEditor implements TableCellEditor {
    private UITextField textField;

    public EffectItemEditor(JTable table) {
        textField = new UITextField();
        textField.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent e) {
                table.setRowSelectionInterval(table.getEditingRow(),  table.getEditingRow());
                table.setColumnSelectionInterval(table.getEditingColumn(), table.getEditingColumn());
            }
            @Override
            public void mouseExited(MouseEvent e) {
                table.clearSelection();
            }

        });
        textField.addMouseMotionListener(new MouseMotionAdapter() {
            @Override
            public void mouseMoved(MouseEvent e) {
                table.setRowSelectionInterval(table.getEditingRow(),  table.getEditingRow());
                table.setColumnSelectionInterval(table.getEditingColumn(), table.getEditingColumn());
            }
        });
        textField.addFocusListener(new FocusAdapter() {
            public void focusGained(FocusEvent e) {
                table.setRowSelectionInterval(table.getEditingRow(),  table.getEditingRow());
                table.setColumnSelectionInterval(table.getEditingColumn(), table.getEditingColumn());
            }
            @Override
            public void focusLost(FocusEvent e) {
                if ((table.getSelectedRow() < 0) || (table.getSelectedColumn() < 0)) {
                    table.getCellEditor().stopCellEditing();
                }
            }
        });
    }
    @Override
    public Object getCellEditorValue() {
        return textField.getText();
    }

    private void setValue(Object value) {
        textField.setText((value != null) ? value.toString() : "");
    }

    @Override
    public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int column) {
        setValue(value);
        return textField;
    }
}
