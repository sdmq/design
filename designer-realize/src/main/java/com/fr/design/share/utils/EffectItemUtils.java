package com.fr.design.share.utils;

import com.fr.design.share.effect.EffectItemGroup;
import com.fr.form.share.utils.ShareUtils;
import com.fr.form.ui.Widget;

import java.util.ArrayList;
import java.util.List;

public class EffectItemUtils {
    public static List<EffectItemGroup> getEffectItemGroupsByWidget(Widget widget) {
        List<Widget> widgetList = ShareUtils.getAllLeafWidget(widget);
        List<EffectItemGroup> effectItemGroupList = new ArrayList<EffectItemGroup>();
        for (Widget w : widgetList) {
            EffectItemGroup effectItemGroup = new EffectItemGroup(w);
            if (effectItemGroup.getEffectItems().size() > 0) {
                effectItemGroupList.add(effectItemGroup);
            }
        }
        return effectItemGroupList;
    }

    public static boolean hasEffectItem(Widget widget) {
        EffectItemGroup effectItemGroup = new EffectItemGroup(widget);
        return effectItemGroup.getEffectItems().size() > 0;
    }
}
