package com.fr.design.share.utils;

import java.awt.Dialog;

public class ShareDialogUtils {
    private static class InstanceHolder {

        private static ShareDialogUtils helper = new ShareDialogUtils();
    }

    public static ShareDialogUtils getInstance() {

        return InstanceHolder.helper;
    }

    private Dialog shareDialog;
    private Dialog ConfigDialog;

    public void setShareDialog(Dialog shareDialog) {

        this.shareDialog = shareDialog;
    }

    public Dialog getShareDialog() {

        return this.shareDialog;
    }

    public void setConfigDialog(Dialog configDialog) {
        ConfigDialog = configDialog;
    }

    public Dialog getConfigDialog() {
        return ConfigDialog;
    }
}
