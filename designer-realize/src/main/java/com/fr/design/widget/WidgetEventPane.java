package com.fr.design.widget;


import com.fr.design.beans.BasicBeanPane;
import com.fr.design.gui.controlpane.NameableCreator;
import com.fr.design.gui.controlpane.UIListGroupControlPane;
import com.fr.design.gui.frpane.ListenerUpdatePane;
import com.fr.design.i18n.Toolkit;
import com.fr.design.javascript.JavaScriptActionPane;
import com.fr.design.mainframe.CellWidgetPropertyPane;
import com.fr.design.mainframe.DesignerContext;
import com.fr.design.mainframe.ElementCasePane;
import com.fr.design.mainframe.JTemplate;
import com.fr.design.write.submit.DBManipulationPane;
import com.fr.design.write.submit.SmartInsertDBManipulationInWidgetEventPane;
import com.fr.form.event.Listener;
import com.fr.form.ui.Widget;
import com.fr.general.NameObject;
import com.fr.grid.selection.CellSelection;
import com.fr.grid.selection.Selection;
import com.fr.js.Commit2DBJavaScript;
import com.fr.stable.AssistUtils;
import com.fr.stable.Nameable;
import com.fr.write.JavaScriptResourceInfo;

import javax.swing.BorderFactory;
import java.lang.reflect.Constructor;

public class WidgetEventPane extends UIListGroupControlPane {
    private static final Selection NO_SELECTION = new CellSelection(-1, -1, -1, -1);

    private Selection selection = NO_SELECTION;

    private ElementCasePane object;

    private Widget targetWidget;

    public WidgetEventPane(ElementCasePane pane) {
        this.object = pane;
        if (pane != null) {
            selection = pane.getSelection();
        }
        setBorder(BorderFactory.createEmptyBorder(10, 0, 15, 0));
    }

    @Override
    public String getAddItemText() {
        return com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Report_Add_Event");
    }

    /**
     * 生成添加按钮的NameableCreator
     * @return 按钮的NameableCreator
     */
    public NameableCreator[] createNameableCreators() {
        return new NameableCreator[]{
                new EventCreator(Widget.EVENT_STATECHANGE, WidgetEventListenerUpdatePane.class)
        };
    }

    @Override
    public void saveSettings() {
        CellWidgetPropertyPane.getInstance().update(selection);
    }

    @Override
    protected String title4PopupWindow() {
        return com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Report_Event");
    }

    public static class WidgetEventListenerUpdatePane extends ListenerUpdatePane {
        private ElementCasePane epane;

        // 反射会用到
        public WidgetEventListenerUpdatePane() {
            this(null);
        }

        public WidgetEventListenerUpdatePane(ElementCasePane epane) {
            this.epane = epane;
            super.initComponents();
        }

        /**
         *  根据有无单元格创建 DBManipulationPane
         * @return   有单元格。有智能添加单元格等按钮，返回 SmartInsertDBManipulationPane
         */
        private DBManipulationPane autoCreateDBManipulationInWidgetEventPane() {
            JTemplate jTemplate = DesignerContext.getDesignerFrame().getSelectedJTemplate();
            return jTemplate.createDBManipulationPaneInWidget();
        }

        @Override
        protected JavaScriptActionPane createJavaScriptActionPane() {
            return new JavaScriptActionPane() {

                @Override
                protected DBManipulationPane createDBManipulationPane() {
                    if (epane == null && DesignerContext.getDesignerFrame().getSelectedJTemplate() != null) {
                        return autoCreateDBManipulationInWidgetEventPane();
                    }

                    return new SmartInsertDBManipulationInWidgetEventPane(epane);
                }

                @Override
                protected String title4PopupWindow() {
                    return com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Report_Set_Callback_Function");
                }

                @Override
                protected boolean isForm() {
                    return false;
                }

                protected String[] getDefaultArgs() {
                    return new String[0];
                }
            };
        }

        @Override
        protected boolean supportCellAction() {
            return false;
        }

    }

    public void populate(Widget widget) {
        if (widget == null) {
            return;
        }
        this.targetWidget = widget;
        refreshPane(widget, EventCreator.createEventCreator(widget.supportedEvents(), WidgetEventListenerUpdatePane.class));
    }

    /**
     * 更新
     *
     * @return 监听器
     */
    public Listener[] updateListeners(Widget widget) {
        this.targetWidget = widget;
        Nameable[] res = this.update();
        Listener[] res_array = new Listener[res.length];
        for (int i = 0, len = res.length; i < len; i++) {
            res_array[i] = (Listener) ((NameObject) res[i]).getObject();
        }
        return res_array;
    }

    @Override
    public BasicBeanPane createPaneByCreators(NameableCreator creator) {
        try {
            if (object == null) {
                return super.createPaneByCreators(creator);
            } else if (object.getClass().isArray()) {
                return creator.getUpdatePane().getConstructor(object.getClass()).newInstance(object);
            } else {
                Constructor<? extends BasicBeanPane> constructor = getConstructor(creator.getUpdatePane(), object.getClass());
                return constructor == null ? super.createPaneByCreators(creator) : constructor.newInstance(object);
            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * 传进BasicBeanPane的构造函数的参数，可能是
     *
     * @param clazz
     * @param cls
     * @return
     */
    private Constructor<? extends BasicBeanPane> getConstructor(Class<? extends BasicBeanPane> clazz, Class<?> cls) {
        Constructor<? extends BasicBeanPane> constructor = null;
        try {
            constructor = clazz.getConstructor(cls);
        } catch (SecurityException e) {
        } catch (NoSuchMethodException e) {
        }
        if (constructor != null) {
            return constructor;
        } else {
            if (AssistUtils.equals(cls.getName(), Object.class.getName())) {
                return null;
            }
            return getConstructor(clazz, cls.getSuperclass());
        }
    }

    @Override
    public void wrapperListener(Listener listener) {
        if (listener.getAction() instanceof Commit2DBJavaScript) {
            Commit2DBJavaScript commit2DBJavaScript = (Commit2DBJavaScript) listener.getAction();
            JavaScriptResourceInfo.PathNode widgetNode = JavaScriptResourceInfo.PathNode.create(JavaScriptResourceInfo.Type.WIDGET, targetWidget.getWidgetName());
            JavaScriptResourceInfo.PathNode EventNode = JavaScriptResourceInfo.PathNode.create(JavaScriptResourceInfo.Type.EVENT, listener.getName());

            JavaScriptResourceInfo javaScriptResourceInfo = new JavaScriptResourceInfo()
                    .addFirstPathNode(EventNode)
                    .addFirstPathNode(widgetNode);
            commit2DBJavaScript.setJsResourceInfo(javaScriptResourceInfo);
        }
    }

    protected String getWrapperLabelText() {
        return Toolkit.i18nText("Fine-Design_Report_Event");
    }

}
