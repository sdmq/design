package com.fr.design.widget.ui;

import com.fr.design.gui.ilable.UILabel;
import com.fr.design.layout.FRGUIPaneFactory;
import com.fr.design.utils.DesignUtils;
import com.fr.form.ui.Radio;

import javax.swing.SwingConstants;
import java.awt.BorderLayout;
import java.awt.Font;

/**
 * @deprecated
 */
@Deprecated
public class RadioDefinePane extends AbstractDataModify<Radio> {
    public RadioDefinePane() {
        this.iniComoponents();
    }

    private void iniComoponents() {
        this.setLayout(FRGUIPaneFactory.createBorderLayout());

        UILabel infoLabel = new UILabel();
        infoLabel.setFont(DesignUtils.getDefaultGUIFont().applySize(24).applyStyle(Font.BOLD));
        infoLabel.setText(com.fr.design.i18n.Toolkit.i18nText(
                "Fine-Design_Report_No_Editor_Property_Definition") + ".");
        infoLabel.setHorizontalAlignment(SwingConstants.CENTER);

        this.add(infoLabel, BorderLayout.CENTER);
    }

    @Override
    protected String title4PopupWindow() {
        return "radio";
    }

    @Override
    public void populateBean(Radio cellWidget) {
    }

    @Override
    public Radio updateBean() {
        return new Radio();
    }
}
