/*
 * Copyright(c) 2001-2010, FineReport Inc, All Rights Reserved.
 */
package com.fr.poly.creator;

import com.fr.design.mainframe.ElementCasePane;
import com.fr.grid.selection.CellSelection;
import com.fr.report.poly.PolyECBlock;

/**
 * @author richer
 * @since 6.5.4 创建于2011-4-6
 */
public abstract class PolyElementCasePane extends ElementCasePane<PolyECBlock> {

	/**
	 * 水平方向在一个屏幕内可见的可扩展的列数
	 */
	private static final int HORIZONTAL_EXTENT_VALUE = 200;

	public PolyElementCasePane(PolyECBlock block) {
		super(block);
        setSelection(new CellSelection(0, 0, 1, 1));
        // 都不加这两个组件，当然他们也就没可见性了
		setHorizontalScrollBarVisible(false);
		setVerticalScrollBarVisible(false);
		this.getGrid().setHorizontalExtent(HORIZONTAL_EXTENT_VALUE);
	}

    @Override
    protected boolean supportRepeatedHeaderFooter() {
    	return false;
    }
}