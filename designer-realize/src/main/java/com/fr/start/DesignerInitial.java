package com.fr.start;

import com.fr.design.constants.DesignerLaunchStatus;
import com.fr.design.fun.DesignerStartClassProcessor;
import com.fr.design.mainframe.DesignerContext;
import com.fr.design.ui.util.UIUtil;
import com.fr.event.Event;
import com.fr.event.EventDispatcher;
import com.fr.event.Listener;
import com.fr.event.Null;
import com.fr.invoke.Reflect;
import com.fr.stable.bridge.StableFactory;

/**
 * Created by juhaoyu on 2019-06-14.
 * 设计器上下文
 */
public class DesignerInitial {

    private static volatile BaseDesigner designer;

    public static void init(final String... args) {
        UIUtil.invokeLaterIfNeeded(new Runnable() {
            @Override
            public void run() {
                DesignerStartClassProcessor processor = StableFactory.getMarkedInstanceObjectFromClass(DesignerStartClassProcessor.MARK_STRING, DesignerStartClassProcessor.class);
                if (processor != null) {
                    designer = Reflect.on(processor.transform()).create(args).get();
                } else {
                    designer = new MainDesigner(args);
                }
            }
        });
    }

    public static void prepare() {
        UIUtil.invokeLaterIfNeeded(new Runnable() {
            @Override
            public void run() {
                if (designer != null) {
                    designer.show();
                }
            }
        });
        EventDispatcher.listen(DesignerLaunchStatus.OPEN_LAST_FILE_COMPLETE, new Listener<Null>() {
            @Override
            public void on(Event event, Null param) {
                EventDispatcher.stopListen(this);
                UIUtil.invokeLaterIfNeeded(new Runnable() {
                    @Override
                    public void run() {
                        DesignerContext.getDesignerFrame().setVisible(true);
                        DesignerContext.getDesignerFrame().resizeFrame();
                        //启动画面结束
                        SplashContext.getInstance().hide();
                    }
                });
                DesignerLaunchStatus.setStatus(DesignerLaunchStatus.STARTUP_COMPLETE);
            }
        });
    }
}
