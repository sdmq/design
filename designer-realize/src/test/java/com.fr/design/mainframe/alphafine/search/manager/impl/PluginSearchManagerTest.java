package com.fr.design.mainframe.alphafine.search.manager.impl;


import com.fr.invoke.Reflect;
import org.junit.Assert;
import org.junit.Test;

/**
 * @author Lucian.Chen
 * @version 10.0
 * Created by Lucian.Chen on 2020/12/17
 */
public class PluginSearchManagerTest {

    @Test
    public void testIsCompatibleCurrentEnv() {

        Assert.assertFalse(Reflect.on(PluginSearchManager.class).call("isCompatibleCurrentEnv", "~9.0").get());
        Assert.assertTrue(Reflect.on(PluginSearchManager.class).call("isCompatibleCurrentEnv", "9.0").get());
        Assert.assertTrue(Reflect.on(PluginSearchManager.class).call("isCompatibleCurrentEnv", "9~").get());
        Assert.assertTrue(Reflect.on(PluginSearchManager.class).call("isCompatibleCurrentEnv", "10").get());
        Assert.assertFalse(Reflect.on(PluginSearchManager.class).call("isCompatibleCurrentEnv", "11").get());
    }

}
