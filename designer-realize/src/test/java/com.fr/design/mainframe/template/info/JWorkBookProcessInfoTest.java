package com.fr.design.mainframe.template.info;

import com.fr.base.Style;
import com.fr.chart.chartattr.ChartCollection;
import com.fr.form.main.parameter.FormParameterUI;
import com.fr.invoke.Reflect;
import com.fr.json.JSONArray;
import com.fr.main.impl.WorkBook;
import com.fr.main.parameter.ReportParameterAttr;
import com.fr.plugin.chart.vanchart.VanChart;
import org.junit.Assert;
import org.junit.Test;

/**
 * Created by kerry on 2020-05-08
 */
public class JWorkBookProcessInfoTest {

    @Test
    public void testIsTestCell() {
        ChartCollection chartCollection = new ChartCollection();
        chartCollection.addChart(new VanChart());
        JWorkBookProcessInfo processInfo = new JWorkBookProcessInfo(new WorkBook());
        boolean result = Reflect.on(processInfo).call("isTestCell", chartCollection, Style.getInstance()).get();
        Assert.assertTrue(result);
    }


    @Test
    public void testUseParaPane(){
        WorkBook workBook = new WorkBook();
        JWorkBookProcessInfo processInfo = new JWorkBookProcessInfo(workBook);
        Assert.assertFalse(processInfo.useParaPane());

        ReportParameterAttr attr = new ReportParameterAttr();
        workBook.setReportParameterAttr(attr);
        Assert.assertFalse(processInfo.useParaPane());

        attr.setParameterUI(new FormParameterUI());
        Assert.assertTrue(processInfo.useParaPane());
    }

    @Test
    public void testGetComponentsInfo() {
        JWorkBookProcessInfo processInfo = new JWorkBookProcessInfo(new WorkBook());
        JSONArray ja = processInfo.getComponentsInfo();
        Assert.assertEquals(0, ja.size());
    }
}
