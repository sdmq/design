package com.fr.design.cell.editor;

import com.fr.invoke.Reflect;
import org.easymock.EasyMock;
import org.junit.Assert;
import org.junit.Test;

/**
 * Created by kerry on 2020-09-21
 */
public class RichTextToolBarTest {
    @Test
    public void testScaleUpAndDown() {
        RichTextToolBar textToolBar = EasyMock.mock(RichTextToolBar.class);

        int result = Reflect.on(textToolBar).call("scaleUp", 10).get();
        Assert.assertEquals(13, result);
        result = Reflect.on(textToolBar).call("scaleDown", 13).get();
        Assert.assertEquals(10, result);

        result = Reflect.on(textToolBar).call("scaleUp", 11).get();
        Assert.assertEquals(15, result);
        result = Reflect.on(textToolBar).call("scaleDown", 15).get();
        Assert.assertEquals(11, result);

        result = Reflect.on(textToolBar).call("scaleUp", 12).get();
        Assert.assertEquals(16, result);
        result = Reflect.on(textToolBar).call("scaleDown", 16).get();
        Assert.assertEquals(12, result);


        result = Reflect.on(textToolBar).call("scaleUp", 13).get();
        Assert.assertEquals(17, result);
        result = Reflect.on(textToolBar).call("scaleDown", 17).get();
        Assert.assertEquals(13, result);

        result = Reflect.on(textToolBar).call("scaleUp", 14).get();
        Assert.assertEquals(19, result);
        result = Reflect.on(textToolBar).call("scaleDown", 19).get();
        Assert.assertEquals(14, result);

        result = Reflect.on(textToolBar).call("scaleUp", 16).get();
        Assert.assertEquals(21, result);
        result = Reflect.on(textToolBar).call("scaleDown", 21).get();
        Assert.assertEquals(16, result);


        result = Reflect.on(textToolBar).call("scaleUp", 17).get();
        Assert.assertEquals(23, result);
        result = Reflect.on(textToolBar).call("scaleDown", 23).get();
        Assert.assertEquals(17, result);

        result = Reflect.on(textToolBar).call("scaleUp", 17).get();
        Assert.assertEquals(23, result);
        result = Reflect.on(textToolBar).call("scaleDown", 23).get();
        Assert.assertEquals(17, result);

        result = Reflect.on(textToolBar).call("scaleUp", 19).get();
        Assert.assertEquals(25, result);
        result = Reflect.on(textToolBar).call("scaleDown", 25).get();
        Assert.assertEquals(19, result);

        result = Reflect.on(textToolBar).call("scaleUp", 20).get();
        Assert.assertEquals(27, result);
        result = Reflect.on(textToolBar).call("scaleDown", 27).get();
        Assert.assertEquals(20, result);
    }

}
