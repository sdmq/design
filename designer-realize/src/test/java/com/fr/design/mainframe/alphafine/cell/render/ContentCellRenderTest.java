package com.fr.design.mainframe.alphafine.cell.render;

import com.fr.stable.StringUtils;
import org.junit.Test;


import static org.junit.Assert.*;

/**
 * @author Yyming
 * @version 10.0
 * Created by Yyming on 2020/9/16
 */
public class ContentCellRenderTest {

    @Test
    public void highLightModelName() {
        final String regex = "<font.*?</font>";
        String searchText = "workb";
        ContentCellRender contentCellRender = new ContentCellRender(searchText,new String[]{searchText});
        String workbook = contentCellRender.highLightModelName("workbook", new String[]{searchText});
        assertTrue(workbook.endsWith("workb</font>ook</body></HTML>"));
        workbook = contentCellRender.highLightModelName("work book", new String[]{searchText});
        assertTrue(workbook.endsWith("work b</font>ook</body></HTML>"));

        searchText = "我是空格";
        contentCellRender = new ContentCellRender(searchText, new String[]{"我是空格", "我是", "空格"});
        workbook = contentCellRender.highLightModelName("我是 空格", new String[]{searchText});
        assertEquals(workbook.replaceFirst(regex, StringUtils.EMPTY), workbook.replaceAll(regex, StringUtils.EMPTY));

        workbook = contentCellRender.highLightModelName("我是是 空格", new String[]{"我是空格", "我是", "空格"});
        assertNotEquals(workbook.replaceFirst(regex, StringUtils.EMPTY), workbook.replaceAll(regex, StringUtils.EMPTY));
    }
}